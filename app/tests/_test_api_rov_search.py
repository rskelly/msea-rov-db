import json

from django.conf import settings

from .test_utils import *   # This import must occur before the model imports.

class SearchAPITest(APITestCase):

    def test_observation_search(self):

        s, headers = user_login()
        
        print('Getting a cruise: PAC2000-031-1')
        result = s.get(f'{settings.REST_API}/cruises/PAC2000-031-1/', headers=headers)
        cruise = result.json()['result']

        # Setup a search with a cruise, but only one field and bounds.
        params = {
            'cruise_id': cruise['id'],
            'terms': 'pycno',
            'fields': ['common_name'],
            'page': 0,
            'bounds': {'zoom': 5, 'x1': -170., 'x2': -100., 'y1': 80., 'y2': 0.}
        }

        # Search.
        print('Search 1 - No result')
        result = s.post(f'{settings.REST_API}/text_search/search/', data=json.dumps(params), headers=headers)
        data1 = self.check_result(result).get('result')
        
        # Result count should be zero.
        self.assertEqual(data1['page'], 0)
        self.assertEqual(data1['terms'], 'pycno')
        self.assertEqual(data1['totalCount'], 0)
        self.assertEqual(len(data1['results']), 0)

        # Try with the scientific name. Should give results.
        params['fields'].append('scientific_name')

        # Search.
        print('Search 2 - Result')
        result = s.post(f'{settings.REST_API}/text_search/search/', data=json.dumps(params), headers=headers)
        data2 = self.check_result(result).get('result')
        
        # Total count and results should be >0.
        self.assertEqual(data2['page'], 0)
        self.assertEqual(data2['terms'], 'pycno')
        self.assertGreater(data2['totalCount'], 0)
        self.assertGreater(len(data2['results']), 0)

        # Remove the cruise. Should give more results.
        del params['cruise_id']

        # Search.
        print('Search 3 - Without cruise')
        result = s.post(f'{settings.REST_API}/text_search/search/', data=json.dumps(params), headers=headers)
        data3 = self.check_result(result).get('result')

        # Results count is the same (paged) but the total count is greater.
        self.assertEqual(data3['page'], 0)
        self.assertEqual(data3['terms'], 'pycno')
        self.assertGreater(data3['totalCount'], 0)
        self.assertGreater(len(data3['results']), 0)
        self.assertGreater(data3['totalCount'], data2['totalCount'])
        self.assertEqual(len(data3['results']), len(data2['results']))

        # Next page.
        params['page'] = 1

        # Search.
        print('Search 4 - Next page')
        result = s.post(f'{settings.REST_API}/text_search/search/', data=json.dumps(params), headers=headers)
        data4 = self.check_result(result).get('result')

        # Results count is the same (paged) but the total count is greater.
        self.assertEqual(data4['page'], 1)
        self.assertEqual(data4['terms'], 'pycno')
        self.assertGreater(data4['totalCount'], 0)
        self.assertGreater(len(data4['results']), 0)
        self.assertEqual(len(data4['results']), len(data3['results']))
