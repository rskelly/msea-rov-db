"""
These are tests for the Django models used in the database Web application.
"""

import json
from django.test import TransactionTestCase

from .test_utils import *

from msea.models.shared import *
from msea.models.rov import *
from msea.util.util import *


class TestSharedPerson(TransactionTestCase):

    def test_person(self):
        # Create
        p = Person.objects.create(
            biigle_user_id=1,
            biigle_uuid=get_randstr(36),
            first_name=get_randstr(32),
            last_name=get_randstr(32),
            email=get_unique_email(),
            photo=None,
            bio=get_randstr(255)
        )
        self.assertIsNotNone(p)
        self.assertTrue(p.id > 0)

        # Update
        e = get_unique_email()
        p = Person.objects.get(pk=p.id)
        self.assertIsNotNone(p)
        p.email = e
        p.save()

        # Select
        id = p.id
        p = Person.objects.get(pk=id)
        self.assertIsNotNone(p)
        self.assertEqual(e, p.email)

        # Delete
        p.delete()
        p = None
        try:
            p = Person.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(p)


class TestSharedFile(TransactionTestCase):
    """
    Tests for the File and FileType models.
    """

    def test_create_file_type(self):
        # Create
        f = FileType.objects.create(
            name=get_randstr(16),
            short_code=get_randstr(8)
        )
        self.assertIsNotNone(f)
        self.assertTrue(f.id > 0)

        # Select
        id = f.id
        f = FileType.objects.get(pk=id)
        self.assertIsNotNone(f)

        # Update
        name = get_randstr(16)
        f.name = name
        f.save()
        f = FileType.objects.get(pk=id)
        self.assertIsNotNone(f)
        self.assertEqual(f.name, name)

        # Delete
        f.delete()
        f = None
        try:
            f = FileType.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(f)

    def test_file(self):
        # File type.
        ft = FileType.objects.create(
            name=get_randstr(16),
            short_code=get_randstr(8)
        )
        self.assertIsNotNone(ft)

        tmp_file = get_random_file()

        # Create
        f = File.objects.create(
            file_type=ft,
            name=get_randstr(16),
            description=get_randstr(255),
            metadata=get_randstr(32),
            path=tmp_file,
            hash=hash_file(tmp_file),
            blob_url=None
        )
        self.assertIsNotNone(f)
        self.assertTrue(f.id > 0)

        # Update
        id = f.id
        name = get_randstr(16)
        f.name = name
        f.save()
        f = File.objects.get(pk=id)
        self.assertIsNotNone(f)
        self.assertEqual(f.name, name)

        # Update null hash (should fail)
        f.hash = None
        exc = None
        try:
            f.save()
        except Exception as e:
            exc = e
        self.assertIsNotNone(exc)

        # Delete
        f.delete()
        f = None
        try:
            f = File.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(f)


class TestSpatialLibrary(TransactionTestCase):
    """
    Test the SpatialLibrary and SpatialLibraryFile models.
    """

    def test_spatial_library(self):

        geom = get_point()

        # Create
        sl = SpatialLibrary.objects.create(
            name=get_randstr(32),
            note=get_randstr(255),
            thumbnail=get_randbytes(1024),
            geom=geom.ExportToWkb(),
            rast=get_randbytes(1025),
            metadata=json.dumps({'meta': get_randstr(16)})
        )
        self.assertIsNotNone(sl)
        self.assertTrue(sl.id > 0)

        # Select
        id = sl.id
        sl = SpatialLibrary.objects.get(pk=id)
        self.assertIsNotNone(sl)

        # Update
        note = get_randstr(255)
        sl.note = note
        sl.geom = geom.ExportToWkb()  # TODO: Won't resave with the geom binary here.
        sl.save()
        sl = SpatialLibrary.objects.get(pk=id)
        self.assertEqual(note, sl.note)

        # Delete
        sl.delete()
        sl = None
        try:
            sl = SpatialLibrary.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(sl)


    def test_spatial_library_file(self):

        geom = get_point()

        # Create a spatial library.
        sl = SpatialLibrary.objects.create(
            name=get_randstr(32),
            note=get_randstr(255),
            thumbnail=get_randbytes(1024),
            geom=geom.ExportToWkb(),
            rast=get_randbytes(1024),
            metadata=json.dumps({'meta': get_randstr(16)})
        )
        self.assertIsNotNone(sl)
        self.assertTrue(sl.id > 0)

        # Create a file
        ft = FileType.objects.create(
            name=get_randstr(16),
            short_code=get_randstr(8)
        )
        self.assertIsNotNone(ft)

        tmp_file = get_random_file()

        # Associated file object.
        f = File.objects.create(
            file_type=ft,
            name=get_randstr(32),
            description=get_randstr(255),
            metadata=json.dumps({'meta': get_randstr(32)}),
            path=tmp_file,
            hash=hash_file(tmp_file),
            blob_url=get_randstr(255)
        )

        # Create
        slf = SpatialLibraryFile.objects.create(
            spatial_library=sl,
            file=f
        )
        self.assertIsNotNone(slf)
        self.assertTrue(slf.id > 0)

        # Select
        id = slf.id
        slf = None
        try:
            slf = SpatialLibraryFile.objects.get(pk=id)
        except:
            pass
        self.assertIsNotNone(slf)

        tmp_file = get_random_file()

        # Update
        # Associated file object.
        f = File.objects.create(
            file_type=ft,
            name=get_randstr(32),
            description=get_randstr(255),
            metadata=json.dumps({'meta': get_randstr(32)}),
            path=tmp_file,
            hash=hash_file(tmp_file),
            blob_url=get_randstr(255)
        )
        slf.file = f
        slf.save()
        self.assertEqual(slf.file, f)

        # Delete
        id = slf.id
        slf.delete()
        slf = None
        try:
            slf = SpatialLibraryFile.object.get(pk=id)
        except:
            pass
        self.assertIsNone(slf)


class TestSharedSite(TransactionTestCase):

    def test_site(self):

        geom = get_point()

        # Create a spatial library.
        sl = SpatialLibrary.objects.create(
            name=get_randstr(32),
            note=get_randstr(255),
            thumbnail=get_randbytes(1024),
            geom=geom.ExportToWkb(),
            rast=get_randbytes(1024),
            metadata=json.dumps({'meta': get_randstr(16)})
        )
        self.assertIsNotNone(sl)
        self.assertTrue(sl.id > 0)

        # Create
        s = Site.objects.create(
            spatial_library=sl,
            name=get_randstr(32),
            note=get_randstr(255)
        )
        self.assertIsNotNone(s)
        self.assertTrue(s.id > 0)

        # Select
        id = s.id
        s = Site.objects.get(pk=id)
        self.assertIsNotNone(s)

        # Update
        name = get_randstr(32)
        s.name = name
        s.save()
        s = Site.objects.get(pk=id)
        self.assertIsNotNone(s)
        self.assertEqual(s.name, name)

        # Delete
        s.delete()
        s = None
        try:
            s = Site.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(s)


class TestSharedOrgainsation(TransactionTestCase):

    def test_organisation(self):
        # Create
        o = Organisation.objects.create(
            name=get_randstr(32),
            short_code=get_randstr(16),
            country=get_randstr(2),
            note=get_randstr(255)
        )
        self.assertIsNotNone(o)
        self.assertTrue(o.id > 0)

        # Select
        id = o.id
        o = Organisation.objects.get(pk=id)
        self.assertIsNotNone(o)

        # Update
        name = get_randstr(32)
        o.name = name
        o.save()
        o = Organisation.objects.get(pk=id)
        self.assertIsNotNone(o)
        self.assertEqual(o.name, name)

        # Delete
        o.delete()
        o = None
        try:
            o = Organisation.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(o)

