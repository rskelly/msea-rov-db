"""
These are tests for the Django models used in the database Web application.
"""

import json
from django.test import TransactionTestCase

from .test_utils import *

from msea.models.shared import *
from msea.models.rov import *
from msea.util.util import *


class TestROVLookups(TransactionTestCase):
    """
    Tests models that represent lookups, each of which has the same signature.
    Some of these are not specifically lookups but have the same structure.
    """
    # Have a name, note, short_code.
    classes = [Disturbance, MediumType, Protocol, SurveyMode, AnnotationSoftware, Substrate, 
               Relief, Biocover, Complexity, StatusType, EquipmentType, CruiseRole, Thickness, 
               Coverage, MeasurementType]
    # As above with the addition of minimum and maximum.
    classes_minmax = [Thickness, Coverage, MeasurementType]
    # As above with the addition of a unit field.
    classes_unit = [MeasurementType]

    def test_lookups(self):
        for cls in TestROVLookups.classes:

            data = {
                'name': get_randstr(32),
                'note': get_randstr(255),
                'short_code': get_randstr(16)
            }

            if cls in TestROVLookups.classes_minmax:
                min, max = get_minmax()
                data['minimum'] = min
                data['maximum'] = max

            if cls in TestROVLookups.classes_unit:
                data['unit'] = get_randstr(16)

            # Create
            o = cls.objects.create(**data)
            self.assertIsNotNone(o)
            self.assertTrue(o.id > 0)

            # Select
            id = o.id
            o = cls.objects.get(pk=id)
            self.assertIsNotNone(o)

            # Update
            name = get_randstr(32)
            o.name = name
            o.save()
            o = cls.objects.get(pk=id)
            self.assertIsNotNone(o)
            self.assertEqual(o.name, name)

            # Delete
            o.delete()
            o = None
            try:
                o = cls.objects.get(pk=id)
            except:
                pass
            self.assertIsNone(o)


class TestROVModel(TransactionTestCase):

    def test_model(self):

        # Create
        m = Model.objects.create(
            brand_name=get_randstr(64),
            model_name=get_randstr(64),
            attributes=json.dumps({'param': get_randstr(16)}),
            note=get_randstr(255)
        )
        self.assertIsNotNone(m)
        self.assertTrue(m.id > 0)

        # Select
        id = m.id
        m = Model.objects.get(pk=id)
        self.assertIsNotNone(m)

        # Update
        name = get_randstr(64)
        m.brand_name = name
        m.save()
        m = Model.objects.get(pk=id)
        self.assertEqual(m.brand_name, name)

        # Delete
        m.delete()
        m = None
        try:
            m = Model.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(m)


class TestROVProject(TransactionTestCase):

    def test_platform(self):

        m = Model.objects.create(
            brand_name=get_randstr(64),
            model_name=get_randstr(64),
            attributes=json.dumps({'param': get_randstr(16)}),
            note=get_randstr(255)
        )

        o = Organisation.objects.create(
            name=get_randstr(32),
            short_code=get_randstr(16),
            country=get_randstr(2),
            note=get_randstr(255)
        )

        # Create
        pf = Platform.objects.create(
            organisation=o,
            model=m,
            short_code=get_randstr(16),
            name=get_randstr(32),
            serial_number=get_randstr(64),
            retired=get_randdate(),
            attributes=json.dumps({'meta': get_randstr(16)}),
            note=get_randstr(255),
        )
        self.assertIsNotNone(pf)
        self.assertTrue(pf.id > 0)

        # Select
        id = pf.id
        pf = Platform.objects.get(pk=id)
        self.assertIsNotNone(pf)

        # Update
        name = get_randstr(32)
        pf.name = name
        pf.save()
        pf = Platform.objects.get(pk=id)
        self.assertIsNotNone(pf)
        self.assertEqual(pf.name, name)

        # Delete
        pf.delete()
        pf = None
        try:
            pf = Platform.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(pf)


class AnnotationConfigTest(TransactionTestCase):

    # TODO: Need annotation job test.

    def test_annotation_protocol(self):
        c = Person.objects.create(
            first_name=get_randstr(16),
            last_name=get_randstr(16),
            email=get_unique_email()
        )

        r_date = get_randdate()

        # Create
        p = AnnotationProtocol.objects.create(
            name=get_randstr(64),
            creator=c,
            medium_type=MediumType.objects.get(name='Video'),
            annotation_software=AnnotationSoftware.objects.get(name='Biigle'),
            is_template=False,
            image_interval=1,
            image_overlap=False,
            observation_interval=1,
            habitat_interval=1,
            fov_interval=1,
            invertebrate_synoptic=True,
            fish_synoptic=False,
            algae_synoptic=True,
            habitat_only=False,
            species_guide=get_randstr(100),
            note=get_randstr(1000),
        )
        self.assertIsNotNone(p)
        self.assertTrue(p.id > 0)

        # Select
        id = p.id
        p = AnnotationProtocol.objects.get(pk=id)
        self.assertIsNotNone(p)

        # Update
        name = get_randstr(64)
        p.name = name
        p.save()
        p = AnnotationProtocol.objects.get(pk=id)
        self.assertIsNotNone(p)
        self.assertEqual(p.name, name)

        # Delete
        p.delete()
        p = None
        try:
            p = AnnotationProtocol.objects.get(pk=id)
        except:
            pass
        self.assertIsNone(p)

    def test_annotation_protocol_documents(self):
        # Create
        c = Person.objects.create(
            first_name=get_randstr(16),
            last_name=get_randstr(16),
            email=get_unique_email()
        )

        r_date = get_randdate()

        # Create
        p = AnnotationProtocol.objects.create(
            name=get_randstr(64),
            creator=c,
            medium_type=MediumType.objects.get(short_code='video'),
            annotation_software=AnnotationSoftware.objects.get(short_code='biigle'),
            is_template=False,
            image_interval=1,
            image_overlap=False,
            observation_interval=1,
            habitat_interval=1,
            fov_interval=1,
            invertebrate_synoptic=True,
            fish_synoptic=False,
            algae_synoptic=True,
            habitat_only=False,
            species_guide=get_randstr(100),
            note=get_randstr(1000),
        )
        self.assertIsNotNone(p)
        self.assertTrue(p.id > 0)

        d1 = AnnotationProtocolDocument.objects.create(
            annotation_protocol=p,
            document_url=get_randstr(100),
            note=get_randstr(1000)
        )
        d2 = AnnotationProtocolDocument.objects.create(
            annotation_protocol=p,
            document_url=get_randstr(100),
            note=get_randstr(1000)
        )

        self.assertTrue(AnnotationProtocolDocument.objects.filter(annotation_protocol=p).count() == 2)

        d2.delete()

        self.assertTrue(AnnotationProtocolDocument.objects.filter(annotation_protocol=p).count() == 1)

        d1.delete()

        self.assertTrue(AnnotationProtocolDocument.objects.filter(annotation_protocol=p).count() == 0)
