# Unit Testing

This is the location for scripts invoked by Django's built-in unit testing system. The system currently tests three
major topic areas:

* The database and Django models.
* The ReST API.
* The data import system.

Unlike most Django development projects, this one uses an "un-managed" database, meaning that when the application's
data structures are updated, the update does not automatically propagate through to the underlying database. The 
decision to sever the database from Django was made so that the Django project could be abandoned (as technology
changes or the need for it subsides) without affecting the maintenance of the database. 

However, even without the need for the Web application, Django's data model and testing systems are extremely 
convenient for data access and testing (what this document is about).

## Test Structure

The tests folder is structured in the following way:

* test_utils.py -- Classes, utilities, settings and initialization code which are imported and run before each of the
  following tests.
* test_models.py -- Tests for the Django data models.
* test_import.py -- Tests for the import process.
* test_api.py -- Tests for the ReST API.

## Model Testing

In most cases, the model tests simply create, retrieve, update and delete models to ensure that their fields are
compatible with the database, and that necessary actions can be performed without error. 

Prior to running the tests, the following must be completed:

1. Ensure that the [development database](../../model) is installed, populated and running on localhost, 
   and that you have access through the `msea_user` account.
2. Run the `generate_fixtures.py` script in the [utilities](../../utilities) folder of this repo, which
   will extract lookup data from the development database and save it in a location of your choosing.
3. Set the fixture file location in the `FIXTURE_FILE` constant in the `test.py` 
   [settings file](../main/settings).

Then, run the tests in the standard Django way, from the `app/msea` folder: 

    python manage.py tests --settings=main.settings.test

Before the tests are run, the test database (the PostgreSQL server on localhost) is dropped, created and upgraded 
using the `db_manager.py` script and DDL patches in the [model](../../model) folder of the repo. Then,
the fixture file is loaded and the lookup tables populated with existing data. Finally, each model is tested against 
the database.

# Import Testing

The import specifications are defined in documents currently stored on the [MSEA Data Management Working Group 
Teams account](https://086gc.sharepoint.com/:f:/r/sites/SCI-PacificMarineSpatialEcologyandAnalysisSectionESD-DataManagementWorkingGroup/Shared%20Documents/Data%20Management%20Working%20Group/Database%20Import?csf=1&web=1&e=BzwKld).
These documents describe the responsibilities of each of the data-providing units (currently Chief Scientists, 
Annotators, Field Crew and NDST) and provide a format specification for the data they provide in CSV form.

The import test attempts to import a sample dataset using the [import script](../../import). The sample dataset
conforms to the specification; when the specification is updated, the database, import script and data set should be 
updated to reflect it and the test will automatically determine whether the new implementation of specification 
succeeds.

The pre-requisites for model testing also apply to import testing.

## ReST API Testing

The ReST* tests attempt to perform data access and modification through the ReST API. ReST endpoints are coded in the
[app/msea/views](../msea/views) folder and configured in the standard Django `urls.py` file for the `msea`
application. The ReST endpoints are used by the Web application and other programs to access and modify data, and
serve as the window through which the world accesses the database.

The pre-requisites for model testing also apply to ReST testing.

\* ReST stands for Representational State Transfer, a paradigm under which creation, retrieval, modification and 
deletion of data objects can be performed through a "stateless" HTTP interface. Statelessness refers to the fact that
the interface doesn't remember anything between requests. More information is 
[here](https://en.wikipedia.org/wiki/Representational_state_transfer).


