"""
These are utilities and classes used by test cases in the other test files.
"""

import os
import json
import requests
from osgeo import ogr
import random
from datetime import datetime, timezone, timedelta
from django.test.runner import DiscoverRunner
from django.test import SimpleTestCase
from django.conf import settings

from msea.util import util

# # Use the test settings.
# settings = main.settings.test

# # App path.
# sys.path.append(settings.APP_DIR)
# # Model path.
# sys.path.append(settings.MODEL_DIR)
# # Importer path.
# sys.path.append(settings.IMPORT_DIR)
# # PG path.
# sys.path.append(settings.PG_BIN_DIR)
        
class APITestCase(SimpleTestCase):
    """
    Base class for test cases that test the ReST API.
    """

    def check_result(self, result):
        """
        Check the service result. It should have a 200 status code, no error entry and a result entry.
        :param result: The result from a call through requests.
        :return: A dictionary containing the json contents of the response.
        """
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertIsNone(data.get('error'))
        self.assertIsNotNone(data.get('result'))
        return data
    
    def check_error(self, result):
        """
        Check the service result. It should have a 200 status code, no result entry and an error entry.
        :param result: The result from a call through requests.
        """
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertIsNone(data.get('result'))
        self.assertIsNotNone(data.get('error'))

    def check_forbidden(self, result):
        """
        Check the service result. It should have a 403 status code.
        :param result: The result from a call through requests.
        """
        self.assertEqual(result.status_code, 403)

    def check_noauth(self, result):
        """
        Check the service result. It should have a 401 status code.
        :param result: The result from a call through requests.
        """
        self.assertEqual(result.status_code, 401)

def load_config(config_file):
    """
    Load and parse the config file.
    :param config_file: The configuration file; each line is a name and value separated by a colon.
    :return: A dictionary with name-value pairs.
    """
    print('Loading config:', config_file)
    config = {}
    with open(config_file, 'r') as f:
        while True:
            try:
                k, v = list(map(str.strip, f.readline().split(':')))
                config[k] = v
            except:
                break
    return config


def load_credentials():
    """
    Load the credentials from a local file. This
    should have username and password for a user who is a member
    of the msea_admin group. There's a template file in the directory where
    this script is stored; create the credentials file and save it to
    ~/configs/test_credentials.json
    """
    print('Loading credentials.')
    with open(os.path.join(os.path.expanduser('~'), 'configs', 'test_credentials.json'), 'r') as f:
        return json.loads(f.read())


def user_login():
    """
    Log the user in and create a session on the target server.
    Will load the login form and strip the CSRF token out of it using a regex. This is a
    brittle operation, but it works and doesn't require edits to the site.
    Returns a tuple containing the session and the headers dict with a csrf token.
    """
    print('Logging in...')
    credentials = load_credentials()
    s = requests.Session()
    # Log in.
    result = s.post(f'{settings.REST_API}/account/login/', data=credentials)
    data = result.json()['result']
    token = data.get('token')
    if not token:
        raise Exception('Token not returned.')
    # The header is set for JSON because all our requests use that format.
    headers = {'Authorization': f'Token {token}', 'content-type': 'application/json'}
    # Return session and token.
    return s, headers


def get_unique_email():
    import secrets
    token = secrets.token_hex(nbytes=16)
    return f'{token}@test.com'


def get_unique_string(prefix='', length=8):
    import secrets
    token = secrets.token_hex(nbytes=length)
    return f'{prefix}{token}'


def get_random_file(n=256):
    """
    Create a temporary file with random content. Return the file name.
    :return: The file name.
    """
    tmp = util.maketempfile()
    with open(tmp, 'r') as fd:
        os.write(fd, bytes(''.join([chr(random.randint(0, 255)) for _ in range(n)]), 'utf-8'))
    return tmp


def get_point():
    """
    Create and return an OGR point.
    :return: An OGR point.
    """
    geom = ogr.Geometry(ogr.wkbPoint)
    geom.AddPoint(-123, 49)
    return geom


def get_polygon():
    """
    Create and return an OGR polygon.
    :return: An OGR polygon.
    """
    geom = ogr.Geometry(ogr.wkbLinearRing)
    geom.AddPoint(-123, 49)
    geom.AddPoint(-122, 49)
    geom.AddPoint(-122, 50)
    geom.AddPoint(-123, 50)
    geom.AddPoint(-123, 49)
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(geom)
    return poly


def get_randstr(n):
    """
    Create a random string with the given number of characters.
    :param n: The number of characters.
    :return: A string.
    """
    return ''.join([chr(random.randint(32, 126)) for _ in range(n)])


def get_randbytes(n):
    """
    Create a random array of bytes with the given number of bytes.
    :param n: The number of characters.
    :return: A string.
    """
    return bytes(''.join([chr(random.randint(0, 256)) for _ in range(n)]), 'utf-8')


def get_minmax(mini=-100, maxi=100):
    """
    Return two numbers, one lower than the other.
    :return: Two numbers, one lower than the other.
    """
    return mini, maxi if mini < maxi else maxi, mini


def get_randdate(past=False, future=False):
    """
    Get a random date. If past is true, it's in the past.
    If future is true it's in the future. If both are true or
    false, it's random.
    :param past: True, for a date in the past.
    :param future: True, for a date in the future.
    :return: A random date.
    """
    ts = datetime.now().timestamp()
    if past:
        ts += random.randint(-99999999, -1)
    elif future:
        ts += random.randint(1, 99999999)
    return datetime.fromtimestamp(ts).replace(tzinfo=timezone.utc)

def get_datestr(offset=0):
    """
    Return an ISO-formatted date string. If offset is given,
    the date is offset by that number of hours.
    """
    return (datetime.now() + timedelta(hours=offset)).strftime('%Y-%m-%dT%H:%M:%SZ')

class MSEATestRunner(DiscoverRunner):
    """
    Disables database setup and teardown: we only test the API here.
    """

    def setup_databases(self, **kwargs):
        return

    def teardown_databases(self, old_config, **kwargs):
        return
    
