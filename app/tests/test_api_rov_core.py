import secrets

from .test_utils import *   # This import must occur before the model imports.

from msea.models.shared import *
from msea.models.rov import *
from msea.util.util import *


def unique_email():
    """
    Return a unique email address.
    :return: A unique email address.
    """
    return '{}@test.com'.format(secrets.token_hex(nbytes=16))


class TestPeopleAPI(APITestCase):

    def is_valid_person(self, item):
        """
        Check that a person has the correct properties and they are set.
        :param item: A dictionary representing a person entity.
        """
        self.assertIsNotNone(item)
        self.assertIsNotNone(item.get('id'))
        self.assertIsNotNone(item.get('first_name'))
        self.assertIsNotNone(item.get('last_name'))
        # self.assertIsNotNone(item.get('email')) TODO: Null allowed?

    def test_people(self):
        """
        Test the /people service.
        """
        s, headers = user_login()

        # Create a user.
        params = json.dumps({
            'first_name': get_unique_string('TestFirstName'),
            'last_name': get_unique_string('TestLastName'),
            'email': unique_email()
        })

        print('Create person without auth.')
        result = s.post(f'{settings.REST_API}/people/', data=params)
        self.check_noauth(result)

        print('Create person.')
        result = s.post(f'{settings.REST_API}/people/', data=params, headers=headers)
        result = self.check_result(result)

        person = result['result']
        self.is_valid_person(person)
        id = int(person.get('id', 0))
        self.assertTrue(id > 0)

        print('Get people without auth.')
        result = s.get(f'{settings.REST_API}/people/')
        self.check_noauth(result)

        print('Get people.')
        result = s.get(f'{settings.REST_API}/people/', headers=headers)  
        result = self.check_result(result)

        lst = result['result']
        self.assertIsNotNone(lst)
        self.assertTrue(isinstance(lst, list))
        for item in lst:
            self.is_valid_person(item)

        print('Get single person without auth.')
        result = s.get(f'{settings.REST_API}/people/{id}/')
        result = self.check_noauth(result)

        print('Get person.')
        result = s.get(f'{settings.REST_API}/people/{id}/', headers=headers)
        result = self.check_result(result)

        item = result['result']
        self.is_valid_person(item)
        self.assertTrue(int(item.get('id', 0)), id)

        print('Delete person without auth.')
        result = s.delete(f'{settings.REST_API}/people/{id}/')
        result = self.check_noauth(result)

        print('Delete person.')
        result = s.delete(f'{settings.REST_API}/people/{id}/', headers=headers)
        result = self.check_result(result)

        self.assertTrue(result['result'])


# Sites
class TestSitesAPI(APITestCase):

    def test_get_sites(self):
        """
        Test the sites service.
        """
        s, headers = user_login()

        print('Get sites without auth.')
        result = requests.get(f'{settings.REST_API}/sites/')
        self.check_noauth(result)

        print('Get sites.')
        result = requests.get(f'{settings.REST_API}/sites/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Programs and Members
class TestProgramsAPI(APITestCase):

    def test_get_programs(self):
        """
        Test the programs service.
        """
        s, headers = user_login()

        print('Get programs without auth.')
        result = requests.get(f'{settings.REST_API}/programs/')
        self.check_noauth(result)

        print('Get programs.')
        result = requests.get(f'{settings.REST_API}/programs/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Program Roles
class TestProgramRolesAPI(APITestCase):

    def test_get_program_roles(self):
        """
        Test the program roles service.
        """
        s, headers = user_login()

        print('Get program roles without auth.')
        result = requests.get(f'{settings.REST_API}/program_roles/')
        self.check_noauth(result)

        print('Get program roles.')
        result = requests.get(f'{settings.REST_API}/program_roles/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Hardware Inventory
class TestHardwareInventoryAPI(APITestCase):
    pass


# Hardware Configuration
class TestHardwareConfigurationAPI(APITestCase):
    pass


# Measurement Types
class TestMeasurementTypesAPI(APITestCase):

    def test_measurement_types(self):
        """
        Test the media type service.
        """
        s, headers = user_login()

        params = json.dumps({
            'name': get_unique_string('Test Measurement Type'),
            'unit': 'm'
        })

        print('Create measurement type without auth.')
        result = s.post(f'{settings.REST_API}/measurement_types/', data=params)
        self.check_noauth(result)

        print('Create measurement type.')
        result = s.post(f'{settings.REST_API}/measurement_types/', data=params, headers=headers)
        result = self.check_result(result)
        type = result['result']
        type_id = type.get('id', 0)
        self.assertGreater(type_id, 0)

        print('Get measurement type without auth.')
        result = s.get(f'{settings.REST_API}/measurement_types/')
        self.check_noauth(result)

        print('Get measurement type.')
        result = s.get(f'{settings.REST_API}/measurement_types/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))

        print('Delete measurement type without auth.')
        result = s.delete(f'{settings.REST_API}/measurement_types/{type_id}/')
        self.check_noauth(result)

        print('Delete measurement type.')
        result = s.delete(f'{settings.REST_API}/measurement_types/{type_id}/', headers=headers)
        result = self.check_result(result)

# Cruise Roles
class TestCruiseRolesAPI(APITestCase):

    def test_cruise_roles(self):
        """
        Test the media type service.
        """
        s, headers = user_login()

        params = json.dumps({
            'name': get_unique_string('Test Cruise Role'),
        })

        print('Create cruise role without auth.')
        result = s.post(f'{settings.REST_API}/cruise_roles/', data=params)
        self.check_noauth(result)

        print('Create cruise role.')
        result = s.post(f'{settings.REST_API}/cruise_roles/', data=params, headers=headers)
        result = self.check_result(result)
        type = result['result']
        type_id = type.get('id', 0)
        self.assertGreater(type_id, 0)

        print('Get cruise role without auth.')
        result = s.get(f'{settings.REST_API}/cruise_roles/')
        self.check_noauth(result)

        print('Get cruise role.')
        result = s.get(f'{settings.REST_API}/cruise_roles/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))

        print('Delete cruise role without auth.')
        result = s.delete(f'{settings.REST_API}/cruise_roles/{type_id}/')
        self.check_noauth(result)

        print('Delete cruise role.')
        result = s.delete(f'{settings.REST_API}/cruise_roles/{type_id}/', headers=headers)
        result = self.check_result(result)


# Dives
class TestDivesAPI(APITestCase):

    def test_dives(self):
        """
        Test the media type service.
        """
        s, headers = user_login()

        print('Get cruise PAC2000-031-1.')
        result = requests.get(f'{settings.REST_API}/cruises/PAC2019-015-1/', headers=headers)
        result = self.check_result(result)
        cruise = result['result']
        cruise_id = cruise['id']

        print('Get platforms.')
        result = requests.get(f'{settings.REST_API}/platforms/', headers=headers)
        result = self.check_result(result)
        sub, ship = result['result'][:2]

        # print('Get platform configs.')
        # result = requests.get(f'{settings.REST_API}/platform_configs/', headers=headers)
        # result = self.check_result(result)
        # sub_config, ship_config = result['result'][:2]

        # print('Get person.')
        # result = requests.get(f'{settings.REST_API}/people/', headers=headers)
        # result = self.check_result(result)
        # person = result['result'][0]

        # print('Get dive role.')
        # result = requests.get(f'{settings.REST_API}/dive_roles/', headers=headers)
        # result = self.check_result(result)
        # role = result['result'][0]

        params = json.dumps({
            'cruise_id': cruise_id,
            'name': get_unique_string('Test Dive'),
            'start_time': get_datestr(0),
            'end_time': get_datestr(3),
            'objectives': 'Objective',
            'summary': 'Summary',
            'note': 'Note',
            'site': None,
            'sub_config': {
                'name': 'Sub Config ' + get_randstr(5),
                'platform': sub,
                'instrument_configs': []
            },
            'ship_config': {
                'name': 'Ship Config ' + get_randstr(5),
                'platform': ship,
                'instrument_configs': []
            },
            'transects': [
                {
                    'name': 'Test Transect ' + get_randstr(5), 
                    'start_time': get_datestr(1),
                    'end_time': get_datestr(2),
                    'objective': 'Objective',
                    'summary': 'Summary',
                    'note': 'Notes'
                }
            ],
            'crew': [
                {
                    'person': {
                        'first_name': 'Firstname_' + get_randstr(5),
                        'last_name': 'Lastname_' + get_randstr(5),
                        'email': unique_email(),
                    },
                    'dive_role': {
                        'name': 'Test Role ' + get_randstr(5),
                    },
                    'dive': None
                },
            ]
        })

        print('Create dive without auth.')
        result = s.post(f'{settings.REST_API}/dives/', data=params)
        self.check_noauth(result)

        print('Create dive.')
        result = s.post(f'{settings.REST_API}/dives/', data=params, headers=headers)
        result = self.check_result(result)
        dive = result['result']
        dive_id = dive.get('id', 0)
        self.assertGreater(dive_id, 0)

        print('Get dive without auth.')
        result = s.get(f'{settings.REST_API}/dives/')
        self.check_noauth(result)

        print('Get dive.')
        result = s.get(f'{settings.REST_API}/dives/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))

        print('Get dives for cruise PAC2000-031-1 without auth.')
        result = requests.get(f'{settings.REST_API}/cruises/{cruise_id}/dives/')
        self.check_noauth(result)

        print('Get dives for cruise PAC2000-031-1.')
        result = requests.get(f'{settings.REST_API}/cruises/{cruise_id}/dives/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))

        print('Delete dive without auth.')
        result = s.delete(f'{settings.REST_API}/dives/{dive_id}/')
        self.check_noauth(result)

        print('Delete dive.')
        result = s.delete(f'{settings.REST_API}/dives/{dive_id}/', headers=headers)
        result = self.check_result(result)


# Dive Roles
class TestDiveRolesAPI(APITestCase):

    def test_get_dive_roles(self):
        """
        Test the dive roles service.
        """
        s, headers = user_login()

        print('Get dive roles without auth.')
        result = requests.get(f'{settings.REST_API}/dive_roles/')
        self.check_noauth(result)

        print('Get dive roles.')
        result = requests.get(f'{settings.REST_API}/dive_roles/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Transects
class TestTransectsAPI(APITestCase):

    def test_get_transects(self):
        """
        Test the transects service.
        """
        s, headers = user_login()

        # Get the cruise.
        print('Get cruise PAC2000-031-1.')
        result = requests.get(f'{settings.REST_API}/cruises/PAC2019-015-1/', headers=headers)
        result = self.check_result(result)
        cruise = result['result']
        cruise_id = cruise['id']

        # Try to get dives for the cruise.
        print('Get dives for cruise PAC2000-031-1.')
        result = requests.get(f'{settings.REST_API}/cruises/{cruise_id}/dives/', headers=headers)
        result = self.check_result(result)
        dive = result['result'][0]
        dive_id = dive['id']
        dive_name = dive['name']

        print('Get transects for cruise PAC2000-031-1, dive', dive_name, 'without auth.')
        result = requests.get(f'{settings.REST_API}/dives/{dive_id}/transects/')
        self.check_noauth(result)

        print('Get transects for cruise PAC2000-031-1, dive', dive_name, '.')
        result = requests.get(f'{settings.REST_API}/dives/{dive_id}/transects/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Medium Formats
class TestMediumFormatsAPI(APITestCase):

    def test_medium_formats(self):
        """
        Test the media format service.
        """
        s, headers = user_login()

        result = s.get(f'{settings.REST_API}/medium_types/', headers=headers)
        result = self.check_result(result)
        mt = result['result'][0]

        params = json.dumps({
            'name': get_unique_string('Test Media Format'),
            'medium_type': mt
        })

        print('Create medium format without auth.')
        result = s.post(f'{settings.REST_API}/medium_formats/', data=params)
        self.check_noauth(result)

        print('Create medium format.')
        result = s.post(f'{settings.REST_API}/medium_formats/', data=params, headers=headers)
        result = self.check_result(result)
        format = result['result']
        format_id = format.get('id', 0)
        self.assertGreater(format_id, 0)

        print('Get medium format without auth.')
        result = s.get(f'{settings.REST_API}/medium_formats/')
        self.check_noauth(result)

        print('Get medium format.')
        result = s.get(f'{settings.REST_API}/medium_formats/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))

        print('Delete medium format without auth.')
        result = s.delete(f'{settings.REST_API}/medium_formats/{format_id}/')
        self.check_noauth(result)

        print('Delete medium format.')
        result = s.delete(f'{settings.REST_API}/medium_formats/{format_id}/', headers=headers)
        result = self.check_result(result)

# Medium Types
class TestMediumTypesAPI(APITestCase):

    def test_medium_types(self):
        """
        Test the media type service.
        """
        s, headers = user_login()

        params = json.dumps({
            'name': get_unique_string('Test Media Type'),
        })

        print('Create medium type without auth.')
        result = s.post(f'{settings.REST_API}/medium_types/', data=params)
        self.check_noauth(result)

        print('Create medium type.')
        result = s.post(f'{settings.REST_API}/medium_types/', data=params, headers=headers)
        result = self.check_result(result)
        type = result['result']
        type_id = type.get('id', 0)
        self.assertGreater(type_id, 0)

        print('Get medium type without auth.')
        result = s.get(f'{settings.REST_API}/medium_types/')
        self.check_noauth(result)

        print('Get medium type.')
        result = s.get(f'{settings.REST_API}/medium_types/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))

        print('Delete medium type without auth.')
        result = s.delete(f'{settings.REST_API}/medium_types/{type_id}/')
        self.check_noauth(result)

        print('Delete medium type.')
        result = s.delete(f'{settings.REST_API}/medium_types/{type_id}/', headers=headers)
        result = self.check_result(result)


# Abundance
class TestAbundanceAPI(APITestCase):

    def test_get_abundances(self):
        """
        Test the abundance service.
        """
        s, headers = user_login()

        print('Get abundances without auth.')
        result = requests.get(f'{settings.REST_API}/abundances/')
        self.check_noauth(result)

        print('Get abundances.')
        result = requests.get(f'{settings.REST_API}/abundances/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Biocover
class TestBiocoverAPI(APITestCase):

    def test_get_biocovers(self):
        """
        Test the biocovers service.
        """
        s, headers = user_login()

        print('Get biocovers without auth.')
        result = requests.get(f'{settings.REST_API}/biocovers/')
        self.check_noauth(result)

        print('Get biocovers.')
        result = requests.get(f'{settings.REST_API}/biocovers/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Complexity
class TestComplexityAPI(APITestCase):

    def test_get_complexities(self):
        """
        Test the complexities service.
        """
        s, headers = user_login()

        print('Get complexities without auth.')
        result = requests.get(f'{settings.REST_API}/complexities/')
        self.check_noauth(result)

        print('Get complexities.')
        result = requests.get(f'{settings.REST_API}/complexities/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Observation Confidence
class TestObservationConfidenceAPI(APITestCase):

    def test_get_observation_confidences(self):
        """
        Test the observation confidence service.
        """
        s, headers = user_login()

        print('Get observation confidences without auth.')
        result = requests.get(f'{settings.REST_API}/observation_confidences/')
        self.check_noauth(result)

        print('Get observation confidences.')
        result = requests.get(f'{settings.REST_API}/observation_confidences/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Coverage
class TestCoverageAPI(APITestCase):

    def test_get_coverages(self):
        """
        Test the coverge service.
        """
        s, headers = user_login()

        print('Get coverages without auth.')
        result = requests.get(f'{settings.REST_API}/coverages/')
        self.check_noauth(result)

        print('Get coverages.')
        result = requests.get(f'{settings.REST_API}/coverages/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Disturbance
class TestDisturbanceAPI(APITestCase):

    def test_get_disturbances(self):
        """
        Test disturbance service.
        """
        s, headers = user_login()

        print('Get disturbances without auth.')
        result = requests.get(f'{settings.REST_API}/disturbances/')
        self.check_noauth(result)

        print('Get disturbances.')
        result = requests.get(f'{settings.REST_API}/disturbances/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Image Quality
class TestImageQualityAPI(APITestCase):

    def test_get_image_quality(self):
        """
        Test the image quality service.
        """
        s, headers = user_login()

        print('Get image qualities without auth.')
        result = requests.get(f'{settings.REST_API}/image_qualities/')
        self.check_noauth(result)

        print('Get image qualities.')
        result = requests.get(f'{settings.REST_API}/image_qualities/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Protocol
class TestProtocolAPI(APITestCase):

    def test_get_thickness(self):
        """
        Test thickness service.
        """
        s, headers = user_login()

        print('Get protocols without auth.')
        result = requests.get(f'{settings.REST_API}/protocols/')
        self.check_noauth(result)

        print('Get protocols.')
        result = requests.get(f'{settings.REST_API}/protocols/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Relief
class TestReliefAPI(APITestCase):

    def test_get_relief(self):
        """
        Test the relief service.
        """
        s, headers = user_login()

        print('Get reliefs without auth.')
        result = requests.get(f'{settings.REST_API}/reliefs/')
        self.check_noauth(result)

        print('Get reliefs.')
        result = requests.get(f'{settings.REST_API}/reliefs/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Substrates
class TestSubstratesAPI(APITestCase):

    def test_get_substrates(self):
        """
        Test the substrate service.
        """
        s, headers = user_login()

        print('Get substrates without auth.')
        result = requests.get(f'{settings.REST_API}/substrates/')
        self.check_noauth(result)

        print('Get substrates.')
        result = requests.get(f'{settings.REST_API}/substrates/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Survey Modes
class TestSurveyModesAPI(APITestCase):

    def test_get_survey_modes(self):
        """
        Test the survey mode service.
        """
        s, headers = user_login()

        print('Get survey mode without auth.')
        result = requests.get(f'{settings.REST_API}/survey_modes/')
        self.check_noauth(result)

        print('Get survey mode.')
        result = requests.get(f'{settings.REST_API}/survey_modes/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


# Thickness
class TestThicknessAPI(APITestCase):

    def test_get_thickness(self):
        """
        Test the thickness service.
        """
        s, headers = user_login()

        print('Get thickness without auth.')
        result = requests.get(f'{settings.REST_API}/thicknesses/')
        self.check_noauth(result)

        print('Get thickness.')
        result = requests.get(f'{settings.REST_API}/thicknesses/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


class TestAnnotationProtocolAPI(APITestCase):

    def test_annotation_software(self):
        """
        Test annotation software service.
        """
        s, headers = user_login()

        print('Get annotation software without auth.')
        result = s.get(f'{settings.REST_API}/annotation_software/')
        self.check_noauth(result)

        print('Get annotation software.')
        result = s.get(f'{settings.REST_API}/annotation_software/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))

    def test_annotation_protocols(self):
        """
        Test annotation protocol service.
        """
        s, headers = user_login()

        print('Get annotation protocols without auth.')
        result = s.get(f'{settings.REST_API}/annotation_protocols/')
        self.check_noauth(result)

        print('Get annotation protocols.')
        result = s.get(f'{settings.REST_API}/annotation_protocols/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        for item in items:
            self.assertIsNotNone(item.get('name'))


class TestInstrumentConfigurationAPI(APITestCase):

    def test_instrument_configs(self):
        """
        Test instrument config service.
        """
        s, headers = user_login()

        # The instrument config list is empty so we gotta make one.
        print('Get an instrument.')
        result = s.get(f'{settings.REST_API}/instruments/', headers=headers)
        result = self.check_result(result)
        inst = result['result'][0]

        print('Get a platform.')
        result = s.get(f'{settings.REST_API}/platforms/', headers=headers)
        result = self.check_result(result)
        platform = result['result'][0]

        pc = json.dumps({
            'platform': platform,
            'instrument_configs': [],
            'configuration': {'test': 'test config'},
            'note': 'This is a platform config.'
        })

        # Create a platform config. Will be deleted after.
        print('Create and retrieve a config.')
        result = s.post(f'{settings.REST_API}/platform_configs/', data=pc, headers=headers)
        result = self.check_result(result)
        pc = result['result']
        pc_id = pc['id']

        # Configure the configuration.
        ic = json.dumps({
            'instrument': inst, 
            'platform_config': pc_id,
            'name': 'Test Instrument Config ' + get_randstr(5),
            'note': 'This is a test instrument config.',
            'configuration': {'test': 'test config 1'},
        })

        # Create the configuration.
        print('Create instrument config without auth.')
        result = s.post(f'{settings.REST_API}/instrument_configs/', data=ic)
        self.check_noauth(result)

        # Create the configuration.
        print('Create instrument config.')
        result = s.post(f'{settings.REST_API}/instrument_configs/', data=ic, headers=headers)
        result = self.check_result(result)
        ic = result['result']
        ic_id = ic['id']

        print('Get instrument configs without auth.')
        result = s.get(f'{settings.REST_API}/instrument_configs/')
        self.check_noauth(result)

        # Retrieve the list.
        print('Get instrument configs.')
        result = s.get(f'{settings.REST_API}/instrument_configs/', headers=headers)
        result = self.check_error(result)

        # Try to retrieve the new IC.
        print('Get instrument config', ic_id, 'without auth.')
        result = s.get(f'{settings.REST_API}/instrument_configs/{ic_id}/')
        self.check_noauth(result)

        print('Get instrument config', ic_id, '.')
        result = s.get(f'{settings.REST_API}/instrument_configs/{ic_id}/', headers=headers)
        result = self.check_result(result)

        item = result['result']
        self.assertIsNotNone(item)
        self.assertTrue(isinstance(item, dict))

        # Try to get the instruments associated with the configuration.
        print('Get instruments for config', ic_id, 'without auth.')
        result = s.get(f'{settings.REST_API}/instrument_configs/{ic_id}/instruments/')
        self.check_noauth(result)

        print('Get instruments for config', ic_id, 'without auth.')
        result = s.get(f'{settings.REST_API}/instrument_configs/{ic_id}/instruments/', headers=headers)
        result = self.check_result(result)

        items = result['result']
        self.assertIsNotNone(items)
        self.assertTrue(isinstance(items, list))
        self.assertTrue(len(items) > 0)

        print('Delete instrument config', ic_id, 'without auth.')
        result = s.delete(f'{settings.REST_API}/instrument_configs/{ic_id}/')
        result = self.check_noauth(result)

        print('Delete instrument config', ic_id, '.')
        result = s.delete(f'{settings.REST_API}/instrument_configs/{ic_id}/', headers=headers)
        result = self.check_result(result)

        print('Delete platform config', ic_id, 'without auth.')
        result = s.delete(f'{settings.REST_API}/platform_configs/{pc_id}/')
        result = self.check_noauth(result)

        print('Delete platform config', ic_id, '.')
        result = s.delete(f'{settings.REST_API}/platform_configs/{pc_id}/', headers=headers)
        result = self.check_result(result)
