"""
This test executes a sample data import using the import.py script and a set of import
CSV files. This tests compatibility of the import script itself with the database and models
(the import script uses Django models internally), and import CSV structure with the import script.

There are two related configurations in main.settings.test:
* The import script root is at IMPORT_PATH.
* The CSV files are in IMPORT_DATA.
"""

import json
from django.test import TransactionTestCase

from .test_utils import *   # This import must occur before the model imports.

from msea.models.shared import *
from msea.models.pa import *
from msea.models.rov import *
from msea.util.util import *

import importer


class TestImportCase(TransactionTestCase):

    def setUp(self):
        pass

    def test_import(self):
        """
        Test the import system. Runs the import once to make sure that the overwrite
        parameter operates correctly. The subsequent model-retrieval test will make sure
        that for an entity, only one instance is returned.
        """
        if not settings.SKIP_IMPORT_1:    # This check just enables (de)activation of this time-consuming step during testing.
            imp1 = importer.importer()
            imp1.download = False
            imp1.run(f'{settings.IMPORT_DATA_DIR}/Test2022-001/import.csv', 'test', True, True, quit_on_error=False)

        if not settings.SKIP_IMPORT_2:
            # Re-run the import to test whether the process duplicates entities or correctly
            # deletes first, then imports.
            imp2 = importer.importer()
            imp2.download = False
            imp2.run(f'{settings.IMPORT_DATA_DIR}/Test2022-001/import.csv', 'test', True, True, quit_on_error=False)

        # Check the import project.
        projects = Project.objects.filter(name='Test2022-001')
        if projects.count() != 1:
            raise Exception(f'There should be only one project: {projects.count()}.')

        project = projects.first()
        if not project.name:
            raise Exception('A project name must be set.')
        if not project.note:
            raise Exception('A project note must be set.')
        if not project.objective:
            raise Exception('A project objective must be set.')
        if not project.summary:
            raise Exception('A project summary must be set.')

        # Check the project importer.
        pms = ProjectMember.objects.filter(project=project)
        if pms.count() != 1:
            raise Exception(f'The project must have at one member: {pms.count()}.')

        pm = pms.first()
        if not pm.role or pm.role.name != 'Importer':
            raise Exception('The program member must be an importer.')
        if not pm.person or not pm.person.first_name or not pm.person.last_name:
            raise Exception(
                f'The program member is not set or not correct: {pm.person.first_name} {pm.person.last_name}.')

        # Check the cruises.
        cruises = Cruise.objects.filter(name='Test2022-001')
        if cruises.count() != 2:
            raise Exception(f'There should be two cruises. {cruises.count()} found.')

        for cruise in cruises:

            # Check for the chief scientists.
            cms = CruiseCrew.objects.filter(cruise=cruise)
            if cms.count() not in (1, 2):
                raise Exception(f'There must be one or two cruise members: {cms.count()}.')
            for cm in cms:
                if not cm.cruise_role:
                    raise Exception('The cruise member must have a role assignment.')
                if not cm.person:
                    raise Exception('The cruise member must have a person assignment.')
                if cm.cruise_role != CruiseRole.objects.get(name='Chief Scientist'):
                    raise Exception(f'The cruise member role must be "Chief Scientist": {cm.cruise_role.name}.')

            if not cruise.name:
                raise Exception('Cruise name not set.')
            if not cruise.leg:
                raise Exception('Cruise leg not set.')
            if not cruise.objective:
                raise Exception('Cruise objective not set.')
            if not cruise.summary:
                raise Exception('Cruise summary not set.')
            if not cruise.note:
                raise Exception('Cruise note not set.')
            if not cruise.start_time or not cruise.end_time or cruise.start_time >= cruise.end_time:
                raise Exception('Cruise start and end times are required, and must be in order.')

            # Check the cruise program.
            cps = CruiseProgram.objects.filter(cruise=cruise)
            if cps.count() != 1:
                raise Exception(f'The cruise must be associated with one program: {cps.count()}.')

            cp = cps.first()
            program = cp.program
            if not program:
                raise Exception('A program must be associated with the cruise.')
            if not program.name:
                raise Exception('A program name must be set.')
            if not program.summary:
                raise Exception('A program summary must be set.')
            if not program.objective:
                raise Exception('A program objective must be set.')
            if not program.start_date or not program.end_date or program.start_date >= program.end_date:
                raise Exception('Program start and end dates are required, and must be in order.')

            # Check the dives.
            dives = Dive.objects.filter(cruise=cruise)
            if dives.count() != 2:
                raise Exception(f'There should be 2 dives, but there are {dives.count()}.')

            counts = {}
            for dive in dives:

                # Check the dive crew. There should be one or two pilots and one or two loggers.
                pcs = DiveCrew.objects.filter(dive=dive, dive_role=DiveRole.objects.get(name='Pilot'))
                if pcs.count() not in (1, 2):
                    raise Exception(f'Dive {dive.name} must have one or two pilots: {pcs.count()}.')
                for pc in pcs:
                    if not pc.dive_role:
                        raise Exception('The pilot must have an assigned role.')
                    if not pc.person:
                        raise Exception('The pilot must have an assigned person.')

                lcs = DiveCrew.objects.filter(dive=dive, dive_role=DiveRole.objects.get(name='Logger'))
                if lcs.count() not in (1, 2):
                    raise Exception(f'Dive {dive.name} must have one or two loggers: {lcs.count()}.')
                for lc in lcs:
                    if not lc.dive_role:
                        raise Exception('The logger must have an assigned role.')
                    if not lc.person:
                        raise Exception('The logger must have an assigned person.')

                if not dive.name:
                    raise Exception('Dive name not set.')
                if not dive.objective:
                    raise Exception('Dive objective not set.')
                if not dive.summary:
                    raise Exception('Dive summary not set.')
                if not dive.note:
                    raise Exception('Dive note not set.')
                if not dive.site:
                    raise Exception('Dive site not set.')
                if not dive.start_time or not dive.end_time or dive.start_time >= dive.end_time:
                    raise Exception('A start and end time must be set for every dive, and must be in order.')

                # Check the dive config.
                dc = dive.dive_config
                if not dc.ship_config or not dc.ship_config.platform or dc.ship_config.platform.name != 'Vector':
                    raise Exception('The dive config should reference the ship Vector.')
                if not dc.sub_config or not dc.sub_config.platform or dc.sub_config.platform.name != 'Phantom 428':
                    raise Exception('The dive config should reference the ship Phantom 428.')

                # Check the ROV instruments.
                pcics = PlatformConfigInstrumentConfig.objects.filter(platform_config=dc.sub_config)
                if pcics.count() != 9:
                    raise Exception(f'There should be 8 instrument configs associated with the ROV config: {pcics.count()}.')

                # Co through the list of instrument configs and collect the list to compare with what *should* be.
                insts = []
                for pcic in pcics:
                    ic = pcic.instrument_config
                    if not ic:
                        raise Exception('The platform config instrument config must be linked to an instrument config.')
                    if not ic.note:
                        raise Exception('The instrument config must have a note.')

                    icis = InstrumentConfigInstrument.objects.filter(instrument_config=ic)
                    for ici in icis:
                        if not ici.configuration:
                            raise Exception('The instrument config instrument must have a configuration.')
                        if not ici.instrument:
                            raise Exception('''The instrument config instrument must have an 
                            instrument associated with it.''')
                        insts.append(ici.instrument.short_code)

                if len(insts) != 10:
                    raise Exception('There must be 10 instruments on the ROV.')

                if insts.sort() != ['rbr_concerto', 'generic_transpo', 'generic_grn_lasr', 'rowetech_dvl',
                                    'tritech_pa500', 'generic_mag', 'rogue_cam', 'minizeus', 'minizeus',
                                    'gopro_hero4'].sort():
                    raise Exception('The instrument list on the ROV is incorrect.')

                # Check the ship instruments in the same fashion as the ROV instruments.
                pcics = PlatformConfigInstrumentConfig.objects.filter(platform_config=dc.ship_config)
                if pcics.count() != 2:
                    raise Exception(f'There should be 2 instrument configs associated with the ROV config: {pcics.count()}.')

                insts = []
                for pcic in pcics:
                    ic = pcic.instrument_config
                    if not ic:
                        raise Exception('The ship config instrument config must be linked to an instrument config.')
                    if not ic.note:
                        raise Exception('The instrument config must have a note.')

                    icis = InstrumentConfigInstrument.objects.filter(instrument_config=ic)
                    for ici in icis:
                        if not ici.configuration:
                            raise Exception('The instrument config instrument must have a configuration.')
                        if not ici.instrument:
                            raise Exception('''The instrument config instrument must have an 
                            instrument associated with it.''')
                        insts.append(ici.instrument.short_code)

                if len(insts) != 2:
                    raise Exception('There must be 2 instruments on the ship.')

                if insts.sort() != ['hemi_gps', 'hemi_gps'].sort():
                    raise Exception('The instrument list on the ship is incorrect.')

                # Check the transects.
                transects = Transect.objects.filter(dive=dive)
                if transects.count() == 0:
                    raise Exception(f'There should be a transect for dive {dive.name}.')

                for transect in transects:
                    if not transect.name:
                        raise Exception('Transect name not set.')
                    if not transect.objective:
                        raise Exception('Transect objective not set.')
                    if not transect.summary:
                        raise Exception('Transect summary not set.')
                    if not transect.note:
                        raise Exception('Transect note not set.')
                    if not transect.start_time or not transect.end_time or transect.start_time >= transect.end_time:
                        raise Exception('A start and end time must be set for every transect, and must be in order.')

                if not counts.get('measurements'):
                    counts['measurements'] = {}
                m = Measurement.objects.filter(dive=dive)
                counts['measurements'][dive.name] = m.count()

                if not counts.get('positions'):
                    counts['positions'] = {}
                p = Position.objects.filter(dive=dive)
                counts['positions'][dive.name] = p.count()

                if not counts.get('status'):
                    counts['status'] = {}
                s = StatusEvent.objects.filter(dive=dive)
                counts['status'][dive.name] = s.count()

                if not counts.get('comments'):
                    counts['comments'] = {}
                c = CommentEvent.objects.filter(dive=dive)
                counts['comments'][dive.name] = c.count()

                if not counts.get('observations'):
                    counts['observations'] = {}
                o = ObservationEvent.objects.filter(dive=dive)
                counts['observations'][dive.name] = o.count()

                if not counts.get('habitats'):
                    counts['habitats'] = {}
                h = HabitatEvent.objects.filter(dive=dive)
                counts['habitats'][dive.name] = h.count()

            if sum(list(counts['measurements'].values())) == 0:
                raise Exception(f'No measurements found for dive {dive.name}.')
            if sum(list(counts['positions'].values())) == 0:
                raise Exception(f'No positions found for dive {dive.name}.')
            if sum(list(counts['status'].values())) == 0:
                raise Exception(f'No status events found for dive {dive.name}.')
            if sum(list(counts['comments'].values())) == 0:
                raise Exception(f'No comment events found for dive {dive.name}.')
            if sum(list(counts['observations'].values())) == 0:
                raise Exception(f'No observation events found for dive {dive.name}.')
            if sum(list(counts['habitats'].values())) == 0:
                raise Exception(f'No habitat events found for dive {dive.name}.')
