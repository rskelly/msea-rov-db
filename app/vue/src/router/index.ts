import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        // Experiments.
        {
            path: "/experiments",
            name: "experiments",
            component: () => import("../views/experiments/Experiments.vue"),
        },
        {
            path: "/experiments/box_plot",
            name: "experiments_box_plot",
            component: () => import("../views/experiments/BoxPlot.vue"),
        },
        {
            path: "/experiments/taxon_search",
            name: "experiments_taxon_search",
            component: () => import("../views/experiments/TaxonSearchView.vue"),
        },


        // ROV Base.
        {
            path: "/",
            alias: "/",
            name: "rov_home",
            component: () => import("../views/rov/HomeView.vue"),
        },
        {
            path: "/cruises",
            name: "rov_cruises",
            component: () => import("../views/rov/CruisesView.vue"),
        },
        {
            path: "/cruises/:cruise_name",
            name: "rov_cruises_named",
            component: () => import("../views/rov/CruisesView.vue"),
        },
        {
            path: "/text_search",
            name: "rov_text_search",
            component: () => import("../views/rov/TextSearchView.vue"),
        },
        {
            path: "/observations/:diveId",
            name: "rov_observations",
            component: () => import("../views/rov/ObservationsView.vue"),
        },

        // Import.
        {
            path: "/annotation_job/:annotationJobId",
            name: "rov_annotation_jobs",
            component: () => import("../views/rov/AnnotationJobView.vue"),
        },
        {
            path: "/annotation_protocol/:annotationProtocolId",
            name: "rov_annotation_protocols",
            component: () => import("../views/rov/AnnotationProtocolView.vue"),
        },

        // Administration.
        {
            path: "/administration",
            name: "rov_admin",
            component: () => import("../views/rov/AdminView.vue"),
        },
        {
            path: "/administration/rov/annotation_protocols",
            name: "rov_admin_annotation_protocols",
            component: () => import("../views/rov/admin/AnnotationProtocol.vue"),
        },
        {
            path: "/administration/rov/annotation_jobs",
            name: "rov_admin_annotation_jobs",
            component: () => import("../views/rov/admin/AnnotationJob.vue"),
        },
        {
            path: "/administration/rov/annotation_software",
            name: "rov_admin_annotation_software",
            component: () => import("../views/rov/admin/AnnotationSoftware.vue"),
        },
        {
            path: "/administration/rov/cruises",
            name: "rov_admin_cruises",
            component: () => import("../views/rov/admin/Cruise.vue"),
        },
        {
            path: "/administration/rov/cruise_roles",
            name: "rov_admin_cruise_roles",
            component: () => import("../views/rov/admin/CruiseRole.vue"),
        },
        {
            path: "/administration/rov/dives",
            name: "rov_admin_dives",
            component: () => import("../views/rov/admin/Dive.vue"),
        },
        {
            path: "/administration/rov/dive_roles",
            name: "rov_admin_dive_roles",
            component: () => import("../views/rov/admin/DiveRole.vue"),
        },
        {
            path: "/administration/rov/transects",
            name: "rov_admin_transects",
            component: () => import("../views/rov/admin/Transect.vue"),
        },
        {
            path: "/administration/shared/people",
            name: "shared_admin_people",
            component: () => import("../views/shared/admin/Person.vue"),
        },
        {
            path: "/administration/shared/organisations",
            name: "shared_admin_organisations",
            component: () => import("../views/shared/admin/Organisation.vue"),
        },
        {
            path: "/shared/admin/site",
            name: "shared_admin_site",
            component: () => import("../views/shared/admin/Site.vue"),
        },
        {
            path: "/administration/rov/cruise_import",
            name: "rov_admin_cruise_import",
            component: () => import("../views/rov/import/CruiseImport.vue"),
        },
        {
            path: "/administration/rov/biigle_label_mapping",
            name: "rov_admin_biigle_label_mapping",
            component: () => import("../views/rov/import/BiigleLabelMapping.vue"),
        },
        {
            path: "/administration/rov/generic_label_mapping",
            name: "rov_admin_generic_label_mapping",
            component: () => import("../views/rov/import/GenericLabelMapping.vue"),
        },
        {
            path: "/administration/rov/status_types",
            name: "rov_admin_status_types",
            component: () => import("../views/rov/admin/StatusType.vue"),
        },
        {
            path: "/administration/rov/substrate",
            name: "rov_admin_substrates",
            component: () => import("../views/rov/admin/Substrate.vue"),
        },
        {
            path: "/administration/rov/abundance",
            name: "rov_admin_abundance",
            component: () => import("../views/rov/admin/Abundance.vue"),
        },
        {
            path: "/administration/rov/hardware",
            name: "rov_admin_hardware",
            component: () => import("../views/rov/admin/Hardware.vue"),
        },
        {
            path: "/administration/rov/organisations",
            name: "rov_admin_organisations",
            component: () => import("../views/rov/admin/Organisation.vue"),
        },
        {
            path: "/administration/rov/equipment_types",
            name: "rov_admin_equipment_types",
            component: () => import("../views/rov/admin/EquipmentType.vue"),
        },
        {
            path: "/administration/rov/measurement_types",
            name: "rov_admin_measurement_types",
            component: () => import("../views/rov/admin/MeasurementType.vue"),
        },
        {
            path: "/administration/rov/position_types",
            name: "rov_admin_position_types",
            component: () => import("../views/rov/admin/PositionType.vue"),
        },
        {
            path: "/administration/rov/biocover",
            name: "rov_admin_biocover",
            component: () => import("../views/rov/admin/Biocover.vue"),
        },
        {
            path: "/administration/rov/complexity",
            name: "rov_admin_complexity",
            component: () => import("../views/rov/admin/Complexity.vue"),
        },
        {
            path: "/administration/rov/flow",
            name: "rov_admin_flow",
            component: () => import("../views/rov/admin/Flow.vue"),
        },
        {
            path: "/administration/rov/confidence",
            name: "rov_admin_confidence",
            component: () => import("../views/rov/admin/Confidence.vue"),
        },
        {
            path: "/administration/rov/coverage",
            name: "rov_admin_coverage",
            component: () => import("../views/rov/admin/Coverage.vue"),
        },
        {
            path: "/administration/rov/disturbance",
            name: "rov_admin_disturbance",
            component: () => import("../views/rov/admin/Disturbance.vue"),
        },
        {
            path: "/administration/rov/image_quality",
            name: "rov_admin_image_quality",
            component: () => import("../views/rov/admin/ImageQuality.vue"),
        },
        {
            path: "/administration/rov/medium_formats",
            name: "rov_admin_medium_formats",
            component: () => import("../views/rov/admin/MediumFormat.vue"),
        },
        {
            path: "/administration/rov/medium_types",
            name: "rov_admin_medium_types",
            component: () => import("../views/rov/admin/MediumType.vue"),
        },
        {
            path: "/administration/rov/restrictions",
            name: "rov_admin_restrictions",
            component: () => import("../views/rov/admin/Restriction.vue"),
        },
        {
            path: "/administration/rov/protocol",
            name: "rov_admin_protocol",
            component: () => import("../views/rov/admin/Protocol.vue"),
        },
        {
            path: "/administration/rov/relief",
            name: "rov_admin_relief",
            component: () => import("../views/rov/admin/Relief.vue"),
        },
        {
            path: "/administration/rov/survey_mode",
            name: "rov_admin_survey_modes",
            component: () => import("../views/rov/admin/SurveyMode.vue"),
        },
        {
            path: "/administration/rov/thickness",
            name: "rov_admin_thickness",
            component: () => import("../views/rov/admin/Thickness.vue"),
        },
        {
            path: "/administration/rov/program",
            name: "rov_admin_program",
            component: () => import("../views/rov/admin/Program.vue"),
        },
        {
            path: "/administration/rov/users",
            name: "rov_admin_users",
            component: () => import("../views/rov/admin/Users.vue"),
        },
        {
            path: "/administration/rov/program_role",
            name: "rov_admin_program_role",
            component: () => import("../views/rov/admin/ProgramRole.vue"),
        },

        // Authentication.
        {
            path: "/login",
            name: "login",
            component: () => import("../views/auth/LoginView.vue"),
        },
        {
            path: "/logout",
            name: "logout",
            component: () => import("../views/auth/LogoutView.vue"),
        },
        {
            path: "/profile",
            name: "profile",
            component: () => import("../views/auth/ProfileView.vue"),
        },
        {
            path: "/reset",
            name: "reset",
            component: () => import("../views/auth/ResetView.vue"),
        },
        {
            path: "/register",
            name: "register",
            component: () => import("../views/auth/RegisterView.vue"),
        },
        {
            path: "/verify",
            name: "verify",
            component: () => import("../views/auth/VerifyView.vue"),
        },

        // Site Information.
        {
            path: "/about",
            name: "about",
            component: () => import("../views/AboutView.vue"),
        },

        // Contact.
        {
            path: "/contact",
            name: "contact",
            component: () => import("../views/ContactView.vue"),
        },
    ],
});

export default router;
