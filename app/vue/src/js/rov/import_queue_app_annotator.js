import { Net } from '@/js/util.js';
import { AnnotationProtocol, AnnotationSoftware, ImageQuality, ImportQueueAnnotatorJob, MediumType, 
    ObservationConfidence, SurveyMode, Protocol, StatusType,
    StatusTypeDetail} from "@/js/rov/models.js";
import { AnnotationProject, LabelTree, Label } from '@/js/rov/labels.js';
import { Nav } from '@/js/rov/import_queue_nav.js';
import Fuse from 'fuse.js'

/**
 * A method to pre-process the update object of an ImportQueueAnnotatorJob
 * and reconstitute the data object properties.
 * @param {*} job 
 */
const resolveJob = (job) => {
    // Create the annotation job.
    job.data.project = new AnnotationProject(job.data.project);
    // Create the LabelTrees. May have already been created in the job, so check first.
    job.data.labelTrees = (job.data.labelTrees || []).map(tree => new LabelTree(tree));
    // Find or create the annotaton protocol.
    job.data.annotationProtocol = job.data.annotationProtocol 
        && job.data.annotationProtocol.id 
        ? Model.find(job.data.annotationProtocol.id, AnnotationProtocol)
        : new AnnotationProtocol(job.data.annotationProtocol);
    return job;
};

/**
 * Sort people by last, first names.
 * @param {*} a 
 * @param {*} b 
 */
const peopleComp = (a, b) => {
    a = a.last_name.toLowerCase() + a.first_name.toLowerCase();
    b = b.last_name.toLowerCase() + b.first_name.toLowerCase();
    if(a < b) {
        return -1;
    } else if(b < a) {
        return 1;
    } else {
        return 0;
    }
};

/**
 The annotator import configuration app. Provides a stepwise method of configuring
 the project, labels, restrictions and annotation protocols for import into the database.
 */
export class ImportQueueAnnotatorApp {

    constructor() {
        // Don't init here. We want reactivity.
    }

    init() {
        this.inited = false;
        this.labelTreesLoaded = null;

        // Set the preUpdate for the annotator job.
        ImportQueueAnnotatorJob.preUpdate = resolveJob;

        // A list of classes used for lookups. Used by the lookup lists below.
        this.lookupClasses = {
            reliefs: Relief, abundances: Abundance, coverages: Coverage, thicknesses: Thickness, biocovers: Biocover,
            substrates: Substrate, complexities: Complexity, image_qualities: ImageQuality, restrictions: Restriction,
            annotation_protocols: AnnotationProtocol, annotation_software: AnnotationSoftware, medium_types: MediumType,
            disturbances: Disturbance, observation_confidences: ObservationConfidence, flows: Flow,
            survey_modes: SurveyMode, survey_protocols: Protocol, status_types: StatusType,  status_details: StatusTypeDetail,
            off_transect_details: {loadAll: () => StatusTypeDetail.loadForStatusType(6) },
        };

        // When a tree is sorted, remember the field it's sorted on. Need to use this
        // because when a tree is saved or refreshed, its sort state is lost.
        this.treeSortFields = {};

        // Used to track the loading status of a lookup. When the lookup is loading or loaded, set to true,
        // otherwise leave null or false (e.g., on error).
        this.lookupsLoading = {};

        // The application's state.
        this.job = null;

        // Sign in view.
        this.jobs = [];                       // A list of previous jobs.
        this.jobsLoading = true;              // Set to true when jobs are loading.

        // Select project view.
        this.biigleProjects = [];

        // Label map view.
        this.autoSaving = false;              // True when the autosave is happening.
        this.labelTrees = [];               // A list of all label trees for all projects.

        // Personnel view.
        this.people = {};                      // A dictionary of people by ID.
        this.peopleList = [];                  // A sorted list of people.

        // Complete view.
        this.saving = false;                  // Set to true when the queue item is being saved.

        // Results from loaded lookup tables. 
        // The keys are keys to the service endpoints object. If there's no
        // corresponding entry in the endpoints object, the name is the endpoint.
        this.labelMappingLookups = {
            reliefs: [],
            abundances: [],
            coverages: [],
            thicknesses: [],
            biocovers: [],
            substrates: [],
            complexities: [],
            flows: [],
            disturbances: [],
            image_qualities: [],
            restrictions: [],
            observation_confidences: [],
            off_transect_details: [],
            survey_modes: [],
            survey_protocols: [],
            status_types: [],
            status_details: [],
        }

        this.annotationProtocolLookups = {
            annotation_protocols: [],      // A list of the existing protocols, loaded from the server.
            medium_types: [],              // A list of the medium types, loaded from the server.
            annotation_software: [],       // A list of the annotation software, loaded from the server.
        };

        // Fuzzy search function for personnel. Will be assigned a Fuse instance.
        this.fusePersonnel = null;

        // Build the navigation data object (an instance of Nav), which supplies the ImportSteps component with state.
        //  id: The section element ID;
        //  name: The button label;
        //  init: a function to call on this object to initialize the section's state;
        //  enabled: checks the app's state to verify that the section is available (NavItem will check prior states).
        this.nav = new Nav([
            { id: "view_start", name: "Start", init: null, enabled: () => true },
            { id: "view_profile", name: "Profile", init: this.initProfile.bind(this), enabled: () => true, },
            { id: "view_select_project", name: "Select Project", init: this.initSelectProject.bind(this),
                enabled: () => { 
                    return this.job;
                } 
            },
            { id: "view_label_mapping", name: "Label Mapping", init: this.initLabelMapping.bind(this),
                enabled: () => { 
                    return this.job.data.project;
                } 
            },
            { id: "view_personnel", name: "Personnel", init: this.initPersonnel.bind(this),
                enabled: () => { 
                    return this.job.data.labelTrees.length > 0;
                } 
            },
            { id: "view_annotation_protocol", name: "Annotation Protocol",
                init: this.initAnnotationProtocol.bind(this)
            },
            { id: "view_complete", name: "Complete", init: this.initComplete.bind(this),
                enabled: () => { 
                    return this.job.data.annotationProtocol;
                } 
            }
        ]);

        this.inited = true;
    }

    /**
     * Reset the application to a fresh state before any entities have
     * been selected or configured.
     */
    resetState() {
        this.job = null;
    }

    /**
     * Init function for the sign in view.
     * Attempts to load the logged-in MSEA user.
     */
    initProfile() {
        console.debug('Init Sign-In.');
        this.job = null;
        // Load the lookups.
        // TODO: Use promise all.
        console.debug('Loading lookups...');
        this.loadLookups(this.labelMappingLookups, this.labelMappingLookups)
            .then(() => {
                // Load the lookups from the service.
                this.loadLookups(this.annotationProtocolLookups, this.annotationProtocolLookups)
                    .then(() => {
                        console.debug('...lookups loaded.');
                        console.debug('Loading jobs...');
                        this.loadJobs();
                    })
                    .catch(alert);
            })
            .catch(alert);
        // Load the personnel table from the DB.
        Person.loadAll()
            .then(result => {
                this.peopleList.push(...result);
                this.peopleList.sort(peopleComp);
                result.forEach(item => this.people[item.id] = item);
                this.fusePersonnel = new Fuse(Object.values(this.people), {keys: ['first_name', 'last_name']});
            })
            .catch(alert);
    }

    newJob() {
        this.job = new ImportQueueAnnotatorJob();
    }

    /**
     * Init the select project page.
     * Loads projects from Biigle.
     */
    initSelectProject() {
        console.debug('Init Select Project.');
        this.biigleProjects.splice(0);
        AnnotationProject.loadBiigleProjects()
            .then(result => {
                this.biigleProjects.splice(0);
                this.biigleProjects.push(...result)
            })
            .catch(alert);
    }

    /**
     * Init the label mapping view.
     * Loads lookups from the database.
     * Loads label trees from Biigle (via proxy).
     */
    initLabelMapping() {
        console.debug('Init Label Mapping.');
        if(!this.labelTrees.length) {
            // Stores the loaded label trees indexed by ID.
            const treeMap = new Map();
            // Load the Biigle projects.
            Net.get(`${Net.API}/biigle_api/projects/`)
                .then(projects => {
                    projects.forEach(project => {
                        // Get the label trees for each project.
                        Net.get(`${Net.API}/biigle_api/projects/${project.id}/label-trees/`)
                            .then(trees => {
                                // Put each tree in the label tree map by its ID.
                                trees.forEach(tree => treeMap.set(tree.id, tree));
                                // Extract a unique list of the label trees.
                                this.labelTrees.splice(0);
                                this.labelTrees.push(...Array.from(treeMap.values()));
                                // Let the view know that the label trees are loaded.
                                if(this.labelTreesLoaded)
                                    this.labelTreesLoaded(this.labelTrees);
                        })
                            .catch(alert);
                    });
                })
                .catch(alert);
        }
    }

    /**
     * Refresh a Biigle label tree in place. Updates both the project tree,
     * and the source tree.
     * @param {*} id 
     */
    async refreshLabelTree(id) {
        const rtree = await Net.get(`${Net.API}/biigle_api/label-trees/${id}/`);
        // Refresh the project tree.
        const ptree = this.job.data.project.labelTrees.find((t) => t.id == id);
        const labelMap = new Map();
        ptree.labels.forEach(label => labelMap.set(label.id, label));
        rtree.labels.forEach(label => {
            if(!labelMap.has(label.id)) {
                ptree.labels.push(new Label(label, ptree));
            }
        });
        this.sortTree(ptree);
        // Refresh the main tree.
        const ltree = this.job.data.labelTrees.find(t => t.id == id)
        labelMap.clear();
        ltree.labels.forEach(label => labelMap.set(label.id, label));
        rtree.labels.forEach(label => {
            if(!labelMap.has(label.id)) {
                ltree.labels.push(new Label(label, ltree));
            }
        });
        this.sortTree(ltree);
    }

    /**
     * Search the loaded label trees for a label with the given ID and return the tree.
     * @param {*} id 
     * @returns 
     */
    searchLabelID(id) {
        const trees = this.labelTrees.concat(this.job.data.project.labelTrees);
        let result = null;
        trees.forEach(tree => {
            tree.labels.forEach(label => {
                if(label.id == id)
                    result = tree;
            });
        });
        return result;
    }

    /**
     * Init the personnel view by loading the personnel roster.
     */
    initPersonnel() {
        console.debug('Init personnel.');
        // Resolve the members.
        this.job.data.project.resolveMembers();
        // Load the project members.
        this.job.data.project.loadMembers()
            .then(() => this.matchMembers())
            .catch(alert);
    }
   
    /**
     * Init the annotation protocol section.
     */
    initAnnotationProtocol() {
    }

    /**
     * Initialize the complete stage.
     */
    initComplete() {
        if(!this.job)
            this.job = new ImportQueueAnnotatorJob();
    }

    /**
     * Load the lookups using the lookups dictionary, whose keys give the names of the services to load.
     * Results are attached to the target dictionary using the same keys.
     * @param {*} lookups 
     * @param {*} target 
     * @returns 
     */
    loadLookups(lookups, target) {
        return new Promise((resolve, reject) => {
            // Load the lookups from the service.
            const load = (names) => {
                if(!names.length) {
                    resolve(lookups);
                } else {
                    const name = names.pop(0);
                    // If the lookup is not already loaded, load it.
                    if(!this.lookupsLoading[name]) {
                        this.lookupsLoading[name] = true;
                        // Load the lookup using the class' loadAll method, or the created loadAll method.
                        this.lookupClasses[name].loadAll()
                            .then(result => {
                                // Add to the targets list.
                                target[name].push(...result);
                                // Load the next item.
                                load(names);
                            })
                            .catch(err => {
                                alert(err);
                                this.lookupsLoading[name] = false;
                            });
                    } else {
                        // If the lookups are alread loaded, skip it and load the next.
                        load(names);
                    }
                }
            };
            load(Object.keys(lookups));
        });
    }

    loadJobs() {
        console.debug('Load previous jobs...');
        this.jobsLoading = true;
        this.jobs.splice(0);
        ImportQueueAnnotatorJob.loadAll()
            .then(result => {
                this.jobs.splice(0);
                this.jobs.push(...result);
                this.jobs.forEach(job => {
                    if(this.labelTreesLoaded)
                        this.labelTreesLoaded(job.data.project.labelTrees);
                });
                this.jobsLoading = false;
                console.debug('...jobs loaded.');
            })
            .catch(err => {
                alert(err);
                this.jobsLoading = false;
            });
    }

    /**
     * Loads the prefill tags and tag data for each label.
     * If a previously-saved job is selected, add the saved tags and data.
     * If it's a new job (job is null) load from the prefills table.
     */
    prepopulateLabels(tree) {
        return new Promise((resolve, reject) => {
            // Get a unique list of names from the selected label trees.
            const names = Array.from(new Set(tree.labels.map(label => label.name)));
            // Load labels for the names list.
            Net.post(`${Net.API}/import_queue_annotator_jobs/label_prefills/`, {names: names, tree_name: tree.name})
                .then(result => {
                    // Resolve the tag data entities. 
                    tree.labels.forEach(label => {
                        if(result[label.name])
                            label.restorePrefills(result[label.name]);
                    });
                    this.sortTree(tree);
                    resolve(tree);
                })
                .catch(reject);
        });
    }

    /**
     * sort the tree's labels using the given field. Choices are 'id' and 'label'.
     */
    sortTree(tree, field) {
        if(!field && !(field = this.treeSortFields[tree.id]))
            return;
        this.treeSortFields[tree.id] = field;
        tree.labels.sort((a, b) => {
            if(field == 'id') {
                return parseInt(a.id) - parseInt(b.id);
            } else if(field == 'hierarchy') {
                let aname = a.hierarchy.toUpperCase();
                let bname = b.hierarchy.toUpperCase();
                if(aname < bname) {
                    return -1;
                } else if(bname < aname) {
                    return 1;
                }
            } else if(field == 'name') {
                let aname = a.name.toUpperCase();
                let bname = b.name.toUpperCase();
                if(aname < bname) {
                    return -1;
                } else if(bname < aname) {
                    return 1;
                }
            }
            return 0;
        });
    };
    
    /**
     * Add a person to the personnel list. This triggers a re-build of the index
     * and a match of un-matched members.
     * @param {*} person 
     */
    addPerson(person) {
        if(!this.people)
            this.people = {};
        this.people[person.id] = person;
        this.peopleList.push(person);
        this.peopleList.sort(peopleComp);
        this.fusePersonnel.add(person);
        this.matchMembers();
    }

    /**
     * Add a label tree to the list of selected trees.
     * Triggered by a button click.
     */
    addTree(tree) {
        // Add the tree.
        if(!(tree instanceof LabelTree))
            tree = new LabelTree(tree);
        // Load the tree labels. Load matches only if a job is not selected.
        this.prepopulateLabels(tree)
            .then(() => {
                this.job.data.project.labelTrees.push(tree);
            })
            .catch(alert);
    }

    /**
     * Update the members with a best guess at the personnel record that fits.
     * Only update items that have no assigned person.
     */
    matchMembers() {
        // Shortcut to the job's personnel map.
        Object.values(this.job.data.project.members).forEach(m => {
            if(!m.person) {
                let match = this.fusePersonnel.search(m.member.first_name + ' ' + m.member.last_name);
                if(match.length > 0)
                    m.person = match[0].item;
            }
        });
    }

    /**
     * Remove the given tree from the selected trees list. Triggered by a button click.
     */
    removeTree(tree) {
        this.job.data.labelTrees.splice(this.job.data.labelTrees.indexOf(tree), 1);
    }

    /**
     * Update the app with data from a previously-saved job. 
     * @param {*} job 
     */
    setJob(job) {
        this.job = job;
    }

    /**
     * Get the common name from the taxon object.
     */
    commonName(taxon) {
        const idx = taxon.names.indexOf('Common Name');
        if(idx > -1)
            return taxon.names[idx + 1];
        return null;
    }

    /**
     * Get the scientific name from the taxon object.
     */
    scientificName(taxon) {
        const idx = taxon.names.indexOf('Scientific Name');
        if(idx > -1)
            return taxon.names[idx + 1];
        return null;
    }

    /**
     * Add a newly-created restriction object to the lookup.
     * @param {*} restriction 
     */
    addRestriction(restriction) {
        this.labelMappingLookups.restrictions.push(restriction);
    }

    /**
     * Start the label map autosave timer. Calls the save method.
     */
    startAutosave() {
        this.stopAutosave();
        this.lmAutoSave = setInterval(this.save.bind(this), 60000);
    }
    
    /**
     * Stop the label map autosave timer.
     */
    stopAutosave() {
        clearInterval(this.lmAutoSave);
    }

    /**
     * Save current progress. If a job is active, saves that. If not,
     * saves the label tree for pre-fill on the next go-round.
     * Will raise the signin if login times out.
     */
    save() {
        this.autoSaving = true;
        this.stopAutosave();
        if(this.job) {
            if(this.job.id > 0) {
                // The job is already saved; resave it.
                this.job.save()
                    .then(() => {
                        this.autoSaving = false;
                        this.startAutosave();
                        this.job.data.labelTrees.forEach(tree => this.sortTree(tree));
                        this.job.data.project.labelTrees.forEach(tree => this.sortTree(tree));
                    })
                    .catch(err => {
                        this.autoSaving = false;
                        this.startAutosave();
                        alert(err);
                    });
            } else {
                // The job is not saved; save just the labels.
                Net.post(`${Net.API}/import_queue_annotator_jobs/autosave/`, {data: {labelTrees: this.job.data.labelTrees.map(tree => tree.toJSON())}})
                    .then(() => {
                        this.autoSaving = false;
                        this.startAutosave();
                        this.job.data.labelTrees.forEach(tree => this.sortTree(tree));
                        this.job.data.project.labelTrees.forEach(tree => this.sortTree(tree));
                    })
                    .catch(err => {
                        this.autoSaving = false;
                        this.startAutosave();
                        alert(err);
                    });
            }
        }
    }

    /**
     * Add the completed import package to the queue.
     * This will send:
     * 1) The annotation protocol ID.
     * 2) The personnel mapping.
     * 3) The label mapping.
     * 4) The selected project.
     * 5) The identity of the submitting user.
     */
    addToQueue() {
        this.saving = true;

        // Save the job. If the job has an ID (is being edited), set hasId to true to trigger the correct message.
        const hasId = this.job.id > 0;
        this.job.save()
            .then(result => {
                this.saving = false;
                alert(`The import job has been ${hasId ? 'saved' : 'created'} successfully.`);
                this.resetState();
                this.nav.goTo('view_profile');
            })
            .catch(alert);
    }

}