import { reactive } from 'vue';

/**
 * Contains state for a navigation item in the navigation bar.
 */
export class NavItem extends EventTarget {

    constructor(data) {
        super();
        this.state = reactive(Object.assign({
            next: null,
            previous: null
        }, data));
    }

    select() {
        this.dispatchEvent(new Event('selected'));
    }

    get next() {
        return this.state.next;
    }

    get previous() {
        return this.state.previous;
    }

    get init() {
        return this.state.init;
    }

    get enabled() {
        // Check previous first: if there isn't one or it's enabled, check current.
        // If there's no enabled function or it returns true, return true.
        return (!this.previous || this.previous.enabled) && (!this.state.enabled || this.state.enabled());
    }

}

/**
 * Represents the navigation bar. Manages the enabled state of items based
 * on the application state.
 */
export class Nav extends EventTarget {

    constructor(items) {
        super();
        // Create a linked list of NavItems.
        items = items.map(item => new NavItem(item));
        for(let i = 0; i < items.length; ++i) {
            items[i].addEventListener('selected', this.itemSelected.bind(this));
            if(i > 0)
                items[i].state.previous = items[i - 1];
            if(i < items.length - 1)
                items[i].state.next = items[i + 1];
        }
        this.state = reactive({
            items: items,                   // The list of nav items.
            first: items[0],                // The first nav item (immutable).
            current: items[0],              // The current nav item (mutable).
            last: items[items.length - 1]   // The last nav item (immutable).
        });
    }

    /**
     * Called when an item is selected.
     * @param {*} evt 
     */
    itemSelected(evt) {
        this.state.current = evt.target;
        this.dispatchEvent(new Event(evt.type, evt));
    }

    /**
     * Returns the ID of the current item or null if one isn't selected.
     */
    get currentId() {
        return this.state.current ? this.state.current.state.id : null;
    }

    /**
     * Returns true if the current nav items and all previous nav items are enabled.
     */
    get isEnabled() {
        return (this.state.previous && this.state.previous.enabled()) && this.state.enabled();
    }

    /**
     * Returns true if there is a next item.
     */
    get hasForward() {
        this.state.current = (this.state.current.next || this.state.current);
        this.state.current.select();
    }

    /**
     * Returns true if there is a previous item.
     */
    get hasBackward() {
        this.state.current = (this.state.current.previous || this.state.current);
        this.state.current.select();
    }

    /**
     * Find the item whose ID matches the hash. If the found item isn't 
     * enabled, roll back to find the last one that is. If nothing
     * is found, return null.} hash 
     * @returns 
     */
    fromHash(hash) {
        let item = this.state.first;
        while(item) {
            if(item.state.id == hash) {
                while(item && !item.state.enabled)
                    item = item.state.previous;
                return item;
            }
            item = item.next;
        }
        return null;
    }

    /**
     * Go to the item with the given name. If the item is not enabled,
     * goest to the last enabled item before it.
     * @param {*} name 
     */
    fromId(name) {
        let item = this.state.first;
        while(item) {
            if(item.state.id == name) {
                while(item && !item.state.enabled)
                    item = item.state.previous;
                return item;
            }
            item = item.next;
        }
        return null;
    }

    /**
     * Select the item with the given ID.
     * @param {*} name 
     */
    goTo(name) {
        this.state.current = this.fromId(name);
        this.state.current.select();
    }

    /**
     * Select the first page in the nav.
     */
    gotoFirst() {
        this.state.first.select();
    }
}