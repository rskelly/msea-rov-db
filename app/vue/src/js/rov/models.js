import { Net } from "@/js/util.js";
import { Model } from "@/js/models.js";
import { UploadedFile } from "@/js/shared/models";

/**
 * Adds methods for accessing the attributes accessor as a JSON string.
 */
const AttributesJsonMixin = (superclass) => class extends superclass {

    get attributes_json() {
        const a = this.attributes;
        return !a ? '' : JSON.stringify(a);
    }

    set attributes_json(v) {
        this.attributes = JSON.parse(v.length == 0 ? 'null' : v);
    }

}

export class Event extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Dive) {
            return `${Net.API}/dives/${inst.id}/events`;
        } else {
            throw new Error("Can't load all events without a dive filter.");
        }
    };
    static defaultProps = {
            id: 0,
            dive: null,
            instrument_config: null,
            annotation_job: null,
            start_time: null,
            end_time: null,
            properties: {},
            note: null,
            created_on: new Date(),
            updated_on: new Date()
        };
        static classMap ={};

        static loadAll(params) {
            return Model.loadAll(Event, params);
        }

}


/**
 * Maps Biigle labels to sets of attributes that are applied to event objects.
 */
export class BiigleLabelMap extends Model {

    static modelPath = `${Net.API}/biigle_label_map`;
    static defaultProps = {
        id: 0,
        label_tree_id: null,
        label_id: null,
        label_hierarchy: null,
        label_text: null,
        properties: {},
        restrictions: [],
        note: null,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {
        restrictions: 'Restriction'
    };

    constructor(props) {
        super(props);
        this._isLeaf = false;
    }
    
    get isLeaf() {
        return this._isLeaf;
    }

    set isLeaf(v) {
        this._isLeaf = v;
    }

    /**
     * Fields that will be copied when copyFrom is called.
     */
    static copyFields = [
        'event_type',
        'substrate_id', 'biocover_id', 'complexity_id', 'coverage_id', 'thickness_id', 'disturbance_id', 
        'relief_id', 'flow_id', 'confidence_id', 'abundance_id', 'taxon_id', 'status_type_id', 'status_type_detail_id',
        'note', 'scientific_name', 'common_name', 'otu', 'aphia_id', 'inaturalist_id', 'hart_code'
    ];

    set restrictionSet(restrictions) {
        this.restrictions.splice(0);
        (restrictions || []).forEach(item => {
            this.restrictions.push(new BiigleLabelMapRestriction({biigle_label_map: this.id, restriction: item}));
        });
    }

    get restrictionSet() {
        return this.restrictions.map(item => item.restriction);
    }

    get tags() {
        if(!('tags' in this.properties))
            this.properties.tags = [];
        return this.properties.tags;
    }

    set tags(tags) {
        this.properties.tags = tags;
    }

    hasTag(tag) {
        return tag.toLowerCase() in (this.tags || []).map(a => a.toLowerCase());
    }

    copyFrom(other) {
        if(other.restrictions) {
            this.restrictions.splice(0);
            this.restrictions.push(...other.restrictions);
        }
        Object.entries(other.properties).forEach(([k, v]) => {
            this.properties[k] = v;
        });
    }

    static createFromLabel(label) {
        const blm = new BiigleLabelMap();
        blm.label_tree_id = label.tree.id;
        blm.label_id = label.id;
        blm.label_text = label.name;
        blm.label_hierarchy = label.hierarchy;
        return blm;
    }

    /**
     * Load the label trees. The trees are loaded directly from Biigle, then the individual 
     * labels are loaded from the local biigle_label_map table to replace the generic label objects.
     * If the label is not present in the database, a new instance is created which can be saved.
     * @param {*} projectId 
     * @returns 
     */
    static loadTrees(projectId) {
        return new Promise((resolve, reject) => {
            Net.get(`${Net.API}/biigle_api/projects/${projectId}/label-trees/`)
                .then(result => {
                    const labels = new Map();       // Map to store all labels.
                    const parentIds = new Set();    // Stores parent IDs. A label with no references via parent ID is a leaf.
                    const trees = [];               // Return list of trees.
                    result.forEach(tree => {

                        tree.labels.forEach(label => {
                            // Store the labels in a map.
                            labels.set(label.id, label);
                            // If the label has a parent, record the parent ID.
                            if(label.parent_id)
                                parentIds.add(parseInt(label.parent_id));
                        });

                        tree.labels.forEach(label => {
                            // Assign the parent reference tot he label based on the parent ID.
                            if(label.parent_id)
                                label.parent = labels.get(label.parent_id);
                            // Store a reference to the tree.
                            label.tree = tree;

                            // Build the label hierarchy
                            let hierarchy = [label.name];
                            let p = label.parent;
                            while(p) {
                                hierarchy.push(p.name);
                                p = p.parent;
                            }
                            hierarchy.reverse();
                            label.hierarchy = hierarchy.join(' > ');
                        });

                        // Sort and save the tree.
                        tree.labels.sort((a, b) => a.id - b.id);
                        trees.push(tree);
                    });

                    // Load the existing label map instances or create new ones.
                    BiigleLabelMap.loadByIDs(Array.from(labels.keys()))
                        .then(biigleLabels => {
                    
                            // Update the labels in the map with the loaded instances.
                            biigleLabels.forEach(biigleLabel => {
                                if(labels.has(biigleLabel.label_id))
                                    labels.set(biigleLabel.label_id, biigleLabel);
                            });
                    
                            // For instances that were not updated, instantiate a BLM.
                            for(let id of Array.from(labels.keys())) {
                                if(!(labels.get(id) instanceof BiigleLabelMap))
                                    labels.set(id, BiigleLabelMap.createFromLabel(labels.get(id)));
                                if(!parentIds.has(id))
                                    labels.get(id).isLeaf = true;
                            }
                    
                            // Assign the BLM instances back to the tree labels.
                            trees.forEach(tree => {
                                const tLabels = [];
                                for(let i = 0; i < tree.labels.length; ++i) {
                                    // Only assign the labels that are in the labels map -- the leaves.
                                    if(labels.has(tree.labels[i].id))
                                        tLabels.push(labels.get(tree.labels[i].id));
                                }
                                // Sort the labels.
                                tLabels.sort((a, b) => {
                                    try {
                                        return a.label_text.localeCompare(b.label_text);
                                    } catch(err) {
                                        return 0;
                                    }
                                });
                                tree.labels = tLabels;
                            });

                            // Sort the trees by name.
                            trees.sort((a, b) => {
                                try {
                                    return a.name.localeCompare(b.name);
                                } catch(err) {
                                    return 0;
                                }
                            });
                            
                            // Return the trees.
                            resolve(trees);
                        })
                        .catch(alert);
                })
                .catch(reject);
        });
    }

    static loadByIDs(ids) {
        return Model.loadAll(BiigleLabelMap, {filter: {label_ids: ids.join(',')}})
    }

    static loadForTree(treeId) {
        return Model.loadAll(BiigleLabelMap, {filter:{treeId: treeId}});
    }

    static saveAll(labels) {
        return Net.post(`${BiigleLabelMap.modelPath}/`, labels);
    }

}


export class BiigleLabelMapRestriction extends Model {

    static modelPath = (params) =>{
        const inst = params.models.pop();
        if(inst && inst instanceof BiigleLabelMap) {
            return `${Net.API}/biigle_label_maps/${inst.id}/restrictions`
        } else {
            throw new Error("Can't load all events without a label map filter.");
        }
    };
    static defaultProps = {
        id: 0,
        restriction: null,
        biigle_label_map: null,
    };
    static classMap = {
        restriction: 'Restriction',
        biigle_label_map: 'BiigleLabelMap'
    };
    
}
Model.registerClass(BiigleLabelMapRestriction, 'BiigleLabelMapRestriction');


/**
 * Maps generic labels to sets of attributes that are applied to event objects.
 */
export class GenericLabelMap extends Model {

    static modelPath = `${Net.API}/generic_label_map`;
    static defaultProps = {
        id: 0,
        label_text: null,
        properties: {},
        restrictions: [],
        note: null,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {
        restrictions: 'Restriction'
    };

    /**
     * Fields that will be copied when copyFrom is called.
     */
    static copyFields = [
        'event_type',
        'substrate_id', 'biocover_id', 'complexity_id', 'coverage_id', 'thickness_id', 'disturbance_id', 
        'relief_id', 'flow_id', 'confidence_id', 'abundance_id', 'taxon_id', 'status_type_id', 'status_type_detail_id',
        'note', 'scientific_name', 'common_name', 'otu', 'aphia_id', 'inaturalist_id', 'hart_code'
    ];

    set restrictionSet(restrictions) {
        restrictions = restrictions.map(item => new GenericLabelMapRestriction({biigle_label_map: this.id, restriction: item}));
        modelUpdate(this.restrictions, restrictions, (a, b) => a.name.localeCompare(b.name));
    }

    get restrictionSet() {
        return this.restrictions.map(item => item.restriction);
    }

    get tags() {
        if(!('tags' in this.properties))
            this.properties.tags = [];
        return this.properties.tags;
    }

    set tags(tags) {
        this.properties.tags = tags;
    }

    hasTag(tag) {
        return tag.toLowerCase() in (this.tags || []).map(a => a.toLowerCase());
    }

    copyFrom(other) {
        if(other.restrictions) {
            this.restrictions.splice(0);
            this.restrictions.push(...other.restrictions);
        }
        Object.entries(other.properties).forEach(([k, v]) => {
            this.properties[k] = v;
        });
    }

    static saveAll(labels) {
        return Net.post(`${GenericLabelMap.modelPath}/`, labels);
    }

    static updateAll(labels) {
        const map = new Map();
        labels.forEach(label => {
            map.set(label.label_text, label);
        });
        Net.post(`${Net.API}/generic_label_map/update_all/`, Array.from(map.keys()))
            .then(result => {
                result.forEach(item => {
                    map.get(item.label_text).update(item);
                })
            })
            .catch(alert);
    }
}


export class GenericLabelMapRestriction extends Model {

    static modelPath = (params) =>{
        const inst = params.models.pop();
        if(inst && inst instanceof GenericLabelMap) {
            return `${Net.API}/biigle_label_maps/${inst.id}/restrictions`
        } else {
            throw new Error("Can't load all events without a label map filter.");
        }
    };
    static defaultProps = {
        id: 0,
        restriction: null,
        biigle_label_map: null,
    };
    static classMap = {
        restriction: 'Restriction',
        biigle_label_map: 'GenericLabelMap'
    };
    
}
Model.registerClass(GenericLabelMapRestriction, 'GenericLabelMapRestriction');


export class MediumType extends Model {

    static modelPath = `${Net.API}/medium_types`;
    static defaultProps = {
        id: 0,
        name: 'New Media Type',
        note: null
    };

    static loadAll() {
        return Model.loadAll(MediumType);
    }

}
Model.registerClass(MediumType, 'MediumType');

export class Medium extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Dive) {
            return `${Net.API}/dives/${inst.id}/media`;
        } else {
            return `${Net.API}/media`;
        }
    };
    static defaultProps = {
        id: 0,
        parent: null,
        dive: null,
        medium_format: null,
        instrument_config: null,
        file: null,
        geostamp_source_id: null,
        start_time: null,
        length: null,
        width: null,
        height: null,
        source_time: null,
        note: null,
        thumbnail: null,
        created_on: new Date()
    };
    static classMap = {
        parent: 'Medium',
        dive: 'Dive',
        medium_format: 'MediumFormat',
        instrument_config: 'InstrumentConfig',
        file: '_File'
    };

    static loadAll() {
        return Model.loadAll(Medium);
    }

}
Model.registerClass(Medium, 'Medium');


export class Instrument extends AttributesJsonMixin(Model) {

    static modelPath = function(params) {
        let inst = params.models.pop();
        if(inst && inst instanceof Dive)
            return `${Net.API}/dives/${inst.id}/instruments`;
        return `${Net.API}/instruments`;
    }
    static defaultProps = {
        id: 0,
        model: null,
        organisation: null,
        serial_number: null,
        retired: null,
        attributes: null,
        note: null,
        created_on: new Date()
    };
    static classMap = {
        model: '_Model',
        organisation: 'Organisation'
    };

    static loadAll() {
        return Model.loadAll(Instrument);
    }

}
Model.registerClass(Instrument, 'Instrument');


export class EquipmentType extends Model {

    static modelPath = function(params) {
        return `${Net.API}/equipment_types`;
    }
    static defaultProps = {
        id: 0,
        name: null,
        note: null,
        category: null,
        created_on: new Date()
    };
    static classMap = {
    };

    static loadAll(filter) {
        return Model.loadAll(EquipmentType, filter);
    }

}
Model.registerClass(EquipmentType, 'EquipmentType');


export class MediumFormat extends Model {

    static modelPath = `${Net.API}/medium_formats`;
    static defaultProps = {
        id: 0,
        name: 'New Medium Format',
        extensions: [],
        note: null,
        medium_type: null
    };
    static classMap = {
        medium_type: 'MediumType'
    };

    get extensions() {
        return this.get_prop('extensions').join(', ');
    }

    set extensions(v) {
        this.set_prop('extensions', v.split(',').map(e => e.trim()));
    }

    static loadAll() {
        return Model.loadAll(MediumFormat);
    }

}
Model.registerClass(MediumFormat, 'MediumFormat');


export class MeasurementType extends Model {

    static modelPath = `${Net.API}/measurement_types`;
    static defaultProps = {
        id: 0,
        name: 'New Measurement Type',
        unit: null,
        minimum: null,
        maximum: null,
        note: null
    };

    static loadAll() {
        return Model.loadAll(MeasurementType);
    }

}
Model.registerClass(MeasurementType, 'MeasurementType');


export class PositionType extends Model {

    static modelPath = `${Net.API}/position_types`;
    static defaultProps = {
        id: 0,
        name: 'New Position Type',
        unit: null,
        minimum: null,
        maximum: null,
        note: null
    };

    static loadAll() {
        return Model.loadAll(PositionType);
    }

}
Model.registerClass(PositionType, 'PositionType');


export class _Model extends AttributesJsonMixin(Model) {

    static modelPath = `${Net.API}/models`;
    static defaultProps = {
        id: 0,
        brand_name: 'New Brand',
        model_name: 'New Model',
        attributes: null,
        note: null,
        equipment_type: null,
    };
    static classMap = {
        equipment_type: 'EquipmentType'
    };

    static loadAll(filter) {
        return Model.loadAll(_Model, filter);
    }

}
Model.registerClass(_Model, '_Model');


export class Platform extends AttributesJsonMixin(Model) {

    static modelPath = `${Net.API}/platforms`;
    static defaultProps = {
        id: 0,
        model: null,
        organisation: null,
        name: 'New Platform',
        serial_number: null,
        retired: null,
        attributes: null,
        note: null,
        created_on: new Date()
    };
    static classMap = {
        model: '_Model',
        organisation: 'Organisation'
    };

    static loadAll() {
        return Model.loadAll(Platform);
    }

    static loadShips() {
        return Model.loadAll(Platform, {filter: {type: 'ship'}});
    }

    static loadSubs() {
        return Model.loadAll(Platform, {filter: {type: ['rov', 'submersible', 'drop_camera', 'plankton_sampler']}});
    }

    static loadOne(id) {
        return Model.loadOne(Platform, id);
    }

}
Model.registerClass(Platform, 'Platform');


export class AnnotationSoftware extends Model{

    static modelPath = `${Net.API}/annotation_software`;
    static defaultProps = {
        id: 0,
        name: 'New Annotation Software',
        note: null
    };

    static loadAll() {
        return Model.loadAll(AnnotationSoftware);
    }
}
Model.registerClass(AnnotationSoftware, 'AnnotationSoftware');


export class AnnotationJob extends Model {

    static modelPath = `${Net.API}/annotation_jobs`;
    static defaultProps = {
        id: 0,
        name: 'New Annotation Job',
        objective: null,
        note: null,
        start_date: null,
        end_date: null,
        annotation_protocols: [],
        crew: [],
        created_on: new Date(),
        updated_on: new Date()
    };
    static classMap = {
        annotation_protocols: 'AnnotationJobAnnotationProtocol',
        crew: 'AnnotationJobCrew',
    };

    static loadAll() {
        return Model.loadAll(AnnotationJob);
    }

    static loadOne(id) {
        return Model.loadOne(AnnotationJob, id);
    }

    addCrew() {
        this.crew.push(new AnnotationJobCrew());
        this.dirty = true;
    }

    removeCrew(crew) {
        this.crew.splice(this.crew.indexOf(crew), 1);
    }

};
Model.registerClass(AnnotationJob, 'AnnotationJob');


export class AnnotationJobRole extends Model {

    static modelPath = `${Net.API}/annotation_job_roles`;
    static defaultProps = {
        id: 0,
        name: 'New Annotation Job Role',
        note: null,
    };
    static classMap = {
    };

    static loadAll() {
        return Model.loadAll(AnnotationJobRole);
    }

};
Model.registerClass(AnnotationJobRole, 'AnnotationJobRole');


export class AnnotationJobCrew extends Model {

    static modelPath = `${Net.API}/annotation_job_crew`;
    static defaultProps = {
        id: 0,
        annotation_job: null,
        role: null,
        person: null,
    };
    static classMap = {
        annotation_job: 'AnnotationJob',
        person: 'Person',
        role: 'AnnotationJobRole',
    };

};
Model.registerClass(AnnotationJobCrew, 'AnnotationJobCrew');


export class AnnotationJobAnnotationProtocol extends Model {
    static modelPath = `${Net.API}/annotation_job_annotation_protocols`;
    static defaultProps = {
        id: 0,
        annotation_job: null,
        annotation_protocol: null,
    };
    static classMap = {
        annotation_job: 'AnnotationJob',
        annotation_protocol: 'AnnotationProtocol',
    };
};
Model.registerClass(AnnotationJobAnnotationProtocol, 'AnnotationJobAnnotationProtocol');


export class AnnotationProtocol extends Model {

    static modelPath = `${Net.API}/annotation_protocols`;
    static defaultProps = {
        id: 0,
        name: 'New Annotation Protocol',
        creator: null,
        note: null,
        medium_type: null,
        annotation_software: null,
        is_template: null,
        image_interval: null,
        image_interval_unit: 's',
        image_overlap: null,
        observation_interval: null,
        observation_interval_unit: 's',
        habitat_interval: null,
        habitat_interval_unit: 's',
        fov_interval: null,
        fov_interval_unit: 's',
        invertebrate_species: null,
        fish_species: null,
        algae_species: null,
        biogenic_habitat: null,
        habitat_only: null,
        documents: [],
        created_on: new Date(),
        updated_on: new Date()
    };
    static classMap = {
        creator: 'Person',
        medium_type: 'MediumType',
        annotation_software: 'AnnotationSoftware',
        documents: 'AnnotationProtocolDocument'
    };

    /**
     * Clone the annotation protocol. Call the parent's clone method,
     * then empty the documents array to avoid polluting the parent object's
     * document list.
     * 
     * @returns 
     */
    clone() {
        let c = super.clone();
        c.documents.splice(0);
        return c;
    }

    static loadOne(pk) {
        return Model.loadOne(AnnotationProtocol, pk);
    }

    static loadAll() {
        return Model.loadAll(AnnotationProtocol);
    }

    loadTaxa() {
        return Model.loadAll(Taxon, {models: [this]});
    }

    addDocument() {
        const ap = new AnnotationProtocolDocument();
        ap.annotation_protocol = this.id;
        this.documents.push(ap);
    }

    removeDocument(doc) {
        let l = this.documents.length;
        this.documents.splice(this.documents.indexOf(doc), 1);
        if(l != this.documents.length)
            this.dirty = true;
    }

}
Model.registerClass(AnnotationProtocol, 'AnnotationProtocol');


export class AnnotationProtocolDocument extends Model {

    static modelPath(params) {
        `${Net.API}/annotation_protocols`;
        let inst = params.models.pop();
        if(inst && inst instanceof AnnotationProtocol) {
            return `${Net.API}/annotation_protocols/${inst.id}/documents`;
        } else {
            return `${Net.API}/annotation_protocol_documents`;
        }
    }

    static defaultProps = {
        id: 0,
        title: 'New Annotation Protocol Document',
        annotation_protocol: null,
        url: null,
        note: null,
        file: null,
        created_on: new Date(),
        updated_on: new Date()
    };
    static classMap = {
        file: 'UploadedFile'
    };

    constructor(props) {
        super(props);
        if(!this.file)
            this.file = new UploadedFile();
    }
    
    clean() {
        const obj = super.clean();
        if(this.uploaded_file_id)
            obj.uploaded_file_id = this.uploaded_file_id;
        return obj;
    }

    static loadAll() {
        return Model.loadAll(AnnotationProtocolDocument);
    }

}
Model.registerClass(AnnotationProtocolDocument, 'AnnotationProtocolDocument');


export class ObservationType extends Model {

    static modelPath = `${Net.API}/observation_types`;
    static defaultProps = {
        id: 0,
        name: 'New Observation Type',
        note: null
    };

    static loadAll() {
        return Model.loadAll(ObservationType);
    }

}
Model.registerClass(ObservationType, 'ObservationType');


export class AnnotationObservationType extends Model {

    static modelPath = `${Net.API}/annotation_observation_types`;
    static defaultProps = {
        id: 0,
        annotation_protocol_id: null,
        observation_category_id: null,
        observation_type_id: null,
        taxon_id: null
    };

    static loadAll() {
        return Model.loadAll(AnnotationObservationType);
    }

}
Model.registerClass(AnnotationObservationType, 'AnnotationObservationType');


export class CruiseFirstNationContact extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Cruise) {
            return `${Net.API}/cruises/${inst.id}/first_nation_contacts`;
        } else {
            return `${Net.API}/first_nation_contacts`;
        }
    };

    static defaultProps = {
        id: 0,
        //cruise: null,
        nation: null,
        contact_name: null,
        email: null,
        phone: null,
        note: null
    };

    static classMap = {
        cruise: 'Cruise'
    };

    static loadAll() {
        return Model.loadAll(CruiseFirstNationContact);
    }

}
Model.registerClass(CruiseFirstNationContact, 'CruiseFirstNationContact');



export class CruiseProgram extends Model {
    static modelPath = `${Net.API}/cruise_programs`;
    static defaultProps = {
        id: 0,
        cruise: null,
        program: null
    };
    static classMap = {
        cruise: 'Cruise',
        program: 'Program'
    };

    static loadAll() {
        return Model.loadAll(CruiseProgram);
    }

}
Model.registerClass(CruiseProgram, 'CruiseProgram');


export class Cruise extends Model {

    static modelPath = function(params) {
        return `${Net.API}/cruises`;
    }
    static defaultProps = {
        id: 0,
        ship: null,
        name: 'New Cruise',
        leg: 1,
        note:  null,
        programs: [],
        objective:  null,
        summary: null,
        start_time: null,
        end_time: null,
        planned_track:  null,
        dives: [],
        crew: [],
        files: [],
        first_nation_contacts: [],
        documents: [],
        approved: 0,
        created_on:  new Date(),
        admin_note: null
    };
    static classMap = {
        crew: 'CruiseCrew',
        programs: 'CruiseProgram',
        dives: 'Dive',
        ship: 'Platform',
        first_nation_contacts: 'CruiseFirstNationContact',
        documents: 'CruiseDocument'
    };

    static loadAll(params = {}) {
        return Model.loadAll(Cruise, params);
    }

    /**
     * Load the deep version of the cruise including dives, transects, etc.
     */
    load() {
        return Model.loadOne(Cruise, this.id);
    }

    get isValid() {
        try {
            if(
                this.name != null && this.name.length > 0
                && this.leg && parseInt(this.leg) > 0
                && Model.isValidDateTime(this.start_time)
                && this.crew && this.crew.length > 0
            ) {
                return true;
            }
        } catch(err) {
            console.error(`${this.constructor} is not valid: ${err.toString()}`);
        }
        return false;
    }

    addProgram(program) {
        if(this.programs.indexOf(program) == -1) {
            this.programs.push(program);
            this.dirty = true;
        }
    }

    removeProgram(program) {
        let l = this.programs.length;
        this.programs.splice(this.programs.indexOf(program), 1);
        if(l != this.programs.length)
            this.dirty = true;
    }

    addDocument() {
        this.documents.push(new CruiseDocument());
    }

    removeDocument(doc) {
        let l = this.documents.length;
        this.documents.splice(this.documents.indexOf(doc), 1);
        if(l != this.documents.length)
            this.dirty = true;
    }
    
    addCrew(crew) {
        if(this.crew.indexOf(crew) == -1) {
            this.crew.push(crew);
            this.dirty = true;
        }
    }

    removeCrew(crew) {
        let l = this.crew.length;
        this.crew.splice(this.crew.indexOf(crew), 1);
        if(l != this.crew.length)
            this.dirty = true;
    }
    
    addFNContact(cont) {
        if(this.first_nation_contacts.indexOf(cont) == -1) {
            this.first_nation_contacts.push(cont);
            this.dirty = true;
        }
    }

    removeFNContact(cont) {
        let l = this.first_nation_contacts.length;
        this.first_nation_contacts.splice(this.first_nation_contacts.indexOf(cont), 1);
        if(l != this.first_nation_contacts.length)
            this.dirty = true;
    }
    
    loadCrew() {
        return new Promise(resolve => {
            Model.loadAll(CruiseCrew, {
                models:[this]
            })
            .then(result => {
                this.crew = result;
                resolve(result);
            });
        });
    }

    loadTaxa() {
        return new Promise(resolve => {
            Model.loadAll(Taxon, {
                models:[this]
            })
            .then(result => {
                this.taxa = result;
                resolve(result);
            });
        });
    }

    loadDives() {
        return new Promise(resolve => {
            Model.loadAll(Dive, {
                models:[this]
            })
            .then(result => {
                this.dives = result;
                resolve(result);
            });
        });
    }

    // TODO: Remove this and add a filter to loadDives.
    loadDivesWithMedia() {
        return new Promise(resolve => {
            Model.loadAll(Dive, {
                models: [this],
                filter: {has_media: true}
            })
            .then(result => {
                this.dives = result;
                resolve(result);
            });
        });
    }

    loadPrograms() {
        return new Promise(resolve => {
            Model.loadAll(Program, {models:[this]})
            .then(result => {
                this.programs = result;
                resolve(result);
            });
        });
    }

    addDive() {
        const d = new Dive({cruise: this.id});
        this.dives.push(d);
        this.dirty = true;
        return d;
    }

    removeDive(dive = null) {
        if(confirm("Are you sure?")) {
            this.dives.splice(this.dives.indexOf(dive), 1);
            dive.destroy();
            this.dirty = true;
        }
    }

    loadFiles() {
        const t = this;
        return new Promise(resolve => {
            Model.loadAll(CruiseFile, {models: [t]})
            .then(result => {
                t.files = result;
                resolve(result);
            });
        });
    }

}
Model.registerClass(Cruise, 'Cruise');


export class CruiseDocument extends Model {

    static modelPath(params) {
        let inst = params.models.pop();
        if(inst && inst instanceof Cruise) {
            return `${Net.API}/cruises/${inst.id}/documents`;
        } else {
            return `${Net.API}/cruise_documents`;
        }
    }

    static defaultProps = {
        id: 0,
        title: 'New Cruise Document',
        url: null,
        note: null,
        file: null,
        created_on: new Date(),
        updated_on: new Date()
    };
    static classMap = {
        file: 'UploadedFile'
    };

    constructor(props) {
        super(props);
        if(!this.file)
            this.file = new UploadedFile();
    }

    static loadAll() {
        return Model.loadAll(CruiseDocument);
    }

}
Model.registerClass(CruiseDocument, 'CruiseDocument');


export class Program extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Cruise) {
            return `${Net.API}/cruises/${inst.id}/programs`;
        } else {
            return `${Net.API}/programs`;
        }
    };
    static defaultProps = {
        id: 0,
        name: 'New Program',
        objective: null,
        note: null,
        start_date: null,
        end_date: null,
        created_on: new Date(),
        members: []
    };
    static classMap = {
        members: 'ProgramMember'
    };

    addMember() {
        const m = new ProgramMember({program: this.id});
        this.members.push(m);
        this.dirty = true;
        return m;
    }

    removeMember(member) {
        if(confirm("Are you sure?")) {
            this.members.splice(this.members.indexOf(member), 1);
            member.destroy();
            this.dirty = true;
        }
    }

    static loadAll() {
        return Model.loadAll(Program);
    }

}
Model.registerClass(Program, 'Program');


export class ProgramRole extends Model {

    static modelPath = `${Net.API}/program_roles`;
    static defaultProps = {
        id: 0,
        name: 'New Program Role',
        note: null
    };

    static loadAll() {
        return Model.loadAll(ProgramRole);
    }
}
Model.registerClass(ProgramRole, 'ProgramRole');


export class ProgramMember extends Model {

    static modelPath = `${Net.API}/program_members`;
    static defaultProps = {
        id: 0,
        program: null,
        person: null,
        role: null
    };
    static classMap = {
        program: 'Program',
        person: 'Person',
        role: 'ProgramRole'
    };

    get label() {
        if(this.person && this.person instanceof Model && this.role && this.role instanceof Model) {
            return `${this.person.last_name}, ${this.person.first_name}: ${this.role.name}`;
        } else {
            return 'Unconfigured program member.';
        }
    }

    static loadAll() {
        return Model.loadAll(ProgramMember);
    }

}
Model.registerClass(ProgramMember, 'ProgramMember');


export class PlatformConfig extends Model {

    static modelPath = `${Net.API}/platform_configs`;
    static defaultProps = {
        id: 0,
        platform: null,
        name: 'New Platform Config',
        note: null,
        configuration: {},
        instrument_configs: [], // Not part of the model.
        created_on: new Date()
    };
    static classMap = {
        platform: 'Platform',
        instrument_configs: 'InstrumentConfig',
    };

    static loadAll() {
        return Model.loadAll(PlatformConfig);
    }

}
Model.registerClass(PlatformConfig, 'PlatformConfig');


export class DiveRole extends Model {

    static modelPath = `${Net.API}/dive_roles`;
    static defaultProps = {
        id: 0,
        name: 'New Dive Role',
        note: null
    };

    static loadAll() {
        return Model.loadAll(DiveRole);
    }

}
Model.registerClass(DiveRole, 'DiveRole');


export class CruiseRole extends Model {

    static modelPath = `${Net.API}/cruise_roles`;
    static defaultProps = {
        id: 0,
        name: 'New Cruise Role',
        note: null
    };

    static loadAll() {
        return Model.loadAll(CruiseRole);
    }

}
Model.registerClass(CruiseRole, 'CruiseRole');


export class Dive extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst instanceof Cruise) {
            return `${Net.API}/cruises/${inst.id}/dives`;
        } else {
            return `${Net.API}/dives`;
        }
    };
    static defaultProps = {
        id: 0,
        cruise_id: null,
        ship_config: null,
        sub_config: null,
        site: null,
        name: 'New Dive',
        objective: null,
        start_time: null,
        end_time: null,
        note: null,
        objective: null,
        summary: null,
        admin_note: null,
        attributes: null,
        seatube_id: null,
        transects: [],
        observationEvents: [],
        statusEvents: [],
        commentEvents: [],
        habitatEvents: [],
        measurements: [],
        positions: [],
        created_on: new Date(),
        dataCounts: {},
        crew: []
    };
    static classMap = {
        rov: 'Platform',
        ship_config: 'PlatformConfig',
        sub_config: 'PlatformConfig',
        site: 'Site',
        transects: 'Transect',
        observationEvents: 'Event',
        statusEvents: 'Event',
        commentEvents: 'Event',
        habitatEvents: 'Event',
        measurementEvents: 'Event',
        measurements: 'Measurement',
        positions: 'Position',
        crew: 'DiveCrew'
    };

    get ship() {
        if(!this._props.ship_config)
            return null;
        return this._props.ship_config ? this._props.ship_config.platform : null;
    }

    set ship(v) {
        if(!this._props.ship_config)
            this._props.ship_config = new PlatformConfig();
        this._props.ship_config.platform = v;
    }

    get sub() {
        if(!this._props.subconfig)
            return null;
        return this._props.sub_config ? this._props.sub_config.platform : null;
    }

    set sub(v) {
        if(!this._props.subconfig)
            this._props.sub_config = new PlatformConfig();
        this._props.sub_config.platform = v;
    }

    get pilots() {
        return this._pilots || [];
    }

    set pilots(v) {
        this._pilots = v;
    }

    get pilotNames() {
        try {
            return this.pilots.map(p => `${p.person.last_name}, ${p.person.first_name}`);
        } catch(err) {
            return [];
        }
    }

    set pilotNames(v) {
        //
    }

    // Loads a list of events, positions, measurments (etc?) and the number of records of each.
    // This is a special ReST endpoint.
    loadDataCounts() {
        return new Promise(resolve => {
            Net.get(`${Net.API}/dives/${this.id}/data_counts/`)
            .then(result => {
                for(let [k, v] of Object.entries(result))
                    this.dataCounts[k] = v;
                resolve(result);
            });
        });
    }

    // Load the list of pilots. Use a filter on the role name: 'Pilot'
    loadCrew() {
        return new Promise(resolve => {
            Model.loadAll(DiveCrew, {
                models:[this]
            })
            .then(result => {
                this.crew = result;
                resolve(result);
            });
        });
    }

    loadTransects() {
        return new Promise(resolve => {
            Model.loadAll(Transect, {models:[this]})
            .then(result => {
                this.transects = result;
                resolve(result);
            });
        });
    }

    addTransect() {
        const t = new Transect({dive: this.id});
        this.transects.push(t);
        this.dirty = true;
        return t;
    }

    removeTransect(transect) {
        if(confirm("Are you sure?")) {
            this.transects.splice(this.transects.indexOf(transect), 1);
            transect.destroy();
            this.dirty = true;
        }
    }

    loadObservationEvents(filter) {
        return new Promise(resolve => {
            if(!filter)
                filter = {};
            filter.type = 'observation';
            Event.loadAll({models: [this], filter: filter})
            .then(result => {
                this.observationEvents = result;
                resolve(result);
            });
        });
    }

    loadStatusEvents(filter) {
        return new Promise(resolve => {
            if(!filter)
                filter = {};
            filter.type = 'status';
            Event.loadAll({models: [this], filter: filter})
            .then(result => {
                this.statusEvents = result;
                resolve(result);
            });
        });
    }

    loadCommentEvents(filter) {
        return new Promise(resolve => {
            if(!filter)
                filter = {};
            filter.type = 'comment';
            Event.loadAll({models: [this], filter: filter})
            .then(result => {
                this.commentEvents = result;
                resolve(result);
            });
        });
    }

    loadMeasurementEvents(filter) {
        return new Promise(resolve => {
            if(!filter)
                filter = {};
            filter.type = 'measurement';
            Event.loadAll({models: [this], filter: filter})
            .then(result => {
                this.measurementEvents = result;
                resolve(result);
            });
        });
    }

    loadHabitatEvents(filter) {
        return new Promise(resolve => {
            if(!filter)
                filter = {};
            filter.type = 'habitat';
            Event.loadAll({models: [this], filter: filter})
            .then(result => {
                this.habitatEvents = result;
                resolve(result);
            });
        });
    }

    loadMeasurements(filter) {
        return new Promise(resolve => {
            Model.loadAll(Measurement, {models: [this], filter: filter})
            .then(result => {
                this.measurements = result;
                resolve(result);
            });
        });
    }

    loadPositions(filter) {
        return new Promise(resolve => {
            Model.loadAll(Position, {models: [this], filter: filter})
            .then(result => {
                this.positions = result;
                resolve(result);
            });
        });
    }

    static loadAll(cruise=null) {
        const params = {};
        if(cruise)
            params.models = [cruise];
        return Model.loadAll(Dive, params);
    }

    static loadOne(id) {
        return Model.loadOne(Dive, id);
    }

}
Model.registerClass(Dive, 'Dive');


export class DiveCrew extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Dive) {
            return `${Net.API}/dives/${inst.id}/crews`;
        } else {
            return `${Net.API}/dive_crews`;
        }
    };
    static defaultProps = {
        id: 0,
        dive: null,
        person: null,
        dive_role: null
    };
    static classMap = {
        dive: 'Dive',
        person: 'Person',
        dive_role: 'DiveRole'
    };

    static loadAll() {
        return Model.loadAll(DiveCrew);
    }

}
Model.registerClass(DiveCrew, 'DiveCrew');


export class CruiseCrew extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Cruise) {
            return `${Net.API}/cruises/${inst.id}/crews`;
        } else {
            return `${Net.API}/cruise_crews`;
        }
    };
    static defaultProps = {
        id: 0,
        cruise: null,
        person: null,
        cruise_role: null
    };
    static classMap = {
        cruise: 'Cruise',
        person: 'Person',
        cruise_role: 'CruiseRole'
    };

    static loadAll() {
        return Model.loadAll(CruiseCrew);
    }

}
Model.registerClass(CruiseCrew, 'CruiseCrew');


export class Transect extends Model {

    static modelPath = function(params) {
        const inst1 = params.models.pop();
        const inst2 = params.models.pop();
        if(inst1 instanceof Cruise) {
            if(inst2 && inst2 instanceof Dive) {
                return `${Net.API}/cruises/${inst1.id}/dives/${inst2.id}/transects`;
            } else {
                throw Error('If instance 1 is a Cruise, and instance 2 is given, it must be a Dive.');
            }
        } else if(inst1 instanceof Dive) {
            return `${Net.API}/dives/${inst1.id}/transects`;
        } else {
            return `${Net.API}/transects`;
        }
    };
    static defaultProps = {
        id: 0,
        dive: null,
        name: 'New Transect',
        objective: null,
        summary: null,
        note: null,
        start_time: null,
        end_time: null,
        admin_note: null
    };
    static classMap = {
        dive: 'Dive'
    };

    static loadAll() {
        return Model.loadAll(Transect);
    }

}
Model.registerClass(Transect, 'Transect');


export class Abundance extends Model {

    static modelPath = `${Net.API}/abundances`;
    static defaultProps = {
        id: 0,
        name: 'New Abundance',
        rank: null,
        source: null,
        note: null
    }

    static loadAll() {
        return Model.loadAll(Abundance);
    }

}
Model.registerClass(Abundance, 'Abundance');


export class Biocover extends Model {

    static modelPath = `${Net.API}/biocovers`;
    static defaultProps = {
        id: 0,
        name: 'New Biocover',
        note: null
    }

    static loadAll() {
        return Model.loadAll(Biocover);
    }

}
Model.registerClass(Biocover, 'Biocover');


export class Complexity extends Model {

    static modelPath = `${Net.API}/complexities`;
    static defaultProps = {
        id: 0,
        name: 'New Complexity',
        note: null
    }

    static loadAll() {
        return Model.loadAll(Complexity);
    }

}
Model.registerClass(Complexity, 'Complexity');


export class Flow extends Model {

    static modelPath = `${Net.API}/flows`;
    static defaultProps = {
        id: 0,
        name: 'New Flow',
        note: null
    }

    static loadAll() {
        return Model.loadAll(Flow);
    }

}
Model.registerClass(Flow, 'Flow');


export class ObservationConfidence extends Model {

    static modelPath = `${Net.API}/observation_confidences`;
    static defaultProps = {
        id: 0,
        name: 'New Observation Confidence',
        rank: null,
        note: null
    }

    static loadAll() {
        return Model.loadAll(ObservationConfidence);
    }

}
Model.registerClass(ObservationConfidence, 'ObservationConfidence');


export class Coverage extends Model {

    static modelPath = `${Net.API}/coverages`;
    static defaultProps = {
        id: 0,
        name: 'New Coverage',
        minimum: null,
        maximum: null,
        note: null
    }

    static loadAll() {
        return Model.loadAll(Coverage);
    }

}
Model.registerClass(Coverage, 'Coverage');


export class Disturbance extends Model {

    static modelPath = `${Net.API}/disturbances`;
    static defaultProps = {
        id: 0,
        name: 'New Disturbance',
        note: null
    }

    static loadAll() {
        return Model.loadAll(Disturbance);
    }

}
Model.registerClass(Disturbance, 'Disturbance');


export class ImageQuality extends Model {

    static modelPath = `${Net.API}/image_qualities`;
    static defaultProps = {
        id: 0,
        name: 'New Image Quality',
        rank: null,
        note: null
    }

    static loadAll() {
        return Model.loadAll(ImageQuality);
    }

}
Model.registerClass(ImageQuality, 'ImageQuality');


export class Protocol extends Model {

    static modelPath = `${Net.API}/protocols`;
    static defaultProps = {
        id: 0,
        name: 'New Protocol',
        note: null
    }

    static loadAll() {
        return Model.loadAll(Protocol);
    }

}
Model.registerClass(Protocol, 'Protocol');


export class Relief extends Model {

    static modelPath = `${Net.API}/reliefs`;
    static defaultProps = {
        id: 0,
        name: 'New Relief',
        note: null
    }

    static loadAll() {
        return Model.loadAll(Relief);
    }

}
Model.registerClass(Relief, 'Relief');


export class Substrate extends Model {

    static modelPath = `${Net.API}/substrates`;
    static defaultProps = {
        id: 0,
        name: 'New Substrate',
        note: null
    }

    static loadAll() {
        return Model.loadAll(Substrate);
    }

}
Model.registerClass(Substrate, 'Substrate');


export class SurveyMode extends Model {

    static modelPath = `${Net.API}/survey_modes`;
    static defaultProps = {
        id: 0,
        name: 'New Survey Mode',
        note: null
    }

    static loadAll() {
        return Model.loadAll(SurveyMode);
    }

}
Model.registerClass(SurveyMode, 'SurveyMode');


export class Thickness extends Model {

    static modelPath = `${Net.API}/thicknesses`;
    static defaultProps = {
        id: 0,
        name: 'New Thickness',
        minimum: null,
        maximum: null,
        note: null
    }

    static loadAll() {
        return Model.loadAll(Thickness);
    }

}
Model.registerClass(Thickness, 'Thickness');


export class StatusType extends Model {

    static modelPath = function(params) {
        let q = [];
        if(params.deep)
            q.push('deep=true');
            q = q.length ? '?' + q.join('&') : '';
        return `${Net.API}/status_types${q}`;
    };
    static defaultProps = {
        id: 0,
        name: 'New Status Type',
        details: [],
        note: null
    };
    static classMap = {
        details: 'StatusTypeDetail'
    };

    addDetail() {
        const s = new StatusTypeDetail({status_type: this.id});
        this.details.push(s);
        this.dirty = true;
        return s;
    }

    removeDetail(detail) {
        if(confirm("Are you sure?")) {
            this.details.splice(this.details.indexOf(detail), 1);
            detail.destroy();
            this.dirty = true;
        }
    }

    loadDetails() {        
        return new Promise(resolve => {
            Model.loadAll(StatusTypeDetail, {
                models:[this]
            })
            .then(result => {
                this.details = result;
                resolve(result);
            });
        });
    }

    static loadAll() {
        return Model.loadAll(StatusType);
    }

}
Model.registerClass(StatusType, 'StatusType');


export class StatusTypeDetail extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof StatusType) {
            return `${Net.API}/status_types/${inst.id}/details`;
        } else {
            return `${Net.API}/status_type_details`;
        }
    }
    static defaultProps = {
        id: 0,
        name: 'New Status Type Detail',
        status_type: null,
        note: null
    };
    static classMap = {
        status_type: 'StatusType'
    }

    static loadAll() {
        return Model.loadAll(StatusTypeDetail);
    }

    static loadForStatusType(id) {
        return Model.loadAll(StatusTypeDetail, {models: [new StatusType({id: id})]});
    }

}
Model.registerClass(StatusTypeDetail, 'StatusTypeDetail');


export class InstrumentConfig extends Model {

    static modelPath = function(params) {
        const inst = params && params.models ? params.models.pop() : null;
        if(inst && inst instanceof Configuration) {
            return `${Net.API}/configurations/${inst.id}/instrument_configs`;
        } else {
            return `${Net.API}/instrument_configs`;
        }
    };
    static defaultProps = {
        id: 0,
        note: null,
        configuration: null,
        instrument: null,
        created_on: new Date(),
        updated_on: new Date()
    };
    static classMap = {
        instrument: 'Instrument'
    };
    
    static loadAll() {
        return Model.loadAll(InstrumentConfig);
    }

    /**
     * Load a measurement summary for the given instrument configuration.
     * It will load a list of objects containing the measurement type name and unit,
     * and the number of records.
     * @param {*} icId 
     */
    static loadMeasurementSummary(icId) {
        return Net.get(`${InstrumentConfig.modelPath()}/${icId}/measurement_summary/`);
    }

}
Model.registerClass(InstrumentConfig, 'InstrumentConfig');


export class Measurement extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst instanceof Dive) {
            return `${Net.API}/dives/${inst.id}/measurements`;
        } else {
            return `${Net.API}/measurements`;
        }
    }

    static defaultProps = {
        id: 0,
        dive: null,
        measurement_type: null,
        timestamp: null,
        quantity: null,
        signal_quality: null,
        instrument_config: null,
    };

    static classMap = {
        dive: 'Dive',
        measurement_type: 'MeasurementType'
    };

    static loadAll() {
        return Model.loadAll(Measurement);
    }
}
Model.registerClass(Measurement, 'Measurement');


export class Position extends Model {

    static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst instanceof Dive) {
            return `${Net.API}/dives/${inst.id}/positions`;
        } else {
            return `${Net.API}/positions`;
        }
    }

    static defaultProps = {
        id: 0,
        dive: null,
        position_type: null,
        coordinates: null,
        timestamp: null,
        position: null,
        signal_quality: null,
        geom: null,
        instrument_config: null,
    };

    static classMap = {
        dive: 'Dive',
        position_type: 'PositionType',
    };

    static loadAll() {
        return Model.loadAll(Position);
    }

}
Model.registerClass(Position, 'Position');
