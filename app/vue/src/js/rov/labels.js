/**
 * This contains classes and constants used by the label mapping tool.
 */

import { Net, hasProp } from '@/js/util.js';

/**
 * Sort comparator for labels.
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
const labelSort = (a, b) => {
    if(!a || !a.name || !b || !b.name)
        return 0;
    a = a.name.toLowerCase();
    b = b.name.toLowerCase();
    if(a < b) {
        return -1;
    } else if(a > b) {
        return 1;
    } else {
        return 0;
    }
};

/**
 * Configurations for the input fields related to event type properties.
 */
const configs = {
    note: {label: 'Note', type: 'text', default: '', doc: 'An optional note about the tag.'},
    common_name: {label: 'Common Name', type: 'text', default: '', doc: 'The common or colloquial name of the organism or object.'},
    scientific_name: {label: 'Scientific Name', type: 'text', default: '', doc: 'The scientific or bi- or trinomial name of the organism.'},
    aphia_id: {label: 'Aphia ID', type: 'int', default: '', doc: 'The ID of the organism in the WoRMS database.'},
    inaturalist_id: {label: 'iNaturalist ID', type: 'int', default: '', doc: 'The ID of the organism in the iNaturalist database.'},
    hart_code: {label: 'Hart Code', type: 'text', default: '', doc: 'The Hart code of the organism.'},
    otu: {label: 'OTU', type: 'text', default: '', doc: 'An optional Operational Taxonomic Unit for the organism.'},
    biocover: {label: 'Biocover', type: 'lookup', source: 'biocovers', default: 0, doc: 'The biocover.'},
    abundance: {label: 'Abundance', type: 'lookup', source: 'abundances', default: 0, doc: 'The relative abundance of the organism.'}, 
    coverage: {label: 'Coverage', type: 'lookup', source: 'coverages', default: 0, doc: 'The percent coverage of the habitat by the biocover or substrate.'},
    thickness: {label: 'Thickness', type: 'lookup', source: 'thicknesses', default: 0, doc: 'The thickness of the biocover.'}, 
    complexity: {label: 'Complexity', type: 'lookup', source: 'complexities', default: 0, doc: 'The complexity of the biocover.'},
    disturbance: {label: 'Disturbance', type: 'lookup', source: 'disturbances', default: 0, doc: 'Human disturbance of the biocover.'},
    substrate: {label: 'Substrate', type: 'lookup', source: 'substrates', default: 0, doc: 'The material composition of the habitat substrate.'},
    relief: {label: 'Relief', type: 'lookup', source: 'reliefs', default: 0, doc: 'The topographic relief of the substrate.'},
    flow: {label: 'Flow', type: 'lookup', source: 'flows', default: 0, doc: 'Current flow at the site.'},
    image_quality: {label: 'Image Quality', type: 'lookup', source: 'image_qualities', default: 0, 
            doc: 'The quality of the video or image at the current time.'},
    off_transect: {label: 'Detail', type: 'lookup', source: 'off_transect_details', default: 0, 
            doc: 'Detail describing why the vehicle is not currently on transect.'},
    no_laser_detail: {label: 'Detail', type: 'lookup', source: 'no_laser_details', default: 0, 
            doc: 'Detail describing why the laser measurement is not currently available.'},
    restriction: {label: 'Restriction', type: 'lookup_multi', source: 'restrictions', default: [],
            doc: 'One or more optional restrictions or licensing conditions which restrict sharing of the observation.'},
    confidence: {label: 'Observation Confidence', type: 'lookup', source: 'observation_confidences', default: 0, 
            doc: 'The confidence level of this observation.'},
    survey_mode: {label: 'Survey Mode', type: 'lookup', source: 'survey_modes', default: 0, 
            doc: 'The current survey mode.'},
    survey_protocol: {label: 'Survey Protocol', type: 'lookup', source: 'survey_protocols', default: 0, 
            doc: 'The current survey protocol.'},
    measurement_quantity: {label: 'Quantity', type: 'float', default: '', doc: 'The quantity of the measurement.'},
    measurement_unit: {label: 'Unit', type: 'lookup', source: 'units', default: 0, doc: 'Measurement units.'},
    status_type: {label: 'Status Type', type: 'lookup', source: 'status_types', default: 0, doc: 'The higher-level status type.'},
    status_detail: {label: 'Status Detail', type: 'lookup', source: 'status_details', default: 0, doc: 'The lower-level status detail.'},
};

/**
 * Reusable sub-lists of tags that can be attached to top-level tags.
 * Items with a fields property are shown as input fields. Those without
 * fields are saved as tags.
 */
const tags = {
    silt: ['Silt'],
    life_status: [ 'Alive', 'Dead', 'Partially Dead', 'Toppled', 'Drifting', 'Attached', 'Detached', 'Habitat-Forming', 'Encrusting', 'Eggs', 'Egg Casings'],
    aggregation: ['Group', 'Mat', 'Patches', 'Solitary', 'Schooling', 'Encrusting'],
    domsub: ['Dominant', 'Subdominant'],
    size: ['Large', 'Medium', 'Small', 'Height: >50cm', 'Height: <50cm'],
    form: ['Rubble', 'Clumps', 'Blocks', 'Reef'],
    terrain: ['Slope', 'Wall', 'Cliff'],
    flow: ['High Flow', 'Low Flow', 'No Flow', 'No Reliable Flow Indicator']
};

/**
 * Filter the status details based on the selected status type.
 * @param {*} label 
 * @param {*} items 
 * @returns 
 */
const statusTypeFilter = (label, items) => {
    try {
        return items.filter(item => {
            return parseInt(item.status_type.id) == parseInt(label.properties['status/type']);
        });
    } catch(err) {
        return [];
    }
};

/**
 * A hierarchical list of event types.
 */
export const eventTypes = [
    {label: 'Ignore', key: 'ignore'},
    {label: 'Not Annotated', key: 'not_annotated'},
    {label: 'Observation', key: 'observation',
        properties: [
            {label: 'Common Name', key: 'observation/common_name', config: configs.common_name},
            {label: 'Scientific Name', key: 'observation/scientific_name', config: configs.hart_code},
            {label: 'Aphia ID', key: 'observation/aphia_id', config: configs.aphia_id},
            {label: 'iNaturalist ID', key: 'observation/inaturalist_id', config: configs.inaturalist_id},
            {label: 'Hart Code', key: 'observation/hart_code', config: configs.hart_code},
            {label: 'OTU', key: 'observation/otu', config: configs.otu},
            {label: 'Abundance', key: 'observation/abundance', config: configs.abundance},
            {label: 'Confidence', key: 'observation/confidence', config: configs.confidence},
            {label: 'Restriction', key: 'observation/restriction', config: configs.restriction},
        ],
        tags: tags.life_status.concat(tags.aggregation).concat(tags.size).concat(tags.form),
    },
    {label: 'Habitat', key: 'habitat', 
        properties: [
            {label: 'Substrate', key: 'habitat/substrate', config: configs.substrate},
            {label: 'Biocover', key: 'habitat', config: configs.biocover},
            {label: 'Common Name', key: 'habitat/common_name', config: configs.common_name},
            {label: 'Scientific Name', key: 'habitat/scientific_name', config: configs.scientific_name},
            {label: 'Aphia ID', key: 'habitat/aphia_id', config: configs.aphia_id},
            {label: 'iNaturalist ID', key: 'habitat/inaturalist_id', config: configs.inaturalist_id},
            {label: 'Hart Code', key: 'habitat/hart_code', config: configs.hart_code},
            {label: 'OTU', key: 'habitat/otu', config: configs.otu},
            {label: 'Coverage', key: 'habitat/coverage', config: configs.coverage},
            {label: 'Disturbance', key: 'habitat/disturbance', config: configs.disturbance},
            {label: 'Complexity', key: 'habitat/complexity', config: configs.complexity},
            {label: 'Relief', key: 'habitat/relief', config: configs.relief},
            {label: 'Flow', key: 'habitat/flow', config: configs.flow},
            {label: 'Confidence', key: 'confidence/confidence', config: configs.confidence},
            {label: 'Restriction', key: 'habitat/restriction', config: configs.restriction},
        ],
        tags: tags.silt.concat(tags.domsub).concat(tags.relief).concat(tags.size).concat(tags.life_status),
    },
    {label: 'Status', key: 'status', properties: [
        {label: 'Status Type', key: 'status/type', config: configs.status_type},
        {label: 'Status Detail', key: 'status/detail', config: configs.status_detail, filter: statusTypeFilter},
    ]},
    {label: 'Measurement', key: 'measurement', properties: [
        {label: 'Quantity', key: 'measurement/quantity', config: configs.measurement_quantity},
        {label: 'Unit', key: 'measurement/unit', config: configs.measurement_unit},
    ]},
    {label: 'Comment', key: 'comment'},
];

/**
 * Get the event by its key.
 * @param {*} key 
 * @returns 
 */
export function getEventType(key) {
    const walk = (key, items) => {
        if(!items)
            return null;
        for(let item of items) {
            if(item.key == key)
                return item;
        }
        return null;
    };
    const type = walk(key, eventTypes);
    return type || {};
}

/**
 * Contains the mapping from an original label to the database entities that comprise the full 
 * corresponding dataset.
 */
export class Label {

    /**
     * Construct the object with data and a reference to the tree that owns it.
     * @param {*} data 
     * @param {*} tree 
     */
    constructor(data, tree) {
        if(!tree)
            throw new Error('A Label must receive a LabelTree instance in the constructor.');
        this._eventType = null;         // The event type is the top-level event type.
        this._hierarchy = null;         // Cache the hierarchical name.
        this._tags = [];                // Set for the tags to eliminate duplicates.
        this._properties = {};          // Event properties.
        this.tree = tree;               // A reference to the parent tree.
        this.update(data);
    }

    /**
     * Return the event type.
     */
    get eventType() {
        return this._eventType;
    }

    /**
     * Set the event type. Deletes the sub and subsub types.
     */
    set eventType(t) {
        this._eventType = t;
        this.updateProperties();
    }

    /**
     * Restore the prefills from a previous usage of this label.
     * Should not overwrite properties that have already been set.
     */
    restorePrefills(data) {
        if(!((this.properties && Object.values(this.properties).length) || (this.tags && this.tags.length > 0))) {
            this.properties = data.properties;
            this.tags = data.tags;
            this.updateProperties();
        }
    }

    /**
     * Update the properties list by visiting the events and sub events.
     */
    updateProperties() {
        // Create a starting list of the keys. These will be removed as they're updated.
        const rem = new Set(Object.keys(this.properties));
        if(this._eventType) {
            // Remove the top key from the removal list.
            rem.delete(this._eventType);
            // Get the event type by key.
            let et = getEventType(this._eventType);
            if(!et)
                return;
            // If the top level key is not set set it.
            if(!hasProp(this.properties, et.key))
                this.properties[et.key] = 0;
            // If there are properties on the event type, set them.
            if(et.properties) {
                et.properties.forEach(p => {
                    // Remove the key from the delete list.
                    rem.delete(p.key);
                    // If the property is not set, set it to null or the default. Does not delete or replace the value.
                    if(!hasProp(this.properties, p.key) || 
                            (p.config.default != null && this.properties[p.key] == null)) {
                        this.properties[p.key] = p.config.default;
                    }
                });
            }
        }
        // Remove the unused properties.
        Array.from(rem).forEach(k => delete this.properties[k]);
    }

    /**
     * Get the lowest-level event type.
     */
    get type() {
        const key = this.eventType || null;
        return getEventType(key);
    }

    /**
     * Get the tags list.
     */
    get tags() {
        if(this._tags == null)
            this._tags = [];
        return this._tags;
    }

    /**
     * Replace the tags list.
     */
    set tags(v) {
        this._tags.splice(0);
        if(v) {
            if(!Array.isArray(v)) 
                v = Object.values(v);
            v.forEach(v => this._tags.push(v));
        }
    }

    /**
     * Get the list of tags both from the event types and the label's own list.
     */
    get availableTags() {
        let tags = new Set(this._tags);
        if(this._eventType && getEventType(this._eventType).tags)
            getEventType(this._eventType).tags.forEach(tt => tags.add(tt));
        tags = Array.from(tags);
        tags = tags.filter(t => t && t.length > 0).sort();
        return tags;
    }

    /**
     * Get the properties list.
     */
    get properties() {
        if(this._properties == null)
            this._properties = {};
        return this._properties;
    }

    /**
     * Replace the properties list.
     */
    set properties(p) {
        Object.keys(this._properties).forEach(k => delete this._properties[k]);
        if(p)
            Object.entries(p).forEach(([k, v]) => this._properties[k] = v);
    }

    /**
     * Copy data from another label.
     * @param {*} label 
     */
    copyFrom(label) {
        this._eventType = label._eventType;
        this.properties = Object.fromEntries(Object.entries(label.properties));
        this.tags = Array.from(label.tags);
        this.updateProperties();
    }

    /**
     * Update the entity's state.
     * @param {*} data 
     */
    update(data) {
        this._type = data._type;                    // The label has a type. TODO: ?
        this._eventType = data.eventType;
        this.id = data.id;                          // The label has an ID.
        this.parent_id = data.parent_id;            // The label has a parent; this is its ID.
        this.name = data.name;                      // The label has a name.
        this.properties = data.properties;
        this.tags = data.tags;
        this.updateProperties();
        return this;
    }

    /**
     * Get the hierarchical label by checking for a parent and concatenating.
     * Will be cached.
     */
    get hierarchy() {
        if(!this._hierarchy) {
            if(this.tree.labelMap.has(this.parent_id)) {
                const parent = this.tree.labelMap.get(this.parent_id);
                this._hierarchy = `${parent.hierarchy} > ${this.name}`;
            } else {
                this._hierarchy = this.name;
            }
        }
        return this._hierarchy;
    }

    /**
     * Returns true if the properties list contains the given key.
     * @param {*} prop 
     * @returns 
     */
    hasProperty(prop) {
        return hasProp(this.properties, prop);
    }

    /**
     * If the tag is in the tags list, return true.
     * Case insensitive.
     * @param {*} tag 
     * @returns 
     */
    hasTag(tag) {
        try {
            tag = tag.toLowerCase();
            return this.tags.find(t => t.toLowerCase() == tag) != null;
        } catch(err) {
            return false;
        }
    }

    /**
     * Return a serializable representation.
     * @returns 
     */
    toJSON() {
        return {
            _type: this._type,
            eventType: this._eventType,
            properties: this._properties,
            tags: this._tags,
            id: this.id,
            parent_id: this.parent_id,
            name: this.name,
        };
    }

}

/**
 * Represents a label tree.
 */
export class LabelTree {

    constructor(data) {
        this.labels = [];           // The labels list.
        this.labelMap = new Map();  // Labels by ID.
        this.hidden = new Set();    // Tracks labels that have been hidden by ID.
        this.update(data);
    }

    /**
     * Update the object's state.
     * @param {*} data 
     */
    update(data) {
        this._type = data._type;                    // The tree type.
        this.id = data.id;                          // The tree ID.
        this.name = data.name;                      // The tree name.
        this.description = data.description;        // The tree description.
        // Prepare the list of lables by instantiating label objects.
        data.labels.forEach(data => {
            const l = new Label(data, this);
            this.labels.push(l);
            this.labelMap.set(l.id, l);
        });
        return this;
    }

    /**
     * Return a serializable representation.
     * @returns 
     */
    toJSON() {
        return {
            _type: this._type,
            id: this.id,
            name: this.name,
            description: this.description,
            labels: this.labels.map(label => label.toJSON())
        };
    }

    /**
     * Sort this tree's labels.
     */
    sortLabels() {
        this.labels.sort(labelSort);
    }

}

/**
 * An annotation project.
 */
export class AnnotationProject {

    constructor(data) {
        this.labelTrees = [];      // The list of label trees.
        this.members = {};         // The map of members.
        this.update(data);
    }

    /**
     * Update the instance's state.
     * @param {*} data 
     */
    update(data) {
        this._type = data._type;                // The type of project. (?)
        this.id = data.id;                      // The project ID.
        this.name = data.name;                  // The project name.
        this.fileName = data.fileName;          // The filename, if the project was loaded from a file.
        this.description = data.description;    // A description of the project.
        this.members = data.members || {};      // A dictionary of project members.
        // Instantiate LabelTree object for any tree that is a raw object.
        this.labelTrees = (data.labelTrees || []).map(tree => tree instanceof LabelTree ? tree : new LabelTree(tree)); 
        // Sort the labels.
        this.labelTrees.forEach(tree => tree.sortLabels());
        return this;
    }

    /**
     * Return a serializable representation.
     * @returns 
     */
    toJSON() {
        return {
            _type: this._type,
            id: this.id,
            name: this.name,
            description: this.description,
            fileName: this.fileName,
            description: this.description,
            members: this.members,
            labelTrees: this.labelTrees.map(t => t.toJSON()),
        }
    }

    /**
     * Resolve the members from Biigle representation to database entities.
     */
    resolveMembers() {
        Object.entries(this.members).forEach(([id, m]) => {
            if(m.person && !(m.person instanceof Person))
                m.person = Model.find(parseInt(m.person.id), Person);
        });
    }

    /**
     * Load the list of label trees. How it does this depends on the _type
     * property. 
     * @returns 
     */
    loadLabelTrees() {
        return new Promise((resolve, reject) => {
            if(this._type == 'biigle') {
                Net.get(`${Net.API}/biigle_api/projects/${this.id}/label-trees/`)
                .then(result => {
                    if(result.message) {
                        reject(new Error('Unable to load label trees for this project: ' + result.message));
                    } else {
                        result.forEach(tree => {
                            this.labelTrees.push(new LabelTree(tree));
                        });
                        resolve();
                    }
                })
                .catch(err => {
                    reject(new Error('Unable to load label trees for this project: ' + err.toString()));
                });
            } else {
                resolve();
            }
        });
    }

    /**
     * Load projects from Biigle for the current user and return a promise. 
     * 
     * Resolves with a list of AnnotationProjects.
     * @returns 
     */
    static loadBiigleProjects() {
        return new Promise((resolve, reject) => {
            Net.get(`${Net.API}/biigle_api/projects/`)
                .then(result => {
                    if(result.message) {
                        reject(new Error('Unable to load Biigle projects: ' + result.message));
                    } else {
                        let projects = [];
                        result.forEach(data => {
                            data._type = 'biigle';
                            projects.push(new AnnotationProject(data));
                        });
                        resolve(projects);
                    }
                })
                .catch(err => {
                    reject(new Error('Unable to load Biigle projects: ' + err.toString()));
                });
        });
    }

    /**
     * Load a project file and return a Promise.
     * Resolves with a single project.
     * @param {*} files A single File, or an array of Files.
     * @returns 
     */
    static loadProjectFile(files) {
        
        // If a raw file is given, wrap it in an array for handling.
        if(!(files instanceof Array))
            files = [files];

        return new Promise((resolve, reject) => {
            // Parse and prepare the loaded label tree.
            const projectData = {
                _type: 'generic',
                _about: 'Generated for file trees.',
                id: 1,
                name: 'Label Mapping Project'
            };

            const project = new AnnotationProject(projectData);

            // Parse the labels trees.
            let count = files.length;
            files.forEach(file => {
                file.text()
                    .then(data => {
                        data = JSON.parse(data);
                        data.labelTrees.forEach(tree => tree.labels.sort((a, b) => a.name.localeCompare(b.name)));
                        project.labelTrees.push(...data.labelTrees);
                        if(--count == 0)
                            resolve([project]);
                    })
                    .catch(err => {
                        reject(new Error('Unable to load project: ' + err.toString()));
                    });
            });
        });
    }

    /**
     * Load members of the project. How it does this depends on the value of _type.
     * 
     * May not work with projects not owned by the user.
     */
    loadMembers() {
        if(this._type == 'biigle') {
            return new Promise((resolve, reject) => {
                const promises = [];
                promises.push(Net.get(`${Net.API}/biigle_api/projects/${this.id}/users/`));
                // TODO: Not allowed yet. this.labelTrees.forEach(tree => promises.push(Net.get(`${Net.API}/biigle_api/label-trees/${this.id}/users/`)));
                Promise.all(promises)
                    .then(result => {
                        if(result.message) {
                            reject(new Error('Unable to load Biigle project members: ' + result.message));
                        } else {
                            result.forEach(result => {
                                result.forEach(member => {
                                    if(!this.members[member.id]) {
                                        this.members[member.id] = {
                                            member: {id: member.id, first_name: member.firstname, last_name: member.lastname}, 
                                            person: null
                                        }
                                    }
                                });
                            });
                            resolve();
                        }
                    })
                    .catch(err => {
                        reject(new Error('Unable to load Biigle project members: ' + err.toString()));
                    });
            });
        } else {
            return new Promise((resolve, reject) => {
                resolve();
            });
        }
    }

}


