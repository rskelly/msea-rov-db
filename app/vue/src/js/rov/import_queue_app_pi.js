import { Nav } from '@/js/rov/import_queue_nav.js';
import { Cruise, Platform, Program, CruiseRole, ImportQueuePrincipalInvestigatorJob } from '@/js/rov/models.js';
import { Person } from '@/js/shared/models.js';

/**
 The Biigle principal investigator import configuration app. Provides a stepwise method of configuring
 the cruise and associated metadata.
 */
export class ImportQueuePrincipalInvestigatorApp {

    constructor() {
        this.inited = false;
    }

    /**
     * This must be called from a Vue component on an instance wrapped in a reactive
     * call (a Proxy). If it is called from constructor, it won't work.
     */
    init() {
        // State.
        this.cruise = null;                 // The selected cruise.
        this.cruises = [];                  // The list of available cruises.
        this.job = null;                    // The current import job.
        this.jobs = [];                     // The list of available jobs.
        this.ships = [];                    // The list of available ships.
        this.people = [];                   // The list of available people.
        this.programs = [];                 // The list of available programs.
        this.cruise_roles = [];             // The list of available cruise roles.
        this.cruiseMetaLoading = false;     // True when the cruises are loading; disables certain UI features.
        this.addingToQueue = false;         // True when being saved to queue.
        this.jobsLoading = false;           // True when jobs are being loaded.

        // Build the navigation data object (an instance of Nav), which supplies the StepsBar component with state.
        //  id: The section element ID;
        //  name: The button label;
        //  init: a function to call on this object to initialize the section's state;
        //  enabled: checks the app's state to verify that the section is available (NavItem will check prior states).
        this.nav = new Nav([
            { id: "view_start", name: "Start", init: null, enabled: () => true },
            { id: "view_profile", name: "Profile", 
                init: this.initProfile.bind(this), 
                enabled: () => true, 
            },
            { id: "view_cruise_metadata", name: "Cruise Metadata", 
                init: this.initCruiseMetadata.bind(this),
                enabled: () => true,
            },
            { id: "view_complete", name: "Complete", 
                init: this.initComplete.bind(this),
                enabled: () => {
                    return this.cruise && this.cruise.isValid
                }
            }
        ]);
        
        this.inited = true;
    }

    /**
     * Set the job for editing.
     * @param {*} job 
     */
    setJob(job) {
        this.job = job;
        this.cruise = job.cruise;
    }

    /**
     * Reset the application state.
     */
    resetState() {
        this.job = null;
        this.cruise = null;
    }

    /**
     * Init function for the profile view.
     */
    initProfile() {
        // Re/load previously created jobs.
        this.resetState();
        this.loadJobs();
    }

    /**
     * Initialize the complete step.
     */
    initComplete() {
        if(!this.job)
            this.job = new ImportQueuePrincipalInvestigatorJob();
    }

    /**
     * Initialize the cruise metadata view.
     */
    initCruiseMetadata() {
        if(this.cruiseMetaLoading)
            return;
        this.cruiseMetaLoading = true;
        // Load all the lookups.
        Platform.loadShips()
            .then(result => {
                this.ships.push(...result);
            }).catch(alert);
        Person.loadAll()
            .then(result => {
                this.people.push(...result);
            }).catch(alert);
        Program.loadAll()
            .then(result => {
                this.programs.push(...result);
            }).catch(alert);
        CruiseRole.loadAll()
            .then(result => {
                this.cruise_roles.push(...result);
            }).catch(alert);
        Cruise.loadAll({filter: {unapproved: true}})
            .then(result => {
                this.cruises.push(...result);
            }).catch(alert);
    }

    /**
     * Add a new program to the program lookups list.
     * @param {*} program 
     */
    addProgram(program) {
        this.programs.push(program);
    }

    /**
     * Add a new person to the person lookups list.
     * @param {*} person 
     */
    addPerson(person) {
        this.people.push(person);
    }

    /**
     * Add a new cruise role to the lookups list.
     * @param {*} role 
     */
    addCruiseRole(role) {
        this.cruise_roles.push(role);
    }
    
    /**
     * Load the list of jobs initiated by this user.
     */
    loadJobs() {
        this.jobsLoading = true;
        this.jobs.splice(0);
        ImportQueuePrincipalInvestigatorJob.loadAll({shallow: true})
            .then(result => {
                this.jobs.splice(0);
                this.jobs.push(...result);
                this.jobsLoading = false;
            })
            .catch(err => {
                this.jobsLoading = false;
                alert(err);
            });
    }

    /**
     * Add the completed import package to the queue.
     */
    addToQueue(evt) {
        evt.preventDefault();
        this.addingToQueue = true;
        // Assemble the job object.
        const job = this.job;
        job.cruise = this.cruise;
        // Save the job.
        job.save()
            .then(result => {
                this.addingToQueue = false;
                alert('The import job has been created successfully.');
                // Reset state and go back to the sign-in page.
                this.resetState();
                this.nav.goTo('view_profile');
            }).catch(err => {
                this.addingToQueue = false;
                alert(err);
            });
    }

}