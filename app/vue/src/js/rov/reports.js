/**
 * This is the application code for the rov/reports.html page. It takes care of collecting user input,
 * checking and generating a variety of reports on taxonomy and cruise/dive tracks.
 */

const pamap = new PAHomeMap();      // The protected areas map.
const drawmap = new DrawMap();      // The drawable map.

let clickedPA = null;       // Set when a protected area is clicked.
let searchGeom = null;      // Set when a region is drawn on a map.

class AppData {

    processParams(list, extra = null) {
        const out = [];
        list.forEach(k => {
            let [k1, k2] = Array.isArray(k) ? k : [k, k];
            if(this[k1])
                out.push(`${k2}=${this[k1]}`);
        });
        if(extra) {
            for(let [k, v] of Object.entries(extra)) {
                if(v)
                    out.push(`${k}=${v}`);
            }
        }
        return out;
    }

    formatUrl(url) {
        while(url.substring(0, 1) == '/')
            url = url.substring(1);
        return document.location.origin + '/' + url;
    }

}

/**
 * Contains information for the water properties report.
 */
class WaterProperties extends AppData {

    constructor() {
        super();
        this._cruise = null;    // The selected cruise.
        this._dive = null;      // The selected dive.
        this.format = null;     // The selected format.
        this.count = '...';     // The count of available records.
        this.formats = ['CSV', 'Excel', 'HTML', 'JSON', 'SQLite', 'SHP']
        this.params = ['cruise_id', 'dive_id', 'format'];
    }

    reset() {
        this.count = '...';
    }

    checkReport(evt = null) {
        if(evt) evt.preventDefault();
        this.reset();
        get(this.getUrl({fn: 'count'}))
        .then(result => {
            this.count = result.count || 0;
        })
        .catch(err => {
            this.count = '?';
        });
    }

    generateReport(evt = null) {
        if(evt) evt.preventDefault();
        window.open(this.getUrl(), '_blank');
    }

    getUrl(extra_params = null) {
        let params = this.processParams(this.params, extra_params).join('&');
        return this.formatUrl(`${Net.API}/rov/water_properties_reports/?${params}`);
    }

    get generateEnabled() {
        return this.format && !isNaN(this.count) && this.count > 0;
    }

    get cruise() {
        return this._cruise;
    }

    set cruise(v) {
        if(v != this._cruise) {
            this.dive = null;
            v.loadDives();
            this._cruise = v;
        }
    }

    get cruise_id() {
        return this._cruise ? this._cruise.id : null;
    }

    get dive() {
        return this._dive;
    }

    set dive(v) {
        this._dive = v;
    }

    get dive_id() {
        return this._dive ? this._dive.id : null;
    }

}

/**
 * Contains information for generating cruise taxa reports.
 */
class CruiseTaxa extends AppData {

    constructor() {
        super();
        this._cruise = null;    // The selected cruise.
        this._dive = null;      // The selected dive.
        this.format = null;     // The selected format.
        this.count = '...';     // The count of available records.
        this.position = null;
        this.depth = null;
        this.temperature = null;
        this.salinity = null;
        this.aggregate = 'yes';
        this.andor = 'and';
        this.term = null;
        this.formats = ['CSV', 'Excel', 'HTML', 'JSON', 'SQLite', 'SHP'];
        this.params = ['cruise_id', 'dive_id', 'format', 'aggregate', 'andor', 'term']; //'position', 'depth', 'temperature', 'salinity'];
    }

    reset() {
        this.count = '...';
    }

    get generateEnabled() {
        return this.format && !isNaN(this.count) && this.count > 0;
    }

    getUrl(extra_params = null) {
        let params = this.processParams(this.params, extra_params).join('&');
        return this.formatUrl(`${Net.API}/rov/cruise_reports/?${params}`);
    }

    checkReport(evt = null) {
        if(evt) evt.preventDefault();
        this.reset();
        get(this.getUrl({fn: 'count'}))
        .then(result => {
            this.count = result.count || 0;
        })
        .catch(err => {
            this.count = '?';
        });
    }

    generateReport(evt) {
        if(evt) evt.preventDefault();
        window.open(this.getUrl(), '_blank');
    }

    get cruise() {
        return this._cruise;
    }

    set cruise(v) {
        if(v != this._cruise) {
            this.dive = null;
            v.loadDives();
            this._cruise = v;
        }
    }

    get cruise_id() {
        return this._cruise ? this._cruise.id : null;
    }

    get dive() {
        return this._dive;
    }

    set dive(v) {
        this._dive = v;
    }

    get dive_id() {
        return this._dive ? this._dive.id : null;
    }
}

/**
 * Contains information for generating taxa and track reports for protected areas.
 */
class ProtectedAreas extends AppData {

    constructor() {
        super();
        this.trackCount = '...';    // The count of available track records.
        this.trackFormat = null;    // The selected format for track reports.
        this.taxaCount = '...';     // The count of available taxa records.
        this.taxaFormat = null;     // The selected format for taxa reports.
        this.pa_id = null;          // The selected protected area ID.
        this.formats = ['CSV', 'Excel', 'HTML', 'JSON', 'SQLite', 'SHP'];
        this.taxaParams = [['taxaFormat', 'format']];
        this.trackParams = [['trackFormat', 'format']];
    }

    reset() {
        this.trackCount = '...';
        this.taxaCount = '...';
    }

    checkTaxaReport(evt = null) {
        if(evt) evt.preventDefault();
        this.pa_id = clickedPA.id;
        get(this.getTaxaUrl({fn: 'count'}))
        .then(result => {
            this.taxaCount = result.count || 0;
        })
        .catch(err => {
            this.taxaCount = '?';
        });
    }

    generateTaxaReport(evt = null) {
        if(evt) evt.preventDefault();
        window.open(this.getTaxaUrl(), '_blank');
    }

    checkTrackReport(evt = null) {
        if(evt) evt.preventDefault();
        this.pa_id = clickedPA.id;
        get(this.getTrackUrl({fn: 'count'}))
        .then(result => {
            this.trackCount = result.count || 0;
        })
        .catch(err => {
            this.trackCount = '?';
        });
    }

    generateTrackReport(evt = null) {
        if(evt) evt.preventDefault();
        window.open(this.getTrackUrl(), '_blank');
    }

    getTaxaUrl(extra_params = null) {
        let params = this.processParams(this.taxaParams, extra_params);
        params.push('type=taxa');
        params = params.join('&');
        return this.formatUrl(`${Net.API}/pas/${this.pa_id}/reports/?${params}`);
    }

    getTrackUrl(extra_params = null) {
        let params = this.processParams(this.trackParams, extra_params);
        params.push('type=dive_tracks');
        params = params.join('&');
        return this.formatUrl(`${Net.API}/pas/${this.pa_id}/reports/?${params}`);
    }

    get trackEnabled() {
        return this.trackFormat && !isNaN(this.trackCount) && this.trackCount > 0;
    }

    get taxaEnabled() {
        return this.taxaFormat && !isNaN(this.taxaCount) && this.taxaCount > 0;
    }

}

/**
 * Contains information for track and taxa reports for the drawable map.
 */
class Draw extends AppData {

    constructor() {
        super();
        this.trackCount = '...';    // The count of available track records.
        this.trackFormat = null;    // The selected format for track reports.
        this.taxaCount = '...';     // The count of available taxa records.
        this.taxaFormat = null;     // The selected format for taxa reports.
        this.bounds = null;         // The drawn boundary.
        this.formats = ['CSV', 'Excel', 'HTML', 'JSON', 'SQLite', 'SHP'];
        this.taxaParams = [['taxaFormat', 'format'], 'bounds'];
        this.trackParams = [['trackFormat', 'format'], 'bounds'];
    }

    reset() {
        this.trackCount = '...';
        this.taxaCount = '...';
    }

    checkTaxaReport(evt = null) {
        if(evt) evt.preventDefault();
        this.bounds = btoa(JSON.stringify(searchGeom.geometry));
        get(this.getTaxaUrl({fn: 'count'}))
        .then(result => {
            this.taxaCount = result.count || 0;
        })
        .catch(err => {
            this.taxaCount = '?';
        });
    }

    // Generate the drawing taxa report.
    generateTaxaReport(evt = null) {
        if(evt) evt.preventDefault();
        window.open(this.getTaxaUrl(), '_blank');
    }

    checkTrackReport(evt = null) {
        if(evt) evt.preventDefault();
        this.bounds = btoa(JSON.stringify(searchGeom.geometry));
        get(this.getTrackUrl({fn: 'count'}))
        .then(result => {
            this.trackCount = result.count || 0;
        })
        .catch(err => {
            this.trackCount = '?';
        });
    }

    generateTrackReport(evt) {
        if(evt) evt.preventDefault();
        window.open(this.getTrackUrl(), '_blank');
    }

    getTaxaUrl(extra_params = null) {
        let params = this.processParams(this.taxaParams, extra_params);
        params.push('type=taxa');
        params = params.join('&');
        return this.formatUrl(`${Net.API}/rov_report_data/?${params}`);
    }

    getTrackUrl(extra_params = null) {
        let params = this.processParams(this.trackParams, extra_params);
        params.push('type=dive_tracks');
        params = params.join('&');
        return this.formatUrl(`${Net.API}/rov_report_data/?${params}`);
    }

    get trackEnabled() {
        return this.trackFormat && !isNaN(this.trackCount) && this.trackCount > 0;
    }

    get taxaEnabled() {
        return this.taxaFormat && !isNaN(this.taxaCount) && this.taxaCount > 0;
    }

}

// Initialize the application.
const wp = new WaterProperties();
const wp_app = new Admin('#wp_app', {wp: wp});
wp_app.registerClass(Cruise, true, {filter: {has_water_properties: true, has_counts: false}});
wp_app.registerClass(Dive, false);

const ct = new CruiseTaxa();
const ct_app = new Admin('#ct_app', {ct: ct});
ct_app.registerClass(Cruise, true, {filter: {has_taxa: true, has_counts: false}});
ct_app.registerClass(Dive, false);

const draw = new Draw();
const draw_app = new Admin('#draw_app', {draw: draw});
draw_app.registerClass(Cruise, true, {filter: {has_counts: false}});
draw_app.registerClass(Dive, false);

const pa = new ProtectedAreas();
const pa_app = new Admin('#pa_app', {pa: pa});
pa_app.registerClass(Cruise, true, {filter: {has_counts: false}});
pa_app.registerClass(Dive, false);

// Handle the drawing event on the map.
drawmap.featureCreated = (feature) => {
    searchGeom = feature;
    draw.reset();
    draw.checkTrackReport();
    draw.checkTaxaReport();
};

// Handle the event when a protected area is clicked.
pamap.clickFn = (pa_) => {
    clickedPA = pa_;
    pa.reset();
    pa.checkTrackReport();
    pa.checkTaxaReport();
};

