import { reactive, computed } from 'vue';
import { Net } from '@/js/util.js';

/**
 * Find the parent row with the given element name of the given element.
 */
const parentRow = (el, name) => {
    while(el.nodeName.toLowerCase() != name)
        el = el.parentNode;
    return el;
};


export class LabelSearch {

    constructor() {
        this.searchTerm = null;           // The search term.
        this.searchFields = null;         // A mapping of fields that tie the buttons in the search result to the input fields' target data.
        this.searchResults = [];          // Results from the species lookup.
        this.searchFiltered = [];         // Filtered list of search results.
        this.searchTarget = null;         // The row under which search results should be shown.
        this.targetLabel = null;          // The label object that will be updated by search results.
        this.searchSources = [];          // List of datasources searched for this result.
        this.searchRanks = [];            // List of taxonomic ranks represented in this result.
        this.searchSource = "all";        // Currently selected source.
        this.searchRank = "all";          // Currently selected rank.
    }


    /**
     * Search for species from the reference tables that match the given term.
     * @param {*} targetLabel The label to update.
     * @param {*} term The search term.
     * @param {*} targetElement The DOM element that the search command event originated from.
     */
    searchSpecies(targetLabel, term, fieldMap, targetElement) {
        this.searchTerm = term;
        this.searchFields = fieldMap;
        // Clear the previous results.
        this.searchResults = null;
        this.searchFiltered = null;
        // Determine the target row for resuts.
        this.searchTarget = parentRow(targetElement, 'tr');
        // Save the label object that will be updated.
        this.targetLabel = targetLabel;
        // Get the results element.
        const res = document.querySelector('#search_results');
        this.searchTarget.parentNode.insertBefore(res, this.searchTarget.nextSibling);
        Net.get(`${Net.API}/taxon_search/?term=${term}`)
            .then(result => this.setSearchResult(result))
            .catch(alert);
    }

    /**
     * Only valid when searchSpecies has been called previously. Runs a 
     * text search using previous parameters.
     * @param {*} term 
     */
    textSearch(term) {
        this.searchTerm = term;
        // Clear the previous results.
        this.searchResults = null;
        this.searchFiltered = null;
        Net.get(`${Net.API}/taxon_search/?term=${term}`)
            .then(result => this.setSearchResult(result))
            .catch(alert);
    }

    /**
     * Update the search results and filtered results/ranks/sources from the result.
     * @param {*} result 
     */
    setSearchResult(result) {
        // Append the results to the list.
        this.searchResults = result.items;
        this.searchFiltered = this.searchResults.concat();
        this.searchSources = new Set((this.searchResults || []).filter(item => (item.source && item.source.length > 0)).map(item => item.source));
        this.searchRanks = new Set((this.searchResults || []).filter(item => (item.rank && item.rank.length > 0)).map(item => item.rank));
        this.searchSource = "all";
        this.searchRank = "all";
    }

    /**
     * Filter the search result using the drop down values for rank and source.
     */
    searchFilter() {
        const sourceSel = document.querySelector('#search_filter_source');
        const rankSel = document.querySelector('#search_filter_rank');
        const source = sourceSel.options[sourceSel.selectedIndex].value;
        const rank = rankSel.options[rankSel.selectedIndex].value;
        const fn = (item) => {
            return (
                (item != null || item.trim() != '') &&
                (rank == null || rank == 'all' || item.rank == rank) &&
                (source == null || source == 'all' || item.source == source)
            );
        };
        this.searchFiltered = this.searchResults.filter(fn);
    }

    /**
     * Set a field value from the search form.
     */
    setField(propName, value) {
        this.targetLabel.properties[propName] = value;
    }

}