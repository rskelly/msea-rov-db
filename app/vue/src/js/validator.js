export class Validator {

    /**
     * The username-validation regex.
     */    
    static USERNAME_REGEX = /^[0-9a-zA-Z][\w!@#$%^&*_+=-~]?$/;

    /**
     * The email-validation regex.
     */
    static EMAIL_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    /**
     * Check the username. The username must:
     * 1. Be a non-zero-length string.
     * 2. Contain only numbers, letters and punctuation in the range !@#$%^&*_+=-~.
     * 3. Begin with a letter or number.
     * @param {*} username 
     * @returns 
     */
    static checkUsername(username) {
        if(username == null || username.trim().length == 0)
            return 'No username given.';
        if(!Validator.USERNAME_REGEX.test(username))
            return 'A username must begin with a number or letter, and must only contain numbers, letters punctuation from the list, "!@#$%^&*_+=-~"';
        return null;
    }

    /**
     * Check the email. The email must.
     * 1. Be a non-zero-length string.
     * 2. Contain an @ symbol after the first character and before a period.
     * 3. Contain a period after the @ symbol and before the last character.
     * 4. Contain a character between the @ and a period.
     * @param {*} email 
     * @returns 
     */
    static checkEmail(email) {
        if(email == null || email.trim().length == 0)
            return 'No email given.';
        if(!Validator.EMAIL_REGEX.test(email))
            return 'A email must be in accepted format.';
        return null;
    }

    /**
     * Check the password using a variety of tests.
     * 1. Check that the password and check password are non-zero-length strings.
     * 2. Check that the password and check password are equal.
     * 3. Check that the password has at least 3 unique characters.
     * 4. Check for disallowed strings in the cannotContain list. This should include 
     *    things like the username, email and any other forbidden strings. The check
     *    is case-insensititve.
     * @param {*} password 
     * @param {*} passwordCheck 
     * @param {*} cannotContain 
     * @returns 
     */
    static checkPassword(password, passwordCheck, cannotContain) {
        // Cleanup.
        password = password == null ? password : password.trim();
        passwordCheck = passwordCheck == null ? passwordCheck : passwordCheck.trim();
        // Check length.
        if(!password || password.length < 12)
            return 'Your password must be at least 12 characters long.';
        // Match the passwords.
        if(password != passwordCheck)
            return 'Your passwords do not match.';
        // Check the number of unique characters.
        const check = {};
        for(let i = 0; i < password.length; ++i) {
            let l = password[i];
            if(!hasProp(check, l))
                check[l] = 0;
            check[l]++;
        }
        if(Object.values(check).length <= 3)
            return 'Your password has too few unique characters.';
        // Check for disallowed contents.
        const test = password.toLowerCase();
        let contains = false;
        (cannotContain || []).forEach(item => {
            if(item && test.indexOf(item.toLowerCase()) > -1)
                contains = true;
        });
        if(contains)
            return 'Your password contains disallowed text.';
        // TODO: Check most common passwords.
        return null;
    }

    /**
     * Check that the string is not empty. Return the given message if it is.
     * @param {*} value 
     * @param {*} message 
     * @returns 
     */
    static checkNotEmpty(value, message) {
        if(!message)
            throw new Error('A message is required.');
        value = value == null ? value : value.trim();
        if(value == null || value.length == 0)
            return message;
        return null;
    }

    /**
     * Check that the string is longer or equal to the minimum length and shorter than or equal
     * to the maximum length. The min and max can be disable by being set to a negative number.
     * The message is returned if the value is not valid.
     * @param {*} value 
     * @param {*} min 
     * @param {*} max 
     * @param {*} message 
     * @returns 
     */
    static checkLength(value, min, max, message) {
        if(!message)
            throw new Error('A message is required.');
        value = value == null ? value : value.trim();
        if(!value || (min >= 0 && value.length < min) || (max >= 0 && value.length > max))
            return message;
        return null;
    }

    /**
     * Check that the given value is in the list, case-sensitively. Return the message
     * if it is not.
     * @param {*} value 
     * @param {*} list 
     * @param {*} message 
     * @returns 
     */
    static checkInList(value, list, message) {
        if(!message)
            throw new Error('A message is required.');
        if(!Array.isArray(list) || list.length == 0)
            throw new Error('A non-empty list is required.');
        if(list.indexOf(value) == -1)
            return message;
        return null;
    }
}