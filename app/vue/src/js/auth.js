import { User } from '@/js/auth/models.js';
import { Net } from '@/js/util.js';

let __instance = null;
let __lastMessage = null;
let __profile = null;
let __authDetail = null;
let __authRequest = null;
let __authResolve = null;
let __authReject = null;
let __loggedIn = false;
const __groups = new Set();

/**
 * Attempt to update the groups set from the result, which should have a property, groups,
 * which is a list of groups. If there is a failure, the groups set is cleared.
 * @param {*} result 
 */
function updateGroups() {
    __groups.clear();
    try {
        __profile.groups.forEach(g => {
            __groups.add(g.name);
        })
    } catch(err) {
        __groups.clear();
    }
}

export class Auth extends EventTarget {

    /**
     * Get a reference to the singleton instance.
     */
    static get inst() {
        if(!__instance)
            __instance = new Auth();
        return __instance;
    }

    constructor() {
        super();
        // The singleton cannot be instantiated twice.
        if(__instance)
            throw new Error('Cannot instantiate Auth.');
    }

    /**
     * Return the last authentication message.
     */
    static get lastMessage() {
        return __lastMessage;
    }

    static get isLoggedIn() {
        return __loggedIn;
    }

    static get isAdmin() {
        return __groups.has('msea_admin');
    }

    static get profile() {
        return __profile;
    }

    /**
     * Returns a promise that resolves with auth: true if the user is logged in, 
     * or an auth: false and a message otherwise.
     */
    static getSession() {
        Net.get(`${Net.API}/account/session/`, null, null, false)
            .then(result => {
                // Update the state.
                __profile = new User(result.profile);
                __profile.deepDirty = false;
                __lastMessage = result.message;
                __loggedIn = result.loggedIn;
                updateGroups();
                Auth.triggerAuthUpdate();
            })
            .catch(err => { 
                __profile = null;
                __lastMessage = 'Error checking authentication status: ' + err.toString();
                __loggedIn = false;
                updateGroups();
                Auth.triggerAuthUpdate();
            });
    }

    /**
     * Register the user with the given profile which must include an email, password,
     * plus whatever else is required.
     * Resolve with register: true and a message about the verification email that will be sent.
     * On failure resolve with register: false and a message.
     * @param {*} profile 
     */
    static register(profile) {
        return new Promise((resolve, reject) => {
            Net.post(`${Net.API}/account/register/`, profile)
                .then(result => {
                    __lastMessage = result.message;
                    resolve(result);
                })
                .catch(err => {
                    __lastMessage = 'Failed to register the user: ' + err.toString();
                    resolve();
                });
        });
    }

    /**
     * Log the user in with credentials. 
     * Uses the Knox login view so the result is just an expiry and a token.
     * @param {*} email 
     * @param {*} password 
     */
    static login(email, password) {
        Net.abortAll();
        __lastMessage = null;
        __loggedIn = false;
        __profile = null;
        __groups.clear();
        Net.deleteData('token');
        Net.deleteData('token_expiry');
        return new Promise((resolve, reject) => {
            Net.post(`${Net.API}/account/login/`, {email: email, password: password})
                .then(result => {
                    // Update the token and retrieve the profile.
                    Net.setData('token', result.token);
                    Net.setData('token_expiry', result.expiry);
                    __profile = new User(result.profile);
                    __profile.deepDirty = false;
                    __loggedIn = result.token != null;
                    __lastMessage = __loggedIn ? 'Logged In' : 'Login Failed';
                    updateGroups();
                    Auth.triggerAuthUpdate();
                    resolve(result);
                })
                .catch(err => {
                    __profile = null;
                    __lastMessage = err.toString();
                    __loggedIn = false;
                    Auth.triggerAuthUpdate();
                    reject(err.toString());
                });
        });
    }

    /**
     * Log in from a form.
     * @param {*} The form. 
     */
    static formLogin(form) {
        const fd = new FormData(form);
        return Auth.login(fd.get('email'), fd.get('password'));
    }
    
    /**
     * Send the password reset email. Return a promise.
     * Deletes the user's token.
     */
    static sendPasswordReset(email) {
        Net.abortAll();
        __lastMessage = null;
        __loggedIn = false;
        __profile = null;
        __groups.clear();
        Net.deleteData('token');
        Net.deleteData('token_expiry');
        return new Promise((resolve, reject) => {
            Net.post(`${Net.API}/account/password_reset/`, {action: 'send', email: email})
                .then(result => {
                    resolve(result);
                })
                .catch(err => {
                    reject(err.toString());
                });
        });
    }
   
    /**
     * Send the password reset email from a form.
     * @param {*} The form. 
     */
    static formSendPasswordReset(form) {
        const fd = new FormData(form);
        return Auth.sendPasswordReset(fd.get('email'));
    }

    /**
     * Send the password reset email. Return a promise.
     * Deletes the user's token.
     */
    static resetPassword(password, password_confirm, code) {
        Net.abortAll();
        __lastMessage = null;
        __loggedIn = false;
        __profile = null;
        __groups.clear();
        Net.deleteData('token');
        Net.deleteData('token_expiry');
        return new Promise((resolve, reject) => {
            Net.post(`${Net.API}/account/password_reset/`, {action: 'reset', password: password, password_confirm: password_confirm, code: code})
                .then(result => {
                    resolve(result);
                })
                .catch(err => {
                    reject(err.toString());
                });
        });
    }
   
    /**
     * Reset the users password from a form.
     * @param {*} The form. 
     */
    static formResetPassword(form) {
        const fd = new FormData(form);
        return Auth.resetPassword(fd.get('password'), fd.get('password_confirm'), fd.get('code'));
    }

    /**
     * Log the user out, return a promise that resolves with false on success.
     */
    static logout() {
        Net.abortAll();
        __lastMessage = null;
        __loggedIn = false;
        __authDetail = null;
        __authRequest = null;
        __profile = null;
        __groups.clear();
        Net.post(`${Net.API}/account/logout/`)
            .then(() => {
                // There is no result.
                this.triggerAuthUpdate();
            })
            .catch(() => {
                this.triggerAuthUpdate();
            });
        Net.deleteData('token');
        Net.deleteData('token_expiry');
        Auth.triggerAuthUpdate();
    }

    /**
     * Saves the the loggen-in user's profile. Returns the object containing user data.
     */
    static saveProfile(data) {
        __lastMessage = null;
        return new Promise((resolve, reject) => {
            Net.put(`${Net.API}/account/profile/`, data)
                .then(result => {
                    __profile.update(result);
                    __profile.deepDirty = false;
                    resolve(result);
                })
                .catch(err => {
                    __lastMessage = 'Failed to save profile: ' + err.toString();
                    resolve(null);
                });
        });
    }

    /**
     * If the user is required to be logged in and is not, trigger the login form.
     */
    static triggerLogin(detail, request, resolve, reject) {
        __authDetail = detail;
        __authRequest = request;
        __authResolve = resolve;
        __authReject = reject;
        Auth.inst.dispatchEvent(new Event('triggerLogin'));
    }

    static triggerAuthUpdate() {
        if(__authRequest) {
            const x = __authRequest;
            const resolve = __authResolve;
            const reject = __authReject;
            __authDetail = null;
            __authRequest = null;
            __authResolve = null;
            __authReject = null;
            x.send(resolve, reject);
        }
        Auth.inst.dispatchEvent(new Event('authUpdate'));
    }

    /**
     * Listen for events.
     * authupdate -- When the logged-in status changes.
     * @param {*} target 
     */
    static observe(type, target) {
        Auth.inst.addEventListener(type, target);
    }

    static unobserve(type, target) {
        Auth.inst.removeEventListener(type, target);
    }
    
}
