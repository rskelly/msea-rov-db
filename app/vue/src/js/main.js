let cursection = null;

/**
 * Hide any open sections and open the named section.
 */
function section(name, evt) {
	if(evt) evt.preventDefault();

    // Hide all the sections. Set all the headers to +.
	let secs = document.querySelectorAll('.section');
    let heads = document.querySelectorAll('.header');
	secs.forEach(sec => { sec.style.display = 'none'; });
    heads.forEach(head => { 
        let sp = head.querySelector('span');
        sp.innerText = '+'; 
    });

    // Get the targets section.
    let sec = document.querySelector(`#section_${name}`);
    if(!sec)
        return;
    
    // If the name is the same as the current section, we're closing. Find the next name
    // up the hierarchy and call again with that name (or empty string). 
    if(name == cursection) {
        let sec0 = sec.parentElement;
        while(sec0) {
            if(sec0.classList.contains('section')) {
                name = sec0.id.substring('section_'.length);
                sec = sec0;
                break;
            }
            sec0 = sec0.parentElement;
        }
    }

    // Crawl up the chain starting with the target segemtn and open each one.
    while(sec) {
        if(sec.classList.contains('section')) {
            sec.style.display = null;
            let n = sec.id.substring('section_'.length);
            let head = document.querySelector(`#header_${n}`);
            if(head) {
                let sp = head.querySelector('span');
                if(sp)
                    sp.innerText = '-';
            }
        }
        sec = sec.parentElement;
    }

    //let head = document.querySelector(`header_${name}`)
    hashAdd('section', name);
	cursection = name;
}

function addModel(evt) {
	if(evt) evt.preventDefault();
	document.location.replace(`?add=model#${hashStr()}`)
}

function editModel(id, evt) {
	if(evt) evt.preventDefault();
	document.location = `?edit=model&id=${id}#${hashStr()}`;
}

function editCancel(evt) {
	if(evt) evt.preventDefault();
	document.location.replace(`?#${hashStr()}`);
}

/**
 * Save the entity represented in the form that owns the button that triggers this function.
 * model - The model name to save (passed into the view so it can decide)
 * evt - The button click event.
 * query - The name of the element that contains relevant form fields.
 */
function saveEntity(model, evt, query = 'tbody') {
	if(evt) evt.preventDefault();
	let form = new FormData();
	form.set('action', 'save');
	form.set('model', model);
	let tbody = parent(evt.target, query);
    	tbody.querySelectorAll('input,textarea,select').forEach(item => { 
	        if(item.type == 'checkbox')
        		item.value = item.checked.toString();
        	if(item.type == 'file') {
        		for(let i = 0; i < item.files.length; ++i)
        			form.append(item.name, item.files[i], item.files[i].name);
        	} else {
        		form.append(item.name, item.value);
        	}
    	});
    post(document.location.href, form, false)
        .then(() => {
            document.location.reload(true);
        })
        .catch(err => {
            alert(err);
        });
}

/**
 * Copy an entity and reload the page.
 */
function copyEntity(model, id, evt) {
	if(evt) evt.preventDefault();
    let tbody = parent(evt.target, 'tbody');
    let form = new FormData();
    form.set('action', 'copy');
    form.set('model', model);
    form.set('id', id);
    // Send the request.
    post(document.location.href, form, false)
        .then(() => {
            document.location.reload(true);
        })
        .catch(err => {
            alert(err);
        });
}

/**
 * Delete the entity.
 */
function deleteEntity(model, id, evt) {
	if(evt) evt.preventDefault();
	if(confirm('Are you sure?')) {
        let tbody = parent(evt.target, 'tbody');
        let form = new FormData();
        form.set('action', 'delete');
        form.set('model', model);
        form.set('id', id);
        // Send the request.
        post(document.location.href, form, false)
            .then(() => {
                document.location.reload(true);
            })
            .catch(err => {
                alert(err);
            });
    }
}

/**
 * Delete the selected entity.
 */
function deleteSelected(model, evt) {
	if(evt) evt.preventDefault();
	if(confirm('Are you sure?')) {
		// Get any checkbox in the form called 'delete'.
		let dc = evt.target.form.elements['delete'];
		// Populate the form with the action and model and all IDs.
        let form = new FormData();
        form.set('action', 'delete');
	    form.set('model', model);
        for(let i = 0; i < dc.length; ++i) {
        	if(dc[i].checked == true)
        		form.append('id', dc[i].value);
        }
        // Send the delete request.
        post(document.location.href, form, false)
            .then(() => {
                document.location.reload(true);
            })
            .catch(err => {
                alert(err);
            });
    }
}

let listId = 0;

/**
 * Add a row to the HTML list.
 */
function addRow(id, evt) {
	evt.preventDefault();
	let row = evt.target;
	while(row.nodeName.toLowerCase() != 'tfoot')
		row = row.parentElement;
	let newrow = document.querySelector(id).content.cloneNode(true).firstChild;
    while(newrow.nodeType != 1)
        newrow = newrow.nextSibling;

    // Set the IDs on the input/datalist pair if they exist.
    let dl = newrow.querySelectorAll("datalist[class='_list_']");
   	let di = newrow.querySelectorAll("input[class='_list_']");
    for(let i = 0; i < dl.length; ++i) {
    	let id = `datalist_${++listId}`;
    	dl[i].id = id;
    	di[i].setAttribute('list', id);
    }

	row.parentElement.insertBefore(newrow, row);
}

/**
 * If called from within a table row, removes the row from its parent.
 */
function removeRow(evt) {
    evt.preventDefault();
    let row = evt.target;
    while(row.nodeName.toLowerCase() != 'tbody')
        row = row.parentElement;
    row.parentElement.removeChild(row);
}

/**
 * Called when a choices field is updated.
 */
function updateChoices(evt) {
	let input = evt.target.parentElement.querySelector("input[type='hidden']");
	let json = JSON.stringify(evt.target.value.split('\n'));
	if(input.value != json)
		input.value = json;
}

/**
 * Renders the choices list into the visible text field.
 */
function renderChoices(evt) {
	let input = evt.target.parentElement.querySelector("input[name='choices_render']");
	try {
		let json = JSON.parse(evt.target.value).join('\n');
		if(input.value != json)
			input.value = json;
	} catch(e) {
        console.log(e);
    }
}

/**
 * Parse the displayed array value and set the value.
 */
function updateArray(evt) {
	let inputs = evt.target.parentElement.querySelectorAll("input:not([name='render'])");
	let values = evt.target.value.split(',');
	for(let i = 0; i < values.length; ++i)
		inputs[i].value = isNaN(parseFloat(values[i])) ? '' : parseFloat(values[i]);
}

/**
 * Format the array value for display.
 */
function renderArray(evt) {
	let lst = evt.target.parentElement.querySelectorAll("input:not([name='render'])")
	let input = evt.target.parentElement.querySelector("input[name='render']");
	let d = [];
	for(let i = 0; i < lst.length; ++i)
		d.push(parseFloat(lst[i].value));
	input.value = d.join(',');
}

let tabPanels = [];

/**
 * A tab panel based on the given element.
 * Any h1 element is treated as a tab label. Any other content
 * is treated as a tab panel. The number of h1 and content element
 * must be equal.
 */
class TabPanel {

    /**
     * Create the tab panel with the given root element and the panel's
     * index of all tab panel's on the page.
     */
    constructor(el, tpidx) {
        this.tpindex = tpidx;
        this.index = 0;
        this.el = el;
        this.tabs = el.querySelectorAll('.tab_panel > h1');
        this.panels = el.querySelectorAll('.tab_panel > *:not(h1)');
        for(let i = 0; i < this.tabs.length; ++i) {
            this.tabs[i].dataset.index = i;
            this.tabs[i].addEventListener('click', this.tabClick.bind(this));
            if(i > 0) {
                this.el.removeChild(this.panels[i]);
            } else {
                this.tabs[i].classList.add('on');                
            }
        }
        // Set the focused tab from the URL hash.
        let h = hash();
        let k = `tab${this.tpindex}`;
        if(h.has(k))
            this.tabs[parseFloat(h[k])].click();
    }

    tabClick(evt) {
        evt.preventDefault();
        let newIndex = evt.target.dataset.index;
        let oldIndex = this.index;
        this.tabs[oldIndex].classList.remove('on');
        this.el.removeChild(this.panels[oldIndex]);
        this.tabs[newIndex].classList.add('on');
        this.el.appendChild(this.panels[newIndex]);
        this.index = newIndex;
        this.updateAnchor(oldIndex, newIndex);
    }

    updateAnchor(oldIndex, newIndex) {
        let oldName = `tab${this.tpindex}=${oldIndex}`;
        let newName = `tab${this.tpindex}=${newIndex}`;
        hashRemove(oldName);
        hashAdd(newName);
    }

}

/**
 * Initialize tab panels from all elements with the class name tab_panel.
 */
function initTabPanels() {
    let panels = document.querySelectorAll('.tab_panel');
    for(let i = 0; i < panels.length; ++i)
        tabPanels.push(new TabPanel(panels[i], i));
}

/**
 * Toggles the visibility of a div that is a child of the clicked
 * element's parent. This is for toggling the sections in the main menu.
 */
function menuSection(evt) {
    const a = evt.target;
    const d = parent(a, 'section').querySelector('div');
    let s = localStorage.getItem('menu_toggle');
    try {
        s = JSON.parse(s);
    } catch(err) {
        console.log(err.toString());
    }
    if(!s || !(s instanceof Object))
        s = {};
    if(d.style.display == 'block') {
        delete s[a.innerText];
        d.style.display = 'none';
    } else {
        s[a.innerText] = true;
        d.style.display = 'block';
    }
    localStorage.setItem('menu_toggle', JSON.stringify(s));
}

/**
 * Toggles menu sections that were open on previous load.
 */
function initMenu() {
    const m = Array.from(document.querySelectorAll('#menu a'));
    let s = localStorage.getItem('menu_toggle');
    try {
        s = JSON.parse(s);
    } catch(err) {
        console.log(err.toString());
    }
    if(!s || !(s instanceof Object))
        s = {};
    for(let [k, v] of Object.entries(s)) {
        const a = m.find(e => e.textContent == k);
        if(a) {
            a.click();
        } else {
            delete s[k];
        }
    }
    localStorage.setItem('menu_toggle', JSON.stringify(s));
}


function start(evt) {

    initTabPanels();

    // Hide subsections. Then, if there's a section name in the anchor text, show it.
    section(hash().get('section'), evt);

	// Update the choice fields.
	document.querySelectorAll('input[name$=".choices"]').forEach(el => { 
		el.dispatchEvent(new Event('change')) 
	});

    // Reset the expanded menu items.
    initMenu();
}

let currentFileMeta = null;
let currentFileMetaLink = null;

function showFileMetadata(evt) {
    evt.preventDefault();
    let p = evt.target.parentElement;
    let t = p.querySelector('.filemeta');
    if(currentFileMeta) {
        currentFileMeta.style.display = 'none';
        currentFileMetaLink.textContent = '[Show Meta]';
    }
    if(t) {
        if(t != currentFileMeta) {
            t.style.display = 'table';
            currentFileMeta = t;
            currentFileMetaLink = evt.target;
            currentFileMetaLink.textContent = '[Hide Meta]';
        } else {
            currentFileMeta = null;
        }
    }
}