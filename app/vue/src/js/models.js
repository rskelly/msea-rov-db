import { Net, hasProp } from "@/js/util.js";

/**
 * Used to generate negative IDs for multiple instances of a single class
 * that must not conflict but are not saved (so have no real IDs). On save
 * the IDs are nulled so the service will save them.
 */
let __default_id = 0;

function pad2(v) {
    return new String(v).padStart(2, '0');
}

/**
 * Compare two objects using the list of given fields, in order.
 * Returns a comparator that checks the fields recursively.
 * @param {*} fields A list of fields to check.
 * @returns A comparator functor.
 */
export function comp(fields) {
    if(!(typeof(fields) == 'array'))
        fields = [fields];
    return (a, b) => {
        let vals = fields.map(field => {
            if(a[field] < b[field]) {
                return -1;
            } else if(a[field] > b[field]) {
                return 1;
            } else {
                return 0;
            }
        });
        let v;
        while((v = vals.pop()) == 0);
        return v;
    };
};

/**
 * A base class for persistable objects which are created in the client.
 */
class Model {

    // Property of the Model class only.
    static __registry = new Map();	// A registry of all instances. Used to prevent multiples of the same entity.
                                    // When an object is created, it has ID=0. When it is saved, its entry in the
                                    // registry is replaced by the valid object. There is currently no solution
                                    // for multiple instance of the same class with the same ID of 0.

    // Properties on each subclass.
    static modelPath = null;		// Required: The ReST enpoint paths. A string, or a function with one parameter
                                    // (a model instance) that will return the correct path.
    static defaultProps = {};		// Required: Default properties of the object. Sets default values and a guide
                                    // for accessor creation.
    static excludeProps = [];       // A list of property names that are not persisted, but are configured on 
                                    // instances so that they may be made reactive, etc.
    static classMap = {};		    // Optional: Maps properties to classes, to allow update and accessors to resolve class
                                    // instances.

    static jsonFields = [];         // A list of fields that will be copied from the _props object to a serializable return value.
                                    // If this is empty or null, all fields that do not begin with underscore are copied.

    static preUpdate = null;        // A function that, if set, is applied to a load/save result.

    static isValidDateTime(v) {
        try {
            new Date(v);
            return true;
        } catch(err) {}
        return false;
    }

    static isValidEmail(v) {
        return v.indexOf('@') > 0; // TODO: Make better.
    }

    /**
     * Register the class by name. Must be called after every class declaration (hack).
     */
    static registerClass(cls, name) {
        if(!cls || !name)
            throw new Error('A class and name are required.');

        let pkg = window;

        if(name.indexOf('.') > -1) {
            // If this is a dot-delimited class path, create 'packages' on the window object.
            const names = name.split('.');
            for(let i = 0; i < names.length - 1; ++i) {
                if(!hasProp(pkg, names[i]))
                    pkg[names[i]] = {};
                pkg = pkg[names[i]];
            }
            name = names[names.length - 1];
        }
        
        if(hasProp(pkg, name)) {
            console.warn(`There is already a registered global entity called ${name}.`);
        } else {
            pkg[name] = cls;
        }
    }

    /**
     * Return an instance of the named class.
     */
    static buildClass(name) {
        if(!hasProp(window, name))
            throw new Error(`No class with the name ${name} found.`);
        return new window[name]();
    }

    /**
     * Construct the model with the initial properties.
     */
    constructor(props = null) {
        this._dirty = false;
        this._active = false;
        this._props = Object.assign({}, structuredClone(this.constructor.defaultProps));
        this._exclude = new Set(this.constructor.excludeProps || []);
        this.buildAccessors();
        this.update(props);
        if(!props || props.id == 0)
            this.id = __default_id--;
    }

    /**
     * Standard JSON serializer function.
     * @returns 
     */
    toJSON() {
        const result = {};
        if(this.constructor.jsonFields && this.constructor.jsonFields.length > 0) {
            this.constructor.jsonFields.forEach(k => {
                result[k] = this._props[k];
            });
        } else {
            for(let [k, v] of Object.entries(this._props)) {
                if(!k.startsWith('_') && !this._exclude.has(k))
                    result[k] = v;
            }
        }
        return result;
    }

    /**
     * Reset the instance's properties.
     * If the property is an array, object, set or map, will be cleared.
     * If the property is a non-null instance of Model, will be reset.
     * Other properties are set to their initial values.
     */
    reset() {
        const _reset = (item, k, v) => {
            if(typeof(v) == 'number' || typeof(v) == 'string' || v instanceof String || 
                    v instanceof Number || v == null || v === undefined) {
                if(item instanceof Model) {
                    item[k] = item.constructor.defaultProps[k];
                } else {
                    item[k] = null;
                }
            } else if(Array.isArray(v)) {
                v.splice(0);
            } else if(v instanceof Set || v instanceof Map) {
                v.clear();
            } else if(v instanceof Model) {
                v.reset();
            } else {
                for(let [k0, v0] of Object.entries(v))
                    _reset(v, k0, v0);
            }
        };
        for(let [k, v] of Object.entries(this.constructor.defaultProps))
            _reset(this, k, v);
        this.id = __default_id--;
    }

    /**
     * Create accessors for each entry in the default props.
     * ignore accessors that already exist on the protototype.
     */
    buildAccessors() {
        const cls = this.constructor;
        const proto = Object.getPrototypeOf(this);
        for(let k of Object.keys(cls.defaultProps)) {
            const d = this.getProperty(proto, k);
            if(!d) {
                try {
                    // Create a property on this instance's prototype with name k.
                    Object.defineProperty(proto, k, {
                        get: function() {
                            return this.get_prop(k);
                        },
                        set: function(v) {
                            this.set_prop(k, v);
                        },
                        enumerable: true,
                        configurable: false
                    });
                } catch(e) {}
            } else {
                d.enumerable = true;
            }
        }
    }

    /**
     * Get the property descriptor from the object's prototype chain.
     */
    getProperty(obj, name) {
        if(obj) {
            const d = Object.getOwnPropertyDescriptor(obj, name);
            if(d !== undefined) {
                return d;
            } else {
                return this.getProperty(Object.getPrototypeOf(obj), name);
            }
        }
        return null;
    }

    /**
     * Get the names of getters on both the prototype and the object itself.
     */
    get getterNames() {
        let names = [];
        for(let [k, v] of Object.entries(Object.getOwnPropertyDescriptors(Object.getPrototypeOf(this)))) {
            if(v.get)
                names.push(k);
        }
        for(let [k, v] of Object.entries(Object.getOwnPropertyDescriptors(this))) {
            if(v.get)
                names.push(k);
        }
        return names;
    }

    /**
     * Given a date, or a date string, return the ISO representation for date inputs.
     * If it's null or invalid, return null.
     */
    static toISODateTimeString(date) {
        if(!date)
            return null;
        if(!(date instanceof Date))
            date = new Date(date);
        return `${date.getUTCFullYear()}-${pad2(date.getUTCMonth())}-${pad2(date.getUTCDate())}T${pad2(date.getUTCHours())}:${pad2(date.getUTCMinutes())}:${pad2(date.getUTCSeconds())}Z`;
    }

    static toISOTimeString(date) {
        if(!date)
            return null;
        if(!(date instanceof Date))
            date = new Date(date);
        return `${pad2(date.getUTCHour())}:${pad2(date.getUTCMinute())}:${pad2(date.getUTCSecond())}`;
    }

    static toISODateString(date) {
        if(!date)
            return null;
        if(!(date instanceof Date))
            date = new Date(date);
        return `${date.getUTCFullYear()}-${pad2(date.getUTCMonth())}-${pad2(date.getUTCDate())}`;
    }

    /**
     * Return the model path from the class or instance. If it's
     * configured as a string, return it. If it's a function, call it
     * with the params. Adds the query string in either case, if one is configured.
     */
    static getModelPath(clsOrInstance, params) {
        let cls = null;
        let inst = null;
        if(clsOrInstance instanceof Model) {
            cls = clsOrInstance.constructor;
            inst = clsOrInstance;
        } else {
            cls = clsOrInstance;
        }

        // Make sure there's a params, and that it has the right properties.
        if(!params)
            params = {};
        if(!hasProp(params, 'models'))
            params.models = [];

        // Assemble a query string if needed.
        let q = params.filter
            ? '?' + Object.entries(params.filter || []).map(item => `${item[0]}=${item[1]}`).join('&')
            : '';

        // Configure the paths.
        let path = null;
        if(cls.modelPath instanceof Function) {
            path = cls.modelPath(params);
        } else {
            path = cls.modelPath;
        }
        return [path, q];
    }

    /**
     * Update the model with the given properties object.
     *
     * The dirty property is automatically set to true and must be set to
     * false when the object is refreshed, loaded from the server or saved.
     *
     * If clean is true, set dirty to false (including any child objects in subclasses).
     *
     * If a property is set to an integer, but the class map indicates that it
     * has an associated class, and the new props have an object, the object will
     * replace the integer (assumed to be an ID).
     */
    update(props) {
        if(props) {
            // Resolve the class mappings.
            const cm = this.constructor.classMap || {};
            // Iterate over the properties entries.
            for(let [k, v] of Object.entries(props)) {
                if(hasProp(cm, k)) {
                    // If the class map contains the property, eval the class name to get the definition.
                    const cls = window[cm[k]];
                    if(typeof(v) == 'number' && this[k] instanceof Object) {
                        // If the new object has an ID (is a shallow serializer), replace
                        // with the object from the current class. 
                        // TODO: This doesn't allow changes of the property.
                        props[k] = this[k];
                    } else if(Array.isArray(v)) {
                        // If the value is an array, convert each element.
                        for(let i = 0; i < v.length; ++i) {
                            // If the element is not a model instance (yet), convert it.
                            if(v[i] && !(v[i] instanceof Model))
                                v[i] = Model.resolve(v[i], cls);
                        }
                    } else if(v && !(v instanceof Model)) {
                        // If the value is not a model instance (yet), convert it.
                        props[k] = Model.resolve(v, cls);
                    }
                }
            }
            // Apply the post-update if there is one.
            if(this.constructor.preUpdate)
                props = this.constructor.preUpdate(props);
            // Overwrite the default properties with the new props object.
            this._props = Object.assign(this._props, props);
            // Default state is dirty. Up to caller to clean it.
            this.dirty = true;
        }
        // (Re)register the object.
        Model.register(this);
    }

    /**
     * Returns true if the instance is valid for saving to the database.
     * The default implementation returns true.
     * @returns 
     */
    get isValid() {
        return true;
    }

    /**
     * Return true if the instance is involved in network activity.
     */
    get isActive() {
        return this._active;
    }

    /**
     * Create a deep clone of the instance.
     * Properties are:
     * - cloned if they're instances of Model.
     * - copied, and their elements cloned, if they're an array, object, map or set.
     * - otherwise just assigned (copied).
     * @param idGenerator A callable that generates IDs for the cloned object and model properties. The default \
     *  returns null, which uses the default. The single idGenerator parameter is an instance. Generators \
     *  can check the instance to see if it should get an ID.
     * @returns A deep clone of the current instance. All of the properties are also cloned.
     */
    clone(idGenerator=() => __default_id--) {
        const _clone = (item) => {
            if(typeof(item) == 'boolean' || item === true || item === false) {
                // Boolean: just return it.
                return item;
            } else if(typeof(item) == 'number' || typeof(item) == 'string' || item instanceof Number || 
                    item instanceof String || item == null || item == undefined) {
                // String, number or undefined: just return it.
                return item;
            } else if(item instanceof Model) {
                // Instance of a Model: just return it. Don't clone models; they are already registered.
                return item.clone(idGenerator);
            } else if(Array.isArray(item)) {
                // Array: recursive clone on each item.
                return item.map(item => _clone(item));
            } else if(item instanceof Set) {
                // Set: recurse as if array, then reconstitute as new set.
                return new Set(Array.from(item).map(item => _clone(item)));
            } else if(item instanceof Map) {
                // Map: recurse array of items, then reconstitute as map.
                return new Map(Array.from(item.entries()).map((k, v) => {k: _clone(v)}));
            } else if(item instanceof File) {
                return new File([item], item.name, {
                    type: item.type,
                    lastModified: item.lastModified,
                });
            } else {
                // Plain old object: create new and recurse properties.
                const out = {};
                for(let [k, v] of Object.entries(item))
                    out[k] = _clone(v);
                return out;
            }
        }
        // Get the class and create an instance.
        const cls = this.constructor;
        const c = new cls();
        // Create a clone of the current object's properties.
        const p = _clone(Object.assign({}, this._props));
        // Update the new instance with the properties.
        c.update(p);
        // Nullify the ID to give the object a new yet-to-be assigned identity, or let the generator assign it.
        c.id = idGenerator(c);
        return c;
    }

    /**
     * Recursive implementation of clean.
     * @param {*} visited 
     * @param {*} level 
     * @returns 
     */
    _clean(visited = new Map(), level = 0) {
        if(visited.has(this)) {
            // If this object has already been cleaned, don't clean it again, just return it.
            const v = visited.get(this);
            return v ? v.value : null;
        }
        // Set the item as visited, and the level.
        visited.set(this, {level: level, value: this});
        // Get the cloned properties object to clean.
        let props = Object.assign({}, this._props);
        for(let [k, v] of Object.entries(props)) {
            if(!this._exclude.has(k)) {
                // Only include a property if it isn't in the exclude list.
                if(v instanceof Model) {
                    // Clean a model instance.
                    props[k] = v._clean(visited, level + 1);
                } else if(Array.isArray(v)) {
                    // Clean the elements an array that are model instances..
                    for(let i = 0; i < v.length; ++i) {
                        if(v[i] && v[i] instanceof Model)
                            props[k][i] = v[i]._clean(visited, level + 1);
                    }
                } else if(k == 'id' && parseInt(v) <= 0) {
                    // Zero or lower IDs are removed for creation on the database side.
                    delete props[k];
                }
            }
        }
        return props;
    }

    /**
     * Return a "clean" representation of the object graph:
     * the props object is elevated, and child instances of Model
     * are cleaned recursively.
     */
    clean() {
        return this._clean();
    }

    /**
     * Returns true if the object or any of its Model-derived
     * children are dirty.
     */
    get dirty() {
        if(this._dirty)
            return true;
        const visited = new Set();
        const result = {dirty: false};
        const dd = (obj, visited, result) => {
            if(obj && !visited.has(obj) && obj instanceof Model) {
                visited.add(obj);
                if(obj._dirty) {
                    result.dirty = true;
                    return;
                }
                for(let k of obj.getterNames) {
                    if(Array.isArray(obj[k])) {
                        obj[k].forEach(v_ => dd(v_, visited, result));
                    } else {
                        dd(obj[k], visited, result);
                    }
                }
            }
        }
        dd(this, visited, result);
        return result.dirty;
    }

    /**
     * Set the instance ID. If it changes, reregister the instance with the
     * new ID.
     */
    set id(i) {
        if(i != this._props.id) {
            Model.unregister(this);
            this._props.id = i;
            Model.register(this);
        }
    }

    /**
     * Get the isntance ID.
     */
    get id() {
        return this._props.id;
    }

    /**
     * Set the dirty state of this object. Does not affect children.
     */
    set dirty(v) {
        this._dirty = v;
    }

    /**
     * Set the dirty state on the object and all of its Model-derived
     * children, recursively. An internal set guarantees that infinite
     * recursion will be avoided.
     */
    set deepDirty(d) {
        const visited = new Set();
        const dd = (obj, visited) => {
            if(obj && !visited.has(obj) && obj instanceof Model) {
                obj._dirty = d;
                visited.add(obj);
                for(let k of obj.getterNames) {
                    if(Array.isArray(obj[k])) {
                        obj[k].forEach(v_ => dd(v_, visited));
                    } else {
                        dd(obj[k], visited);
                    }
                }
            }
        }
        dd(this, visited);
    }

    /**
     * Set the property. If it differs from the current value (or there isn't)
     * one, the dirty flag is set.
     */
    set_prop(prop, v) {
        if(prop == 'props')
            throw new Error('Something is wrong: property named "props" is not allowed.');
        if(!hasProp(this._props, prop) || this._props[prop] != v)
            this.dirty = true;
        this._props[prop] = v;
    }

    /**
     * Return the property.
     */
    get_prop(prop) {
        return this._props[prop];
    }

    /**
     * Convert empty strings to nulls. 
     */
    cleanNulls() {
        for(let [k, v] of Object.entries(this._props)) {
            try {
                if(typeof v === 'string' && v.trim() == '')
                    this._props[k] = null;
            } catch(err) {}
        }
    }

    /**
    * Create or save the object.
    * If pathParam is given, will be passed to the modelPath function as
    * a discriminator (if implemented).
    */
    save(params = null) {
        
        this._active = true;

        this.cleanNulls();

        let formData = null;
        if(params instanceof FormData) {
            formData = new FormData();
            for(let [k, v] of params.entries())
                formData.append(k, v);
        } else if(params && hasProp(params, 'formData')) {
            formData = new FormData();
            for(let [k, v] of params.formData.entries())
                formData.append(k, v);
        }

        const t = this;
        // Get the model path.
        const [path, query] = Model.getModelPath(this, params);
        return new Promise((resolve, reject) => {
            if(parseInt(t.id) > 0) {
                // If the ID is > 0 (the object exists in the DB), put it.
                Net.put(`${path}/${t.id}/${query}`, formData || t.clean())
                    .then(result => {
                        t.update(result);
                        t.deepDirty = false;
                        t._active = false;
                        resolve(t);
                    })
                    .catch(error => {
                        t._active = false;
                        reject(error);
                    });
            } else {
                // Otherwise post the new object.
                Net.post(`${path}/${query}`, formData || t.clean())
                    .then(result => {
                        if(t.id <= 0)
                            Model.unregister(t); // Unregister the un-saved object.
                        t.update(result);
                        t.deepDirty = false;
                        t._active = false;
                        resolve(t);
                    })
                    .catch(error => {
                        t._active = false;
                        reject(error);
                    });
            }
        });
    }

    /**
    * Load the current state of theobject from the server.
    * If pathParam is given, will be passed to the modelPath function as
    * a discriminator (if implemented).
    */
    refresh(params = null) {

        this._active = true;

        const t = this;
        // Get the model path.
        const [path, query] = Model.getModelPath(this, params);
        return new Promise((resolve, reject) => {
            if(parseInt(t.id) > 0) {
                // If the ID is valid, re-load the entity.
                Net.get(`${path}/${t.id}/${query}`)
                    .then(result => {
                        t.update(result);
                        t.deepDirty = false;
                        t._active = false;
                        resolve(t);
                    })
                    .catch(error => {
                        t._active = false;
                        reject(error);
                    });
            } else {
                // The object isn't refreshable. Just resolve.
                t._active = false;
                resolve(t);
            }
        });
    }

    /**
    * Delete the object.
    * If pathParam is given, will be passed to the modelPath function as
    * a discriminator (if implemented).
    */
    destroy(params = null) {

        this._active = true;

        const t = this;
        // Get the model path.
        const [path, query] = Model.getModelPath(this, params);
        return new Promise((resolve, reject) => {
            if(parseInt(t.id) > 0) {
                // Delete the object if the ID is valid.
                Net.del(`${path}/${t.id}/${query}`)
                    .then(result => {
                        Model.unregister(t);
                        t._active = false;
                        resolve(result);
                    })
                    .catch(error => {
                        t._active = false;
                        reject(error);
                    });
            } else {
                t._active = false;
                // Just resolve the object so it can be removed from in-memory.
                resolve({result: true}); //error: `Object has invalid ID: ${t.id}.`});
            }
        });
    }

    /**
     * Replace each non-model element in the array with an instance of the given class.
     */
    static fromArray(a, cls) {
        if(!cls)
            throw new Error('A class is required for fromArray.');
        const o = [];
        if(a) {
            a.forEach(e => {
                if(e)
                    o.push(Model.resolve(e, cls));
            });
        }
        return o;
    }

    /**
     * Load all of the entities from the server.
     * TODO: Paginate.
     */
    static loadAll(cls, params = null) {
        const [path, query] = Model.getModelPath(cls, params);
        return new Promise((resolve, reject) => {
            Net.get(`${path}/${query}`)
                .then(result => {
                    const lst = cls.fromArray(result, cls, true);
                    if(result.pagination)
                        lst.pagination = result.pagination;
                    lst.forEach(e => { e.deepDirty = false });
                    resolve(lst);
                })
                .catch(error => {
                    reject(error.error || error);
                });
        });
    }

    static loadOne(cls, pk) {
        const [path, query] = Model.getModelPath(cls);
        return new Promise((resolve, reject) => {
            Net.get(`${path}/${pk}/${query}`)
                .then(result => {
                    const item = cls.fromArray([result], cls, true)[0];
                    item.deepDirty = false;
                    resolve(item);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    static formatQuery(query) {
        if(query) {
            return '?' + Object.entries(query).map(item => `${item[0]}=${item[1]}`).join('&');
        } else {
            return '';
        }
    }

    /**
     * Find the entity in the registry by ID. 
     * If cls is an object, the name property is read; if it is a string it is assumed
     * to be the classname. The ID is the ID of the entity.
     */
    static find(id, cls) {
        if(!cls)
            throw new Error('No class or class name was given. Did you call Model.registerClass?');
        const clsName = typeof(cls) == 'string' ? cls : cls.name;
        const m = Model.__registry.get(clsName, null);
        if(m)
            return m.get(id, null);
    }

    asJson() {
        return JSON.stringify(this.asObject());
    }

    asObject() {
        let props = {};
        for(let [k, v] of Object.entries(this._props))
            props[k] = Model.toObject(v);
        props._cls = this.constructor.name;
        return props;
    }

    static toObject(input) {
        let result = null;
        if(Array.isArray(input) || input instanceof Set) {
            result = Array.from(input).map(item => { Model.toJson(item) });
        } else if(input instanceof Model) {
            result = input.asJson();
        } else if(input instanceof Object || input instanceof Map) {
            result = {};
            for(let [k, v] of Object.entries(input))
                result[k] = Model.toJson(v);
        } else {
            result = input;
        }
        return result;
    }

    static toJson(v) {
        return JSON.stringify(v);
    }

    static fromObject(obj) {
        let result = null;
        if(Array.isArray(obj)) {
            result = obj.map(item => { Model.fromObject(item) });
        } else if(hasProp(obj, '_cls')) {
            result = Model.resolve(obj, obj._cls);
        } else if(obj instanceof Object) {
            result = {};
            for(let [k, v] of obj)
                result[k] = Model.fromObject(v);
        }
        return result;
    }

    static fromJson(input) {
        let obj = null;
        try {
            obj = JSON.parse(input);
        } catch(err) {
            console.debug(err);
        }
        return obj;
    }
    
    /**
     * Find the registered entity, or create and register if it it doesn't exist.'
     */
    static resolve(data, cls) {
        if(data == null)
            return null;
        if(typeof(data) == 'number')
            return data; // Probably an ID.
        if(hasProp(data, '_props'))
            data = data._props;
        let inst = Model.find(data.id, cls);
        if(!inst) {
            inst = new cls(data);
        } else {
            // Update the stored values with new ones.
            inst.update(data);
        }
        return inst;
    }

    /**
     * Register the entity.
     */
    static register(inst) {
        if(!('id' in inst))
            throw Error('The instance has no "id" property. It should be present in default props, or as an accessor.');
        let m = Model.__registry.get(inst.constructor.name);
        if(!m) {
            m = new Map();
            Model.__registry.set(inst.constructor.name, m);
        }
        m.set(inst.id, inst);
    }

    /**
     * Remove the entity from the registry.
     */
    static unregister(inst) {
        const m = Model.__registry.get(inst.constructor.name, null);
        if(m)
            m.delete(inst.id);
    }
}


const global = window || global;
global.Model = Model;

export { Model };

/**
 * Update the array in-place with a list of model instances s by removing the 
 * missing objects and adding the new objects. If cmp is given, will sort the array.
 * @param {*} models 
 */
const modelUpdate = (current, input, cmp=null) => {
    current.splice(0);
    current.push(...input);
};

global.modelUpdate = modelUpdate;

export { modelUpdate };
