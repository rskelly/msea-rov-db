import tinycolor from 'tinycolor2';
import { Auth } from '@/js/auth.js';

/**
 * Return true if the property is in the object. Return false if the object
 * is not an object (i.e., a string or number) or does not contain the property.
 * @param {*} obj 
 * @param {*} prop 
 * @returns 
 */
export function hasProp(obj, prop) {
    try {
        if(typeof(obj) == 'string' || typeof(obj) == 'number')
            return false;
        return (prop in obj);
    } catch(err) {
        return false;
    }
}

/**
 * Get the parent (or current) node with the given name.
 * n - The node to start with.
 * name - The name of the element to stop and return.
 */
export function parent(n, name) {
	while(n && n.nodeName.toLowerCase() != name.toLowerCase())
		n = n.parentElement;
	return n;
}

/**
 * Remove the children of the given node.
 * 
 * If only is given, it's the name of elements to *remove*.
 */
export function clear(n, only = null) {
    if(n) {
        let c_, c = n.firstChild;
        while(c) {
            c_ = c.nextSibling;
            if(!only || only.toLowerCase() == c.nodeName.toLowerCase())
                n.removeChild(c);
            c = c_;
        }
    }
    return n;
}

export const md5 = (d) => {
    var r = M(V(Y(X(d),8*d.length)));return r.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_
};

/**
 * Extract a colour from the given string by hashing it and taking the first 6 characters.
 * The bright and opacity parameters affect the brightness and opacity,
 * respectively. Bright lightens from 0-100% if positive, darkens if negative.
 * Opacity just sets the opacity.
 * @param {*} str 
 */
export const strToColour = (str, lighten=0.0, alpha=1.0) => {
    let col = tinycolor('#' + md5(str).substring(0, 6));
    return col.lighten(lighten).setAlpha(alpha).toHex8String();
}

/**
 * A Promise that provides an internal XHR object that can be cancelled.
 */
class XHRPromise extends Promise {

    constructor(callback, request) {
        super(callback);
        this.request = request;
    }

    abort() {
        try {
            this.request.abort();
        } catch(err) {}
    }

}

const __reqs = new Set();

/**
 * Return true if the token is valid.
 * @param {*} t 
 * @returns 
 */
function isValidToken(t) {
    return t != null && t != undefined && t != "undefined";
}

class XHRRequest {

    /**
     * Make a request.
     * @param {*} method The HTTP method.
     * @param {*} url The URL.
     * @param {*} data The data to send. Can be a form, JSON, object, etc. Handling depends on the method.
     * @param {*} headers A list of headers to send.
     * @param {*} forceLogin If true and the request is unauthorized (401), triggers a login. If false, the request fails silently.
     */
    constructor(method, url, data, headers = null, forceLogin = true) {
        this.method = method;
        this.url = url;
        this.data = data;
        this.headers = headers;
        this.forceLogin = forceLogin;
        this.uploadCallback = null;
        __reqs.add(this);
    }

    /**
     * Send the request, return a promise. If resolve and reject are given,
     * they are used inside the promise, instead of the new handlers. This 
     * allows a new promise to call already-assigned handlers.
     * 
     * @param {*} _resolve 
     * @param {*} _reject 
     * @returns An XHRPromise.
     */
    send(_resolve = null, _reject = null) {
        this.abort();
        const x = this.x = new XMLHttpRequest();
        let method = this.method;
        let data = this.data;
        let url = this.url;
        let headers = {};

        // If headers are set, convert keys to lower case.
        if(this.headers) {
            for(let [k, v] of Object.entries(this.headers))
                headers[k.toLowerCase()] = v;
        }

        return new XHRPromise((resolve, reject) => {
            if(_resolve)
                resolve = _resolve;
            if(_reject)
                reject = _reject;

            // If given, set up the upload callback events.
            if(this.uploadCallback) {
                ['loadstart', 'abort', 'error', 'timeout', 'load', 'loadend'].forEach(k => {
                    x.addEventListener(k, evt => this.uploadCallback(evt, k));
                });
            }

            x.open(method, url, true);
            // Set by the Django application. If the header is set, do not set it automatically.
            if(!('x-csrftoken' in headers)) 
                x.setRequestHeader("X-CSRFToken", getCSRFToken());
            // If json, set the content type header.
            if(data && data instanceof FormData) {
                // XHR will set the header correctly.
            } else if(data) {
                x.setRequestHeader("Content-Type", "application/json; encoding=utf-8");
            }
            if(headers) {
                // Set the header, but only if it's not null.
                for(let [k, v] of Object.entries(headers)) {
                    if(v)
                        x.setRequestHeader(k, v);
                }
            }
            if(Net.hasData('token')) {
                const token = Net.getData('token');
                if(isValidToken(token))
                    x.setRequestHeader('Authorization', `Token ${token}`);
            }
            // Listen for result.
            x.addEventListener('readystatechange', evt => {
                this.handleResult(x, resolve, reject);
            });
            // If it's not a string and not a FormData object, turn it into a string.
            if(data && !(data instanceof FormData) && typeof(data) != 'string')
                data = JSON.stringify(data);
            try {
                x.send(data);
            } catch(err) {
                console.error(err);
                reject(err);
            }
        }, this);
    }

    /**
     * Abort the request.
     */
    abort() {
        try {
            __reqs.delete(this);
            if(this.x)
                this.x.abort();
        } catch(err) {}
    }

    /**
     * Handle the HTTP result.
     * 200: Success. Resolve as usual.
     * 204: Success; no content: happens on logout.
     * 401: Trigger a login, and allow the request to be re-sent on success.
     * 403: Forbidden: throw an error.
     * Others: error.
     * 
     * @param {*} x 
     * @param {*} resolve 
     * @param {*} reject 
     */
    handleResult(x, resolve, reject) {
        switch(x.readyState) {
            case 4:
                if(x.status == 403) {

                    // The resource is not accesible.
                    reject('Forbidden.');
                    document.location = '/';

                } else if(x.status == 401) {
                    
                    // The user must be authenticated to access the resource. Force login.
                    // In the event that the token is invalid (expired, or the user has been deleted), delete the token.
                    // On login, the request will be repeated.
                    let detail = null;
                    Net.deleteData('token');
                    try {
                        detail = JSON.parse(x.responseText)
                    } catch(err) {};

                    // The user isn't authenticated. 
                    if(this.forceLogin)
                        Auth.triggerLogin(detail, this, resolve, reject);
                
                } else if(x.status == 204) {

                    // Success but no result (e.g., on logout).
                    resolve();

                } else if(x.status >= 200 && x.status < 300) {

                    // Figure out the response type.
                    let type = x.getResponseHeader('Content-Type');
                    if(type) {
                        if(type.indexOf('json') > -1) {
                            type = 'json';
                        } else if(type.indexOf('html') > -1) {
                            type = 'html';
                        } else if(type.indexOf('text') > -1) {
                            type = 'text';
                        } else {
                            throw new Error('Unknown media type: ' + type);
                        }                    
                    }

                    // Handle depending on type.
                    if(type == 'text' || type == 'html') {
                        resolve(x.responseText);
                    } else if(type == 'json') {
                        
                        let r = JSON.parse(x.responseText);
                        
                        // If for some archaic reason the result is misnamed, rename it.
                        if(hasProp(r, 'results'))
                            r.result = r.results;
                        
                        if(hasProp(r, 'download')) {
                            
                            // The result is a download. Let the browser handle it.
                            window.open(`${Net.API}/downloads/${r.download.filename}/?attachment_filename=${r.download.attachment_filename}`, '_blank');
                            resolve(r.download);

                        } else if(hasProp(r, 'result')) {
                        
                            if(r.result instanceof Array)
                                // Apply the pagination object.
                                r.result.pagination = getPagination(r);    
                            // Handle the result.
                            resolve(r.result);

                        } else if(hasProp(r, 'error')) {
                            
                            // Handle an error.
                            reject(r.error);

                        } else if(hasProp(r, 'detail')) {
                            
                            // Handle an error.
                            reject(r.detail);

                        } else {
                            
                            // Handle a generic result.
                            resolve(r);

                        }
                    }

                } else {
                    // Errors from rest framework, can be an object with
                    // arrays of errors relevant to the property represented by
                    // the key. Compile as a string and add the status result.
                    let r = null;
                    let str = [];
                    try {
                        r = JSON.parse(x.responseText);
                        for(let [k, v] of Object.entries(r))
                            v.forEach(txt => str.push(`${k}: ${txt}.`));
                    } catch(err) {}
                    str.push(`Status: ${x.status}.`);
                    reject(str.join('\n'));
                }
                break;
        }
    }

}


/**
 * Try to extract pagination information from the given object.
 */
function getPagination(obj) {
    const p = {page: 1, count: 100, total: 0};
    try {
        p.page = obj.page;
        p.count = obj.count;
        p.total = obj.total;
    } catch(err) {
        p.page = -1;
        p.count = -1;
        p.total = -1;
    }
    return p;
}

/**
 * Get the CSRF token from the cookie or failing that from the form on the base template.
 * This is a hack.
 */
function getCSRFToken(){
    let t = getCookie('csrftoken');
    if(!t) {
        let c = document.querySelector('#_csrf');
        if(c) {
            let f = new FormData(c);
            t = f.get('csrftoken');
        }
    }
    return t;
}

export class Net {

    static get ROOT() {
        if(document.location.hostname == 'localhost') {
            return 'http://localhost:9091/rov/';
        } else {
            return '/rov/';
        }
    }

    static get API() {
        return Net.ROOT + 'api/v1';
    };
    
    static getData(name) {
        return localStorage.getItem(name);
    }

    static hasData(name) {
        return localStorage.getItem(name) != null;
    }

    static setData(name, value) {
        localStorage.setItem(name, value);
    }

    static deleteData(name) {
        localStorage.removeItem(name);
    }

    static getSessionData(name) {
        return sessionStorage.getItem(name);
    }

    static hasSessionData(name) {
        return sessionStorage.getItem(name) != null;
    }

    static setSessionData(name, value) {
        sessionStorage.setItem(name, value);
    }

    static deleteSessionData(name) {
        sessionStorage.removeItem(name);
    }

    /**
     * Send an HTTP request.
     *
     * method: The method: POST, PUT, GET, DELETE, HEAD, OPTIONS.
     * url: The URL.
     * data: Request data.
     * headers: An object containing header name/value pairs.
     *
     * Returns a promise with an abort() method for cancelling the request.
     */
    static _send(method, url, data, headers = null, forceLogin = true, uploadCallback = null) {
        
        return new XHRRequest(method, url, data, headers, forceLogin, uploadCallback).send();

    }

    /**
     * Download the uploaded file with the given ID. The download is triggered
     * on result by a JSON object with the 'download' property.
     * @param {*} id 
     * @returns 
     */
    static download(id) {
        window.open(`${Net.API}/downloads/${id}/`, '_blank');
    }

    /**
     * Upload a list of files to the upload service and return a list of objects containing
     * the new uploaded file ID, name and type.
     */
    static upload(files, callback) {
        const form = new FormData();
        files.forEach(file => {
            form.append('file', file);
        });
        const xr = Net._send('POST', Net.API + '/uploads/', form);
        xr.uploadCallback = callback;
        return xr;
    }

    static get(url, data = null, headers = null, forceLogin = true) {
        return Net._send('GET', url, data, headers, forceLogin);
    }

    static post(url, data = null, headers = null, forceLogin = true) {
        return Net._send('POST', url, data, headers, forceLogin);
    }

    static put(url, data = null, headers = null, forceLogin = true) {
        return Net._send('PUT', url, data, headers, forceLogin);
    }

    static del(url, data = null, headers = null, forceLogin = true) {
        return Net._send('DELETE', url, data, headers, forceLogin);
    }

    static head(url, data = null) {
        const x = new XMLHttpRequest();
        return new XHRPromise((resolve, reject) => {
            x.open('HEAD', url);
            x.addEventListener('readystatechange', evt => {
                switch(x.readyState) {
                    case 4:
                        if(x.status >= 200 && x.status < 300) {
                            resolve();
                        } else {
                            reject(x.status);
                        }
                        break;
                }
            });
            x.send(data);
        }, x);
    }

    /**
     * Abort all outstanding requests.
     */
    static abortAll() {
        Array.from(__reqs).forEach(req => req.abort());
        __reqs.clear();
    }

}


function getcsv(url, evt) {
    if(evt) evt.preventDefault();
    const x = new XMLHttpRequest();
    x.open('GET', url + "?type=csv");
    //x.setRequestHeader('Accept', 'text/csv');
    x.addEventListener('readystatechange', evt => {
        if(x.readyState == 4) {
            console.log(x.responseText);
        }
    });
    x.send();
}

/**
 * Get the cookie value by name.
 */
export function getCookie(name) {
    let cookieValue = null;
    let cname = name + '=';
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, cname.length) == cname) {
                cookieValue = decodeURIComponent(cookie.substring(cname.length));
                break;
            }
        }
    }
    return cookieValue;
}

/**
 * Set the cookie value by name.
 */
export function setCookie(name, value) {
    let exp_date = new Date();
    exp_date.setTime(exp_date.getTime() + 30 * 24 * 60 * 60 * 1000);
    let exp_date_str = exp_date.toUTCString();
    document.cookie = `${name}=${value};expires=${exp_date_str}`;
}
