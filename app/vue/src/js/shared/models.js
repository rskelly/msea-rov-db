import { Net, getCookie, setCookie } from "../util.js";
import { Model } from '../models.js';

export class Person extends Model {

    static modelPath = `${Net.API}/people`;
    
    static defaultProps = {
    	id: 0,
        first_name: 'First Name',
        last_name: 'Last Name',
        biigle_user_id: null,
        email: 'noone@nowhere.com',
        bio: null,
        affiliation: null,
    };

    static loadAll() {
        return Model.loadAll(Person);
    }

    static loadOne(pk) {
        return Model.loadOne(Person, pk);
    }

    toString() {
        return `${this.last_name}, ${this.first_name}`;
    }

    /**
     * Compare two people to enable sorting by last name and first name.
     * @param {*} a A person.
     * @param {*} b A person.
     * @return -1 if person a is before person b; 1 if the opposite; 0 if no difference.
     */
    static comp(a, b) {
        try {
            let l = a.last_name.localeCompare(b.last_name);
            if(l == 0)
                return a.first_name.localeCompare(b.first_name);
            return l;
        } catch(err) {
            return 0;
        }
    }
    
}
Model.registerClass(Person, 'Person');


export class Taxon extends Model {

	static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Cruise) {
            return `${Net.API}/cruises/${inst.id}/taxa`;
        } else {
            return `${Net.API}/taxa`;
        }
    }
	static defaultProps = {
		id: 0,
		hart_code: null,
		aphia_id: null,
		inaturalist_id: null,
		original_label: null,
		scientific_name: null,
		common_name: null,
		label: null,
		otu: null,
		reviewed_by_id: null,
		reviewed_on: null,
		reviewed_note: null
	};
    static classMap = {
    };

    static loadAll() {
        return Model.loadAll(Taxon);
    }

}
Model.registerClass(Taxon, 'Taxon');


export class NewsItem extends Model {

	static modelPath = `${Net.API}/news`;
	static defaultProps = {
		id: 0,
		title: null,
		content: null,
		sites: null,
		created_on: new Date()
	};

	static loadAll(sites = null, top = 0) {
	    return Model.loadAll(NewsItem, {filter:{sites: sites, top: top}});
	}

}
Model.registerClass(NewsItem, 'NewsItem');


export class _File extends Model {

	static modelPath = `${Net.API}/files`;
	static defaultProps = {
		id: 0,
		name: 'New File',
		description: null,
		filetype: null,
		metadata: null,
		path: null,
		hash: null,
		blob_url: null,
		created_on: null,
		updated_on: null
	};
}
Model.registerClass(_File, '_File');


export class SpatialLibrary extends Model {

	static modelPath = `${Net.API}/spatial_library`;
	static defaultProps = {
		id: 0,
		name: null,
		note: null,
		thumbnail: null,
		geom: null,
		rast: null,
		metadata: null,
		files: []
	};

}
Model.registerClass(SpatialLibrary, 'SpatialLibrary');


export class SpatialLibraryFile extends Model {

	static modelPath = `${Net.API}/spatial_library/{id}/files`;
	static defaultProps = {
		id: 0,
		file: null,
		spatiallibrary: null
	};

}
Model.registerClass(SpatialLibraryFile, 'SpatialLibraryFile');


export class Site extends Model {

	static modelPath = `${Net.API}/sites`;
	static defaultProps = {
		id: 0,
		name: null,
		note: null,
		spatiallibrary: null,
	};

    static loadAll() {
        return Model.loadAll(Site);
    }

}
Model.registerClass(Site, 'Site');


/**
 * This is a built in Django class which is used by RestrictionGroup
 * to enable restrictions for logged-in users belonging to the group.
 */
export class AuthGroup extends Model {

	static modelPath = `${Net.API}/auth_groups`;
	static defaultProps = {
		id: 0,
		name: null
	};

	static loadAll() {
		return Model.loadAll(AuthGroup);
	}
}
Model.registerClass(AuthGroup, 'AuthGroup');


export class Restriction extends Model {

    static modelPath = `${Net.API}/restrictions`;
    
    static defaultProps = {
        id: 0,
        name: null,
        key: null,
		pattern: null,
        note: null,
		groups: [],
        created_on: new Date(),
        updated_on: new Date()
    };
	static classMap = {
		groups: 'RestrictionGroup'
	};

    static loadAll() {
        return Model.loadAll(Restriction);
    }

}
Model.registerClass(Restriction, 'Restriction');

export class RestrictionGroup extends Model {

	static modelPath = function(params) {
        const inst = params.models.pop();
        if(inst && inst instanceof Restriction) {
            return `${Net.API}/restrictions/${inst.id}/groups`;
        } else {
            return `${Net.API}/restriction_groups`;
        }
    };
	static defaultProps = {
		id: 0,
		restriction: null,
		group: null
	};
	static classMap = {
		restriction: 'Restriction',
		group: 'Group'
	};

}

export class Organisation extends Model {

	static modelPath = `${Net.API}/organisations`;
	static defaultProps = {
		id: 0,
		name: 'New Organisation',
		country: 'Canada',
        note: null
	};
    static classMap = {
    };

    static loadAll() {
        return Model.loadAll(Organisation);
    }

}
Model.registerClass(Organisation, 'Organisation');


export class UploadedFile extends Model {

	static modelPath = null;
	static defaultProps = {
		id: 0,
		path: null,
		type: null,
		name: null,
		marked_for_delete: false,
		expires_on: null,
		created_on: new Date(),

		// Excluded.
		file: null,			// Reference to a file object from an input field.
		uploading: false,
		status: 0.0
	};
	static classMap = {};
	static excludeProps = ['file', 'uploading', 'status'];

	/**
	 * Lookup for file type icon based on mime type.
	 */
	static fileIcons = {
		'text/plain': 'fiv-cla fiv-size-md fiv-icon-txt icon_extra',
		'text/csv': 'fiv-cla fiv-size-md fiv-icon-csv icon_extra',
		'application/json': 'fiv-cla fiv-size-md fiv-icon-json icon_extra',
		'text/json': 'fiv-cla fiv-size-md fiv-icon-json icon_extra',
		'application/pdf': 'fiv-cla fiv-size-md fiv-icon-pdf icon_extra',
		'application/msword': 'fiv-cla fiv-size-md fiv-icon-doc icon_extra',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'fiv-cla fiv-size-md fiv-icon-docx icon_extra',
		'application/vnd.ms-powerpoint': 'fiv-cla fiv-size-md fiv-icon-ppt icon_extra'
	};
	
	static get fileTypes() {
		return Array.from(Object.keys(UploadedFile.fileIcons));
	}

	reset() {
		this.update({
			id: 0,
			path: null,
			type: null,
			name: null,
			marked_for_delete: false,
			expires_on: null,
			created_on: new Date(),
			file: null,
			uploading: false,
			status: 0.0
		});
	}

	/**
	 * Returns true if the file is not uploaded, but can be.
	 */
	get canUpload() {
		return !this.isUploaded && !this.isUploading && this.file != null;
	}

	/**
	 * Returns true if the file is on the server.
	 */
	get isUploaded() {
		return this.id > 0;
	}

	/**
	 * Returns true if the file is uploading.
	 * @returns 
	 */
	get isUploading() {
		return this.uploading;
	}

	clone() {
		return super.clone((inst) => inst.id);
	}

	/**
	 * Handle upload status.
	 * @param {*} evt 
	 */
	handleUpload(evt, type) {
		switch(type) {
			case 'loadstart':
			case 'error':
			case 'abort':
			case 'timeout':
				this.status = 0.0;
				break;
			case 'load':
				this.status = 1.0;
				break;
			case 'progress':
				if(evt.lengthComputable && evt.total > 0) {
					this.status = evt.loaded / evt.total;
				} else {
					this.status = 0.0;
				}
				break;
			default:
				break;
		}
	}

    /**
     * Do the upload on a single file.
     */
    upload() {
		return new Promise((resolve, reject) => {
			this.uploading = true;
			Net.upload([this.file], (evt, type) => this.handleUpload(evt, type))
				.then(result => {
					this.update(result[0]);
					this.file = null;
					this.uploading = false;
					resolve(this);
				})
				.catch(err => {
					this.uploading = false;
					reject(err);
				});
		});
    }

	selectFile(evt) {
		this.file = evt.target.files[0];
        this.name = this.file.name;
		this.type = this.file.type;
	}

	dragOver(evt) {
		if(evt)
			evt.preventDefault();
	}

	dragDrop(evt) {
        // This line seems to be required in firefox to make the dataTransfer valid.
        evt.dataTransfer.getData("text");
        // Get the list of dropped items.
        let items = evt.dataTransfer.items;
        // The dropped input.
        let input = evt.target;
        // Only drop if the input type is file.
        if(items && input.type == 'file') {
            // Filter the items so only files are included.
            items = Array.from(items).filter(item => item.kind == 'file');
            // Get the list of sibling inputs starting with the dropped one.
            for(let i = 0; i < items.length; ++i) {
                // Must create a data transfer and add a single file to it.
                const dt = new DataTransfer();
                dt.items.add(items[i].getAsFile());
                // TODO: If the input doesn't exist, create one. See #107.
                // If there is an input for this item, set it.
				input.value = null;
				input.files = dt.files;
				input.dispatchEvent(new Event('change',  {bubbles: true}));
            }
        }

	}
	
}
Model.registerClass(UploadedFile, 'UploadedFile');


