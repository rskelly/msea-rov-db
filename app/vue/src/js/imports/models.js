import { Net } from "@/js/util.js";
import { Model } from "@/js/models.js";
import { UploadedFile } from "@/js/shared/models";

/**
 * Provides a dummy model for the idGenerator to use to check that an instance 
 * should have a bogus ID. 
 */
export class ImportModel extends Model {

}

/**
 * Delete the uploaded file by ID. Deletes the file on the server, and the file in the client's list.
 * @param files The file list.
 * @param file The file to delete.
 */
const deleteUpload = (files, file) => {
    files.splice(files.findIndex(f => file.id == f.id), 1);
    if(file.file_id) {
        Net.del(`${Net.API}/uploads/${file.file_id}/`)
            .catch(console.error);
    }
};

/**
 * Delete all the files in the upload list.
 * @param {*} files 
 */
const deleteUploads = (files) => {
    files.forEach(file => {
        try {
            deleteUpload(files, file);
        } catch(err) { 
            console.error(err); 
        }
    });
};

/**
 * Compare the new list and the old list of files. Delete the files from the old list that are not in the new list.
 * @param {*} newFiles 
 * @param {*} oldFiles 
 */
const deleteUnusedUploads = (newFiles, oldFiles) => {
    const remove = [];
    oldFiles.forEach(of => {
        if(newFiles.findIndex(nf => of.id == nf.id) == -1)
            remove.push(of);
    });
    deleteUploads(remove);
}


export class ImportUpload extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        event_type: null,
        stream_type: null,
        annotation_type: null,
        annotation_job: null,
        measurement_type: null,
        title: null,
        note: null,
        url: null,
        file: null
    };
    static classMap = {
        file: 'UploadedFile',
        annotation_job: 'AnnotationJob',
        measurement_type: 'MeasurementType'
    };

    static _instances = [];

    constructor(props = null) {
        super(props);
        if(!this.file)
            this.file = new UploadedFile();
        ImportUpload._instances.push(this);
    }

    /**
     * Returns all of the instances of this class.
     */
    static getAll() {
        return ImportUpload._instances;
    }
    
}
Model.registerClass(ImportUpload, 'ImportUpload');


export class ImportInstrumentConfig extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        name: null,
        instrument: null,
        configuration: null,
        streams: [],
        annotations: [],
        note: null,

        doUpdate: false,                // If true, update the metadata (instrument and configuration).
        keepEvents: true,               // If true, existing events are kept. New events are appended.
        processEvents: false,           // If true, new events are processed.
        processAnnotations: false,      // If true, new annotations are processed (annotations are events, so keepEvents applies to both).
        keepPositions: true,            // If true, existing positions are kept. New positions are appended.
        processPositions: false,        // If true, new positions are processed.
        keepMeasurements: true,         // If true, existing measurements are keps. New measurements are appended.
        processMeasuremnts: false,      // If true, new measurements are processed.
    };
    static classMap = {
        instrument: 'Instrument',
        streams: 'ImportUpload',
        annotations: 'ImportUpload',
    };

    /**
     * Configure the instance from an InstrumentConfig.
     * @param {*} ic 
     */
    fromInstrumentConfig(ic) {
        this.name = ic.name;
        this.instrument = ic.instrument;
        this.configuration = ic.configuration;
        this.note = ic.note;
    }

    /**
     * Add an upload instance to represent an annotation file.
     */
    addAnnotation() {
        this.annotations.push(new ImportUpload());
        this.dirty = true;
    }

    /**
     * Add an upload instance to represent a stream file.
     */
    addStream() {
        this.streams.push(new ImportUpload());
        this.dirty = true;
    }

    /**
     * Delete unused streams and add new ones.
     * @param {*} streams 
     */
    updateStreams(streams) {
        deleteUnusedUploads(streams, this.streams);
        this.streams.splice(0);
        this.streams.push(...streams);
        this.dirty = true;
    }

    /**
     * Delete unused annotations and add new ones.
     * @param {*} streams 
     */
    updateAnnotations(annotations) {
        deleteUnusedUploads(annotations, this.annotations);
        this.annotations.splice(0);
        this.annotations.push(...annotations);
        this.dirty = true;
    }

    /**
     * Remove the stream and delete the file.
     * @param {*} stream 
     */
    removeStream(stream) {
        deleteUpload(this.streams, stream);
        this.dirty = true;
    }

    /**
     * Remove the annotation and delete the file.
     * @param {*} annotation 
     */
    removeAnnotation(annotation) {
        deleteUpload(this.annotations, annotation);
        this.dirty = true;
    }

    /**
     * Clear and delete all files.
     */
    clearFiles() {
        deleteUploads(this.annotations);
        deleteUploads(this.streams);
        this.dirty = true;
    }

}
Model.registerClass(ImportInstrumentConfig, 'ImportInstrumentConfig');

export class ImportPlatformConfig extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        name: null,
        platform: null,
        configuration: null,
        instrument_configs: [],
        note: null,
        doUpdate: false
    };
    static classMap = {
        platform: 'Platform',
        instrument_configs: 'ImportInstrumentConfig'
    };

    /**
     * Configure the instance from the PlatformConfig.
     * @param {*} pc 
     */
    fromPlatformConfig(pc) {
        this.name = pc.name;
        this.platform = pc.platform;
        this.configuration = pc.configuration;
        this.note = pc.note;
        let ic = null;
        pc.instrument_configs.forEach(config => {
            this.instrument_configs.push((ic = new ImportInstrumentConfig()));
            ic.fromInstrumentConfig(config);
        });
    }

    /**
     * Add an InstrumentConfig.
     */
    addInstrumentConfig() {
        this.instrument_configs.push(new ImportInstrumentConfig());
        this.dirty = true;
    }

    /**
     * Remove the InstrumentConfig.
     * @param {*} instrument_config 
     */
    removeInstrumentConfig(instrument_config) {
        instrument_config.clearFiles();
        this.instrument_configs.splice(this.instrument_configs.findIndex(ic => ic.id == instrument_config.id), 1);
        this.dirty = true;
    }

    /**
     * Clear all files in all InstrumentConfigs.
     */
    clearFiles() {
        this.instrument_configs.forEach(ic => ic.clearFiles());
        this.dirty = true;
    }

}
Model.registerClass(ImportPlatformConfig, 'ImportPlatformConfig');

export class ImportTransect extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        name: null,
        start_time: Date.UTC(),
        end_time: Date.UTC(),
        objective: null,
        summary: null,
        note: null,

        doUpdate: false     // If true, metadata updates are applied to transect.
    };
    static classMap = {
    };

    /**
     * Configure the instance from a Transect.
     * @param {*} transect 
     */
    fromTransect(transect) {
        this.name = transect.name;
        this.start_time = transect.start_time;
        this.end_time = transect.end_time;
        this.objective = transect.objective;
        this.summary = transect.summary;
        this.note = transect.note;
    }

}
Model.registerClass(ImportTransect, 'ImportTransect');

export class ImportDiveCrew extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        person: null,
        role: null,
        note: null,

        keepEvents: true,       // If true, existing events are kept. New events are appended.
        processEvents: false,   // If true, new events are processed.
    };
    static classMap = {
        person: 'Person',
        role: 'DiveRole'
    };

    /**
     * Configure the instance from a DiveCrew.
     * @param {*} member 
     */
    fromDiveCrew(member) {
        this.person = member.person;
        this.role = member.dive_role;
        this.note = member.note;
    }

}
Model.registerClass(ImportDiveCrew, 'ImportDiveCrew');

export class ImportDive extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        do_import: false,
        name: null,
        start_time: Date.UTC(),
        end_time: Date.UTC(),
        objective: null,
        summary: null,
        note: null,
        crew: [],
        transects: [],
        ship_config: null,
        sub_config: null,
        site: null,
        events: [],

        doUpdate: false,        // If true, dive metadata are update.
        keepCrew: true,         // If true, existing crew are kept.
        processCrew: false,     // If true, new crew are processed.
        keepEvents: true,       // If true, existing events are kept.
        processEvents: false,   // If true, new events are processed.
    };
    static classMap = {
        crew: 'ImportDiveCrew',
        transects: 'ImportTransect',
        ship_config: 'ImportPlatformConfig',
        sub_config: 'ImportPlatformConfig',
        site: 'Site',
        events: 'ImportUpload',
    };

    /**
     * Create the instance. Creates a blank InportPlatformConfig for each
     * of the ship and sub configs.
     * @param {*} props 
     */
    constructor(props = null) {
        super(props);
        if(!this.ship_config) {
            this.ship_config = new ImportPlatformConfig();
            this.dirty = true;
        }
        if(!this.sub_config) {
            this.sub_config = new ImportPlatformConfig();
            this.dirty = true;
        }
    }

    /**
     * Configure the instance from a Dive.
     * @param {*} dive 
     */
    fromDive(dive) {
        this.name = dive.name,
        this.start_time = dive.start_time,
        this.end_time = dive.end_time,
        this.objective = dive.objective,
        this.summary = dive.summary,
        this.note = dive.note
        let c = null;
        dive.crew.forEach(member => {
            this.crew.push((c = new ImportDiveCrew()));
            c.fromDiveCrew(member);
        });
        let t = null;
        dive.transects.forEach(transect => {
            this.transects.push((t = new ImportTransect()));
            t.fromTransect(transect);
        });
        let pc = null;
        this.ship_config = (pc = new ImportPlatformConfig());
        pc.fromPlatformConfig(dive.ship_config);
        this.sub_config = (pc = new ImportPlatformConfig());
        pc.fromPlatformConfig(dive.sub_config);
    }

    /**
     * Add a crew member.
     */
    addCrew() {
        this.crew.push(new ImportDiveCrew());
        this.dirty = true;
    }

    /**
     * Remove a crew member.
     * @param {*} crew 
     */
    removeCrew(crew) {
        this.crew.splice(this.crew.indexOf(crew), 1);
        this.dirty = true;
    }

    /**
     * Add a transect.
     */
    addTransect() {
        const t = new ImportTransect();
        t.start_time = this.start_time;
        t.end_time = this.end_time;
        t.name = this.transects.length + 1;
        this.transects.push(t);
        this.dirty = true;
    }

    /**
     * Remove a transect.
     * @param {*} transect 
     */
    removeTransect(transect) {
        this.transects.splice(this.transects.indexOf(transect, 1));
        this.dirty = true;
    }

    /**
     * Add an events file.
     */
    addEvents() {
        this.events.push(new ImportUpload());
        this.dirty = true;
    }

    /**
     * Remove and delete the unused events files and add the new events.
     * @param {*} events 
     */
    updateEvents(events) {
        deleteUnusedUploads(events, this.events);
        this.events.splice(0);
        this.events.push(...events);
        this.dirty = true;
    }

    /**
     * Remove and delete all events. Files.
     * @param {*} events 
     */
    removeEvents(events) {
        deleteUpload(this.events, events);
        this.dirty = true;
    }

    /**
     * Remove and delete all event files.
     */
    clearFiles() {
        deleteUploads(this.events);
        this.ship_config.clearFiles();
        this.sub_config.clearFiles();
        this.dirty = true;
    }

}
Model.registerClass(ImportDive, 'ImportDive');

export class ImportCruiseCrew extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        person: null,
        role: null,
        note: null,
    };
    static classMap = {
        person: 'Person',
        role: 'CruiseRole'
    };

    /**
     * Configure the instance from a CruiseCrew.
     * @param {*} member 
     */
    fromCruiseCrew(member) {
        this.person = member.person;
        this.role = member.cruise_role;
        this.note = member.note;
    }

}
Model.registerClass(ImportCruiseCrew, 'ImportCruiseCrew');


export class ImportCruise extends ImportModel {

    static modelPath = null;
    static defaultProps = {
        id: 0,
        name: null,
        leg: 1,
        ship: null,
        start_time: Date.UTC(),
        end_time: Date.UTC(),
        objective: null,
        summary: null,
        note: null,
        crew: [],
        dives: [],
        documents: [],

        doUpdate: false,            // If true, cruise metadata are updated.
        keepCrew: true,             // If true, existing crew are kept.
        processCrew: false,         // If true, new crew are processed.
        keepDocuments: true,        // If true, existing documents are kept.
        processDocuments: false,    // If true, new documents are added.
    };
    static classMap = {
        crew: 'ImportCruiseCrew',
        dives: 'ImportDive',
        ship: 'Platform',
        documents: 'CruiseDocument'
    };

    /**
     * Configure the instance from a Cruise.
     */
    fromCruise(cruise) {
        this.name = cruise.name;
        this.leg = cruise.leg;
        this.ship = cruise.ship;
        this.start_time = cruise.start_time;
        this.end_time = cruise.end_time;
        this.objective = cruise.objective;
        this.summary = cruise.summary;
        this.note = cruise.note;
        let c = null;
        cruise.crew.forEach(member => {
            this.crew.push((c = new ImportCruiseCrew()));
            c.fromCruiseCrew(member);
        });
        let d = null;
        cruise.dives.forEach(dive => {
            this.dives.push((d = new ImportDive()));
            d.fromDive(dive);
        });
        cruise.documents.forEach(doc => {
            this.documents.push(doc);
        });
    }

    /**
     * Add a crew member.
     */
    addCrew() {
        this.crew.push(new ImportCruiseCrew());
        this.dirty = true;
    }

    /**
     * Remove the crew member.
     * @param {*} crew 
     */
    removeCrew(crew) {
        this.crew.splice(this.crew.indexOf(crew), 1);
        this.dirty = true;
    }

    /**
     * Add a dive.
     */
    addDive() {
        this.dives.push(new ImportDive());
        this.dirty = true;
    }

    /**
     * Copy and add the last dive in the list.
     */
    copyDive() {
        if(this.dives.length > 0) {
            const dive = this.dives[this.dives.length - 1].clone();
            dive.name = dive.name + ' (Copy)';
            this.dives.push(dive);
        } else {
            this.dives.push(new ImportDive());
        }
    }

    /**
     * Remove the dive.
     * @param {*} dive 
     */
    removeDive(dive) {
        dive.clearFiles();
        this.dives.splice(this.dives.indexOf(dive), 1);
        this.dirty = true;
    }

    /**
     * Add a document.
     */
    addDocument() {
        this.documents.push(new ImportUpload());
        this.dirty = true;
    }
    
    /**
     * Remove and delete the unused document files and add the new ones.
     * @param {*} documents 
     */
    updateDocuments(documents) {
        deleteUnusedUploads(documents, this.documents);
        this.documents.splice(0);
        this.documents.push(...documents);
        this.dirty = true;
    }

    /**
     * Remove and delete the document file.
     * @param {*} document 
     */
    removeDocument(document) {
        deleteUpload(this.documents, document.id);
        this.dirty = true;
    }

    /**
     * Remove and delete all document files.
     */
    clearFiles() {
        deleteUploads(this.documents);
        this.dirty = true;
    }

}
Model.registerClass(ImportCruise, 'ImportCruise');



export class CruiseImport extends ImportModel {

    static NOT_STARTED = 'Not Started';
    static RUNNING = 'Running';
    static FAILED = 'Failed';
    static COMPLETED = 'Completed';
    static CANCELED = 'Canceled';

    static DEBUG = 'DEBUG';
    static INFO = 'INFO';
    static NOTICE = 'NOTICE';
    static WARNING = 'WARN ';
    static ERROR = 'ERROR';

    static modelPath = `${Net.API}/cruise_imports`;
    static defaultProps = {
        id: 0,
        version: 1,
        name: 'New Cruise',
        data: null,
        status: CruiseImport.NOT_STARTED,
        logs: [],
        created_on: new Date(),
        updated_on: new Date()
    };

    /**
     * Cancel the import job.
     * @returns 
     */
    cancel() {
        this.status = CruiseImport.CANCELED;
        return this.save();
    }

    /**
     * Start the import job.
     */
    start() {
        this.status = CruiseImport.NOT_STARTED;
        return this.save();
    }

    /**
     * Load all import jobs. Returns a promise.
     * @param {*} loadLogs Load the logs if true.
     * @returns 
     */
    static loadAll() {
        return Model.loadAll(CruiseImport);
    }

    /**
     * Load a specific import job. Returns a promise.
     * @param {*} pk 
     * @returns 
     */
    static loadOne(pk) {
        return Model.loadOne(CruiseImport, pk);
    }
}
Model.registerClass(CruiseImport, 'CruiseImport');
