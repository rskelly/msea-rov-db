/**
 * These are models that mirror those of the NDST Shiny app, which can be imported
 * into tables in the ndst schema.
 */
import { Net } from "@/js/util.js";
import { Model } from "@/js/models.js";

/**
 * Provides some extra functionality related to the NDST models.
 */
class NDSTModel extends Model {

    constructor(props) {
        super(props);
        // Set the default row ID.
        this.row_id = crypto.randomUUID();
        this.dirty = false;
    }

    /**
     * Deep-clones the model and sets the row ID with a new UUID.
     * @returns 
     */
    clone() {
        const c = super.clone();
        c.row_id = crypto.randomUUID();
        return c;
    }
    
}

export class Cruise extends NDSTModel {

    static modelPath = `${Net.API}/ndst/cruises`;
    static defaultProps = {
        id: 0,
        row_id: crypto.randomUUID(),
        name: 'New Cruise',
        leg: 0,
        objective: null,
        summary: null,
        note: null,
        status: 1,
        active: true,
        hide: 0,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {};

    static loadAll(filter) {
        return Model.loadAll(ndst.Cruise, filter);
    }

};
Model.registerClass(Cruise, 'ndst.Cruise');

export class DiveConfig extends NDSTModel {

    static modelPath = `${Net.API}/ndst/dive_configs`;
    static defaultProps = {
        id: 0,
        row_id: crypto.randomUUID(),
        name: 'New Dive Config',
        ship_config: null,
        sub_config: null,
        ship_instrument_configs: null,
        sub_instrument_configs: null,
        note: null,
        active: true,
        hide: 0,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {
    };

    static loadAll() {
        return Model.loadAll(ndst.DiveConfig);
    }

    set ship_instrument_configs_array(c) {
        try {
            this.ship_instrument_configs = c.join(';');
        } catch(err) {}
    }

    get ship_instrument_configs_array() {
        try {
            return this.ship_instrument_configs.split(';');
        } catch(err) {
            return [];
        }
    }

    set sub_instrument_configs_array(c) {
        try {
            this.sub_instrument_configs = c.join(';');
        } catch(err) {}
    }

    get sub_instrument_configs_array() {
        try {
            return this.sub_instrument_configs.split(';');
        } catch(err) {
            return [];
        }
    }

};
Model.registerClass(DiveConfig, 'ndst.DiveConfig');

export class Dive extends NDSTModel {

    static modelPath = `${Net.API}/ndst/dives`;
    static defaultProps = {
        id: 0,
        row_id: crypto.randomUUID(),
        cruise_name: null,
        leg: 0,
        name: 'New Dive',
        pilot: null,
        start_time: null,
        end_time: null,
        site_name: null,
        dive_config: null,
        objective: null,
        summary: null,
        note: null,
        active: true,
        hide: 0,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {};

    static loadAll(filter) {
        return Model.loadAll(ndst.Dive, filter);
    }

    set pilots_array(p) {
        try {
            this.pilot = p.join(';');
        } catch(err) {}
    }

    get pilots_array() {
        try {
            return this.pilot.split(';');
        } catch(err) {
            return [];
        }
    }

};
Model.registerClass(Dive, 'ndst.Dive');

export class Transect extends NDSTModel {

    static modelPath = `${Net.API}/ndst/transects`;
    static defaultProps = {
        id: 0,
        row_id: crypto.randomUUID(),
        cruise_name: null,
        leg: 0,
        dive_name: null,
        name: 'New Transect',
        start_time: null,
        end_time: null,
        objective: null,
        summary: null,
        note: null,
        active: true,
        hide: 0,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {};

    static loadAll(filter) {
        return Model.loadAll(ndst.Transect, filter);
    }

};
Model.registerClass(Transect, 'ndst.Transect');

export class Person extends NDSTModel {
    
    static modelPath = `${Net.API}/ndst/people`;
    static defaultProps = {
        id: 0,
        row_id: crypto.randomUUID(),
        person_id: 0,
        initials: null,
        first_name: null,
        last_name: null,
        email: null,
        active: true,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {};

    static loadAll() {
        return Model.loadAll(ndst.Person);
    }

};
Model.registerClass(Person, 'ndst.Person');

export class Equipment extends NDSTModel {

    static modelPath = `${Net.API}/ndst/equipment`;
    static defaultProps = {
        id: 0,
        row_id: crypto.randomUUID(),
        brand: null,
        model: null,
        serial_number: null,
        type: null,
        note: null,
        instrument_id: null,
        platform_id: null,
        active: true,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {};

    static loadAll(filter) {
        return Model.loadAll(ndst.Equipment, filter);
    }

    static loadTypes() {
        return Net.get(`${Net.API}/ndst/equipment/types/`);
    }

};
Model.registerClass(Equipment, 'ndst.Equipment');

export class EquipConfig extends NDSTModel {

    static modelPath = `${Net.API}/ndst/equipment_configs`;
    static defaultProps = {
        id: 0,
        row_id: crypto.randomUUID(),
        name: 'New Equipment Config',
        type: null,
        configuration: null,
        note: null,
        active: true,
        created_on: new Date(),
        updated_on: new Date(),
    };
    static classMap = {};

    static loadAll(filter) {
        return Model.loadAll(ndst.EquipConfig, filter);
    }

};
Model.registerClass(EquipConfig, 'ndst.EquipConfig');
