/**
 * This file contains classes and functions for managing operations on the taxonomy_audit.html page.
 */

/**
 * The TaxonomyAudit class manages methods, objects and UI related to 
 * the auditing page.
 */
export class TaxonomyAudit {

	constructor(data) {
		this.currentRow = null;     // The currently-selected row when "Find" is clicked.
        this.findReq = null;        // The current find request.
		this.panel = document.querySelector('#find_results_panel'); // The search results panel.
		this.state = reactive({
		    term: null,
		    ranks: [],
		    results: [],
		    annotationProtocol: computed({
		        get: () => { return this.state._annotationProtocol; },
		        set: (ap) => {
		            if(ap != this.state._annotationProtocol) {
                        this.state._annotationProtocol = ap;
                        this.state.annotationProtocolTaxa.splice(0);
                        ap.loadTaxa()
                            .then(result => {
                                this.state.annotationProtocolTaxa.push(...result);
                            });
                    }
		        }
		    }),
		    annotationProtocols: null,
		    annotationProtocolTaxa: []
		});
	}

	/**
	 * Move the results form to follow the specified row.
	 */
	moveTo(row) {
		const node = document.querySelector('#find_results_panel');
		row.parentNode.insertBefore(node, row.nextSibling);
	}

	/**
	 * Find records matching the data in the current row.
	 */
	find(term, row) {
		this.state.term = term;
		this.moveTo(row);
		// Make the request to the search endpoint.
		if(this.findReq)
			this.findReq.abort();
		this.findReq = get(`/api/v1/taxon_search?term=${term}`)
			.then(result => this.handleResult(result));
		// Show the result table.
		document.querySelector('#find_results_loading').style.display = 'inline';
	}

	/**
	 * Create a link to the external site using the taxon ID and source.
	 * Returns an anchor element or the ID itself if there's no link for the source.
	 */
	taxonLink(id, source) {
		let external = null;
		let url = null;
		// Create the URL and external label if possible.
		switch(source.toLowerCase()) {
			case 'itis':
				url = `https://www.itis.gov/servlet/SingleRpt/SingleRpt?search_topic=TSN&search_value=${id}`;
				external = 'ITIS.gov';
				break;
			case 'inaturalist':
				url = `https://www.inaturalist.org/taxa/${id}`;
				external = 'iNaturalist.org';
				break;
			case 'obis':
			case 'worms':
			case 'aphia':
				url = `https://www.marinespecies.org/aphia.php?p=taxdetails&id=${id}`;
				external = 'marinespecies.org (WoRMS)';
				break;
			default: 
				break;
		}
		return [url, external];
	}

	/**
	 * Populate the find results table with matches for the original label.
	 */
	handleResult(result) {
		// Find the container and templates.
		let ranks = new Set();
		let items = [];
		// Iterate over the results.
		(result.items || []).forEach(item => {
			// Iterate over the returned names; figure out which is the main result
			// and add the others to a list of extra results.
			let names = [];
			for(let i = 0; i < item.names.length; i += 2)
				names.push({n: i / 2, type: item.names[i], name: item.names[i + 1]});
			item.names = names;
			items.push(item);
			ranks.add(item.rank.trim());
		});
        // Set result array and ranks on the data object (for Vue).
		this.state.ranks = Array.from(ranks);
		this.state.results = items;
		// Show the results UI.
		document.querySelector('#find_results_panel').style.display = 'table-row';
		document.querySelector('#find_results_loading').style.display = 'none';
	}

    /**
     * Hide results that do not match the selected rank type (species, class, etc.)
     */
	filterByRank(evt) {
		let r = evt.target.options[evt.target.selectedIndex].value;
		this.rows.forEach(row => {
			row.style.display = row.dataset.rank == r ? 'table-row-group': 'none';
		});
	}

	/**
	 * Perform the search. If a search term is available, it will be applied;
	 * if the cruise leg is selected, results will be limited to it. If neither are
	 * supplied, nothing happens.
	 */
	doSearch() {
		// Get the search term.
		const search = this.state.term;

		// Assemble the params. The models property configures the model request path.
		// The filter configures the search term.
		const params = {};
		params.models = [this.state.annotationProtocol];
		if(search)
			params.filter = {search: search};

		// Perform the search.
		if(params.models || params.filter) {
			Model.loadAll(AnnotationProtocolTaxon, params)
			.then(result => {
				this.state.annotationProtocolTaxa = result;
			});
		} else {
			this.state.annotationProtocolTaxa.clear();
		}
	};

	/**
	 * When search is clicked, do the search.
	 */
	searchClick(evt) {
		evt.preventDefault();
		this.doSearch();
	}

	/**
	 * When clear is clicked, perform the search with no search term.
	 */
	clearClick(evt) {
		evt.preventDefault();
		this.state.term = '';
		this.doSearch();
	}

    /**
     * Hide the results display panel.
     */
	hideResults(evt) {
		evt.preventDefault();
		document.querySelector('#find_results_panel').style.display = 'none';
	}

    rankFilter(list, filter) {
        return list.filter(item => filter == 'All' || item.rank == filter);
    }

    // Perform the search when an annotation protocol is selected.
    selectAnnotationProtocol() {
        this.doSearch();
    }

    // Find species results for the selected taxon.
    findTaxon(taxon, evt) {
        if(evt) evt.preventDefault();
        this.state.results = [];
        this.state.selectedTaxon = taxon;
        this.state.rank = 'All';
        this.find(taxon.original_label, parent(evt.target, 'tr'));
    }

    // Do a custom search to find records for the taxon.
    findCustom(term, evt) {
        if(evt) evt.preventDefault();
        this.state.results = [];
        this.state.rank = 'All';
        this.find(term, this.state, parent(evt.target, 'tr'));
    }

    // Remove the taxon from the list. This is irreversible.
    deleteTaxon(taxon, evt) {
        if(evt) evt.preventDefault();
        if(confirm('Are you sure?')) {
            this.state.selectedTaxon = null;
            this.state.annotationProtocolTaxa.splice(this.state.annotationProtocolTaxa.indexOf(taxon), 1);
            taxon.destroy();
        }
    }

}


// Initialize the application.
/*
let app = new Admin('#app', {
    ranks: [],              // The rankings (best=1) of search results.
    results: [],            // The search results.
    term: '',               // The search term.
    selectedTaxon: null,    // The selected taxon.
    rank: 'All'             // The initial value for the rank filter.
}, {
    // Provides a filter based on the taxonomic rank drop-down.
    rankFilter: (list, filter) => {
        return list.filter(item => filter == 'All' || item.rank == filter);
    },

    // Perform the search when an annotation protocol is selected.
    selectAnnotationProtocol: () => {
        auditor.doSearch();
    },

    // Find species results for the selected taxon.
    findTaxon: (taxon, evt) => {
        if(evt) evt.preventDefault();
        app.data.results = [];
        app.data.selectedTaxon = taxon;
        app.data.rank = 'All';
        auditor.find(taxon.original_label, parent(evt.target, 'tr'));
    },

    // Do a custom search to find records for the taxon.
    findCustom: (term, evt) => {
        if(evt) evt.preventDefault();
        app.data.results = [];
        app.data.rank = 'All';
        auditor.find(term, app.data, parent(evt.target, 'tr'));
    },

    // Remove the taxon from the list. This is irreversible.
    deleteTaxon: (taxon, evt) => {
        if(evt) evt.preventDefault();
        if(confirm('Are you sure?')) {
            app.data.selectedTaxon = null;
            app.data.collection.AnnotationProtocolTaxon.splice(app.data.collection.AnnotationProtocolTaxon.indexOf(taxon), 1);
            taxon.destroy();
        }
    },

    test(e, r) {
        console.log(e);
        app.data.selectedTaxon[r.idField] = r.id
    }
});

// Register model classes.
app.registerClass(AnnotationProtocol, true);
app.registerClass(AnnotationProtocolTaxon, false);
app.registerClass(Taxon, false);
app.registerClass(iNaturalistTaxon, false)
app.registerClass(WoRMSTaxon, false);
app.registerClass(HartTaxon, false)

// The auditor.
const auditor = new TaxonomyAudit(app.data);
*/