/**
 * This file provides map classes for Leaflet maps that get shown on various pages on the site.
 */

import { clear, hasProp, Net } from '@/js/util.js';

import "leaflet";

/**
 * The default layer that will be shown when a user first enters the site. When they change the base layer,
 * the change is stored in LocalSettings so it persists indefinitely.
 */
export const DEFAULT_BASE_LAYER = 'Carto Light';

/**
 * @returns The google satellite tile URL.
 */
export function google_sat_tile_url() {
    return 'https://mt3.google.com/vt/lyrs=s&x={x}&y={y}&z={z}';
}

/**
 * @returns The GRMT tile URL.
 */
export function gmrt_tile_url() {
    return 'https://www.gmrt.org/services/mapserver/wms_merc';
}

/**
 * @returns The Carto light tile URL.
 */
export function carto_light_tile_url() {
	return 'https://a.basemaps.cartocdn.com/light_all/{z}/{x}/{y}@2x.png';
}

/**
 * @returns The Carto dark tile URL.
 */
export function carto_dark_tile_url() {
	return 'https://a.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}@2x.png';
}

/**
 * Returns the dive track URL. This is a Mapnik/renderd/mod_tile service. When called from
 * localhost, loads the URL from mseastage.
 * @returns The dive track URL.
 */
export function dive_track_url() {
    return (document.location.hostname == 'localhost'
        ? 'https://msea.science'
        : document.location.origin)
        + '/tiles/dive_tracks/{z}/{x}/{y}.png';
}

/**
 * Base class for maps.
 */
class _Map {

	/**
	 * Build the map on the given DOM element ID.
	 * @param {*} id 
	 */
	constructor(id) {
		this.map = null;				// Reference to the Leaflet map.
		this.overlayLayers = {};		// A layers that should show above the basemap.
		this.baseLayers = {};			// Base layers.
		if(id)							// If an ID is given, build the map.
		    this.showMap(id);
	}

	/**
	 * Get the map bounds as a query string.
	 * @returns 
	 */
	getBoundsString() {
		const bounds = this.map.getBounds();
		const zoom = this.map.getZoom();
		return `zoom=${zoom}&x1=${bounds.getWest()}&y1=${bounds.getNorth()}&x2=${bounds.getEast()}&y2=${bounds.getSouth()}`;
	}

	/**
	 * Get the map bounds as a object.
	 * @returns 
	 */
	getBoundsObject() {
		const bounds = this.map.getBounds();
		const zoom = this.map.getZoom();
		return {zoom: zoom, x1: bounds.getWest(), y1: bounds.getNorth(), x2: bounds.getEast(), y2: bounds.getSouth()};
	}

    /**
     * Encode any string into its hexadecimal representation. Useful
     * for getting a colour string from a string in a stable, fast
     * way.
     */
    hexEncode(str, len = -1) {
        let md5 = function(d){var r = M(V(Y(X(d),8*d.length)));return r.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_}
        let result = md5(str);
        return len < 0 ? result : result.substr(0, len);
    }

	/**
	 * For position measurements.
	 * 
	 * Add the data to the map with the given ID. Clears
	 * previous markers and loads a Google tile basemap.
	 */
	showMap(id) {
		// On first run, find the map element and load tiles.
		if(!this.map) {
			this.baseLayers = {
			    'Carto Dark': L.tileLayer(carto_dark_tile_url(), {
                    maxZoom: 20,
					attribution: 'Map tiles by CartoDB, under CC BY 3.0. Data by OpenStreetMap, under ODbL.'
                }),
			    'Carto Light': L.tileLayer(carto_light_tile_url(), {
                    maxZoom: 20,
					attribution: 'Map tiles by CartoDB, under CC BY 3.0. Data by OpenStreetMap, under ODbL.'
                }),
			    'Google Satellite': L.tileLayer(google_sat_tile_url(), {
                    maxZoom: 20,
                    subdomains:['mt0','mt1','mt2','mt3'],
					attribution: 'Map data ©2015 Google'
                }),
                'GMRT': L.tileLayer.wms(gmrt_tile_url(), {
                    layers: 'GMRT',
					maxZoom: 18,
                    attribution: 'GMRT CC BY 4.0'
                })
			};
			this.map = L.map(id, {center: [52, -135], zoom: 5});
			// Drag/zoom events for the map.
	    	this.map.on('moveend', this.moveEnd.bind(this));
	    	this.map.on('zoomstart', this.zoomStart.bind(this));
	    	// Dispatched when the base layer is changed using the control.
            this.map.on('baselayerchange', this.baseLayerChange.bind(this));
            // Prepare the layer control.
            this.layerControl = L.control.layers(this.baseLayers).addTo(this.map);
            // Load the previously-used base layer, or a default.
            let defaultLayer = localStorage.getItem('default_map_layer');
            if(!defaultLayer)
                defaultLayer = DEFAULT_BASE_LAYER;
           	this.baseLayers[defaultLayer].addTo(this.map);
	    }
	}

	/**
	 * Handle a base layer change by raising the overlay layers to front
	 * in the order in which they were added.
	 * @param {*} evt 
	 */
    baseLayerChange(evt) {
        localStorage.setItem('default_map_layer', evt.name);
    }

	/**
	 * Add an overlay layer and bring it to the front.
	 * @param {*} layer 
	 */
	addOverlayLayer(name, layer) {
		this.overlayLayers[name] = layer;
		this.layerControl.addOverlay(layer, name);
		layer.addTo(this.map);
	}

	moveEnd(evt) {}
	zoomStart(evt) {}
}


/**
 * Displays a map for positions on the observations page.
 */
export class ObservationMap extends _Map {

	constructor(id) {
		super(id);
	}

	showDive(id) {
		Net.get(`${Net.API}/dives/${id}/track/`)
			.then(result => {
				if(!result.coordinates || result.coordinates.length == 0)
					return;
				const layer = L.geoJSON({
					type: "Feature",
					properties: {},
					geometry: result
				});
				this.addOverlayLayer('Dive Track', layer);
				this.map.flyToBounds(layer.getBounds());
			})
			.catch(alert);
	}

}

export class SearchMap extends _Map {

	constructor(id) {
		super(id);
		this.obs = [];					// A list of observation markers.
		this.mapTimeout = 0;
	}

	/**
	 * Render the result array.
	 * @param {*} result 
	 */
	render(result) {
		// Clear the map from the last result.
		this.obs.forEach(item => {
			item.removeFrom(this.map);
		});
		this.obs.splice(0);

		result.forEach(item => {
			// Construct the marker.
			const latLng = L.latLng(item.lat, item.lon);
			const e = document.createElement('div');
			e.className = 'obs_label';
			//e.textContent = item.scientific_name;
			e.addEventListener('click', (evt) => {
				const depth = item.depth ? `${item.depth}m` : 'Not available';
				alert(`Scientific Name: ${item.scientific_name}\nCommon Name: ${item.common_name}\nTimestamp: ${item.timestamp}\nPosition: ${item.lon}°,${item.lat}°\nDepth: ${depth}`);
			});
			const obsIcon = L.divIcon({
				html: e, 
				className: '', 
				bgPos: [0, 0]
			});
			this.obs.push(L.marker(latLng, {icon: obsIcon}).addTo(this.map));
		});

	}

}


/**
 * Displays the map of dive tracks and observations with a choice of base maps.
 */
export class ROVHomeMap extends _Map {

	constructor(id) {
		super(id);
		this.obs = [];					// A list of observation markers.
		this.optXhr = null;				// XHR object for loading observations.
		this.firstLoad = true;			// True if first load; shows the whole map.
		this.loadEl = null;				// The "Loading" element.
		this.cruiseColours = {};        // A map for cruise names and colours.
		this.cruiseCount = 0;			// The number of cruises.
		this.diveCount = 0;				// The number of dives.
	}

	/**
	 * Override the showMap method to add the dive tracks layer.
	 * @param {*} id 
	 */
	showMap(id) {
		super.showMap(id);
		const tracks = L.tileLayer(dive_track_url(), {
			tms: false,
		});
		this.addOverlayLayer('Dive Tracks', tracks);
		this.map.on('click', this.handleClick.bind(this));
		this.loadCruises();
	}

	/**
	 * Handle a map click.
	 * - For dive tracks, uses the coordinate to locate a list of relevant dives by 
	 *   intersecting the clicked point with a buffered representation of the track.
	 * 
	 * @param {*} evt 
	 */
	handleClick(evt) {
		// Get the click and map properties.
		const lat = evt.latlng.lat;
		const lon = evt.latlng.lng;
		const zoom = this.map.getZoom();
		// If the zoom is < 5, don't bother.
		if(zoom < 5)
			return;
		// Raise a tooltip with the relevant dives.
		Net.get(`${Net.API}/rov_home/map_dives/?lat=${lat}&lon=${lon}&zoom=${zoom}`)
			.then(result => {
				// If there are no results, quit.
				if(!result.length)
					return;
				// Create a div and populate it with links.
				const d = document.createElement('div');
				result.forEach(dive => {
					const l = document.createElement('div');
					const a = document.createElement('a');
					a.href = `/rov/cruises/${dive.cruise_name}#${dive.dive_name}`;
					a.target = '_blank';
					a.title = `${dive.cruise_name} - ${dive.dive_name}.`;
					a.innerHTML = `${dive.cruise_name} - ${dive.dive_name}`;
					l.appendChild(a);
					d.appendChild(l);
				});
				L.popup(evt.latlng, {content: d}).openOn(this.map);
			})
			.catch(alert);
	}

	loadCruises() {
		const c = document.querySelector('#cruise_menu');
		if(c)
			c.innerHTML = 'Loading...';
		Net.get(`${Net.API}/rov_home/cruise_colours_bounds/`)
			.then(result => {
				if(!this.map)
					return;
				this.cruiseCount = result.cruise_count;
				this.diveCount = result.dive_count;
				let boundsAll = null;
				result.cruises.forEach(item => {
					// The boundary geometry is either a point or a polygon, so we have to determine which and
					// create a latlngbounds from the result.
					const geom = JSON.parse(item.geom);
					const coords = geom.coordinates;
					let tl, br;
					if(geom.type == 'Point') {
						tl = coords;
						br = coords
					} else {
						tl = coords[0][0];
						br = coords[0][2];
					}
					// Add the entry to the registry.
					let bounds = L.latLngBounds(L.latLng(tl.reverse()), L.latLng(br.reverse()));
					this.cruiseColours[item.cruise_name] = {
						id: item.cruise_id, 
						name: item.cruise_name, 
						colour: item.colour,
						bounds: bounds
					};
					// Initialize the bounds object on first run, then extend it on subsequent runs.
					if(!boundsAll) {
						boundsAll = L.latLngBounds(bounds.getSouthWest(), bounds.getNorthEast());
					} else {
						boundsAll.extend(bounds);
					}
				});
				this.cruiseColours['Show All'] = {id: 0, name: 'Show All', colour: '#ccc', bounds: boundsAll};
				this.showCruiseStats();
				this.showCruiseMenu();
				if(boundsAll)
					this.map.fitBounds(boundsAll);
			})
			.catch(err => alert(err));
	}

	loadPlacesAndThings(evt) {
		// Add the loading label.
		let hl = null;
		if(!this.loadEl) {
			hl = document.createElement('div');
			hl.innerHTML = 'Loading...';
			hl.style.fontSize = '1.5em';
			hl.style.position = 'absolute';
			hl.style.zIndex = 4000;
			hl.style.color = 'white';
			hl.style.left = '60px';
			hl.style.top = '10px';
			hl.style.display = 'block';
		}
	}

    showCruiseTrack(cruise_name = null) {
        this.cruise_name = cruise_name;
		const bounds = this.cruiseColours[cruise_name].bounds;
		this.map.flyToBounds(bounds);
	}

	showCruiseStats() {
        const c = document.querySelector('#cruise_count');
		if(c)
			c.innerHTML = this.cruiseCount;
		const d = document.querySelector('#dive_count');
		if(d)
			d.innerHTML = this.diveCount;
	}

    showCruiseMenu() {
        const menu = document.querySelector('#cruise_menu');
        clear(menu);
        if(menu) {
            // Create a label for each of the cruises in the colours dictionary,
            // since they're already there.
            for(let cruise_name of Object.keys(this.cruiseColours).sort()) {
                const colour = this.cruiseColours[cruise_name].colour;
                const d = document.createElement('div');
                const c = document.createElement('div');
                const a = document.createElement('a');
                a.href = 'javascript:void(0)';
                a.addEventListener('mouseover', evt => { evt.preventDefault(); this.showCruiseTrack(cruise_name) });
				a.href = `/rov/cruises/${cruise_name}`;
                a.title = `Show cruise ${cruise_name}.`;
                a.innerText = cruise_name;
                c.className = 'cruise_label_patch';
                d.className = 'cruise_label';
                c.style.backgroundColor = colour;
                d.appendChild(c);
                d.appendChild(a);
                menu.appendChild(d);
            }
        }
    }

	/**
	 * Set up and render the map.
	 */
	show(id) {
		this.id = `#${id}`;
		this.showMap(id);
	   	this.map.setView([52, -133], 5);
	}

	zoomStart(evt) {
		
	}

	moveEnd(evt) {
		this.loadPlacesAndThings();
	}

}

/**
 * Shows a map of protected areas that can be clicked for more information.
 */
export class PAHomeMap extends _Map {

	constructor(id) {
		super(id);
		this.markers = [];				// List of Leaflet markers.
		this.pas = [];                  // A list of PA markers.
		this.pa_data = [];				// A list of PAs.
		this.lastLabel = null; 			// Reference to the last active label.
		this.animations = new Map();	// A list of items currently being animated.
		this.ptXhr = null;
		this.clickFn = (pa) => {
			document.location = `/pa/detail?pa_id=${pa.id}`;
		};
		this.colours = {
		    'MPA': '#07ffbd',
		    'RCA': '#f702ba',
		    'default': '#999999'
		}
	}

	clearMarkers() {
		this.pas.forEach(pa => {
			pa.remove();
		});
	}
	
	loadPAs(paid = 0) {
	    if(paid === false)
	        return;
		// Load the places and things data.
		let bounds = this.map.getBounds();
		let mapdiv = document.querySelector(this.id);
		let hl = document.createElement('div');
		hl.innerHTML = 'Loading PAs...';
		hl.style.fontSize = '1.5em';
		hl.style.position = 'relative';
		hl.style.zIndex = 4000;
		hl.style.color = 'white';
		hl.style.left = '60px';
		hl.style.top = '10px';
		hl.style.display = 'block';
		mapdiv.appendChild(hl);
		
		if(this.ptXhr)
			this.ptXhr.abort();

		this.ptXhr = Net.get(`${Net.API}/pas` + (paid > 0 ? `/${paid}` : ''))
			.then(result => {
				let paFocus = null;

				// Iterate over dive track results, set up objects.
				result.pas.forEach(pa => {
					let d = {
						'type': 'Feature',
						'geometry': JSON.parse(pa.geom),	// Returned as an escaped string.
						'properties': {
							'name': `${pa.name_e} - ${pa.name_f}`.replace(' ', '&nbsp;'),
							'track_count': pa.track_count
						}
					}
					// Add the tracks markers.
					let t = L.geoJSON(d, {
						style: {
							color: pa.track_count > 0 ? this.colours[pa.type] : this.colours['default']
						}
					});
					t.bindPopup(`${pa.name_e} - ${pa.name_f}`);
					t.on('click', evt => {
						this.clickFn(pa);
					});
					this.pas.push(t.addTo(this.map));
                    this.pa_data.push(pa);

					if(pa.pa_id == paid)
						paFocus = t;
				});
			
			if(paFocus) {
				this.map.fitBounds(paFocus.getBounds());
			} else if(this.pas.length) {
				this.map.fitBounds(new L.featureGroup(this.pas).getBounds());
			}

			mapdiv.removeChild(hl);
		});
	}

	/**
	 * Set up and render the map.
	 * \param id The map element ID.
	 * \param params An object containing parameters. Includes
	 */
	show(id, params = {}) {
	    const paid = hasProp(params, 'paid') ? params.paid : 0;
		this.id = `#${id}`;
		this.showMap(id);
	   	this.map.setView([49.294444, -123.111389], 12);
		this.clearMarkers();
   	    this.loadPAs(paid);
	}

}


/**
 * A drawable map used for report generation and searching.
 */
export class DrawMap extends _Map {

	constructor(id) {
		super(id);
		this.marker = null;				// The drawn polygon.
	}

	/**
	 * Set up and render the map.
	 * \param id The map element ID.
	 * \param id An item to focus on.
	 */
	show(id) {
		this.id = `#${id}`;
		this.showMap(id);
	   	this.map.setView([49.294444, -123.111389], 12);

		// FeatureGroup is to store editable layers
		this.drawnItems = new L.FeatureGroup();
		this.drawControl = new L.Control.Draw({
			draw: {
				polyline: false,
				marker: false,
				circlemarker: false,
				circle: false
			},
			edit: false
		});
		this.map.addLayer(this.drawnItems);
	    this.map.addControl(this.drawControl);
	    this.map.on(L.Draw.Event.CREATED, this.shapeCreated.bind(this));
	}

	shapeCreated(e) {
		const type = e.layerType;
		const radius = e.layer.options.radius;
		if(this.drawnLayer)
			this.map.removeLayer(this.drawnLayer);
		this.drawnLayer = e.layer;
		this.map.addLayer(this.drawnLayer);
		this.featureCreated(this.drawnLayer.toGeoJSON());
	}

	featureCreated(feature) {}

}


let _markers = [];
let _map = null;

function _showOnMap(id, data) {

	_map = L.map(id);
	L.tileLayer(google_sat_tile_url(), {
		maxZoom: 20,
		subdomains:['mt0','mt1','mt2','mt3']
	}).addTo(_map);

	// Clear existing markers.
	_markers.forEach(marker => marker.remove());

	if(data) {
		// Parse the JSON data.
		data = JSON.parse(data);
		// Create and add the marker.
		let marker = L.geoJSON(data);
		marker.addTo(_map);
		_markers.push(marker);
		// Fit the map.
		_map.fitBounds(marker.getBounds());
	} else {
		// By default, center on IOS.
    	_map.setView([48.6502888, -123.4495954], 14)		
	}
}


