import { Net, hasProp } from "@/js/util.js";
import { Model } from '@/js/models.js';

export class Group extends Model {

    static modelPath = `${Net.API}/account/groups`;
    static defaultProps = {
        id: 0,
        name: null,
    };

    static loadAll(filter) {
        return Model.loadAll(Group, filter);
    }
}
Model.registerClass(Group, 'Group');


export class User extends Model {

    static categories = ['First Nation', 'Government', 'Commercial', 'NGO', 'Other'];

	static modelPath = `${Net.API}/account`;
	static defaultProps = {
		id: 0,
        password: null,
        last_login: null,
        is_superuser: null,
        username: null,
        first_name: null,
        last_name: null,
        email: null,
        is_staff: null,
        is_active: null,
        date_joined: null,
    
        organization: null,
        org_type: null,
        registration_reason: null,
        bio: null,
        biigle_username: null,
        biigle_api_key: null,
    
        password_reset_code: null,
        password_reset_expiry: null,
        
        verification_code: null,
        verification_expiry: null,
        verification_time: null,
        registration_ip: null,
        ip_in_region: null,
        registration_location: null,
        registration_note: null,
        allowed: true,

        groups: [],
	};
    static classMap = {
        groups: 'Group'
    };

    get isStaff() {
        return this.is_staff === true;
    }

    get isSuperuser() {
        return this.is_superuser === true;
    }

    get isAdmin() {
        return this.inGroup('msea_admin');
    }

    get isImporter() {
        return this.inGroup('msea_importer');
    }

    get isAccountDigestRecipient() {
        return this.inGroup('msea_account_digest_recipient');
    }

    get isDataUpdateRecipient() {
        return this.inGroup('msea_data_update_recipient');
    }

    inGroup(group) {
        if(!group)
            return false;
        let name = group;
        if(hasProp(group, 'name'))
            name = group.name;
        return Array.isArray(this.groups) && this.groups.some(g => g.name == name);
    }

    addGroup(group) {
        if(group && !this.inGroup(group)) {
            this.groups.push(group);
            this.dirty = true;
        }
    }

    removeGroup(group) {
        if(group && this.inGroup(group)) {
            let name = group;
            if(hasProp(group, 'name'))
                name = group.name;
            this.groups.splice(this.groups.findIndex(item => item.name == name), 1);
            this.dirty = true;
        }
    }

    get hasGroups() {
        return Array.isArray(this.groups) && this.groups.length > 0;
    }

    static loadAll(filter) {
        return Model.loadAll(User, filter);
    }

}
Model.registerClass(User, 'User');