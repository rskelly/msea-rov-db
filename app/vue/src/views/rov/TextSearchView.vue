<script setup>
    import { reactive, ref, onMounted } from 'vue';
    import { Cruise } from '@/js/rov/models.js';
    import { SearchMap } from '@/js/map.js';
    import { Net } from '@/js/util.js';

    // Default search fields.
    const defaultFields = ['common_name', 'scientific_name', 'original_label'];

    const pageCount = 7;            // The number of pages in the nav.
    const cruises = reactive([]);   // A list of available cruises.
    const map = new SearchMap();    // The map.
    const searched = ref(false);    // Switched to true on the first search.
    const downloading = ref(false); // True when a download is in progress.

    const searchData = reactive({
        page: 0,                    // The page to navigate to.
        cruise_id: null,               // The current cruise.
        terms: '', 	            // The search term.
        bounds: null,               // The search bounds.
        fields: ['scientific_name', 'common_name'],    // The search fields.
    });

    const resultData = reactive({
        page: 0,                    // The page.
        totalCount: 0,              // The total result count.
        pageSize: 0,                // The number of items in a result page.
        lastPage: 0,                // The index of the last page of results.
        results: [],                // The search results.
        loading: false,             // True when the search is in progress.
        pages: [],                  // Track the number of pages.
        terms: null,                 // The search term.
    });

    /**
     * Handle the search form submit.
     * @param {*} evt 
     */
    const searchSubmit = (evt) => {
        if(evt) evt.preventDefault();
        searchData.page = 0;
        search();
    }

    const search = () => {

        // Sanity-check the fields list.
        const fields = searchData.fields;
        if(!fields.length)
            fields.push(...defaultFields);

        resultData.loading = true;
	    searched.value = true;

        searchData.bounds = map.getBoundsObject();
        
        Net.post(`${Net.API}/text_search/search/`, searchData)
            .then(result => {
                // Populate the results object.
                resultData.page = result.page;
                resultData.totalCount = result.totalCount;
                resultData.pageSize = result.pageSize;
                resultData.lastPage = Math.floor(resultData.totalCount / resultData.pageSize);
                resultData.page = result.page;
                resultData.results.splice(0);
                resultData.results.push(...result.results);
                resultData.terms = searchData.terms;
                resultData.loading = false;
                
                // Go to the first page.
                showPage();
            })
            .catch(err => {
                resultData.loading = false;
                alert(err);
            });
    }

    /**
     * If one of the field checkboxes changes update the list.
     * @param {*} name 
     * @param {*} evt 
     */
    const fieldChange = (name, evt) => {
        if(evt) evt.preventDefault();
        if(evt.target.checked) {
            if(!searchData.fields.includes(name))
                searchData.fields.push(name);
        } else{
            searchData.fields.splice(searchData.fields.indexOf(name), 1);
        }
    };

    /**
     * Retrieve and display the given page of results.
     * @param {*} page 
     * @param {*} evt 
     */
    const goPage = (page) => {
        searchData.page = page;
        search();
    }

    /**
     * Show the current page of results.
     * @param {*} page 
     */
    const showPage = () => {
        // Calculate the nav bar.
        let startPage = 0;
        let endPage = resultData.lastPage - 1;
        if(resultData.lastPage > pageCount) {
            startPage = Math.max(resultData.page - 3, 0);
            endPage = Math.min(startPage + pageCount, resultData.lastPage - 1);
        }
        // Add pages to the list.
        resultData.pages.splice(0);
        while(startPage <= endPage + 1) {
            resultData.pages.push(startPage);
            ++startPage;
        }
        while(resultData.pages.length < pageCount && resultData.lastPage >= pageCount)
            resultData.pages.unshift(resultData.pages[0] - 1);
        // Render the items on the map.
        map.render(resultData.results);
    };

    /**
     * Download a CSV file.
     */
    const download = () => {
        // Sanity-check the fields list.
        downloading.value = true;
        const fields = searchData.fields;
        if(!fields.length)
            fields.push(...defaultFields);
        // Execute the search.
        Net.post(`${Net.API}/text_search/search/`, {
            cruise_id: searchData.cruise_id, 
            terms: searchData.terms, 
            fields: fields, 
            bounds: map.getBoundsObject(),
            download: true
        })
            .then(() => downloading.value = false)
            .catch(err => {
                downloading.value = false;
                alert(err);
            });
    };

    onMounted(() => {
        map.showMap('searchmap');
        // Load the cruises.
        Cruise.loadAll()
            .then(result => {
                // Clear the cruises list and populate it. Cruises are joined with legs.
                cruises.splice(0);
                cruises.push(...result);
            })
            .catch(alert);
    });
</script>

<template>
  <main>
	<section>
        <h1>Observation Search</h1>
        <p>Enter your search term below. Comma-separated terms will be treated as an OR query.</p>
        <p>Select a cruise to narrow your search results to that cruise. Leave un-selected to search in all cruises.</p>
        <p>Zoom the map to restrict your search to the boundaries of the map.</p>
        <form @submit="searchSubmit($event)">
            <table>
                <tr>
                    <td>
                        <label for="cruise">Cruise</label>
                    </td>
                    <td>
                        <select :disabled="searchData.loading" v-model="searchData.cruise_id">
                            <option :value="null">-- All Cruises --</option>
                            <option v-for="cruise in cruises" :value="cruise.id">{{ cruise.name }}-{{ cruise.leg }}</option>
                        </select>
                    </td>
                    <td>
                        <label for="terms">Search Term</label>
                    </td>
                    <td>
                        <input name="terms" id="terms" :disabled="searchData.loading" v-model="searchData.terms" />
                    </td>
                    <td>
			            <button :disabled="resultData.loading || searchData.terms.length == 0"
                            class="material-symbols">search</button>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        Search fields:
                        <span>
                            <label for="fields">Common Name</label>
                            <input type="checkbox" name="fields" value="common_name" alt="Search the common name." 
                                :checked="searchData.fields.includes('common_name')" @change="fieldChange('common_name', $event)" />
                            <label for="fields">Scientific Name</label>
                            <input type="checkbox" name="fields" value="scientific_name" alt="Search the scientific name." 
                                :checked="searchData.fields.includes('scientific_name')" @change="fieldChange('scientific_name', $event)" />
                        </span>
                    </td>
                </tr>
            </table>
        </form>
    </section>
    
    <section v-if="resultData.loading">
        Searching...
    </section>

    <section v-else-if="!resultData.loading && resultData.totalCount > 0">
        <h2>{{ resultData.totalCount }} results for "{{ resultData.terms }}"</h2>
        <div>
            Download results as a CSV file:
	        <button @click.prevent="download()" class="material-symbols">download</button>
            <span v-if="downloading"> Preparing (check for blocked popups)...</span>
        </div>
        <br />
        <div>
            <table class="grid">
                <tbody class="sticky_header">
                    <tr>
                        <td colspan="9">
                            <span v-if="resultData.page > 0">
                                <a href="javascript:void(0)" @click.prevent="goPage(0)" title="First Page" class="nav_btn">&lt;&lt;</a>
                                <a href="javascript:void(0)" @click.prevent="goPage(resultData.page - 1)" title="Previous Page" class="nav_btn">&lt;</a>
                            </span>
                            <span v-else>
                                <span class="nav_btn">&lt;&lt;</span>
                                <span class="nav_btn">&lt;</span>
                            </span>
                            <span v-for="p in resultData.pages">
                                <span v-if="p == resultData.page" class="nav_btn">{{ (p + 1) }}</span>
                                <a v-else href="javascript:void(0)" @click.prevent="goPage(p)" :title="'Page ' + (p + 1)" class="nav_btn">{{ (p + 1) }}</a>
                            </span>
                            <span v-if="resultData.page < (resultData.lastPage - 1)">
                                <a href="javascript:void(0)" @click.prevent="goPage(resultData.page + 1)" title="Next Page" class="nav_btn">&gt;</a>
                                <a href="javascript:void(0)" @click.prevent="goPage(resultData.lastPage)" title="Last Page" class="nav_btn">&gt;&gt;</a>
                            </span>
                            <span v-else>
                                <span class="nav_btn">&gt;</span>
                                <span class="nav_btn">&gt;&gt;</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Row</th><th>Cruise</th><th>Leg</th><th>Dive</th><th>Common Name</th><th>Scientific Name</th>
                        <th>Timestamp</th><th>Position (lon, lat)</th><th>Depth (m)</th>
                    </tr>
                </tbody>
                <tbody>
                    <tr v-for="item in resultData.results">
                        <td>{{ item.row_num }}</td>
                        <td>{{ item.cruise_name }}</td>
                        <td>{{ item.cruise_leg }}</td>
                        <td>{{ item.dive_name }}</td>
                        <td>{{ item.common_name }}</td>
                        <td>{{ item.scientific_name }}</td>
                        <td>{{ item.timestamp }}</td>
                        <td>{{ item.lon.toFixed(4) }}&deg;, {{ item.lat.toFixed(4) }}&deg;</td>
                        <td>{{ item.depth ? item.depth.toFixed(2) + 'm' : '' }}</td>
                    </tr>
                </tbody>                    
            </table>
        </div>
    </section>
    
    <section v-if="!resultData.loading && searched && resultData.totalCount == 0">
        No results found.
    </section>

    <br />

    <section>
		<div id="searchmap" class="map"></div>
		<canvas id="canv" class="map_canv"></canvas>
	</section>

  </main>
</template>
