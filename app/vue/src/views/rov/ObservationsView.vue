<script setup>
    import { reactive, ref, onMounted, resolveDirective } from 'vue';
    import { useRoute } from 'vue-router';
    import { Dive } from "@/js/rov/models.js";
    import { ObservationMap } from '@/js/map.js';

    const route = useRoute();

    const dive = ref(null);         // The current dive.
    const needLoad = ref(true);     // Set to true when a dive needs to be (re)loaded.
    const loading = ref(0);         // Incremented when something is loading; decremented when complete.
    const showPages = 7;            // The number of pages to show in the navigation bar.
    let map = null;                 // Reference to the position map.

    /**
     * Format a date-time as a time string.
     * @param {*} v 
     */
    const time = (v) => {
        if(v && v.indexOf('T') > 0)
            v = v.split('T')[1].split('.')[0];
        v = v.replace('Z', '');
        return v;
    }

    /**
     * Format a date-time as a date string.
     * @param {*} v 
     */
    const date = (v) => {
        if(v && v.indexOf('T') > 0)
            v = v.split('T')[0];
        return v;
    }

    /**
     * Navigation tracking for the different event/object types.
     */
    const datasets = {
        'observation_events': reactive({page: 1, pages: 0, count: 20, total: 0, result: [], fn: 'loadObservationEvents'}),
        'habitat_events': reactive({page: 1, pages: 0, count: 20, total: 0, result: [], fn: 'loadHabitatEvents'}),
        'comment_events': reactive({page: 1, pages: 0, count: 20, total: 0, result: [], fn: 'loadCommentEvents'}),
        'status_events': reactive({page: 1, pages:0, count: 20, total: 0, result: [], fn: 'loadStatusEvents'}),
        'measurement_events': reactive({page: 1, pages:0, count: 20, total: 0, result: [], fn: 'loadMeasurementEvents'}),
        'measurements': reactive({page: 1, pages: 0, count: 20, total: 0, result: [], fn: 'loadMeasurements'}),
        'positions': reactive({page: 1, pages:0, count: 20, total: 0, result: [], fn: 'loadPositions'})
    };

    // Stores column headings for each results section based on the properties available.
    const colMaps = reactive({});
    const colLists = {
        observation_events: [
            'scientific_name', 'common_name', 'aphia_id', 'inaturalist_id', 'hart_code', 
            'otu', 'cf', 'tags', 'life_stage', 'annotation_id', 'label_id'
        ],
        habitat_events: [
            'description', 'scientific_name', 'substrate', 'substrate_coverage', 
            'biocover', 'biocover_coverage', 'coverage', 'relief', 'disturbance', 
            'thickness', 'complexity', 'tags', 'annotation_id', 'label_id'
        ],
        status_events: [
            'status_type', 'status_detail'
        ],
        comment_events: ['comment', 'note'],
        measurement_events: ['quantity', 'type']
    };

    /**
     * Return the keys of the given map, sorted in the order of the given column list.
     * @param cols The map.
     * @param collist The column list.
     */
    const sortKeys = (cols, collist) => {
        return collist.filter(c => cols.has(c));
    };

    /**
     * Return the values of the given map, sorted in the order of the given column list.
     * @param cols The map.
     * @param collist The column list.
     */
    const sortValues = (cols, collist) => {
        return sortKeys(cols, collist).map(c => cols.get(c));
    };

    /**
     * Convert the key to title case by splitting on underscore,
     * applying case conversion and joining with space. If a part
     * is equal to 'Id', is uppercased.
     * @param str 
     */
    const titleCase = (str) => {
        return str.split('_')
            .map(s => s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase())
            .map(s => s == 'Id' ? s.toUpperCase() : s)
            .join(' ');
    };

    /**
     * Load the observations. The name is a key into the datasets object.
     * @param {*} name A key into the datasets object.
     */
    const loadObs = (name) => {
        let data = datasets[name];
        // Initializes sets for the column headings and keys for extracting 
        // the values. Note that Set is ordered.
        colMaps[name] = new Map();
        // Increment the loading counter. When it reaches zero, the load is complete.
        loading.value++;
        dive.value[data.fn]({page: data.page, count: data.count})
            .then(result => {
                // Get the pagination info; default if there is none.
                const p = result.pagination || {page: -1, total: -1, count: -1};
                // Set the pagination info for display.
                data.page = p.page;
                data.total = p.total;
                data.pages = Math.ceil(p.total / p.count);
                // Set the results list.
                data.result = result;
                // For items with instrument config IDs, look up the hosting platform.
                result.forEach(item => {
                    // Set up the column heads and keys based on properties in the item.
                    (colLists[name] || []).forEach(k => {
                        if(k in item.properties) 
                            colMaps[name].set(k, titleCase(k));
                    });
                });
                // Count down the loader.
                loading.value--;
                // For positions, show on the map.
                if(name == 'positions') {
                    // Show the dive map.
                    showTrack(dive.value.id);
                }
            })
            .catch(err => {
                loading.value--;
                alert(err);
            });
    }

    /**
     * Load the dive indicated in the page's hash.
     * The the load isn't needed or the app isn't logged in do not load.
     */
    const loadDive = () => {
        if(!needLoad.value || loading.value)
            return;
        resetPages();
        loading.value++;
        const diveId = parseInt(route.params.diveId);
        if(isNaN(diveId))
            return; // TODO: Hack. Sometimes the hash updates while this page is in memory and raises a mysterious error.
        // Load the dive and associated observations.
        Dive.loadOne(diveId)
            .then(result => {
                dive.value = result;
                needLoad.value = false;
                loading.value--;
                // Load the observation values.
                for(let name of Object.keys(datasets))
                    loadObs(name);
            })
            .catch(err => {
                alert(err);
                loading.value--;
                needLoad.value = true;
            });
    }

    /**
     * Format the SeaTube URL using the dive ID and timestamp.
     */
    const seatubeUrl = (diveId, time) => {
        return `https://data.oceannetworks.ca/app/dive-logs/${diveId}?time=${time}`;
    }

    const showTrack = (id) => {
        if(!map)
            map = new ObservationMap('map');
        map.showDive(id);
    }

    /**
     * Show the page for the given dataset. The name is a key into the datasets object.
     * @param {*} name A key into the datasets object.
     */
    const pages = (name) => {
        let max = datasets[name].pages;         // The last page.
        let p = datasets[name].page;            // The current page.
        let s = p - parseInt(showPages / 2);    // The first page in the nav bar.
        let e = p + parseInt(showPages / 2);    // The last page in the nav bar.
        if(max > showPages) {
            // If there are more pages than can be shown, make sure the bounds don't extend past the max/min.
            while(s < 1) {
                ++s; ++e;
            }
            while(e > max) {
                --s; --e;
            }
        } else {
            // The bounds and extents are the same.
            s = 1;
            e = max;
        }
        // Add the necessary indices to the result.
        let res = [];
        for(let i = s; i <= e; ++i)
            res.push(i);
        return res;
    }

    /**
     * Navigate to the next page of results.
     * @param {*} name 
     */
    const nextPage = (name) => {
        let max = datasets[name].pages;
        if(datasets[name].page < max)
            datasets[name].page++;
        loadObs(name);
    }

    /**
     * Navigate to the previous page of results.
     * @param {*} name 
     */
    const prevPage = (name) => {
        if(datasets[name].page > 1)
            datasets[name].page--;
        loadObs(name);
    }

    /**
     * Navigate to the first page of results.
     * @param {*} name 
     */
    const firstPage = (name) => {
        datasets[name].page = 1;
        loadObs(name);
    }

    /**
     * Navigate to the last page of results.
     * @param {*} name 
     */
    const lastPage = (name) => {
        datasets[name].page = datasets[name].pages;
        loadObs(name);
    }

    /**
     * Go to a specific page of results.
     * @param {*} page 
     * @param {*} name 
     */
    const gotoPage = (page, name) => {
        let max = datasets[name].pages;
        if(page < 1) {
            page = 1;
        } else if(page > max) {
            page = max;
        }
        datasets[name].page = page;
        loadObs(name);
    }

    /**
     * Reset all the page positions for all datasets.
     */
    const resetPages = () => {
        for(let k of Object.keys(datasets)) {
            datasets[k].page = 1;
            datasets[k].pages = 0;
            datasets[k].total = 0;
            datasets[k].result = [];
        }
    };

    /**
     * Format a coordinate pair and return as a comma-delimited string.
     */
    const formatCoords = (coords) => {
        coords = JSON.parse(coords);
        return (coords || []).map(n => n.toFixed(5)).join(', ');
    };

    onMounted(() => {
        loadDive();
    });

</script>

<template>
  <main>
    <h1>Observations</h1>

	<p>This page provides observations, status events, comments, navigation and sensor data.</p>

    <section id="observation">

        <h2>Observation Events</h2>
        <p>These are observations of organisms and anthropogenic phenomena derived from annotations or live observation of video feeds.</p>
        
        <table v-if="datasets.observation_events.result.length > 0" class="grid nowrap">
        
            <tr v-if="datasets.observation_events.total > datasets.observation_events.count">
                <td :colspan="colMaps['observation_events'].size + 3">    
                    <span v-if="datasets.observation_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="firstPage('observation_events')">&lt;&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&lt;&nbsp;</span>
                    <span v-if="datasets.observation_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="prevPage('observation_events')">&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&nbsp;</span>
                    <span v-for="page in pages('observation_events')">
                        <span v-if="page != datasets.observation_events.page">
                            <a href="javascript:void(0)" v-on:click="gotoPage(page, 'observation_events')">{{ page }}</a>&nbsp;
                        </span>
                        <span v-else>{{ page }}&nbsp;</span>
                    </span>
                    <span v-if="datasets.observation_events.page < datasets.observation_events.pages">
                        <a href="javascript:void(0)" v-on:click="nextPage('observation_events')">></a>&nbsp;
                    </span>
                    <span v-else>&gt;&nbsp;</span>
                    <span v-if="datasets.observation_events.page < datasets.observation_events.pages">
                        <a href="javascript:void(0)" v-on:click="lastPage('observation_events')">&gt;&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&gt;&nbsp;</span>
                </td>
            </tr>

            <tr>
                <th>Timestamp</th>
                <th v-for="head in sortValues(colMaps['observation_events'], colLists['observation_events'])">{{ head }}</th>
                <th>Meta</th>
                <th>Seatube Link</th>
            </tr>
            
            <tr v-for="o in datasets.observation_events.result">
                
                <!-- The time range. -->
                <td>{{ o.start_time }}<template v-if="o.end_time"> - {{ o.end_time }}</template></td>
                
                <!-- Conditional rendering of properties depending on key value. -->
                <template v-for="key in sortKeys(colMaps['observation_events'], colLists['observation_events'])">
                    <td v-if="key == 'inaturalist_id'">
                        <a :href="'https://www.inaturalist.org/taxa/' + o.properties.inaturalist_id"
                        target="_blank">{{ o.properties.inaturalist_id }}</a>
                    </td>
                    <td v-else-if="key == 'aphia_id'">
                        <a :href="'https://marinespecies.org/aphia.php?p=taxdetails&id=' + o.properties.aphia_id" 
                        target="_blank">{{ o.properties.aphia_id }}</a>
                    </td>
                    <td v-else-if="key == 'tags'" >{{ (o.properties.tags || []).join(', ')}}</td>
                    <td v-else>{{ o.properties[key] }}</td>
                </template>
                
                <!-- Link to annotation job. -->
                <td v-if="o.annotation_job">
                    <a :href="'/rov/annotation_job/' + o.annotation_job" 
                        title="View annotation job" target="_blank">View</a>
                </td>
                <td v-else></td>

                <!-- Link to Seatube. -->
                <td v-if="dive.seatube_id">
                    <a :href="seatubeUrl(dive.seatube_id, o.start_time)"
                        target="_blank" :title="'SeaTube video at ' + time(o.start_time)">{{ time(o.start_time) }}</a>
                </td>
                <td v-else></td>

            </tr>
        </table>
        <span v-else-if="loading">Loading...</span>
        <span v-else>None Recorded</span>
    </section>

    <section id="habitat">

        <h2>Habitat Events</h2>
        <p>These are observations of habitat derived from annotations or live observation of video feeds.</p>
        
        <table v-if="datasets.habitat_events.result.length > 0" class="grid nowrap">

            <tr v-if="datasets.habitat_events.total > datasets.habitat_events.count">
                <td :colspan="colMaps['habitat_events'].size + 3">
                    <span v-if="datasets.habitat_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="firstPage('habitat_events')">&lt;&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&lt;&nbsp;</span>
                    <span v-if="datasets.habitat_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="prevPage('habitat_events')">&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&nbsp;</span>
                    <span v-for="page in pages('habitat_events')">
                        <span v-if="page != datasets.habitat_events.page">
                            <a href="javascript:void(0)" v-on:click="gotoPage(page, 'habitat_events')">{{ page }}</a>&nbsp;
                        </span>
                        <span v-else>{{ page }}&nbsp;</span>
                    </span>
                    <span v-if="datasets.habitat_events.page < datasets.habitat_events.pages">
                        <a href="javascript:void(0)" v-on:click="nextPage('habitat_events')">&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&nbsp;</span>
                    <span v-if="datasets.habitat_events.page < datasets.habitat_events.pages">
                        <a href="javascript:void(0)" v-on:click="lastPage('habitat_events')">&gt;&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&gt;&nbsp;</span>
                </td>
            </tr>
            
            <tr><th>Timestamp</th>
                <th v-for="head in sortValues(colMaps['habitat_events'], colLists['habitat_events'])">{{ head }}</th>
                <th>Meta</th>
                <th>SeaTube</th>
            </tr>
            
            <tr v-for="o in datasets.habitat_events.result">

                <!-- The time range. -->
                <td>{{ o.start_time }}<template v-if="o.end_time"> - {{ o.end_time }}</template></td>
                
                <!-- Conditional rendering of properties depending on key value. -->
                <template v-for="key in sortKeys(colMaps['habitat_events'], colLists['habitat_events'])">
                    <td v-if="key == 'inaturalist_id'">
                        <a :href="'https://www.inaturalist.org/taxa/' + o.properties.inaturalist_id"
                        target="_blank">{{ o.properties.inaturalist_id }}</a>
                    </td>
                    <td v-else-if="key == 'aphia_id'">
                        <a :href="'https://marinespecies.org/aphia.php?p=taxdetails&id=' + o.properties.aphia_id" 
                        target="_blank">{{ o.properties.aphia_id }}</a>
                    </td>
                    <td v-else-if="key == 'tags'" >{{ (o.properties.tags || []).join(', ')}}</td>
                    <td v-else>{{ o.properties[key] }}</td>
                </template>
                
                <!-- Link to annotation job. -->
                <td v-if="o.annotation_job">
                    <a :href="'/rov/annotation_job/' + o.annotation_job" 
                        title="View annotation job" target="_blank">View</a>
                </td>
                <td v-else></td>

                <!-- Link to Seatube. -->
                <td v-if="dive.seatube_id">
                    <a :href="seatubeUrl(dive.seatube_id, o.start_time)"
                        target="_blank" :title="'SeaTube video at ' + time(o.start_time)">{{ time(o.start_time) }}</a>
                </td>
                <td v-else></td>

            </tr>
        </table>
        <span v-else-if="loading">Loading...</span>
        <span v-else>None Recorded</span>
    </section>

    <section id="status">

        <h2>Status Events</h2>
        <p>These are submersible or mission status events.</p>

        <table v-if="datasets.status_events.result.length > 0" class="grid nowrap">

            <tr v-if="datasets.status_events.total > datasets.status_events.count">
                <td :colspan="colMaps['status_events'].size + 3">
                    <span v-if="datasets.status_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="firstPage('status_events')">&lt;&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&lt;&nbsp;</span>
                    <span v-if="datasets.status_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="prevPage('status_events')">&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&nbsp;</span>
                    <span v-for="page in pages('status_events')">
                        <span v-if="page != datasets.status_events.page">
                            <a href="javascript:void(0)" v-on:click="gotoPage(page, 'status_events')">{{ page }}</a>&nbsp;
                        </span>
                        <span v-else>{{ page }}&nbsp;</span>
                    </span>
                    <span v-if="datasets.status_events.page < datasets.status_events.pages">
                        <a href="javascript:void(0)" v-on:click="nextPage('status_events')">&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&nbsp;</span>
                    <span v-if="datasets.status_events.page < datasets.status_events.pages">
                        <a href="javascript:void(0)" v-on:click="lastPage('status_events')">&gt;&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&gt;&nbsp;</span>
                </td>
            </tr>

            <tr>
                <th>Timestamp</th>
                <th v-for="head in sortValues(colMaps['status_events'], colLists['status_events'])">{{ head }}</th>
                <th>Meta</th>
                <th>SeaTube</th>
            </tr>                
            
            <tr v-for="o in datasets.status_events.result">

                <!-- The time range. -->
                <td>{{ o.start_time }}<template v-if="o.end_time"> - {{ o.end_time }}</template></td>
                
                <!-- Conditional rendering of properties depending on key value. -->
                <template v-for="key in sortKeys(colMaps['status_events'], colLists['status_events'])">
                    <td v-if="key == 'tags'" >{{ (o.properties.tags || []).join(', ')}}</td>
                    <td v-else>{{ o.properties[key] }}</td>
                </template>
                
                <!-- Link to annotation job. -->
                <td v-if="o.annotation_job">
                    <a :href="'/rov/annotation_job/' + o.annotation_job" 
                        title="View annotation job" target="_blank">View</a>
                </td>
                <td v-else></td>

                <!-- Link to Seatube. -->
                <td v-if="dive.seatube_id">
                    <a :href="seatubeUrl(dive.seatube_id, o.start_time)"
                        target="_blank" :title="'SeaTube video at ' + time(o.start_time)">{{ time(o.start_time) }}</a>
                </td>
                <td v-else></td>

            </tr>
        </table>
        <span v-else-if="loading">Loading...</span>
        <span v-else>None Recorded</span>
    </section>

    <section id="comment">

        <h2>Comment Events</h2>
        <p>These are comments collected from any source during a mission.</p>
        
        <table v-if="datasets.comment_events.result.length > 0" class="grid nowrap">
            
            <tr v-if="datasets.comment_events.total > datasets.comment_events.count">
                <td :colspan="colMaps['comment_events'].size + 3">
                    <span v-if="datasets.comment_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="firstPage('comment_events')">&lt;&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&lt;&nbsp;</span>
                    <span v-if="datasets.comment_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="prevPage('comment_events')">&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&nbsp;</span>
                    <span v-for="page in pages('comment_events')">
                        <span v-if="page != datasets.comment_events.page">
                            <a href="javascript:void(0)" v-on:click="gotoPage(page, 'comment_events')">{{ page }}</a>&nbsp;
                        </span>
                        <span v-else>{{ page }}&nbsp;</span>
                    </span>
                    <span v-if="datasets.comment_events.page < datasets.comment_events.pages">
                        <a href="javascript:void(0)" v-on:click="nextPage('comment_events')">&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&nbsp;</span>
                    <span v-if="datasets.comment_events.page < datasets.comment_events.pages">
                        <a href="javascript:void(0)" v-on:click="lastPage('comment_events')">&gt;&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&gt;&nbsp;</span>
                </td>
            </tr>

            <tr>
                <th>Timestamp</th>
                <th v-for="head in sortValues(colMaps['comment_events'], colLists['comment_events'])">{{ head }}</th>
                <th>Meta</th>
                <th>SeaTube</th>
            </tr>
            
            <tr v-for="o in datasets.comment_events.result">

                <!-- The time range. -->
                <td>{{ o.start_time }}<template v-if="o.end_time"> - {{ o.end_time }}</template></td>

                <!-- Conditional rendering of properties depending on key value. -->
                <template v-for="key in sortKeys(colMaps['comment_events'], colLists['comment_events'])">
                    <td v-if="key == 'tags'" >{{ (o.properties.tags || []).join(', ')}}</td>
                    <td v-else>{{ o.properties[key] }}</td>
                </template>
                
                <!-- Link to annotation job. -->
                <td v-if="o.annotation_job">
                    <a :href="'/rov/annotation_job/' + o.annotation_job" 
                        title="View annotation job" target="_blank">View</a>
                </td>
                <td v-else></td>

                <!-- Link to Seatube. -->
                <td v-if="dive.seatube_id">
                    <a :href="seatubeUrl(dive.seatube_id, o.start_time)"
                        target="_blank" :title="'SeaTube video at ' + time(o.start_time)">{{ time(o.start_time) }}</a>
                </td>
                <td v-else></td>

            </tr>
        </table>
        <span v-else-if="loading">Loading...</span>
        <span v-else>None Recorded</span>
    </section>

    <section id="measurement">

        <h2>Measurement Events</h2>
        <p>These are measurements collected by human operators during a mission. Distinct from machine-collected measurements (below).</p>

        <table v-if="datasets.measurement_events.result.length > 0" class="grid nowrap">

            <tr v-if="datasets.measurement_events.total > datasets.measurement_events.count">
                <td :colspan="colMaps['measurement_events'].size + 3">
                    <span v-if="datasets.measurement_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="firstPage('measurement_events')">&lt;&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&lt;&nbsp;</span>
                    <span v-if="datasets.measurement_events.page > 1">
                        <a href="javascript:void(0)" v-on:click="prevPage('measurement_events')">&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&nbsp;</span>
                    <span v-for="page in pages('measurement_events')">
                        <span v-if="page != datasets.measurement_events.page">
                            <a href="javascript:void(0)" v-on:click="gotoPage(page, 'measurement_events')">{{ page }}</a>&nbsp;
                        </span>
                        <span v-else>{{ page }}&nbsp;</span>
                    </span>
                    <span v-if="datasets.measurement_events.page < datasets.measurement_events.pages">
                        <a href="javascript:void(0)" v-on:click="nextPage('measurement_events')">&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&nbsp;</span>
                    <span v-if="datasets.measurement_events.page < datasets.measurement_events.pages">
                        <a href="javascript:void(0)" v-on:click="lastPage('measurement_events')">&gt;&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&gt;&nbsp;</span>
                </td>
            </tr>

            <tr>
                <th>Timestamp</th>
                <th v-for="head in sortValues(colMaps['measurement_events'], colLists['measurement_events'])">{{ head }}</th>
                <th>Meta</th>
                <th>SeaTube</th>
            </tr>
            
            <tr v-for="o in datasets.measurement_events.result">

                <!-- The time range. -->
                <td>{{ o.start_time }}<template v-if="o.end_time"> - {{ o.end_time }}</template></td>

                <!-- Conditional rendering of properties depending on key value. -->
                <template v-for="key in sortKeys(colMaps['measurement_events'], colLists['measurement_events'])">
                    <td v-if="key == 'tags'" >{{ (o.properties.tags || []).join(', ')}}</td>
                    <td v-else>{{ o.properties[key] }}</td>
                </template>
                
                <!-- Link to annotation job. -->
                <td v-if="o.annotation_job">
                    <a :href="'/rov/annotation_job/' + o.annotation_job" 
                        title="View annotation job" target="_blank">View</a>
                </td>
                <td v-else></td>

                <!-- Link to Seatube. -->
                <td v-if="dive.seatube_id">
                    <a :href="seatubeUrl(dive.seatube_id, o.start_time)"
                        target="_blank" :title="'SeaTube video at ' + time(o.start_time)">{{ time(o.start_time) }}</a>
                </td>
                <td v-else></td>
            </tr>

        </table>
        <span v-else-if="loading">Loading...</span>
        <span v-else>None Recorded</span>
    </section>

    <section id="measurement">

        <h2>Measurements</h2>
        <p>These are machine-collected measurements.</p>
        
        <table v-if="datasets.measurements.result.length > 0" class="grid nowrap">
        
            <tr v-if="datasets.measurements.total > datasets.measurements.count">
                <td colspan="5">
                    <span v-if="datasets.measurements.page > 1">
                        <a href="javascript:void(0)" v-on:click="firstPage('measurements')">&lt;&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&lt;&nbsp;</span>
                    <span v-if="datasets.measurements.page > 1">
                        <a href="javascript:void(0)" v-on:click="prevPage('measurements')">&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&nbsp;</span>
                    <span v-for="page in pages('measurements')">
                        <span v-if="page != datasets.measurements.page">
                            <a href="javascript:void(0)" v-on:click="gotoPage(page, 'measurements')">{{ page }}</a>&nbsp;
                        </span>
                        <span v-else>{{ page }}&nbsp;</span>
                    </span>
                    <span v-if="datasets.measurements.page < datasets.measurements.pages">
                        <a href="javascript:void(0)" v-on:click="nextPage('measurements')">&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&nbsp;</span>
                    <span v-if="datasets.measurements.page < datasets.measurements.pages">
                        <a href="javascript:void(0)" v-on:click="lastPage('measurements')">&gt;&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&gt;&nbsp;</span>
                </td>
            </tr>

            <tr>
                <th>Timestamp</th>
                <th>Quantity</th>
                <th>Type</th>
                <th>SeaTube</th>
            </tr>

            <tr v-for="o in datasets.measurements.result">
                <td>{{ o.timestamp }}</td>
                <td>{{ o.quantity }}</td>
                <td>{{ o.measurement_type.name }} ({{ o.measurement_type.unit }})</td>
                <td v-if="dive.seatube_id">
                    <a :href="seatubeUrl(dive.seatube_id, o.start_time)"
                        target="_blank" :title="'SeaTube video at ' + time(o.timestamp)">{{ time(o.timestamp) }}</a>
                </td>
                <td v-else></td>
            </tr>
        </table>
        <span v-else-if="loading">Loading...</span>
        <span v-else>None Recorded</span>
    </section>

    <section id="position">

        <h2>Positions</h2>
        <p>These are positions of both the ship (if available) and the submersible.</p>
        
        <table v-if="datasets.positions.result.length > 0" class="grid nowrap">

            <tr v-if="datasets.positions.total > datasets.positions.count">
                <td colspan="5">
                    <span v-if="datasets.positions.page > 1">
                        <a href="javascript:void(0)" v-on:click="firstPage('positions')">&lt;&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&lt;&nbsp;</span>
                    <span v-if="datasets.positions.page > 1">
                        <a href="javascript:void(0)" v-on:click="prevPage('positions')">&lt;</a>&nbsp;
                    </span>
                    <span v-else>&lt;&nbsp;</span>
                    <span v-for="page in pages('positions')">
                        <span v-if="page != datasets.positions.page">
                            <a href="javascript:void(0)" v-on:click="gotoPage(page, 'positions')">{{ page }}</a>&nbsp;
                        </span>
                        <span v-else>{{ page }}&nbsp;</span>
                    </span>
                    <span v-if="datasets.positions.page < datasets.positions.pages">
                        <a href="javascript:void(0)" v-on:click="nextPage('positions')">&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&nbsp;</span>
                    <span v-if="datasets.positions.page < datasets.positions.pages">
                        <a href="javascript:void(0)" v-on:click="lastPage('positions')">&gt;&gt;</a>&nbsp;
                    </span>
                    <span v-else>&gt;&gt;&nbsp;</span>
                </td>
            </tr>

            <tr>
                <th>Time</th>
                <th>Position</th>
                <th>SeaTube</th>
            </tr>

            <tr v-for="o in datasets.positions.result">
                <td>{{ o.timestamp }}</td>
                <td>({{ formatCoords(o.coordinates) }})</td>
                <td v-if="dive.seatube_id">
                    <a :href="seatubeUrl(dive.seatube_id, o.start_time)"
                        target="_blank" :title="'SeaTube video at ' + time(o.timestamp)">{{ time(o.timestamp) }}</a>
                </td>
                <td v-else></td>
            </tr>

        </table>
        <span v-else-if="loading">Loading...</span>
        <span v-else>None Recorded</span>

        <h3>Map</h3>
        <div id="map" style="height:500px;width:100%"></div>

    </section>

  </main>
</template>
