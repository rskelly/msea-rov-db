<script setup>
    import { reactive, ref, onMounted, onUnmounted } from 'vue';
    import { useRoute } from 'vue-router'
    import { Cruise, Platform } from "@/js/rov/models.js";
    import { Net } from '@/js/util.js';

    const route = useRoute();

    const taxa = reactive([]);              // A list of taxa found during the cruise.
    const cruises = reactive([]);           // List of cruises.
    const cruise = ref(null);               // Reference to the current cruise.
    const ship = ref(null);                 // Reference to the ship.
    const dives = reactive([]);             // List of dives on the selected cruise.
    const programs = reactive([]);          // List of programs for the selected cruise.
    const taxaLoading = ref(false);         // True when taxa are loading.
    const cruiseLoading = ref(false);       // True when the cruise is loading.
    const divesLoading = ref(0);            // Zero when no dives are loading, >0 when at least one is.
    const programsLoading = ref(false);     // True when the programs are loading.
    const platformLoading = ref(false);     // True when the platform is loading.
    const transectCount = ref(0);           // The count of all transects.
    const cruiseMap = new Map();            // A map of cruises keyed by name.
    let scrollTo = null;                    // If a hash is given, scroll to it after load.

    /**
     * Load the given cruise's child objects (dives, programs).
     * @param {*} c 
     */
    const loadCruise = (c) => {
        cruise.value = c;
        taxaLoading.value = true;
        taxa.splice(0);
        c.loadTaxa()
            .then(result => {
                taxa.splice(0);
                taxa.push(...result);
                taxaLoading.value = false;
            });
        divesLoading.value = 1;
        dives.splice(0);
        c.loadDives()
            .then(result => {
                // Update the dives list.
                dives.splice(0);
                dives.push(...result);
                // Update the transect count.
                transectCount.value = 0;
                dives.forEach(dive => transectCount.value += dive.transects ? dive.transects.length : 0);
                // Use the dives list (which is reactive) here, not the result. 
                dives.forEach(dive => { 
                    divesLoading.value++;
                    dive.loadDataCounts()
                        .then(() => {
                            divesLoading.value--;
                        });
                });
                divesLoading.value--;
            });
        programsLoading.value = true;
        programs.splice(0);
        c.loadPrograms()
            .then(result => {
                programs.splice(0);
                programs.push(...result);
                programsLoading.value = false;
            });
        platformLoading.value = true;
        Platform.loadOne(c.ship.id)
            .then(result => {
                ship.value = result;
                platformLoading.value = false;
            });
    };

    /**
     * Download species observations for the cruise.
     * @param {*} evt 
     */
    const observationDownload = (evt) => {
        if(evt) evt.preventDefault();
        const cruiseId = cruise.value ? `${cruise.value.name}-${cruise.value.leg}` : 'all';
        Net.post(`${Net.API}/observation_events/download/`, {cruise: cruiseId});
    }
    
    /**
     * Download dives or transects for the cruise.
     * @param {*} evt 
     */
     const lineDownload = (evt, type) => {
        if(evt) evt.preventDefault();
        const cruiseId = cruise.value ? `${cruise.value.name}-${cruise.value.leg}` : 'all';
        // Ensure the type is correct.
        type = type == 'dives' ? type : 'transects';
        const select = evt.target.elements['format'];
        const format = select.selectedOptions[0].value;
        Net.post(`${Net.API}/${type}/download/`, {cruise: cruiseId, fmt: format});
    }

    /**
     * Select the initial cruise from the URL.
     */
    const initialCruise = () => {
        scrollTo = document.location.hash.substring(1);
        let key = route.params.cruise_name;
        if(cruiseMap.has(key))
            loadCruise(cruiseMap.get(key));
    }

    /**
     * Handle cruise selection from the list.
     * @param {*} evt 
     */
    const selectCruise = (evt) => {
        evt.preventDefault();
        let c = cruises[evt.target.selectedIndex - 1];
        if(c) {
            let name = `${c.name}-${c.leg}`;
            let url = `/rov/cruises/${name}`;
            history.pushState(name, '', url);
            loadCruise(c);
        } else {
            history.pushState('', '', '/rov/cruises/');
        }
    }

    /**
     * Format the time for display.
     * @param {*} v 
     */
    const time = (v) => {
        if(v && v.indexOf('T') > 0)
            v = v.split('T')[1].split('.')[0].replace('Z', '');
        return v;
    }

    /**
     * Format the date for display.
     * @param {*} v 
     */
    const date = (v) => {
        if(v && v.indexOf('T') > 0)
            v = v.split('T')[0];
        return v;
    }

    /**
     * Load the cruises.
     */
    const loadCruises = () => {
        if(!cruises.length && !cruiseLoading.value) {
            // If logged in but nothing is loaded, load the cruises. The app will keep trying if the list length is zero.
            cruiseLoading.value = true;
            Cruise.loadAll()
                .then(result => {
                    cruises.push(...result);
                    result.forEach(c => {
                        cruiseMap.set(`${c.name}-${c.leg}`, c);
                    });
                    cruiseLoading.value = false;
                    initialCruise();
                });
        }
    };

    const urlChange = (evt) => {
        try {
                loadCruise(cruiseMap.get(evt.state));
            } catch(err) {}
    };

    onMounted(() => {
        // When the forward/back buttons are used, load the appropriate cruise from the history.
        addEventListener('popstate', urlChange);
        // Load all cruises.
        loadCruises();
    });

    onUnmounted(() => {
        removeEventListener('popstate', urlChange);
    });

</script>

<template>
  <main>
    <h1>Cruises</h1>
    
	<p>This page provides metadata about cruises, programs, dives and transects.</p>

    <table>
        <tr>
            <th><label for="cruise_select">Select a Cruise</label></th>
            <td>
                <select id="cruise_select" @change="selectCruise($event)" v-model="cruise">
                    <option v-if="cruiseLoading" disabled selected="selected" :value="null">-- Loading... --</option>
                    <option v-else selected="selected" :value="null">-- Select One--</option>
                    <option v-for="cr in cruises" :value="cr">{{ cr.name + ' - ' + cr.leg }}</option>
                </select>
            </td>
        </tr>
    </table>

    <section v-if="!cruise">
        <h2>Data from All Cruises</h2>
        <p>These are downloads that contain data covering all cruises.</p>
        <table class="grid">
            <tr><td>
                <h4>Species Observations</h4>
                <p>Species observations are provided in tabular form with common and scientific names, the IDs of the species in various databases (
                    <a href="https://www.marinespecies.org" title="WoRMS site." target="_blank">WoRMS</a>, 
                    <a href="https://inaturalist.org" title="iNaturalist site." target="_blank">iNaturalist</a>, Hart), the geographic coordinate of the
                    observation and the depth.
                </p>
                <p>Download species observations as
                    <form @submit="observationDownload($event)" method="get">
                        <select name="format">
                            <option value="csv">CSV</option>
                        </select>
                        <button class="material-symbols">download</button>
                    </form>
                </p>
            </td></tr>
            <tr><td>
                <h4>Dives</h4>
                <p>A dive is the period between launch and retrieval of an ROV, submersible or drop camera, or between the start and end of telemetry
                    records. It depends on how the data were recorded. Dives are provided as line strings with the start and end times and other metadata.</p>
                <p>Download dives as
                    <form @submit="lineDownload($event, 'dives')" method="get">
                        <select name="format">
                            <option value="spatialite">Spatialite</option>
                            <option value="shapefile">ESRI Shapefile</option>
                        </select>
                        <button class="material-symbols">download</button>
                    </form>
                </p>
            </td></tr>
        </table>        
    </section>

    <section v-if="cruise">
        <h2>Cruise {{ cruise.name }} - {{ cruise.leg }}</h2>
        <table class="grid">
            <tr>
                <th>Dates</th>
                <td>{{ date(cruise.start_time) }} to {{ date(cruise.end_time) }}</td>
            </tr>
            <tr>
                <th>Ship</th>
                <td v-if="ship">{{ ship.name }}</td>
                <td v-else></td>
            </tr>
            <tr>
                <th>Objective</th>
                <td v-if="cruise.objective"><pre>{{ cruise.objective }}</pre></td>
                <td v-else>None Recorded</td>
            </tr>
            <tr>
                <th>Summary</th>
                <td v-if="cruise.summary"><pre>{{ cruise.summary }}</pre></td>
                <td v-else>None Recorded</td>
            </tr>
            <tr>
                <th>Notes</th>
                <td v-if="cruise.note"><pre>{{ cruise.note }}</pre></td>
                <td v-else>None Recorded</td>
            </tr>
            <tr>
                <th>Crew</th>
                <td>
                    <table v-if="cruise.crew && cruise.crew.length > 0" class="grid" width="100%">
                        <tr><th>Name</th><th>Role</th></tr>
                        <tr v-for="cm in cruise.crew">
                            <td>{{ cm.person.first_name + ' ' + cm.person.last_name }}</td>
                            <td>{{ cm.cruise_role.name }}</td>
                        </tr>
                    </table>
                    <span v-else>None Recorded</span>
                </td>
            </tr>
            <tr>
                <th>Documents</th>
                <td v-if="cruise.documents && cruise.documents.length > 0">
                    <div style="height: 150px; overflow-y: scroll;">
                        <div v-for="doc in cruise.documents"><a :href='Net.API + "/downloads/" + doc.file.id + "/"' target="_blank">{{ doc.title }}</a></div>
                    </div>
                </td>
                <td v-else>None</td>
            </tr>
            <tr>
                <th>Observed Taxa</th>
                <td v-if="taxa.length > 0">
                    <p>These are taxa that have been observed an annotated turing the cruise and are not restricted. 
                        A full list of observations can be downloaded below.</p>
                    <table class="grid" style="display: block;max-height: 300px;overflow-y: scroll;">
                        <tr><th>Scientific Name</th><th>Common Name</th><th>OTU</th><th>Cf.</th><th>Hart</th><th>iNaturalist</th><th>Aphia</th></tr>
                        <tr v-for="taxon in taxa">
                            <td>{{ taxon.scientific_name }}</td>
                            <td>{{ taxon.common_name }}</td>
                            <td>{{ taxon.otu }}</td>
                            <td>{{ taxon.cf }}</td>
                            <td>{{ taxon.hart_code }}</td>
                            <td v-if="taxon.inaturalist_id">
                                <a :href="'https://www.inaturalist.org/taxa/' + taxon.inaturalist_id"
                                    target="_blank">{{ taxon.inaturalist_id }}</a></td>
                            <td v-else></td>
                            <td v-if="taxon.aphia_id">
                                <a :href="'https://marinespecies.org/aphia.php?p=taxdetails&id=' + taxon.aphia_id" 
                                    target="_blank">{{ taxon.aphia_id }}</a>
                            </td>
                            <td v-else></td>
                        </tr>
                    </table>
                </td>
                <td v-else>None</td>
            </tr>
            <tr>
                <th>Data</th>
                <td>
                    <p>Species observations are provided in tabular form with common and scientific names, the IDs of the species in various databases (
                        <a href="https://www.marinespecies.org" title="WoRMS site." target="_blank">WoRMS</a>, 
                        <a href="https://inaturalist.org" title="iNaturalist site." target="_blank">iNaturalist</a>, Hart), the geographic coordinate of the
                        observation and the depth.
                    </p>
                    <p>Download species observations as
                        <form @submit="observationDownload($event)" method="get">
                            <select name="format">
                                <option value="csv">CSV</option>
                            </select>
                            <button class="material-symbols">download</button>
                        </form>
                    </p>
                    <p>A dive is the period between launch and retrieval of an ROV, submersible or drop camera, or between the start and end of telemetry
                        records. It depends on how the data were recorded. Dives are provided as line strings with the start and end times and other metadata.</p>
                    <p>Download dives as
                        <form @submit="lineDownload($event, 'dives')" method="get">
                            <select name="format">
                                <option value="spatialite">Spatialite</option>
                                <option value="shapefile">ESRI Shapefile</option>
                            </select>
                            <button class="material-symbols">download</button>
                        </form>
                    </p>
                    <p>A transect represents a period of time during a dive when observations are meant to be recorded or
                        analyzed. Some dives do not have transects. In those cases, this section is disabled.</p>
                    <p>Download transects as
                        <form @submit="lineDownload($event, 'transects')" method="get">
                            <select name="format">
                                <option value="spatialite">Spatialite</option>
                                <option value="shapefile">ESRI Shapefile</option>
                            </select>
                            <button :disabled="transectCount == 0" class="material-symbols">download</button>
                        </form>
                    </p>
                </td>
            </tr>
            <tr v-if="cruise.project">
                <th>Import Information</th>
                <td>
                    <table class="grid">
                        <tr v-if="cruise.project.members.length > 0">
                            <th>Imported By</th>
                            <td v-for="pm  in cruise.project.members">{{ pm.person.first_name + ' ' + pm.person.last_name }}</td>
                        </tr>
                        <tr v-if="cruise.project.objective">
                            <th>Objective</th>
                            <td><pre>{{ cruise.project.objective }}</pre></td>
                        </tr>
                        <tr v-if="cruise.project.note">
                            <th>Notes</th>
                            <td><pre>{{ cruise.project.note }}</pre></td>
                        </tr>
                        <tr>
                            <th>Created On</th>
                            <td>{{ date(cruise.project.start_date) }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr v-if="programs.length > 0">
                <th>Programs</th>
                <td>
                    <table class="grid">
                        <tbody v-for="program in programs">
                            <tr>
                                <th>Program Name</th>
                                <td>{{ program.name }}</td>
                            </tr>
                            <tr>
                                <th>Dates</th>
                                <td>{{ date(program.start_date) }} to {{ date(program.end_date) }}</td>
                            </tr>
                            <tr>
                                <th>Objective</th>
                                <td v-if="program.objective"><pre>{{ program.objective }}</pre></td>
                                <td v-else>None Recorded</td>
                            </tr>
                            <tr>
                                <th>Summary</th>
                                <td v-if="program.summary"><pre>{{ program.summary }}</pre></td>
                                <td v-else>None Recorded</td>
                            </tr>
                            <tr>
                                <th>Notes</th>
                                <td v-if="program.note"><pre>{{ program.note }}</pre></td>
                                <td v-else>None Recorded</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>

        <section v-if="dives.length > 0">
            <h2>Dives</h2>

            <template v-for="dive in dives">

                <a :name="dive.name"></a>
                <h3>Dive {{ dive.name }}</h3>
                <table class="grid">
                    <tr>
                        <th>Start Time</th>
                        <td>{{ date(dive.start_time) + ' ' + time(dive.start_time) }}</td>
                    </tr>
                    <tr>
                        <th>End Time</th>
                        <td>{{ date(dive.end_time) + ' ' + time(dive.end_time) }}</td>
                    </tr>
                    <tr>
                        <th>Platform</th>
                        <td>{{ dive.sub_config.platform.name }}</td>
                    </tr>
                    <tr>
                        <th>Objective</th>
                        <td v-if="dive.objective"><pre>{{ dive.objective }}</pre></td>
                        <td v-else>None Recorded</td>
                    </tr>
                    <tr>
                        <th>Notes</th>
                        <td v-if="dive.note"><pre>{{ dive.note }}</pre></td>
                        <td v-else>None Recorded</td>
                    </tr>
                    <tr v-if="dive.seatube_id">
                        <th>SeaTube Link</th>
                        <td><a :href="'https://data.oceannetworks.ca/SeaTubeV3?resourceTypeId=600&resourceId=' + dive.seatube_id" 
                            target="_blank" title="Link to SeaTube video.">SeaTube video.</a></td>
                    </tr>

                    <tr>
                        <th>Observations</th>
                        <td>
                            <!-- Show the observations and measurements -->
                            <table class="grid" style="width:100%">
                                <tr>
                                    <td>Observation Events</td>
                                    <td><a :href="'/rov/observations/' + dive.id + '#observation'" target="records">{{ dive.dataCounts.observation_event }}</a></td>
                                    <td>Habitat Events</td>
                                    <td><a :href="'/rov/observations/' + dive.id + '#habitat'" target="records">{{ dive.dataCounts.habitat_event }}</a></td>
                                </tr>
                                <tr>
                                    <td>Status Events</td>
                                    <td><a :href="'/rov/observations/' + dive.id + '#status'" target="records">{{ dive.dataCounts.status_event }}</a></td>
                                    <td>Comment Events</td>
                                    <td><a :href="'/rov/observations/' + dive.id + '#comment'" target="records">{{ dive.dataCounts.comment_event }}</a></td>
                                </tr>
                                <tr>
                                    <td>Measurements Events</td>
                                    <td colspan="3"><a :href="'/rov/observations/' + dive.id + '#status'" target="records">{{ dive.dataCounts.measurement_event }}</a></td>
                                </tr>
                                <tr>
                                    <td>Measurements</td>
                                    <td><a :href="'/rov/observations/' + dive.id + '#measurement'" target="records">{{ dive.dataCounts.measurement }}</a></td>
                                    <td>Positions</td>
                                    <td><a :href="'/rov/observations/' + dive.id + '#position'" target="records">{{ dive.dataCounts.position }}</a></td>
                                </tr>
                            </table>
                        </td>

                    </tr>

                    <tr>
                        <th>Crew</th>
                        <td>
                            <table v-if="dive.crew && dive.crew.length > 0" class="grid" style="width: 100%">
                                <tr><th>Name</th><th>Role</th></tr>
                                <tr v-for="dm in dive.crew">
                                    <td>{{ dm.person.first_name + ' ' + dm.person.last_name }}</td>
                                    <td>{{ dm.dive_role.name }}</td>
                                </tr>
                            </table>
                            <span v-else>None Recorded</span>
                        </td>
                    </tr>

                    <tr>
                        <th>Transects</th>
                        <td>
                            <table v-if="dive.transects && dive.transects.length > 0" class="grid" style="width: 100%">
                                <tr><th>Name</th><th>Start Time</th><th>End Time</th></tr>
                                <tbody v-if="dive.transects" v-for="t in dive.transects">
                                    <tr class="transect_row">
                                        <td>{{ t.name }}</td>
                                        <td>{{ time(t.start_time) }}</td>
                                        <td>{{ time(t.end_time) }}</td>
                                    </tr>
                                    <tr>
                                        <th>Objective</th>
                                        <td colspan="3" v-if="t.objective"><pre>{{ t.objective }}</pre></td>
                                        <td colspan="3" v-else>None Recorded</td>
                                    </tr>
                                    <tr>
                                        <th>Notes</th>
                                        <td colspan="3" v-if="t.note"><pre>{{ t.note }}</pre></td>
                                        <td colspan="3" v-else>None Recorded</td>
                                    </tr>
                                </tbody>
                            </table>
                            <span v-else>None Recorded</span>
                        </td>
                    </tr>
                </table>
            </template>
        </section>
        <div v-else-if="divesLoading">Loading...</div>
        <div v-else>None Recorded</div>

    </section>
  </main>
</template>
