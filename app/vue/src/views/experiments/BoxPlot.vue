<script setup>
    import { onMounted, reactive, ref } from 'vue';
    import { Net } from '@/js/util';
    import { MeasurementType, Cruise, Dive } from '@/js/rov/models';
    import * as Plotly from 'plotly.js-dist';

    // Result data. An object containing a data property with results, and a cruises
    // property containing the names of cruises that sourced the data.
    const data = ref({data: {}, cruises: []});
    const lifeStages = reactive([]);        // List of loaded life stages.
    const measurementTypes = reactive([]);  // Measurement types.
    const cruises = reactive([]);           // A list of cruises.
    const dives = reactive([]);             // The list of dives belonging to the cruise.
    const divesLoading = ref(false);        // True when dives are loaded.
    const cruisesLoading = ref(false);      // True when cruises are loading.
    const lifeStagesLoading = ref(false);   // True when life stages are loading.
    const loading = ref(false);             // True when search is in progress.

    const fields = reactive({
        terms: '',
        fields: {scientific_name: true, common_name: true, tags: true, otu: true, cf: true},
        lifeStages: [],
        cruise: null,
        dive: null,
        measurementType: null
    });            // Fields to search.

    /**
     * Render the plot.
     */
    const render = () => {

        const plotData = [];
        const negate = fields.measurementType.name == 'Depth';
        for(let [k, v] of Object.entries(data.value.data)) {
            plotData.push({
                y: negate ? v.map(d => -d) : v,
                name: `${k} (n=${v.length})`,
                type: 'box'
            });
        }

        Plotly.newPlot('plot', plotData);
        document.querySelector('#plot').style.display = 'block';
    };

    /**
     * Perform the search.
     */
    const search = () => {    
        loading.value = true;
        document.querySelector('#plot').style.display = 'none';
        const stages = fields.lifeStages.join(',');
        const flds = Object.keys(fields.fields).filter(k => fields.fields[k]).join(',');
        let url = `${Net.API}/experiments/obs_measurement/?`
            + `fields=${flds}`
            + `&terms=${fields.terms}`
            + `&life_stages=${stages}`
            + `&measurement_type_id=${fields.measurementType.id}`;
        if(fields.cruise)
            url += `&cruise_id=${fields.cruise.id}`;
        if(fields.dive)
            url += `&dive_id=${fields.dive.id}`;
        Net.get(url)
            .then(result => {
                data.value.data = result.data;
                data.value.cruises = result.cruises;
                data.value.cruises.sort();
                loading.value = false;
                render();
            })
            .catch(err => {
                alert(err);
                loading.value = false;
            });
    }

    const cruiseSelected = () => {
        fields.dive = null;
        if(fields.cruise) {
            divesLoading.value = true;
            Dive.loadAll(fields.cruise)
                .then(result => {
                    fields.dive = null;
                    modelUpdate(dives, result, (a, b) => a.name.localeCompare(b.name));
                    divesLoading.value = false;
                })
                .catch(err => {
                    alert(err);
                    divesLoading.value = false;
                });
        }
    };

    onMounted(() => {
        loading.value = true;
        MeasurementType.loadAll()
            .then(result => {
                measurementTypes.push(...result);
                loading.value = false;
            })
            .catch(err => {
                alert(err);
                loading.value = false;
            });
        cruisesLoading.value = true;
        Cruise.loadAll()
            .then(result => {
                cruises.push(...result);
                cruisesLoading.value = false;
            })
            .catch(err => {
                alert(err);
                cruisesLoading.value = false;
            });
        lifeStagesLoading.value = true;
        Net.get(`${Net.API}/experiments/obs_measurement/life_stages/`)
            .then(result => {
                lifeStages.push(...result);
                lifeStagesLoading.value = false;
            })
            .catch(err => {
                alert(err);
                lifeStagesLoading.value = false;
            });
    });
</script>

<template>
    <main>

        <h1>Box Plot - Species vs. Water Properties.</h1>
        <p>Explanations of the search fields.</p>
        <ul>
            <li><b>Cruises</b>: select a cruise to filter down to that cruise or "All" to search all cruises.</li>
            <li><b>Dives</b>: select a dive to filter down to that dive or "All" to cover all dives. You must select a cruise first.</li>
            <li><b>Search terms</b>: type a comma-delimited list of terms to search. Leave empty to match any record. 
                See the fields description for information about <em>what</em> will be searched.</li>
            <li><b>Search fields</b>: the fields to search using the search terms. The tags field is a free-form list of values that
                do not fit into prescribed categories.</li>
            <li><b>Life stage</b>: search observations by life stage, e.g., egg cases. Select "All" to get any 
                result regardless of life stage, "None" to get observations that do not have a life stage, 
                or one or more life stages to filter to only observations with those stages.</li>
            <li><b>Measurement type</b>: the type of measurement. <em>Note that water properties may be stored with different 
                units, which are not automatically converted. For example, salinity can be g/L, g/kg or PSU. 
                These are considered separate measurements (for now).</em></li>
        </ul>
        
        <table>
            <tr>
                <td>Cruises</td>
                <td>
                    <select v-model="fields.cruise" @change.prevent="cruiseSelected()">
                        <option v-if="cruisesLoading" :value="null" disabled>-- Loading --</option>
                        <template v-else>
                            <option :value="null">-- All --</option>
                            <option v-for="c in cruises" :value="c">{{ c.name }} - {{ c.leg }}</option>
                        </template>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Dives</td>
                <td>
                    <select v-model="fields.dive">
                        <option v-if="divesLoading" :value="null" disabled>-- Loading --</option>
                        <template v-else>
                            <option :value="null">-- All --</option>
                            <option v-for="d in dives" :value="d">{{ d.name }}</option>
                        </template>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Search Terms</td>
                <td><input v-model="fields.terms" /></td>
            </tr>
            <tr>
                <td>Search Fields</td>
                <td>
                    <table>
                        <tr>
                            <td><input class="chk" type="checkbox" v-model="fields.fields.scientific_name" :disabled="!fields.terms" /> Scientific Name</td>
                            <td><input class="chk" type="checkbox" v-model="fields.fields.otu" :disabled="!fields.terms" /> OTU</td>
                        </tr>
                        <tr>
                            <td><input class="chk" type="checkbox" v-model="fields.fields.cf" :disabled="!fields.terms" /> Cf.</td>
                            <td><input class="chk" type="checkbox" v-model="fields.fields.tags" :disabled="!fields.terms" /> Tags</td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>Life Stage</td>
                <td>
                    <select v-model="fields.lifeStages" multiple>
                        <option v-if="lifeStagesLoading" :value="null" disabled>-- Loading --</option>
                        <template v-else>
                            <option value="__none__">-- None --</option>
                            <option value="__all__">-- All --</option>
                            <option v-for="l in lifeStages" :value="l">{{ l }}</option>
                        </template>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Measurement Type</td>
                <td>
                    <select v-model="fields.measurementType">
                        <option :value="null">-- Select One --</option>
                        <option v-for="mt in measurementTypes" :value="mt">{{ mt.name }} ({{ mt.unit }})</option>
                    </select>
                </td>
            </tr>
            <tr><td colspan="2">
                <button  @click.prevent="search()" :disabled="!(fields.measurementType && (fields.terms || fields.lifeStages))">Search</button>
            </td></tr>
        </table>

        <p v-if="loading">Searching...</p>
        <h2 v-else-if="Object.keys(data.data).length">Results</h2>

        <div id="plot"></div>

        <template v-if="!loading && data.cruises.length > 0">
            <h3>Involved Cruises</h3>
            <ul>
                <li v-for="c in data.cruises">{{ c }}</li>
            </ul>
        </template>
        
</main>
</template>

<style>
    .chk {
        margin: 0 !important;
        padding: 0 !important;
    }
</style>