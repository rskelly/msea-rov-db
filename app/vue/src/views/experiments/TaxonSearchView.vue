<script setup>
    import { reactive, ref, onMounted } from 'vue';
    import { Cruise } from '@/js/rov/models.js';
    import { SearchMap } from '@/js/map.js';
    import { Net } from '@/js/util.js';

    const cruises = reactive([]);   // A list of available cruises.
    const map = new SearchMap();    // The map.
    const searched = ref(false);    // Switched to true on the first search.
    const selectedCruise = ref(0);  // The cruise ID selected from the drop-down.
    const downloading = ref(false); // True when a download is in progress.
    const term = ref('');           // Current search term.

    const taxonSearchResult = reactive([]); // The result of the taxon search.

    const resultData = reactive({
        page: 0,                    // The current page.
        pageSize: 20,               // The number of items in a result page.
        results: [],                // The search results.
        loading: false,             // True when the search is in progress.
        taxonId: null,              // The requested taxon ID.
        cruiseId: null,             // The requested cruise ID. Zero for all cruises.
        commonName: null,           // The common name of the requested taxon.
        scientificName: null,       // The scientific name of the requested taxon.
    });

    /**
     * Search for observations with the given taxon and cruise IDs.
     * @param cruiseId 
     * @param taxonId 
     * @param page 
     */
    const search = (cruiseId, taxonId, page=0) => {
        resultData.loading = true;
        resultData.taxonId = taxonId;
        resultData.cruiseId = cruiseId;
        resultData.page = page;
        searched.value = true;
        Net.post(`${Net.API}/experiments/taxon_search/search/`, {
            direction: 'down',
            cruise_id: cruiseId,
            taxon_id: taxonId,
            offset: page * resultData.pageSize,
            limit: resultData.pageSize
        })
            .then(result => {
                resultData.commonName = result.common_name;
                resultData.scientificName = result.scientific_name;
                resultData.results.splice(0);
                resultData.results.push(...result.items);
                resultData.loading = false;
                map.render(result.items);
            })
            .catch(err => {
                resultData.loading = false;
                alert(err);
            });
    }

    /**
     * Download a CSV file.
     */
    const download = () => {
        // Execute the search.
        downloading.value = true;
        Net.post(`${Net.API}/experiments/taxon_search/search/`, {
            direction: 'down',
            cruise_id: resultData.cruiseId,
            taxon_id: resultData.taxonId,
            download: true
        })
            .then(() => downloading.value = false)
            .catch(err => {
                downloading.value = false;
                alert(err);
            });
    };

    /**
     * Perform the taxon search.
     * @param evt 
     */
    const taxonSearch = () => {
        resultData.loading = true;
        Net.get(`${Net.API}/experiments/taxon_search/?term=${term.value}`)
            .then(result => {
                taxonSearchResult.splice(0);
                taxonSearchResult.push(...result.items);
                resultData.loading = false;
            })
            .catch(err => {
                resultData.loading = false;
                alert(err)
            });
    };

    const taxonUp = (taxonId) => {
        Net.post(`${Net.API}/experiments/taxon_search/search/`, {
            direction: 'up',
            taxon_id: taxonId
        })
            .then(result => {
                taxonSearchResult.splice(0);
                taxonSearchResult.push(result.parent);
            })
            .catch(alert);
    }

    const titleCase = (v) => {
        if(!v) return '';
        return v.split(' ').map(p => p.substring(0, 1).toUpperCase() + p.substring(1).toLowerCase()).join(' ');
    }

    const nextPage = () => {
        search(resultData.cruiseId, resultData.taxonId, resultData.page + 1);
    };

    const prevPage = () => {
        search(resultData.cruiseId, resultData.taxonId, resultData.page - 1);
    };

    onMounted(() => {
        map.showMap('searchmap');
        // Load the cruises.
        Cruise.loadAll()
            .then(result => {
                // Clear the cruises list and populate it. Cruises are joined with legs.
                cruises.splice(0);
                cruises.push(...result);
            })
            .catch(alert);
    });
</script>

<template>
  <main>
	<section>
        <h1>Taxonomic Search</h1>
        <p>It sometimes happens that marine organisms are unknown to science, or difficult to identify under prevailing conditions, 
            and can only be identified at a higher taxonomic level (e.g., genus). In these cases, the organism may be tagged with
            <em>cf.</em>, which directs the reader to compare the sample to a known taxon, or an operational taxonomic unit (OTU),
            which is a temporary ID.</p>
        <p>This is a search feature that captures those uncertain identifications by performing a recursive search beginning
            at a higher taxonomic level and including every taxon beneath it. For example, to capture all sponges with uncertain
            identifications, one can start with the phylum <em>Porifera</em> and return all of the sponges observed by MSEA.
        </p>
    </section>

    <section>
        <h3>Search</h3>
        <p>Start by entering the name of a species or taxon to search for.</p>
        <form @submit.prevent="taxonSearch()">
            <table>
                <tr>
                    <th><label for="term">Search Term</label></th>
                    <td><input name="term" id="term" v-model="term" /></td>
                    <td><button class="material-symbols" :disabled="!term.trim().length || resultData.loading">search</button></td>
                </tr>
            </table>
        </form>
    </section>

    <section v-if="taxonSearchResult.length">
        <hr />
        <p>Select the taxon to search for observations. To refine the search, select a cruise from the drop-down.</p>
        <p>Click the <button class="inline-button material-symbols">arrow_upward</button> button to go up to the 
                next higher taxonomic level. Click the <button class="inline-button material-symbols">search</button>
                button to search for observations of this and all descendant taxa.</p>

        <form @submit="search($event)">
            <table>
                <tr>
                    <th>Cruise Name</th>
                    <td>
                        <select v-model="selectedCruise">
                            <option value="0">-- All Cruises --</option>
                            <option v-for="c in cruises" :value="c.id">{{ c.name + ' - ' + c.leg }}</option>
                        </select>
                    </td>
                </tr>
            </table>
            <br />
            <table class="grid">
                <tbody style="overflow-y: scroll; max-height: 300px; display:block;">
                    <tr><th>Scientific Name</th><th>Common Name</th><th>Aphia ID</th><th>Rank</th><th colspan="2"></th></tr>
                    <tr v-for="t in taxonSearchResult">
                        <td>{{ t.scientific_name }}</td>
                        <td>{{ t.common_name }}</td>
                        <td>{{ t.taxon_id }}</td>
                        <td>{{ titleCase(t.rank) }}</td>
                        <td><button @click.prevent="taxonUp(t.taxon_id)" class="material-symbols" 
                            :disabled="resultData.loading">arrow_upward</button></td>
                        <td><button @click.prevent="search(selectedCruise, t.taxon_id)" 
                            class="material-symbols" :disabled="resultData.loading">search</button></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </section>
    
    <section v-if="resultData.results.length">
        <hr />
        <h2>Results for "{{ resultData.scientificName }}"</h2>
        <div>
            Download results as a CSV file:
	        <button @click.prevent="download()" :disabled="resultData.loading || downloading" class="material-symbols">download</button>
        </div>
        <br />
        <div>
            <table class="grid">
                <tbody class="sticky_header">
                    <tr>
                        <td colspan="9">
                            <button @click.prevent="prevPage()" 
                                :disabled="resultData.loading || resultData.page == 0" class="material-symbols">arrow_back</button>
                            <button @click.prevent="nextPage()" 
                                :disabled="resultData.loading || resultData.results.length == 0 || resultData.results.length < resultData.pageSize" 
                                class="material-symbols">arrow_forward</button>
                        </td>
                    </tr>
                    <tr>
                        <th>Row</th><th>Cruise</th><th>Leg</th><th>Dive</th><th>Common Name</th><th>Scientific Name</th>
                        <th>Timestamp</th><th>Position</th><th>Depth</th>
                    </tr>
                </tbody>
                <tbody>
                    <tr v-for="item in resultData.results">
                        <td>{{ item.row_num }}</td>
                        <td>{{ item.cruise_name }}</td>
                        <td>{{ item.cruise_leg }}</td>
                        <td>{{ item.dive_name }}</td>
                        <td>{{ item.common_name }}</td>
                        <td>{{ item.scientific_name }}</td>
                        <td>{{ item.timestamp }}</td>
                        <td>{{ item.lon.toFixed(4) }}&deg; {{ item.lat.toFixed(4) }}&deg;</td>
                        <td>{{ item.depth ? item.depth.toFixed(2) + 'm': '' }}</td>
                    </tr>
                </tbody>                    
            </table>
        </div>
    </section>
    
    <section v-if="resultData.loading">
        Searching...
    </section>

    <section v-if="searched && !resultData.loading && resultData.results.length == 0">
        No results found.
    </section>

    <br />

    <section>
		<div id="searchmap" class="map"></div>
	</section>

  </main>
</template>
