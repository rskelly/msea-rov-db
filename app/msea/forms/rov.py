"""
This file contains form class definitions for the views/rov.py file.

Rob Skelly <Robert.Skelly@dfo-mpo.gc.ca>
"""

from django.forms import ModelForm

from msea.models import rov
from msea.models import shared


class ProgramAdminForm(ModelForm):
    class Meta:
        model = rov.Program
        fields = '__all__'


class ProgramRoleAdminForm(ModelForm):
    class Meta:
        model = rov.ProgramRole
        fields = '__all__'


class ProgramMemberAdminForm(ModelForm):
    class Meta:
        model = rov.ProgramMember
        fields = '__all__'


class AbundanceAdminForm(ModelForm):
    class Meta:
        model = rov.Abundance
        fields = '__all__'


class BiocoverAdminForm(ModelForm):
    class Meta:
        model = rov.Biocover
        fields = '__all__'


class ComplexityAdminForm(ModelForm):
    class Meta:
        model = rov.Complexity
        fields = '__all__'

class FlowAdminForm(ModelForm):
    class Meta:
        model = rov.Flow
        fields = '__all__'

class CoverageAdminForm(ModelForm):
    class Meta:
        model = rov.Coverage
        fields = '__all__'


class DisturbanceAdminForm(ModelForm):
    class Meta:
        model = rov.Disturbance
        fields = '__all__'


class ImageQualityAdminForm(ModelForm):
    class Meta:
        model = rov.ImageQuality
        fields = '__all__'


class MediumFormatAdminForm(ModelForm):
    class Meta:
        model = rov.MediumFormat
        fields = '__all__'


class CruiseRoleAdminForm(ModelForm):
    class Meta:
        model = rov.CruiseRole
        fields = '__all__'


class DiveRoleAdminForm(ModelForm):
    class Meta:
        model = rov.DiveRole
        fields = '__all__'


class ModelModelAdminForm(ModelForm):
    class Meta:
        model = rov.Model
        fields = '__all__'


class AvailableSettingAdminForm(ModelForm):
    class Meta:
        model = rov.AvailableSetting
        fields = '__all__'


class PlatformAdminForm(ModelForm):
    class Meta:
        model = rov.Platform
        fields = '__all__'


class InstrumentAdminForm(ModelForm):
    class Meta:
        model = rov.Instrument
        fields = '__all__'


class PlatformConfigAdminForm(ModelForm):
    class Meta:
        model = rov.PlatformConfig
        fields = '__all__'


class PlatformSettingAdminForm(ModelForm):
    class Meta:
        model = rov.PlatformSetting
        fields = '__all__'


class InstrumentConfigAdminForm(ModelForm):
    class Meta:
        model = rov.InstrumentConfig
        fields = '__all__'


class InstrumentSettingAdminForm(ModelForm):
    class Meta:
        model = rov.InstrumentSetting
        fields = '__all__'


class MeasurementAdminForm(ModelForm):
    class Meta:
        model = rov.Measurement
        fields = '__all__'


class MeasurementTypeAdminForm(ModelForm):
    class Meta:
        model = rov.MeasurementType
        fields = '__all__'


class ObservationConfidenceAdminForm(ModelForm):
    class Meta:
        model = rov.ObservationConfidence
        fields = '__all__'


class PositionAdminForm(ModelForm):
    class Meta:
        model = rov.Position
        fields = '__all__'


class PositionTypeAdminForm(ModelForm):
    class Meta:
        model = rov.PositionType
        fields = '__all__'


class ProtocolAdminForm(ModelForm):
    class Meta:
        model = rov.Protocol
        fields = '__all__'


class ReliefAdminForm(ModelForm):
    class Meta:
        model = rov.Relief
        fields = '__all__'


class SurveyModeAdminForm(ModelForm):
    class Meta:
        model = rov.SurveyMode
        fields = '__all__'


class StatusEventAdminForm(ModelForm):
    class Meta:
        model = rov.StatusEvent
        fields = '__all__'


class StatusTypeAdminForm(ModelForm):
    class Meta:
        model = rov.StatusType
        fields = '__all__'


class StatusTypeDetailAdminForm(ModelForm):
    class Meta:
        model = rov.StatusTypeDetail
        fields = '__all__'


class DiveAdminForm(ModelForm):
    class Meta:
        model = rov.Dive
        fields = '__all__'


class SubstrateAdminForm(ModelForm):
    class Meta:
        model = rov.Substrate
        fields = '__all__'


class TaxonAdminForm(ModelForm):
    class Meta:
        model = shared.Taxon
        fields = '__all__'


class ThicknessAdminForm(ModelForm):
    class Meta:
        model = rov.Thickness
        fields = '__all__'


class CruiseAdminForm(ModelForm):
    class Meta:
        model = rov.Cruise
        fields = '__all__'


class TransectAdminForm(ModelForm):
    class Meta:
        model = rov.Transect
        fields = '__all__'
