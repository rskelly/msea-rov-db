'''
This file contains form class definitions for shared views and models.

Rob Skelly <Robert.Skelly@dfo-mpo.gc.ca>
'''

from django.forms import ModelForm

from msea.models.shared import *


class SiteAdminForm(ModelForm):

	class Meta:
		model = Site
		fields = '__all__'


class SpatialLibraryAdminForm(ModelForm):

	class Meta:
		model = SpatialLibrary
		fields = '__all__'


class PersonAdminForm(ModelForm):

	class Meta:
		model = Person
		fields = '__all__'

