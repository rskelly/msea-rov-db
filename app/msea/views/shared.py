import os
import urllib
import subprocess
import csv
import hashlib
from PIL import Image

from django.http import FileResponse
from django.conf import settings
from django.db import connection

from rest_framework import serializers
from rest_framework.response import Response
from drf_writable_nested.mixins import UniqueFieldsMixin
from drf_writable_nested.serializers import WritableNestedModelSerializer

from msea.forms.shared import *
from msea.util.views import *
from msea.util import util, map_thumb
from msea.models.shared import Person, Group
from msea.models.taxonomy import Taxon
from msea.views.auth import ReSTAdminViewSet, ReSTOpenViewSet, ReSTUserViewSet


class PersonSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    
    class Meta:
        model = Person
        fields = '__all__'


class PersonViewSet(ReSTAdminViewSet):
    serializer_class = PersonSerializer
    model_class = Person


class GroupSeralizer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = '__all__'


class DefaultView(TemplateView):
    """
    Default view for data administration.
    """

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'MSEA Database Administration'

        cmd = 'git log --date=relative --pretty=format:"<tbody><tr><td>%cd</td><td>%s</td><td>%cn</td></tr></tbody>" --all --max-count=5'
        gitlist = str(subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True).stdout.read(), encoding='utf-8')
        gitlist = gitlist.replace('"<tbody', '<tbody')
        gitlist = gitlist.replace('tbody>"', 'tbody>')
        context['gitlist'] = gitlist

        return context


def csv_lookup(request, type_name):
    """
    Render CSV from the model class. This is a hack and should be done with a renderer,
    but it formats output more reliably.
    :param request: The HTTP request.
    :param type_name: The name of the lookup entity to download.
    :return:
    """
    # Get the lookup model class for the type.
    cls = None
    if type_name == 'abundance':
        cls = Abundance
    elif type_name == 'image_quality':
        cls = ImageQuality
    elif type_name == 'complexity':
        cls = Complexity
    elif type_name == 'thickness':
        cls = Thickness
    elif type_name == 'biocover':
        cls = Biocover
    elif type_name in ('substrate_coverage', 'biocover_coverage', 'coverage'):
        cls = Coverage
    elif type_name == 'relief':
        cls = Relief
    elif type_name == 'substrate':
        cls = Substrate
    elif type_name == 'instrument':
        cls = Instrument
    elif type_name == 'platform':
        cls = Platform
    elif type_name == 'disturbance':
        cls = Disturbance
    elif type_name == 'observation_confidence':
        cls = ObservationConfidence
    elif type_name == 'flow':
        cls = Flow

    if cls:
        # Run the query, write CSV result to a temporary file.
        queryset = cls.objects.all()
        fields = [field.name.split('.')[-1] for field in queryset.first()._meta.fields]
        tf = util.maketempfile(suffix='.csv')
        with open(tf.name, 'w', newline='') as fo:
            db = csv.writer(fo)
            db.writerow(fields)
            for row in queryset:
                db.writerow([getattr(row, field) for field in fields])

        # Send the response.
        try:
            # Create and return the file response.
            basename = f'{type_name}.csv'
            res = FileResponse(open(tf.name, 'rb'), as_attachment=True, filename=basename)
            return res
        except Exception as e:
            raise e

    # Document not found.
    return HttpResponse(404)


def thumbnail(request):
    """
    Retrieves the binary data for a PNG file from a DB field and
    return an HttpResponse containing the image data.
    """

    clear = request.GET.get('clear')  # Clear the image cache.

    # For database entities.
    entity = request.GET.get('entity')  # The name of the entity type to retrieve a thumbnail for.
    id = request.GET.get('id')  # The ID of the row to retrieve.

    # For image URLs.
    url = request.GET.get('url')  # A URL.

    try:
        size = int(request.GET.get('size', 0))
    except:
        size = 0

    # If the clear param is given, clean out the image cache.
    if clear == 'true':
        for f in [x for x in os.listdir(settings.MSEA_THUMB_DIR) if x not in ('.', '..')]:
            os.unlink(os.path.join(settings.MSEA_THUMB_DIR, f))
        return HttpResponse(200)

    if id and entity:
        cache_file = os.path.join(settings.MSEA_THUMB_DIR, '{}_{}.png'.format(entity, id))

        # If the cache file exists, return it.
        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as f:
                return HttpResponse(f, content_type='image/png')

        # Retrieve the site thumbnail.
        with connection.cursor() as cur:
            if entity == 'site':
                id = int(id)
                cur.execute('SELECT thumbnail FROM shared.site WHERE id=%s', [id])
                data, = cur.fetchone()
                print(data)
                if not data:
                    print('thumb for site', id)
                    map_thumb.site_thumb(cur, id)
                    cur.execute('SELECT thumbnail FROM shared.site WHERE id=%s', [id])
                    data, = cur.fetchone()
                    print(data)
            elif entity == "spatial_library":
                id = int(id)
                cur.execute('SELECT thumbnail FROM shared.spatial_library WHERE id=%s', [id])
                data, = cur.fetchone()
                if not data:
                    map_thumb.spatial_library_thumb(cur, id)
                    cur.execute('SELECT thumbnail FROM shared.spatial_library WHERE id=%s', [id])
                    data, = cur.fetchone()

            if not data:
                return HttpResponse(404)
            else:
                # Write the cache file.
                with open(cache_file, 'wb') as f:
                    f.write(data)

                return HttpResponse(data, content_type='image/png')

    elif url:

        # TODO: iNaturalist seems to have moved their images to AWS. This
        # temporarily rewrites the URL. Future versions of the dataset will
        # hopefully update the URLS.
        url = url.replace('https://static.inaturalist.org/photos/',
                          'https://inaturalist-open-data.s3.amazonaws.com/photos/')

        # Use a hash of the URL to name the file.
        url_hash = hashlib.md5(url.encode('utf-8')).hexdigest()

        cache_file = os.path.join(settings.MSEA_THUMB_DIR, '{}_{}.{}'.format(url_hash, size, 'jpg'))

        # If the cache file exists, return it.
        if os.path.exists(cache_file):
            with open(cache_file, 'rb') as f:
                return HttpResponse(f, content_type='image/jpeg')

        u = urllib.request.urlopen(url)
        data = u.read()
        content_type = u.headers.get('Content-Type')

        if not content_type:
            print('No content type.')
            return HttpResponse(404)

        content_type = content_type.lower()
        if content_type in ('image/jpg', 'image/jpeg'):
            ext = 'jpg'
        elif content_type == 'image/png':
            ext = 'png'
        elif content_type == 'image/gif':
            ext = 'gif'
        else:
            print('Unknown content type: ', content_type)
            return HttpResponse(404)

        # Write the tmp file.
        tf = util.maketempfile()
        with open(tf.path, 'wb') as f:
            f.write(data)

        if size <= 0 or size > 2048:
            size = 200

        img = Image.open(tf.path)
        width, height = img.size
        size2 = int(size * (float(height) / width))
        if width >= height:
            img1 = img.resize((size, size2))
        else:
            img1 = img.resize((size2, size))
        img1 = img1.convert('RGB')
        img1.save(cache_file)

        # Read the cache file.
        with open(cache_file, 'rb') as f:
            data = f.read()

        return HttpResponse(data, content_type='image/{}'.format(ext))

    try:
        with open('static/img/missing_100.png', 'rb') as f:
            return HttpResponse(f.read(), content_type='image/png')
    except:
        pass

    return HttpResponse(404)


class SiteSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Site
        fields = '__all__'


class SiteViewSet(ReSTUserViewSet):
    serializer_class = SiteSerializer
    model_class = Site


class FileSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = File
        fields = '__all__'


class EventTaxonViewSet(ReSTOpenViewSet):
    """
    Returns a list of taxa observed in events, grouped and counted. Taxa are
    assembled by collecting the scientific and common names, and aphia, hart and 
    inaturalist IDs.
    """

    def list(self, request, cruises_pk=None, dives_pk=None):
        """
        Get a list of taxa from events. A dive or cruise ID is required.
        """
        try:
            if cruises_pk:
                queryset = Event.objects.filter(dive__in=Dive.objects.filter(cruise_id=cruises_pk))
            elif dives_pk:
                queryset = Event.objects.filter(dive_id=dives_pk)
            else:
                raise Exception('A drive or cruise ID is required.')
            queryset = queryset.filter(models.Q(properties__event_types__icontains='observation') | models.Q(properties__event_types__icontains='habitat'))
            taxa = {}
            for item in queryset:
                scientific_name = item.properties.get('scientific_name')
                if scientific_name:
                    if taxa.get(scientific_name.lower()) == None:
                        taxa[scientific_name.lower()] = {
                            'scientific_name': item.properties.get('scientific_name'),
                            'common_name': item.properties.get('common_name'),
                            'otu': item.properties.get('otu'),
                            'cf': item.properties.get('cf'),
                            'aphia_id': item.properties.get('aphia_id'),
                            'inaturalist_id': item.properties.get('inaturalist_id'),
                            'hart_code': item.properties.get('hart_code')
                        }
            return JsonResponse({'result': sorted(list(taxa.values()), key=lambda a: a['scientific_name'])})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class TaxonSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    reviewed_by = PersonSerializer()

    class Meta:
        model = Taxon
        fields = '__all__'


class TaxonViewSet(ReSTOpenViewSet):
    serializer_class = TaxonSerializer
    model_class = Taxon


class RestrictionSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Restriction
        fields = '__all__'


class RestrictionViewSet(ReSTOpenViewSet):
    serializer_class = RestrictionSerializer
    model_class = Restriction


class RestrictionGroupSerializer(WritableNestedModelSerializer):
    restriction = RestrictionSerializer()
    group = GroupSeralizer()

    class Meta:
        model = RestrictionGroup
        fields = '__all__'


class RestrictionGroupViewSet(ReSTOpenViewSet):
    serializer_class = RestrictionGroupSerializer
    model_class = RestrictionGroup

    def list(self, request, restrictions_pk=None):
        if restrictions_pk:
            try:
                restriction = Restriction.objects.get(pk=restrictions_pk)
                result = RestrictionGroup.objects.filter(restriction=restriction)
                return JsonResponse({'result': RestrictionGroupSerializer(result, many=True).data})
            except Exception as e:
                return JsonResponse({'error': str(e)})
        else:
            return super().list(self, request)


class UploadedFileSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = UploadedFile
        fields = '__all__'


class DownloadFileViewSet(ReSTOpenViewSet):

    def retrieve(self, request, pk):
        """
        Download the given file from the downloads directory. 
        :param request: The HTTP request.
        :param pk: The ID of the file.
        :return: A FileResponse which streams the file.
        """
        try:
            try:
                # PK is the ID of an uploaded file.
                pk = int(pk)
                file = UploadedFile.objects.get(pk=pk)
                attachment_filename = request.GET.get('attachment_filename', file.name)
                path = os.path.join(settings.MSEA_UPLOAD_DIR, file.path)
            except:
                # PK is the filename of a file on disk.
                attachment_filename = request.GET.get('attachment_filename', 'file')                
                path = os.path.join(settings.MSEA_DOWNLOAD_DIR, pk)
            return FileResponse(open(path, 'rb'), as_attachment=True, filename=attachment_filename)
        except:
            return HttpResponse(404)
        
    def list(self, request):
        return HttpResponse(404)

    def create(self, request):
        return HttpResponse(404)

    def update(self, request, pk):
        return HttpResponse(404)

    def destroy(self, pk):
        return HttpResponse(404)
    

class UploadedFileViewSet(ReSTOpenViewSet):
    """
    This is a service which enables the uploading of any file to the server for use elsewhere in the application.
    Upon upload, an UploadedFile instance is created, which can then be referenced anywhere in the app. The app
    can use this entity to physically copy the file to its ultimate location, at which point the UploadedFile and
    its temporary physical file can be deleted. A scheduled cleanup will also remove UploadedFile instances and
    files after a specified interval.
    """
    serializer_class = UploadedFileSerializer
    model_class = UploadedFile

    def list(self, request):
        return HttpResponse(404)
    
    def destroy(self, request, pk):
        """
        Delete the file identified by pk.
        """
        try:
            UploadedFile.objects.get(pk=pk).delete()
            return JsonResponse({'result': True})
        except Exception as e:
            return JsonResponse({'error': str(e)})
        
    def create(self, request):
        """
        Handle uploaded files. The form must send file data in a file field. Multiples are allowed.
        Will return a list of IDs corresponding to the newly-created UploadFile which can be used to
        reference the file on further accesses. These files must be moved to a final location,
        as UploadFile instances and the physical file will be removed after a scheduled interval.

        :param request: A POST request with one or more file fields.
        :return: A list of IDs.
        """
        try:
            # Make sure there's an upload dir.
            upload_dir = settings.MSEA_UPLOAD_DIR
            if not upload_dir:
                raise Exception('The upload directory is not configured.')

            result = []

            # Iterate over files.
            for file in request.FILES.values():
                if not file:
                    # If the file is not successfully uploaded or is absent, its position in the
                    # return value is set to null.
                    result.append(None)
                else:
                    # Write the file to a temp dir.
                    tf = util.maketempfile()
                    with open(tf.path, 'wb') as f:
                        for chunk in file.chunks():
                            f.write(chunk)

                    # Move the file to the upload directory.
                    tf.move_to(upload_dir)

                    # Instantiate the uploaded file object.
                    uf = UploadedFile.objects.create(
                        path=os.path.basename(tf.path), # Only the basename is stored in case the upload path changes.
                        name=file.name,
                        type=util.get_file_type(file.name)
                    )
                    # Add the UploadedFile ID to the return list.
                    result.append(uf)

            # Return the list of uploaded files.
            return JsonResponse({'result': UploadedFileSerializer(result, many=True).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class OrganisationSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    
    class Meta:
        model = Organisation
        fields = '__all__'


class OrganisationViewSet(ReSTOpenViewSet):
    serializer_class = OrganisationSerializer
    model_class = Organisation
