import json

from django.http import JsonResponse
from rest_framework.decorators import action
from rest_framework import serializers
from drf_writable_nested.serializers import WritableNestedModelSerializer


from msea.models.auth import User
from msea.models.shared import Restriction
from msea.models.rov.imports import GenericLabelMap, GenericLabelMapRestriction, \
    CruiseImport, BiigleLabelMap, BiigleLabelMapRestriction

from msea.views.auth import ReSTAdminViewSet
from msea.views.shared import RestrictionSerializer


class CruiseImportSerializer(serializers.ModelSerializer):

    class Meta:
        model = CruiseImport
        fields = '__all__'


class CruiseImportViewSet(ReSTAdminViewSet):
    serializer_class = CruiseImportSerializer
    model_class = CruiseImport

    def list(self, request):
        try:
            # Don't load the data or logs.
            cruise_imports = CruiseImport.objects.all().order_by('-updated_on').defer('logs', 'data')
            data = CruiseImportSerializer(cruise_imports, many=True).data
            return JsonResponse({'result': data})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def update(self, request, pk):
        try:
            data = request.data
            if type(data['data']) != dict:
                data['data'] = json.loads(data['data'])
            cruise_import = CruiseImport.objects.get(id=pk)
            for k, v in data.items():
                setattr(cruise_import, k, v)
            cruise_import.save()
            return JsonResponse({'result': CruiseImportSerializer(cruise_import).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def create(self, request):
        try:
            data = request.data
            if type(data['data']) != dict:
                data['data'] = json.loads(data['data'])
            cruise_import = CruiseImport.objects.create(**data)
            return JsonResponse({'result': CruiseImportSerializer(cruise_import).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})
        
    def retrieve(self, request, pk):
        """
        Get the true/false status of the task.
        """
        try:
            cruise_import = CruiseImport.objects.get(pk=pk)
            return JsonResponse({'result': CruiseImportSerializer(cruise_import).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def destroy(self, request, pk):
        """
        Get the true/false status of the task.
        """
        try:
            CruiseImport.objects.get(pk=pk).delete()
            return JsonResponse({'result': True})
        except Exception as e:
            return JsonResponse({'error': str(e)})

class BiigleLabelMapRestrictionSerializer(WritableNestedModelSerializer):
    restriction = serializers.PrimaryKeyRelatedField(queryset=Restriction.objects.all())
    biigle_label_map = serializers.PrimaryKeyRelatedField(queryset=BiigleLabelMap.objects.all())

    class Meta:
        model = BiigleLabelMapRestriction
        fields = '__all__'


class BiigleLabelMapSerializer(WritableNestedModelSerializer):
    restrictions = RestrictionSerializer(many=True)

    class Meta:
        model = BiigleLabelMap
        fields = '__all__'


class BiigleLabelMapViewSet(ReSTAdminViewSet):
    model_class = BiigleLabelMap
    serializer_class = BiigleLabelMapSerializer

    def create(self, request):
        """
        Create an individual or list of items. If a list is given,
        checks each item for an ID and creates the ones without, while
        updating the ones with. 

        Note: This is here because writable nested doesn't work for this application.
        """
        return self.process(request)
    
    def update(self, instance, request):
        """
        Update an individual or list of items. If a list is given,
        checks each item for an ID and creates the ones without, while
        updating the ones with. 

        Note: This is here because writable nested doesn't work for this application.
        """
        return self.process(request)
    
    def process(self, request):
        """
        Process an individual or list of label map items. 
        """
        try:
            data = request.data
            # Record whether it's a list so we know what to return.
            is_list = type(data) == list

            if not is_list:
                # If it's not a list wrap it for convenience.
                data = [data]

            result = []
            for item in data:
                # Pop restrictions for later.
                restrictions = item.pop('restrictions')
                # Get or create the instance.
                id = item.pop('id')
                instance = None
                if id:
                    try:
                        instance = self.model_class.objects.get(pk=id)
                        for k, v in item.items():
                            setattr(instance, k, v)
                        instance.save()
                    except: pass
                if not instance:
                    try:
                        instance = self.model_class.objects.get(label_id=item['label_id'])
                        for k, v in item.items():
                            setattr(instance, k, v)
                        instance.save()
                    except:
                        instance = self.model_class.objects.create(**item)
                result.append(instance)
                # A list to store IDs of created/updated mappings.
                brids = []
                for ritem in restrictions:
                    # Get the restriction ID.
                    rid = ritem.pop('id')
                    if rid:
                        rinst = Restriction.objects.get(pk=rid)
                    else:
                        rinst = Restriction.objects.create(**ritem)
                    britem = {'restriction': rinst, 'biigle_label_map': instance}
                    # Try to get the mapping, otherwise create it.
                    try:
                        brinst = BiigleLabelMapRestriction.objects.get(**britem)
                    except:
                        brinst = BiigleLabelMapRestriction.objects.create(**britem)
                    # Save the ID.
                    brids.append(brinst.id)
                # Delete mappings not in the update/create list.
                BiigleLabelMapRestriction.objects.filter(biigle_label_map=instance).exclude(pk__in=brids).delete()
            # Serialize.
            serializer = BiigleLabelMapSerializer(result if is_list else result[0], many=is_list)
            return JsonResponse({'result': serializer.data})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class GenericLabelMapSerializer(WritableNestedModelSerializer):
    restrictions = RestrictionSerializer(many=True)

    class Meta:
        model = GenericLabelMap
        fields = '__all__'


class GenericLabelMapRestrictionSerializer(serializers.ModelSerializer):
    restriction = RestrictionSerializer()
    generic_label_map = GenericLabelMapSerializer()

    class Meta:
        model = GenericLabelMapRestriction
        fields = '__all__'


class GenericLabelMapViewSet(ReSTAdminViewSet):
    model_class = GenericLabelMap
    serializer_class = GenericLabelMapSerializer

    @action(methods=['post'], detail=False)
    def update_all(self, request):
        """
        Given an array of IDs, return all of the corresponding label map objects.
        """
        try:
            labels = request.data
            result = GenericLabelMap.objects.filter(label_text__in=labels)
            return JsonResponse({'result': GenericLabelMapSerializer(result, many=True).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})
        
    def list(self, request):
        raise Exception('Cannot load the entire label map list.')
        
    def create(self, request):
        """
        Create an individual or list of items. If a list is given,
        checks each item for an ID and creates the ones without, while
        updating the ones with. 

        Note: This is here because writable nested doesn't work for this application.
        """
        return self.process(request)
    
    def update(self, instance, request):
        """
        Update an individual or list of items. If a list is given,
        checks each item for an ID and creates the ones without, while
        updating the ones with. 

        Note: This is here because writable nested doesn't work for this application.
        """
        return self.process(request)
    
    def process(self, request):
        """
        Process an individual or list of label map items. 
        """
        try:
            data = request.data
            # Record whether it's a list so we know what to return.
            is_list = type(data) == list

            if not is_list:
                # If it's not a list wrap it for convenience.
                data = [data]

            result = []
            for item in data:
                # Pop restrictions for later.
                restrictions = item.pop('restrictions')
                # Get or create the instance.
                id = item.pop('id')
                instance = None
                try:
                    instance = self.model_class.objects.get(pk=id)
                    for k, v in item.items():
                        setattr(instance, k, v)
                    instance.save()
                except: pass
                if not instance:
                    try:
                        instance = self.model_class.objects.get(label_id=item['label_id'])
                        for k, v in item.items():
                            setattr(instance, k, v)
                        instance.save()
                    except:
                        instance = self.model_class.objects.create(**item)
                result.append(instance)
                # A list to store IDs of created/updated mappings.
                brids = []
                for ritem in restrictions:
                    # Get the restriction ID.
                    rid = ritem.pop('id')
                    if rid:
                        rinst = Restriction.objects.get(pk=rid)
                    else:
                        rinst = Restriction.objects.create(**ritem)
                    britem = {'restriction': rinst, 'generic_label_map': instance}
                    # Try to get the mapping, otherwise create it.
                    try:
                        brinst = GenericLabelMapRestriction.objects.get(**britem)
                    except:
                        brinst = GenericLabelMapRestriction.objects.create(**britem)
                    # Save the ID.
                    brids.append(brinst.id)
                # Delete mappings not in the update/create list.
                GenericLabelMapRestriction.objects.filter(generic_label_map=instance).exclude(pk__in=brids).delete()
            # Serialize.
            serializer = GenericLabelMapSerializer(result if is_list else result[0], many=is_list)
            return JsonResponse({'result': serializer.data})
        except Exception as e:
            return JsonResponse({'error': str(e)})
