import logging

from django.db import connection
from django.http import JsonResponse
from rest_framework.decorators import action

from msea.views.shared import ReSTOpenViewSet
from .util import get_requested_bounds

class ROVHomeViewSet(ReSTOpenViewSet):
    """
    Services for the ROV home page.
    """
    
    @action(detail=False, methods=['get'])
    def map_dives(self, request):
        """
        When the user clicks on the map, the lat and lon parameters are used to locate
        dives associated with the clicked tracks. To do this, the candidate set of tracks
        is retreived using the bounding box index, then filtered using buffered track
        lines.
        :param request: A Django request object representing the HTTP request.
        :return: A Response. The object contains the cruise name and dive name.
        """
        import math
        try:
            # Get the parameters.
            lat = float(request.GET.get('lat'))
            lon = float(request.GET.get('lon'))
            zoom = float(request.GET.get('zoom'))

            # We buffer a region around the cursor to intersect with the dive lines.
            buffer = 500000. / (2**(zoom-1))
            print(buffer, zoom)

            cur = connection.cursor()

            cur.execute(f'''
                with coord as (
                    select st_transform(st_buffer(st_transform(st_setsrid(st_makepoint(%s, %s), 4326), 3857), %s), 4326) as geom
                )
                select distinct concat(c.name, '-', c.leg) as cruise_name, d.name as dive_name
                from rov.dive d
                    inner join rov.cruise c on c.id=d.cruise_id
                    inner join cache.dive_track t on t.dive_id=d.id
                    inner join coord on 't'::boolean
                where c.approved=2 and 
                    coord.geom && t.geom and
                    st_intersects(coord.geom, t.geom)
            ''', [lon, lat, buffer])

            result = []
            for cruise_name, dive_name in cur:
                result.append({'cruise_name': cruise_name, 'dive_name': dive_name})

            return JsonResponse({'result': result})
        except Exception as e:
            return JsonResponse({'error': str(e)})
        
    @action(detail=False, methods=['get'])
    def cruise_colours_bounds(self, request):
        """
        Return a list of cruises with the assigned colour for each.
        :param request: A Django request object representing the HTTP request.
        :return: A Response.
        """
        try:
            all_cruises = request.GET.get('all')
            cur = connection.cursor()

            # Filter out the unapproved cruises.            
            cruise_status_filter = ''
            if not all_cruises:
                cruise_status_filter = " where c.approved = 2"

            # Query the cruises.
            cur.execute(f'''
                select d.cruise_name, d.cruise_id, d.colour, 
                    st_asgeojson(box2d(st_collect(st_force2d(d.geom::geometry)))) as geom 
                from cache.dive_track d
                    inner join rov.cruise c on c.id=d.cruise_id
                {cruise_status_filter}
                group by cruise_name, cruise_id, colour
            ''')
            cols = [desc[0] for desc in cur.description]
            cruises = [dict(zip(cols, row)) for row in cur.fetchall()]
            
            # Quert the stats.
            cur.execute(f'''
                select 'dive' as category, count(*) as count 
                from rov.dive d 
                where d.id in (
                    select distinct d.id 
                    from rov.dive d
                        inner join rov.cruise c on c.id=d.cruise_id
                        inner join rov.platform_config f on f.id=d.sub_config_id
                        inner join rov.instrument_config i on i.platform_config_id=f.id
                        inner join rov.position p on p.instrument_config_id=i.id
                    {cruise_status_filter}
                )
                union
                select 'cruise' as category, count(*) as count
                from rov.cruise c
                where c.id in (
                    select distinct c.id 
                    from rov.cruise c
                        inner join rov.dive d on d.cruise_id=c.id
                        inner join rov.platform_config f on f.id=d.sub_config_id
                        inner join rov.instrument_config i on i.platform_config_id=f.id
                        inner join rov.position p on p.instrument_config_id=i.id
                    {cruise_status_filter}
                )
            ''')
            counts = {}
            for cat, cnt in cur:
                counts[cat] = cnt

            # Construct the result.
            result = {
                'cruises': cruises,
                'cruise_count': counts['cruise'],
                'dive_count': counts['dive']
            }
            return JsonResponse({'result': result})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    @action(detail=False, methods=['get'])
    def rov_dive_tracks(self, request):
        """
        Returns a list of dive tracks.
        Used on the "Where we've been and what we've seen" map.

        Query string params:
            cruise_name - If given, return dives for the given cruise.
            all_cruises - If given, return dives for all cruises, not just those that are approved.
            zoom - Used to calculate a simplification paramter for tracks.
            [bounds] - Coordinates, a geometry or bounds signifying a region of interest. Parse by get_requested_bounds().

        :param request: A Django request object representing the HTTP request.
        :return: A Response.
        """
        try:
            cruise_name = request.GET.get('cruise_name')
            all_cruises = request.GET.get('all')

            # Get the bounds query and params.
            bounds_query, bounds_params = get_requested_bounds(request)

            # Figure out the zoom level.
            try:
                zoom = max(1., float(request.GET.get('zoom', 1)) - 5.)
            except Exception:
                zoom = 1

            # Create a simplification parameter based on the zoom level.
            simp = 10. / (float(zoom) ** 5.)

            # Container for result objects.
            result = []

            # Parameter list.
            params = [simp]

            # Add the bounds parameters to the query.
            params.extend(bounds_params)

            cruise_name_filter = ''
            if cruise_name:
                cruise_name_filter = " and concat(a.name, ' - ', a.leg)=%s"
                params.append(cruise_name)
            
            cruise_status_filter = ''
            if not all_cruises:
                cruise_status_filter = " and cruise.approved = 0"

            with connection.cursor() as cur:

                # Get the dive tracks.
                sql = f"""
                    select concat(a.name, '-', a.leg, ' - ', b.name) as name, a.name as cruise_name,
                        b.start_time, st_asgeojson(st_simplifypreservetopology(c.geom::geometry, %s)) as geom 
                        from rov.cruise a
                            inner join rov.dive b on b.cruise_id = a.id
                            inner join cache.dive_track c on c.dive_id = b.id
                        where {bounds_query} && c.geom::geometry
                            {cruise_name_filter}
                            {cruise_status_filter}
                """

                # Execute the query.
                cur.execute(sql, params)

                # Get a list of the fields.
                fields = [desc[0] for desc in cur.description]

                # Append the dive tracks to the result object.
                for row in cur.fetchall():
                    result.append(dict(zip(fields, row)))

            # Send the result.
            return JsonResponse({'result': result})
        except Exception as e:
            logging.warning(str(e))
            return JsonResponse({'error': str(e)})

