import csv
import json

from django.http import JsonResponse, FileResponse, HttpResponse
from django.db import connection
from rest_framework.decorators import action
from rest_framework import viewsets

from msea.util.reporting import *

from .util import get_requested_bounds

class ReportViewSet(viewsets.ViewSet):
    """
    Provides services for retrieving various reports.
    """

    @action(detail=False, methods=['get'])
    def cruise_taxa(self, request):
        """
        Report taxonomy records in relation to a cruise, with optional fields
        and search terms, in a variety of formats.
        :param request: The Django request object representing the HTTP request.
        """

        # Get the parameters.
        cruise_id = request.GET.get('cruise_id')                # The cruise ID.
        dive_id = request.GET.get('dive_id')                    # The dive ID.
        output_format = request.GET.get('fmt', '').lower()      # The output format.
        fn = request.GET.get('fn', '')
        andor = request.GET.get('andor', '')                    # Whether individual search terms are ANDed or ORed.
        aggregate = request.GET.get('aggregate', '')            # If results should be aggregated (counts by species) or individual.

        # The search term is split into an array.
        try:
            terms = request.GET.get('term', '').split(',')
        except:
            terms = []

        # If a geometry format is needed, set a flag.
        rawgeom = format in ('shp', 'sqlite')

        title = 'Taxa'
        meta = None

        # Labels for tables.
        labels = ['Event ID', 'Timestamp', 'Cruise Name', 'Dive Name', 'Label', 'Original Label', 'Scientific Name',
                  'Common Name', 'Count', 'Geometry']
        # Select fields.
        fields = ['c.id', 'c.start_time', 'a.name as cruise_name', 'b.name as dive_name', 'g.label', 'g.original_label',
                  'g.scientific_name', 'g.common_name', 'd.count']
        # Labels for aggregate labels.
        agglabels = ['Dive Name', 'First Timestamp', 'Label', 'Original Label', 'Scientific Name', 'Common Name',
                     'Obs. Count', 'Count', 'Geometry']
        # Aggregate fields. Sometimes the count is null; if there's a record use 1.
        agg = ['dive_name', 'min(start_time)', 'label', 'original_label', 'scientific_name', 'common_name',
               'count(*) as obs_count', 'sum(coalesce(count, 1)) as count']
        # Aggregate group fields.
        agggroup = ['dive_name', 'label', 'original_label', 'scientific_name', 'common_name']

        # Use raw or encoded geometries depending on the output format.
        if rawgeom:
            fields.append('p.geom')
            agg.append('st_centroid(st_collect(geom::geometry))')
        else:
            if aggregate == 'yes':
                fields.append('p.geom')
            else:
                fields.append('st_astext(p.geom)')
            agg.append('st_astext(st_centroid(st_collect(geom::geometry)))')

        # Accumulate parameters and query statements.
        params = []
        query = []

        if cruise_id:
            # If there's a cruise ID, add it to the query.
            params.append(cruise_id)
            query.append('a.id=%s')
        if dive_id:
            # If there's a dive ID, add it to the query.
            params.append(dive_id)
            query.append('c.dive_id=%s')

        if terms:
            # If there are search terms, add them to the query.
            terms_sql = []
            # Parse the search terms and create the query. Only use non-empty strings.
            # This is a case-insensitive match against any part of the string.
            for t in [t for t in terms if t]:
                terms_sql.append('''
                    (g.label ilike %s 
                        or g.original_label ilike %s 
                        or g.common_name ilike %s 
                        or g.scientific_name ilike %s
                    )
                ''')
                params.extend([f'%{t}%'] * 4)
            if terms_sql:
                # If terms were found, append to the query.
                query.append('(' + (' and ' if andor.lower() == 'and' else ' or ').join(terms_sql) + ')')

        # The base select statement.
        sql = """
            select {fields}
            from rov.observation_event d 
            inner join rov.observation_event_position e on e.event_id=d.event_id
            inner join rov.event c on c.id = d.event_id
            inner join rov.dive b on b.id = c.dive_id
            inner join rov.cruise a on a.id = b.cruise_id
            inner join rov.annotation_protocol_taxon j on j.id = d.annotation_protocol_taxon_id
            inner join shared.taxon g on g.id = j.taxon_id
            inner join rov.position p on p.id = e.start_position_id
            {where}
        """.format(
            fields=', '.join(fields),
            # If the query is empty, leave the where section blank.
            where='where ' + ' and '.join(query) if query else ''
        )

        # If aggregate, wrap the query in an aggregator query.
        if aggregate == 'yes':
            # Replace the labels list with the aggregate labels list.
            labels = agglabels
            # Wrap the query.
            sql = """
                select {fields}
                from ({sql}) t
                group by {group}
            """.format(
                sql=sql,
                fields=', '.join(agg),
                group=','.join(agggroup)
            )

        if fn == 'count':
            # If count is desired, wrap query in a count query.
            sql = f'select count(*) as count from ({sql}) t'

        # For non-spatial, execute the query.
        with connection.cursor() as cur:

            if fn != 'count' and rawgeom:
                # If a geometry format is needed, use OGR to get it directly from the DB.
                sql = cur.mogrify(sql, params).decode('utf-8')
                filename = render_spatial(format, sql)
                return FileResponse(open(filename, 'rb'))

            cur.execute(sql, params)

            if fn == 'count':
                # If count, just get the count.
                output = {'count': cur.fetchone()[0]}
            else:
                # If not count, turn the result into a list.
                output = []
                # Get the column names.
                cols = [x[0] for x in cur.description]

                # Prepare a row-handling function.
                row_fn = prepare_row(labels if format in ('html', 'pdf') else cols)

                # Process the rows.
                output = [row_fn(row) for row in cur]

        if fn == 'count' or format == 'json':

            # Export the count or result as JSON.
            return JsonResponse({'result': output})

        elif output_format == 'excel':

            # Export Excel format.
            filename = render_excel(title, output)
            return FileResponse(open(filename, 'rb'))

        elif output_format == 'csv':

            # Export CSV format.
            filename = render_csv(output)
            return FileResponse(open(filename, 'rb'))

        elif output_format == 'html':

            # Create an html file.
            file = render_html(title, labels, output, meta)
            return FileResponse(open(file, 'rb'))

        elif output_format == 'pdf':

            # Create a pdf file.
            filename = render_pdf(title, labels, output, meta)
            return FileResponse(open(filename, 'rb'))

        # Return error.
        return HttpResponse(500)

    @action(detail=False, methods=['get'])
    def water_properties(self, request):
        """
        Report water properties records in relation to a cruise and optional dive in a variety of formats.

        Water properties are stored in one table, distinguished by labels in one column. This is pivoted
        to provide column labels.
        """
        try:
            # Get the parameters.
            cruise_id = request.GET.get('cruise_id')                # The cruise name
            dive_id = request.GET.get('dive_id')                    # The optional dive.
            output_format = request.GET.get('fmt', '').lower()      # The output format.
            fn = request.GET.get('fn')                              # The function -- count
            labels = []

            if fn != 'count':
                # The title of the report.
                title = 'Water Properties'
                meta = None

                # Labels for tables.
                labels += [
                    'Timestamp', 'Cruise Name', 'Leg', 'Dive Name',
                    'Ship Longitude', 'Ship Latitude', 'Ship Time Delta',
                    'ROV Longitude', 'ROV Latitude', 'ROV Time Delta'
                ]

            try:
                # Try to parse the cruise ID.
                cruise_id = int(cruise_id)
            except Exception:
                raise Exception('A cruise is required.')

            try:
                # Try to parse the dive ID.
                dive_id = int(dive_id)
            except Exception:
                dive = None

            with connection.cursor() as cur:

                # If a dive is given, place it in array, else find all the dives for the given cruise.
                # If no cruise is given, get all dives.
                if dive_id:
                    dives = (dive_id,)
                elif cruise_id:
                    cur.execute('select id from rov.dive where cruise_id=%s', [cruise_id])
                    dives = tuple([row[0] for row in cur])
                else:
                    cur.execute('select id from rov.dive')
                    dives = tuple([row[0] for row in cur])

                # Short circuit if count is requested.
                if fn == 'count':
                    if (dive_id or cruise_id) and dives:
                        sql = '''
                            select count(distinct b."timestamp") 
                            from rov.measurement_position a
                                    inner join rov.measurement b on b.id=a.measurement_id
                                    inner join rov.instrument_config c on c.id = b.instrument_config_id
                                    inner join rov.platform_config d on d.id = c.platform_config_id
                                    inner join rov.dive e on e.sub_config_id = d.id
                            where e.id in %s
                        '''
                        cur.execute(sql, [dives])
                        count, = cur.fetchone()
                    else:
                        # If there are no dives, the result is zero.
                        count = 0

                    return JsonResponse({'result': {'count': count}})

                # Get the distinct unit fields for the dive(s).
                sql = f'''
                    select distinct f.unit 
                    from rov.measurement_position a
                            inner join rov.measurement b on b.id=a.measurement_id
                            inner join rov.instrument_config c on c.id = b.instrument_config_id
                            inner join rov.platform_config d on d.id = c.platform_config_id
                            inner join rov.dive e on e.sub_config_id = d.id
                            inner join rov.measurement_type f on f.id = b.measurement_type_id
                    where e.id in %s
                    order by f.unit
                '''
                cur.execute(sql, params=[dives])
                units = [row[0] for row in cur]

                # Add the units to the end of the labels array.
                labels += units

                # Join the unit fields into a query.
                unit_fields = ', '.join(['"{}" text'.format(u.replace('%', '%%').replace('"', '\"')) for u in units])

                # The crosstab select statement.
                sql = f"""
                    select * from 
                        crosstab('
                            select 
                                concat(
                                    to_char(b."timestamp", ''YYYY-MM-DD''), ''T'', 
                                    to_char(b."timestamp", ''HH24:MI:SS''), ''Z'') as "timestamp",
                                f.name as cruise_name, f.leg, e.name as dive_name,
                                st_x(g.geom::geometry) as ship_lon, st_y(g.geom::geometry) as ship_lat, -- TODO: Not valid.
                                0 as ship_diff,
                                st_x(g.geom::geometry) as rov_lon, st_y(g.geom::geometry) as rov_lat,
                                0 as rov_tdiff,
                                h.unit, b.quantity
                            from rov.measurement_position a
                                inner join rov.measurement b on b.id=a.measurement_id
                                inner join rov.instrument_config c on c.id = b.instrument_config_id
                                inner join rov.platform_config d on d.id = c.platform_config_id
                                inner join rov.dive e on e.sub_config_id = d.id
                                inner join rov.cruise f on f.id = e.cruise_id
                                inner join rov.position g on g.id = a.position_id
                                inner join rov.measurement_type h on h.id = b.measurement_type_id
                            where e.id=%s
                        ',' 
                            select distinct f.unit 
                            from rov.measurement_position a
                                    inner join rov.measurement b on b.id=a.measurement_id
                                    inner join rov.instrument_config c on c.id = b.instrument_config_id
                                    inner join rov.platform_config d on d.id = c.platform_config_id
                                    inner join rov.dive e on e.sub_config_id = d.id
                                    inner join rov.measurement_type f on f.id = b.measurement_type_id
                            where e.id in %s
                            order by f.unit
                       ') as ct(
                            "timestamp" text, cruise_name text, leg integer, dive_name text,
                            ship_lon real, ship_lat real, ship_tdiff real,
                            rov_lon real, rov_lat real, rov_tdiff real,
                            {unit_fields} 
                        )
                    order by "timestamp"
                """

                if output_format == 'html':
                    head = '''
                        <html>
                            <head>
                                <title>Water Properties</title>
                                <style>
                                    table {
                                        border-collapse: collapse;
                                    }
                                    td {
                                        border: 1px solid black;
                                    }
                                </style>
                            </head>
                            <body>
                                <h1>Water Properties</h1>
                                <h2>Results</h2>
                                <table>
                    '''
                    foot = '''
                                </table>
                            </body>
                        </html>
                    '''
                else:
                    head = None
                    foot = None

                tmp, content_type = ReportViewSet.render_result(cur, sql, dives, output_format, labels, head, foot)

                return FileResponse(open(tmp, 'rb'))
        except Exception as e:
            return JsonResponse({'error': str(e)})

    @staticmethod
    def render_result(cur, sql, dive_ids, output_format, labels, head='', foot=''):
        """
        Render a database result in the chosen format. The header and footer of the
        file are given as strings and written before and after the rows, which are formatted
        according to the corresponding row-rendering function.
        :param cur: The database cursor; should contain a row set.
        :param sql: The SQL query.
        :param dive_ids: The list of dive IDs.
        :param output_format: The output format.
        :param labels: A list of labels to use for formats whose output is in dictionary format. Must match the fields in the query.
        :param head: The header text, to be written before the body (rows).
        :param foot: The footer text, to be written after the body (rows).
        :return: A tuple containing the temporary file object and a string containing the MIME type.
        """

        db = None
        tmp = util.maketempfile(suffix=f'.{output_format}')
        content_type = None
        with open(tmp.path, 'w', newline='') as fd:
            if output_format == 'json':
                content_type = 'text/json'
                fd.write('[')

                def row_fn(keys, values):
                    fd.write(json.dumps(dict(zip(labels, values))) + '\n')

            elif output_format == 'excel':
                content_type = 'application/vnd.ms-excel'
                db = csv.writer(fd, dialect='excel')
                db.writerow(labels)

                def row_fn(keys, values):
                    db.writerow(values)

            elif output_format == 'csv':
                content_type = 'text/csv'
                db = csv.writer(fd)
                db.writerow(labels)

                def row_fn(keys, values):
                    db.writerow(values)

            elif output_format == 'html':
                content_type = 'text/html'
                fd.write(head)
                fd.write('<tr><th>' + '</th><th>'.join(labels) + '</th></tr>')

                def row_fn(keys, values):
                    fd.write('<tr><td>' + '</td><td>'.join(list(map(str, values))) + '</td></tr>')

            elif output_format == 'pdf':
                raise Exception('PDF not implemented.')

            # Converts Nones to empty strings.
            or_null = lambda a: '' if a is None else a

            for dive_id in dive_ids:
                # Generate report for each dive.
                cur.execute(sql, [dive_id, dive_ids])
                cols = [x[0] for x in cur.description]
                for row in cur:
                    if output_format == 'json':
                        # Don't replace Nones if the output is JSON.
                        row_fn(cols, row)
                    else:
                        row_fn(cols, [or_null(x) for x in row])

            if output_format == 'json':
                fd.write(']')
            if output_format == 'html':
                fd.write(foot)

        return tmp.path, content_type

    @action(detail=False, methods=['get'])
    def rov_data(self, request, bounds_query=None, bounds_params=None):
        """
        Report taxonomy records in relation to a cruise, with optional fields
        and search terms, in a variety of formats.

        If bounds is given, it's a GeoJSON geometry object and is used
        to filter the result. If no bounds is given, it's extracted from
        the request.
        """

        if not bounds_query:
            bounds_query, bounds_params = get_requested_bounds(request)

        fn = request.GET.get('fn', '')                          # The aggregate function.
        output_format = request.GET.get('fmt', '').lower()   # The output format.
        report_type = request.GET.get('type', '')               # The report type.
        cruise_name = request.GET.get('cruise_name', '')        # The name of the cruise.

        # If a geometry format is needed, set a flag.
        rawgeom = output_format in ('shp', 'sqlite')

        # Params for the query.
        params = []

        # Get results for dive tracks.
        if report_type == 'dive_tracks':

            title = 'Dive Tracks'
            labels = ['Cruise Name', 'Dive Name', 'Dive Start Time', 'Dive End Time', 'Track']
            meta = None

            # Add the boundary params to the params list.
            if bounds_params:
                params.extend(bounds_params)

            # Configure the filter for the cruise name.
            cruise_name_filter = ''
            if cruise_name:
                cruise_name_filter = 'AND a.name=%s'
                params.append(cruise_name)

            sql = f"""
                select c.name as cruise_name,
                    b.name as dive_name, b.start_time as dive_start, b.end_time as dive_end, 
                    st_simplifypreservetopology(a.geom::geometry, 0.001) as geom
                from cache.dive_track a
                    inner join rov.dive b on b.id = a.dive_id
                    inner join rov.cruise c on c.id = b.cruise_id
                where {bounds_query} && a.geom
                    {cruise_name_filter}
            """

            # If it's not a raw geometry, wrap with a GeoJSON call.
            if not rawgeom and not output_format == 'json':
                sql = f"""
                    select cruise_name, dive_name, dive_start, dive_end, 
                        '[geometry excluded]' as geom 
                    from ({sql}) t
                """

            # If it's json, format the geometry.
            elif output_format == 'json':
                sql = f"""
                    select cruise_name, dive_name, dive_start, dive_end, 
                        st_asgeojson(geom) as geom 
                    from ({sql}) t
                """

        # Get results for taxa.
        elif report_type == 'taxa':

            title = 'Observed Taxa'
            labels = ['Event ID', 'Dive Name', 'Cruise Name', 'Taxon ID', 'Original Label', 'Label', 'Scientitic Name',
                      'Common Name']
            meta = None

            # Add the boundary params to the params list.
            if bounds_params:
                params.extend(bounds_params)

            # Prepare the cruise name filter.
            cruise_name_filter = ''
            if cruise_name:
                cruise_name_filter = 'AND a.name=%s'
                params.append(cruise_name)

            sql = f"""
                select array_agg(distinct a.event_id) as event_ids, array_agg(distinct d.name) as dive_names, 
                    array_agg(distinct e.name) as cruise_names,
                    f.id as taxon_id, f.original_label, f.label, f.scientific_name, f.common_name 
                from rov.observation_event_position a
                    inner join rov.observation_event b on b.event_id = a.event_id
                    inner join rov.event c on c.id = b.event_id
                    inner join rov.dive d on d.id = c.dive_id
                    inner join rov.cruise e on e.id = d.cruise_id
                    inner join shared.taxon f on f.id = b.annotation_protocol_taxon_id
                    inner join rov.position g on g.id = a.start_position_id
                where 
                    {bounds_query} && g.geom
                    {cruise_name_filter}
                group by f.id, f.original_label, f.label, f.scientific_name
            """

            if rawgeom:
                pass  # There are no geom fields.

        # If a count is needed, wrap the sql.
        if fn == 'count':
            sql = f'select count(*) as count from ({sql}) t'

        with connection.cursor() as cur:

            if fn != 'count' and rawgeom:
                # If a geometry format is needed, use OGR to get it directly from the DB.
                # Process the query and params to get safe SQL.
                sql = cur.mogrify(sql, params).decode('utf-8')
                filename = render_spatial(output_format, sql)
                return FileResponse(open(filename, 'rb'))

            else:
                # Otherwise execute the query.
                cur.execute(sql, params)

            if fn == 'count':

                # If count, just get the count.
                try:
                    output = {'count': cur.fetchone()[0]}
                except:
                    output = {'count': 0}

            else:

                # If not count, turn the result into a list.
                output = []
                # Get the column names.
                cols = [x[0] for x in cur.description]

                # Prepare a row-handling function.
                row_fn = prepare_row(labels if output_format in ('html', 'pdf') else cols)

                # Process the rows.
                output = [row_fn(row) for row in cur]

        if fn == 'count' or output_format == 'json':

            # Export the count or result as JSON.
            return JsonResponse({'result': output})

        elif output_format == 'excel':

            # Export Excel.
            filename = render_excel(title, output)
            return FileResponse(open(filename, 'rb'))

        elif output_format == 'csv':

            # Export CSV.
            filename = render_csv(output)
            return FileResponse(open(filename, 'rb'))

        elif output_format == 'html':

            # Create an html file.
            filename = render_html(title, labels, output, meta)
            return FileResponse(open(filename, 'rb'))

        elif output_format == 'pdf':

            # Create a pdf file.
            filename = render_pdf(title, labels, output, meta)
            return FileResponse(open(filename, 'rb'))

        return HttpResponse(500)
