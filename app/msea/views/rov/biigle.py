
import json

from django.http import JsonResponse
from rest_framework.decorators import action

from msea.util import biigle

from msea.models.auth import User

from msea.views.auth import ReSTAdminViewSet, get_user


class BiigleViewSet(ReSTAdminViewSet): 
    """
    A simple service that allows pass-through queries to the Biigle ReST API.
    """

    @action(detail=False, methods=['get', 'post'], url_path='(?P<path>.+)')
    def biigle_api(self, request, path):
        """
        Forwards requests to the Biigle ReST API using the logged-in user's
        Biigle credentials.
        :param request: The Django request object.
        :param path: The path to the Biigle API endpoint.
        :return: The response from the Biigle API.
        """
        try:
            # Get the logged in user. This is where the Biigle credentials are stored.
            user = get_user(request)
            b = biigle.biigle()
            if request.method == 'POST':
                p = b.post_to_api(path, request.data, user.biigle_username, user.biigle_api_key)
            else:
                p = b.get_from_api(path, user.biigle_username, user.biigle_api_key)
            return JsonResponse({'result': p})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    @action(detail=False, methods=['get'])
    def all_trees(self, request):
        """
        Using all the user credentials stored in the User table, load
        all available label trees without duplicates and return the list.
        """
        try:
            users = User.objects.all()
            b = biigle.biigle()
            trees = {}
            for user in users:
                if user.biigle_username:
                    ps = b.get_from_api('label_trees', user.biigle_username, user.biigle_api_key)
                    if type(ps) is list:
                        for p in ps:
                            if not trees.get(p['id']):
                                p['users'] = set()
                                trees[p['id']] = p
                            trees[p['id']]['users'].add(user.biigle_username)
            return JsonResponse({'result': list(trees.values())}, json_dumps_params={'default': list})
        except Exception as e:
            return JsonResponse({'error': str(e)})


    @action(detail=False, methods=['get'])
    def all_projects(self, request):
        """
        Using all the user credentials stored in the User table,
        load all available projects (without duplicates) and return the list.
        Adds the Biigle username of the responsible user to a list of users on each
        project.
        """
        try:
            users = User.objects.all()
            b = biigle.biigle()
            projects = {}
            for user in users:
                if user.biigle_username:
                    ps = b.get_from_api('projects', user.biigle_username, user.biigle_api_key)
                    if type(ps) is list:
                        for p in ps:
                            if not projects.get(p['id']):
                                p['users'] = set()
                                projects[p['id']] = p
                            projects[p['id']]['users'].add(user.biigle_username)
            return JsonResponse({'result': list(projects.values())}, json_dumps_params={'default': list})
        except Exception as e:
            return JsonResponse({'error': str(e)})




