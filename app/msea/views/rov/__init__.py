"""
This file contains the view logic for the MSEA Django app.

Rob Skelly <Robert.Skelly@dfo-mpo.gc.ca>
"""
import os
import csv
import json
from datetime import datetime

from django.conf import settings
from django.db import transaction, connection, models
from django.http import HttpResponseServerError, HttpResponse, JsonResponse, FileResponse
from rest_framework import serializers, viewsets
from rest_framework.decorators import action
from drf_writable_nested import WritableNestedModelSerializer, UniqueFieldsMixin
from wsgiref.util import FileWrapper

from msea.models import shared
from msea.models import rov
from msea.views.rov import util as rov_util, geo_util
from msea.views.auth import ReSTUserViewSet, ReSTOpenViewSet
from msea.views.shared import PersonSerializer, OrganisationSerializer, \
    UploadedFileSerializer, SiteSerializer


class MediumTypeSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    """
    Serialize MediumType instances.
    """
    class Meta:
        model = rov.MediumType
        fields = '__all__'


class MediumTypeViewSet(ReSTUserViewSet):
    """
    Endpoint for MediumType instances.
    """
    serializer_class = MediumTypeSerializer
    model_class = rov.MediumType
    

class AnnotationSoftwareSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    """
    Serializer for AnnotationSoftware instances.
    """
    class Meta:
        model = rov.AnnotationSoftware
        fields = '__all__'


class AnnotationSoftwareViewSet(ReSTUserViewSet):
    """
    Endpoint for AnnotationSoftware instances.
    """
    serializer_class = AnnotationSoftwareSerializer
    model_class = rov.AnnotationSoftware


class AnnotationProtocolDocumentSerializer(WritableNestedModelSerializer):
    """
    Serializer for AnnotationProtocolDocument instances.
    """
    annotation_protocol = serializers.PrimaryKeyRelatedField(read_only=True)
    file = UploadedFileSerializer()

    class Meta:
        model = rov.AnnotationProtocolDocument
        fields = '__all__'


class AnnotationProtocolDocumentViewSet(ReSTUserViewSet):
    """
    Endpoint for AnnotationProtocolDocument instances.
    """
    serializer_class = AnnotationProtocolDocumentSerializer
    model_class = rov.AnnotationProtocolDocument

    @action(methods=['get'], detail=True)
    def file(self, request, pk=None, annotation_protocols_pk=None):
        """
        Return the file associated with the document object.
        :param request: The Django request object.
        :param pk: The primary key of the document (required).
        :param annotation_protocols_pk: The primary key of the annotaton protocol (not required).
        :return: An HttpResponse containing the file data and mime type.
        """
        apd = rov.AnnotationProtocolDocument.objects.get(pk=pk)
        path = os.path.join(settings.MSEA_UPLOAD_DIR, apd.file_name)
        with open(path, 'rb') as f:
            return HttpResponse(FileWrapper(f), content_type=apd.file_type,
                                headers={'Content-Disposition': f'inline; filename={apd.file_name}'})


class AnnotationProtocolSerializer(WritableNestedModelSerializer):
    """
    Serializer for AnnotationProtocol instances.
    """
    creator = PersonSerializer()
    medium_type = MediumTypeSerializer()
    annotation_software = AnnotationSoftwareSerializer()
    documents = AnnotationProtocolDocumentSerializer(many=True)
    
    class Meta:
        model = rov.AnnotationProtocol
        fields = '__all__'


class AnnotationProtocolViewSet(ReSTUserViewSet):
    """
    Endpoint for AnnotationProtocol instances.
    """
    serializer_class = AnnotationProtocolSerializer
    model_class = rov.AnnotationProtocol

    
class AnnotationJobRoleSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = rov.AnnotationJobRole
        fields = '__all__'


class AnnotationJobRoleViewSet(ReSTUserViewSet):
    serializer_class = AnnotationJobRoleSerializer
    model_class = rov.AnnotationJobRole


class AnnotationJobCrewSerializer(WritableNestedModelSerializer):
    annotation_job = serializers.PrimaryKeyRelatedField(read_only=True)
    person = PersonSerializer()
    role = AnnotationJobRoleSerializer()

    class Meta:
        model = rov.AnnotationJobCrew
        fields = '__all__'


class AnnotationJobCrewViewSet(ReSTUserViewSet):
    serializer_class = AnnotationJobCrewSerializer
    model_class = rov.AnnotationJobCrew
    

class AnnotationJobAnnotationProtocolSerializer(WritableNestedModelSerializer):
    annotation_job = serializers.PrimaryKeyRelatedField(read_only=True)
    annotation_protocol = AnnotationProtocolSerializer()

    class Meta:
        model = rov.AnnotationJobAnnotationProtocol
        fields = '__all__'


class AnnotationJobSerializer(WritableNestedModelSerializer):
    annotation_protocols = AnnotationJobAnnotationProtocolSerializer(many=True)
    crew = AnnotationJobCrewSerializer(many=True)
   
    class Meta:
        model = rov.AnnotationJob
        fields = '__all__'


class AnnotationJobViewSet(viewsets.ModelViewSet):
    serializer_class = AnnotationJobSerializer
    model_class = rov.AnnotationJob
    queryset = rov.AnnotationJob.objects.all()


class ProgramRoleSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    """
    Serializer for ProgramRole instances.
    """
    class Meta:
        model = rov.ProgramRole
        fields = '__all__'


class ProgramRoleViewSet(ReSTUserViewSet):
    """
    Endpoint for ProgramRole instances.
    """
    serializer_class = ProgramRoleSerializer
    model_class = rov.ProgramRole


class CruiseRoleSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    """
    Serializer for CruiseRole instances.
    """
    class Meta:
        model = rov.CruiseRole
        fields = '__all__'


class CruiseRoleViewSet(ReSTUserViewSet):
    """
    Endpoint for CruiseRole instances.
    """
    serializer_class = CruiseRoleSerializer
    model_class = rov.CruiseRole


class CruiseCrewSerializer(WritableNestedModelSerializer):
    """
    Serializer for CruiseCrew instances.
    """
    cruise = serializers.PrimaryKeyRelatedField(read_only=True)
    person = PersonSerializer()
    cruise_role = CruiseRoleSerializer()

    class Meta:
        model = rov.CruiseCrew
        fields = '__all__'


class CruiseCrewViewSet(ReSTUserViewSet):
    """
    Endpoint for CruiseCrew instances.
    """

    serializer_class = CruiseCrewSerializer
    model_class = rov.CruiseCrew


class ProgramMemberSerializer(WritableNestedModelSerializer):
    """
    Serializer for ProgramMember instances.
    """
    program = serializers.PrimaryKeyRelatedField(read_only=True)
    person = PersonSerializer()
    role = ProgramRoleSerializer()

    class Meta:
        model = rov.ProgramMember
        fields = '__all__'


class ProgramSerializer(WritableNestedModelSerializer):
    """
    Serializer for Program instances.
    """
    members = ProgramMemberSerializer(many=True)

    class Meta:
        model = rov.Program
        fields = '__all__'


class ProgramViewSet(ReSTUserViewSet):
    """
    Endpoint for Program instances.
    """
    serializer_class = ProgramSerializer
    model_class = rov.Program
    queryset = rov.Program.objects.all()

    def list(self, request, cruises_pk=None):
        try:
            if cruises_pk:
                programs = rov.CruiseProgram.objects.filter(pk=cruises_pk).values_list('program', flat=True)
            else:
                programs = rov.Program.objects.all()
            serializer = ProgramSerializer(programs, many=True)
            return JsonResponse({'result': serializer.data})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class ProgramMemberViewSet(ReSTUserViewSet):
    """
    Endpoint for ProgramMember instances.
    """
    serializer_class = ProgramMemberSerializer
    model_class = rov.ProgramMember


class MeasurementTypeSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    """
    Serializer for MeasurementType instances.
    """
    class Meta:
        model = rov.MeasurementType
        fields = '__all__'


class MeasurementTypeViewSet(ReSTUserViewSet):
    """
    Endpoint for MeasurementType instances.
    """
    serializer_class = MeasurementTypeSerializer
    model_class = rov.MeasurementType


class PositionTypeSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    """
    Serializer for PositionType instances.
    """
    class Meta:
        model = rov.PositionType
        fields = '__all__'


class PositionTypeViewSet(ReSTUserViewSet):
    """
    Endpoint for PositionType instances.
    """
    serializer_class = PositionTypeSerializer
    model_class = rov.PositionType


class EquipmentTypeSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = rov.EquipmentType
        fields = '__all__'


class EquipmentTypeViewSet(ReSTUserViewSet):
    serializer_class = EquipmentTypeSerializer
    model_class = rov.EquipmentType
    queryset = rov.EquipmentType.objects.all()

    def list(self, request):
        try:
            category = request.GET.get('category')
            q = self.get_list_queryset(request)
            if category:
                q = q.filter(category__iexact=category)
            return JsonResponse({'result': EquipmentTypeSerializer(q, many=True).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class ModelSerializer(WritableNestedModelSerializer):
    equipment_type = EquipmentTypeSerializer()

    class Meta:
        model = rov.Model
        fields = '__all__'


class ModelViewSet(ReSTUserViewSet):
    serializer_class = ModelSerializer
    model_class = rov.Model

    def list(self, request):
        try:
            category = request.GET.get('category')
            q = self.get_list_queryset(request)
            if category:
                et = rov.EquipmentType.objects.filter(category=category)
                q = q.filter(equipment_type__in=et)
            return JsonResponse({'result': ModelSerializer(q, many=True).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    @action(detail=False, methods=['get'])
    def refresh(self, request):
        """
        Pulls data from the Shiny app's SQLite database into the main database and
        transfers missing entities into the model, platform and instrument tables.
        """
        try:
            result = rov_util.refresh_from_ndst(request)
            result = None
            with transaction.atomic():

                pass
            return JsonResponse({'result': result})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class PlatformSerializer(WritableNestedModelSerializer):
    model = ModelSerializer()
    organisation = OrganisationSerializer()

    class Meta:
        model = rov.Platform
        fields = '__all__'


class PlatformViewSet(ReSTUserViewSet):
    serializer_class = PlatformSerializer
    model_class = rov.Platform
    queryset = rov.Platform.objects.all()


class InstrumentSerializer(WritableNestedModelSerializer):
    model = ModelSerializer()
    organisation = OrganisationSerializer()

    class Meta:
        model = rov.Instrument
        fields = '__all__'


class InstrumentConfigSerializer(WritableNestedModelSerializer):
    platform_config = serializers.PrimaryKeyRelatedField(read_only=True)
    instrument = InstrumentSerializer()
    
    class Meta:
        model = rov.InstrumentConfig
        fields = '__all__'


class PlatformConfigSerializer(WritableNestedModelSerializer):
    platform = PlatformSerializer()
    instrument_configs = InstrumentConfigSerializer(many=True)

    class Meta:
        model = rov.PlatformConfig
        fields = '__all__'


class PlatformConfigViewSet(ReSTUserViewSet):
    serializer_class = PlatformConfigSerializer
    model_class = rov.PlatformConfig


class CruiseFirstNationContactSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    cruise = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = rov.CruiseFirstNationContact
        fields = '__all__'


class DiveRoleSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = rov.DiveRole
        fields = '__all__'


class DiveCrewSerializer(WritableNestedModelSerializer):
    person = PersonSerializer()
    dive_role = DiveRoleSerializer()

    class Meta:
        model = rov.DiveCrew
        fields = '__all__'


class TransectSerializer(WritableNestedModelSerializer):
    dive = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = rov.Transect
        fields = '__all__'


class DiveSerializer(WritableNestedModelSerializer):
    cruise = serializers.PrimaryKeyRelatedField(read_only=True)
    sub_config = PlatformConfigSerializer()
    ship_config = PlatformConfigSerializer()
    site = SiteSerializer(allow_null=True)
    transects = TransectSerializer(many=True)
    crew = DiveCrewSerializer(many=True)

    class Meta:
        model = rov.Dive
        fields = '__all__'


class CruiseDocumentSerializer(WritableNestedModelSerializer):
    """
    Serializer for CruiseDocument instances.
    """
    cruise = serializers.PrimaryKeyRelatedField(read_only=True)
    file = UploadedFileSerializer()

    class Meta:
        model = rov.CruiseDocument
        fields = '__all__'


class CruiseDocumentViewSet(ReSTOpenViewSet):
    """
    Endpoint for CruiseDocument instances.
    """
    serializer_class = CruiseDocumentSerializer
    model_class = rov.CruiseDocument


class CruiseProgramSerializer(WritableNestedModelSerializer):
    cruise = serializers.PrimaryKeyRelatedField(read_only=True)
    program = ProgramSerializer()

    class Meta:
        model = rov.CruiseProgram
        fields = '__all__'


class CruiseSerializer(WritableNestedModelSerializer):
    programs = CruiseProgramSerializer(many=True)
    first_nation_contacts = CruiseFirstNationContactSerializer(many=True)
    crew = CruiseCrewSerializer(many=True)
    ship = PlatformSerializer()
    documents = CruiseDocumentSerializer(many=True)

    class Meta:
        model = rov.Cruise
        fields = '__all__'


class CruiseSummaryViewSet(ReSTOpenViewSet):
    serializer_class = CruiseSerializer
    model_class = rov.Cruise

    def list(self, request):
        """
        Produces a CSV file with a summary of cruises, dives, start and end coordinates and maximum depths.
        Requested by the Ocean Discovery League (Brian Kennedy, Kristen Johansen).
        """
        # Will force the cached file to be overwritten.
        force = request.GET.get('force', False)
        
        # The cruise summary file is not subject to automatic clean-up (like maketempfile()), 
        # but it may be cleaned up on reboot.
        dl_dir = settings.MSEA_DOWNLOAD_DIR
        filename = os.path.join(dl_dir, 'odl_cruise_summary.csv')

        if force or not os.path.exists(filename):
            
            sql = '''
                with depth as (
                    select p.id as platform_config_id, max(m.quantity) as max_depth
                    from rov.measurement m
                        inner join rov.instrument_config i on i.id=m.instrument_config_id
                        inner join rov.platform_config p on p.id=i.platform_config_id
                    where measurement_type_id=(select id from rov.measurement_type where name='Depth' and unit='m')
                    group by p.id
                ), start_pos as (
                    select distinct on (p.id) p.id as platform_config_id, st_x(m.geom::geometry) as start_lon, st_y(m.geom::geometry) as start_lat
                    from rov.position m
                        inner join rov.instrument_config i on i.id=m.instrument_config_id
                        inner join rov.platform_config p on p.id=i.platform_config_id
                    group by p.id, geom::geometry, timestamp
                    order by p.id, timestamp asc
                ), end_pos as (
                    select distinct on (p.id) p.id as platform_config_id, st_x(m.geom::geometry) as end_lon, st_y(m.geom::geometry) as end_lat
                    from rov.position m
                        inner join rov.instrument_config i on i.id=m.instrument_config_id
                        inner join rov.platform_config p on p.id=i.platform_config_id
                    group by p.id, geom::geometry, timestamp
                    order by p.id, timestamp desc
                )
                select c.id, c.name as cruise_name, c.leg as cruise_leg, d.name as dive_name, d.start_time, d.end_time, concat(l.brand_name, ' ', l.model_name) as rov_name, 
                    depth.max_depth, start_pos.start_lat, start_pos.start_lon, end_pos.end_lat, end_pos.end_lon
                from rov.cruise c 
                    inner join rov.dive d on d.cruise_id=c.id
                    inner join rov.platform_config p on p.id=d.sub_config_id
                    inner join rov.platform f on f.id=p.platform_id
                    inner join rov.model l on l.id=f.model_id
                    inner join depth on depth.platform_config_id=p.id
                    inner join start_pos on start_pos.platform_config_id=p.id
                    inner join end_pos on end_pos.platform_config_id=p.id
                where c.approved > 1 
                group by c.id, c.name, c.leg, d.name, d.start_time, d.end_time, l.brand_name, l.model_name, start_pos.start_lat, start_pos.start_lon, end_pos.end_lat, end_pos.end_lon, depth.max_depth
                order by c.name, d.start_time;
            '''
            fields = ['cruise_id', 'cruise_name', 'cruise_leg', 'dive_name', 'start_time', 'end_time', 'rov', 'max_depth', 'start_lat', 'start_lon', 'end_lat', 'end_lon']
            with open(filename, 'w', newline='') as f:
                db = csv.writer(f)
                db.writerow(['Generated on', datetime.now().strftime('%Y-%m-%d %H:%M:%S')])
                db.writerow(fields)
                with connection.cursor() as cur:
                    cur.execute(sql)
                    for row in cur:
                        db.writerow(row)

        return FileResponse(open(filename, 'rb'), filename='cruise_summary.csv', as_attachment=True)

    
class CruiseViewSet(ReSTUserViewSet):
    serializer_class = CruiseSerializer
    model_class = rov.Cruise
    queryset = rov.Cruise.objects.all()

    def retrieve(self, request, pk=None):
        """
        This is a modification of the retreive method to allow loading of cruises using the 
        name-leg format.
        """
        try:
            try:
                cruise = rov.Cruise.objects.get(pk=int(pk))
            except:
                # If PK is a string, assume it's a dash-delimited combo of the name and leg.
                name, _, leg = str(pk).rpartition('-')
                cruise = rov.Cruise.objects.filter(name__iexact=name, leg=leg).first()
            return JsonResponse({'result': CruiseSerializer(cruise).data})
        except Exception as e:
            return JsonResponse({'error': 'Failed to load cruise {pk}: ' + str(e)})
    
    def list(self, request):
        """
        Return cruises in shallow form, without dives and associated child data.
        Use retrieve for deep cruise data.
        """
        try:
            if request.GET.get('unapproved', False):
                # If the unapproved flag is true, return all the cruises whether they're approved or not.
                cruises = rov.Cruise.objects.all()
            else:
                # Otherwise only return the ones with an approved value of greater than 1 (pending).
                cruises = rov.Cruise.objects.filter(approved__gt=1)
            # Sort the cruises by name and leg.
            cruises = cruises.order_by('name', 'leg')
            return JsonResponse({'result': CruiseSerializer(cruises, many=True).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    @action(detail=False, methods=['get'])
    def summary(self, request):
        """
        Produces a CSV file with a summary of cruises, dives, start and end coordinates and maximum depths.
        Requested by the Ocean Discovery League (Brian Kennedy, Kristen Johansen).
        """
        # Will force the cached file to be overwritten.
        force = request.GET.get('force', False)
        
        # The cruise summary file is not subject to automatic clean-up (like maketempfile()), 
        # but it may be cleaned up on reboot.
        dl_dir = settings.MSEA_DOWNLOAD_DIR
        filename = os.path.join(dl_dir, 'odl_cruise_summary.csv')

        if force or not os.path.exists(filename):
            
            sql = '''
                with depth as (
                    select p.id as platform_config_id, max(m.quantity) as max_depth
                    from rov.measurement m
                        inner join rov.instrument_config i on i.id=m.instrument_config_id
                        inner join rov.platform_config p on p.id=i.platform_config_id
                    where measurement_type_id=(select id from rov.measurement_type where name='Depth' and unit='m')
                    group by p.id
                ), start_pos as (
                    select distinct on (p.id) p.id as platform_config_id, st_x(m.geom::geometry) as start_lon, st_y(m.geom::geometry) as start_lat
                    from rov.position m
                        inner join rov.instrument_config i on i.id=m.instrument_config_id
                        inner join rov.platform_config p on p.id=i.platform_config_id
                    group by p.id, geom::geometry, timestamp
                    order by p.id, timestamp asc
                ), end_pos as (
                    select distinct on (p.id) p.id as platform_config_id, st_x(m.geom::geometry) as end_lon, st_y(m.geom::geometry) as end_lat
                    from rov.position m
                        inner join rov.instrument_config i on i.id=m.instrument_config_id
                        inner join rov.platform_config p on p.id=i.platform_config_id
                    group by p.id, geom::geometry, timestamp
                    order by p.id, timestamp desc
                )
                select c.id, c.name as cruise_name, c.leg as cruise_leg, d.name as dive_name, d.start_time, d.end_time, concat(l.brand_name, ' ', l.model_name) as rov_name, 
                    depth.max_depth, start_pos.start_lat, start_pos.start_lon, end_pos.end_lat, end_pos.end_lon
                from rov.cruise c 
                    inner join rov.dive d on d.cruise_id=c.id
                    inner join rov.platform_config p on p.id=d.sub_config_id
                    inner join rov.platform f on f.id=p.platform_id
                    inner join rov.model l on l.id=f.model_id
                    inner join depth on depth.platform_config_id=p.id
                    inner join start_pos on start_pos.platform_config_id=p.id
                    inner join end_pos on end_pos.platform_config_id=p.id
                where c.approved > 1 
                group by c.id, c.name, c.leg, d.name, d.start_time, d.end_time, l.brand_name, l.model_name, start_pos.start_lat, start_pos.start_lon, end_pos.end_lat, end_pos.end_lon, depth.max_depth
                order by c.name, d.start_time;
            '''
            fields = ['cruise_id', 'cruise_name', 'cruise_leg', 'dive_name', 'start_time', 'end_time', 'rov', 'max_depth', 'start_lat', 'start_lon', 'end_lat', 'end_lon']
            with open(filename, 'w', newline='') as f:
                db = csv.writer(f)
                db.writerow(['Generated on', datetime.now().strftime('%Y-%m-%d %H:%M:%S')])
                db.writerow(fields)
                with connection.cursor() as cur:
                    cur.execute(sql)
                    for row in cur:
                        db.writerow(row)

        return FileResponse(open(filename, 'rb'), filename='cruise_summary.csv', as_attachment=True)


class DiveRoleViewSet(ReSTUserViewSet):
    serializer_class = DiveRoleSerializer
    model_class = rov.DiveRole


class MediumTypeViewSet(ReSTUserViewSet):
    serializer_class = MediumTypeSerializer
    model_class = rov.MediumType


class MediumFormatSerializer(WritableNestedModelSerializer):
    medium_type = MediumTypeSerializer()

    class Meta:
        model = rov.MediumFormat
        fields = '__all__'


class MediumFormatViewSet(ReSTUserViewSet):
    serializer_class = MediumFormatSerializer
    model_class = rov.MediumFormat
    queryset = rov.MediumFormat.objects.all()


class TransectViewSet(ReSTUserViewSet):
    serializer_class = TransectSerializer
    model_class = rov.Transect
    queryset = rov.Transect.objects.all()

    @action(detail=False, methods=['post'])
    def download(self, request):
        """
        Downloads a geospatial file containing dive and transect segments.
        """
        try:
            # Cruise.
            cruise = request.data.get('cruise')
            format = request.data.get('fmt', '').lower()

            # Process the request and return the filenames.
            attachment_filename, dl_filename = geo_util.download_dt(cruise, format, 'transects', connection)
            
            return JsonResponse({'download': {'attachment_filename': attachment_filename, 'filename': dl_filename}})
        except Exception as e:
            return HttpResponseServerError('Error in transect download: ' + str(e))

    def list(self, request, dives_pk=None):
        if dives_pk:
            try:
                transects = rov.Transect.objects.filter(dive=dives_pk)
                return JsonResponse({'result': TransectSerializer(transects, many=True).data})
            except Exception as e:
                return JsonResponse({'error': str(e)})
        else:
            return super().list(request)


class DiveViewSet(ReSTUserViewSet):
    serializer_class = DiveSerializer
    model_class = rov.Dive
    queryset = rov.Dive.objects.all()

    @action(detail=False, methods=['post'])
    def download(self, request):
        """
        Downloads a geospatial file containing dive and transect segments.
        """
        try:
            # Cruise.
            cruise = request.data.get('cruise')
            format = request.data.get('fmt', '').lower()

            # Process the request and return the filenames.
            attachment_filename, dl_filename = geo_util.download_dt(cruise, format, 'dives', connection)
            
            return JsonResponse({'download': {'attachment_filename': attachment_filename, 'filename': dl_filename}})
        except Exception as e:
            return HttpResponseServerError('Error in dive download: ' + str(e))

    @action(detail=True, methods=['get'])
    def track(self, request, pk):
        try:
            with connection.cursor() as cur:
                cur.execute('''
                    select st_asgeojson(st_transform(st_simplifypreservetopology(st_makeline(st_force2d(geom::geometry)), 0.0001), 4326)) as geom 
                    from cache.dive_track
                    where dive_id = %s
                ''', [pk])
                result = cur.fetchone()
                if result:
                    # If there's a result, parse the json.
                    path = json.loads(result[0])
                else:
                    path = []
                return JsonResponse({'result': path})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    @action(detail=True, methods=['get'])
    def data_counts(self, request, pk):
        """
        Return a dictionary of data counts for the dive. These include observation, habitat, comment and
        status events, plus measurements and positions.
        :param request: A Django request which represents the HTTP request.
        :param pk: The ID of the dive.
        :return: A Response.
        """
        try:
            dive = rov.Dive.objects.get(pk=pk)
            # A list of instrument configs used on the sub.
            ics = rov.InstrumentConfig.objects.filter(
                platform_config=dive.sub_config
            )
            # TODO: This is way too slow.
            result = {
                'observation_event': rov.Event.objects.filter(
                    models.Q(dive=dive) & \
                    models.Q(properties__event_types__icontains='observation') & \
                    (models.Q(properties__restrictions__isnull=True) | models.Q(properties__restrictions__exact=[]))
                ).count(),
                'habitat_event': rov.Event.objects.filter(dive=dive, properties__event_types__icontains='habitat').count(),
                'comment_event': rov.Event.objects.filter(dive=dive, properties__event_types__icontains='comment').count(),
                'status_event': rov.Event.objects.filter(dive=dive, properties__event_types__icontains='status').count(),
                'measurement_event': rov.Event.objects.filter(dive=dive, properties__event_types__icontains='measurement').count(),
                'measurement': rov.Measurement.objects.filter(instrument_config__in=ics).count(),
                'position': rov.Position.objects.filter(instrument_config__in=ics).count()
            }
            return JsonResponse({'result': result})
        except Exception as e:
            return JsonResponse({'error': str(e)})


    def list(self, request, cruises_pk=None):
        """
        Returns a list of dives. Filter on given properties.
        The cruise ID is technically optional, but the request will fail if it is not given --
        there are too many dives to return them all.
        :param request: A Django request representing the HTTP request.
        :param cruises_pk: The cruise ID.
        :return: A Response.
        """
        try:
            # If a cruise ID is given, find the dives that belong to it. Otherwise, fail.
            if cruises_pk:
                dives = self.get_queryset().filter(cruise_id=cruises_pk)
            else:
                raise Exception('The dive list cannot be loaded whole. Provide a cruise ID to filter.')
            dives = dives.order_by('name', 'start_time')
            return JsonResponse({'result': DiveSerializer(dives, many=True).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    
    @staticmethod
    def create_or_update_instrument_configs(platform_config, data):
        """
        Retrieves and updates, or creates, an instrument config for each item in data, which is a list.
        """
        for item in data:
            config = None
            try:
                config = rov.InstrumentConfig.objects.get(pk=rov_util.obj_id(item))
            except: pass
            if config:
                config.platform_config = platform_config
                config.configuration = item['configuration']
                config.note = item['note']
                config.save()
            else:
                config = rov.InstrumentConfig.objects.create(
                    platform_config = platform_config,
                    configuration = item['configuration'],
                    note = item['note']
                )


    @staticmethod
    def create_or_update_platform_config(data):
        """
        Retrieves and updates, or creates, a platform config with the given data.
        """
        config = None
        try:
            config = rov.PlatformConfig.objects.get(pk=rov_util.obj_id(data))
        except: pass
        if config:
            config.platform = rov.Platform.objects.get(pk=rov_util.obj_id(data['platform']))
            config.configuration = data['configuration']
            config.note = data['note']
        else:
            config = rov.PlatformConfig.objects.create(
                platform = rov.Platform.objects.get(pk=rov_util.obj_id(data['platform'])),
                configuration = data['configuration'],
                note = data['note']
            )
        config.save()

        DiveViewSet.create_or_update_instrument_configs(config, data['instrument_configs'])

        return config


class DiveCrewViewSet(ReSTUserViewSet):
    serializer_class = DiveCrewSerializer
    model_class = rov.DiveCrew


class InstrumentViewSet(ReSTUserViewSet):
    serializer_class = InstrumentSerializer
    model_class = rov.Instrument
    queryset = rov.Instrument.objects.all()

    def list(self, request, dives_pk=None, instrument_configs_pk=None):
        try:
            if instrument_configs_pk:
                ic = rov.InstrumentConfig.objects.get(pk=instrument_configs_pk)
                instruments = [ic.instrument]
            if dives_pk:
                # Get instruments used by the given dive by linking through
                # the platform and instrument configurations.
                dive = rov.Dive.objects.get(pk=dives_pk)
                instruments = rov.InstrumentConfig.objects \
                    .filter(models.Q(platform_config=dive.sub_config) | models.Q(platform_config=dive.ship_config)) \
                    .values_list('instrument').distinct().order_by('model__brand_name', 'model__model_name')
            else:
                instruments = rov.Instrument.objects.order_by('model__brand_name', 'model__model_name')
            return JsonResponse({'result': InstrumentSerializer(instruments, many=True).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class StatusTypeDetailSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    status_type = serializers.PrimaryKeyRelatedField(read_only=True)
    
    class Meta:
        model = rov.StatusTypeDetail
        fields = '__all__'


class StatusTypeSerializer(WritableNestedModelSerializer):
    details = StatusTypeDetailSerializer(many=True)

    class Meta:
        model = rov.StatusType
        fields = '__all__'


class StatusTypeViewSet(ReSTUserViewSet):
    serializer_class = StatusTypeSerializer
    model_class = rov.StatusType
    queryset = rov.StatusType.objects.all()


class StatusTypeDetailViewSet(ReSTUserViewSet):
    serializer_class = StatusTypeDetailSerializer
    model_class = rov.StatusTypeDetail
    queryset = rov.StatusTypeDetail.objects.all()

    def list(self, request, status_types_pk=None):
        if status_types_pk:
            return JsonResponse({
                'result': StatusTypeDetailSerializer(
                    rov.StatusTypeDetail.objects.filter(status_type_id=status_types_pk), many=True
                ).data
            })
        else:
            return super().list(request)
        

class EventSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = rov.Event
        fields = '__all__'


class EventViewSet(ReSTUserViewSet):
    serializer_class = EventSerializer
    model_class = rov.Event

    def list(self, request, dives_pk=None):
        if not dives_pk:
            return super().list(request)
        else:
            evt_type = request.GET.get('type')
            if not evt_type:
                raise Exception('An event type must be provided.')
            queryset = self.model_class.objects.raw("""
                select * 
                from rov.event 
                where dive_id=%(dive_id)s and properties->'event_types' ? lower(%(evt_type)s) 
                order by start_time""", 
                {'evt_type': evt_type, 'dive_id': dives_pk}
            )
            queryset = self.paginate(queryset, request)
            return self.render_list(queryset, request)


class MeasurementSerializer(WritableNestedModelSerializer):
    measurement_type = MeasurementTypeSerializer()

    class Meta:
        model = rov.Measurement
        fields = '__all__'


class MeasurementViewSet(ReSTUserViewSet):
    serializer_class = MeasurementSerializer
    model_class = rov.Measurement

    def list(self, request, dives_pk=None):
        if not dives_pk:
            raise Exception('Cannot load entire list of measurements.')
        else:
            dive = rov.Dive.objects.get(pk=dives_pk)
            ics = rov.InstrumentConfig.objects.filter(platform_config=dive.sub_config)
            queryset = rov.Measurement.objects.filter(instrument_config__in=ics).order_by('timestamp')
            queryset = self.paginate(queryset, request)
            return self.render_list(queryset, request)


class PositionSerializer(WritableNestedModelSerializer):
    coordinates = serializers.CharField()

    class Meta:
        model = rov.Position
        exclude = 'geom',


class PositionViewSet(ReSTUserViewSet):
    serializer_class = PositionSerializer
    model_class = rov.Position

    def list(self, request, dives_pk=None): #TODO: instrument_configs_pk, platform_configs_pk
        if not dives_pk:
            raise Exception('Cannot load entire list of positions.')
        else:
            dive = rov.Dive.objects.get(pk=dives_pk)
            ics = rov.InstrumentConfig.objects.filter(platform_config=dive.sub_config)
            queryset = rov.Position.objects.raw("""
                select *, '[' || st_x(geom::geometry) || ',' || st_y(geom::geometry) || ']' as coordinates
                from rov.position
                where instrument_config_id in %s
                order by timestamp
            """, [tuple([i.id for i in ics])])
            queryset = self.paginate(queryset, request)
            return self.render_list(queryset, request)


class InstrumentConfigViewSet(ReSTUserViewSet):
    serializer_class = InstrumentConfigSerializer
    model_class = rov.InstrumentConfig

    @action(methods=['get'], detail=True)
    def measurement_summary(self, request, pk=None):
        """
        Return a list of measurement types with counts for the instrument configuration.
        """
        from django.db.models import Count
        meas = rov.Measurement.objects.filter(instrument_config=rov.InstrumentConfig.objects.get(pk=pk)) \
            .values('measurement_type__name').annotate(count=Count('measurement_type__name'))
        return JsonResponse({'result': [{'type': m['measurement_type__name'], 'count': m['count']} for m in meas]})

    def list(self, request, configurations_pk=None):
        return JsonResponse({'error': 'Retrieving the whole list is not allowed.'})


class DiveTrackSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = None


class DiveTrackViewSet(ReSTUserViewSet):
    """
    Provides an endpoint for retreiving dive track geometries.
    """
    model_class = None
    serializer_class = DiveTrackSerializer
    
    @action(detail=False, methods=['get'])
    def dive_tracks(self, request):
        """
        Retrieve dive tracks as GeoJSON with attributes.
        :param request: A Django request object representing the HTTP request.
        :return: A Response.
        """
        try:
            cur = connection.cursor()
            cur.execute('''
                select dive_name, dive_id, cruise_name, cruise_id, color, 
                    st_asgeojson(geom) as geom
                from cache.dive_track
            ''')
            cols = [desc[0] for desc in cur.description]
            return JsonResponse({'result': [dict(zip(cols, row)) for row in cur.fetchall()]})
        except Exception as e:
            return JsonResponse({'error': str(e)})

