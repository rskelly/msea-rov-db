import re
import os
import base64
import json
import requests
import sqlite3
import shutil
from contextlib import closing

from django.db import connection, transaction
from django.conf import settings

from msea.util import util
from msea.models.shared import UploadedFile

def strnone(v):
    """
    If the value is an empty string, return None, else return the value.
    """
    if v == '':
        return None
    return v

def obj_id(obj):
    """
    Get the ID of the object if it is not an integer, otherwise
    return the object itself, as it is an id. If the parameter is None,
    return None.
    :param obj: An object with an 'id' property, or a string/number parseable as an int.
    """
    if obj is None:
        return None
    elif isinstance(obj, dict):
        return obj.get('id', 0)
    else:
        return int(obj)


def refresh_from_ndst(request):
    """
    Pulls data from the Shiny app's SQLite database into the main database.
    Uses the row-id (GUID) for matching. The file path is stored in the configuration
    variable, 'ndst_db_path'.

    If people or equipment are missing from the local database, the administrator can configure them through
    the admin page.

    :param request: The Django request object reresenting the HTTP request that initiated this call.
    :return: A dictionary containing: 
        'errors', a list of errors;
        'missing_people': a list of missing people; 
        'missing_equipment', a list of missing equipment;
        'missing_types', a list of missing equipment types.
    """
    # Download the db file from the site and write to a temp file.
    db_path = settings.MSEA_NDST_DB_PATH
    tf = util.maketempdir()
    with open(tf.path, 'wb') as fo:
        with requests.get(db_path, stream=True) as fi:
            for chunk in fi.iter_content(chunk_size=2048):
                fo.write(chunk)

    # Connect to the downloaded sqlite database.
    with closing(sqlite3.connect(tf.path)) as sconn:
        scur = sconn.cursor()
        scur.execute('pragma encoding=UTF8')
        
        # Connect to the main database.
        with connection.cursor() as pcur:

            # Get the table list.
            scur.execute("select name from sqlite_master where type='table'")
            tables = [x for x in scur.fetchall()]

            with transaction.atomic():
                # For each table, copy or update all of the columns.
                for table, in tables:
                    # Get the column list.
                    scur.execute(f"pragma table_info('{table}')")
                    cols = [x[1] for x in scur.fetchall()]
                    # Get the columns for insert.
                    col_names = '"{}"'.format('","'.join(cols))
                    # Get the columns for update.
                    set_names = ','.join([f'"{col}"=%s' for col in cols])
                    # Get the column placeholders.
                    col_values = ','.join(['%s'] * len(cols))
                    # Select the row values.
                    scur.execute(f'select {col_names} from {table}')
                    pcur.execute(f'''update "ndst"."{table}" set active='f' ''')
                    for row in scur:
                        # Insert or update. Deactivate the record first.
                        pcur.execute(f'''
                            insert into "ndst"."{table}" ({col_names}, "active") values ({col_values}, 't')
                            on conflict(row_id) do update set {set_names}, "active"='t'
                        ''', row + row)

                # Check if there are missing people mappings in the database.
                pcur.execute('''
                    select a.*, b.id 
                    from ndst.people a 
                        left join shared.person b on lower(a.email)=lower(b.email) 
                    where b.id is null
                ''')
                cols = [desc[0] for desc in pcur.description]
                missing_people = [dict(zip(cols, row)) for row in pcur]

                # Check if there are missing equipment mappings in the database.
                pcur.execute('''
                    select * from ndst.equipment 
                    where short_code not in (
                        select short_code from rov.platform
                        union
                        select short_code from rov.instrument
                    ) and short_code_mapped is null or short_code_mapped not in (
                        select short_code from rov.platform
                        union
                        select short_code from rov.instrument
                    )
                ''')
                cols = [desc[0] for desc in pcur.description]
                missing_equipment = [dict(zip(cols, row)) for row in pcur]

                # Check for missing equipment types.
                pcur.execute('''
                    select type 
                    from ndst.equipment 
                    where lower(type) not in (
                        select lower(name) from rov.equipment_type
                    )
                ''')
                cols = [desc[0] for desc in pcur.description]
                missing_types = [dict(zip(cols, row)) for row in pcur]

                # Map the users.
                pcur.execute('''
                    update ndst.people a set person_id=b.id 
                    from shared.person b 
                    where lower(a.email)=lower(b.email)
                ''')

                # Map the instruments.
                pcur.execute('''
                    update ndst.equipment a set instrument_id=b.id
                    from rov.instrument b
                    where a.short_code=b.short_code
                        and lower(a.type) in (select lower(name) from rov.equipment_type where category='Instrument')
                ''')
                
                # Map the platforms.
                pcur.execute('''
                    update ndst.equipment a set platform_id=b.id
                    from rov.platform b
                    where a.short_code=b.short_code 
                        and lower(a.type) in (select lower(name) from rov.equipment_type where category='Platform')
                ''')

    # Remove the donloaded file.
    try:
        os.unlink(tf.path)
    except: pass

    return {
        'errors': len(missing_people) > 0 or len(missing_equipment) > 0 or len(missing_types),
        'missing_people': missing_people,
        'missing_equipment': missing_equipment,
        'missing_types': missing_types
    }

X1 = -170. #-154.81933593750003
Y1 = 80. #58.263287052486035
X2 = -100. #-115.22460937500001
Y2 = 0. #44.715513732021336

def get_bounds_query(bounds):
    """
    Return a tuple containing a string representing a parameterized PostGIS geometry extracted 
    from a dictionary containing bounds in the form x1, y1, x2, y2.
    
    If there's no valid data, return the maximum extent.

    :param bounds: A dictionary containing bounds in the form x1, y1, x2, y2.
    :return: A tuple containing a formatted PostGIS geometry and a list of the parameters.
    """
    query = "st_geomfromtext('POLYGON((%s %s, %s %s, %s %s, %s %s, %s %s))', 4326)"
    params = None
    try:
        # If no bounds are POSTed, create a WKT polygon with the map's bounds,
        # either from URL params, or default to the whole Earth.
        x1 = float(bounds.get('x1', X1))
        y1 = float(bounds.get('y1', Y1))
        x2 = float(bounds.get('x2', X2))
        y2 = float(bounds.get('y2', Y2))
        params = [x1, y1, x2, y1, x2, y2, x1, y2, x1, y1]
    except:
        params = [X1, Y1, X2, Y1, X2, Y2, X1, Y2, X1, Y1]
    return query, params

def get_requested_bounds(request):
    """
    Return a tuple containing a string representing a parameterized PostGIS geometry extracted 
    from a GET or POST request and a list of parameters.
    
    If the request is a POST, it's expected to have a GeoJSON geometry object in the
    request body. If it's a GET, it could have a base64-encoded JSON geometry object, or a box defined by the
    coordinates, x1, y1, x2 and y2.

    The geometry and parameters are expected to be with respect to WGS84, ing decimal degrees.

    If there's no valid data, just returns None.

    :param request: The Django object representing an HTTP request.
    :return: A tuple containing a formatted PostGIS geometry and a list of the parameters.
    """

    query = None
    params = None
    try:
        if bounds:
            # If this is a GET and there's GeoJSON in it, try to decode it.
            if isinstance(bounds, bytes):
                bounds = bounds.decode('utf-8')
            geomtxt = base64.b64decode(bounds).decode('utf-8')
        else:
            # Otherwise assume it's a POST and try to decode the body.
            geomtxt = request.body.decode('utf-8')
        # Try to parse the geometry.
        geom = json.loads(geomtxt)
        if geom.get('coordinates'):
            # If it's a legitimate geometry, it will have a 'coordinates' property.
            query = 'st_setsrid(st_geomfromgeojson(%s), 4326)'
            params = [geomtxt]
    except:
        pass

    if not query:
        # If no bounds are POSTed, create a WKT polygon with the map's bounds,
        # either from URL params, or default to the whole Earth.
        x1 = float(request.GET.get('x1', -179.99))
        y1 = float(request.GET.get('y1', 90.))
        x2 = float(request.GET.get('x2', 180.))
        y2 = float(request.GET.get('y2', -89.99))
        query = "st_geomfromtext('POLYGON((%s %s, %s %s, %s %s, %s %s, %s %s))', 4326)"
        params = [x1, y1, x2, y1, x2, y2, x1, y2, x1, y1]

    return query, params

def find_unique_name(path):
    """
    If the file exists, create a new name with an integer appended before the extension.
    If the filename already has an integer, increment it.
    Return the new path.
    """
    rem = re.compile(r'^.+_([0-9]+)$')                      # Match a number after an underscore at the end of a path; group the int.
    rer = re.compile(r'_[0-9]+$')                           # Match a number after an underscore at the end of a path.

    dirname = os.path.dirname(path)
    while os.path.exists(path):
        # Get the name and extension.
        name, ext = os.path.splitext(os.path.basename(path))
        try:
            # If there's an underscore and int after the name, capture it and remove it.
            m = rem.match(name)
            i = int(m.group(1)) + 1
            name = rer.sub('', name)
        except Exception as e:
            # If the match failes, use 1.
            i = 1
        # Build the new name and path.
        name = f'{name}_{i}{ext}'
        path = os.path.join(dirname, name)
    
    return path

def handle_documents(cls, inst, field_name, documents):
    """
    Associates the document items with any uploaded files. The uploaded_file_id
    property should have been set on the request data, and must be deleted
    before saving.
    :param cls: The document class (CruiseDocument, AnnotationProtocolDocument, etc.)
    :param inst: The owner instance (Cruise, AnnotationProtocol, etc.)
    :param field_name: The field name on the document entity that refers to the owning class. For example, 
                       on a CruiseDocument, the field name is 'cruise' which is set to an instance of Cruise.
    :param documents: A list of documents from the request object.
    """
    raise Exception('This method should be deprecated.')
    upload_dir = settings.MSEA_UPLOAD_DIR                   # Get the upload folder.
    documents_dir = settings.MSEA_DOCUMENTS_DIR             # Get the documents folder.

    for d in (upload_dir, documents_dir):
        try:
            os.makedirs(d)
        except: pass

    # Iterate over the document items.
    for doc in documents:
        uid = doc.get('file_id', 0)
        if uid > 0:
            # If there's an uploaded file, retrieve it.
            upload = UploadedFile.objects.get(pk=uid)

            # Check whether the file exists already in the folder. If so, rename it.
            # TODO: This is a race condition.
            new_name = upload.name
            old_path = os.path.join(upload_dir, upload.path)
            new_path = os.path.join(documents_dir, new_name)
            new_path = find_unique_name(new_path)
            new_name = os.path.basename(new_path)

            # Move the temporary file to the permanent upload directory.
            shutil.move(old_path, new_path)

            # Update the document with the uploaded file information.
            doc['file_type'] = upload.type
            doc['file_name'] = new_name

            # Delete the temporary uploaded file.
            upload.delete()

        # Set the parent instance on the document.
        doc[field_name] = inst
        fields = [f.name for f in cls._meta.get_fields()]
        props = {k: doc[k] for k in fields if k in doc.keys()}
        try:
            # Retrieve the instance.
            apd = cls.objects.get(pk=props['id'])

            # Remove the original file associated with the instance, if the name has changed.
            try:
                if os.path.join(documents_dir, apd.file_name) != os.path.join(documents_dir, props['file_name']):
                    os.unlink(os.path.join(documents_dir, apd.file_name))
            except Exception: pass

            # Set the instance new attributes and save.
            for k in ('title', 'note', 'url', 'file_type', 'file_name'):
                setattr(apd, k, strnone(doc.get(k)))
            apd.save()
        except:
            # Create the document from scratch if it does not exist.
            cls.objects.create(**props)
