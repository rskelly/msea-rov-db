import re
import csv

from django.http import JsonResponse
from django.db import connection
from django.conf import settings
from rest_framework.decorators import action
from rest_framework import viewsets
from rest_framework.response import Response

from msea.util import util
from msea.views.rov.util import get_bounds_query
from msea.views.auth import ReSTUserViewSet, ReSTOpenViewSet


def _str(v):
    """
    Strip the string and return None if it's empty.
    """
    try:
        v = str(v).strip()
        return None if not v else v
    except:
        return None
    

class ObservationSearchViewSet(ReSTOpenViewSet):
    """
    Used to search the observation table, which is a schemaless table containing annotated properties.
    """

    @action(detail=False, methods=['get'])
    def search(self, request):
        """
        """
        try:
            term = request.GET.get('term')
            category = request.GET.get('category') # One of habitat, species, anthropogenic or empty.

            # Get the user ID if it's available (id should be).
            try:
                user_id = request.user.id
            except:
                user_id = 0

            with connection.cursor() as cur:
                sql = cur.mogrify("""
                            select * 
                            from get_restricted_events(%s)
                            where exists (
                                select key, value from jsonb_each_text(properties) where value ilike %s 
                            )
                            limit 100
                            """, [user_id, f'%{term}%'])
                cur.execute(sql)
                cols = [x[0] for x in cur.description]
                result = []
                for row in cur:
                    result.append(dict(zip(cols, row)))
                return JsonResponse({'result': result})
        except Exception as e:
            return JsonResponse({'error': str(e)})


allowed_fields = ['common_name', 'scientific_name', 'otu', 'cf', 'tags']

class TextSearchViewSet(ReSTUserViewSet):

    @action(detail=False, methods=['post'])
    def search(self, request):
        """
        Search the observations using a string search term. 
        
        The search term is split on 'or' into individual terms.
        The cruise is the name and leg of the cruise concatenated with a hyphen ('-').
        """

        try:

             # Get the user ID if it's available (id should be).
            try:
                user_id = request.user.id
            except:
                user_id = 0

            # Search terms currently hit the scientific name column.
            terms = list(filter(bool, map(_str, request.data.get('terms', '').split(','))))
            # Fields to search for keywords.
            fields = request.data.get('fields', [])
            # Life stage.
            life_stages = list(filter(bool, map(_str, request.data.get('life_stages', '').split(','))))
            # Cruise ID if available.
            cruise_id = request.data.get('cruise_id')
            # Dive ID if available.
            dive_id = request.data.get('dive_id')
            # If true, download as a file.
            download = bool(request.data.get('download', False)) 
            # The bounds filter.
            bounds = request.data.get('bounds')

            if download:
                page = 0
                offset = 0
                limit = 999999999
            else:
                # The current page-1; default to 0.
                limit = 25
                try:
                    page = request.data.get('page', 0)
                    offset = int(page) * limit
                except:
                    offset = 0

            if not (terms or cruise_id or life_stages):
                raise Exception('At least one search term or life stage or cruise must be provided.')
            
            # Accumulate term parameters and queries.
            term_q = [f"(e.properties->>'{f}' ilike %s)" for f in fields if f in allowed_fields]
            term_p = [f'%{t}%' for t in terms] * len(term_q)
            term_q = ' or '.join(term_q * len(terms)) if terms else '(1=1)'

            bounds_q = "ep.geoms::geometry && st_geomfromtext('POLYGON(({x1} {y1}, {x2} {y1}, {x2} {y2}, {x1} {y2}, {x1} {y1}))', 4326)".format(**bounds) if bounds else '(1=1)'

            # The life stages query.
            if '__all__' in life_stages:
                # If all is in the list, match all.
                life_stage_q = '(1=1)'
                life_stage_p = []
            elif '__none__' in life_stages:
                # If none is in the list, life_stage has to be null.
                life_stage_q = "(e.properties->>'life_stage' is null)"
                life_stage_p = []
            else:
                # Otherwise, match selected, unless none given, in which case match all.
                life_stage_q = "(e.properties->>'life_stage'=any(%s))" if life_stages else '(1=1)'
                life_stage_p = [life_stages] if life_stages else []

            # Assemble the cruises query.
            cruise_p = []
            cruise_q = '(1=1)'
            if cruise_id:
                cruise_p.append(int(cruise_id))
                cruise_q = '(d.cruise_id=%s)'

            # Assemble the dives query.
            dive_p = []
            dive_q = '(1=1)'
            if dive_id:
                dive_p.append(int(dive_id))
                dive_q = '(d.id=%s)'

            sql = f'''
                select 
                    count(*) over() as total_count,
                    row_number() over() as row_num,
                    e.id as event_id,
                    c.name as cruise_name, c.leg as cruise_leg, d.name as dive_name,
                    e.properties->>'scientific_name' as scientific_name,
                    e.properties->>'common_name' as common_name,
                    to_char(e.start_time, 'YYYY-MM-DD"T"HH24:MI:SS"Z"') as timestamp,
                    st_x(st_geometryn(ep.geoms, 1)) as lon, st_y(st_geometryn(ep.geoms, 1)) as lat, 
                    ed.depths[1] as depth
                from get_restricted_events(%s) e
                    inner join rov.dive d on d.id=e.dive_id
                    inner join rov.cruise c on c.id=d.cruise_id
                    left join cache.evt_pos ep on ep.event_id=e.id
                    left join cache.evt_depth ed on ed.event_id=e.id
                where ({term_q})
                    and {cruise_q} and {dive_q}
                    and {life_stage_q}
                    and {bounds_q}
                    and properties->>'scientific_name' is not null
                order by e.start_time
                limit %s offset %s
            '''            
            with connection.cursor() as cur:
                cur.execute(sql, [user_id] + term_p + cruise_p + dive_p + life_stage_p + [limit, offset])

                # Get the column names and fetch the rows.
                cols = [desc[0] for desc in cur.description]
                total_count_idx = cols.index('total_count')
                cols = cols[:total_count_idx] + cols[total_count_idx+1:]

                if download:

                    # Open a text file and write the entire rowset to it.
                    tf = util.maketempfile(suffix='.csv')
                    with open(tf.path, 'w', newline='') as f:
                        db = csv.writer(f)
                        db.writerow(cols)
                        for row in cur:
                            db.writerow(row[:total_count_idx] + row[total_count_idx+1:])

                    dl_dir = settings.MSEA_DOWNLOAD_DIR
                    tf.rename_unique()
                    tf.move_to(dl_dir)
                    return JsonResponse({'download': {'attachment_filename': 'search_results.csv', 'filename': tf.name}})

                else:
                    rows = cur.fetchall()

                    # Format and send the result.
                    result = {
                        'page': page,
                        'terms': ','.join(terms),
                        'pageSize': limit,
                        'totalCount': rows[0][total_count_idx] if rows else 0,
                        'results': [dict(zip(cols, row[:total_count_idx] + row[total_count_idx+1:])) for row in rows],
                    }
                
                    return JsonResponse({'result': result})
                
            raise Exception('No query given.')
        except Exception as e:
            return JsonResponse({'error': 'Error in observation search: ' + str(e)})
        

