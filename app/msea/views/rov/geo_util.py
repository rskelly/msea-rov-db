"""
Utilities for processing geospatial data.
"""
import os
from datetime import datetime
from osgeo import ogr, osr

from django.conf import settings

from msea.util import util

def _str(s):
    """
    Return a stringified version of the value, or none on failure.
    """
    try: return str(s)
    except: return None

def _int(s):
    """
    Return the value cast to integer, or none on failure.
    """
    try: return int(s)
    except: return None
    
def _dt(s):
    """
    Return the value parsed as an ISO1806 timestamp, or none on failure.
    """
    try: return datetime.strftime(s, '%Y-%m-%dT%H:%M:%SZ')
    except: return None

def _trim(name, format):
    """
    Return the value trimmed to a length appropriate to the geospatial file format.
    This only applies to shapefiles, but is used for constistency.
    """
    if format == 'ESRI Shapefile' and len(name) > 10:
        return name[:10]
    else:
        return name

def download_dt(cruise, format, type, connection):
    """
    Download a shapefile or sqlite file containing the linework for either the dives
    or transects of the given cruise.
    :param cruise: The cruise in the form, [cruise name]-[cruise leg].
    :param format: The file format, either "shapefile" or "spatialite".
    :param type: If dives are desired, "dives" else "transects".
    :param connection: The database connection to operate on.
    :return: A JsonResponse for the sqlite file or shapefile archive.
    """

    # Choose the format.
    if format == 'shapefile':
        format = 'ESRI Shapefile'
        ext = 'shp'
    elif format == 'spatialite':
        format = 'SQLite'
        ext = 'sqlite'
    else:
        raise Exception(f'Invalid file format: {format}.')

    # The select statements and column definitions for each type.
    layers = {
        'transects': (
            """
                select 
                    dt.cruise_name, dt.dive_name, dt.transect_name, dt.cruise_id, dt.dive_id, dt.transect_id,
                    t.start_time as start_time, t.end_time as end_time,
                    split_part(st_asewkt(geom), ';', 2) as geom
                from cache.transect_track dt
                    left join rov.transect t on t.id=dt.transect_id
                where dt.cruise_name=%s or %s='all'
                order by dt.cruise_name, dt.dive_name, dt.transect_name
            """,
            (
                ('cruise_name', ogr.OFTString, _str),
                ('dive_name', ogr.OFTString, _str),
                ('transect_name', ogr.OFTString, _str),
                ('cruise_id', ogr.OFTInteger, _int),
                ('dive_id', ogr.OFTInteger, _int),
                ('transect_id', ogr.OFTInteger, _int),
                ('start_time', ogr.OFTString, _dt),
                ('end_time', ogr.OFTString, _dt),
            )
        ),
        'dives': ( 
            """
                select 
                    dt.cruise_name, dt.dive_name, dt.cruise_id, dt.dive_id, 
                    d.start_time, d.end_time as end_time,
                    split_part(st_asewkt(geom), ';', 2) as geom
                from cache.dive_track dt
                    inner join rov.dive d on d.id=dt.dive_id
                where dt.cruise_name=%s or %s='all'
                order by dt.cruise_name, dt.dive_name
            """,
            (
                ('cruise_name', ogr.OFTString, _str),
                ('dive_name', ogr.OFTString, _str),
                ('cruise_id', ogr.OFTInteger, _int),
                ('dive_id', ogr.OFTInteger, _int),
                ('start_time', ogr.OFTString, _dt),
                ('end_time', ogr.OFTString, _dt),
            )
        )
    }

    # Prepare the filename.
    filename = f'{type}_{cruise}.{ext}'

    # Temporary file.
    tf = util.maketempfile(suffix=f'.{ext}', create=False)

    # Create the output datasett.
    drv = ogr.GetDriverByName(format)
    ds = drv.CreateDataSource(tf.path)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(4326)

    # Extract the sql and column definitions.
    sql, col_defs = layers[type]

    # Create the layer.
    lyr = ds.CreateLayer(type, srs, ogr.wkbLineString)
        
    # Create fields on the layer.
    for name, tp, _ in col_defs:
        lyr.CreateField(ogr.FieldDefn(_trim(name, format), tp))

    # Extract data to the layer.
    with connection.cursor() as cur:
        cur.execute(sql, [cruise, cruise])
        cols = [desc[0] for desc in cur.description]
        for row in cur:
            row = dict(zip(cols, row))
            geom = ogr.CreateGeometryFromWkt(row['geom'])
            feat = ogr.Feature(lyr.GetLayerDefn())
            feat.SetGeometry(geom)
            for name, tp, cast in col_defs:
                feat.SetField(_trim(name, format), cast(row[name]))
            lyr.CreateFeature(feat)
            feat = None

    lyr = None
    ds = None

    if format == 'ESRI Shapefile':
        # Zip the shapefile files, and use a zip file.
        tfz = util.maketempfile(suffix='.zip', create=False)
        util.create_zip(tfz.path, tf.dir)
        tf = tfz
        filename = os.path.splitext(filename)[0] + '.zip'

    # Rename and move the temporary file to dl_dir to make it available for download.
    dl_dir = settings.MSEA_DOWNLOAD_DIR
    tf.rename_unique()
    tf.move_to(dl_dir)

    return (filename, tf.name)
