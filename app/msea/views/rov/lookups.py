
from rest_framework import serializers

from msea.views.auth import ReSTUserViewSet
from msea.models.rov.lookups import Abundance, Biocover, Complexity, Substrate, Relief, ImageQuality, \
    Disturbance, Coverage, Flow, Protocol, SurveyMode, Thickness, ObservationConfidence

class AbundanceSerializer(serializers.ModelSerializer):
    """
    Serializer for Abundance instances.
    """
    class Meta:
        model = Abundance
        fields = '__all__'


class AbundanceViewSet(ReSTUserViewSet):
    """
    Endpoint for Abundance instances.
    """

    serializer_class = AbundanceSerializer
    model_class = Abundance


class BiocoverSerializer(serializers.ModelSerializer):
    """
    Serializer for Biocover instances.
    """

    class Meta:
        model = Biocover
        fields = '__all__'


class BiocoverViewSet(ReSTUserViewSet):
    """
    Endpoint for Biocover instances.
    """

    serializer_class = BiocoverSerializer
    model_class = Biocover


class ComplexitySerializer(serializers.ModelSerializer):
    """
    Serializer for Complexity instances.
    """

    class Meta:
        model = Complexity
        fields = '__all__'


class ComplexityViewSet(ReSTUserViewSet):
    """
    Endpoint for Complexity instances.
    """

    serializer_class = ComplexitySerializer
    model_class = Complexity


class FlowSerializer(serializers.ModelSerializer):
    """
    Serializer for Flow instances.
    """

    class Meta:
        model = Flow
        fields = '__all__'


class FlowViewSet(ReSTUserViewSet):
    """
    Endpoint for Flow instances.
    """

    serializer_class = FlowSerializer
    model_class = Flow


class CoverageSerializer(serializers.ModelSerializer):
    """
    Serializer for Coverage instances.
    """

    class Meta:
        model = Coverage
        fields = '__all__'


class CoverageViewSet(ReSTUserViewSet):
    """
    Endpoint for Coverage instances.
    """

    serializer_class = CoverageSerializer
    model_class = Coverage


class DisturbanceSerializer(serializers.ModelSerializer):
    """
    Serializer for Disturbance instances.
    """

    class Meta:
        model = Disturbance
        fields = '__all__'


class DisturbanceViewSet(ReSTUserViewSet):
    """
    Endpoint for Disturbance instances.
    """

    serializer_class = DisturbanceSerializer
    model_class = Disturbance


class ImageQualitySerializer(serializers.ModelSerializer):
    """
    Serializer for ImageQuality instances.
    """

    class Meta:
        model = ImageQuality
        fields = '__all__'


class ImageQualityViewSet(ReSTUserViewSet):
    """
    Endpoint for ImageQuality instances.
    """

    serializer_class = ImageQualitySerializer
    model_class = ImageQuality


class ProtocolSerializer(serializers.ModelSerializer):
    """
    Serializer for Protocol instances.
    """

    class Meta:
        model = Protocol
        fields = '__all__'


class ProtocolViewSet(ReSTUserViewSet):
    """
    Endpoint for Protocol instances.
    """

    serializer_class = ProtocolSerializer
    model_class = Protocol


class ReliefSerializer(serializers.ModelSerializer):
    """
    Serializer for Relief instances.
    """

    class Meta:
        model = Relief
        fields = '__all__'


class ReliefViewSet(ReSTUserViewSet):
    """
    Endpoint for Relief instances.
    """

    serializer_class = ReliefSerializer
    model_class = Relief


class SubstrateSerializer(serializers.ModelSerializer):
    """
    Serializer for Substrate instances.
    """

    class Meta:
        model = Substrate
        fields = '__all__'


class SubstrateViewSet(ReSTUserViewSet):
    """
    Endpoint for Substrate instances.
    """
    serializer_class = SubstrateSerializer
    model_class = Substrate


class SurveyModeSerializer(serializers.ModelSerializer):
    """
    Serializer for SurveyMode instances.
    """

    class Meta:
        model = SurveyMode
        fields = '__all__'


class SurveyModeViewSet(ReSTUserViewSet):
    """
    Endpoint for SurveyMode instances.
    """
    serializer_class = SurveyModeSerializer
    model_class = SurveyMode


class ThicknessSerializer(serializers.ModelSerializer):
    """
    Serializer for Thickness instances.
    """

    class Meta:
        model = Thickness
        fields = '__all__'


class ThicknessViewSet(ReSTUserViewSet):
    """
    Endpoint for Thickness instances.
    """

    serializer_class = ThicknessSerializer
    model_class = Thickness


class ObservationConfidenceSerializer(serializers.ModelSerializer):
    """
    Serializer for ObservationConfidence instances.
    """

    class Meta:
        model = ObservationConfidence
        fields = '__all__'


class ObservationConfidenceViewSet(ReSTUserViewSet):
    """
    Endpoint for ObservationConfidence instances.
    """

    serializer_class = ObservationConfidenceSerializer
    model_class = ObservationConfidence


