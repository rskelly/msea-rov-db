import csv

from django.db import connection
from django.conf import settings
from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.decorators import action

from msea.util import util

def _str(v):
    """
    Strip the string and return None if it's empty.
    """
    try:
        v = str(v).strip()
        return None if not v else v
    except:
        return None
    
class ObservationMeasurementViewSet(viewsets.ViewSet):
    """
    Return a list of measurements associated with each species.
    Tags and scientific names can be searched. Results are filtered using the 
    measurement type ID.
    """
    
    @action(methods=['get'], detail=False)
    def life_stages(self, request):
        """
        Return a list of unique values from the life_stage property.
        """
        try:
            with connection.cursor() as cur:
                cur.execute("""
                            select distinct properties->>'life_stage'
                            from rov.event 
                            where properties->>'life_stage' is not null
                            order by properties->>'life_stage'
                        """)
                return JsonResponse({'result': [r[0] for r in list(cur)]})
        except Exception as e:
            return JsonResponse({'error': str(e)})


    def list(self, request):
        """
        TODO: Move this search functionality to a utility function.
        """
        try:

             # Get the user ID if it's available (id should be).
            try:
                user_id = request.user.id
            except:
                user_id = 0

            # Search terms currently hit the scientific name column.
            terms = list(filter(bool, map(_str, request.GET.get('terms', '').split(','))))
            # Fields to search for keywords.
            fields = list(filter(bool, map(_str, request.GET.get('fields', '').split(','))))
            # Life stage.
            life_stages = list(filter(bool, map(_str, request.GET.get('life_stages', '').split(','))))
            # Cruise ID if available.
            cruise_id = request.GET.get('cruise_id')
            # Dive ID if available.
            dive_id = request.GET.get('dive_id')

            if not (terms or cruise_id or life_stages):
                raise Exception('At least one search term or life stage or cruise must be provided.')
            
            # A measurement type is required to filter measurements.
            measurement_type_id = request.GET.get('measurement_type_id', 0)
            if not measurement_type_id:
                raise Exception('A measurement type is required.')

            # Accumulate term parameters and queries.
            allowed_fields = ['common_name', 'scientific_name', 'otu', 'cf', 'tags']
            term_q = [f"(e.properties->>'{f}' ilike %s)" for f in fields if f in allowed_fields]
            
            # A list of paramters of length terms * fields.
            term_p = [f'%{t}%' for t in terms] * len(term_q)

            # Join the paramterers in 'or' configuration, or use a tautology if there are no params.
            term_q = ' or '.join(term_q * len(terms)) if terms else '(1=1)'

            # The life stages query.
            if '__all__' in life_stages:
                # If all is in the list, match all.
                life_stage_q = '(1=1)'
                life_stage_p = []
            elif '__none__' in life_stages:
                # If none is in the list, life_stage has to be null.
                life_stage_q = "(e.properties->>'life_stage' is null)"
                life_stage_p = []
            else:
                # Otherwise, match selected, unless none given, in which case match all.
                life_stage_q = "(e.properties->>'life_stage'=any(%s))" if life_stages else '(1=1)'
                life_stage_p = [life_stages] if life_stages else []

            # Assemble the cruises query.
            cruise_p = []
            cruise_q = '(1=1)'
            if cruise_id:
                cruise_p.append(int(cruise_id))
                cruise_q = '(d.cruise_id=%s)'

            # Assemble the dives query.
            dive_p = []
            dive_q = '(1=1)'
            if dive_id:
                dive_p.append(int(dive_id))
                dive_q = '(d.id=%s)'

            sql = f'''
                select c.name, c.leg, 
                    e.properties->>'scientific_name' as scientific_name,
                    array_agg(m.measurements[1]) as measurements
                from get_restricted_events(%s) e
                    inner join rov.dive d on d.id=e.dive_id
                    inner join rov.cruise c on c.id=d.cruise_id
                    inner join cache.evt_measurement m on m.event_id=e.id
                where ({term_q})
                    and m.measurement_type_id=%s
                    and {cruise_q} and {dive_q}
                    and {life_stage_q}
                    and properties->>'scientific_name' is not null
                    and c.approved >= 2
                group by c.name, c.leg, e.properties->>'scientific_name';
            '''            
            with connection.cursor() as cur:
                cur.execute(sql, [user_id] + term_p + [int(measurement_type_id)] + cruise_p + dive_p + life_stage_p)
                # Counts accumulates the counts for cruises.
                counts = {}
                # Result dictionary.
                data = {'data': {}, 'cruises': []}
                for cruise_name, cruise_leg, scientific_name, measurements in cur:
                    # If there's no array for the taxon, create it.
                    if data['data'].get(scientific_name) == None:
                        data['data'][scientific_name] = []
                    # Add measurements for the taxon.
                    data['data'][scientific_name].extend(measurements)
                    # If there's no slot for the cruise name, create it.
                    cname = f'{cruise_name}-{cruise_leg}'
                    if counts.get(cname) == None:
                        counts[cname] = 0
                    # Add the measurement count to the cruise.
                    counts[cname] += len(measurements)

                # Add the cruise counts to the data object.
                for k, v in counts.items():
                    data['cruises'].append(f'{k} ({v})')
                
            return JsonResponse({'result': data})
        except Exception as e:
            return JsonResponse({'error': str(e)})


class TaxonSearchViewSet(viewsets.ViewSet):
    """
    Provides methods for searching the taxonomy table.
    """

    @action(detail=False, methods=['post'])
    def search(self, request):
        """
        Taxonomic search is a search that can travel up the taxonomic hierarchy, then
        include all descendant taxa beginning with that ancestor. A search for observations
        can then be performed using all the IDs in the subtree.

        This should make it possible for a researcher to locate all observations concerning
        members of the subtree, so if there are uncertain identifications (i.e., using cf. or spp., etc),
        they can still be matched by searching on an ancestor.

        There are two parameters. The first is taxon_id, which is an Aphia ID. The second is direction, 
        which has two possible values, "up" and "down". The "up" mode searches for the immediate parent 
        of the given Aphia ID. The "down" mode returns the Aphia IDs of all descendant taxa of the 
        given Aphia ID.
        """
        try:
            direction = request.data['direction']
            taxon_id = int(request.data['taxon_id'])
            cruise_id = int(request.data.get('cruise_id', 0))
            limit = int(request.data.get('limit', 100))
            offset = int(request.data.get('offset', 0))
            download = bool(request.data.get('download', False))
            result = {'direction': direction, 'taxon_id': taxon_id, 'limit': limit, 'offset': offset}

            # Get the user ID if it's available (id should be).
            try:
                user_id = request.user.id
            except:
                user_id = 0

            if direction == 'up':
                # Get the immediate parent of the taxon. 
                with connection.cursor() as cur:
                    try:
                        cur.execute("select parent_taxon_id from taxonomy.taxon where source='aphia' and taxon_id=%s", [str(taxon_id)])
                        parent_taxon_id, = cur.fetchone()
                        cur.execute("select taxon_id, scientific_name, common_name from taxonomy.taxon where source='aphia'and  taxon_id=%s", [str(parent_taxon_id)])
                        taxon_id, scientific_name, common_name = cur.fetchone()
                    except Exception as e:
                        raise Exception('The parent was not found. The taxon may be out of scope (e.g., terrestrial).')
                    result['parent'] = {'scientific_name': scientific_name, 'common_name': common_name, 'taxon_id': int(taxon_id)}
                    return JsonResponse({'result': result})
            elif direction == 'down':
                # Recursively locate the children of the given taxon and return observations, taxa, position
                # and depth.
                with connection.cursor() as cur:
                    # Get the search taxon names.
                    cur.execute("select scientific_name, common_name from taxonomy.taxon where taxon_id=%s and source='aphia'", [str(taxon_id)])
                    scientific_name, common_name = cur.fetchone()
                    # Search the children.
                    sql = """
                        with recursive t as (
                            select taxon_id, rank from taxonomy.taxon where taxon_id=%s and source='aphia'
                            union all
                            select a.taxon_id, a.rank
                            from taxonomy.taxon a inner join t on a.parent_taxon_id=t.taxon_id::integer
                        )
                        select row_number() over () as row_num, 
                                t.taxon_id, 
                                e.id as event_id,
                                e.properties->>'scientific_name' as scientific_name, 
                                e.properties->>'common_name' as common_name, 
                                t.rank,
                                c.name as cruise_name, c.leg as cruise_leg,
                                d.name as dive_name, 
                                e.start_time as timestamp, 
                                st_y(st_geometryn(p.geoms::geometry, 1)) as lat, 
                                st_x(st_geometryn(p.geoms::geometry, 1)) as lon, 
                                h.depths[1] as depth
                        from t 
                            inner join get_restricted_events(%s) e on e.properties->>'aphia_id'=t.taxon_id
                            inner join rov.dive d on d.id=e.dive_id
                            inner join rov.cruise c on c.id=d.cruise_id
                            inner join cache.evt_pos p on p.event_id=e.id
                            left join cache.evt_depth h on h.event_id=e.id
                        -- The cruise ID is zero (all cruises) or positive (just that cruise)                                
                        where %s=0 or c.id=%s
                        order by row_num, t.taxon_id
                    """
                    params = [str(taxon_id), user_id, cruise_id, cruise_id]
                    if not download:
                        sql = sql + "limit %s offset %s"
                        params += [limit, offset]

                    cur.execute(sql, params)

                    cols = [x[0] for x in cur.description]

                    if not download:
                        rows = [dict(zip(cols, row)) for row in cur]

                        result = {'items': rows, 'limit': limit, 'offset': offset, 'scientific_name': scientific_name, 'common_name': common_name}
                    
                        # Return the result, whatever it is.
                        return JsonResponse({'result': result})
                    else:
                        tf = util.maketempfile(suffix=".csv", create=True)
                        with open(tf.path, 'w', newline='') as f:
                            db = csv.writer(f)
                            db.writerow(cols)
                            for row in cur:
                                db.writerow(row)
                            dl_dir = settings.MSEA_DOWNLOAD_DIR
                            tf.rename_unique()
                            tf.move_to(dl_dir)
                            return JsonResponse({'download': {'attachment_filename': f'taxonomic_search_{taxon_id}.csv', 'filename': tf.name}})
            else:
                raise Exception(f'Unexpected parameter: {direction}.')                
        except Exception as e:
            return JsonResponse({'error': str(e)})
      

    def list(self, request):
        """
        Searches the taxonomy table for near matches to the search term. Returns a list sorted in 
        order of similarity.

        Returns an ID field, a source field (the source DB), a type (which field was matched),
        and the matched name or a formatted version of it.
        """
        
        term = request.GET.get('term', '').lower()
        if not term:
            return JsonResponse({'result': {'items': [], 'term': term}})

        res = []
        with connection.cursor() as cur:
            cur.execute("""
                with t as (
                    select taxon_id, scientific_name, common_name, rank, source,
                            accepted_taxon_id, parent_taxon_id,
                            similarity(%s, scientific_name) as score,
                            'Scientific Name' as field
                    from taxonomy.taxon where %s %% scientific_name
                    union
                    select taxon_id, scientific_name, common_name, rank, source,
                            accepted_taxon_id, parent_taxon_id,
                            similarity(%s, scientific_name) as score,
                            'Common Name' as field
                    from taxonomy.taxon where %s %% common_name
                ), u as (
                    select distinct on(taxon_id, source) * 
                    from t 
                    where source='aphia'
                    order by taxon_id
                )
                select * from u order by score desc limit 30
            """, [term] * 4)

            fields = [desc[0] for desc in cur.description]

            for row in cur.fetchall():
                res.append(dict(zip(fields, row)))

        return JsonResponse({'result': {'items': res, 'term': term}})
