"""
Contains view and serializer classes related to user and group management and authentication.
"""
import logging
from datetime import datetime, timezone

from django.conf import settings
from django.db import connection, transaction
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import UserPassesTestMixin
from rest_framework.permissions import BasePermission
from rest_framework import serializers, permissions, status
from rest_framework.decorators import action
from drf_writable_nested import WritableNestedModelSerializer, UniqueFieldsMixin

from knox.models import AuthToken
from knox.auth import TokenAuthentication
from knox.views import LoginView as KnoxLoginView
from knox.views import LogoutView as KnoxLogoutView

from msea.models.auth import User #, UserGroup
from msea.views.util import ReSTViewSet
from msea.util.util import get_ip_address, process_email_template, send_email


def get_data(request, name):
    """
    Return the named data from the POST data or fallback to the GET data without
    checking the request type (because we're lazy).
    """
    try:
        return request.POST.get(name, request.GET.get(name, request.data.get(name, ''))).strip()
    except:
        return ''


def get_user(request):
    """
    Get the User using a token contained in the request.
    """
    obj = TokenAuthentication().authenticate(request)
    if obj:
        return obj[0]
    else:
        return None


class AdminMixin(UserPassesTestMixin):
    """
    Restricts view permissions to users in the msea_admin group.
    """

    login_url = '/login'
    redirect_url = '/'

    def test_func(self):
        try:
            return self.request.user.groups.filter(name='msea_admin').exists()
        except Exception as e:
            logging.warning(str(e))
        return False

    def get_permission_denied_message(self):
        return 'You must have MSEA administrator permissions to access this resource.'


class ReSTAdminPermissions(BasePermission):
    """
    Restricts API access permissions to users in the msea_admin group.
    """

    message = 'You must have administrator permissions to access this resource.'

    def has_permission(self, request, view):
        try:
            return request.user.groups.filter(name='msea_admin').exists()
        except Exception as e:
            logging.warning(str(e))
        return False


class ReSTAuthPermissions(BasePermission):
    """
    Restricts API access permissions to logged in users.
    """

    message = 'You must be logged in to access this resource.'

    def has_permission(self, request, view):
        try:
            return request.user.is_authenticated()
        except Exception as e:
            logging.warning(str(e))
        return False


class ReSTUserViewSet(ReSTViewSet):
    """
    A ReSTViewSet subclass that allows open retrieval but modifications
    require admin group membership.
    """
    permission_classes = [permissions.IsAuthenticated]


class ReSTAdminViewSet(ReSTViewSet):
    """
    A ReSTViewSet subclass that allows retrieval to authenticated users,
    but modifications require admin group membership.
    """
    permission_classes = [ReSTAdminPermissions]


class ReSTOpenViewSet(ReSTViewSet):
    """
    Provides a view set that does not care about permissions for updates, creations.
    """
    permission_classes = [permissions.AllowAny] # See: https://github.com/encode/django-rest-framework/issues/2383


class GroupSerializer(UniqueFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ('id', 'name',)


class UserSerializer(WritableNestedModelSerializer):
    groups = GroupSerializer(many=True)

    class Meta:
        model = User
        fields = [
            'id',
            'email', 'first_name', 'last_name',
            'is_staff', 'is_active', 'is_superuser',
            'date_joined', 'last_login',
            'organization', 'org_type',
            'bio', 'biigle_username', 'biigle_api_key',
            'allowed', 'groups',
        ]


class UserAdminSerializer(WritableNestedModelSerializer):
    """
    Contains extra fields related to account administration.
    """
    groups = GroupSerializer(many=True)

    class Meta:
        model = User
        fields = '__all__'


class LogoutView(KnoxLogoutView):
    """
    Provides a view for logging the user out, using the Knox token auth infrastructure,
    and Django user authentication.
    """
    permission_classes = (permissions.AllowAny,)
      
    def post(self, request, format=None):
        try:
            request.user.auth_token.delete()
        except: pass
        request.authenticators = []
        return HttpResponse(status.HTTP_204_NO_CONTENT)
    

class LoginView(KnoxLoginView):
    """
    Provides a view for logging the user in, using the Knox token auth infrastructure,
    and Django user authentication.
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        try:
            email = request.data.get('email')
            password = request.data.get('password')
            # Sanity check the parameters.
            if not email:
                return JsonResponse({'result': {'loggedIn': False, 'status': User.EMAIL_EMPTY}})
            if not password:
                return JsonResponse({'result': {'loggedIn': False, 'status': User.PASSWORD_EMPTY}})

            # It is not enough to use iexact as the framework authentication methods
            # are not case-insensitive.
            email = email.lower()

            user = User.objects.get(email=email)
            if user.is_verification_pending():
                return JsonResponse({'result': {'loggedIn': False, 'status': User.VERIFICATION_PENDING}})
            if user.is_verification_expired():
                return JsonResponse({'result': {'loggedIn': False, 'status': User.VERIFICATION_EXPIRED}})

            # Authenticate the user.
            user = authenticate(username=email, password=password)
            if not user:
                return JsonResponse({'result': {'loggedIn': False, 'status': User.LOGIN_FAILED}})

            # Taken from knox.LoginView: check the token limit.
            token_limit_per_user = super().get_token_limit_per_user()
            if token_limit_per_user is not None:
                now = datetime.now(tz=timezone.utc)
                token = user.auth_token_set.filter(expiry__gt=now)
                if token.count() >= token_limit_per_user:
                    return JsonResponse({'result': {'loggedIn': False, 'status': User.MAX_TOKENS_EXCEEDED}})
                
            token_ttl = super().get_token_ttl()

            # Get the token.
            instance, token = AuthToken.objects.create(user, token_ttl)

            # Set the user on the request.
            request.user = user

            # Returns a dict with token, expiry, user properties.
            data = super().get_post_response_data(request, token, instance)

            # Replace the default user property with profile.
            data['profile'] = data['user']
            del data['user']
            
            return JsonResponse({'result': {'loggedIn': True, 'status': User.LOGGED_IN, 
                                            'profile': data['profile'], 'expiry': data['expiry'], 'token': data['token']}})
        except Exception as e:
            return JsonResponse({'result': {'loggedIn': False, 'status': User.LOGIN_FAILED}})


class GroupViewSet(ReSTOpenViewSet):
    """
    Retrieve lists of groups.
    """
    model_class = Group
    serializer_class = GroupSerializer
    
    def list(self, request):
        name = request.GET.get('name')
        if name:
            return JsonResponse({'result': GroupSerializer(self.model_class.objects.filter(name=name), many=True).data})
        else:
            return super().list(request)


class UserViewSet(ReSTAdminViewSet):
    """
    Actions pertaining to users. The standard viewset actions are admin-only,
    while the others are open to all users for registration, auth, etc.
    """
    model_class = User
    serializer_class = UserSerializer

    # Used to restrict the fields that can be updated by the user.
    update_fields = [            
        'email', 'first_name', 'last_name',
        'organization', 'org_type',
        'bio', 'biigle_username', 'biigle_api_key',
        'allowed', 'groups'
    ]

    # Used to provide fields that can be updated by an admin.
    admin_update_fields = [
        'email', 'first_name', 'last_name',
        'is_staff', 'is_active', 'is_superuser',
        'date_joined', 'last_login', 'organization', 'org_type',
        'allowed', 'bio', 'biigle_username', 'biigle_api_key',
        'registration_ip', 'registration_location', 'ip_in_region',
        'verification_code', 'verification_expiry', 'verification_time',
        'password_reset_code', 'password_reset_expiry',
        'groups'
    ]

    def list(self, request):
        try:
            category = request.GET.get('category')
            status = request.GET.get('status')
            
            q = self.model_class.objects.all()
            
            if category:
                # Filter by category.
                q = q.filter(org_type__iexact=category.strip())

            if status == 'new':
                # Sort by signup date descending.
                q = q.order_by('-date_joined')
            
            return JsonResponse({'result': UserAdminSerializer(q, many=True).data})
        except Exception as e:
            return JsonResponse({'error': 'Failed to load users: ' + str(e)})

    def retrieve(self, request, pk):
        auth_user = get_user(request)
        if auth_user.is_admin:
            user = User.objects.get(pk=pk)
            return JsonResponse({'result': UserAdminSerializer(user).data})
        else:
            return super().retrieve(request, pk)

    @action(methods=['get'], detail=False, url_path='session', permission_classes=[permissions.AllowAny])
    def check_session(self, request):
        """
        Return True if there's a logged-in User.
        :param request: The Django request object.
        :return: True if logged in, False otherwise.
        """
        try:
            user = get_user(request)
            if user:
                return JsonResponse({'result': {'loggedIn': True, 'status': User.LOGGED_IN, 'profile': UserSerializer(user).data}})
            else:
                return JsonResponse({'result': {'loggedIn': False, 'status': User.NOT_LOGGED_IN, 'profile': None}})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    @action(methods=['put'], detail=False, url_path='profile', permission_classes=[permissions.IsAuthenticated])
    def update_profile(self, request):
        try:
            data = request.data
            user = get_user(request)
            if int(user.id) != int(data.get('id', 0)):
                raise Exception(f'The submitted user is not the logged in user.')
            for key in self.update_fields:
                try:
                    setattr(user, key, data[key])
                except Exception as e:
                    pass

            groups = [Group.objects.get(name=g['name']) for g in data['groups']]
            user.groups.set(groups)
            user.save()

            return JsonResponse({'result': UserSerializer(user).data})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def get_serializer_class(self):
        """
        Select the serializer based on the user's admin status. Admins
        use the admin serializer which serializes all fields. Other users
        user a serializer with a limited field set.
        """
        admin_user = get_user(self.request)
        if admin_user.is_admin():
            return UserAdminSerializer
        else:
            return UserSerializer
        
    @action(methods=['get', 'post'], detail=False, url_path="password_reset", permission_classes=[permissions.AllowAny])
    def password_reset(self, request):
        try:
            action = get_data(request, 'action')
            
            if action == 'send':
                # This action sends the code to the user's email.

                # Get the email and lowercase it.
                email = get_data(request, 'email')
                if email:
                    email = email.lower()

                # Check the email's validity.
                valid, status = User.validate_email(email)
                if not valid:
                    return JsonResponse({'result': {'success': False, 'status': status}})

                # Get the user by email and generate and send a password code.
                try:
                    user = User.objects.get(email=email)
                except Exception as e:
                    # Failed to find user/generate/send.
                    return JsonResponse({'result': {'success': False, 'status': User.INVALID_EMAIL, 'message': str(e)}})

                try:
                    user.generate_password_reset_code()
                except Exception as e:
                    # Failed to find user/generate/send.
                    return JsonResponse({'result': {'success': False, 'status': User.PASSWORD_RESET_FAILED, 'message': str(e)}})

                try:
                    self.send_password_reset(user)
                except Exception as e:
                    # Failed to find user/generate/send.
                    return JsonResponse({'result': {'success': False, 'status': User.PASSWORD_RESET_SEND_FAILED, 'message': str(e)}})

                # Succeeded.
                return JsonResponse({'result': {
                    'success': True, 'status': User.PASSWORD_RESET_SEND_SUCCEEDED
                }})
            
            elif action == 'reset':
                # This action resets the password.

                # Get the passwords and code.
                password = get_data(request, 'password')
                password_confirm = get_data(request, 'password_confirm')
                code = get_data(request, 'code')

                if not code:
                    # The code is required.
                    return JsonResponse({'result': {'success': False, 'status': User.INVALID_PASSWORD_CODE}})
                
                # See if the email corresponds to an actual user.
                try:
                    user = User.objects.get(password_reset_code=code)
                except: 
                    return JsonResponse({'result': {'success': False, 'status': User.INVALID_PASSWORD_CODE}})

                if not user:
                    # If the user wasn't found.
                    return JsonResponse({'result': {'success': False, 'status': User.INVALID_PASSWORD_CODE}})

                if user.password_reset_expired():
                    # If the user's password reset code is expired.
                    return JsonResponse({'result': {'success': False, 'status': User.PASSWORD_RESET_EXPIRED}})

                # Check the passwords for validity.
                valid, status = User.validate_password(password, password_confirm, user)
                if not valid:
                    return JsonResponse({'result': {'success': False, 'status': status}})

                # Set the password. Also invalidates the code.
                user.reset_password(password, password_confirm)
                
                # Succeeded.
                return JsonResponse({'result': {
                    'success': True, 'status': User.PASSWORD_RESET_SUCCEEDED,
                }})
            
            else:
                raise Exception('No action given.')
            
        except Exception as e:
            return JsonResponse({'error': 'Password reset failed: ' + str(e)})

    @action(methods=['get'], detail=False, url_path="verification", permission_classes=[permissions.AllowAny])
    def verification(self, request):
        try:
            action = request.GET.get('action')
            email = request.GET.get('email')

            # The email is always lowercased.
            if email:
                email = email.lower()

            # Check the email.
            validation = {}
            valid, status = User.validate_email(email, validation)
            if not valid:
                return JsonResponse({'result': {'success': False, 'status': status, 'validation': validation}})
            
            if action == 'resend':
                try:
                    user = User.objects.get(email=email)
                except Exception as e:
                    # Failed to find user.
                    return JsonResponse({'result': {'success': False, 'status': User.INVALID_EMAIL, 'message': str(e)}})

                try:
                    user.generate_verification_code()
                    self.send_verification(user)
                except Exception as e:
                    # Failed to generate/send.
                    return JsonResponse({'result': {'success': False, 'status': User.VERIFICATION_SEND_FAILED, 'message': str(e)}})
                
                return JsonResponse({'result': {
                    'success': True, 'status': User.VERIFICATION_SENT
                }})
            
            else:
                raise Exception('No action given.')
        except Exception as e:
            return JsonResponse({'error': 'Verification failed: ' + str(e)})
    
    @action(methods=['get'], detail=False, permission_classes=[permissions.AllowAny])
    def verify(self, request):
        try:
            code = request.GET.get('code')
            user = User.objects.get(verification_code=code)
            if user.is_verification_expired():
                dt = datetime.strftime(user.verification_expiry, '%B %d, %Y')
                raise Exception(f'Verification expired on {dt}. Please register again or contact us.')
            if user.is_verified():
                return JsonResponse({'result': {
                    'success': False, 'status': User.ALREADY_VERIFIED
                }})
            user.verify(code)
            return JsonResponse({'result': {
                'success': True, 'status': User.VERIFICATION_SUCCEEDED, 
            }})
        except Exception as e: 
            return JsonResponse({'result': {
                'success': False, 'status': User.VERIFICATION_FAILED, 
                'message': 'Verification has failed: ' + str(e)
            }})

    def check_ip(self, addr):
        """
        Make sure that the given address is allowable by checking against the signin area.
        :param addr: The IP address.
        :return: The user's location, the country name and true/false if the IP is in the signup region.
        """
        line = None
        with connection.cursor() as cur:
            cur.execute('''
                select a.location, c.city_name, c.country_name, (
                        select st_contains(geom::geometry, a.location::geometry) from rov.signup_area) as in_region
                from maxmind.geoip_city_blocks a
                    inner join maxmind.geoip_city_locations c on c.geoname_id = a.geoname_id
                where inet %s << a.network
            ''', [addr])
            try:
                # If the fetch fails, return Nones.
                location, city, country, in_region = cur.fetchone()
                # Create a location from whichever of city, country is available.
                city = ', '.join(filter(lambda a: a != None, [city, country]))
                return location, city, in_region
            except: pass
        return None, None, None
    
    @action(methods=['post'], detail=False, permission_classes=[permissions.AllowAny])
    def register(self, request):
        """
        Registers the new user.
        1. Validates the user data.
        2. Checks whether the given email is already registered.
          - If yes, checks whether a validation link has been sent for the already-registered user and if it is expired.
            - If yes, and it is expired, provides the user an opportunity to resend the validation link, or contact an admin.
            - If no, tell user to wait and give the opportunity to resend the link.
        3. Checks the user's IP to make sure it's within the geographic area.
          - If not register, prevent registration and notify.
        4. Register user, send validation link.
        """
        try:
            data = request.data
            
            # Get the user's IP.
            ip_address = get_ip_address(request)

            # Fields excluded from the user data when user is created.
            exclude_fields = ('password', 'password_confirm', 'email')

            # Since the email is used as a username, which is case sensitive, we have to
            # be careful to lowercase it whenever it is checked or inserted or use a 
            # case-insensitive match. The framework authentication methods are not
            # case-insensitive.
            email = data.get('email')
            if email:
                email = email.lower()

            # Initial user data.
            user = {
                'password': data.get('password'),
                'password_confirm': data.get('password_confirm'),
                'email': email,
                'first_name': data.get('first_name'),
                'last_name': data.get('last_name'),
                'organization': data.get('organization'),
                'org_type': data.get('org_type'),
                'registration_reason': data.get('registration_reason'),
                'registration_ip': ip_address,
                'registration_location': None,
                'ip_in_region': None,
                'is_superuser': False,
                'is_staff': False,
                'is_active': False,
                'date_joined': datetime.now(tz=timezone.utc),
                'allowed': data.get('allowed', True),
            }

            # Check to see if the IP address is allowed; add location data to user.
            location, country_name, in_region = self.check_ip(ip_address)
            user['registration_location'] = country_name
            user['ip_in_region'] = in_region

            # Validate the user.
            validation = User.validate_user_data(user)
            if len(validation):
                # The validation data are not empty. Return for fixing.
                return JsonResponse({'result': {
                    'success': True, 'status': 'validation_failed', 'validation': validation,
                }})

            try:
                # Try to get the user by email. Email is not case sensitive.
                existing_user = User.objects.get(email=email)
            except:
                # The user has not been previously created.
                existing_user = None
            
            if existing_user:
                # The user exists. Try to handle its state.
                if existing_user.is_verified():
                    # The user has already been verified. Redirect to login or password change.
                    return JsonResponse({'result': {
                        'success': False, 'status': User.USER_EXISTS,
                    }})
                if existing_user.is_verification_expired():
                    # The verification code has been created/sent is expired. Ask the user if they would like to send another one.
                    return JsonResponse({'result': {
                        'success': False,  'status': User.VERIFICATION_EXPIRED,
                    }})
                if existing_user.is_verification_pending():
                    # The user exists and verification is still pending and not expired.
                    return JsonResponse({'result': {
                        'success': False, 'status': User.VERIFICATION_PENDING,
                    }})
                
                # The user exists but is in an unexpected state. User must contact .
                raise Exception('Unexpected error. Please contact us.')                

            # Try to create the user.
            with transaction.atomic():
                # Generate the User. This function processes the password correctly.
                new_user = User.objects.create_user(
                    email=email, password=user['password'],
                    **{k: user[k] for k in user.keys() if k not in exclude_fields}
                )
                # Generate the verification code.
                new_user.generate_verification_code()
                # Send the verification.
                self.send_verification(new_user)

            return JsonResponse({'result': {
                'success': True, 'status': User.REGISTRATION_SUCCEEDED,
            }})
        except Exception as e:
            return JsonResponse({'error': 'Registration failed: ' + str(e)})

    def send_verification(self, user):
        """
        Send the verification email.
        The user is an instance of User.
        """
        message = process_email_template(
            settings.MSEA_VERIFICATION_TEMPLATE,
            first_name=user.first_name, 
            verification_code=user.verification_code, 
            verification_expiry_date=user.verification_expiry.strftime('%B %d, %Y'),
            verification_expiry_time=user.verification_expiry.strftime('%I:%M:%S %p'),
            verification_host=settings.MSEA_VERIFICATION_HOST,
        )
        
        send_email(
            message=message, 
            subject=settings.MSEA_VERIFICATION_SUBJECT, 
            from_email=settings.MSEA_VERIFICATION_FROM, 
            to_email=user.email
        )

    def send_password_reset(self, user):
        """
        Send the password reset email.
        The user is an instance of User.
        """
        message = process_email_template(
            settings.MSEA_PASSWORD_RESET_TEMPLATE,
            first_name=user.first_name, 
            password_reset_code=user.password_reset_code,
            password_reset_expiry_date=user.password_reset_expiry.strftime('%B %d, %Y'),
            password_reset_expiry_time=user.password_reset_expiry.strftime('%I:%M:%S %p'),
            password_reset_host=settings.MSEA_PASSWORD_RESET_HOST,
        )
        
        send_email(
            message=message, 
            subject=settings.MSEA_PASSWORD_RESET_SUBJECT,
            from_email=settings.MSEA_PASSWORD_RESET_FROM, 
            to_email=user.email
        )

    def destroy(self, request, *args, **kwargs):
        try:
            raise Exception('Not allowed.')
        except Exception as e:
            return JsonResponse({'error': str(e)})
