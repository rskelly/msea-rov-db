"""
Contains view and serializer classes related to user and group management and authentication.
"""
from django.conf import settings
from django.http import JsonResponse

from msea.util.util import send_email, get_ip_address
from msea.views.auth import ReSTOpenViewSet
from msea.models.auth import User

class ContactViewSet(ReSTOpenViewSet):

    def create(self, request):
        """
        Sends an email message in response to a POST.
        """
        try:
            validation = {}

            # Validate required inputs.
            topic = request.data.get('topic')
            if not topic:
                validation['topic'] = 'Please select a topic.'
            message = request.data.get('message')
            if not message:
                validation['message'] = 'Your message must not be empty.'
            email = request.data.get('email')
            User.validate_email(email, validation)

            # If validation messages, send them back.
            if len(validation):
                return JsonResponse({'result': {'sent': False, 'validation': validation}})

            # Get user's time and IP.
            time = request.data.get('time')
            ip = get_ip_address(request)

            # Format the message.
            message = f'''<p>Email from: {email}</p>
                            <p>Topic: {topic}</p>
                            <p>Date: {time}</p>
                            <p>IP: {ip}</p>
                            <p>Message:</p>
                            <pre>{message}</pre>'''
            
            # Send the email.
            send_email(
                message=message, 
                subject='ROV Website Contact Form',
                from_email=settings.MSEA_CONTACT_EMAIL, 
                to_email=settings.MSEA_CONTACT_EMAIL,
                reply_to=email,
            )
            return JsonResponse({'result': {'sent': True}})
        except Exception as e:
            return JsonResponse({'error': str(e)})
        