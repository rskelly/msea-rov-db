from django.http import JsonResponse
from django.core.exceptions import ValidationError
from rest_framework import viewsets, serializers

from knox.auth import TokenAuthentication


class ReSTViewSet(viewsets.ModelViewSet):
    """
    Provides a ReST endpoint for retrieving and modifying models.
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = None

    ordering = None
    offset = 0
    limit = -1
    total = 0

    def get_permissions(self):
        """
        Return permissions such that list and retrieve are allowed to everyone, and
        modifications required admin group membership. Subclasses must override this.
        """
        if self.permission_classes is None:
            raise Exception('Permissions not defined. Please use a subclass of ReSTViewSet and define the get_permissions method.')
        return [p() for p in self.permission_classes]

    def perform_authentication(self, request):
        """
        Perform authentication on the incoming request.

        Note that if you override this and simply 'pass', then authentication
        will instead be performed lazily, the first time either
        `request.user` or `request.auth` is accessed.
        """
        pass

    def paginate(self, q, request):
        """
        Select a slice of the query result given the pagination parameters in the request,
        or using the defaults.
        """
        if request.query_params.get('count'):
            self.limit = int(request.query_params.get('count'))
        if request.query_params.get('page'):
            self.offset = self.limit * (int(request.query_params.get('page')) - 1)
        if q:
            self.total = len(q) # Not .count because raw querysets don't have that.
            if self.limit > 0:
                q = q[self.offset:self.offset + self.limit]
        else:
            self.total = 0
            q = []
        return q

    def get_list_queryset(self, request):
        """
        Get the query set. If the order field is specified, apply it.
        """
        try:
            q = self.model_class.objects.all()
            if self.ordering:
                q = q.order_by(*self.ordering)
            return q
        except:
            # There might not be a model class.
            return None

    def list(self, request):
        """
        Get the paginated list.
        """
        queryset = self.get_list_queryset(request)
        queryset = self.paginate(queryset, request)
        return self.render_list(queryset, request)

    def render_list(self, queryset, request):
        """
        Return the result as a list (result) and pagination properties (page, count, total).
        """
        try:
            serializer = self.get_serializer(queryset, many=True)
            return JsonResponse({
                'result': [dict(d) for d in serializer.data],
                'page': int(self.offset / self.limit) + 1,
                'count': self.limit,
                'total': self.total
            })
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def retrieve(self, request, pk=None):
        """
        Retrieve the instance indicated by pk.
        """
        try:
            item = self.model_class.objects.get(pk=pk)
            serializer = self.get_serializer(item)
            return JsonResponse({'result': dict(serializer.data)})
        except Exception as e:
            return JsonResponse({'error': str(e)})
        
    def create(self, request):
        """
        Create an instance using the request data. If an object with an 
        ID is given, the ID is deleted and the instance created. If there
        are restrictions (e.g., duplicates in a unique column) and exception
        occurs.
        """
        try:
            data = request.data
            # Instantiate the serializer, check its validity and save.
            serializer = self.get_serializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return JsonResponse({'result': serializer.data})
        except ValidationError as e:
            return JsonResponse({'error': e.message})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def update(self, request, pk=None):
        """
        Update the entity.
        """
        try:
            # Retrieve the instance by ID.
            item = self.model_class.objects.get(pk=pk)
            # Instantiate the serializer, check its validity and save.
            serializer = self.get_serializer(instance=item, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return JsonResponse({'result': serializer.data})
        except ValidationError as e:
            return JsonResponse({'error': e.message})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def partial_update(self, request, pk=None):
        """
        Update the entity.
        """
        try:
            # Retrieve the instance by ID.
            item = self.model_class.objects.get(pk=pk)
            # Instantiate the serializer, check its validity and save.
            serializer = self.get_serializer(instance=item, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return JsonResponse({'result': serializer.data})
        except ValidationError as e:
            return JsonResponse({'error': e.message})
        except Exception as e:
            return JsonResponse({'error': str(e)})

    def destroy(self, request, pk=None):
        """
        Delete the entity.
        """
        try:
            item = self.model_class.objects.get(pk=pk)
            item.delete()
            return JsonResponse({'result': True})
        except ValidationError as e:
            return JsonResponse({'error': e.message})
        except Exception as e:
            return JsonResponse({'error': str(e)})


