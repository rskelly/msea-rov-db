"""
Contains view and serializer classes related NDST models.
"""
import json
import uuid

from rest_framework import serializers
from django.http import JsonResponse
from django.db import models
from rest_framework.decorators import action

from msea.views.auth import ReSTUserViewSet
from msea.models import ndst

class TransferViewSet(ReSTUserViewSet):
        
    @action(methods=['POST'], detail=False)
    def upload(self, request):

        data = request.FILES['file']
        data = json.load(data.file)

        types = (
            ('people', ndst.NDSTPerson, ('first_name', 'last_name')), 
            ('cruises', ndst.NDSTCruise, ('name', 'leg')),
            ('dives', ndst.NDSTDive, ('cruise_name', 'leg', 'name')),
            ('transects', ndst.NDSTTransect, ('cruise_name', 'leg', 'dive_name', 'name')),
            ('equipment', ndst.NDSTEquipment, ('brand', 'model', 'serial_number')),
            ('equipconfigs', ndst.NDSTEquipConfig, ('name',)),
            ('diveconfigs', ndst.NDSTDiveConfig, ('name',))
        )
        try:
            for name, cls, keys in types:
                for d in data[name]:
                    # Find the entities.
                    ret = cls.objects.filter(**{k: d[k] for k in keys})
                    if len(ret) > 1:
                        # If there's more than one, delete all but the first.
                        ret.exclude(pk=ret.first().pk).delete()
                    elif len(ret) == 0:
                        # If there are none, insert one.
                        del d['id']
                        d['row_id'] = str(uuid.uuid4())
                        cls.objects.create(**d)
            return JsonResponse({'result': True})
        except Exception as e:
            return JsonResponse({'error': str(e)})
    
    @action(methods=['GET'], detail=False)
    def download(self, request):
        """
        Download all NDST data for a cruise as a single JSON file.
        """
        cruises = ndst.NDSTCruise.objects.all()
        dives = ndst.NDSTDive.objects.all()
        transects = ndst.NDSTTransect.objects.all()
        equipment = ndst.NDSTEquipment.objects.all()
        equipconfigs = ndst.NDSTEquipConfig.objects.all()
        people = ndst.NDSTPerson.objects.all()
        diveconfigs = ndst.NDSTDiveConfig.objects.all()
        result = {
            'cruises': CruiseSerializer(cruises, many=True).data,
            'dives': DiveSerializer(dives, many=True).data,
            'transects': TransectSerializer(transects, many=True).data,
            'equipment': EquipmentSerializer(equipment, many=True).data,
            'equipconfigs': EquipConfigSerializer(equipconfigs, many=True).data,
            'people': PersonSerializer(people, many=True).data,
            'diveconfigs': DiveConfigSerializer(diveconfigs, many=True).data,
        }
        return JsonResponse(result)

class CruiseSerializer(serializers.ModelSerializer):
    class Meta:
        model = ndst.NDSTCruise
        fields = '__all__'

class CruiseViewSet(ReSTUserViewSet):
    serializer_class = CruiseSerializer
    model_class = ndst.NDSTCruise

    def list(self, request):
        active = request.GET.get('active') == 'true'
        filter = {}
        if active:
            filter = {'active': True}
        return JsonResponse({'result': CruiseSerializer(
                ndst.NDSTCruise.objects.filter(**filter).order_by('name', 'leg'), many=True
        ).data})

class DiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = ndst.NDSTDive
        fields = '__all__'

class DiveViewSet(ReSTUserViewSet):
    serializer_class = DiveSerializer
    model_class = ndst.NDSTDive

    def list(self, request):
        cruise_name = request.GET.get('cruise_name')
        leg = request.GET.get('leg')
        return JsonResponse({'result': 
            DiveSerializer(ndst.NDSTDive.objects.filter(cruise_name=cruise_name, leg=leg)
                .order_by('cruise_name', 'leg', 'name'), many=True
        ).data})

class DiveConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = ndst.NDSTDiveConfig
        fields = '__all__'
        
class DiveConfigViewSet(ReSTUserViewSet):
    serializer_class = DiveConfigSerializer
    model_class = ndst.NDSTDiveConfig

    def list(self, request):
        return JsonResponse({'result': 
            DiveConfigSerializer(ndst.NDSTDiveConfig.objects.all().order_by('name'), many=True
        ).data})

class TransectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ndst.NDSTTransect
        fields = '__all__'

class TransectViewSet(ReSTUserViewSet):
    serializer_class = TransectSerializer
    model_class = ndst.NDSTTransect

    def list(self, request):
        cruise_name = request.GET.get('cruise_name')
        leg = request.GET.get('leg')
        dive_name = request.GET.get('dive_name')
        return JsonResponse({'result': TransectSerializer(
                ndst.NDSTTransect.objects.filter(cruise_name=cruise_name, leg=leg, dive_name=dive_name)
                    .order_by('cruise_name', 'leg', 'dive_name', 'name'), many=True
            ).data
        })

class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = ndst.NDSTPerson
        fields = '__all__'

class PersonViewSet(ReSTUserViewSet):
    serializer_class = PersonSerializer
    model_class = ndst.NDSTPerson

    def list(self, request):
        return JsonResponse({'result': PersonSerializer(
            ndst.NDSTPerson.objects.all().order_by('last_name', 'first_name'), many=True
        ).data})

class EquipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ndst.NDSTEquipment
        fields = '__all__'

class EquipmentViewSet(ReSTUserViewSet):
    serializer_class = EquipmentSerializer
    model_class = ndst.NDSTEquipment

    def list(self, request):
        active = request.GET.get('active') == 'true'
        filter = {}
        if active:
            filter['active'] = True
        return JsonResponse({'result': EquipmentSerializer(
            ndst.NDSTEquipment.objects.filter(**filter).order_by('type', 'brand', 'model', 'serial_number'), many=True
        ).data})

    @action(methods=['get'], detail=False)
    def types(self, request):
        """
        Returns a list of unique types from the equipment type column.
        """        
        types = list(map(lambda a: a['type'], ndst.NDSTEquipment.objects.values('type').distinct().order_by('type')))
        return JsonResponse({'result': types})
    
class EquipConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = ndst.NDSTEquipConfig
        fields = '__all__'

class EquipConfigViewSet(ReSTUserViewSet):
    serializer_class = EquipConfigSerializer
    model_class = ndst.NDSTEquipConfig

    def list(self, request):
        active = request.GET.get('active') == 'true'
        filter = {}
        if active:
            filter['active'] = True
        return JsonResponse({'result': EquipConfigSerializer(
            ndst.NDSTEquipConfig.objects.filter(**filter).order_by('name'), many=True
        ).data})
