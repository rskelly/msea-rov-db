"""
These are the Django models for the MSEA database.

Note that the data model is designed independently so these entities 
are set as "unmanaged." That is, these models have to be manually updated
to reflect changes to the database; changes here will not automatically be 
reflected in the database.

Documentation for the entities and fields are found in the data model.

Rob Skelly <Robert.Skelly@dfo-mpo.gc.ca>
"""
from datetime import timedelta, datetime, timezone as tz

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import Group
from msea.models.auth import User

class Person(models.Model):

    biigle_user_id = models.IntegerField(null=True, blank=True)
    biigle_uuid = models.CharField(max_length=36, null=True, blank=True)
    first_name = models.CharField(max_length=32, null=False)
    last_name = models.CharField(max_length=32, null=False)
    email = models.CharField(max_length=128, null=True, blank=True, default=None)
    photo = models.BinaryField(null=True, blank=True)
    bio = models.TextField(null=True, blank=True)
    affiliation = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"shared"."person"'
        managed = False
        ordering = ('last_name', 'first_name')
        constraints = [
            models.UniqueConstraint(
                models.functions.Lower('first_name'), models.functions.Lower('last_name'), 
                name='person_name_unique'
            )
        ]

    def __repr__(self):
        return '{}, {}'.format(self.last_name, self.first_name)


class FileType(models.Model):

    name = models.CharField(max_length=32, null=False, unique=True)

    class Meta:
        db_table = '"shared"."file_type"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class File(models.Model):
    file_type = models.ForeignKey(FileType, on_delete=models.RESTRICT, null=True, blank=True)
    name = models.CharField(max_length=64, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    metadata = models.JSONField(null=True, blank=True)
    path = models.CharField(max_length=255, null=False, unique=True)
    hash = models.CharField(max_length=64, null=False)
    blob_url = models.CharField(max_length=255, null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False)
    updated_on = models.DateTimeField(default=timezone.now, null=False)

    class Meta:
        db_table = '"shared"."file"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.path



class SpatialLibrary(models.Model):

    name = models.CharField(max_length=64, null=False)
    note = models.TextField(null=True, blank=True)
    thumbnail = models.BinaryField(null=True, blank=True)
    geom = models.BinaryField(null=True, blank=True)
    rast = models.BinaryField(null=True, blank=True)
    metadata = models.JSONField(null=True, blank=True)

    class Meta:
        db_table = '"shared"."spatial_library"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class SpatialLibraryFile(models.Model):

    spatial_library = models.ForeignKey(SpatialLibrary, on_delete=models.RESTRICT, null=False)
    file = models.ForeignKey(File, on_delete=models.RESTRICT, null=False)

    class Meta:
        db_table = '"shared"."spatial_library_file"'
        managed = False
        ordering = ('spatial_library', 'file')

    def __repr__(self):
        return self.file.name


class Site(models.Model):
    spatial_library = models.ForeignKey(SpatialLibrary, on_delete=models.RESTRICT, null=True, blank=True)
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"shared"."site"'
        managed = False
        ordering = ('spatial_library', 'name')

    def __repr__(self):
        return self.name


class Organisation(models.Model):

    name = models.CharField(max_length=64, null=False, unique=True)
    country = models.CharField(max_length=8, null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"shared"."organisation"'
        managed = False
        ordering = ('name', 'country')

    def __repr__(self):
        return self.name


class Restriction(models.Model):

    name = models.CharField(max_length=128, null=False, unique=True)
    key = models.CharField(max_length=128, null=False)
    pattern = models.TextField(null=False)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(null=False, default=timezone.now, blank=True)
    updated_on = models.DateTimeField(null=False, default=timezone.now, blank=True)

    class Meta:
        db_table = '"shared"."restriction"'
        managed = False
        unique_together = ('key', 'pattern')
        ordering = ('name', 'key', 'pattern')


class RestrictionGroup(models.Model):
    
    restriction = models.ForeignKey(Restriction, related_name='+', null=False, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, related_name='+', null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = '"shared"."restriction_group"'
        managed = False
        unique_together = ('restriction', 'group')
        ordering = ('restriction', 'group')


def _one_hour():
    return datetime.now(tz=tz.utc) + timedelta(minutes=60)


class UploadedFile(models.Model):
    path = models.CharField(max_length=256, null=False, unique=True)
    type = models.CharField(max_length=256, null=False)
    name = models.CharField(max_length=256, null=False)
    marked_for_delete = models.BooleanField(null=False, default=False)
    expires_on = models.DateTimeField(null=False, default=_one_hour)
    created_on = models.DateTimeField(null=False, auto_now_add=True)

    class Meta:
        db_table = '"shared"."uploaded_file"'
        managed = False
        ordering = ('name', 'type')
