"""
TODO: Is this necessary?
"""
from django.db import models


class DjangoContentType(models.Model):

    app_label = models.CharField(max_length=200)
    model = models.CharField(max_length=200)

    class Meta:
        db_table = '"public"."django_content_type"'
        managed = False


class DjangoMigrations(models.Model):

    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        db_table = '"public"."django_migrations"'
        managed = False


class DjangoSession(models.Model):

    session_key = models.CharField(max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        db_table = '"public"."django_session"'
        managed = False


class DjangoAdminLog(models.Model):

    action_time = models.DateTimeField()
    object_id = models.TextField()
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type_id = models.IntegerField()
    user_id = models.IntegerField()

    class Meta:
        db_table = '"public"."django_admin_log"'
        managed = False
