"""
These are the Django models for the MSEA database.

Note that the data model is designed independently so these entities 
are set as "unmanaged." That is, these models have to be manually updated
to reflect changes to the database; changes here will not automatically be 
reflected in the database.

Documentation for the entities and fields are found in the data model.

Rob Skelly <Robert.Skelly@dfo-mpo.gc.ca>
"""
from django.db import models
from django.utils import timezone

from msea.models import shared
from msea.models.rov.lookups import *

## Equiment-related

class EquipmentType(models.Model):
    """
    Lists the types of equipment that a Model can be. Referenced by Model.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)
    category = models.CharField(max_length=64, null=False)

    class Meta:
        db_table = '"rov"."equipment_type"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class Model(models.Model):
    """
    The model name of a piece of equipment. Records the brand and model name, attributes 
    and the type of equipment.
    """
    equipment_type = models.ForeignKey(EquipmentType, null=False, on_delete=models.RESTRICT)
    brand_name = models.CharField(max_length=64, null=True, blank=True)
    model_name = models.CharField(max_length=64, null=True, blank=True)
    attributes = models.JSONField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."model"'
        managed = False
        ordering = ('brand_name', 'model_name')

    def __repr__(self):
        return f'{self.brand_name}, {self.model_name}'


class Platform(models.Model):
    """
    Represents a platform -- a ship, ROV, submersible, drop camera, etc. -- a
    piece of equipment upon which instruments can be mounted.
    """
    organisation = models.ForeignKey(shared.Organisation, on_delete=models.RESTRICT, null=False)
    model = models.ForeignKey(Model, on_delete=models.RESTRICT, null=False)
    name = models.CharField(max_length=64, null=False)
    serial_number = models.CharField(max_length=256, null=True, blank=True)
    retired = models.DateField(null=True, blank=True)
    attributes = models.JSONField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=True, blank=True)

    class Meta:
        db_table = '"rov"."platform"'
        managed = False
        unique_together = ('model', 'serial_number')
        ordering = ('name',)

    def __repr__(self):
        return f'{self.name}, {self.serial_number}'


class PlatformConfig(models.Model):
    """
    The configuration of a platform. Referenced by dives, via the sub config
    and ship config and by instrument configs.
    """
    platform = models.ForeignKey(Platform, related_name='configs', on_delete=models.RESTRICT, null=False)
    configuration = models.JSONField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=True, blank=True)

    class Meta:
        db_table = '"rov"."platform_config"'
        managed = False
        ordering = ('id',)

    def __repr__(self):
        return f'{self.platform.name} Config'


class Instrument(models.Model):
    """
    An instrument used to record data.
    """
    model = models.ForeignKey(Model, on_delete=models.RESTRICT, null=False)
    organisation = models.ForeignKey(shared.Organisation, on_delete=models.RESTRICT, null=False)
    serial_number = models.CharField(max_length=256, null=False)
    retired = models.DateField(null=True, blank=True)
    attributes = models.JSONField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False, blank=True)

    class Meta:
        db_table = '"rov"."instrument"'
        managed = False
        ordering = ('model', 'serial_number')

    def __repr__(self):
        return f'{self.model} {self.serial_number}'


class InstrumentConfig(models.Model):
    """
    The configuration of an instrument.
    """
    instrument = models.ForeignKey(Instrument, related_name='configs', on_delete=models.CASCADE, null=False)
    platform_config = models.ForeignKey(PlatformConfig, related_name="instrument_configs", on_delete=models.CASCADE, null=False)
    note = models.TextField(null=True, blank=True)
    configuration = models.JSONField(null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False, blank=True)
    updated_on = models.DateTimeField(default=timezone.now, null=False, blank=True)

    class Meta:
        db_table = '"rov"."instrument_config"'
        managed = False
        unique_together = ('instrument', 'platform_config')
        ordering = ('id',)

    def __repr__(self):
        return 'InstrumentConfig'


## Cruise metadata related

class Program(models.Model):
    """
    A program is an initiative which may oversee one or more cruises -- 
    a funding program, scientific program, collaboration, etc.
    """
    name = models.CharField(max_length=64, null=False)
    objective = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    start_date = models.DateField(null=False, default=timezone.now)
    end_date = models.DateField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(null=True, blank=True, default=timezone.now)

    class Meta:
        db_table = '"rov"."program"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class ProgramRole(models.Model):
    """
    Describes the role a person might have with respect to a program.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."program_role"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class ProgramMember(models.Model):
    """
    Links a person to a program with their role within the program.
    """
    program = models.ForeignKey(Program, related_name='members', on_delete=models.CASCADE, null=False)
    person = models.ForeignKey(shared.Person, on_delete=models.RESTRICT, null=False)
    role = models.ForeignKey(ProgramRole, on_delete=models.RESTRICT, null=False)

    class Meta:
        db_table = '"rov"."program_member"'
        managed = False
        ordering = ('person', 'role')

    def __repr__(self):
        return f'{self.program}, {self.person}, {self.role}'


class Cruise(models.Model):
    """
    A cruise -- the journey of a ship related to submersible or ROV activities
    from port to port. Dives and associated activities derive from this central entity.
    """
    ship = models.ForeignKey(Platform, null=False, on_delete=models.RESTRICT)
    name = models.CharField(max_length=64, null=False)
    leg = models.IntegerField(default=1, null=False)
    start_time = models.DateTimeField(null=False, auto_now_add=True)
    end_time = models.DateTimeField(null=True, blank=True)
    planned_track = models.BinaryField(null=True)
    note = models.TextField(null=True, blank=True)
    objective = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=False, blank=True)
    updated_on = models.DateTimeField(auto_now_add=True, null=False, blank=True)
    approved = models.IntegerField(default=0, null=False)
    admin_note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."cruise"'
        managed = False
        ordering = ('name', 'leg')

    def __repr__(self):
        return self.name


class CruiseRole(models.Model):
    """
    The role a person might have on a cruise, e.g., Chief Scientist.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."cruise_role"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class CruiseCrew(models.Model):
    """
    Links a cruise to a person and a role to construct the cruise's crew.
    """
    cruise = models.ForeignKey(Cruise, related_name='crew', on_delete=models.CASCADE, null=False)
    person = models.ForeignKey(shared.Person, on_delete=models.RESTRICT, null=False)
    cruise_role = models.ForeignKey(CruiseRole, on_delete=models.RESTRICT, null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."cruise_crew"'
        managed = False
        unique_together = ('cruise', 'person', 'cruise_role')
        ordering = ('cruise', 'person', 'cruise_role')

    def __repr__(self):
        return f'{self.cruise}, {self.person}, {self.cruise_role}'


class CruiseDocument(models.Model):
    """
    Links a document URL or file to a cruise.
    """
    cruise = models.ForeignKey(Cruise, related_name="documents", on_delete=models.CASCADE)
    file = models.ForeignKey(shared.UploadedFile, null=True, blank=True, on_delete=models.RESTRICT)
    title = models.CharField(max_length=256, null=False)
    note = models.TextField(null=True, blank=True)
    url = models.CharField(max_length=1024, null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False)
    updated_on = models.DateTimeField(default=timezone.now, null=False)

    class Meta:
        db_table = '"rov"."cruise_document"'
        managed = False
        ordering = ('title',)

    def __repr__(self):
        return f'{self.cruise.name}-{self.cruise.leg} -> {self.url or self.file_name}'


class CruiseFirstNationContact(models.Model):
    """
    Records First Nations contacts in relation to a cruise.
    """
    cruise = models.ForeignKey(Cruise, related_name='first_nation_contacts',
                               on_delete=models.CASCADE, null=False)
    nation = models.TextField(null=True, blank=True)
    contact_name = models.CharField(max_length=128, null=False)
    email = models.CharField(max_length=256, null=True, blank=True)
    phone = models.CharField(max_length=32, null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."cruise_fn_contact"'
        managed = False
        ordering = ('cruise', 'nation', 'contact_name')

    def __repr__(self):
        return self.contact_name + (f' - {self.nation}' if self.nation else '')


class CruiseProgram(models.Model):
    """
    Links a cruise to a program.
    """
    program = models.ForeignKey(Program, on_delete=models.CASCADE, null=False)
    cruise = models.ForeignKey(Cruise, related_name='programs', on_delete=models.CASCADE, null=False)

    class Meta:
        db_table = '"rov"."cruise_program"'
        managed = False
        unique_together = ('program', 'cruise')
        ordering = ('cruise', 'program')

    def __repr__(self):
        return f'{self.cruise} > {self.program}'


class Dive(models.Model):
    """
    A dive is the activity of an ROV, submerisble, etc. from submersion to retrieval. All underwater activites are
    linked to this entity which is, in turn, linked to a cruise. A dive can only occur during one cruise.
    """
    sub_config = models.ForeignKey(PlatformConfig, related_name='+', on_delete=models.CASCADE, null=False)
    ship_config = models.ForeignKey(PlatformConfig, related_name='+', on_delete=models.CASCADE, null=False)
    site = models.ForeignKey(shared.Site, on_delete=models.RESTRICT, null=True, blank=True, related_name='+')
    cruise = models.ForeignKey(Cruise, related_name='dives', on_delete=models.CASCADE, null=False)
    name = models.CharField(max_length=64, null=False)
    objective = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    end_time = models.DateTimeField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    attributes = models.JSONField(null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False, blank=True)
    updated_on = models.DateTimeField(default=timezone.now, null=False, blank=True)
    admin_note = models.TextField(null=True, blank=True)
    seatube_id = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."dive"'
        managed = False
        ordering = ('name',)
        unique_together = ('cruise_id', 'name')

    def __repr__(self):
        return self.name


class DiveRole(models.Model):
    """
    The role a person might have on a dive, e.g., Pilot.
    """

    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."dive_role"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class DiveCrew(models.Model):
    """
    Links a dive to a person and a role to construct the dive's crew.
    """
    dive = models.ForeignKey(Dive, related_name='crew', on_delete=models.CASCADE, null=False)
    person = models.ForeignKey(shared.Person, on_delete=models.RESTRICT, null=False)
    dive_role = models.ForeignKey(DiveRole, on_delete=models.RESTRICT, null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."dive_crew"'
        managed = False
        ordering = ('dive', 'person', 'dive_role')

    def __repr__(self):
        return f'{self.dive}, {self.person}, {self.dive_role}'


class Transect(models.Model):
    """
    A transect is a time period during a dive. Not all dives have transects, and most derivative
    entities link to the dive by necessity, but only optionally to the transect. In that sense,
    transects are an important accounting fiction.
    """
    dive = models.ForeignKey(Dive, related_name='transects', on_delete=models.CASCADE, null=False)
    name = models.CharField(max_length=64, null=False)
    objective = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    end_time = models.DateTimeField(null=True, blank=True)
    admin_note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."transect"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class WeatherObservation(models.Model):
    """
    A weather observation.
    """
    cruise = models.ForeignKey(Cruise, on_delete=models.CASCADE, null=False)
    cruise_crew = models.ForeignKey(CruiseCrew, on_delete=models.RESTRICT, null=True, blank=True)
    temperature = models.FloatField(null=True, blank=True)
    pressure = models.FloatField(null=True, blank=True)
    wind_speed = models.FloatField(null=True, blank=True)
    wind_direction = models.FloatField(null=True, blank=True)
    swell = models.CharField(max_length=32, null=True, blank=True)
    time = models.DateTimeField(default=timezone.now, null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."weather_observation"'
        managed = False
        ordering = ('cruise', 'time')

    def __repr__(self):
        return self.time


## Measurement related.

class MeasurementType(models.Model):
    """
    Records types of measurements. Measurements (depth, altitude, velocity, etc.)
    are stored in a single table, distinguished by a required reference to this
    table.
    """
    name = models.CharField(max_length=64, null=False)
    unit = models.CharField(max_length=32, null=False)
    minimum = models.FloatField(null=True, blank=True)
    maximum = models.FloatField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."measurement_type"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return f'{self.name}: {self.unit}'


class PositionType(models.Model):
    """
    Records types of positions. Positions are stored in a single table, 
    distinguished by a required reference to this table.

    TODO: Currently there is only one type of position (decimal degrees) so this
    entity may be unnecessary.
    """
    name = models.CharField(max_length=64, null=False)
    unit = models.CharField(max_length=32, null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."position_type"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class MediumType(models.Model):
    """
    Records the type of medium (video, photo, etc.) used to record events.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."medium_type"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class MediumFormat(models.Model):
    """
    Records the format (i.e., file type) of a medium.
    """
    medium_type = models.ForeignKey(MediumType, on_delete=models.RESTRICT, null=False)
    name = models.CharField(max_length=64, null=False, unique=True)
    extensions = models.JSONField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."medium_format"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name



class AnnotationSoftware(models.Model):
    """
    The annotation software used to generate an event. VideoMiner and Biigle are common.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."annotation_software"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class AnnotationJob(models.Model):
    """
    Provides information about an annotation job. The annotation job is linked to event,
    whereas the annotation protocol was formerly linked to habitat and observation events;
    those links are removed. The annotation job may link to more than one annotation protocol.
    """
    name = models.CharField(max_length=128, null=False, unique=True)
    objective = models.TextField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False)
    updated_on = models.DateTimeField(default=timezone.now, null=False)

    class Meta:
        db_table = '"rov"."annotation_job"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class AnnotationProtocol(models.Model):
    """
    The protocol used by annotators to generate annotations which are in turn used
    to create observation, habitat, status and measurement events.
    """
    name = models.CharField(max_length=128, null=False, unique=True)
    creator = models.ForeignKey(shared.Person, on_delete=models.RESTRICT, null=True)
    medium_type = models.ForeignKey(MediumType, on_delete=models.RESTRICT, null=False)
    annotation_software = models.ForeignKey(AnnotationSoftware, on_delete=models.RESTRICT, null=False)
    is_template = models.BooleanField(default=False, null=True, blank=True)
    image_interval = models.FloatField(null=True, blank=True)
    image_interval_unit = models.CharField(max_length=2, null=False, default='s')
    image_overlap = models.BooleanField(null=True, blank=True)
    observation_interval = models.FloatField(null=True, blank=True)
    observation_interval_unit = models.CharField(max_length=2, null=False, default='s')
    habitat_interval = models.FloatField(null=True, blank=True)
    habitat_interval_unit = models.CharField(max_length=2, null=False, default='s')
    fov_interval = models.FloatField(null=True, blank=True)
    fov_interval_unit = models.CharField(max_length=2, null=False, default='s')
    invertebrate_species = models.CharField(max_length=32, null=True, blank=True)
    fish_species = models.CharField(max_length=32, null=True, blank=True)
    algae_species = models.CharField(max_length=32, null=True, blank=True)
    biogenic_habitat = models.BooleanField(null=True, blank=True)
    habitat_only = models.BooleanField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False)
    updated_on = models.DateTimeField(default=timezone.now, null=False)

    class Meta:
        db_table = '"rov"."annotation_protocol"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class AnnotationJobAnnotationProtocol(models.Model):
    """
    Cross-reference table enables AnnotationJob to own AnnotationProtocols.
    """
    annotation_job = models.ForeignKey(AnnotationJob, related_name='annotation_protocols', null=False, on_delete=models.CASCADE)
    annotation_protocol = models.ForeignKey(AnnotationProtocol, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = '"rov"."annotation_job_annotation_protocol"'
        managed = False
        ordering = ('annotation_job', 'annotation_protocol')

    def __repr__(self):
        return f'{self.annotation_job.name} -> {self.annotation_protocol.name}'
    

class AnnotationJobRole(models.Model):
    """
    Roles available to crew members on an annotation job.
    """
    name = models.CharField(max_length=64, null=False, blank=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."annotation_job_role"'
        managed = False
        ordering = ('name',)


class AnnotationJobCrew(models.Model):
    """
    Associates crew members with an annotation job and their role.
    """
    annotation_job = models.ForeignKey(AnnotationJob, related_name='crew', null=False, on_delete=models.CASCADE)
    person = models.ForeignKey(shared.Person, null=False, on_delete=models.CASCADE)
    role = models.ForeignKey(AnnotationJobRole, null=False, on_delete=models.RESTRICT)

    class Meta:
        db_table = '"rov"."annotation_job_crew"'
        managed = False
        ordering = ('annotation_job', 'person', 'role')
        

class AnnotationProtocolDocument(models.Model):
    """
    Links a file or URL to an annotation protocol.
    """
    annotation_protocol = models.ForeignKey(AnnotationProtocol, related_name="documents", on_delete=models.CASCADE,
                                            null=False)
    file = models.ForeignKey(shared.UploadedFile, null=True, blank=True, on_delete=models.RESTRICT)
    title = models.CharField(max_length=256, null=False)
    note = models.TextField(null=True, blank=True)
    url = models.CharField(max_length=1024, null=True, blank=True)
    created_on = models.DateTimeField(default=timezone.now, null=False)
    updated_on = models.DateTimeField(default=timezone.now, null=False)
    
    class Meta:
        db_table = '"rov"."annotation_protocol_document"'
        managed = False
        ordering = ('annotation_protocol', 'title')

    def __repr__(self):
        return f'{self.annotation_protocol.name} -> {self.url or self.file_name}'


class Event(models.Model):
    """
    The superclass of observation, habitat, status, measurement and other future events. Contains
    information common to those event types.

    The shape, shape_area and frames fields are specific to Biigle.
    """
    dive = models.ForeignKey(Dive, related_name='events', on_delete=models.CASCADE, null=False)
    instrument_config = models.ForeignKey(InstrumentConfig, related_name='events', on_delete=models.CASCADE, null=True)
    annotation_job = models.ForeignKey(AnnotationJob, null=True, on_delete=models.RESTRICT)
    start_time = models.DateTimeField(null=False, default=timezone.now)
    end_time = models.DateTimeField(null=True, blank=True)
    properties = models.JSONField(null=False)
    created_on = models.DateTimeField(default=timezone.now, null=False, blank=True)
    updated_on = models.DateTimeField(default=timezone.now, null=False, blank=True)

    class Meta:
        db_table = '"rov"."event"'
        managed = False
        ordering = ('dive', 'start_time', 'end_time')

    def __repr__(self):
        return self.start_time


class EventLogger(models.Model):
    """
    The person or persons responsible for logging an event.
    """
    person = models.ForeignKey(shared.Person, on_delete=models.RESTRICT, null=False)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, null=False)

    class Meta:
        db_table = '"rov"."event_logger"'
        managed = False
        ordering = ('person', 'event')
        
    def __repr__(self):
        return f'{self.event.timestamp}: {self.person.last_name}, {self.person.first_name}'


class StatusType(models.Model):
    """
    Provides a description of the status type. This is a hierarchical labeling system:
    the top level describes things like on- or off-transect; the lower level gives
    a description of why the platform might be on- or off- transect.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."status_type"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class StatusTypeDetail(models.Model):
    """
    The lower level of the status hierarchy. Contains details to describe the status.
    """
    status_type = models.ForeignKey(StatusType, related_name='details', on_delete=models.RESTRICT, null=False)
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."status_type_detail"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return f'{self.status_type.name}: {self.detail}'


class Position(models.Model):
    """
    A machine-generated position.
	Accommodates geodetic as well as Cartesian positions.
	"""
    instrument_config = models.ForeignKey(InstrumentConfig, related_name='positions', on_delete=models.CASCADE, null=False)
    position_type = models.ForeignKey(PositionType, related_name='+', on_delete=models.RESTRICT, null=False)
    timestamp = models.DateTimeField(null=True, blank=True)
    signal_quality = models.FloatField(null=True, blank=True)
    is_modelled = models.BooleanField(default=False)
    geom = models.BinaryField(null=False)

    class Meta:
        db_table = '"rov"."position"'
        managed = False
        ordering = ('timestamp',)

    def __repr__(self):
        return f'Position: {self.timestamp}'


class Measurement(models.Model):
    """
    A machine-generated measurement such as velocity or depth.
    """
    instrument_config = models.ForeignKey(InstrumentConfig, related_name='measurements', on_delete=models.CASCADE, null=False)
    measurement_type = models.ForeignKey(MeasurementType, related_name='+', on_delete=models.RESTRICT, null=False)
    timestamp = models.DateTimeField(null=False)
    quantity = models.FloatField(null=False)
    signal_quality = models.FloatField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."measurement"'
        managed = False
        ordering = ('timestamp',)

    def __repr__(self):
        return f'Measurement: {self.measurement_type.name} ({self.quantity})'


