"""
This module stores lookup classes, most of which were inherited from VideoMiner.
They associated with Event and its subclases to describe habitats, observations, etc.
"""
from django.db import models

class Disturbance(models.Model):
    """
    Records the disturbance of substrate or biocover. Referenced by HabitatEvents.
    """
    name = models.CharField(max_length=64, null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."disturbance"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class ImageQuality(models.Model):
    """
    Records the current quality of imagery or video when an event was recorded.
    """
    name = models.CharField(max_length=128, null=False, unique=True)
    rank = models.IntegerField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."image_quality"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class Protocol(models.Model):
    """
    The survey protocol. This is an archaic entity used in VideoMiner databases
    and is distinct from the AnnotationProtocol.
    """
    name = models.CharField(max_length=128, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."protocol"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class SurveyMode(models.Model):
    """
    The current survey mode in use at the time an event is created.
    """
    name = models.CharField(max_length=128, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."survey_mode"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class Flow(models.Model):
    """
    Describes current/tidal flow. Linked by a HabitatEvent.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."flow"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class Relief(models.Model):
    """
    The relieve of a habitat. Attached to a HabitatEvent.
    """

    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."relief"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class Biocover(models.Model):
    """
    The biocover over a habitat. Attached to a HabitatEvent.
    """

    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."biocover"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class Thickness(models.Model):
    """
    Describes the thickness of a biocover. Attached to a HabitatEvent.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    minimum = models.FloatField(null=False)
    maximum = models.FloatField(null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."thickness"'
        managed = False
        ordering = ('minimum', 'maximum')

    def __repr__(self):
        return self.name


class Complexity(models.Model):
    """
    Describes the complexity of a substrate. Attached to a HabitatEvent.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."complexity"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


class Coverage(models.Model):
    """
    Describes the coverage of habitat by a biocover. Attached to a HabitatEvent.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    minimum = models.FloatField(null=False)
    maximum = models.FloatField(null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."coverage"'
        managed = False
        ordering = ('minimum', 'maximum')

    def __repr__(self):
        return self.name


class ObservationConfidence(models.Model):
    """
    Linked to an ObservationEvent to record the logger's confidence in the identification.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    rank = models.IntegerField(null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."observation_confidence"'
        managed = False
        ordering = ('rank', 'name',)

    def __repr__(self):
        return self.name


class Abundance(models.Model):
    """
    Indicates the abundance of an observation. Linked by ObservationEvents.
    """
    name = models.CharField(max_length=64, null=False, unique=True)
    rank = models.IntegerField(null=True, blank=True)
    source = models.CharField(max_length=64, null=False)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."abundance"'
        managed = False
        ordering = ('rank', 'name',)

    def __repr__(self):
        return self.name


class Substrate(models.Model):
    """
    A substrate. Linked by a HabitatEvent.
    """

    name = models.CharField(max_length=64, null=False, unique=True)
    note = models.TextField(null=True, blank=True)

    class Meta:
        db_table = '"rov"."substrate"'
        managed = False
        ordering = ('name',)

    def __repr__(self):
        return self.name


