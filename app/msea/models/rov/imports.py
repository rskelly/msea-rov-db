from datetime import datetime

from django.db import models
from django.utils import timezone
from django.db import models

from msea.models.shared import Restriction
from msea.models.rov import Cruise
from msea.models.shared import User

class CruiseImport(models.Model):
    """
    Stores the data for a cruise import and tracks its status and logs.
    Imports are processed by an external process that loads tasks from the database.
    """
    
    NOT_STARTED = 'Not Started'
    RUNNING = 'Running'
    FAILED = 'Failed'
    COMPLETED = 'Completed'
    CANCELED = 'Canceled'

    DEBUG = 'DEBUG'
    INFO = 'INFO'
    NOTICE = 'NOTICE'
    WARNING = 'WARN '
    ERROR = 'ERROR'

    version = models.IntegerField(null=False)
    name = models.CharField(max_length=512, null=False)
    data = models.JSONField(null=False)
    status = models.CharField(max_length=32, null=False)
    logs = models.JSONField(null=False)
    created_on = models.DateTimeField(default=datetime.now, null=False)
    updated_on = models.DateTimeField(default=datetime.now, null=False)

    def cancel(self):
        self.status = self.CANCELED
        self.save()

    def set_running(self):
        self.status = self.RUNNING
        self.save()

    def set_not_started(self):
        self.status = self.NOT_STARTED
        self.save()

    def set_failed(self):
        self.status = self.FAILED
        self.save()

    def set_completed(self):
        self.status = self.COMPLETED
        self.save()

    def is_canceled(self):
        return self.status == self.CANCELED
    
    def log(self, msg, level=INFO):
        logs = self.logs
        if logs == None or type(logs) != list:
            logs = []
        dt = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        msg = f'[{dt}][{level}]: {msg}'
        print(msg)
        logs.append(msg)
        self.logs = logs
        self.save()

    def clear_log(self):
        self.logs = []
        self.save()

    class Meta:
        db_table = '"rov"."cruise_import"'
        managed = False
        ordering = ('created_on',)

    def __repr__(self):
        return self.name        


class BiigleLabelMap(models.Model):
    """
    Maps Biigle labels to sets of attributes that are applied to event objects.
    """
    label_tree_id = models.IntegerField(null=False)
    label_id = models.IntegerField(null=False, unique=True)
    label_hierarchy = models.TextField(null=False)
    label_text = models.TextField(null=False)
    properties = models.JSONField(null=False)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField()
    updated_on = models.DateTimeField()
    
    restrictions = models.ManyToManyField(Restriction, through='BiigleLabelMapRestriction')

    class Meta:
        db_table = '"rov"."biigle_label_map"'
        managed = False
        ordering = ('label_text',)


class BiigleLabelMapRestriction(models.Model):
    """
    Links a label map item to a zero or more restrictions.
    """
    biigle_label_map = models.ForeignKey(BiigleLabelMap, null=False, on_delete=models.CASCADE)
    restriction = models.ForeignKey(Restriction, null=True, on_delete=models.RESTRICT)

    class Meta:
        db_table = '"rov"."biigle_label_map_restriction"'
        managed = False
        unique_together = ('biigle_label_map_id', 'restriction_id')
        ordering = ('biigle_label_map', 'restriction')


class GenericLabelMap(models.Model):
    """
    Maps generic labels to sets of attributes that are applied to event objects.
    """
    label_text = models.TextField(null=False, unique=True)
    properties = models.JSONField(null=False)
    note = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField()
    updated_on = models.DateTimeField()
    
    restrictions = models.ManyToManyField(Restriction, through='GenericLabelMapRestriction')

    class Meta:
        db_table = '"rov"."generic_label_map"'
        managed = False
        ordering = ('label_text',)


class GenericLabelMapRestriction(models.Model):
    """
    Links a label map item to a zero or more restrictions.
    """
    generic_label_map = models.ForeignKey(GenericLabelMap, null=False, on_delete=models.CASCADE)
    restriction = models.ForeignKey(Restriction, null=False, on_delete=models.RESTRICT)

    class Meta:
        db_table = '"rov"."generic_label_map_restriction"'
        managed = False
        unique_together = ('generic_label_map_id', 'restriction_id')
        ordering = ('generic_label_map', 'restriction',)

