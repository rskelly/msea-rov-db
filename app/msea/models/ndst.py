"""
These are models for the ndst schema which is used to store
NDST-related data copied from the shiny app's database.

The structure of these models and tables is dictated by the Shiny app, and 
should not be changed.
"""
import uuid
from django.db import models
from django.utils import timezone


class NDSTCruise(models.Model):
    """
    Represents the cruise.
    """
    row_id = models.CharField(max_length=36, null=False, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=64, null=False, unique=True)
    leg = models.CharField(max_length=4, null=False)
    objective = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=16, null=True, blank=True)
    active = models.BooleanField(default=True, null=False)
    hide = models.IntegerField(null=True, blank=True)
    created_on = models.DateTimeField(null=False, default=timezone.now)
    updated_on = models.DateTimeField(null=False, default=timezone.now)

    class Meta:
        db_table = '"ndst"."cruise"'
        managed = False
        ordering = ('name', 'leg')


class NDSTDiveConfig(models.Model):
    """
    Represents the configuration for a dive.
    """
    row_id = models.CharField(max_length=36, null=False, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=64, null=False, unique=True)
    ship_config = models.CharField(max_length=128, null=True, blank=True)
    sub_config = models.CharField(max_length=128, null=True, blank=True)
    ship_instrument_configs = models.TextField(null=True, blank=True)
    sub_instrument_configs = models.TextField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=False, null=False)
    created_on = models.DateTimeField(null=False, default=timezone.now)
    updated_on = models.DateTimeField(null=False, default=timezone.now)

    class Meta:
        db_table = '"ndst"."diveconfig"'
        managed = False
        ordering = ('name',)


class NDSTDive(models.Model):
    """
    Represents a dive.
    """
    row_id = models.CharField(max_length=36, null=False, unique=True, default=uuid.uuid4)
    cruise_name = models.CharField(max_length=64, null=False)
    leg = models.CharField(max_length=4, null=False)
    name = models.CharField(max_length=64, null=False)
    pilot = models.CharField(max_length=64, null=True)
    start_time = models.DateTimeField(null=False)
    end_time = models.DateTimeField(null=False)
    site_name = models.CharField(max_length=128, null=True, blank=True)
    dive_config = models.CharField(max_length=128, null=True, blank=True)
    objective = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=False, null=False)
    hide = models.IntegerField(null=True, blank=True)
    created_on = models.DateTimeField(null=False, default=timezone.now)
    updated_on = models.DateTimeField(null=False, default=timezone.now)

    class Meta:
        db_table = '"ndst"."dives"'
        managed = False
        ordering = ('cruise_name', 'leg', 'name')


class NDSTTransect(models.Model):
    """
    Represents a transect.
    """
    row_id = models.CharField(max_length=36, null=False, unique=True, default=uuid.uuid4)
    cruise_name = models.CharField(max_length=64, null=False)
    leg = models.CharField(max_length=4, null=False)
    dive_name = models.CharField(max_length=64, null=False)
    name = models.CharField(max_length=64, null=False)
    start_time = models.DateTimeField(null=False)
    end_time = models.DateTimeField(null=False)
    objective = models.TextField(null=True, blank=True)
    summary = models.TextField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=False, null=False)
    hide = models.IntegerField(null=True, blank=True)
    created_on = models.DateTimeField(null=False, default=timezone.now)
    updated_on = models.DateTimeField(null=False, default=timezone.now)

    class Meta:
        db_table = '"ndst"."transects"'
        managed = False
        ordering = ('cruise_name', 'leg', 'name')


class NDSTPerson(models.Model):
    """
    A person involved in the cruise.
    """
    row_id = models.CharField(max_length=36, null=False, unique=True, default=uuid.uuid4)
    person_id = models.IntegerField(null=False)
    initials = models.CharField(max_length=8, null=False, unique=True)
    first_name = models.CharField(max_length=64, null=False, unique=True)
    last_name = models.CharField(max_length=64, null=False, unique=True)
    email = models.CharField(max_length=256, null=False, unique=True)
    active = models.BooleanField(default=False, null=False)
    created_on = models.DateTimeField(null=False, default=timezone.now)
    updated_on = models.DateTimeField(null=False, default=timezone.now)

    class Meta:
        db_table = '"ndst"."people"'
        managed = False
        ordering = ('last_name', 'first_name')


class NDSTEquipment(models.Model):
    """
    A piece of equipment used on the cruise.
    """
    row_id = models.CharField(max_length=36, null=False, unique=True, default=uuid.uuid4)
    short_code = models.CharField(max_length=64, null=False, unique=True)
    short_code_mapped = models.CharField(max_length=64, null=True, blank=True)
    brand = models.CharField(max_length=64, null=False)
    model = models.CharField(max_length=64, null=False)
    serial_number = models.CharField(max_length=256, null=False)
    type = models.CharField(max_length=64, null=False)
    note = models.TextField(null=True, blank=True)
    instrument_id = models.IntegerField(null=True, blank=True)
    platform_id = models.IntegerField(null=True, blank=True)
    active = models.BooleanField(default=False, null=False)
    created_on = models.DateTimeField(null=False, default=timezone.now)
    updated_on = models.DateTimeField(null=False, default=timezone.now)

    class Meta:
        db_table = '"ndst"."equipment"'
        managed = False
        ordering = ('brand', 'model')


class NDSTEquipConfig(models.Model):
    """
    A configuration for equipment used on the cruise.
    """
    row_id = models.CharField(max_length=36, null=False, unique=True, default=uuid.uuid4)
    name = models.CharField(max_length=64, null=False, unique=True)
    short_code = models.CharField(max_length=64, null=False)
    type = models.CharField(max_length=64, null=True, blank=True)
    configuration = models.TextField(null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=False, null=False)
    created_on = models.DateTimeField(null=False, default=timezone.now)
    updated_on = models.DateTimeField(null=False, default=timezone.now)

    class Meta:
        db_table = '"ndst"."equipconfig"'
        managed = False
        ordering = ('name',)
