"""
The Taxon model from the taxonomy database, which stores authoritative taxonomic information.
Currently, WoRMS, iNaturalist and Hart.
"""

from django.db import models

class Taxon(models.Model):
    taxon_id = models.CharField(max_length=8, null=False)
    source = models.CharField(max_length=32, null=False)
    rank = models.CharField(max_length=64, null=True)
    scientific_name = models.CharField(max_length=256, null=False)
    common_name = models.CharField(max_length=256, null=False)
    superdomain = models.CharField(max_length=256, null=True)
    domain = models.CharField(max_length=256, null=True)
    kingdom = models.CharField(max_length=256, null=True)
    subkingdom = models.CharField(max_length=256, null=True)
    infrakingdom = models.CharField(max_length=256, null=True)
    phylum = models.CharField(max_length=256, null=True)
    phylum_division = models.CharField(max_length=256, null=True)
    subphylum_subdivision = models.CharField(max_length=256, null=True)
    subphylum = models.CharField(max_length=256, null=True)
    infraphylum = models.CharField(max_length=256, null=True)
    parvphylum = models.CharField(max_length=256, null=True)
    gigaclass = models.CharField(max_length=256, null=True)
    megaclass = models.CharField(max_length=256, null=True)
    superclass = models.CharField(max_length=256, null=True)
    vars()['class'] = models.CharField(max_length=256, null=True)
    subclass = models.CharField(max_length=256, null=True)
    infraclass = models.CharField(max_length=256, null=True)
    subterclass = models.CharField(max_length=256, null=True)
    superorder = models.CharField(max_length=256, null=True)
    order = models.CharField(max_length=256, null=True)
    suborder = models.CharField(max_length=256, null=True)
    infraorder = models.CharField(max_length=256, null=True)
    parvorder = models.CharField(max_length=256, null=True)
    superfamily = models.CharField(max_length=256, null=True)
    family = models.CharField(max_length=256, null=True)
    subfamily = models.CharField(max_length=256, null=True)
    supertribe = models.CharField(max_length=256, null=True)
    tribe = models.CharField(max_length=256, null=True)
    subtribe = models.CharField(max_length=256, null=True)
    genus = models.CharField(max_length=256, null=True)
    genus_hybrid = models.CharField(max_length=256, null=True)
    subgenus = models.CharField(max_length=256, null=True)
    section = models.CharField(max_length=256, null=True)
    subsection = models.CharField(max_length=256, null=True)
    series = models.CharField(max_length=256, null=True)
    species = models.CharField(max_length=256, null=True)
    hybrid = models.CharField(max_length=256, null=True)
    subspecies = models.CharField(max_length=256, null=True)
    natio = models.CharField(max_length=256, null=True)
    variety = models.CharField(max_length=256, null=True)
    subvariety = models.CharField(max_length=256, null=True)
    form = models.CharField(max_length=256, null=True)
    subform = models.CharField(max_length=256, null=True)
    no_common_name = models.BooleanField(default=False, null=False)
    accepted_taxon_id = models.IntegerField(null=True)
    parent_taxon_id = models.IntegerField(null=True)

    class Meta:
        db_table = '"taxonomy"."taxon"'
        managed = False
        ordering = ('scientific_name',)

    def __repr__(self):
        return self.scientific_name