import re
import string
import random
from datetime import datetime, timedelta, timezone
from django.apps import apps
from django.conf import settings
from django.db import models
from django.contrib import admin
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from django.contrib.auth import models as auth_models

# Regex to check username validity.
uname_re = re.compile(r'^[0-9a-zA-z][\w!@#$%^&*_+=-~]?')
# Regex to check email validity.
email_re = re.compile(r'^[^\s@]+@[^\s@]+\.[^\s@]+$')

class UserManager(BaseUserManager):
    """
    Creates users and superusers.
    """
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given email and password.
        Uses the User model configured in this file.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        # Lookup the real model class from the global app registry so this
        # manager method can be used in migrations. This is fine because
        # managers are by definition working on the real model.
        GlobalUserModel = apps.get_model(
            self.model._meta.app_label, self.model._meta.object_name
        )
        user = self.model(email=email, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, email, password=None, **extra_fields):
        """
        Create a user.
        """
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        """
        Create a superuser.
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


class User(auth_models.AbstractBaseUser, auth_models.PermissionsMixin):
    """
    A custom User model.
    """
    # Validation status constants. TODO: Use these to access i18n dictionary for messages.
    ALREADY_VERIFIED = 'already_verified'
    EMAIL_BAD_FORMAT = 'email_bad_format'
    EMAIL_EMPTY = 'email_empty'
    INVALID_EMAIL = 'password_invalid_email'            # The email does not belong to a user.
    INVALID_PASSWORD_CODE = 'password_invalid_code'
    LOGGED_IN = 'logged_in'
    LOGIN_FAILED = 'login_failed'
    MAX_TOKENS_EXCEEDED = 'max_tokens_exceeded'
    NOT_LOGGED_IN = 'not_logged_in'
    PASSWORD_DISALLOWED = 'password_disallowed'
    PASSWORD_EMPTY = 'password_empty'
    PASSWORD_FEW_UNIQUE = 'password_few_unique'
    PASSWORD_MISMATCH = 'password_mismatch'
    PASSWORD_SHORT = 'password_short'
    REGISTRATION_SUCCEEDED = 'registration_succeeded'
    PASSWORD_RESET_EXPIRED = 'password_reset_expired'
    PASSWORD_RESET_SEND_SUCCEEDED = 'password_reset_send_succeeded'
    PASSWORD_RESET_SUCCEEDED = 'password_reset_succeeded'
    PASSWORD_RESET_FAILED = 'password_reset_failed'
    PASSWORD_RESET_SEND_FAILED = 'password_reset_send_failed'
    USER_EXISTS = 'user_exists'
    VERIFICATION_EXPIRED = 'verification_expired'
    VERIFICATION_FAILED = 'verification_failed'
    VERIFICATION_PENDING = 'verification_pending'
    VERIFICATION_SENT = 'verification_sent'
    VERIFICATION_SUCCEEDED = 'verification_succeeded'
    VERIFICATION_FAILED = 'verification_failed'
    VERIFICATION_SEND_FAILED = 'verification_send_failed'

    # The field to be used as a username. Overrides the Django default, which is a separate username.
    USERNAME_FIELD = 'email'

    # The custom UserManager for creating users.
    objects = UserManager()

    password = models.CharField(max_length=128)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254, unique=True)
    bio = models.TextField(null=True, blank=True)

    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    is_superuser = models.BooleanField()
    date_joined = models.DateTimeField()
    last_login = models.DateTimeField(null=True, blank=True)

    # The organization to which the user belongs.
    organization = models.CharField(max_length=128, null=True, blank=True)
    # The type of organization to which the user belongs.
    org_type = models.CharField(max_length=32, null=True, blank=True)
    # The user's reason for registering.
    registration_reason = models.TextField(null=True)

    # The user's Biigle username.
    biigle_username = models.CharField(max_length=32, null=True, blank=True)
    # The user's Biigle API key.
    biigle_api_key = models.CharField(max_length=32, null=True, blank=True)

    # A code to be used for resetting the user's password.
    password_reset_code = models.CharField(max_length=32, null=True, blank=True)
    # An expiry date and time for the password reset code.
    password_reset_expiry = models.DateTimeField(null=True, blank=True)

    # A code to be used for verifying the user's registration.
    verification_code = models.CharField(max_length=32, null=True, blank=True)
    # An expiry date and time for the user's verification.
    verification_expiry = models.DateTimeField(null=True, blank=True)
    # The time that verification was completed.
    verification_time = models.DateTimeField(null=True, blank=True)
    # The IP from which the user has registered.
    registration_ip = models.CharField(max_length=64, null=True, blank=True)
    # The location from which the user has registered. Unreliable.
    registration_location = models.CharField(max_length=256, null=True, blank=True)
    # If the IP's associated region is in the "allowed" region, this is true.
    ip_in_region = models.BooleanField()
    # A note accompanying the registration.
    registration_note = models.TextField(null=True, blank=True)
    # If true, the user is allowed to log in.
    allowed = models.BooleanField(default=True, null=False)

    groups = models.ManyToManyField(auth_models.Group, through='UserGroup')

    def sign_in_allowed(self):
        """
        Return true if the user is verified and if the allowed flag is True.
        """
        return self.is_verified() and self.allowed
    
    def is_verified(self):
        """
        Return true if the user is verified (by clicking on a verification link).
        """
        return self.verification_time is not None
    
    def verify(self, code=None): 
        """
        Verify the user. If the given code matches and the expiry isn't passed,
        set the verification date and save. Return true on success, false otherwise.

        Note: the verification code is not deleted unless it is replaced. We want to be able
        to tell the user if they've already verified to go log in, not just return an error.
        """
        code = code.strip() if code else ''
        if self.verification_code == code and not self.is_verification_expired():
            self.verification_time = datetime.now(tz=timezone.utc)
            self.verification_expiry = None
            self.allowed = True
            self.is_active = True
            self.save()
            return True
        return False
    
    def is_verification_expired(self):
        """
        Return true if the verification link has expired without being clicked to verify the account.
        If the user is already verified, returns false.
        If there is no verification expiry date, returns false.
        """
        return self.is_verified() is not True \
            and self.verification_expiry is not None \
            and self.verification_expiry.replace(tzinfo=timezone.utc) < datetime.now(tz=timezone.utc)
    
    def is_verification_pending(self):
        """
        Return true if the user is not verified, there is a verification date and the verification 
        date has not passed.
        """
        return self.is_verified() is not True \
            and self.verification_expiry is not None \
            and self.verification_expiry.replace(tzinfo=timezone.utc) >= datetime.now(tz=timezone.utc)

    def generate_verification_code(self):
        """
        Generates a verification code and expiry date and return a tuple containing both.
        """
        try:
            delta = float(settings.MSEA_VERIFICATION_EXPIRY_HOURS)
        except:
            delta = 1
        self.verification_code = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
        self.verification_expiry = datetime.now(tz=timezone.utc) + timedelta(hours=delta)
        self.verification_time = None
        self.save()

        return self.verification_code, self.verification_expiry

    def reset_password(self, pw, pw2):
        """
        Reset the password and remove the reset code and expiry.
        """
        valid, status = User.validate_password(pw, pw2, self)
        if valid:
            super().set_password(pw)
            self.password_reset_code = None
            self.password_reset_expiry = None
            self.save()

    def password_reset_expired(self):
        """
        Return true if the password reset link has expired without being clicked.
        If there is no verification expiry date, returns false.
        """
        return self.verification_expiry is not None \
            and self.verification_expiry.replace(tzinfo=timezone.utc) < datetime.now(tz=timezone.utc)

    def generate_password_reset_code(self):
        """
        Generates a verification code and expiry date and return a tuple containing both.
        """
        try:
            delta = float(settings.MSEA_PASSWORD_RESET_EXPIRY_HOURS)
        except: 
            delta = 1
        self.password_reset_code = ''.join(random.choices(string.ascii_lowercase + string.digits, k=32))
        self.password_reset_expiry = datetime.now(tz=timezone.utc) + timedelta(hours=delta)
        self.save()

        return self.verification_code, self.verification_expiry

    def is_admin(self):
        """
        Return true if the user is in the msea_admin group
        """
        return len(self.groups.filter(name='msea_admin')) > 0
    
    def is_importer(self):
        """
        Return true if the user is in the msea_importer group
        """
        return len(self.groups.filter(name='msea_importer')) > 0

    @staticmethod
    def validate_email(email, validation={}):
        """
        Validate the email address.
        1) Check that it's not empty.
        2) Check that it matches the regex.
        """
        email = email.strip() if email else ''
        status = None

        if len(email) == 0:
            validation['email'] = 'No email given.'
            status = User.EMAIL_EMPTY
        elif not email_re.match(email):
            validation['email'] = 'An email must be in accepted format.'
            status = User.EMAIL_BAD_FORMAT

        return status is None, status
   
    @staticmethod   
    def validate_password(password, password_confirm, user, validation={}):
        """
        Validate the user's password.
        1) Make sure the password isn't empty and is at least 8 characters.
        2) Make sure the password and confirmation match.
        3) Check that the password has enough unique characters.
        4) Check that the password isn't equal to and doesn't contain values from other fields.
        5) Return a tuple containing True and None if the status is none (there is no error), or False and the status if there is an error.
        """
        password = password.strip() if password else ''
        password_confirm = password_confirm.strip() if password_confirm else ''
        status = None

        # Check password length.
        if not password or len(password) < 8:
            validation['password'] = 'Your password must be at least 8 characters long.'
            status = User.PASSWORD_SHORT

        # Check that the passwords are equal.
        if password != password_confirm:
            validation['password'] = 'Your passwords do not match.'
            status = User.PASSWORD_MISMATCH

        # Check for a sufficient number of unique characters.
        if not status:
            # Check the number of unique characters.
            pw_check = {}
            for i in range(len(password)):
                l = password[i]
                if not l in pw_check.keys():
                    pw_check[l] = 0
                pw_check[l] += 1
            if len(pw_check) <= 3:
                validation['password'] = 'Your password has too few unique characters.'
                status = User.PASSWORD_FEW_UNIQUE

        # Check for disallowed values from other fields.
        if not status:
            # Check for disallowed contents.
            # A list of strings that cannot be in the password.
            # Search is case-insensitive.
            test = password.lower()
            # Check for the presence of items equal to the password.
            for v in list(map(str.lower, User.get_password_prohibited_values(user))):
                if test == v:
                    validation['password'] = 'Your password contains disallowed text.'
                    status = User.PASSWORD_DISALLOWED
                    break

        # TODO: Check most common passwords.
    
        return status is None, status
    
    @staticmethod
    def get_password_prohibited_values(user):
        """
        Returns a list of the fields of values that cannot appear as substrings in a password.
        """
        utype = type(user)
        if utype == User:
            return list(filter(lambda a: a is not None, (getattr(user, k) for k in ('email', 'first_name', 'last_name', 'organization'))))
        elif utype == dict:
            return list(filter(lambda a: a is not None, (user.get(k) for k in ('email', 'first_name', 'last_name', 'organization'))))
        else:
            raise Exception('Parameter user must be of type User or dict.')
        
    @staticmethod
    def validate_user_data(user):
        """
        Validate the user. Based on the clilent-side Validation class.
        :param user: A dict containing user data.
        :return: A tuple containing true or false if the check succeeded or failed, and the validation dict.
        """
        # Stores error messages for fields.
        validation = {}

        # Validate simple required properties.
        fields = [
            ('first_name', 'Please give your first name.'), 
            ('last_name', 'Please give your last name.'), 
            ('organization', 'Please give the name of your organization.'),
            ('org_type', 'Please select the type of organization or "other".'),
            ('registration_reason', 'Please tell us why you\'d like to access this data.')
        ]
        for p, msg in fields:
            v = user.get(p)
            if v is None: v = ''
            v = v.strip()
            if len(v) == 0:
                validation[p] = msg
        
        # Validate email.
        User.validate_email(user.get('email'), validation)

        # Validate password.
        User.validate_password(user.get('password'), user.get('password_confirm'), user, validation)

        # Try to retrieve the user. If it exists, the email is in use.
        try:
            user = User.objects.get(email__iexact=user.get('email'))
            if user:
                validation['email'] = 'This email is already in use.'
        except: pass

        return validation

    class Meta:
        db_table = '"public"."auth_user"'
        managed = False
        ordering = ('last_name', 'first_name')

    def __repr__(self):
        return '{}, {}'.format(self.last_name, self.first_name)

admin.site.register(User)


class UserGroup(models.Model):
    """
    Joins users to groups.
    """
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    group = models.ForeignKey(auth_models.Group, null=False, on_delete=models.CASCADE)

    class Meta:
        db_table = '"public"."auth_user_groups"'
        managed = False
        ordering = ('user', 'group')
        