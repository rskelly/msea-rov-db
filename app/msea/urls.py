
import os

from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework_nested import routers
# from drf_spectacular.views import get_schema_view
# from drf_spectacular import openapi

import knox.views as knox

from msea.views.rov import experiments
from msea.views import shared, rov, auth, ndst, contact
from msea.views.rov import biigle, search, ui, imports
from msea.views.rov.lookups import *
    

# Root path of the API.
#API = os.environ.get('MSEA_BASE_DIR') + '/api/v1/'
API = 'api/v1/'

# Metadata about the API.
# schema_view = get_schema_view(
#    openapi.Info(
#       title="MSEA ReST API",
#       default_version='v1',
#       description="ReST Endpoints for the marine spatial ecology analysis section of Fisheries and Oceans Canada",
#       terms_of_service="",
#       contact=openapi.Contact(email="robert.skelly@dfo-mpo.gc.ca"),
#       license=openapi.License(name="BSD License"),
#    ),
#    public=True,
#    permission_classes=(permissions.AllowAny,),
# )

# This is the list of routers for ReST API endpoints in the system.
router = routers.DefaultRouter()
router.register(r'account', auth.UserViewSet, basename='account')

router.register(r'biigle_label_map', imports.BiigleLabelMapViewSet, basename='biigle_label_map')
router.register(r'generic_label_map', imports.GenericLabelMapViewSet, basename='generic_label_map')
router.register(r'biigle_api', biigle.BiigleViewSet, basename='biigle_api')

router.register(r'contact', contact.ContactViewSet, basename='contact')

router.register(r'cruise_imports', imports.CruiseImportViewSet, basename='cruise_imports')

router.register(r'abundances', AbundanceViewSet, basename='abundances')
router.register(r'biocovers', BiocoverViewSet, basename='biocovers')
router.register(r'complexities', ComplexityViewSet, basename='complexities')
router.register(r'flows', FlowViewSet, basename='flows')
router.register(r'observation_confidences', ObservationConfidenceViewSet, basename='observation_confidences')
router.register(r'coverages', CoverageViewSet, basename='coverages')
router.register(r'disturbances', DisturbanceViewSet, basename='disturbances')
router.register(r'image_qualities', ImageQualityViewSet, basename='image_qualities')
router.register(r'protocols', ProtocolViewSet, basename='protocols')
router.register(r'reliefs', ReliefViewSet, basename='reliefs')
router.register(r'substrates', SubstrateViewSet, basename='substrates')
router.register(r'survey_modes', SurveyModeViewSet, basename='survey_modes')
router.register(r'thicknesses', ThicknessViewSet, basename='thicknesses')

router.register(r'ndst/cruises', ndst.CruiseViewSet, basename='ndst/cruises')
router.register(r'ndst/people', ndst.PersonViewSet, basename='ndst/people')
router.register(r'ndst/dives', ndst.DiveViewSet, basename='ndst/dives')
router.register(r'ndst/dive_configs', ndst.DiveConfigViewSet, basename='ndst/dive_configs')
router.register(r'ndst/transects', ndst.TransectViewSet, basename='ndst/transects')
router.register(r'ndst/equipment', ndst.EquipmentViewSet, basename='ndst/equipment')
router.register(r'ndst/equipment_configs', ndst.EquipConfigViewSet, basename='ndst/equipment_configs')
router.register(r'ndst/transfer', ndst.TransferViewSet, basename='ndst/transfer')

router.register(r'dive_tracks', rov.DiveTrackViewSet, basename='dive_tracks')
router.register(r'annotation_software', rov.AnnotationSoftwareViewSet, basename='annotation_software')
router.register(r'annotation_protocols', rov.AnnotationProtocolViewSet, basename='annotation_protocols')
router.register(r'annotation_jobs', rov.AnnotationJobViewSet, basename='annotation_jobs')
router.register(r'annotation_job_crew', rov.AnnotationJobCrewViewSet, basename='annotation_job_crew')
router.register(r'annotation_job_roles', rov.AnnotationJobRoleViewSet, basename='annotation_job_roles')
router.register(r'annotation_protocol_documents', rov.AnnotationProtocolDocumentViewSet,
                basename='annotation_protocol_documents')
router.register(r'cruise_documents', rov.CruiseDocumentViewSet,
                basename='cruise_documents')
router.register(r'programs', rov.ProgramViewSet, basename='programs')
router.register(r'program_roles', rov.ProgramRoleViewSet, basename='program_roles')
router.register(r'program_members', rov.ProgramMemberViewSet, basename='program_members')
router.register(r'measurement_types', rov.MeasurementTypeViewSet, basename='measurement_types')
router.register(r'position_types', rov.PositionTypeViewSet, basename='position_types')
router.register(r'models', rov.ModelViewSet, basename='models')
router.register(r'platforms', rov.PlatformViewSet, basename='platforms')
router.register(r'cruises', rov.CruiseViewSet, basename='cruises')
router.register(r'cruise_summary', rov.CruiseSummaryViewSet, basename='cruise_summary')
router.register(r'cruise_roles', rov.CruiseRoleViewSet, basename='cruise_roles')
router.register(r'cruise_crews', rov.CruiseCrewViewSet, basename='cruise_crews')
router.register(r'dives', rov.DiveViewSet, basename='dives')
router.register(r'transects', rov.TransectViewSet, basename='transects')
router.register(r'dive_roles', rov.DiveRoleViewSet, basename='dive_roles')
router.register(r'dive_crews', rov.DiveCrewViewSet, basename='dive_crews')
router.register(r'platform_configs', rov.PlatformConfigViewSet, basename='platform_configs')
router.register(r'medium_formats', rov.MediumFormatViewSet, basename='medium_formats')
router.register(r'medium_types', rov.MediumTypeViewSet, basename='medium_types')
router.register(r'status_types', rov.StatusTypeViewSet, basename='status_types')
router.register(r'status_type_details', rov.StatusTypeDetailViewSet, basename='status_type_details')
router.register(r'instruments', rov.InstrumentViewSet, basename='instruments')
router.register(r'instrument_configs', rov.InstrumentConfigViewSet, basename='instrument_configs')
router.register(r'events', rov.EventViewSet, basename='events')
router.register(r'measurements', rov.MeasurementViewSet, basename='measurements')
router.register(r'positions', rov.PositionViewSet, basename='positions')
router.register(r'equipment_types', rov.EquipmentTypeViewSet, basename='equipment_types')

router.register(r'text_search', search.TextSearchViewSet, basename='text_search')
router.register(r'obs_search', search.ObservationSearchViewSet, basename='obs_search')

router.register(r'people', shared.PersonViewSet, basename='people')
router.register(r'sites', shared.SiteViewSet, basename='sites')
router.register(r'restrictions', shared.RestrictionViewSet, basename='restrictions')
router.register(r'organisations', shared.OrganisationViewSet, basename='organisations')
router.register(r'uploads', shared.UploadedFileViewSet, basename='uploads')
router.register(r'downloads', shared.DownloadFileViewSet, basename='downloads')

router.register(r'rov_home', ui.ROVHomeViewSet, basename='rov_home')

router.register(r'experiments/obs_measurement', experiments.ObservationMeasurementViewSet, basename='obs_measurement')
router.register(r'experiments/taxon_search', experiments.TaxonSearchViewSet, basename='taxon_search')

cruise_dive_router = routers.NestedSimpleRouter(router, 'cruises', lookup='cruises')
cruise_dive_router.register(r'dives', rov.DiveViewSet, basename='dives')
cruise_crew_router = routers.NestedSimpleRouter(router, 'cruises', lookup='cruises')
cruise_crew_router.register(r'crews', rov.CruiseCrewViewSet, basename='crews')
dive_crew_router = routers.NestedSimpleRouter(router, 'dives', lookup='dives')
dive_crew_router.register(r'crews', rov.DiveCrewViewSet, basename='crews')
dive_event_router = routers.NestedSimpleRouter(router, 'dives', lookup='dives')
dive_event_router.register(r'events', rov.EventViewSet, basename='events')
transect_router = routers.NestedSimpleRouter(router, 'dives', lookup='dives')
transect_router.register(r'transects', rov.TransectViewSet, basename='transects')
instrument_router = routers.NestedSimpleRouter(router, 'dives', lookup='dives')
instrument_router.register(r'instruments', rov.InstrumentViewSet, basename='instruments')
instrument_config_instrument_router = routers.NestedSimpleRouter(router, 'instrument_configs', lookup='instrument_configs')
instrument_config_instrument_router.register(r'instruments', rov.InstrumentViewSet, basename='instruments')
annotation_protocol_document_router = routers.NestedSimpleRouter(router, 'annotation_protocols', lookup='annotation_protocols')
annotation_protocol_document_router.register(r'documents', rov.AnnotationProtocolDocumentViewSet, basename='documents')
status_type_detail_router = routers.NestedSimpleRouter(router, 'status_types', lookup='status_types')
status_type_detail_router.register(r'details', rov.StatusTypeDetailViewSet, basename='details')
cruise_program_router = routers.NestedSimpleRouter(router, 'cruises', lookup='cruises')
cruise_program_router.register(r'programs', rov.ProgramViewSet, basename='programs')
cruise_document_router = routers.NestedSimpleRouter(router, 'cruises', lookup='cruises')
cruise_document_router.register(r'documents', rov.CruiseDocumentViewSet, basename='documents')
cruise_taxon_router = routers.NestedSimpleRouter(router, 'cruises', lookup='cruises')
cruise_taxon_router.register(r'taxa', shared.EventTaxonViewSet, basename='taxa')
dive_meas_router = routers.NestedSimpleRouter(router, 'dives', lookup='dives')
dive_meas_router.register(r'measurements', rov.MeasurementViewSet, basename='measurements')
dive_pos_router = routers.NestedSimpleRouter(router, 'dives', lookup='dives')
dive_pos_router.register(r'positions', rov.PositionViewSet, basename='positions')

group_restriction_router = routers.NestedSimpleRouter(router, 'restrictions', lookup='restrictions')
group_restriction_router.register(r'users', shared.RestrictionGroupViewSet, basename='users')

urlpatterns = [

   # Auth
   # Uses the knox views for logout and logoutall, a custom view for login.

   path(f'{API}account/login/', auth.LoginView.as_view(), name="login"),
   path(f'{API}account/logout/', auth.LogoutView.as_view(), name="logout"),
   path(f'{API}account/groups/', auth.GroupViewSet.as_view({'get': 'list'}), name="groups"),
   path(f'{API}account/logoutall/', knox.LogoutAllView.as_view(), name="logoutall"),

   # Data Services

   path(API, include(router.urls)),
   path(API, include(cruise_dive_router.urls)),
   path(API, include(cruise_crew_router.urls)),
   path(API, include(cruise_program_router.urls)),
   path(API, include(cruise_document_router.urls)),
   path(API, include(cruise_taxon_router.urls)),
   path(API, include(dive_crew_router.urls)),
   path(API, include(dive_meas_router.urls)),
   path(API, include(dive_pos_router.urls)),
   path(API, include(dive_event_router.urls)),
   path(API, include(transect_router.urls)),
   path(API, include(instrument_router.urls)),
   path(API, include(instrument_config_instrument_router.urls)),
   path(API, include(annotation_protocol_document_router.urls)),
   path(API, include(status_type_detail_router.urls)),
   path(API, include(group_restriction_router.urls)),
   path(API, include('rest_framework.urls', namespace='rest_framework')),

   # Event lookups
   re_path(fr'^lookups/(?P<type_name>.+)/$', shared.csv_lookup, name='lookups'),

   # Serve various file types.
   path('thumbnail', shared.thumbnail, name='thumbnail'),
   
   #path(f'{API}docs/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    path('/rov/site_admin/', admin.site.urls),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

