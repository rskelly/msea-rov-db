from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe
import json
import base64

register = template.Library()


@register.filter(name='user_groups')
def user_groups(user):
    """
	Returns the list of user groups.
	"""
    return [x.name for x in user.groups.all()]


@register.filter(name='is_admin')
def is_admin(user):
    """
	Returns true if the user has a group whose name ends in '_admin'.
	"""
    return user.groups.filter(name='msea_admin').exists()


@register.filter(name='truncate')
def truncate(txt, title, length=100):
    """
	If the txt is longer than length, truncate and add a link to the 
	ellipsis to pop up a text window.

	The message text is base64 encoded to pass it safely into the 
	javascript function, where it is decoded.
	"""
    if length <= 0:
        length = 100
    if not txt:
        return ''
    if len(txt) > length:
        return '<span>{}<a href="javascript:void(0)" onclick="textPopup(\'{}\', \'{}\')">...</a>'.format(
            txt[:length],
            title,
            base64.b64encode(bytes(txt, encoding='utf-8')).decode('utf-8')
        )
    return txt


@register.filter(name='truncate25')
def truncate25(txt, title):
    return truncate(txt, title, 25)


@register.filter(name='truncate50')
def truncate50(txt, title):
    return truncate(txt, title, 50)


@register.filter(name='coalesce')
def coalesce(lst, delim=','):
    """
	Concatenates a list of items, returns a string with their __str__ values.
	Excludes null elements.
	"""
    if not lst:
        return ''
    return delim.join([str(a) if a else '' for a in lst])


@register.filter(name='mendeley_authors')
def mendeley_authors(value, delim):
    """
	Returns a concatenated list of mendeley authors.
	"""
    return delim.join([x['name'] for x in value])


@register.filter(name='split')
@stringfilter
def split(value, delim):
    return value.split(delim) if value else ''


@register.filter(name='datalist')
@stringfilter
def datalist(value):
    lines = []
    try:
        lst = json.loads(value)
        for item in lst:
            lines.append('<option value="{}" />'.format(item))
    except:
        pass
    return '\n'.join(lines)


@register.filter(name='navigator', is_safe=True)
def navigator(value):
    output = []
    pages = value.get('pages')
    del value['pages']

    if value['page'] > 0:
        v = value.copy()
        v['page'] = 0
        params = '&'.join(['{}={}'.format(x, y or '') for x, y in v.items()])
        output.append('<a href="?{}"> << </a>&nbsp;'.format(params))

    if value['page'] > 0:
        v = value.copy()
        v['page'] = v['page'] - 1
        params = '&'.join(['{}={}'.format(x, y or '') for x, y in v.items()])
        output.append('<a href="?{}"> < </a>&nbsp;'.format(params))

    for p in pages:
        v = value.copy()
        v['page'] = p
        params = '&'.join(['{}={}'.format(x, y or '') for x, y in v.items()])
        output.append('<a href="?{}">{}</a>&nbsp;'.format(params, (p + 1)))

    if value['page'] < value['last']:
        v = value.copy()
        v['page'] = v.get('page', 1) + 1
        params = '&'.join(['{}={}'.format(x, y or '') for x, y in v.items()])
        output.append('<a href="?{}"> > </a>&nbsp;'.format(params))

    if value['page'] < value['last']:
        v = value.copy()
        v['page'] = value['last']
        params = '&'.join(['{}={}'.format(x, y or '') for x, y in v.items()])
        output.append('<a href="?{}"> >> </a>'.format(params))

    return mark_safe(''.join(output))
