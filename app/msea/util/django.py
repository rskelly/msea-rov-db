"""
Set up the Django environment so models can be used.
"""

import os
import sys
import django

from msea.util import util

def django_setup(config_file):
	"""
	Setup a Django environment so that the ORM system can be used outside of the
	Django app.

	The config_file argument is the path of one of the configuration files.
	"""

	if not config_file:
		raise Exception('A configuraton file is required.')
		
	# Load the configuration.
	config = util.load_config(config_file)

	# Add the project to the search path.
	sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
	# Make the configuration path available as an environment variable. When django.setup() is called
	# the settings file will use this to load its configuration.
	os.environ['MSEA_CONFIG_PATH'] = config_file
	# Set the name of the settings module from the MSEA_MODE parameter in the config file.
	# django.setup() will use this to determine which settings file to use.
	# TODO: It may be possible to go back to one settings file and eliminate this redundancy.
	os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings.{}'.format(config['MSEA_MODE'])
	# Set up django.
	django.setup()
