"""
These are utility classes and functions used in scripts.
"""

import os
import cv2
import sys
import math
import uuid
import json
import base64
import hashlib
import logging
import mimetypes
import shutil
import ipaddress
import psycopg2
import numpy as np
from itertools import chain
from urllib import request
from zipfile import ZipFile
from tempfile import mkdtemp, mkstemp, gettempdir
from datetime import datetime, timezone, timedelta, date
from string import Template
from django.core.mail import EmailMessage, get_connection


def gen_uuid():
    """
	Generate a random UUID.
	"""
    return uuid.uuid4()


def check_int(v):
    """
	Return the int value or None if the value is not representable as an int.
	"""
    try:
        return int(v)
    except:
        return None


def check_float(v):
    """
	Return the float value or None if the value is not representable as a float.
	"""
    try:
        return float(v)
    except:
        return None


def check_na(v):
    """
	Some spreadsheets annoyingly use 0 or NA to indicate empty cells.
	"""
    return not v or v.lower() in ('na', 'n/a', 'null')


def _coalesce(*args):
    for arg in args:
        if arg:
            return arg
    return None


class as_date:
    """
	Functor, used to convert a string to a date object.
	"""

    def __init__(self, fmt, tz=0):
        """
		Construct with the give string format used by strptime.
		"""
        self.fmt = fmt
        self.tz = tz

    def __call__(self, v):
        """ Return the date. """
        return date.strptime(v, self.fmt).replace(tzinfo=timezone(timedelta(hours=self.tz)))


class as_datetime:
    """
	Functor, used to convert a string to a datetime object.
	"""

    def __init__(self, fmt, tz=0):
        """
		Construct with the give string format used by strptime.
		"""
        self.fmt = fmt
        self.tz = tz

    def __call__(self, v):
        """ Return the datetime. """
        return datetime.strptime(v, self.fmt).replace(tzinfo=timezone(timedelta(hours=self.tz)))


class is_date:
    """
	Functor, tries to parse the date with the given format and return true on success.
	"""

    def __init__(self, fmt):
        """
		Construct with the give string format used by strptime.
		"""
        self.conv = as_date(fmt)

    def __call__(self, v):
        try:
            return False if self.conv(v) is None else True
        except Exception as e:
            logging.warning(str(e))
        return False


class is_datetime:
    """
	Functor, tries to parse the datetime with the given format and return true on success.
	"""

    def __init__(self, fmt):
        """ Construct with a date format. """
        self.conv = as_datetime(fmt)

    def __call__(self, v):
        """ Return true if the string can be parsed as a datetime. """
        try:
            return False if self.conv(v) is None else True
        except Exception as e:
            logging.warning(str(e))
        return False


def is_dd_coordinate(x, y):
    """
	Return True if x and y comprise a geographic coordinate in decimal degrees. 
	"""
    lat = in_range(-90., 90)
    lon = in_range(-180., 180.)
    if is_float(x) and is_float(y):
        if lat(y) and lon(x):
            return True
    return False


def is_float(v):
    """
	Return true if the value can be interpreted a float.
	"""
    try:
        v = float(v)
        return True
    except Exception as e:
        logging.warning(str(e))
        return False


def is_int(v):
    """
	Return true if the value can be interpreted an int.
	"""
    try:
        v = int(v)
        return True
    except Exception as e:
        logging.warning(str(e))
        return False


def is_uint(v):
    """
	Return true if the value can be interpreted an unsigned int.
	"""
    return is_int(v) and int(v) >= 0


class is_equal:
    """
	Functor, returns true if the value is equal to the configued value.
	"""

    def __init__(self, e):
        """
		e is the check value.
		"""
        self.e = e

    def __call__(self, v):
        return self.e == v


class in_range:
    """
	Functor, returns true if the value is between min and max. Implies
	that the value is a numeric type. Returns false if not.
	"""

    def __init__(self, min, max):
        """ Configure with the min and max. """
        self.min = min
        self.max = max

    def __call__(self, v):
        """ Return true if the value is a number and within range. """
        if not is_float(v) and not is_int(v):
            return False
        v = float(v)
        return v >= self.min and v <= self.max


class equals_filter:
    """
	functor, returns true if the column value equals the match value.
	"""

    def __init__(self, column, match):
        self.column = column
        self.match = match

    def __call__(self, header, row):
        """ Apply the matcher function to the value and return the result. """
        return self.match == row[header.index(self.column)]


class Filter:

    def __init__(self, column, f):
        self.column = column
        self.f = f if isinstance(f, list) else [f]

    def __call__(self, header, row):

        # If the filter is a list of filters, apply them in order.
        for f in self.f:
            if not f(row[header.index(self.column)]):
                return False
        return True


def kts2ms(v):
    """ Convert nautical miles per hour to m/s. """
    return float(v) * 0.5144447


def mScm2Sm(v):
    """ Convert milliSieverts per cm to Sieverts per m. """
    return float(v) * 0.1


def uScm2Sm(v):
    """ Converts microSieverts per cm to Sieverts per m. """
    return float(v) * 0.0001


def dbar2pa(v):
    """ Converts decibars to Pascals. """
    return float(v) * 10000.


def mgL2gL(v):
    """ Converts mg/L to g/L. """
    return float(v) * 0.001


def polygon_area(x, y):
    """
	Compute the area of a polygon represented by separate lists of x and y coordinates.
	Stolen from https://stackoverflow.com/a/53864271/1050386
	"""
    if len(x) < 4 or len(y) < 4:
        raise Exception('Not a closed polygon.')
    # coordinate shift
    x_ = x - x.mean()
    y_ = y - y.mean()
    # everything else is the same as maxb's code
    correction = x_[-1] * y_[0] - y_[-1] * x_[0]
    main_area = np.dot(x_[:-1], y_[1:]) - np.dot(y_[:-1], x_[1:])
    return 0.5 * np.abs(main_area + correction)


def circle_area(r):
    """
	Area of a circle given its radius.
	"""
    return 2. * math.pi * r


def json_post(url, data, email, token):
    """
	Perform a POST request and get JSON back.
	Shamelessly stolen from: https://stackoverflow.com/a/36678494 and https://stackoverflow.com/a/24648149
	and https://stackoverflow.com/a/36485152/1050386
	url - The URL to send the request to.
	data - A dict containing POST data.
	"""
    data = json.dumps(data).encode('utf-8')  # parse.urlencode(data).encode()
    b64 = base64.b64encode(bytes('{}:{}'.format(email, token), 'ascii'))
    req = request.Request(url)
    req.add_header('Authorization', 'Basic {}'.format(b64.decode('utf-8')))
    req.add_header('Accept', 'application/json')
    req.add_header('Content-type', 'application/json; charset=utf-8')
    req.add_header('Content-length', len(data))
    res = request.urlopen(req, data)
    return res.read().decode('utf-8')


def json_get(url, email, token):
    """
	Perform a GET request and get JSON back.
	"""
    b64 = base64.b64encode(bytes('{}:{}'.format(email, token), 'ascii'))
    req = request.Request(url)
    req.add_header('Authorization', 'Basic {}'.format(b64.decode('utf-8')))
    req.add_header('Accept', 'application/json')
    res = request.urlopen(req)
    return res.read().decode('utf-8')


def download(url, outfile, email, token):
    """
	Download the given URL and save to the given file.
	"""
    b64 = base64.b64encode(bytes('{}:{}'.format(email, token), 'ascii'))
    req = request.Request(url)
    req.add_header('Authorization', 'Basic {}'.format(b64.decode('utf-8')))
    with request.urlopen(req) as f:
        with open(outfile, 'wb') as o:
            o.write(f.read())


def create_zip(zipfile, dirname, replace=False):
    """
    Create a zip file in the specified location and add the given files.
    :param zipfile: The name of the created zip file.
    :param files: A list of files to put in the zip.
    """
    if zipfile.endswith('.zip'):
        zipfile = os.path.splitext(zipfile)[0]
    if os.path.exists(zipfile):
        if not replace:
            raise Exception('Zip file exists.')
        else:
            os.unlink(zipfile)
    return shutil.make_archive(zipfile, 'zip', dirname)

class _tmpfile:
    """
    Contains the return value of mkstemp and closes the file handle on destruction.
    """
    def __init__(self, fd, path):
        self.path = path
        self.name = os.path.basename(path)
        self.dir = os.path.dirname(path)
        try:
            os.close(fd)
        except: pass

    def rename(self, to_path):
        """
        Rename the file to a new path. Will close the file
        and reopen it. Will create the necessary directory structure.
        """
        if os.path.exists(to_path):
            raise Exception('The file must not exist.')
        to_dir = os.path.dirname(to_path)
        try:
            os.makedirs(to_dir)
        except: pass
        shutil.move(self.path, to_path)
        self.path = to_path
        self.name = os.path.basename(to_path)
        self.dir = to_dir

    def move_to(self, to_dir):
        """
        Move the file to a new path. Will close the file
        and reopen it. Will create the necessary directory structure.
        """
        if os.path.isfile(to_dir):
            raise Exception('The given path is a file. It must be a directory or non-existent.')
        try:
            os.makedirs(to_dir)
        except: pass
        to_path = os.path.join(to_dir, self.name)
        shutil.move(self.path, to_path)
        self.path = to_path
        self.name = os.path.basename(to_path)
        self.dir = to_dir

    def delete(self):
        """
        Delete the file.
        :return: True on success, false on failure.
        """
        try:
            os.unlink(self.path)
            return True
        except:
            return False
        
    def rename_unique(self):
        """
        Rename the contained file with a unique hash.
        Will close the open file.
        """
        hash = uuid.uuid4().hex
        self.rename(os.path.join(self.dir, hash))
    

def tempdir():
    return gettempdir()

def maketempdir():
    return mkdtemp(prefix='msea_')

def maketempfile(suffix=None, create=True):
    """
    Return an opened temporary file with a hardcoded prefix so that it may be cleaned up correctly.
    :param suffix: A suffix (i.e., extension) for the file.
    :param create: If true, create the file as usual. If false, create a temporary directory and random name for the file inside it.
    :return: The file handle and name of the file.
    """
    if create:
        fd, name = mkstemp(prefix='msea_', suffix=suffix)
    else:
        fd = -1
        name = os.path.join(mkdtemp(prefix='msea_'), str(uuid.uuid4().hex) + suffix)
    return _tmpfile(fd, name)

def tempdir():
    """
    Return a temporary dir with a hardcoded prefix so that it may be cleaned up correctly.
    :return: The file handle and name of the file.
    """
    return mkdtemp(prefix='msea_')

def extract_zip(zipfile):
    """
	Recursively extract a zip file. If the file is 
	archived within it self, this function will 'peel back'
	the layers and extract the files within.
	Returns a list of the files extracted to a temporary folder and a reference to the
	temporary folder context itself. When the latter goes out of scope the
	folder and its contents are destroyed.
	"""

    result = []

    # Open the zipfile and get the list of files inside.
    with ZipFile(zipfile) as z:
        names = z.namelist()
        d = tempdir()
    
        # Iterate over the names; create a full path incl. the temp dir and add it
        # to the result list.
        for name in names:
            zname = os.path.join(d, name)
            result.append(zname)

            # Open the output file and extract the zipped file to it.
            with z.open(names[0], 'r') as f:
                with open(zname, 'wb') as o:
                    o.write(f.read())

        if len(names) == 1 and names[0].lower().endswith('.zip'):
            # If there was one archive file inside, recursively extract it.
            return extract_zip(os.path.join(d, names[0]))
        else:
            # Otherwise return the result list and the tempdir context.
            return result, d


def hash_file(filename):
    """
	Return the md5 hash of a file as a hex string.
	Shamelessly stolen from here: https://stackoverflow.com/a/22058673/1050386
	"""
    # BUF_SIZE is totally arbitrary, change for your app!
    BUF_SIZE = 65536  # lets read stuff in 64kb chunks!

    md5 = hashlib.md5()
    try:
        with open(filename, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                md5.update(data)
    except Exception as e:
        logging.warning(str(e))

    return md5.hexdigest()


def crawl_dir(dirname, result, ignore):
    """
	Crawl the directory and return a list of all files/folders
	contained therein. 
	
	The ignore argument is a list of substrings to ignore if
	found in the full path of a file.
	"""
    for f in [x for x in os.listdir(dirname) if x not in ('.', '..')]:
        ff = os.path.join(dirname, f)
        skip = False
        for ig in ignore:
            if ff.find(ig) > -1:
                skip = True
                break
        if not skip:
            if os.path.isdir(ff):
                crawl_dir(ff, result, ignore)
            else:
                result.append(ff)


def to_dict(instance):
    """
	Convert a Django model instance to a dictionary.
	Shamelessly stolen from: https://stackoverflow.com/a/29088221/1050386
	"""
    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields):
        data[f.name] = f.value_from_object(instance)
    for f in opts.many_to_many:
        data[f.name] = [i.id for i in f.value_from_object(instance)]
    return data


def get_file_type(path):
    """
    Return the standard MIME type of the give file. Return a default if no type can be divined.
    :param path: The path to a file, including the filename.
    :return: A MIME type as a string.
    """
    mime_type, encoding = mimetypes.guess_type(path)
    if not mime_type:
        mime_type = 'application/octet-stream'
    return mime_type


def get_file_meta(path):
    """
	Return a dictionary containing metadata about the file.
	"""
    try:
        import pythoncom
        import win32com.client
        pythoncom.CoInitialize()
        sh = win32com.client.gencache.EnsureDispatch('Shell.Application', 0)
        ns = sh.NameSpace(os.path.dirname(path))
        item = ns.ParseName(os.path.basename(path))
        meta = {}

        col = 0
        while True:
            # Get the colname and break if it's null.
            colname = ns.GetDetailsOf(None, col)
            if not colname:
                break
            # Get the column value.
            colval = ns.GetDetailsOf(item, col)
            colval = str(colval).strip() if colval else ''
            if colval:
                meta[colname] = colval
            col = col + 1

        # video = {}
        # try:
        # 	tag = TinyTag.get(path)
        # 	for k, v in dict(tag):
        # 		v = str(v).strip() if v else None
        # 		if v:
        # 			meta['tags'][k] = v
        # except Exception as e:
        # 	logging.warning(str(e))

        # if len(meta):
        # 	print('Got file meta')

        # if len(video):
        # 	print('Got video meta')
        # 	meta['video'] = video

        # photo = {}
        # try:
        # 	img = Image.open(path)
        # 	photo = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }
        # except Exception as e:
        # 	logging.warning(str(e))

        # if len(photo):
        # 	print('Got photo meta')
        # 	meta['photo'] = photo

        # exif = {}
        # try:
        # 	with open(path, 'rb') as f:
        # 		img = eImage(f)
        # 		for k in img.list_all():
        # 			exif[k] = getattr(img, k)
        # except Exception as e:
        # 	logging.warning(str(e))

        # if len(exif):
        # 	meta['exif'] = exif
        return meta
    except Exception as e:
        logging.warning(str(e))

    print('Done')
    return None


def get_medium_props(filename):
    """
	Attempt to glean some characteristics of the medium file.
	"""
    try:
        cv = cv2.VideoCapture(filename)
        fct = cv.get(cv2.CAP_PROP_FRAME_COUNT)
        fps = cv.get(cv2.CAP_PROP_FPS)
        w = cv.get(cv2.CAP_PROP_FRAME_WIDTH)
        h = cv.get(cv2.CAP_PROP_FRAME_HEIGHT)
        _, frame = cv.retrieve(fct / 2)
        frame = cv2.imencode('png', frame)
    except Exception as e:
        logging.warning(str(e))
        fct = 0
        fps = 1.
        w = 0
        h = 0
        frame = None

    try:
        hash = hash_file(filename)
    except Exception as e:
        logging.warning(str(e))
        hash = None

    return {
        'length': fct / fps if fps > 0 else 0,
        'width': w,
        'height': h,
        'path': filename,
        'hash': hash,
        'thumbnail': frame
    }

def process_email_template(template_file, **kwargs):
    with open(template_file, 'r') as f:
        template = f.read()
    template = Template(template)
    return template.substitute(**kwargs)


def send_email(**kwargs):
    """
    Send the verification email.
    :param message: The email message body.
    :param subject: The email subject line.
    :param from_email: The email to send from.
    :param to_email: The email to send to.
    :param reply_to: The email to reply to (optional).
    """
    message = kwargs['message']
    subject = kwargs['subject']
    from_email = kwargs['from_email']
    to_email = kwargs['to_email']
    reply_to = kwargs.get('reply_to')

    # For additional optional arguments.
    args = {}

    if reply_to:
        # If givenm check formatting and add to args.
        if type(reply_to) == str:
            if reply_to.find(',') > -1:
                reply_to = reply_to.split(',')
            else:
                reply_to = [reply_to]
        args['reply_to'] = reply_to

    try:
        with get_connection(fail_silently=False) as conn:
            msg = EmailMessage(
                subject=subject, body=message, 
                from_email=from_email, 
                to=[to_email], 
                headers={'Content-Type': 'text/html'}, 
                connection=conn,
                **args
            )
            msg.send()
    except Exception as e:
        raise Exception('Failed to send email: ' + str(e))


def is_valid_ip(ip_address):
    """ Check Validity of an IP address """
    try:
        ipaddress.ip_address(u'' + ip_address)
        return True
    except:
        return False


def get_ip_address(request):
    """ 
    Makes the best attempt to get the client's real IP or return the loopback .
    Shamelessly stolen from: https://github.com/Miserlou/django-easy-timezones/blob/master/easy_timezones/utils.py
    """
    PRIVATE_IPS_PREFIX = ('10.', '172.', '192.', '127.')
    ip_address = ''
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', '')
    if x_forwarded_for and ',' not in x_forwarded_for:
        if not x_forwarded_for.startswith(PRIVATE_IPS_PREFIX) and is_valid_ip(x_forwarded_for):
            ip_address = x_forwarded_for.strip()
    else:
        ips = [ip.strip() for ip in x_forwarded_for.split(',')]
        for ip in ips:
            if ip.startswith(PRIVATE_IPS_PREFIX):
                continue
            elif not is_valid_ip(ip):
                continue
            else:
                ip_address = ip
                break
    if not ip_address:
        x_real_ip = request.META.get('HTTP_X_REAL_IP', '')
        if x_real_ip:
            if not x_real_ip.startswith(PRIVATE_IPS_PREFIX) and is_valid_ip(x_real_ip):
                ip_address = x_real_ip.strip()
    if not ip_address:
        remote_addr = request.META.get('REMOTE_ADDR', '')
        if remote_addr:
            if not remote_addr.startswith(PRIVATE_IPS_PREFIX) and is_valid_ip(remote_addr):
                ip_address = remote_addr.strip()
    if not ip_address:
        ip_address = '127.0.0.1'
    return ip_address


def load_config(config_file):
    """
    Load the given configuration file and return
    the dictionary.
    """
    print('Loading config:', config_file)
    with open(config_file, 'r') as f:
        return json.load(f)

def load_env(env_file):
    """
    Load the env file and put it in the environment.
    """
    with open(env_file, 'r') as f:
        line = f.readline()
        while line:
            try:
                k, v = line.strip().split('=')
                os.environ[k] = v
            except: pass
            line = f.readline()

def db_connect(autocommit=True):
    """
    Connect to the database using the given configuration and return the connection.
    The configuration must contain a DATABASES block, like that found in a Django
    configuration.
    :param config: A configuration object.
    :return: A database connection.
    """    
    conn = psycopg2.connect(f'dbname={os.environ["POSTGRES_DB"]} user={os.environ["POSTGRES_USER"]} password={os.environ["POSTGRES_PASSWORD"]} host={os.environ["POSTGRES_HOST"]}')
    conn.autocommit = autocommit
    return conn
