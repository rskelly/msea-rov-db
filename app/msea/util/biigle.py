#!/usr/bin/env python

"""
A set of tools for listing Biigle projects, requesting reports and label trees, and injesting
this data into the database.
"""

import os
import json
import zipfile
import math
import requests
from requests.auth import HTTPBasicAuth

from msea.util import util

# Biigle base URL.
base_url = 'https://biigle.de'
# Biigle API root.
api = 'api/v1'
# Report types. These are determined by Biigle.
report_types = {'id': 8, 'name': 'VideoAnnotations\\Csv'}, {'id': 10, 'name': 'VideoLabels\\Csv'}


def get_csv_from_zip(zfile):
    """
    Recursively look through zip file, which may contain another zip file,
    for a csv. Extract the csv and return the temporary path to it.
    """
    tmp = util.tempdir()
    with zipfile.ZipFile(zfile) as z:
        for n in z.infolist():
            if n.filename.strip().lower().endswith('.csv'):
                z.extract(n, path=tmp)
                return os.path.join(tmp, n.filename)
            elif n.filename.strip().lower().endswith('.zip'):
                z.extract(n, path=tmp)
                return get_csv_from_zip(os.path.join(tmp, n.filename))
    raise Exception('No CSV file found in archive.')


class biigle:
    """
    A class with methods for retrieving data from Biigle.
    """

    def __init__(self):
        self.projects = None  # A list of available projects.
        self.reports = None  # A list of available reports.
        self.label_trees = None  # A list of label trees.

    def get_from_api(self, path, username, api_key):
        """
        Proxy a Biigle API request to the Biigle endpoint using stored user credentials.
        :param path: The API path, after the /api/v1 part.
        :param username: The Biigle username.
        :param api_key: The Biigle API key.
        :return:
        """
        res = requests.get(
            f'{base_url}/{api}/{path}/',
            headers={'Accept': 'application/json'},
            auth=HTTPBasicAuth(username, api_key)
        )
        return res.json()

    def post_to_api(self, path, data, username, api_key):
        """
        Proxy a Biigle API request to the Biigle endpoint using stored user credentials.
        :param path: The API path, after the /api/v1 part.
        :param username: The Biigle username.
        :param api_key: The Biigle API key.
        :return:
        """
        res = requests.post(
            f'{base_url}/{api}/{path}/',
            headers={'Accept': 'application/json'},
            auth=HTTPBasicAuth(username, api_key),
            data=data
        )
        return res.json()

    def get_label_trees(self, username, api_key):
        """
        Return the list of Biigle label trees accessible from this user.
        :param self:
        :param username:
        :param api_key:
        :return:
        """
        res = requests.get(
            "{}/{}/projects".format(base_url, api),
            headers={'Accept': 'application/json'},
            auth=HTTPBasicAuth(username, api_key)
        )
        self.label_trees = res.json()
        return self.label_trees

    def get_projects(self, username, api_key):
        """
        Return a list of Biigle annotation projects accessible by the user.
        username - The Biigle username (email) of the user.
        api_key - The user's Biigle API token.
        return The raw JSON response.
        """
        res = requests.get(
            '{}/{}/projects'.format(base_url, api),
            headers={'Accept': 'application/json'},
            auth=HTTPBasicAuth(username, api_key)
        )
        self.projects = res.json()
        return self.projects

    def generate_report(self, project_id, username, api_key, type_id=8, disable_notifications=True):
        """
        Request report generation from the Biigle service.
        project_id - The ID of the project.
        username - The Biigle username (email) of the user.
        api_key - The user's Biigle API token.
        type_id - The report type.
        disable_notifications - If true, email notification will not be generated.
        return The raw JSON reponse (contains the report ID).
        """
        data = {'disable_notifications': disable_notifications, 'type_id': type_id}

        res = requests.post(
            '{}/{}/projects/{}/reports'.format(base_url, api, project_id),
            headers={'Accept': 'application/json'},
            json=data,
            auth=HTTPBasicAuth(username, api_key)
        )
        res = res.json()

        # Turn the list of errors into an exception.
        if res.get('errors'):
            errs = [res['message']]
            for k, v in res['errors'].items():
                errs.append(v[0] if isinstance(v, list) else v)
            raise Exception(' '.join(errs))

        self.reports = res
        return self.reports

    def get_report(self, report_id, username, api_key, file=None):
        """
        Get and download the report indicated by report_id. Return the temporary
        filename used to save the report data. The report will be a zip file.
        report_id - The ID of the report.
        username - The Biigle username (email) of the user.
        api_key - The user's Biigle API token.
        file - The file to save the report to. If null, a temporary file is used and returned.
        return The local file name of the report.
        """
        res = requests.get(
            url='{}/{}/reports/{}'.format(base_url, api, report_id),
            headers={'Accept': 'application/json'},
            auth=HTTPBasicAuth(username, api_key)
        )

        # If the request fails, exception
        if res.status_code != 200:
            raise Exception('Report generation request failed: {}.'.format(res.status_code))

        # Save the result to a temporary file.
        zfile = util.maketempfile()
        with open(zfile.path, 'w') as z:
            for chunk in res.iter_content(chunk_size=1024):
                z.write(chunk)

        # Extract the report file from the archive.
        tfile = get_csv_from_zip(zfile.path)
        if file and tfile:
            os.rename(tfile, file)
        else:
            file = tfile

        # Try to remove zip file.
        zfile.delete()

        return file


def configure_shape(type, points):
    """
    Configure a JSON geometry from the biigle shape parameters.
    """
    type = type.lower()
    points = json.loads(points)
    if type == 'point':
        shape = {
            'type': 'Point',
            'geometry': points
        }
    elif type == 'polygon' or type == 'rectangle':
        shape = {
            'type': 'Polygon',
            'geometry': [list(zip(points, points[1:]))]
        }
    elif type == 'linestring':
        shape = {
            'type': 'Linestring',
            'geometry': list(zip(points, points[1:]))
        }
    elif type == 'circle':
        print('circle1', points)
        # The result may be a list of points(?)
        if points and isinstance(points[0], list):
            points = points[0]
        print('circle2', points)
        x, y, rad = points
        pts = [[x + math.cos(a) * rad, y + math.sin(a) * rad] for a in
               [(a * 20.) * (math.pi / 180.) for a in range(18)]]
        shape = {
            'type': 'Polygon',
            'geometry': [pts + pts[0]]  # Add the first element to close the poly.
        }
    elif type == 'wholeframe':
        pts = None
        shape = None
    else:
        raise Exception('Unknown type: ' + type)


if __name__ == '__main__':
    # Test.
    b = biigle()
    # b.get_projects()
    b.get_report(541, 8)
