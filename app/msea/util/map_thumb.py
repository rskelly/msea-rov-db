#!/usr/bin/env python3

'''
This script makes map thumbnails with geometries and a tile map background and saves them to 
the database. The database should have a geometry/geography field for input and a 
byte array field for output. The script will fill any thumbnail field that is null.
'''
import os
import json
import psycopg2
import staticmaps
from staticmaps.tile_provider import TileProvider

from msea.util import util

line_color = '#f702ba'
bg_color = '#4a5f98'

def read_geojson(data):
	'''
	Recursively extract coords from geojson and return as a list of 'shapes'.

	NOTE: GeoJson uses x, y order. We need lat lon (y, x) order.
	'''

	# If the data is a string, load it into a dict.
	coords = []
	if isinstance(data, str):
		data = json.loads(data)

	if not data:
		return None

	# If the entity is a feature collection, recursively unwrap it.
	type = data.get('type')
	if type == 'FeatureCollection':
		for f in data['features']:
			geoms = read_geojson(f)
			for geom in geoms:
				coords.append(geom)
	
	# The the entity is a feature, extract the geometry.
	if type == 'Feature':
		data = data['geometry']
		type = data.get('type')
	
	# Extract the geometry coords.
	if type == 'Point':
		x, y = data.get('coordinates')
		coords.append(('point', (x, y)))

	elif type == 'MultiPoint':
		# For points, the geometry gets buffered a little bit by
		# creating a polygon with a radius.
		for x, y in data.get('coordinates'):
			coords.append(('point', (x, y)))

	elif type == 'LineString':
		# Extract a line string.
		coords.append(('line', [(y, x) for x, y in data.get('coordinates')]))

	elif type == 'Polygon':
		# Extract a polygon.
		coords.append(('poly', [(y, x) for x, y in data.get('coordinates')[0]]))

	elif type == 'MultiLineString':
		# Extract a multipolygon.
		for line in data.get('coordinates'):
			coords.append(('line', [(y, x) for x, y in line]))

	elif type == 'MultiPolygon':
		# Extract a multipolygon.
		for poly in data.get('coordinates'):
			coords.append(('poly', [(y, x) for x, y in poly[0]]))

	# TODO: Extract multis.

	return coords

def render_png(shapes, width, height, filename = None, center = None, radius = None):
	'''
	Return a byte array containing PNG data for the rendered map.

	Overlays shapes on the tile map and returns a PNG of the given size in pixels.

	Shapes is an array of arrays of tuples containing lat/lon pairs.

	Center (x, y tuple) and radius (km) together make a transparent circle that forces a zoom-out.
	'''

	global bg_color
	global line_color

	context = staticmaps.Context()

	context.set_tile_provider(TileProvider(
	    'google-sat-noatt',
	    url_pattern='https://mt1.google.com/vt/lyrs=s&x=$x&y=$y&z=$z',
	    attribution = '',
	    max_zoom = 24,
	))
	
	context.set_background_color(staticmaps.parse_color(bg_color))

	if center and radius:
		obj = staticmaps.Circle(
			staticmaps.create_latlng(center[1], center[0]),
			color = staticmaps.TRANSPARENT,
			fill_color = staticmaps.TRANSPARENT,
			radius_km = radius,
			width = 1,
		)
		context.add_object(obj)

	if shapes:
		if not isinstance(shapes, list):
			shapes = [shapes]

		# Overlay the shapes.
		for item in shapes:
			if len(item) == 2:
				type, coords = item
				l_color = None
			else:
				type, coords, l_color = item
			obj = None
			if type == 'point':
				obj = staticmaps.Circle(
					staticmaps.create_latlng(coords[1], coords[0]),
					color = staticmaps.parse_color(l_color if l_color else line_color),
					radius_km = .01,
					width = 3,
				)
			elif type == 'line':
				obj = staticmaps.Line(
					[staticmaps.create_latlng(y, x) for y, x in coords],
					color = staticmaps.parse_color(l_color if l_color else line_color),
					width = 2,
				)
			elif type == 'poly':
				obj = staticmaps.Area(
					[staticmaps.create_latlng(y, x) for y, x in coords],
					color = staticmaps.parse_color(l_color if l_color else line_color),
					width = 2,
				)
			if obj:
				context.add_object(obj)

	else:
		obj = staticmaps.Circle(
			staticmaps.create_latlng(49.294444, -123.111389),
			fill_color = staticmaps.parse_color('#00000000'),
			color = staticmaps.parse_color('#00000000'),
			radius_km = 100,
			width = 0,
		)
		context.add_object(obj)

	# Render the image.
	image = context.render_cairo(width, height)

	# If a filename is given, write to it and return True, 
	# otherwise use a temp file and return the bytes.
	if not filename:
		tmp = util.maketempfile()
		image.write_to_png(tmp.path)
		with open(tmp.path, 'rb') as f:
			data = f.read()
		return data
	else:
		image.write_to_png(filename)
		return True

def site_thumb(cur, id):
	'''
	Convenience function for updating a specific site thumbnail.
	'''
	cur.execute('select st_asgeojson(st_transform(st_force2d(geom::geometry), 4326)) as json from shared.site where id=%s', [id])
	geojson, = cur.fetchone()
	shapes = read_geojson(geojson)
	print(id, geojson)
	if shapes:
		data = render_png(shapes, 100, 100)
		cur.execute('update shared.site set thumbnail=%s where id=%s', [psycopg2.Binary(data), id])

def spatial_library_thumb(cur, id):
	'''
	Convenience function for updating a specific spatial library thumbnail.
	'''
	cur.execute('select st_asgeojson(st_transform(st_force2d(geom::geometry), 4326)) as json from shared.spatial_library where id=%s', [id])
	geojson, = cur.fetchone()
	shapes = read_geojson(geojson)
	if shapes:
		data = render_png(shapes, 100, 100)
		cur.execute('update shared.spatial_library set thumbnail=%s where id=%s', [psycopg2.Binary(data), id])

if __name__ == '__main__':
	
	import argparse

	parser = argparse.ArgumentParser(description = 'Populate the thumbnail field in a table by rendering a geometry field.')
	parser.add_argument('connection', metavar = 'database connection', type = str, help = 'Connection string. Enclose in quotes.')
	parser.add_argument('table', metavar = 'table name', type = str, help = 'The name of the table to update.')
	parser.add_argument('geom', metavar = 'geom field', type = str, help = 'The name of the geometry field.')
	parser.add_argument('thumb', metavar = 'thumb field', type = str, help = 'The name of the thumbnail field.')
	parser.add_argument('id', metavar = 'id field', nargs = '?', type = str, help = 'The name of the ID field. Defaults to "id".', default = 'id')
	parser.add_argument('-r', '--refresh', action = 'store_true', help = 'Refresh all thumbnails.')
	args = parser.parse_args()

	print('Running')

	conn = psycopg2.connect(args.connection)
	cur = conn.cursor()
	
	if args.refresh:
		cur.execute('update {} set "{}" = NULL'.format(args.table, args.thumb))

	cur.execute('select {}, st_asgeojson(st_transform(st_force2d({}::geometry), 4326)) as json from {} where {} is null'.format(args.id, args.geom, args.table, args.thumb))
	for id, geojson in cur.fetchall():
		shapes = read_geojson(geojson)
		if shapes:
			data = render_png(shapes, 100, 100)
			cur.execute('update {} set {}=%s where {}=%s'.format(args.table, args.thumb, args.id), [psycopg2.Binary(data), id])

	conn.commit()

	# Create/find the image dir and cache file.
	imgdir = os.path.join(util.tempdir(), 'django_image_cache')
	for f in [x for x in os.listdir(imgdir) if x.endswith('.png')]:
		os.unlink(os.path.join(imgdir, f))

	print('Done')