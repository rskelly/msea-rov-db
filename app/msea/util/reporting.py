import os
import shutil
import pandas as pd
from datetime import datetime
from osgeo import ogr

from django.conf import settings

from msea.util import util

def prepare_row(keys):
    """
    Returns a function that pepares a row by zipping keys and values.
    Fixes dates using ISO format. Nones are changed to empty strings.
    """
    def format_cell(v):
        if v is None:
            return ''
        elif isinstance(v, datetime):
            return v.strftime('%m/%d/%YT%H:%M:%SZ')
        else:
            return v
    return lambda row: dict(zip(keys, [format_cell(v) for v in row]))


def render_spatial(format, sql):
    """
    Render the data to a spatial database given the SQL statement (which
    may not actually have a geometry column).
    """

    # Get the connection info from the main application settings file.
    dbconfig = settings.DATABASES.get('default')
    connstr = 'PG:host={host} dbname={dbname} user={user} password={password} port={port}'.format(
        host=dbconfig.get('HOST'),
        dbname=dbconfig.get('NAME'),
        user=dbconfig.get('USER'),
        password=dbconfig.get('PASSWORD'),
        port=dbconfig.get('PORT')
    )

    # Run the query.
    conn = ogr.Open(connstr)
    lyr = conn.ExecuteSQL(sql, dialect='PostgreSQL')

    # Tempfile handle.
    tf = None

    # For shp we need a folder; other formats need a file.
    if format == 'shp':
        file = util.tempdir()
    else:
        tf = util.maketempfile(suffix='.sqlite')
        file = tf.path

    # Create the output file(s)
    drv = ogr.GetDriverByName('ESRI Shapefile' if format == 'shp' else 'SQLite')
    ds = drv.CreateDataSource(file)
    olyr = ds.CopyLayer(lyr, 'results')
    del lyr, olyr, ds

    # For shp, zip up the result.
    if format == 'shp':
        tfile = file
        file = shutil.make_archive(file, 'zip', file)
        try:
            os.unlink(tfile)
        except: pass

    return file


def render_excel(title, data):
    """
    Render the data (a list of dicts) to a dataframe and save as
    a MS Excel document.
    """
    tf = util.maketempfile(suffix='.xlsx')

    # Create a dataframe
    df = pd.DataFrame(data)

    # Export the dataframe as excel.
    with pd.ExcelWriter(tf.name, engine='openpyxl') as w:
        df.to_excel(w, sheet_name=title)

    return tf.name


def render_csv(data):
    """
    Render the data (a list of dicts) to a dataframe and save as
    a CSV file.
    """

    tf = util.maketempfile(suffix='.csv')

    # Create a dataframe
    df = pd.DataFrame(data)

    # Export the dataframe as csv.
    df.to_csv(tf.name)

    return tf.name


def render_html(title, labels, data, meta=None):
    """
    Render a report as HTML.
    The data parameter is a list of dictionaries whose keys correspond to the
    list of labels.

    The result is written to the given filename.
    """

    tf = util.maketempfile(suffix='.html')

    mt = []
    if meta:
        mt.append('<table>')
        for k, v in meta.items():
            mt.append('<tr><td>{k}</td><td>{v}</td></tr>'.format(k=k, v=v))
        mt.append('</table>')

    with open(tf.name, 'w') as f:
        f.write('''
            <html>
                <head>
                    <title>{title}</title>
                    <style>
                        table {{
                            border-collapse: collapse;
                        }}
                        td {{
                            border: 1px solid black;
                        }}
                    </style>
                </head>
                <body>
                    <h1>{title}</h1>
                    {meta}

                    <h2>Results</h2>
                    <table>
        '''.format(
            meta='\n'.join(mt),
            title=title
        ))

        # Column headings.
        f.write('<tr><th>{}</th></tr>'.format('</th><th>'.join(labels)))

        # Data rows.
        for item in data:
            f.write('<tr><td>{}</td></tr>'.format('</td><td>'.join(list(map(str, [item[x] for x in labels])))))

        f.write('''
                    </table>
                </body>
            </html>
        ''')

    return tf.name


def render_pdf(title, labels, data, meta=None):
    """
    Render the data to HTML and then to PDF. Return the PDF file.
    """

    file = render_html(title, labels, data, meta)

    # Make a pdf from the html (doesn't work)
    tf = util.maketempfile(suffix='.pdf')
    HTML(file).write_pdf(tf.name)

    try:
        os.unlink(file)
    except:
        pass

    return pfile
