"""
Utilities related to Django views.
"""

import math

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic.base import TemplateView

from msea.models.rov import *


def make_nav(page, pagecount, numitems):
	"""
	Return a configuration for a page navigation.
	page - The current page (starting with 0).
	pagecount - The number of pages.
	numitems - The number of displayed page numbers.
	"""

	if pagecount <= numitems:
		# No need to calculate if perpage is less than the total.
		return {'page': page, 'first': 0, 'last': pagecount - 1, 'pages': range(0, pagecount)}
	else:
		# The start page is either 0, or the current page minus 1/2 the number of items.
		start = max(0, page - int(numitems / 2))
		# The end page is the start plus the number of items.
		end = start + numitems

		# If the end is beyond the available number, shift back.
		if end > pagecount:
			end = pagecount
			start = end - numitems

		result = {'page': page, 'first': 0, 'last': pagecount - 1, 'pages': range(start, end)}

		# Calculate the nav arrows.
		if page > 0:
			result['previous'] = page - 1
		if page <= pagecount:
			result['next'] = page + 1

		return result


def ldt_filter(req, model = None):
	"""
	Extract the cruises, dives and transects lists and return specific instances using the IDs in the GET request.
	If a model is provided, will only extract entities that have relationships with instances of the model. 
	E.g., only cruises that have dives that have measurements, if the Measurement model is given.

	The output of this function works with the templates,
	ldt_filter.html (Cruise, dive, transect)
	ld_filter.html (Cruise, dive)
	ldti_filter.html (Cruise, dive, transect, instrument)
	ldi_filter.html (Cruise, dive, instrument)
	l_filter.html (Cruise)
	"""

	# By default, no selections.
	cruiseid = 0
	diveid = 0
	transectid = 0

	# Get the selections from the request.
	try:
		cruiseid = int(req.GET.get('cruiseid', 0))
	except: pass
	try:
		diveid = int(req.GET.get('diveid', 0))
	except: pass
	try:
		transectid = int(req.GET.get('transectid', 0))
	except: pass

	# By default, the entities are null.
	dive = None
	transect = None
	cruise = None
	dives = None
	transects = None
	cruises = None

	# Get the cruise if an ID is given.
	try:
		cruise = Cruise.objects.get(pk = cruiseid)
	except: pass

	# Get the dive if an ID is given.
	try:
		dive = Dive.objects.get(pk = diveid)
	except: pass

	# Get the transect if an ID is given.
	try:
		transect = Transect.objects.get(pk = transectid)
	except: pass

	# If a cruise is found, get the dives from it.
	if cruise:
		q = {'cruise': cruise}
		if model and hasattr(model, 'dive_id'):
			# When the model is given, make a query using the list of dive IDs in the model rows.
			q['pk__in'] = model.objects.all().values_list('dive_id', flat = True)
		# Get the dives matching the query.
		dives = Dive.objects.filter(**q).order_by('name')
	
	# If a dive is found, get the transects associated with it.
	if dive:
		q = {'dive': dive}
		if model and hasattr(model, 'transect_id'):
			# When the model is given, make a query using the list of transect IDs in the model rows.
			q['pk__in'] = model.objects.all().values_list('transect_id', flat = True)
		# Get the transects matching the query.
		transects = Transect.objects.filter(**q).order_by('name')

	# If there's a model, load cruises that own an instance of the model through the dives.
	# Do not use the filtered version of the dives list.
	q = {}
	if model and hasattr(model, 'dive_id'):
		d = Dive.objects.filter(pk__in = model.objects.all().values_list('dive_id', flat = True))
		q['pk__in'] = d.values_list('cruise_id', flat = True)
	cruises = Cruise.objects.filter(**q).order_by('name')

	return {
		'cruises': cruises, 
		'dives': dives,
		'transects': transects, 
		'cruise': cruise,
		'dive': dive, 
		'transect': transect,
		'cruiseid': cruiseid,
		'diveid': diveid,
		'transectid': transectid
	}


def cldt_filter(req, model = None):
	"""
	Extract the cruises, dives and transects lists and return specific instances using the IDs in the GET request.
	If a model is provided, will only extract entities that have relationships with instances of the model. 
	E.g., only cruises that have dives that have measurements, if the Measurement model is given.

	The output of this function works with the templates,
	ldt_filter.html (Cruise, dive, transect)
	ld_filter.html (Cruise, dive)
	ldti_filter.html (Cruise, dive, transect, instrument)
	ldi_filter.html (Cruise, dive, instrument)
	l_filter.html (Cruise)
	"""

	# By default, no selections.
	cruiseid = 0
	diveid = 0
	transectid = 0

	# Get the selections from the request.
	try:
		cruiseid = int(req.GET.get('cruiseid', 0))
	except: pass
	try:
		diveid = int(req.GET.get('diveid', 0))
	except: pass
	try:
		transectid = int(req.GET.get('transectid', 0))
	except: pass

	# By default, the entities are null.
	dive = None
	transect = None
	cruise = None
	dives = None
	transects = None
	cruises = None

	# Get the cruise if an ID is given.
	try:
		cruise = Cruise.objects.get(pk = cruiseid)
	except: pass

	# Get the dive if an ID is given.
	try:
		dive = Dive.objects.get(pk = diveid)
	except: pass

	# Get the transect if an ID is given.
	try:
		transect = Transect.objects.get(pk = transectid)
	except: pass

	# If a cruise is found, get the dives from it.
	if cruise:
		q = {'cruise': cruise}
		if model and hasattr(model, 'dive_id'):
			# When the model is given, make a query using the list of dive IDs in the model rows.
			q['pk__in'] = model.objects.all().values_list('dive_id', flat = True)
		# Get the dives matching the query.
		dives = Dive.objects.filter(**q).order_by('name')
	
	# If a dive is found, get the transects associated with it.
	if dive:
		q = {'dive': dive}
		if model and hasattr(model, 'transect_id'):
			# When the model is given, make a query using the list of transect IDs in the model rows.
			q['pk__in'] = model.objects.all().values_list('transect_id', flat = True)
		# Get the transects matching the query.
		transects = Transect.objects.filter(**q).order_by('name')

	# If there's a model, load cruises that own an instance of the model through the cruises and dives.
	# Do not use the filtered version of the dives and cruises list.
	q = {}
	if model and hasattr(model, 'dive_id'):
		dives = Dive.objects.filter(pk__in = model.objects.all().values_list('dive_id', flat = True))
		cruises = Cruise.objects.filter(pk__in = dive.values_list('cruise_id', flat = True))

	return {
		'cruises': cruises, 
		'dives': dives,
		'transects': transects, 
		'cruise': cruise,
		'dive': dive, 
		'transect': transect,
		'cruiseid': cruiseid,
		'diveid': diveid,
		'transectid': transectid
	}


class Data:
	"""
	Provides a dictionary whose values are lists. Setting a value at the 
	appropriate key appends it to the list.

	This is used for reshaping form data.
	"""

	def __init__(self, obj = None):
		self.d = []

	def get(self, k, i):
		"""
		Return the ith element of the list with key k.
		k - The key into the parameter dictionary.
		i - The index of the element to retrieve.
		"""
		if 0 <= i < len(self.d):
			return self.d[i].get(k)
		return None

	def set(self, k, v, i):
		"""
		Set the given value at the ith element of the
		list with key k. If i is beyond the end of the list,
		the list is extended.
		k - The key into the parameter dictionary.
		v - The value to set.
		i - The index of the element to update.
		"""
		while i >= len(self.d):
			self.d.append({})
		self.d[i][k] = v

	def add(self, k, v, i):
		"""
		Add the elements in v to the list at 
		key k, starting at index i.
		k - The key into the parameter dictionary.
		v - The list of values to set.
		i - The start index.
		"""
		for v0 in v:
			self.set(k, v0, i)
			i += 1

	def items(self):
		"""
		Return a pivoted list of objects containing
		the key-value pairs.
		"""
		return self.d if self.d else []
	
	def size(self):
		return len(self.d)
		
	def __str__(self):
		return str(self.d)


class SimpleAdminView(TemplateView):
	"""
	Provides a simple view for saving/deleting single-entity structures.
	Saves are triggered by the 'action' property in the POST data: 'save'
	cause creation or update, 'delete' causes deletion. For delete, 
	only the ID is necessary, to save, the full object data must be given.

	These class proprties must be set:
	template_name - The name of the template (from TemplateView)
	model_class - The model class.
	form_class - The form class.
	title - The title used in the template.
	list_name - The name used in the context object to store the list of entities.
	redirect_referer - If true, redirects to the referer rather than returning JSON.
	"""

	template_name = None
	model_class = None
	form_class = None
	title = None
	list_name = 'items'
	redirect_referer = False

	def post(self, request):
		"""
		Handle a POST request.
		"""
		data = request.POST
		if data.get('action', False):
			if data['action'] == 'save':
				return self.save(data)
			elif data['action'] == 'delete':
				return self.delete(data)
		return HttpResponse(404)

	def delete(self, postdata):
		"""
		Delete an entity represented by the given POST data.
		"""
		self.model_class.objects.get(pk = int(postdata['id'])).delete()
		if self.redirect_referer:
			return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
		else:
			return JsonResponse({'result': True})

	def save(self, postdata):
		"""
		Save the entity.
		"""
		# Initialize data objects for the model and its children.
		data = Data()

		# Iterate over the POST data. Field names are classified by the
		# [model name].[field name]. 
		for k in postdata.keys():
			v = postdata.getlist(k)
			try:
				data.add(k, v, 0)
			except: pass

		# Construct and attempt to load the instance.
		info = data.items()[0]
		item = None
		if info.get('id', False):
			item = self.model_class.objects.get(pk = int(info['id']))
		
		# If the model takes a project member, try to assign it.
		#if hasattr(self.model_class, 'projectmember'):
			# If a project member is not assigned, use the default.
		#	pmid = info.get('projectmember_id')
		#	info['projectmember'] = ProjectMember.objects.get(pk = pmid)

		# Build the form and validate it.
		data = self.form_class(info, instance = item)
		
		# Save the instance and commit.
		if data.is_valid():
			data.save(commit = True)
			if self.redirect_referer:
				return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
			else:
				return JsonResponse({'result': True})
		else:
			print(data.errors)
			if self.redirect_referer:
				return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))
			else:
				return JsonResponse({'error': ', '.join(data.errors)})

	def get_context_data(self, **kwargs):
		
		context = super().get_context_data(**kwargs)
		context['errors'] = []
		context['messages'] = []
		context['title'] = self.title

		# Load the position types.
		context[self.list_name] = self.model_class.objects.all().order_by('id')
		
		return context


class SimpleView(TemplateView):
	"""
	Provides a simple view for single-entity structures.

	These class proprties must be set:
	template_name - The name of the template (from TemplateView)
	model_class - The model class.
	title - The title used in the template.
	list_name - The name used in the context object to store the list of entities.
	"""

	template_name = None
	model_class = None
	title = None
	list_name = 'items'

	def get_context_data(self, **kwargs):
		
		context = super().get_context_data(**kwargs)
		context['errors'] = []
		context['messages'] = []
		context['title'] = self.title

		# Get the requested page number.
		try:
			page = int(self.request.GET.get('page', 0))
		except:
			page = 0

		perpage = 25

		filtered = ldt_filter(self.request, model = self.model_class)

		if filtered.get('transect') and hasattr(self.model_class, 'transect'):
			obs = self.model_class.objects.filter(transect = filtered['transect'])
		elif filtered.get('dive') and hasattr(self.model_class, 'dive'):
			obs = self.model_class.objects.filter(dive = filtered['dive'])
		elif filtered.get('cruise') and hasattr(self.model_class, 'dive'):
			d = Dive.objects.filter(cruise = filtered['cruise']).distinct()
			obs = self.model_class.objects.filter(dive__in = d)
		else:
			obs = []

		# Make nav and load measurement.
		pagecount = int(math.ceil(float(len(obs)) / perpage))

		# Load the position types.
		context[self.list_name] = obs
		context['page'] = page
		context['nav'] = make_nav(page, pagecount, 7)
		context['nav'].update({'transectid': filtered['transectid'], 'diveid': filtered['diveid'], 'cruiseid': filtered['cruiseid']})
		context.update(filtered)

		return context


