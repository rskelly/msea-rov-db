from django.contrib import admin

from msea.models.rov import *
from msea.models.public import *
from msea.models.shared import *
from msea.models.auth import *

class MSEAAdmin(admin.AdminSite):
    site_header = 'MSEA Administration'


admin_site = MSEAAdmin(name='mesa_admin')

admin_site.register(User)
admin_site.register(Platform)
admin_site.register(PlatformConfig)
admin_site.register(Site)
admin_site.register(Dive)
admin_site.register(MeasurementType)
admin_site.register(PositionType)
admin_site.register(ObservationConfidence)
admin_site.register(Substrate)
admin_site.register(Biocover)
admin_site.register(Thickness)
admin_site.register(Complexity)
admin_site.register(Flow)
admin_site.register(Position)
admin_site.register(Instrument)
admin_site.register(InstrumentConfig)
admin_site.register(Measurement)
admin_site.register(EquipmentType)
admin_site.register(MediumFormat)
admin_site.register(Person)
admin_site.register(DiveRole)
admin_site.register(Program)
admin_site.register(Cruise)
admin_site.register(CruiseCrew)
admin_site.register(CruiseRole)
admin_site.register(WeatherObservation)
