# The Django Application

This is a [Django](https://www.djangoproject.com/)/[Vue](https://www.vuejs.org) app for viewing/modifying entities in the database, generating
reports and other (future) activities.

This is a standard Django install with a few exceptions:
* The `main` folder performs no role other than storing the site configuration files in `settings_:
  * Settings are stored in the standard format, but there are several configuration files
    with different purposes. 
    * `base.py` is the default configuration from which the others derive.
    * `dev.py` is the development configuration, used by developers on their own computers.
    * `prod.py` is the production configuration, used by the live site.
    * `prod_admin.py` is a production configuration with some extra administrator permissions.
    * `test.py` is a configuration used by unit tests.
* Configurations for the database are stored by convention in `~/configs`, and are loaded by the Django settings in 
  `main/settings`. There are settings for production and development.
* The site is proxied by Nginx and uWSGI. The deployment scripts and configurations are in [administration/deployment](../administration/deployment).
* When the database is created, permissions must be granted to the `msea` user by the admin account using the SQL 
  script in [/model/db_permissions.sql](../model/db_permissions.sql). This is done automatically by the [/model/db_manager.py](../model/db_manager.py) file with the `-p` switch.
* The application itself is stored in the [/app/msea](./msea/) folder and generally follows the standard Django format, except 
  that whereas the basic setup places, for example, form classes in the file, `forms.py`, this application places the
  forms in separate files within the `forms` folder. This avoids having too-large, unorganized code files.

The folder structure within [/app/msea](./msea/) is as follows:

* [msea](./msea)
  * [forms](./msea/forms) - The declarations of Django forms for data model classes declared in [models](./msea/models).
  * [migrations](./msea/migrations) - Django-generated database migration files. Normally, these are used to created and update the
    database, but in this database the model is unmanaged by the code. Still, it is occassionally necessary to run
    `manage.py makemigrations` and `manage.py migrate` on the site.
  * [models](./msea/models) - The Django data models that correspond to tables in the database.
  * [static](./msea/static) - Static files used by the Web application, including JavaScript, images, documents and other static
    materials.
  * [templates](./msea/templates) - The Django HTML templates used to render the site pages.
  * [templatetags](./msea/templatetags) - Files containing Django tags used by the files in [templates](./msea/templates).
  * [util](./msea/util) - Utilities used by code in the [views](./msea/views) folder.
  * [views](./msea/views) - The Django view components which are responsible for handling requests, loading data and rendering 
    responses.
  * [admin.py](./msea/admin.py) - A standard Django registry of classes (from `models_) for which administration pages should be 
    automatically created.
  * [apps.py](./msea/apps.py) - Configures the Django application. In this case, there is only one: MSEA.
  * [urls.py](./msea/urls.py) - A listing of URL patterns and the pages/view classes they point to.

Note that the [models](./msea/models), [views](./msea/views) and [forms](./msea/forms) folders contain identically-named files for each section of the site.
By default, Django provides these classes in files named `models.py`, `views.py` and `forms.py`. The change is 
explained simply by the need to break up those monolithic files into more manageable chunks.

## Vue

The source code for the Vue app is in the [apps/vue](./vue/) folder.

## Tests

There ~~are~~ should be automated tests for the backend of the site in the [tests](./tests) folder. 

Tests are the foundation of "test-driven development" (TDD), in which the program's contract is defined by the 
tests, and the program must fulfil the contract by passing the tests. The ROV site is not strictly designed using TDD, 
but tests help to keep things working, when there's time to write them.

