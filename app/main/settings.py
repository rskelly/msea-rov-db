"""
Django settings for msea app.
"""

import os
import sys
from pathlib import Path

from django.contrib.staticfiles import handlers

# Settings configured by config file.
DEBUG = os.getenv('DEBUG') in ('True', '1', True, 1)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

# Hosts allowed to access the site.
ALLOWED_HOSTS = []
try:
    ALLOWED_HOSTS += os.getenv('ALLOWED_HOSTS').split(',')
except: pass

# Allow Vue dev server access DEBUG.
if DEBUG:
    CSRF_TRUSTED_ORIGINS = [
        'http://localhost:5173',
        'http://localhost:9091',
    ]

    CORS_ALLOWED_ORIGINS = [
        'http://localhost:5173',  # For Vue testing.
        'http://localhost:9091',
    ]

    DEBUG_TOOLBAR_CONFIG = {'JQUERY_URL': '', }

# WSGI is used on the webserver to proxy requests through (e.g.) Nginx to
# Django. The configuration files are in the application root.

WSGI_APPLICATION = 'main.wsgi.application'

# Backend for sending emails.
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# CSRF_COOKIE_SECURE = False
# CSRF_COOKIE_SAMESITE = 'None'
# CSRF_COOKIE_HTTPONLY = False

# Default field type for auto IDs (serials).
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent.parent

# Configuration for Knox security handler.
REST_KNOX = {
   'TOKEN_TTL': None,  # Disable token expiry.
   'USER_SERIALIZER': 'msea.views.auth.UserSerializer',
}

# Application definition
INSTALLED_APPS = [
    'msea.apps.MseaConfig',
    'django.contrib.postgres',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'knox',
    'drf_spectacular',
    'corsheaders',
]

X_FRAME_OPTIONS = 'SAMEORIGIN'

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'HOST': os.getenv('DB_HOST'),
        'PORT': os.getenv('DB_PORT'),
    }
}

# Log to stdout in Docker.
LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "verbose": {
            "format": "{levelname} {asctime} {module} {process:d} {thread:d} {message}",
            "style": "{",
        },
        'handlers': {
            'console': {
                'level': 'INFO',
                'class': 'logging.StreamHandler',
                'stream': sys.stdout,
                #'formatter': 'verbose'
            },
        },        
        "loggers": {
            "django": {
                "handlers": ["console"],
                "level": "INFO",
                "propagate": True
        }
    }
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# User is a custom model with extra fields.
AUTH_USER_MODEL = 'msea.User'

# Redirect to the login form (ideally not used).
LOGIN_REDIRECT_URL = '/rov/profile'

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = False

# Static files (CSS, JavaScript, Images)
STATIC_ROOT = os.getenv('STATIC_ROOT')
STATIC_URL = '/rov/static/'
STATICFILES_DIRS = [
]

# Configuration for rest framework.
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'knox.auth.TokenAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DATETIME_FORMAT': '%Y-%m-%dT%H:%M:%SZ',
}


# General site settings.
MSEA_HOST = os.getenv('MSEA_HOST')
MSEA_CONTACT_EMAIL = os.getenv('MSEA_CONTACT_EMAIL')
MSEA_THUMB_DIR = os.getenv('MSEA_THUMB_DIR')
MSEA_UPLOAD_DIR = os.getenv('MSEA_UPLOAD_DIR')
MSEA_DOCUMENTS_DIR = os.getenv('MSEA_DOCUMENTS_DIR')
MSEA_DOWNLOAD_DIR = os.getenv('MSEA_DOWNLOAD_DIR')
MSEA_DDL_DIR = os.getenv('MSEA_DDL_DIR')
MSEA_NDST_DB_PATH = os.getenv('MSEA_NDST_DB_PATH')


# User profile management settings.
MSEA_VERIFICATION_TEMPLATE = os.getenv('MSEA_VERIFICATION_TEMPLATE')
MSEA_VERIFICATION_SUBJECT = os.getenv('MSEA_VERIFICATION_SUBJECT')
MSEA_VERIFICATION_FROM = os.getenv('MSEA_VERIFICATION_FROM')
MSEA_VERIFICATION_HOST = os.getenv('MSEA_VERIFICATION_HOST')
MSEA_VERIFICATION_EXPIRY_HOURS = os.getenv('MSEA_VERIFICATION_EXPIRY_HOURS')
 
MSEA_PASSWORD_RESET_TEMPLATE = os.getenv('MSEA_PASSWORD_RESET_TEMPLATE')
MSEA_PASSWORD_RESET_SUBJECT = os.getenv('MSEA_PASSWORD_RESET_SUBJECT')
MSEA_PASSWORD_RESET_FROM = os.getenv('MSEA_PASSWORD_RESET_FROM')
MSEA_PASSWORD_RESET_HOST = os.getenv('MSEA_PASSWORD_RESET_HOST')
MSEA_PASSWORD_RESET_EXPIRY_HOURS = os.getenv('MSEA_PASSWORD_RESET_EXPIRY_HOURS')


# Email forwarder settings.
EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_PORT = os.getenv('EMAIL_PORT')
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = os.getenv('EMAIL_USE_TLS')
SENDGRID_RECOVERY_CODE = os.getenv('SENDGRID_RECOVERY_CODE')


# Maxmind settings.
MAXMIND_ACCOUNT_ID = os.getenv('MAXMIND_ACCOUNT_ID')
MAXMIND_LICENSE_KEY = os.getenv('MAXMIND_LICENSE_KEY')
MAXMIND_GEOIP_PATH = os.getenv('MAXMIND_GEOIP_PATH')


# Extend StaticFilesHandler to add "Access-Control-Allow-Origin" to every response.
# This bypasses the usual static file handler, which does not allow CORS configuration.
# Should not be required in production.
class CORSStaticFilesHandler(handlers.StaticFilesHandler):
    def serve(self, request):
        response = super().serve(request)
        response['Access-Control-Allow-Origin'] = '*'
        return response

# monkeypatch handlers to use our class instead of the original StaticFilesHandler
handlers.StaticFilesHandler = CORSStaticFilesHandler


# Used by test harness.
REST_API = 'http://localhost:9091/rov/api/v1'