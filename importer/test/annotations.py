import sys
import os

sys.path.append(os.path.realpath('./import/scripts')) 

import util

"""
ID - numerical row ID.
code - Unknown.
dive - Dives are identified by a 4-digit code.
transect - Transects are numbers, 1, 2, etc. Some are given 999.
date - m/d/y
time - PDT
FV - unknown (field of view? values don't seem right)
Species - Hart code
Count - integer, including 0
Pan - P, S or C
Length - Null, 0 or a number. Unit unknown probably cm.
Range - Null or number. Unit unknown probably cm.
ps - 
ss - 
pb - 
sb - 
bt - 
rl -  
cx - 
comment - Notes. Start/end transect, etc.
odepth - All 0
odepth_original - All 0
datetime - 
depth - All 0
depth_original - All 0
Transectid - All 0
Lat - All 0
Lon - All 0
Cam-scam - 
Cam-fcam - 
Cam-voice - 
immature - 
other_species - 
habitat, primary - 
habitat, secondary - 
sub-system - 
Habitat-Slope - 
Habitat-Class - 
Habitat-Texture - 
Habitat-Dusting - 
Habitat-Rugosity - 
% - 
bottom morphic - 
bottom depostic - 
bottom texture - 
current speed - 
current direction - 
item - 
biological processes - 
Footage-pf/g/ex - 
New - 
Obs - 
Camera - There are 2 cameras, front (F) and side (S).

"""

if True:
        
    mdb_file = r'c:\Users\SKELLYR\Documents\database\datasets\import\_done\PAC2000-031\2000 WCQCI Bowie Seamount ROV Data.accdb'

    db = util.access(mdb_file)

    # To indicate whether a field has any data or what the types and ranges are.
    field_status = {}

    # Build the lookup table for 
    lookups = {}
    for name, table in (('biocover_type', 'C_Biocover_Type'), ('biocover', 'C_Biocover_Thickness_Type'), ('complexity', 'C_Complexity'), ('relief_type', 'C_Relief_Type'), ('sunstrate_type', 'C_Substrate_Type')):
        if lookups.get(name) == None:
            lookups[name] = {}
        for row, cols in db.query_list(f'select * from [{table}]'):
            lookups[name][row[0]] = row[1]

    csv_file = os.path.splitext(mdb_file)[0] + '.csv'
    with open(csv_file, 'w', newline='') as f:
        cdb = csv.writer(f)

    first = True
    for row, cols in db.query_list('select * from "AMR Data Table"'):
        if first:
            cdb.writerow(cols)
            first = False
        cdb.writerow(row)

if True:
        
    confidence_mapping = {
        
    }
    mdb_file = r"c:\Users\SKELLYR\Documents\database\datasets\import\_done\PAC2000-031\Jon's Bowie Video Database.mdb"

    db = util.access(mdb_file)

    # To indicate whether a field has any data or what the types and ranges are.
    field_status = {}

    # Build the lookup table for 
    lookups = {}
    for name, table in (('data_codes', 'data_codes'), ('species', 'Specieshrt'), ('confidence_ids', 'C_ConfidenceIds'), ('disturbance', 'C_Disturbance'), ('percent', 'C_Percent'), ('substrate_type', 'C_Substrate_Type '), ('survey_mode', 'C_SurveyMode'), ('video', 'C_Video')):
        if lookups.get(name) == None:
            lookups[name] = {}
        for row, cols in db.query_list(f'select * from [{table}]'):
            lookups[name][row[0]] = row[1]

    csv_file = os.path.splitext(mdb_file)[0] + '.csv'
    with open(csv_file, 'w', newline='') as f:
        cdb = csv.writer(f)

    first = True
    for row, cols in db.query_list('select * from "data"'):
        if first:
            cdb.writerow(cols)
            first = False
        cdb.writerow(row)
