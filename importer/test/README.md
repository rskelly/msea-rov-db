# PAC2000-030

## Annotations

There are three Access databases, `Jon's Bowie Video Database.mdb`, `Pac2000-031_videominer.mdb` and `2000 WCQCI Bowie Seamount ROV Data.mdb`. The first seems to be the best one, being built with VideoMiner. The second only has records from 2011. The third has a lot of data.