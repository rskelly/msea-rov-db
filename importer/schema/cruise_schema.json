{
    "$id": "https://msea.science/2024-09/schema/cruise",
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Cruise Schema",
    "description": "A schema describing a cruise.",
    "type": "object",
    "properties": {
        "name": {
            "description": "The name of the cruise.",
            "type": "string"
        },
        "leg": {
            "description": "The cruise leg. A cruise may have more than one leg under the same name. These are numbered from 1.",
            "type": "integer",
            "minExclusive": 0
        },
        "start_time": {
            "description": "The start date and time of the cruise.",
            "type": "string",
            "format": "date-time"
        },
        "end_time": {
            "description": "The end date and time of the cruise.",
            "type": "string",
            "format": "date-time"
        },
        "objective": {
            "description": "The cruise objective.",
            "type": "string"
        },
        "summary": {
            "description": "A summary of the cruise's activities, successes and failures.",
            "type": "string"
        },
        "note": {
            "description": "Operational notes provided by the importer or other actors, which may not be directly relevant to the activities of the cruise itself.",
            "type": "string"
        },
        "crew": {
            "description": "A list of crew members associated with the cruise and their roles.",
            "type": "array",
            "item": {
                "$ref": "https://msea.science/2024-09/schema/cruise_crew"
            },
            "minItems": 1
        },
        "documents": {
            "description": "A list of documents related to the cruise.",
            "type": "array",
            "item": {
                "$ref": "https://msea.science/2024-09/schema/cruise_document"
            }
        },
        "dives": {
            "description": "A list of dives that were conducted during the cruise.",
            "type": "array",
            "item": {
                "$ref": "https://msea.science/2024-09/schema/dive"
            },
            "minItems": 1
        }
    },
    "required": ["name", "leg", "dives", "start_time", "end_time"]
}