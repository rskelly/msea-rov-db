# Cruise Import Schema

This schema defines the structure of a JSON file and associated files that combine to enable the import of a complete cruise, or parts of a cruise, into the database.

## Structure

The import file structure is hierarchical:

- cruise import
    - cruise
        - name
        - leg
        - start time
        - end time
        - crew
        - objective
        - summary
        - notes
        - dives
            - dive
                - name
                - start time
                - end time
                - crew
                - objective
                - summary
                - notes
                - ship configuration
                    - platform
                    - configuration
                    - instrument configurations
                        - instrument configuration
                            - measurment stream
                                - type
                                - unit
                                - data
                                    - timestamp, value
                            - positions
                                - crs
                                - data
                                    - timestamp, x, y, [z]
                - sub configuration
                    - platform
                    - configuration
                    - instrument configurations
                        - instrument configuration
                            - measurments
                                - type
                                - unit
                                - data
                                    - timestamp, value
                            - positions
                                - crs
                                - data
                                    - timestamp, x, y, [z]
                            - annotations
                                - data
                                    - time, type, taxon, points, start time, end time, etc.
                                        - annotation job
                                            - annotation protocol
                - transects
                    - transect
                        - name
                        - start time
                        - end time
                        - objective
                        - summary
                        - notes


