#!/usr/bin/env python

"""
Syncs a local label tree with a remote one. The user must download the label tree
json file from the admin page, then run this script with it and the credentials
for the remote database.
"""

import sys
import os
import json
import psycopg2
from django.db import transaction

# Add application libraries (e.g., models) to the path.
sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))), 'app'))

def check_lookup(obj):
    """
    See if the looked-up object is in the database. Create if necessary.
    """
    from msea.models import rov, shared, intertidal
    
    for mod in [rov, shared, intertidal]:
        try:
            cls = getattr(mod, obj['cls'])
            try:
                # print('Trying to get lookup object.')
                cls.objects.get(short_code=obj['short_code'])
                # print('Found it.')
            except Exception as e:
                print(e)
                # print('Creating lookup object.')
                cls.objects.create(name=obj['name'], short_code=obj['short_code'])
                print('Created new lookup object: ', obj['name'])
        except: pass

def sync(lt_file, config_file, commit):
    """
    Synchronize.
    """
    # Initialize Django and models.
    from msea.util.django import django_setup
    django_setup(config_file)
    
    from msea.models.rov import IQALabelMapPrefill

    try:
        with transaction.atomic():
            print('Opening map file.')
            with open(lt_file, 'r') as f:
                data = json.load(f)
                for item in data:
                    name = item['fields']['name']
                    #print('Loading item: ', name)
                    # Check for any lookup entities in the tag data.
                    properties = item['fields']['properties']
                    for k, v in properties.items():
                        if type(v) == dict and 'short_code' in v.keys():
                            check_lookup(v)

                    tags = item['fields']['tags']

                    try:
                        print('Updating item: ', name)
                        p = IQALabelMapPrefill.objects.get(name=name)
                        p.tags = tags
                        p.properties = properties
                        p.save()
                    except Exception as e:
                        print(e)
                        print('Creating item: ', name)
                        IQALabelMapPrefill.objects.create(
                            name=name,
                            tags=tags,
                            properties=properties
                        )
            if not commit:
                raise Exception('Dry run')
    except Exception as e:
        print(e)

if __name__ == '__main__':

    lt_file = sys.argv[1]
    db_config = sys.argv[2]
    commit = sys.argv[3] if len(sys.argv) > 3 else False

    sync(lt_file, db_config, commit)
