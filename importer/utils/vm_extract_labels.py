#!/usr/bin/env python

"""
This script attempts to extract a list of labels roughly equivalent to
those used in Biigle. This allows the importer to map the labels to
entities in the database. This file is loaded into the Annotator import
app on the website at the create project stage.

Where possible, the script will use tables in the access database for lookups,
otherwise the lookups will have to be added or provided.
"""

import os
import json
import pyodbc
import sys
import sqlite3
from datetime import time, date, datetime, timedelta, timezone
from threading import Thread

def int_or_str(v):
    """
    If the value is an int, parse and return it. Otherwise return the string.
    :param v:
    :return:
    """
    if v is None:
        return None
    try:
        return int(v)
    except:
        v = str(v).strip()
        if len(v) == 0:
            return None
        return v

def connect_access(db_file):
    """
    Connect to the access database.
    :param db_file: The database file. If the extension is "sqlite", will attempt to open as an SQLite file.
    :return: A connection.
    """
    db_file = os.path.realpath(db_file)
    if db_file.lower().endswith('.sqlite'):
        return sqlite3.connect(db_file)
    else:
        dsns = [
            fr'DRIVER={{Microsoft Access Driver (*.mdb, *.accdb)}};DBQ={db_file};',
            fr'DRIVER=MDBTools;DBQ={db_file};'
        ]
        for dsn in dsns:
            try:
                return pyodbc.connect(dsn)
            except Exception as e:
                print(e)
    raise Exception('Failed to find a useable driver for MS Access.')

def load_label_config(filename):
    """
    Load the JSON configuration file. This file maps the columns in the
    annotation table to values stored in the lookups. The lookups must
    be present in the database.

    File structure:
    (object)
        db_file                 - The name of the access file.
        data_table              - The name of the main data table.
        label_tree_file         - The destination label tree file.
        id_column               - The unique identifier. Ideally an integer.
        sort_columns            - The columns on which the data should be sorted before processing.
        data_code_column        - The column containing data codes signifying events.
        time_columns            - The column containing datetime or time. If the latter, a date_column is required, otherwise not.
        time_format             - A format string for the time column.
        date_column             - An optional column containing date if it is missing from the time column.
        date_format             - An optional format string for the date.
        timezone_offset         - If times are in local time, give the offset to bring it back to UTC.
        medium_filename_column  - If media are referenced, give the column name.
        timestamp_column        - The name of the timestamp column to be added to the dataset with a standardized time representation.
        
        data_code_map  (object) - A mapping of data codes to activity names. Can be a string or an array, if there are multiple values for the ID.
            [id]: [name]        
        
        data_code_list (array)  - A list of activities that are applied to every row.
            [name]              - An activity that is applied to every row.

        label_groups (object)   - Extracts groups of columns to concatenate into virtual labels.
            [id]: (object)      - The name of the label group.
                label           - A label for the group.
                labels (arrray)
                    (object)
                        label           - A lookup column label.
                        column          - The source of the lookup value.
                        lookup          - The name of the lookup table.
                        id_column       - The ID column in the lookup table.
                        label_column    - The label column in the lookup table.
                        type            - The type of the looked up value.
                        required        - True if the lookup value is required to be valid.

    :param filename: The configuration filename.
    :return: The dictionary containing configuration data.
    """
    with open(filename, 'r') as f:
        return json.load(f)

def get_lookups(cur, config):
    """
    Generate a dictionary of lookup values where the key is the name of the data column
    and the value is a dictionary with the lookup item ID and label as key and value.
    :param cur: The cursor.
    :param config: The main config object.
    :return: A dictionary of lookups.
    """
    lookups = {}    # List of formatted labels.
    for dc, group in config['label_groups'].items():
        if group.get('labels'):
            for label in group.get('labels'):
                if label.get('column'):
                    name = label['column']
                    if label.get('lookup'):
                        lookups[name] = {}
                        # If the item has a lookup, query it.
                        sql = f"select [{label['id_column']}], [{label['label_column']}] from [{label['lookup']}]"
                        try:
                            cur.execute(sql)
                            for pk, value in cur:
                                pk = int_or_str(pk)
                                lookups[name][pk] = value
                        except Exception as e:
                            raise Exception(f'Error in query. The fields may be misnamed. ({sql}): ' + str(e))

    return lookups

def get_label_tree(filename):
    """
    Load the label tree.
    :param config: The config object.
    :return: A dictionary containing the label mapping from ID to label name.
    """
    labels = {}
    with open(filename, 'r') as f:
        project = json.load(f)
        for tree in project['labelTrees']:
            for label in tree['labels']:
                labels[label['id']] = label['name']
    return labels

def get_actions(row, config):
    """
    Returns the action for the row (based on the data code), plus actions that apply to every row.
    :param row: The database row.
    :param config: The config object.
    :return: A list of actions.
    """
    # Get the data code given by the row.
    dc = config['data_code_map'].get(str(row[config['data_code_column']]))

    if not dc:
        # If there's no mapped code, make an empty list.
        dc = []
    elif type(dc) != list:
        # If the dc is not a list, turn it into one.
        dc = [dc]

    # Get the data code(s) that apply to all rows.
    all_rows = config['data_code_map'].get('-', [])
    if all_rows and type(all_rows) != list:
        # If an all-rows value is given but is not a list, wrap it in one.
        all_rows = [all_rows]

    # Add the all_rows to the dc list.
    dc.extend(all_rows)

    # Filter out the duplicates.
    return list(set(dc))


def get_group_labels(row, groups, lookups, config):
    """
    This method returns the label groups that apply to the current row.
    :param row: The data row.
    :param groups: A list of the configured label column groups.
    :param lookups: A dictionary of lookups.
    :param config: The configuration object.
    :return: A list of labels.
    """
    # Iterate over the field groups and append the labels to the labels dict.
    labels = {}
    for name, group in groups.items():
        group_label = group['label']

        if not labels.get(name):
            # If there's no space in the dict for the action, add a set.
            labels[name] = set()

        # Each item in the labels list is turned into a separate label as {group name} > {label name}: {value}
        # If a lookup is provided, the value will be looked up. If the value is None/empty, won't be created.
        # If one element of the group is required and is not set, the entire label is scrapped.
        if group.get('labels'):
            for label in group['labels']:
                # The base label text.
                label_label = label['label']

                if 'value' in label.keys():
                    # If a value is hard-coded, use it.
                    v = int_or_str(label['value'])

                elif label.get('column'):
                    # Get the column value.
                    v = int_or_str(row[label['column']])
                    if v is not None and label.get('lookup'):
                        # If there's a lookup, use the column value to find the lookup value.
                        try:
                            v = int_or_str(lookups[label['column']][v])
                        except Exception as e:
                            #raise Exception(f"A value was not found in the lookup table {label['lookup']} for key {v}.")
                            print(f"A value was not found in the lookup table {label['lookup']} for key {v}.")
                            v = None

                if v is not None:
                    # Append the value to the label.
                    labels[name].add(f"{group_label} > {label_label}: {v}".strip())
                elif label.get('required', False):
                    # The element is required and not present. Scrap it.
                    del labels[name]
                    break

        # Each item in the columns list is concatenated onto the label as
        # {group name} > {label name}: {value}, {label name}: {value}, [etc.]
        # If an element is required and is not set, the entire label is scrapped.
        # If a lookup is provided, the value will be looked up.
        if group.get('columns'):
            label_text = []
            for label in group['columns']:
                # The base label text.
                label_label = label['label']

                if 'value' in label.keys():
                    # If a value is hard-coded, use it.
                    v = int_or_str(label['value'])

                elif label.get('column'):
                    # Get the column value.
                    v = int_or_str(row[label['column']])
                    if v and label.get('lookup'):
                        # If there's a lookup, use the column value to find the lookup value.
                        try:
                            v = int_or_str(lookups[label['column']][v])
                        except Exception as e:
                            #raise Exception(f"A value was not found in the lookup table {label['lookup']} for key {v}.")
                            print(f"A value was not found in the lookup table {label['lookup']} for key {v}.")
                            v = None

                if v is not None:
                    # Append the label.
                    label_text.append(f"{label_label}: {v}".strip())
                elif label.get('required', False):
                    # The element is required and not present. Scrap it.
                    del labels[name]
                    break

            if len(label_text):
                # If the label_text isn't empty, build the label.
                labels[name].add(f"{group_label} > {', '.join(label_text)}".strip())

        if not group.get('labels') and not group.get('columns'):
            # There're no labels and no columns, make a blank label.
            labels[name].add(f"{group_label}".strip())

    return {k: list(v) for k, v in labels.items() if len(v)}

def build_label_tree(config_file):
    """
    Build the label tree. The label tree is saved as a JSON file in a format roughly
    similar to that of a Biigle label tree, though the IDs in the entities aren't real.

    The configuration gives a list of fields relevant to labels. This script concatenates
    the column values of each of those fields, swapping out lookup values where necessary
    and creates a list of unique

    :param config_file: The configuration file (JSON).
    """
    # Load the configuration.
    config = load_label_config(config_file)
    # Extract the DB file
    db_file = os.path.join(os.path.dirname(config_file), config['db_file'])

    # Connect to the DB and get a cursor.
    conn = connect_access(db_file)
    cur = conn.cursor()

    # Get the lookup dictionary.
    lookups = get_lookups(cur, config)

    # Select all the data.
    cur.execute(f"select * from [{config['data_table']}]")
    cols = [col[0] for col in cur.description]

    labels = set()              # Set of labels to prevent duplicates.
    groups = config['label_groups']   # The list of label group configs.

    # Iterate over the data rows.
    for row in cur:
        row = dict(zip(cols, row))

        # Get the label goups that can be extracted from this row and update the labels set.
        group_labels = get_group_labels(row, groups, lookups, config)
        for gl in group_labels.values():
            labels.update(gl)

    # Format the label list to look like a Biigle tree.
    labels = list(labels)
    label_id = 1
    for i in range(len(labels)):
        labels[i] = {
            'id': label_id,
            'parent_id': None,
            'source_id': None,
            'label_source_id': None,
            'label_tree_id': 1,
            'name': labels[i]
        }
        label_id += 1

    # Save the label tree.
    with open(os.path.join(os.path.dirname(config_file), config['label_tree_file']), 'w') as f:
        f.write(json.dumps({
            '_about': 'Produced by vm_extract_labels.py',
            'id': 1,
            'name': 'VideoMiner Label Tree for ' + os.path.basename(db_file),
            'labelTrees': [{
                'id': 1,
                'name': 'Label Tree',
                'description': 'There is only one label tree.',
                'labels': labels
            }]
        }, indent=4))

def get_timestamp(row, config):
    """
    Get the timestamp for the row. If a time column is specified with no date column, the
    time column is expected to contain an entire date time. If the date column is provided,
    it is expected to contain a date while the time column contains the time.
    :param row:
    :param config:
    :return:
    """
    # Format the date.
    if config.get('time_column'):
        # Extract the time field.
        tm = row[config['time_column']]
        if type(tm) is int:
            try:
                tm = datetime.fromtimestamp(tm / 1000 if tm > 4200000000 else tm)
            except Exception as e:
                raise Exception(f'Could not parse timestamp {row[config["time_column"]]}: ' + str(e))
        elif not type(tm) in (time, datetime):
            try:
                tm = datetime.strptime(row[config['time_column']].replace(' ', ''), config['time_format'])
            except Exception as e:
                raise Exception(f'Could not parse time {row[config["time_column"]]}: ' + str(e))
        tm = tm.replace(tzinfo=timezone.utc)
        del row[config['time_column']]

        if config.get('date_column'):
            # Extract the date field.
            dt = row[config['date_column']]
            if not type(dt) in (date, time, datetime,):
                dt = datetime.strptime(row[config['date_column']].replace(' ', ''), config['date_format'])
            dt = dt.replace(tzinfo=timezone.utc)
            del row[config['date_column']]

            # Timestamp by date and time.
            tm = dt + timedelta(hours=tm.hour, minutes=tm.minute, seconds=tm.second)

        if config.get('timezone_offset', None) is not None:
            tm = tm + timedelta(hours=config['timezone_offset'])

        return tm
    else:
        raise Exception('The configuration must have a time_column or a time_column and date_column.')


def run_gui():
    """
    Run the program with a GUI interface.
    """
    from tkinter import Tk, StringVar
    from tkinter.ttk import Frame, Label, Entry, Button
    from tkinter.filedialog import askopenfilename
    from tkinter import messagebox

    class app:

        def __init__(self, root):
            super().__init__()
            self.filename = StringVar(root, value="")

        def select_intput_file(self):
            """
            Callback function for selecting the filename.
            """
            self.filename.set(askopenfilename())

        def run(self):
            build_label_tree(self.filename.get())

    root = Tk()
    data = app(root)
    frm = Frame(root)
    frm.grid()

    row = 0

    Label(frm, text='VideoMiner Config File').grid(column=0, row=row, sticky="ew")
    Entry(frm, textvariable=data.filename).grid(column=1, row=row, sticky="w")
    Button(frm, text="Select File...", command=data.select_intput_file).grid(column=2, row=row, sticky="w")
    row += 1

    grp = Frame(root)
    Button(grp, text="Run", command=data.run).grid(column=0, row=row, sticky="w")
    Button(grp, text="Quit", command=root.destroy).grid(column=2, row=row, sticky="w")
    grp.grid(column=0, row=row, stick="e")

    root.mainloop()

if __name__ == '__main__':

    config_file = sys.argv[1]
    if config_file == '-g':
        run_gui()
    else:
        build_label_tree(config_file)



