#!/usr/bin/env python

"""
SeaTube data extraction program. Accesses the data services on the site
and exports some different kinds of data.

1) Get lists of the cruises, dives, etc.
2) Get a CSV stream containing annotations (log entries) for dives.
"""
import argparse
import csv
import requests

url_tree = 'https://data.oceannetworks.ca/expedition/tree'
url_annots = 'https://data.oceannetworks.ca/seatubeV3/annotations?diveIds={dive_ids}'

def crawl_tree(items, key, value):
    """
    Crawl the tree and return the first object containing the given key and value.
    """
    def _crawl(items):
        for item in items:
            if type(item) == dict and item.get(key) == value:
                return item
            elif type(item) == list:
                return _crawl(item)
        return None
    return _crawl(items)
    
def organisations(tree):
    exp = crawl_tree(tree['payload']['videoTreeConfig'], 'title', 'Expeditions')
    return exp['children']

def organisation(tree, name):
    exp = crawl_tree(tree['payload']['videoTreeConfig'], 'title', 'Expeditions')
    return crawl_tree(exp['children'], 'title', name)
    
def years(tree, org_name):
    org = organisation(tree, org_name)
    return org['children']

def year(tree, org_name, yr):
    org = organisation(tree, org_name)
    return crawl_tree(org['children'], 'title', str(yr))

def cruises(tree, org_name, yr):
    yr = year(tree, org_name, yr)
    return yr['children']

def cruise(tree, org_name, yr, cr):
    yr = year(tree, org_name, str(yr))
    return crawl_tree(yr['children'], 'title', cr)

def dives(tree, org_name, yr, cr):
    cr = cruise(tree, org_name, yr, cr)
    return cr['children']

def dive(tree, org_name, yr, cr, dv):
    yr = cruise(tree, org_name, str(yr), cr)
    return crawl_tree(yr['children'], 'title', dv)

def run(o=None, y=None, e=None, d=None, t=None, c=None, f=None):

    tree = requests.get(url_tree).json()
    if not o:
        orgs = organisations(tree)
        print('Organisations:')
        for org in orgs:
            print('-', org['title'])
        return
    o = organisation(tree, o)['title']
    if not y:
        yrs = years(tree, o)
        print('Years:')
        for yr in yrs:
            print('-', yr['title'])
        return
    y = year(tree, o, y)['title']
    if not c:
        crs = cruises(tree, o, y)
        print('Cruises:')
        for cr in crs:
            print('-', cr['title'])
        return
    c = cruise(tree, o, y, c)['title']
    if not d:
        dvs = dives(tree, o, y, c)
        if not t:
            print('Dives:')
            for dv in dvs:
                print('-', dv['title'], dv['id'])
            return
        else:
            d = [o['id'] for o in dvs]
    else:
        d = [dive(tree, o, y, d)['id']]

    if t == 'annotations':
        url = url_annots.format(dive_ids=','.join(list(map(str, d))))
        data = requests.get(url).json()
        first = True
        with open(f, 'w', newline='') as f:
            db = csv.writer(f)
            for annot in data['payload']['annotations']:
                row = []
                if first:
                    for k in sorted(annot.keys()):
                        v = annot[k]
                        if type(v) == dict:
                            for kk in sorted(v.keys()):
                                row.append(k + '_' + kk)
                        else:
                            row.append(k)
                    db.writerow(row)
                    first = False

                row = []
                for k in sorted(annot.keys()):
                    v = annot[k]
                    if type(v) == dict:
                        for kk in sorted(v.keys()):
                            row.append(v[kk])
                    else:
                        row.append(v)
                db.writerow(row)

if __name__ == '__main__':

    args = argparse.ArgumentParser('seatube_data.py', 'python seatube_data.py <flags>')
    args.add_argument('-o', '--organisation', required=False, help='The name of the organisation which ran the expedition.')
    args.add_argument('-y', '--year', required=False, help='The year of the expedition. An organisation is required.')
    args.add_argument('-e', '--expedition', required=False, help='The name of the expedition. A year is required.')
    args.add_argument('-c', '--cruise', required=False, help='The name of the cruise. An expedition is required.')
    args.add_argument('-d', '--dive', required=False, help='A comma-delimited list of dives. A cruise is required.')
    args.add_argument('-t', '--type', required=False, help='Print out the annotations for the selected dive as a CSV stream. If a dive is not given, the annotations for all dives will be printed.')
    args.add_argument('-f', '--file', required=False, help='Output file for result.')

    args = args.parse_args()
    
    run(o=args.organisation, y=args.year, e=args.expedition, d=args.dive, t=args.type, c=args.cruise, f=args.file)