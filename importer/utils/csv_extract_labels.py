#!/usr/bin/env python

"""
This script attempts to extract a list of labels roughly equivalent to
those used in Biigle. This allows the importer to map the labels to
entities in the database. This file is loaded into the Annotator import
app on the website at the create project stage.
"""

import os
import json
import pyodbc
import sys
import csv
from datetime import time, date, datetime, timedelta, timezone

def int_or_str(v):
    """
    If the value is an int, parse and return it. Otherwise return the string.
    :param v:
    :return:
    """
    if v is None:
        return None
    try:
        return int(v)
    except:
        v = str(v).strip()
        if len(v) == 0:
            return None
        return v

def load_label_config(filename):
    """
    Load the JSON configuration file. This file maps the columns in the
    annotation table to values stored in the lookups. The lookups must
    be present in the database.

    File structure:
    (object)
        db_file                 - The name of the CSV file.
        label_tree_file         - The destination label tree file.
        id_column               - The unique identifier. Ideally an integer.
        label_column            - The columns that will be mapped. Can be an array.
        time_columns            - The column containing datetime or time. If the latter, a date_column is required, otherwise not.
        time_format             - A format string for the time column.
        date_column             - An optional column containing date if it is missing from the time column.
        date_format             - An optional format string for the date.
        timezone_offset         - If times are in local time, give the offset to bring it back to UTC.
        medium_filename_column  - If media are referenced, give the column name.
        timestamp_column        - The name of the timestamp column to be added to the dataset with a standardized time representation.

    :param filename: The configuration filename.
    :return: The dictionary containing configuration data.
    """
    with open(filename, 'r') as f:
        return json.load(f)

def get_label_tree(filenames):
    """
    Load the label tree.
    :param filename: The name of the label tree file, or an array of same.
    :return: A dictionary containing the label mapping from ID to label name.
    """
    if type(filenames) == str:
        filenames = [filenames]
    labels = {}
    for filename in filenames:
        with open(filename, 'r') as f:
            project = json.load(f)
            for tree in project['labelTrees']:
                for label in tree['labels']:
                    labels[label['id']] = label['name']
    return labels

def build_label_tree(config_file):
    """
    Build the label tree. The label tree is saved as a JSON file in a format roughly
    similar to that of a Biigle label tree, though the IDs in the entities aren't real.

    The configuration gives a list of fields relevant to labels. This script concatenates
    the column values of each of those fields, swapping out lookup values where necessary
    and creates a list of unique

    :param config_file: The configuration file (JSON).
    """
    # Load the configuration.
    config = load_label_config(config_file)
    # Extract the DB files.
    file_names = config['db_file']
    if type(file_names) == str:
        file_names = [file_names]
    db_files = [os.path.join(os.path.dirname(config_file), x) for x in file_names]

    # Set of labels to prevent duplicates.
    labels = set()                      
    personnel = {}

    # Open the file, get the header.
    for db_file in db_files:
        with open(db_file, 'r') as f:
            db = csv.reader(f)
            # Lower case header.
            head = list(map(str.lower, next(db)))
            # Lower case label column. If it is not a list, turn it into one.
            label_column = config['label_column']
            if not type(label_column) == list:
                label_column = [label_column]
            label_column = list(map(str.lower, label_column))
            personnel_column = config.get('annotator_column')

            # Lower case personnel column. If it is not a list, turn it into one.
            # If one column is given, it will be placed in the first and last name. If two are
            # given, first and last name are assumed. Further columns will be ignored.
            if personnel_column:
                if not type(personnel_column) == list:
                    personnel_column = [personnel_column]
                if len(personnel_column) > 2:
                    personnel_column = personnel_column[:2]
                personnel_column = list(map(str.lower, personnel_column))

            # Iterate over the data rows.
            person_id = 1
            for row in db:
                if not any(row):
                    continue
                row = dict(zip(head, row))
                
                # Extract the labels.
                for col in label_column:
                    value = row[col].strip()
                    if value:
                        labels.add(value)

                # Join the personnel columns into a name.
                if personnel_column and not personnel.get(''.join(personnel_column)):
                    personnel[''.join(personnel_column)] = {
                        'member': {
                            'id': person_id,
                            'first_name': row[personnel_column[0]], 
                            'last_name': row[personnel_column[1] if len(personnel_column) > 1 else personnel_column[0]],
                            'project_role_id': 1,
                        }
                    }
                    person_id += 1

    # Get the personnel objects as a list.
    personnel = list(personnel.values())

    # Format the label list to look like a Biigle tree.
    labels = list(labels)
    label_id = 1
    for i in range(len(labels)):
        labels[i] = {
            'id': label_id,
            'parent_id': None,
            'source_id': None,
            'label_source_id': None,
            'label_tree_id': 1,
            'name': labels[i]
        }
        label_id += 1

    # Save the label tree.
    with open(os.path.join(os.path.dirname(config_file), config['label_tree_file']), 'w') as f:
        f.write(json.dumps({
            '_about': 'Produced by csv_extract_labels.py',
            'id': 1,
            'name': 'CSV Label Tree for ' + os.path.basename(db_file),
            'labelTrees': [{
                'id': 1,
                'name': 'Label Tree',
                'description': 'There is only one label tree.',
                'labels': labels
            }],
            "members": personnel
        }, indent=4))

def get_timestamp(row, config):
    """
    Get the timestamp for the row. If a time column is specified with no date column, the
    time column is expected to contain an entire date time. If the date column is provided,
    it is expected to contain a date while the time column contains the time.
    :param row:
    :param config:
    :return:
    """
    if config.get('timestamp_column'):
        return datetime.strptime(row[config['timestamp_column']], '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=timezone.utc)
    
    # Format the date.
    if config.get('time_column'):
        time_column = config['time_column'].lower()
        time_format = config['time_format']
        # Extract the time field.
        tm = row[time_column]
        if not type(tm) in (time, datetime):
            tm = datetime.strptime(row[time_column].replace(' ', ''), time_format)
        tm = tm.replace(tzinfo=timezone.utc)
        del row[time_column]

        if config.get('date_column'):
            date_column = config['date_column'].lower()
            date_format = config['date_format']
            # Extract the date field.
            dt = row[date_column]
            if not type(dt) in (date, time, datetime,):
                dt = datetime.strptime(row[date_column].replace(' ', ''), date_format)
            dt = dt.replace(tzinfo=timezone.utc)
            del row[date_column]

            # Timestamp by date and time.
            tm = dt + timedelta(hours=tm.hour, minutes=tm.minute, seconds=tm.second)

        if config.get('timezone_offset', None) is not None:
            tm = tm + timedelta(hours=config['timezone_offset'])

        return tm
    else:
        raise Exception('The configuration must have a time_column or a time_column and date_column.')


if __name__ == '__main__':

    config_file = sys.argv[1]
    build_label_tree(config_file)



