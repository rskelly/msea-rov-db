#!/usr/bin/env python

"""
Extract taxonomic records from a downloaded archive file and insert them into the taxonomy table.

Currently these sources are accepted:
- WoRMS
    This is the authoritative taxonomic database that OBIS and other databases rely on. It is 
    available as a downloadable file set from https://marinespecies.org/downloads, but permission 
    and a user account are required. It does not include a list of vernaculars, so those must be
    downloaded via the ReST API at https://marinespecies.org/rest/AphiaVernacularsByAphiaID/{ID}.
- OBIS
    This provides observations with a reference to the Aphia ID (WoRMS) and several
    important fields. This is an enormous archive, which is read directly from the compressed
    form which is currently 11GB. Download the zipped CSV form at:
    https://obis.org/data/access
- iNaturalist
    This provides observations from iNaturalist. Export the file from
    https://www.inaturalist.org/observations/export by selecting the ID, all Taxon fields
    and all Taxon Extra fields. Ideally, navigate here from the project page to narrow the
    export to relevant observations.

The script will extract unique species information from the observations (to remove duplicates)
and save them directly to the database with a source identifier. 
"""
import csv
import io
import zipfile
import os
import sys
import psycopg2
import psycopg2.extras
import requests
from time import sleep

def sentence_case(v):
    if v:
        v = str(v)
        return v[0].upper() + v[1:].lower()
    return v

def title_case(v):
    if v:
        v = str(v)
        return ' '.join(list(map(lambda a: sentence_case(a), v.split())))
    return v

def lower_case(v):
    try:
        return str(v).lower()
    except:
        return None

def _str(v):
    if v:
        return str(v)
    return None

def _urn(v):
    if v:
        _, _, id = v.rpartition(':')
        return int(id)
    return None

# Constants.

hart_species_code = 'speciescode' # species_code
hart_rank = 'taxonomyclasslevelcode' # taxonomic_rank

# List of Hart fields.
## From GFBio.
_hart_fields = list(map(str.lower, [
    'species_code', 'species_science_name', 'species_common_name',
    'rank', # Added
]))

## From VideoMiner.
hart_fields = list(map(str.lower, [
    'speciescode', 'scientificname', 'commonname', 
    'rank', # Added
]))

# Map hart rank codes onto field names.
hart_rank_map = {
    'NOT': None,
    'KNG': 'kingdom',
    'SPP': 'superphylum',
    'PHY': 'phylum',
    'SBP': 'subphylum',
    'CLS': 'class',
    'SBC': 'subclass',
    'FAM': 'family',
    'SPE': 'species',
    'GEN': 'genus',
    'ORD': 'order',
    'SBO': 'suborder',
    'SBF': 'subfamily',
    'SPF': 'superfamily',
    'SSP': 'subspecies',
    'HYB': 'hybrid',
    'SPC': 'subclass',
    'IFO': 'infraorder',
    'SPO': 'superorder',
    'IFC': 'infraclass',
    'SBS': 'subsection',
}

# Mapping from iNaturalist fields to database fields.
# From VideoMiner.
_hart_field_map = {
    'species_science_name': ('scientific_name', sentence_case),
    'species_common_name': ('common_name', title_case),
    'rank': ('rank', _str),
    'species_code': ('taxon_id', _str),
}

## From VideoMiner.
hart_field_map = {
    'scientificname': ('scientific_name', sentence_case),
    'commonname': ('common_name', title_case),
    'rank': ('rank', _str),
    'speciescode': ('taxon_id', _str),
}

# iNaturalist report fields.
inat_fields = list(map(str.lower, [
    # 'id', 'species_guess', 
    'rank', # Added at runtime.
    'scientific_name', 'common_name', 
    #'iconic_taxon_name', 
    'taxon_id', 
    'taxon_kingdom_name', 'taxon_phylum_name', 'taxon_subphylum_name', 'taxon_superclass_name', 
    'taxon_class_name', 'taxon_subclass_name', 'taxon_superorder_name', 'taxon_order_name', 
    'taxon_suborder_name', 'taxon_superfamily_name', 'taxon_family_name', 'taxon_subfamily_name',
    'taxon_supertribe_name', 'taxon_tribe_name', 'taxon_subtribe_name', 'taxon_genus_name', 
    'taxon_genushybrid_name', 'taxon_species_name', 'taxon_hybrid_name', 'taxon_subspecies_name', 
    'taxon_variety_name', 'taxon_form_name'
]))

# List of fields that, in order, comprise the taxonomic ranks from highest to lowest.
# The actual rank name comes from the field map, so the database field name for the rank
# is the name of the rank itself.
inat_rank_fields = inat_fields[6:]      

# Mapping from iNaturalist fields to database fields.
inat_field_map = {
    'scientific_name': ('scientific_name', sentence_case),
    'common_name': ('common_name', title_case),
    'rank': ('rank', _str),
    'taxon_id': ('taxon_id', _str),
    'taxon_kingdom_name': ('kingdom', _str),
    'taxon_phylum_name': ('phylum', _str),
    'taxon_subphylum_name': ('subphylum', _str),
    'taxon_superclass_name': ('superclass', _str),
    'taxon_class_name': ('class', _str),
    'taxon_subclass_name': ('subclass', _str),
    'taxon_superorder_name': ('superorder', _str),
    'taxon_order_name': ('order', _str),
    'taxon_suborder_name': ('suborder', _str),
    'taxon_superfamily_name': ('superfamily', _str),
    'taxon_family_name': ('family', _str),
    'taxon_subfamily_name': ('subfamily', _str),
    'taxon_supertribe_name': ('supertribe', _str),
    'taxon_tribe_name': ('tribe', _str),
    'taxon_subtribe_name': ('subtribe', _str), 
    'taxon_genus_name': ('genus', _str),
    'taxon_genushybrid_name': ('genus_hybrid', _str),
    'taxon_species_name': ('species', _str),
    'taxon_hybrid_name': ('hybrid', _str),
    'taxon_subspecies_name': ('subspecies', _str),
    'taxon_variety_name': ('variety', _str),
    'taxon_form_name': ('form', _str),
}

obis_fields = list(map(str.lower, [
    #'id',
    'scientificName', 'vernacularName',
    #'originalScientificName', 'marine', 'brackish', 'freshwater', 'terrestrial', 'taxonRank', 
    'AphiaID', 
    'rank',
    'superdomain', 'domain', 'kingdom', 'subkingdom', 'infrakingdom', 'phylum', 'phylum_division', 'subphylum_subdivision', 
    'subphylum', 'infraphylum', 'parvphylum', 'gigaclass', 'megaclass', 'superclass', 'class', 'subclass', 'infraclass', 'subterclass', 
    'superorder', 'order', 'suborder', 'infraorder', 'parvorder', 'superfamily', 'family', 'subfamily', 'supertribe', 'tribe', 
    'subtribe', 'genus', 'subgenus', 'section', 'subsection', 'series', 'species', 'subspecies', 'natio', 'variety', 'subvariety', 
    'forma', 'subforma', 
]))

obis_rank_fields = obis_fields[9:]

obis_field_map = {
    'scientificname': ('scientific_name', sentence_case),
    'rank': ('rank', _str),
    'vernacularname': ('common_name', title_case),
    'aphiaid': ('taxon_id', _str),
    'superdomain': ('superdomain', _str),
    'domain': ('domain', _str),
    'kingdom': ('kingdom', _str),
    'subkingdom': ('subkingdom', _str), 
    'infrakingdom': ('infrakingdom', _str),
    'phylum': ('phylum', _str),
    'phylum_division': ('phylum_division', _str),
    'subphylum_subdivision': ('subphylum_subdivision', _str),
    'subphylum': ('subphylum', _str),
    'infraphylum': ('infraphylum', _str),
    'parvphylum': ('parvphylum', _str),
    'gigaclass': ('gigaclass', _str),
    'megaclass': ('megaclass', _str),
    'superclass': ('superclass', _str),
    'class': ('class', _str),
    'subclass': ('subclass', _str), 
    'infraclass': ('infraclass', _str),
    'subterclass': ('subterclass', _str),
    'superorder': ('superorder', _str),
    'order': ('order', _str),
    'suborder': ('suborder', _str), 
    'infraorder': ('infraorder', _str),
    'parvorder': ('parvorder', _str),
    'superfamily': ('superfamily', _str),
    'family': ('family', _str),
    'subfamily': ('subfamily', _str),
    'supertribe': ('supertribe', _str),
    'tribe': ('tribe', _str),
    'subtribe': ('subtribe', _str), 
    'genus': ('genus', _str),
    'subgenus': ('subgenus', _str), 
    'section': ('section', _str),
    'subsection': ('subsection', _str),
    'series': ('series', _str),
    'species': ('species', _str),
    'subspecies': ('subspecies', _str),
    'natio': ('natio', _str),
    'variety': ('variety', _str),
    'subvariety': ('subvariety', _str),
    'forma': ('form', _str),
    'subforma': ('subform', _str),
}

worms_fields = [
    'taxonID', 
    #'scientificNameID', 
    'acceptedNameUsageID', 'parentNameUsageID', 
    #'namePublishedInID', 
    'scientificName', 
    #'acceptedNameUsage', 'parentNameUsage', 'namePublishedIn', 'namePublishedInYear', 
    'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'subgenus', 
    #'specificEpithet', 'infraspecificEpithet', 
    'taxonRank', 
    #'scientificNameAuthorship', 'nomenclaturalCode', 'taxonomicStatus', 'nomenclaturalStatus', 
    #'modified', 'bibliographicCitation', 'references', 'license', 'rightsHolder', 'datasetName', 'institutionCode', 'datasetID'
]

worms_field_map = {
    'taxonID': ('taxon_id', _urn),
    'acceptedNameUsageID': ('accepted_taxon_id', _urn),
    'parentNameUsageID': ('parent_taxon_id', _urn),
    'scientificName': ('scientific_name', _str),
    'taxonRank': ('rank', lower_case),
    'kingdom': ('kingdom', _str),
    'phylum': ('phylum', _str),
    'class': ('class', _str),
    'order': ('order', _str),
    'family': ('family', _str),
    'genus': ('genus', _str),
    'subgenus': ('subgenus', _str),
}

def get_rank(row, rank_fields, field_map):
    """
    Get the taxonomic rank by finding the last field name in rank_fields with a value in the
    row. The rank is then determined by extracting the DB column name for the field using the field
    map. The DB columns are named with the rank names.
    """
    rank = None
    for rf in rank_fields:
        if row[rf]:
            rank = field_map[rf][0]
    return rank

def insert_row(cur, taxon_id, source, row, field_list, field_map):
    """
    """
    # Get the field names. The name is the first element in the mapped tuple.
    sql_fields = ['"source"'] + list(map(lambda a: f'"{a}"', [field_map[f][0] for f in field_list]))
    # Get and cast the values. The cast is the second element in the mapped tuple.
    sql_values = [source] + [field_map[f][1](row[f]) for f in field_list]
    cur.execute('select taxon_id from taxonomy.taxon where taxon_id=%s and source=%s', [str(taxon_id), source])
    try:
        id, = cur.fetchone()
    except:
        id = None
    if id:
        taxon_id_idx = sql_fields.index('"taxon_id"')
        sql_update_params = ','.join([f'{f}=%s' for f in sql_fields[:taxon_id_idx] + sql_fields[taxon_id_idx + 1:]])
        sql = f'update taxonomy.taxon set {sql_update_params} where "taxon_id"=%s'
        cur.execute(sql, sql_values[:taxon_id_idx] + sql_values[taxon_id_idx + 1:] + [taxon_id])
    else:
        # Assemble a list of param placeholders.
        sql_insert_fields = ','.join(sql_fields)
        sql_insert_params = ','.join(['%s'] * len(sql_values))
        sql = f'insert into taxonomy.taxon ({sql_insert_fields}) values ({sql_insert_params})'
        cur.execute(sql, sql_values)

def _null(s):
    if type(s) == str:
        s = s.strip()
    if not s:
        return None
    return s

def insert_rows(cur, source, rows, field_map):
    """
    """
    for i in range(len(rows)):
        rows[i] = {field_map[k][0]: field_map[k][1](v) for k, v in rows[i].items() if k in field_map.keys()}
    fields = list(rows[0].keys())
    # Get the field names. The name is the first element in the mapped tuple.
    sql_fields = ','.join(['"source"'] + [f'"{f}"' for f in fields])
    update_fields = ','.join([f'"{f}"=EXCLUDED."{f}"' for f in fields])
    # Get and cast the values. The cast is the second element in the mapped tuple.
    sql_values = [tuple([source] + [_null(row[f]) for f in fields]) for row in rows]

    psycopg2.extras.execute_values(
        cur, 
        f'''insert into taxonomy.taxon ({sql_fields}) values %s 
            on conflict (source, taxon_id) do update set {update_fields}''',
        sql_values,
        template=None,
        page_size=10000
    )

def process_worms(infile, conn):
    """
    Load records from the WoRMS database.
    """
    cur = conn.cursor()
    
    # We don't want to do this in case vernaculars have been loaded.
    #cur.execute("delete from taxonomy.taxon where source in ('worms')")

    count = 0
    with open(infile, 'r', encoding='utf-8-sig') as fi:
        count = sum(1 for line in fi)

    with open(infile, 'r', encoding='utf-8-sig') as fi:
        dbi = csv.reader(fi, delimiter='\t')
        head = next(dbi)

        try:    
            rows = []
            num = 0
            for row in dbi:
                row = dict(zip(head, row))
                aphia_id = _urn(row['taxonID'])
                parent_id = _urn(row['parentNameUsageID'])
                accepted_id = _urn(row['acceptedNameUsageID'])
                # If either of these IDs point to themselves, null them. 
                # We don't want cycles.
                if parent_id == aphia_id:
                    row['parentNameUsageID'] = None
                if accepted_id == aphia_id:
                    row['acceptedNameUsageID'] = None
                rows.append(row)
                if len(rows) >= 10000:
                    num += len(rows)
                    insert_rows(cur, 'aphia', rows, worms_field_map)
                    print(num, 'of', count, 'lines processed.')
                    rows = []
            num += len(rows)
            insert_rows(cur, 'aphia', rows, worms_field_map)
            print(num, 'of', count, 'lines processed.')
            rows = []
        except Exception as e:
            print(e)
            
    conn.commit()


def process_vernaculars(conn):
    """
    Process the obis records to get their vernaculars.
    """
    cur = conn.cursor()
    for col in ('common_name', 'scientific_name'):
        cur.execute(f"update taxonomy.taxon set {col}=NULL where trim({col})=''")
    cur.execute("""
                select count(*) 
                from taxonomy.taxon 
                where source='aphia' and common_name is null and not no_common_name
                """)
    count, = cur.fetchone()
    cur.execute("""
                select taxon_id 
                from taxonomy.taxon 
                where source='aphia' and common_name is null and not no_common_name 
                order by taxon_id
                """)
    ids = cur.fetchall()
    for aphia_id, in ids:
        print(f'Fetching ID {aphia_id}; {count} remaining.')
        res = requests.get(f'https://www.marinespecies.org/rest/AphiaVernacularsByAphiaID/{aphia_id}')
        found = False
        if res.status_code == 200:
            for item in res.json():
                try:
                    if item['language'] == 'English' and item['vernacular']:
                        cur.execute("""
                                    update taxonomy.taxon 
                                    set common_name=%s 
                                    where source='aphia' and taxon_id=%s
                                    """, [title_case(item['vernacular']), aphia_id])
                        conn.commit()
                        found = True
                        break
                except Exception as e:
                    print(e)
        if not found:
            cur.execute("""
                        update taxonomy.taxon 
                        set no_common_name='t' 
                        where taxon_id=%s and source='aphia'
                        """, [aphia_id])
            conn.commit()
        count -= 1
        sleep(0.1)

def process_hart(infile, conn):
    """
    Process records from a Hart code CSV file "SpeciesCodes_fromGFBio.csv".
    """

    cur = conn.cursor()
    #cur.execute("delete from taxonomy.taxon where source='hart'")

    hart_ids = set()
    
    with open(infile, 'r', encoding='utf-8-sig') as fi:
        dbi = csv.reader(fi)
        head = list(map(str.lower, next(dbi)))
    
        rows = []
        for row in dbi:
            try:
                # Remove invalid field values.
                row = list(map(lambda a: None if a in ('#NAME?', 'NULL') else a, row))
                row = dict(zip(head, row))
                hart_id = row[hart_species_code]
                if not hart_id:
                    # Invalid Excel value.
                    continue

                try:
                    # Zero-pad integer records.
                    hart_id = str(int(hart_id)).zfill(3)
                    row[hart_species_code] = hart_id
                except: pass
                if hart_id and not hart_id in hart_ids:
                    hart_ids.add(hart_id)
                    if not row[hart_rank]:
                        continue
                    row['rank'] = row[hart_rank]
                    rows.append(row)
            except Exception as e:
                print(e)

        insert_rows(cur, 'hart', rows, hart_field_map)
    conn.commit()


def process_inat(infile, conn):
    """
    Process records from an iNaturalist CSV file.
    """

    cur = conn.cursor()
    #cur.execute("delete from taxonomy.taxon where source='inaturalist'")

    inat_ids = set()
    
    with open(infile, 'r') as fi:
        dbi = csv.reader(fi)
        head = list(map(str.lower, next(dbi)))
    
        rows = []
        for row in dbi:
            try:
                row = dict(zip(head, row))
                inat_id = row['taxon_id']
                if inat_id and not inat_id in inat_ids:
                    inat_ids.add(inat_id)
                    row['rank'] = get_rank(row, inat_rank_fields, inat_field_map)
                rows.append(row)
            except Exception as e:
                print(e)
        insert_rows(cur, 'inaturalist', rows, inat_field_map)
    conn.commit()


def process_obis(infile, conn):
    """
    Process records from an OBIS CSV or Zip file.
    """
    csv.field_size_limit(100000)

    cur = conn.cursor()
    #cur.execute("delete from taxonomy.taxon where source='aphia'")

    aphia_ids = set()
    
    fz = None
    fi = None
    if infile.endswith('.zip'):
        fz = zipfile.ZipFile(infile)
        infile_ = os.path.splitext(os.path.basename(infile))[0]
        fi = io.TextIOWrapper(fz.open(infile_), encoding="utf-8")
    else:
        fi = open(infile, 'r')
        
    dbi = csv.reader(fi)
    head = list(map(str.lower, next(dbi)))
    
    while True:
        try:
            row = next(dbi)
            if not row: 
                break
            row = dict(zip(head, row))
            aphia_id = row['aphiaid']
            sn = row['scientificname']
            if not (aphia_id in aphia_ids) and sn:
                aphia_ids.add(aphia_id)
                row['rank'] = get_rank(row, obis_rank_fields, obis_field_map)
                insert_row(cur, aphia_id, 'aphia', row, obis_fields, obis_field_map)
        except StopIteration:
            break
        except Exception as e:
            print(e)

    conn.commit()

    fi.close()
    if fz:
        fz.close()

def connect(configfile):
    """
    Connect to the database using parameters in the ENV file and return the connection.
    """
    config = {}
    with open(configfile, 'r') as f:
        while True:
            line = f.readline()
            if not line:
                break
            if len(line.strip()) == 0 or line[0] == '#':
                continue
            k, v = line.split('=')
            config[k.strip()] = v.strip()
    conn = psycopg2.connect(f"dbname={config['DB_NAME']} host={config['DB_HOST']} user={config['DB_USER']} port={config['DB_PORT']} password={config['DB_PASSWORD']}")
    return conn

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(
        'taxonomy_loader.py',
        'python taxonomy_loader.py <flags>',
        'Reads unique taxa out of the input file to create a list of unique taxonomic records.'
    )
    parser.add_argument('-v', '--vernacular', action='store_true', help="Find and update the vernacular names for all aphia records where the common name isn't set.")
    parser.add_argument('-t', '--type', help='The type of the input file. One of "worms", "obis", "hart", or "inaturalist".')
    parser.add_argument('-i', '--input', help='The input file.')
    parser.add_argument('-c', '--config', help='The .env file containing the database configuration.')
    args = parser.parse_args()

    conn = connect(args.config)

    if args.vernacular:
        process_vernaculars(conn)
    elif args.type == 'worms':
        process_worms(args.input, conn)
    elif args.type == 'obis':
        process_obis(args.input, conn)
    elif args.type == 'inaturalist':
        process_inat(args.input, conn)
    elif args.type == 'hart':
        process_hart(args.input, conn)
    else:
        print('Unknown type:', args.type)
        sys.exit(1)
