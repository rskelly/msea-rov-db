#!/usr/bin/env python

"""
Process a Biigle CSV report (video or photo) and prepare an annotation stream file for import.

Three of files can be generated:
1) Observations -- organisms and anthropogenic objects.
2) Habitats -- substrates, biocovers and associated characteristics.
3) Status -- on/off transect, on/off bottom etc.

The columns required for the output of each type are:
    cols = ['start_time', 'end_time', 'medium_filename', 'attributes', 'original_labels', 
        'original_id', 'shape', 'shape_area', 'frames', 'tags', 'observation_confidence',
        'abundance', 'count']
Biigle doesn't provide values for all of these so they remain empty.
"""
import csv
import argparse

def run(report_filename):

    annotations = {} # Annotations indexed by ID; may have multiple labels.

    with open(report_filename, 'r') as f:
        db = csv.reader(f)
        head = next(db)
        for row in db:
            row = dict(zip(head, row))
            if annotations.get(row['annotation_id']) == None:
                annotations[row['annotation_id']] = []
            annotations[row['annotation_id']].append(row)

    for annotation_id, labels in annotations.items():
        print(annotation_id, len(labels))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        'biigle_extract.py', 
        'python biigle_extract.py <report filename>', 
        'Converts a Biigle report file to a stream of annotations that can be imported.'
    )
    parser.add_argument('report_filename', help='The path to the Biigle report file.')
    args = parser.parse_args()

    run(args.report_filename)