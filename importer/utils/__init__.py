"""
Contains useful utilities for use elsewhere in the importer program.
"""

import os
import re
import json
import shapely
from datetime import datetime, timezone, timedelta

def to_epoch(dt):
    """
    Get the epoch of a datetime in seconds.

    :param dt: The datetime to convert.
    :return: The datetime as an epoch in seconds.
    :rtype: int
    """
    if not dt:
        raise Exception('No datetime passed.')
    return int((dt.replace(tzinfo=timezone.utc) - datetime.utcfromtimestamp(0).replace(tzinfo=timezone.utc)).total_seconds())

def from_epoch(e):
    """
    Produce a date time in UTC from an epoch in seconds.

    :param e: The epoch in seconds.
    :return: The epoch as a datetime.
    :rtype: datetime
    """
    return datetime.utcfromtimestamp(e).replace(tzinfo=timezone.utc)

class DryRun(Exception):
    """
    An exception that, when thrown, indicates the end of a dry run.
    """
    def __init__(self, *args):
        super().__init__(*args)

class _status:
    """
    A callable which will either update the status callback or print the status.
    """
    def __init__(self, log=None):
        self.steps = 0
        self.step = 1
        self.running = True
        self.log = log

    def __call__(self, message, step=-1, steps=-1):
        if step == -1:
            step = self.step
        if steps == -1:
            steps = self.steps
        if self.log:
            self.log.trace(self, f'Status: {message} ({step}/{steps})')
        else:
            print(message, step, steps)
        self.step += 1

class logger:
    """
    Provides logging methods; can be passed through the call graph.
    """

    def __init__(self, log_file, do_print=True):
        """
        Initialize the logger to write output to log_file.
        Opens a file handle that will be closed on destruction.

        :param log_file: The path to the log file.
        :param do_print: If True, will print logging messages to screen, as well as file.
        """
        self.fd = open(log_file, 'w', encoding='utf8')
        self.do_print = do_print

    def write(self, head, inst, msg):
        """
        Write the log message to file and screen if necessary.

        :param head: The head text; something like 'WARN', 'ERROR', etc.
        :param inst: The the instance that is generating the log message.
        :param msg: The message.
        """
        dt = datetime.strftime(datetime.now(tz=timezone.utc), '%Y-%m-%dT%H:%M:%SZ')
        self.fd.write(f"{head} {dt} {msg}\n")
        if self.do_print:
            print(f"{head} {dt} {msg}")

    def warn(self, inst, msg):
        """
        Write a warning message.

        :param inst: The instance that is generating the message.
        :param msg: The message.
        """
        self.write('WARN ', inst, msg)

    def trace(self, inst, msg):
        """
        Write a trace message.

        :param inst: The instance that is generating the message.
        :param msg: The message.
        """
        self.write('TRACE', inst, msg)

    def debug(self, inst, msg):
        """
        Write a debug message.

        :param inst: The instance that is generating the message.
        :param msg: The message.
        """
        self.write('DEBUG', inst, msg)

    def error(self, inst, msg):
        """
        Write a error message.

        :param inst: The instance that is generating the message.
        :param msg: The message.
        """
        self.write('ERROR', inst, msg)

    def __del__(self):
        try:
            self.fd.close()
        except: pass


def parse_time(t, format=None, tzoffset=0):
    """
    Parse the time using the given format or a list of possible formats. 
    If the correct format isn't found and the given format doesn't work, an exception is thrown.
    If a format is given, the internal list is ignored.
    Only parses to UTC.

    :param t: A Time string.
    :param format: If given, the time format is used to parse the time.
    :return: A datetime.
    """
    if not t:
        return None
    # Use the format or an alternative.
    formats = ['%Y-%m-%dT%H:%M:%SZ', '%Y-%m-%d %H:%M:%S', '%Y-%m-%d %H:%M:%S.%f', '%Y-%m-%dT-%H:%M:%S']
    if format:
        formats = [format] + formats
    for f in formats:
        try:
            return (datetime.strptime(t, f) + timedelta(hours=tzoffset)).replace(tzinfo=timezone.utc)
        except: pass
    raise Exception(f'Failed to parse datetime, {t}.')

def ddl_type(colname):
    """
    Return the column type for the column name, from a list of known column names related to Biigle. 
    Default is text.

    :param colname: The name of the column.
    :return: A string representing the type of the column, one of 'integer' or 'text'.
    :rtype: str
    """
    if colname in ('video_annotation_label_id', 'label', 'annotation_id', 'label_id'):
        return 'integer'
    else:
        return 'text'

def parse_file_datetime(filename):
    """
    Parse the file name and return the datetime. The file name is
    expected to consist of underscore-delimited parts, with the last two being
    the date as YYYYMMDD and the time as HHMMSS.

    :param filename: A file name containing a date.
    :return: A datetime.
    :rtype: datetime
    """
    parts = os.path.splitext(filename)[0].split('_')
    d, t = parts[-2:]
    return datetime.strptime(d + t, '%Y%m%d%H%M%S')

def load_config(config_file):
    """
    Load the configuration dictionary from the given file and return the dictionary.

    :param config_file: The configuration file.
    :return: A tuple containing the config dictionary and the root directory of the import dataset.
    :rtype: tuple(dict, str)
    """
    config_dir = os.path.dirname(config_file)
    with open(config_file, 'r', encoding='utf-8-sig') as f:
        config = json.load(f)
    return config, config_dir

def point_to_wkb(p):
    """
    Convert the tuple/list of coordinates to a wkb string. Coorinates must
    be in the order x, y, z.

    :param p: A tuple or list containing at least 2 coordinates.
    :return: A well-known binary string representing and OGR point geometry.
    :rtype: str
    """
    p_ = shapely.geometry.Point(*[float(c) for c in p])
    return shapely.wkb.dumps(p_, hex=True, srid=4326)

def format_values(v):
    """
    Format a list of values into a list of floats.
    If any of the values is an empty string, None, 'NA' or
    some other non-float value, the function returns None.

    :param v: A list of values that can be cast or converted to floats.
    :return: A list of floats if the conversion succeeds or None.
    :rtype: any
    """
    try:
        return list(map(float, v))
    except:
        return None

def has_tag(label, tag_names):
    """
    Return True if the label contains a tag with the given name.
    The propname can be null if the label is a user-added tag.

    :param label: The label text.
    :param tag_names: A string, tuple or list containing tag names.
    :return: True if the label is in the list of tags names; False otherwise.
    :rtype: bool
    """
    if not label:
        return False
    tags = [x.lower() for x in label['tags']]
    if type(tag_names) == str:
        return tag_names.lower() in tags
    else:
        for tag_name in tag_names:
            if tag_name.lower() in tags:
                return True
    return False

def free_name(filename):
    """
    Find a filename that does not conflict with an existing filename by
    adding an incrementing integer to the end of the filename before the
    extension. For example, file.jpg becomes file_1.jpg and file_1.jpg 
    becomes file_2.jpg.
    :param filename: The full path to a file.
    :return: A new filename.
    """
    pat = re.compile(r'^(.+)_([0-9]+)$')
    dirname = os.path.dirname(filename)
    while os.path.exists(filename):
        name, ext = os.path.splitext(os.path.basename(filename))
        result = pat.match(name)
        if result:
            name = result.group(1)
            num = result.group(2)
            num = int(num) + 1
        else:
            num = 1
        filename = os.path.join(dirname, f'{name}_{num}' + ext)
    return filename