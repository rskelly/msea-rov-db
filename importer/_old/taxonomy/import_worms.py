#!/usr/bin/env python

"""
Build a useable database from the OBIS occurrence table.

Figure out the column lengths in the database and convert each one to
a varchar if < 256 or text otherwise. For selected columns, convert to
the specified type with the specified function(s).

Build new tables:
scientific -- Unique scientificname and aphia ID for all records.
vernacular -- Unique vernacularname and aphia ID.
originalscientitific -- Unique originalscientificname and aphia ID.

Trigram indices will be built on each name column to allow substring searching.
"""

import os
import sys
import csv
import logging

dbname = 'sap'
user = 'rob'
occurrence = 'C:\\Users\\skellyr\\Documents\\datasets\\taxonomy\\obis_20211021.csv'

# Make a file with just the taxon records.
# https://dwc.tdwg.org/terms/#taxon
taxa_cols = (
	'aphiaid',
	'taxonid', 'scientificname', 'namepublishedin', 'namepublishedinyear', 'kingdom',
	'phylum', 'class', 'order', 'family', 'subfamily',  'genus', 
	#'genericname', 
	'subgenus',
	#'infragenericepithet', 
	'specificepithet', 'infraspecificepithet', 
	#'cultivarepithet', 
	'taxonrank',
	'verbatimtaxonrank', 'scientificnameauthorship', 'vernacularname', 'nomenclaturalcode', 
	'taxonomicstatus', 'nomenclaturalstatus', 
	#'taxonremarks',
	'vernacularname',
)

def parse_line(l):
	'''
	Parse a line (a string) as a comma-delimited list, handling quotes.

	Makes an effort to clean up errors in the OBIS table that are not handled by
	the ``csv`` module. In particular, erroneously-escaped quotes, and double-double
	quotes used for coordinates.

	:param l: A string containing a comma-delimited line.
	:return: A list containing the individual field values.
	'''
	# Replace rogue characters.
	l = l.replace('\\"', '"')
	l = l.replace('""', '\\"')

	# The quote count -- even if quotes are balanced; odd otherwise.
	q = 0
	# True if an escape char has been encountered.
	e = False
	# The output row.
	o = []
	# The character buffer for the current field.
	b = []

	# Iterate over the line, char by char.
	for c in l:
		if c == ',' and q % 2 == 0:
			# If a comma is encountered and quotes are balanced, add the field to the buffer.
			o.append(''.join(b).strip())
			b = []
		elif c == '\\':
			# If an escape is found, set true and continue to jump to the next char.
			e = True
			continue
		elif c == '"' and not e:
			# If an unescaped quote is encountered, increment the count.
			q += 1
		elif c not in ('\n', '\r'):
			# Append all other legit chars to the buffer.
			b.append(c)

		# Unset the escape flag.
		e = False

	return o

			
def escape(v):
	'''
	If a string contains a comma, return it with quotes. Escape any quotes already 
	contained in the string.
	:param v: The input string.
	:return: The output string.
	'''
	v = v.replace('"', '\\"')
	if v.find(',') > -1:
		return '"{}"'.format(v)
	return v


def filter_taxa(infile, outfile):
	'''
	Read through the original OBIS export to extract unique taxon records.

	A taxon record should conform to the [Darwin Core specification](https://dwc.tdwg.org/terms/#dwc:taxonRemarks).

	:param infile: The input file, a path to the original OBIS CSV file.
	:param outfile: The output file, a path to a CSV file.
	'''

	# A set to track aphia IDs. We don't need to have repeating taxa in the output.
	# TODO: A more sophisticated filter to keep vernacular names if necessary.
	ids = set()

	import io
	import zipfile
	
	from msea.util import util

	# Temporary file for unique records.
	tmpfile = util.maketempfile()
	# Temporary file for accumulating vernaculars.
	vfile = util.maketempfile()

	ft = open(tmpfile, 'w', encoding='utf8')
	fv = open(vfile, 'w', encoding='utf8')

	# Write the header.
	ft.write('{}\n'.format(','.join(map(escape, taxa_cols))))

	with zipfile.ZipFile(infile) as zf:
		with io.TextIOWrapper(zf.open(os.path.splitext(os.path.basename(infile))[0]), encoding="utf-8") as f:

			# Extract and clean up the header line.
			head = list(map(str.lower, parse_line(f.readline())))

			# Start iterating over the input lines.
			line = f.readline()
			c = 0
			while line:
				row = None
				try:
					
					# If the row is shorter than the header, expand it using blank fields.
					# TODO: Is this necessary?
					if len(line) < len(head):
						line = line + [''] * (len(head) - len(line))

					# Turn the head/line into a dictionary.
					row = dict(zip(head, parse_line(line)))

					# Extract the taxon ID and continue if it has not already been visited.
					id = int(row['aphiaid'].strip())
					if id and not id in ids:
						ids.add(id)
						# Clean up and write the row; add the taxon ID to the set.
						ft.write('{}\n'.format(','.join([escape(row[c]) for c in taxa_cols])))
						
						vn = row['vernacularname'].strip()
						if vn:
							fv.write(f'{id},"{row["vernacularname"]}"')

				except Exception as e:
					logging.warning(str(e))
					break

				# Read the next line.
				line = f.readline()

				# Output status info.
				c += 1
				if c % 1000 == 0:
					print('Row ', c)

	os.rename(tmpfile, outfile)
	os.rename(vfile, outfile + '.vn.csv')
	
# Field types for preserved fields.
types = {
	'aphiaid': ('int4', 0, int),
	'id': ('uuid', 32, None),
	'dataset_id': ('uuid', 32, None),
	'scientificname': ('varchar(255)', 255, None),
	'vernacularname': ('text[]', 0, None),
	'originalscientificname': ('varchar(255)', 255, None),
	'dropped': ('bool', 0, bool),
	'absence': ('bool', 0, bool),
	'marine': ('bool', 0, bool),
	'brackish': ('bool', 0, bool),
	'freshwater': ('bool', 0, bool),
	'terrestrial': ('bool', 0, bool),
}


def generate_ddl(infile):
	'''
	Generates DDL (SQL) for the data contained in the given input CSV file.

	This is intended for building a table to contain the output generated by
	``filter_taxa``.

	:param infile: The path to the input CSV file.
	'''

	fields = []
	for k, v in types:
		fields.append('"{}" {}'.format(k, v[0]))

	yield 'drop table if exists "obis"."taxon";'
	yield 'create table "obis"."taxon" ({});'.format(', '.join(fields))
	yield 'insert into "obis"."taxon" ({}) values '.format(', '.join(types.keys()))

	with open(infile, 'r', encoding = 'utf8') as f:
		db = csv.reader(f)
		head = next(db)
		for row in db:
			row = dict(zip(head, row))
			inert = []
			for k, v in types:
				dtype, length, func = v
				if length > 0 and len(row[k]):
					raise Exception('Field "{}" value is too long: {}'.format(k, row[k]))
				if func:
					row[k] = func(row[k])
				insert.append('"{}"::{}'.format(k, dtype))
			yield '({})'.format(','.join(insert))

	yield ');'

"""
ddl = [
	('scientific', 'scientificname', '*', None),
	('vernacular', 'vernacularname', 'aphiaid, vernacularname', None),
	('originalscientific', 'originalscientificname', 'aphiaid, originalscientificname', None),
]

old_fields = {}
new_fields = {}

def next2(v):
	if not v:
		v = 0
	t = 2
	while v >= t:
		t *= 2
	return t

if False:
	with psycopg2.connect('dbname={} user={}'.format(dbname, user)) as conn:
		try:
			with conn.cursor() as cur:
				with open(occurrence, 'r', encoding = 'utf-8') as f:
					head = f.readline().strip().split(',')
					fields = ','.join(['"{}" text'.format(x) for x in head])
					cur.execute('drop table obis.occurrence')
					cur.execute('create table if not exists obis.occurrence ({})'.format(fields))
					j = 0
					while True:
						with open(tmp, 'w', encoding = 'utf-8') as o:
							o.truncate()
							o.write(fields + '\n')
							for i in range(100000):
								line = f.readline()
								if not line:
									break
								o.write(line)
						cur.execute('copy obis.occurrence from \'{}\' with csv header'.format(tmp)) # Can't copy the whole file because of a bug in psql.
						if not line:
							break
						j += 100000
						print(j)
		except Exception as e:
			logging.error(e)
			sys.exit(1)

if False:
	with psycopg2.connect('dbname={} user={}'.format(dbname, user)) as conn:
		with conn.cursor() as cur:

			if not os.path.exists('field_length.json'):

				# Get the max length of each field.
				cur.execute('select * from obis.occurrence limit 0')
				cols = [desc[0] for desc in cur.description]
				for col in cols:
					print('Getting max length of', col)
					cur.execute('''
						with t as (
							select "{}" as v from obis.occurrence where "{}" is not null and random() < 0.01 limit 1000
						)
						select max(length(v)) as len from t
					'''.format(col, col))
					clen = cur.fetchone()[0]
					old_fields[col] = next2(clen)

				with open('field_length.json', 'w') as f:
					f.write(json.dumps(old_fields))

			else:

				with open('field_length.json', 'r') as f:
					old_fields = json.loads(f.read())

			for col, clen in old_fields.items():
				print('Configuring', col)
				if types.get(col):
					new_fields[col] = types[col]
				elif clen > 255:
					new_fields[col] = ('text', None)
				else:
					new_fields[col] = ('varchar({})'.format(clen), None)

			print('Preparing transfer')
			ddl_cols = []
			transfer_cols = []
			for col, config in new_fields.items():
				type, func = config
				ddl_cols.append('"{}" {}'.format(col, new_fields[col][0]))
				if func:
					transfer_cols.append(func)
				else:
					transfer_cols.append('"{}"::{}'.format(col, type))

			print('Create the new tables')
			cur.execute('drop table if exists obis.occurrence_formatted')
			cur.execute('create table obis.occurrence_formatted ({})'.format(', '.join(ddl_cols)))

			print('Transfer values to new table')
			cur.execute('insert into obis.occurrence_formatted ("{}") select {} from obis.occurrence'.format('", "'.join(new_fields.keys()), ', '.join(transfer_cols)))

if False:
	with psycopg2.connect('dbname={} user={}'.format(dbname, user)) as conn:
		with conn.cursor() as cur:
			print("Create indexes on source table")
			cur.execute('create index if not exists occurrence_aphiaid_idx on obis.occurrence_formatted(aphiaid asc)')
			cur.execute('create index if not exists occurrence_scientificname_idx on obis.occurrence_formatted(lower(scientificname) asc)')
			cur.execute('create index if not exists occurrence_originalscientificname_idx on obis.occurrence_formatted(lower(originalscientificname) asc)')
			cur.execute('create index if not exists occurrence_vernacularname_idx on obis.occurrence_formatted(lower(vernacularname) asc)')
			cur.execute('create index if not exists occurrence_taxonomicstatus_idx on obis.occurrence_formatted(lower(taxonomicstatus) asc)')

if False:
	with psycopg2.connect('dbname={} user={}'.format(dbname, user)) as conn:
		conn.autocommit = True
		print('Vacuum')
		with conn.cursor() as cur:
			cur.execute('vacuum analyze obis.occurrence_formatted')

if True:
	with psycopg2.connect('dbname={} user={}'.format(dbname, user)) as conn:
		with conn.cursor() as cur:
			print('Create the new tables')
			for table, field, cols, where in ddl:
				print(table)
				if True:
					cur.execute('drop table if exists obis.{}'.format(table))
					cur.execute('create table obis.{} as select distinct on(aphiaid, lower({})) {} from obis.occurrence_formatted where taxonrank is not null and {} is not null {}'.format(table, field, cols, field, 'and {}'.format(where) if where else ''))
				else:
					cur.execute('drop index {}_aphiaid_idx on obis.{}(aphiaid asc)'.format(table, table))
					cur.execute('index {}_{}_trgm_idx on obis.{} using gist(lower({}) gist_trgm_ops)'.format(table, field, table, field))
				cur.execute('create index {}_aphiaid_idx on obis.{}(aphiaid asc)'.format(table, table))
				cur.execute('create index {}_{}_trgm_idx on obis.{} using gist(lower({}) gist_trgm_ops)'.format(table, field, table, field))

if True:
	with psycopg2.connect('dbname={} user={}'.format(dbname, user)) as conn:
		conn.autocommit = True
		print('Vacuum')
		with conn.cursor() as cur:
			for table, field, cols, where in ddl:
				print(table)
				cur.execute('vacuum analyze obis.{}'.format(table))
"""

import re

def format_row(row, head, fidx, fields, names):
	sciname = row[fidx['scientificname']].strip()
	aphiaid = row[fidx['aphiaid']].strip()
	
	if not aphiaid or not sciname:
		return None
	
	if sciname.lower() in names:
		#print('skip', sciname)
		return None
	names.add(sciname.lower())

	l = min(len(head), len(row))
	for i in range(l):
		if head[i] != 'aphiaid':
			if not row[i].strip():
				row[i] = 'NULL'
			else:
				row[i] = "'{}'".format(row[i].replace('\'', '\'\''))

	return [row[fidx[field]] for field in fields]


def generate_inserts(csvfile, ddlfile):

	fields = (
		'id',
		'scientificname',
		'originalscientificname',
		'taxonrank',
		'aphiaid',
		'superdomain',
		'domain',
		'kingdom',
		'subkingdom',
		'infrakingdom',
		'phylum',
		'phylum_division',
		'subphylum_subdivision',
		'subphylum',
		'infraphylum',
		'parvphylum',
		'gigaclass',
		'megaclass',
		'superclass',
		'class',
		'subclass',
		'infraclass',
		'subterclass',
		'superorder',
		'order',
		'suborder',
		'infraorder',
		'parvorder',
		'superfamily',
		'family',
		'subfamily',
		'supertribe',
		'tribe',
		'subtribe',
		'genus',
		'subgenus',
		'section',
		'subsection',
		'series',
		'species',
		'subspecies',
		'natio',
		'variety',
		'subvariety',
		'forma',
		'subforma',
		'type',
		'organismname',
		#'identificationverificationstatus',
		#'typestatus',
		'taxonid',
		#'nameaccordingto',
		#'namepublishedin',
		#'namepublishedinyear',
		#'higherclassification',
		#'specificepithet',
		#'infraspecificepithet',
		#'verbatimtaxonrank',
		#'scientificnameauthorship',
		'vernacularname',
		#'nomenclaturalcode',
		'taxonomicstatus',
		#'nomenclaturalstatus',
		'taxonremarks'
	)

	sql = 'insert into obis.taxon ({fields}) values ({{values}}) on conflict (aphiaid, scientificname) do nothing;\n'.format(fields = '"{}"'.format('","'.join(fields)))
	names = set()

	with open(ddlfile, 'w', encoding = 'utf-8') as o:
		with open(csvfile, 'r', encoding = 'utf-8') as f:
			db = csv.reader(f) #, quoting = csv.QUOTE_NONE)
			head = next(db)
		
			fidx = {}
			for field in fields:
				idx = head.index(field)
				if idx < 0:
					raise Exception('Field not found in header: ', field)
				fidx[field] = idx

			row_ = None
			i = 0
			buf = []
			bad = 0
			while True:
				try:
					row = next(db)
					bad = 0
				except Exception as e:
					logging.warning(str(e))
					bad += 1
					if bad > 3:
						break
					continue

				row = format_row(row, head, fidx, fields, names)
				if not row:
					continue
				buf.append(sql.format(values = ",".join(row)))
				if i % 1000 == 0:
					o.write('\n'.join(buf))
					buf.clear()
					print(i)
				i += 1
				row_ = row
			o.write('\n'.join(buf))
			buf.clear()


if __name__ == '__main__':

	action = sys.argv[1]

	if action == 'filter':
		infile = sys.argv[2]
		outfile = sys.argv[3]
		filter_taxa(infile, outfile)
	elif action == 'insert':
		infile = sys.argv[2]
		outfile = sys.argv[3]
		generate_inserts(infile, outfile)

	# try:
	# 	app = sys.argv[1]

	# 	if app == 'filter':
	# 		filter_taxa(sys.argv[2], sys.argv[3])
	# 	elif app == 'ddl':
	# 		generate_ddl(sys.argv[2])
	# 	else:
	# 		raise Exception('Unknown command: {}'.format(app))
	# except Exception as e:
	# 	traceback.print_exc()
	# 	print('Usage: import_taxonomy_obis.py <command> [arg [arg [...]]]')

#os.path.join(os.path.dirname(occurrence), 'obis_taxa.csv')



'''
		id,
		dataset_id,
		decimallongitude,
		decimallatitude,
		date_start,
		date_mid,
		date_end,
		date_year,
		scientificname,
		originalscientificname,
		minimumdepthinmeters,
		maximumdepthinmeters,
		coordinateuncertaintyinmeters,
		flags,
		dropped,
		absence,
		shoredistance,
		bathymetry,
		sst,
		sss,
		marine,
		brackish,
		freshwater,
		terrestrial,
		taxonrank,
		aphiaid,
		redlist_category,
		superdomain,
		domain,
		kingdom,
		subkingdom,
		infrakingdom,
		phylum,
		phylum_division,
		subphylum_subdivision,
		subphylum,
		infraphylum,
		parvphylum,
		gigaclass,
		megaclass,
		superclass,
		class,
		subclass,
		infraclass,
		subterclass,
		superorder,
		order,
		suborder,
		infraorder,
		parvorder,
		superfamily,
		family,
		subfamily,
		supertribe,
		tribe,
		subtribe,
		genus,
		subgenus,
		section,
		subsection,
		series,
		species,
		subspecies,
		natio,
		variety,
		subvariety,
		forma,
		subforma,
		type,
		modified,
		language,
		license,
		rightsholder,
		accessrights,
		bibliographiccitation,
		references,
		institutionid,
		collectionid,
		datasetid,
		institutioncode,
		collectioncode,
		datasetname,
		ownerinstitutioncode,
		basisofrecord,
		informationwithheld,
		datageneralizations,
		dynamicproperties,
		materialsampleid,
		occurrenceid,
		catalognumber,
		occurrenceremarks,
		recordnumber,
		recordedby,
		recordedbyid,
		individualcount,
		organismquantity,
		organismquantitytype,
		sex,
		lifestage,
		reproductivecondition,
		behavior,
		establishmentmeans,
		occurrencestatus,
		preparations,
		disposition,
		othercatalognumbers,
		associatedmedia,
		associatedreferences,
		associatedsequences,
		associatedtaxa,
		organismid,
		organismname,
		organismscope,
		associatedoccurrences,
		associatedorganisms,
		previousidentifications,
		organismremarks,
		eventid,
		parenteventid,
		samplingprotocol,
		samplesizevalue,
		samplesizeunit,
		samplingeffort,
		eventdate,
		eventtime,
		startdayofyear,
		enddayofyear,
		year,
		month,
		day,
		verbatimeventdate,
		habitat,
		fieldnumber,
		fieldnotes,
		eventremarks,
		locationid,
		highergeographyid,
		highergeography,
		continent,
		waterbody,
		islandgroup,
		island,
		country,
		countrycode,
		stateprovince,
		county,
		municipality,
		locality,
		verbatimlocality,
		verbatimelevation,
		minimumelevationinmeters,
		maximumelevationinmeters,
		verbatimdepth,
		minimumdistanceabovesurfaceinmeters,
		maximumdistanceabovesurfaceinmeters,
		locationaccordingto,
		locationremarks,
		verbatimcoordinates,
		verbatimlatitude,
		verbatimlongitude,
		verbatimcoordinatesystem,
		verbatimsrs,
		geodeticdatum,
		coordinateprecision,
		pointradiusspatialfit,
		footprintwkt,
		footprintsrs,
		footprintspatialfit,
		georeferencedby,
		georeferenceddate,
		georeferenceprotocol,
		georeferencesources,
		georeferenceverificationstatus,
		georeferenceremarks,
		geologicalcontextid,
		earliesteonorlowesteonothem,
		latesteonorhighesteonothem,
		earliesteraorlowesterathem,
		latesteraorhighesterathem,
		earliestperiodorlowestsystem,
		latestperiodorhighestsystem,
		earliestepochorlowestseries,
		latestepochorhighestseries,
		earliestageorloweststage,
		latestageorhigheststage,
		lowestbiostratigraphiczone,
		highestbiostratigraphiczone,
		lithostratigraphicterms,
		group,
		formation,
		member,
		bed,
		identificationid,
		identifiedby,
		identifiedbyid,
		dateidentified,
		identificationreferences,
		identificationremarks,
		identificationqualifier,
		identificationverificationstatus,
		typestatus,
		taxonid,
		scientificnameid,
		acceptednameusageid,
		parentnameusageid,
		originalnameusageid,
		nameaccordingtoid,
		namepublishedinid,
		taxonconceptid,
		acceptednameusage,
		parentnameusage,
		originalnameusage,
		nameaccordingto,
		namepublishedin,
		namepublishedinyear,
		higherclassification,
		specificepithet,
		infraspecificepithet,
		verbatimtaxonrank,
		scientificnameauthorship,
		vernacularname,
		nomenclaturalcode,
		taxonomicstatus,
		nomenclaturalstatus,
		taxonremarks
'''
