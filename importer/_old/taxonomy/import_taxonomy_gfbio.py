#!/usr/bin/env python

'''
This script loads the taxonomy table from a VideoMiner-generated Access database into
the hart_taxonomy table in the ROV database.
'''

import os
import sys
import pyodbc
import traceback
import argparse
import csv 

from datetime import datetime, timedelta, timezone
import django
from django.db import transaction, connection

# Import the utils to support this program.
from util import *
import util

# Setup the Django environment.
django_setup()

# Import the MSEA models. Has to be done after setting up Django.
from msea.models import *
from msea.forms import *

rank_map = {
	'Family': 1,
	'Genus': 2,
	'Order': 3,
	'Kingdom': 4,
	'Species': 5,
	'Phylum': 6,
	'Class': 7,
	'Sub Phylum': 8,
	'Sub Class': 9,
	'Sub Order': 10,
	'Sub Family': 11,
	'Super Family': 12,
	'Sub Species': 13,
	'Not a taxon': 14,
	'Not a Taxonomic classification, old historic codes': 14,
	'Super Phylum': 15,
	'Super Class': 16,
	'Infra Order': 17,
	'Super Order': 18,
	'Hybrid': 19,
	'Infra Class': 20,
	'Sub Section': 21
}

group_map = {
	'Fish': 1,
	'Invertebrate': 2,
	'Plant': 3,
	'Mammal': 4,
	'Bird': 5,
	'Reptile': 6,
	'Other': 7
}


def init_maps():
	for k, v in rank_map.items():
		rank_map[k] = TaxonomicRank.objects.get(pk = v)
	for k, v in group_map.items():
		group_map[k] = TaxonomicGrouping.objects.get(pk = v)


def load_taxa(filename):
	'''
	Load the named taxonomy table from the Access database.
	'''
	
	init_maps()

	with transaction.atomic():

		with open(filename, 'r', encoding = 'utf-8-sig') as f:
			db = csv.reader(f)
			fields = list(map(str.lower, next(db)))
			for row in db:
				row = dict(zip(fields, map(lambda a: None if a == 'NULL' else a, row)))
				#print(row)
				species_code = row['species_code']
				while len(species_code) < 3:
					species_code = '0' + species_code

				t = None
				try:
					print('a')
					t = Taxon.objects.get(hart_code = species_code)
				except: pass

				#if not t:
				#	try:
				#		print('b')
				#		t = Taxon.objects.get(scientific_name__iexact = row['species_science_name'])
				#	except: pass

				#if species_code == '66Z':
				#	print('found scallop')
				#	print(t)
				#	sys.exit(1)

				if not t:
					parent_name = row['parent_taxonomic_unit']
					pt = None
					if parent_name:
						parent_name = parent_name.split('(')[0].strip()
						try:
							pt = Taxon.objects.get(scientific_name__iexact = parent_name)
						except: pass
						if not pt:
							try:
								pt = Taxon.objects.get(scientific_name__icontains = parent_name) 
							except: pass

					tt = Taxon.objects.create(**{
						'hart_code': species_code,
						'parent': pt,
						'scientific_name': row['species_science_name'],
						'common_name': row['species_common_name'],
	#					'sp_species_code': row['spspeciescode'],
	#					'nodc_code_v7': None, #util.check_float(row['nodccodev7']),
						'lsid': row['lsid'],
						'rsty_id': row['rsty_id'],
						'parent_rsty_id': row['parent_rsty_id'],
						'itis_id': row['itis_tsn'],
						'rank': rank_map.get(row['taxonomic_rank']),
						'grouping': group_map.get(row['species_grouping']),
						#'species_prov_code': row['speciesprovcode'],
						#'from_date': None, #datetime.strptime('%d/%m/%Y %H:%M:%S', row['fromdate']) if row['fromdate'] else None,
						#'to_date': None, #datetime.strptime('%d/%m/%Y %H:%M:%S', row['todate']) if row['todate'] else None,
						#'authority': row['authority'],
						#'taxa_reference': row['taxareference']
					})
					tt.save()

					ht = HartTaxon.objects.create(**{
						'taxon': tt,
						'orig_common_name': row['species_desc'],
					})
					ht.save()

				# else:
				# 	ht = HartTaxon.objects.create(**{
				# 		'taxon': t,
				# 		'hart_code': row['speciescode'],
				# 		'latin_name': row['latinname'],
				# 		'orig_common_name': row['commonname'],
				# 		'sp_species_code': row['spspeciescode'],
				# 		'nodc_code_v7': None, #util.check_float(row['nodccodev7']),
				# 		'itis_id': row['nodccodev8'],
				# 		'rank': rank_map.get(row['taxonomyclasslevelcode']),
				# 		'grouping': group_map.get(row['grouping']),
				# 		'species_prov_code': row['speciesprovcode'],
				# 		'from_date': None, #datetime.strptime('%d/%m/%Y %H:%M:%S', row['fromdate']) if row['fromdate'] else None,
				# 		'to_date': None, #datetime.strptime('%d/%m/%Y %H:%M:%S', row['todate']) if row['todate'] else None,
				# 		'authority': row['authority'],
				# 		'taxa_reference': row['taxareference']
				# 	})

if __name__ == '__main__':
	
	filename = sys.argv[1]

	load_taxa(filename)