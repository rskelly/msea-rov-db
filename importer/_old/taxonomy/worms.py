#!/usr/bin/env python

"""
This script reads through the records in the taxon table in the ROV database
and attempts to match each entry to a record in the WoRMS database, returning an AphiaID.

First, the script will attempt to match a batch of scientific names (Genus species) exactly,
then the non-matches will be re-run using the fuzzy match method. If a fuzzy match is made, the
worms_fuzzy flag is set. If no match is found, the aphia_id is null. These records must be audited.

When observations are ingested an attempt is made to find a link to the species or subspecies taxon.
If that can't be managed, an attempt is made to match the lowest taxon and the identification is
recorded as an OTU.
"""

import os
import sys
import csv
import django
import json
import logging
from urllib import request, parse

from django.db import transaction, connection
from datetime import datetime, timedelta, timezone

# Import the utils to support this program.
from util import *
import util

# Setup the Django environment.
django_setup()

# Import the MSEA models. Has to be done after setting up Django.
from msea.models import *
from msea.forms import *

base_url_fuzzy = 'http://marinespecies.org/rest/AphiaRecordsByMatchNames?'
base_url_sciname = 'http://marinespecies.org/rest/AphiaRecordsByName'
base_url_byid = 'http://marinespecies.org/rest/AphiaRecordByAphiaID'

# Mapping of WoRMS response fields to database fields.
fields = (
	('AphiaID', 'aphia_id'), 
	('scientificname', 'scientific_name'), 
	('authority', 'authority'),
	('taxonRankID', 'taxonomic_rank_id'),
	('rank', 'rank'),
	('valid_AphiaID', 'valid_aphia_id'),
	('valid_name', 'valid_name'),
	('valid_authority', 'valid_authority'),
	('parentNameUsageID', 'parent_name_usage_id'),
	('kingdom', 'kingdom'),
	('phylum', 'phylum'),
	('class', 'classs'),
	('order', 'order'),
	('family', 'family'),
	('genus', 'genus'),
	('citation', 'citation'),
	('lsid', 'lsid'),
	('isMarine', 'is_marine'),
	('isBrackish', 'is_brackish'),
	('isTerrestrial', 'is_terrestrial'),
	('isExtinct', 'is_extinct'),
	('match_type', 'match_type'),
	('modified', 'modified'),
#	('url', 'url')
)


otu_words = ['indet.', 'indet', 'sp.', 'sp', 'spp.', 'spp', 'inc.', 'inc', 'stet.', 'stet', 'sp. aff.', 'sp. aff', 'sp aff.', 'sp aff', 'aff.', 'aff', 'cf.', 'cf', 'f.', 'f']


def get_sciname(sciname):
	'''
	Attempt to retrieve the Aphia ID by matching the scientific name exactly.
	'''
	url = '{}/{}?{}'.format(base_url_sciname, parse.quote_plus(sciname), 'like=false&marine_only=false&offset=1')
	try:
		res = request.urlopen(url)
		data = None
		if res.status == 200:
			data = json.loads(res.read().decode('utf-8'))
		return res.status, res, data
	except Exception as e:
		logging.warning(str(e))
		return 0, None, None


def get_fuzzy(sciname):
	'''
	Attempt to match the name using the "fuzzy" method and return an Aphia ID.
	'''
	url = '{}&scientificnames[]={}&marine_only=false'.format(base_url_fuzzy, parse.quote_plus(sciname))
	try:
		res = request.urlopen(url)
		data = None
		if res.status == 200:
			data = json.loads(res.read().decode('utf-8'))
			for i in range(len(data)):
				if len(data[i]):
					data[i] = data[i][0]
		return res.status, res, data
	except Exception as e:
		logging.warning(str(e))
		return 0, None, None


def split_otu(name):
	'''
	If the name contains a word with a standard OTU, will split there and return
	the prior string.
	'''
	parts = name.split()
	while parts:
		for otu in otu_words:
			# Look for an element which starts with the otu text, but only if the 
			# OTU has a period. Otherwise only match the whole OTU.
			idx = -1
			for i in range(len(parts)):
				if otu.endswith('.') and parts[i].startswith(otu):
					idx = i
				elif parts[i] == otu:
					idx = i
			# If a match was found, truncate the name.
			if idx > -1:
				parts = parts[:idx]
				break
		break
	# If the OTU name isn't different, return None
	return ' '.join(parts) if parts else None


def create_worms_by_id(aphia_id):
	'''
	Attempt to get the entity by ID.
	'''
	url = '{}/{}?marine_only=false'.format(base_url_byid, aphia_id)

	try:
		try:
			return WoRMS.objects.get(aphia_id = item['aphia_id'])
		except:
			res = request.urlopen(url)
			data = None
			if res.status == 200:
				data = json.loads(res.read().decode('utf-8'))
				return create_worms(data)
	except Exception as e:
		logging.warning(str(e))
		return None


def create_worms(data):
	'''
	Load or create a WoRMS object based on the information contained in data.
	If data is a list, the first item is assumed to be the object of interest.
	Uses the fields list to remap to WoRMS model field names.
	'''

	if isinstance(data, list):
		data = data[0]

	# Create a data object by mapping the fields from the retrieved object.
	item = {}
	for frm, to in fields:
		item[to] = data[frm]

	try:
		# Try to get an existing WORMS object.
		worm = WoRMSTaxon.objects.get(aphia_id = item['aphia_id'])
	except:
		# Create the new WORMS object.
		worm = WoRMSTaxon.objects.create(**item)
		worm.save()

	return worm
