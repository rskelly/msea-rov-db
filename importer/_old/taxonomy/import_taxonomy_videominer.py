#!/usr/bin/env python

"""
This script loads the taxonomy table from a VideoMiner-generated Access database into
the hart_taxonomy table in the ROV database.
"""

import os
import sys
import pyodbc
import traceback
import argparse
import logging

from datetime import datetime, timedelta, timezone
import django
from django.db import transaction, connection

# Import the utils to support this program.
from util import *
import util

# Setup the Django environment.
django_setup()

# Import the MSEA models. Has to be done after setting up Django.
from msea.models import *
from msea.forms import *

# Lowercase entity names to avoid case confusion.
pyodbc.lowercase = True

# The ranks and groups should already be loaded from a fixture.
# rank_map = {
# 	'FAM': 1,
# 	'GEN': 2,
# 	'ORD': 3,
# 	'KNG': 4,
# 	'SPE': 5,
# 	'PHY': 6,
# 	'CLS': 7,
# 	'SBP': 8,
# 	'SBC': 9,
# 	'SBO': 10,
# 	'SBF': 11,
# 	'SPF': 12,
# 	'SSP': 13,
# 	'NOT': 14,
# 	'SPP': 15,
# 	'SPC': 16,
# 	'IFO': 17,
# 	'SPO': 18,
# 	'HYB': 19,
# 	'IFC': 20,
# 	'SBS': 21
# }

# group_map = {
# 	'F': 1,
# 	'I': 2,
# 	'P': 3,
# 	'M': 4,
# 	'B': 5,
# 	'R': 6,
# 	'O': 7
# }

# Lookups for ranks and groups. Populated later.
rank_map = {}
group_map = {}

def _str(v):
	'''
	Return the value as a string or None.
	'''
	return str(v) if not v is None else None

def check_int(v):
	'''
	Return the int or None if it doesn't parse.
	'''
	try:
		return int(v)
	except:
		return None

def check_float(v):
	'''
	Return the float or None if it doesn't parse.
	'''
	try:
		return float(v)
	except:
		return None

def handle_dt(a):
	if isinstance(a, str):
		return datetime.strptime(a, '%d/%m/%Y %H:%M:%S').replace(tzinfo = timezone.utc)
	elif isinstance(a, datetime):
		return a.replace(tzinfo = timezone.utc)
	else:
		return None

def handle_sc(a):
	if a:
		while len(a) < 3:
			a = '0' + a
	return a



# A mapping from the Access database fields to the output database 
# fields and parse/validate function.
field_mapping = (
	('speciescode', 'species_code', handle_sc),
	('scientificname', 'scientific_name', _str),
	('latinname', 'latin_name', _str),
	('commonname', 'common_name', _str),
	('spspeciescode', 'sp_species_code', _str),
	('nodccodev7', 'nodc_code_v7', check_float),
	('nodccodev8', 'nodc_code_v8', check_int),
	('taxonomyclasslevelcode', 'rank', lambda a: rank_map[a] if a else None),
	('grouping', 'grouping', lambda a: group_map[a] if a else None),
	('speciesprovcode', 'species_prov_code', _str),
	('fromdate', 'from_date', handle_dt),
	('todate', 'to_date', handle_dt),
	('authority', 'authority', _str),
	('taxareference', 'taxa_reference', _str)
)


def init_maps():
	'''
	Populate the lookups for ranks and groupings.
	'''
	for r in TaxonomicRank.objects.all():
		rank_map[r.code] = r
	for t in TaxonomicGrouping.objects.all():
		group_map[t.code] = t


def load_taxa(mdbfile, tablename):
	'''
	Load the named taxonomy table from the Access database.
	'''
	
	init_maps()

	# Connect to the database.
	conn = pyodbc.connect(r'Driver={{Microsoft Access Driver (*.mdb, *.accdb)}};DBQ={};'.format(mdbfile))

	cur = conn.cursor()
	cur.execute('SELECT * FROM "{}"'.format(tablename))
	fields = [f[0] for f in cur.description]

	added = 0

	for row in cur:
		row = dict(zip(fields, row))

		# Hack for a weird entry in one of the table.
		if not row['speciescode'] or row['speciescode'] == 'Field of view':
			continue

		# Try to find the entry by Hart code.
		try:
			t = HartTaxon.objects.get(species_code = row['speciescode'])
		except: 
			# Create the data object for creation.
			data = {}
			for kfrom, kto, fn in field_mapping:
				data[kto] = fn(row.get(kfrom))

			try:
				t = HartTaxon.objects.create(**data)
				added += 1
			except Exception as e: 
				logging.warning(str(e))

	print('Added', added)

	
if __name__ == '__main__':
	
	mdbfile = sys.argv[1]
	tablename = sys.argv[2]

	load_taxa(mdbfile, tablename)