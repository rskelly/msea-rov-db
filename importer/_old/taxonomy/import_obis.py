#!/usr/bin/env python

import os
import sys
import csv
from datetime import datetime

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), 'app'))

from msea.util import django

fields = [
    'id', 
    'dataset_id', 
        'decimallongitude', 'decimallatitude', 'date_start', 'date_mid', 'date_end', 'date_year', 
    'scientificname', 
    'originalscientificname', 
        'minimumdepthinmeters', 'maximumdepthinmeters', 'depth', 'coordinateuncertaintyinmeters', 'flags', 'dropped', 'absence', 'shoredistance', 'bathymetry', 'sst', 'sss', 'marine', 'brackish', 'freshwater', 'terrestrial', 
    'taxonrank', 
    'aphiaid', 
        'redlist_category', 
    'superdomain', 
    'domain', 
    'kingdom', 
    'subkingdom', 
    'infrakingdom', 
    'phylum', 
    'phylum_division', 
    'subphylum_subdivision', 
    'subphylum', 
    'infraphylum', 
    'parvphylum', 
    'gigaclass', 
    'megaclass', 
    'superclass', 
    'class', 
    'subclass', 
    'infraclass', 
    'subterclass', 
    'superorder', 
    'order', 
    'suborder', 
    'infraorder', 
    'parvorder', 
    'superfamily', 
    'family', 
    'subfamily', 
    'supertribe', 
    'tribe', 
    'subtribe', 
    'genus', 
    'subgenus', 
    'section', 
    'subsection', 
    'series', 
    'species', 
    'subspecies', 
    'natio', 
    'variety', 
    'subvariety', 
    'forma', 
    'subforma', 
    'type', 
        'modified', 'language', 'license', 'rightsholder', 'accessrights', 'bibliographiccitation', 'references', 'institutionid', 'collectionid', 'datasetid', 'institutioncode', 'collectioncode', 'datasetname', 'ownerinstitutioncode', 'basisofrecord', 'informationwithheld', 'datageneralizations', 'dynamicproperties', 'materialsampleid', 'occurrenceid', 'catalognumber', 'occurrenceremarks', 'recordnumber', 'recordedby', 'recordedbyid', 'individualcount', 'organismquantity', 'organismquantitytype', 'sex', 'lifestage', 'reproductivecondition', 'behavior', 'establishmentmeans', 'occurrencestatus', 'preparations', 'disposition', 'othercatalognumbers', 'associatedmedia', 'associatedreferences', 'associatedsequences', 'associatedtaxa', 'organismid', 'organismname', 'organismscope', 'associatedoccurrences', 'associatedorganisms', 'previousidentifications', 'organismremarks', 'eventid', 'parenteventid', 'samplingprotocol', 'samplesizevalue', 'samplesizeunit', 'samplingeffort', 'eventdate', 'eventtime', 'startdayofyear', 'enddayofyear', 'year', 'month', 'day', 'verbatimeventdate', 'habitat', 'fieldnumber', 'fieldnotes', 'eventremarks', 'locationid', 'highergeographyid', 'highergeography', 'continent', 'waterbody', 'islandgroup', 'island', 'country', 'countrycode', 'stateprovince', 'county', 'municipality', 'locality', 'verbatimlocality', 'verbatimelevation', 'minimumelevationinmeters', 'maximumelevationinmeters', 'verbatimdepth', 'minimumdistanceabovesurfaceinmeters', 'maximumdistanceabovesurfaceinmeters', 'locationaccordingto', 'locationremarks', 'verbatimcoordinates', 'verbatimlatitude', 'verbatimlongitude', 'verbatimcoordinatesystem', 'verbatimsrs', 'geodeticdatum', 'coordinateprecision', 'pointradiusspatialfit', 'footprintwkt', 'footprintsrs', 'footprintspatialfit', 'georeferencedby', 'georeferenceddate', 'georeferenceprotocol', 'georeferencesources', 'georeferenceverificationstatus', 'georeferenceremarks', 'geologicalcontextid', 'earliesteonorlowesteonothem', 'latesteonorhighesteonothem', 'earliesteraorlowesterathem', 'latesteraorhighesterathem', 'earliestperiodorlowestsystem', 'latestperiodorhighestsystem', 'earliestepochorlowestseries', 'latestepochorhighestseries', 'earliestageorloweststage', 'latestageorhigheststage', 'lowestbiostratigraphiczone', 'highestbiostratigraphiczone', 'lithostratigraphicterms', 'group', 'formation', 'member', 'bed', 'identificationid', 'identifiedby', 'identifiedbyid', 'dateidentified', 'identificationreferences', 'identificationremarks', 'identificationqualifier', 'identificationverificationstatus', 'typestatus', 
    'taxonid', 'scientificnameid', 'acceptednameusageid', 
    'parentnameusageid', 
        'originalnameusageid', 'nameaccordingtoid', 'namepublishedinid', 'taxonconceptid', 'acceptednameusage', 'parentnameusage', 'originalnameusage', 'nameaccordingto', 'namepublishedin', 'namepublishedinyear', 'higherclassification', 'specificepithet', 'infraspecificepithet', 'verbatimtaxonrank', 'scientificnameauthorship', 
    'vernacularname', 
        'nomenclaturalcode', 'taxonomicstatus', 'nomenclaturalstatus', 'taxonremarks'
]

fields_comp = [
    'scientificname', 'vernacularname', 'taxonrank', 'aphiaid'
]

def run(filename, config_file):

    django.django_setup(config_file)

    from msea.models.taxonomy import WoRMSTaxon


    aphiaids = {}
    with open(filename, 'r', encoding='utf-8-sig') as f:
        db = csv.reader(f)
        head = next(db)
        field_idxs = [fields.index(f) for f in fields_comp]
        aphiaid_idx = head.index('aphiaid')
        for row in db:
            aphiaid = row[aphiaid_idx]
            if aphiaids.get(aphiaid):
                a = [aphiaids[aphiaid][i] for i in field_idxs]
                b = [row[i] for i in field_idxs]
                if any([a[i] != b[i] for i in range(len(a))]):
                    if len(''.join(a)) > len(''.join(b)):
                        aphiaids[aphiaid] = a
                        #print('old', a)
                    else:
                        aphiaids[aphiaid] = b
                        #print('new', b)
            aphiaids[row[aphiaid_idx]] = row

    for aphiaid, row in aphiaids.items():
        row = dict(zip(head, row))
        try:
            taxon = WoRMSTaxon.objects.get(pk=aphiaid)
            taxon.aphiaid = row['aphiaid']
            taxon.url = 'https://marinespecies.org/aphia.php?p=taxdetails&id=' + row['aphiaid'] if row.get('aphiaid') else None
            taxon.scientificname = row['scientificname']
            taxon.vernacularname = row['vernacularname']
            taxon.rank = row['taxonrank']
            try:
                taxon.parentnameusageid = int(row['parentnameusageid'])
            except: pass
            taxon.kingdom = row['kingdom']
            taxon.phylum = row['phylum']
            taxon.cls = row['class']
            taxon.order = row['order']
            taxon.family = row['family']
            taxon.genus = row['genus']
            taxon.citation = row['bibliographiccitation']
            taxon.ismarine = row['marine']
            taxon.isterrestrial = row['terrestrial']
            taxon.created_on = datetime.now()
            taxon.updated_on = datetime.now()
            taxon.save()
        except Exception as e:
            data = {
                'aphiaid': row['aphiaid'],
                'url': 'https://marinespecies.org/aphia.php?p=taxdetails&id=' + row['aphiaid'] if row.get('aphiaid') else None,
                'scientificname': row['scientificname'],
                'vernacularname': row['vernacularname'],
                'rank': row['taxonrank'],
                'kingdom': row['kingdom'],
                'phylum': row['phylum'],
                'cls': row['class'],
                'order': row['order'],
                'family': row['family'],
                'genus': row['genus'],
                'citation': row['bibliographiccitation'],
                'ismarine': row['marine'],
                'isterrestrial': row['terrestrial'],
                'created_on': datetime.now(),
                'updated_on': datetime.now(),
            }                     
            try:
                data['parentnameusageid'] = int(row['parentnameusageid'])
            except: pass

            WoRMSTaxon.objects.create(**data)

if __name__ == '__main__':

    run(sys.argv[1], sys.argv[2])