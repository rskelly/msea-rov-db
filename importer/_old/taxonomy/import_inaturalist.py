#!/usr/bin/env python

"""
Imports species from the iNaturalist export spreadsheet into the rov.inaturalist_taxon table.

Reconstructs the taxonomic hierarchy by looking through the rank names so iNaturalist exports must
include those fields.
"""

import os
import sys
import csv
import psycopg2
import logging
import json
from datetime import datetime

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))), 'app'))

# Import the utils to support this program.
from msea.util import util

def _ts(v):
	try:
		d, t, tz = v.split()
		#return datetime.strptime('{} {}'.format(d, t)).replace(tzinfo = timezone.UTC if tz == 'UTC' else timezone.PST)
		return datetime.strptime(v, '%Y-%m-%d %H:%M:%S %Z')
	except Exception as e:
		return None

def check_bool(v):
	try:
		if v[0] in ('t', 'f', '1') or v[0] == 1 or v[0] == True:
			return True
		elif v[0] in ('t', 'f', '0') or v[0] == 0 or v[0] == False:
			return False
	except: pass
	return None

types = (
	('observed_on', 'timestamp with time zone', lambda row: _ts(row['time_observed_at'])),
	('user_id', 'int4', lambda row: util.check_int(row['user_id'])),
	('user_login', 'varchar(64)', None),
	('created_at', 'timestamp with time zone', lambda row: _ts(row['created_at'])),
	('updated_at', 'timestamp with time zone', lambda row: _ts(row['updated_at'])),
	('quality_grade', 'varchar(32)', None),
	('license', 'varchar(32)', None),
	('url', 'text', None),
	('image_url', 'text', None),
	('sound_url', 'text', None),
	('tag_list', 'jsonb', lambda row: json.dumps(row['tag_list'].split(',')) if row.get('tax_list') else None),
	('description', 'text', None),
	('num_identification_agreements', 'int2', lambda row: util.check_int(row['num_identification_agreements'])),
	('num_identification_disagreements', 'int2', lambda row: util.check_int(row['num_identification_disagreements'])),
	('captive_cultivated', 'bool', lambda row: check_bool(row['captive_cultivated'])),
	('oauth_application_id', 'varchar(128)', None),
	('place_guess', 'varchar(64)', None),
	('latitude', 'float4', lambda row: util.check_int(row['latitude'])),
	('longitude', 'float4', lambda row: util.check_int(row['longitude'])),
	('positional_accuracy', 'int4', lambda row: util.check_int(row['positional_accuracy'])),
	('public_positional_accuracy', 'int4', lambda row: util.check_int(row['public_positional_accuracy'])),
	('geoprivacy', 'varchar(32)', None),
	('taxon_geoprivacy', 'varchar(32)', None),
	('coordinates_obscured', 'bool', lambda row: check_bool(row['coordinates_obscured'])),
	('positioning_method', 'varchar(32)', None),
	('positioning_device', 'varchar(32)', None),
	('species_guess', 'varchar(64)', None),
	('scientific_name', 'varchar(64)', None),
	('common_name', 'varchar(64)', None),
	('iconic_taxon_name', 'varchar(64)', None),
	('taxon_id', 'int4', lambda row: util.check_int(row['taxon_id'])),
	('taxon_kingdom_name', 'varchar(64)', None),
	('taxon_phylum_name', 'varchar(64)', None),
	('taxon_subphylum_name', 'varchar(64)', None),
	('taxon_superclass_name', 'varchar(64)', None),
	('taxon_class_name', 'varchar(64)', None),
	('taxon_subclass_name', 'varchar(64)', None),
	('taxon_superorder_name', 'varchar(64)', None),
	('taxon_order_name', 'varchar(64)', None),
	('taxon_suborder_name', 'varchar(64)', None),
	('taxon_superfamily_name', 'varchar(64)', None),
	('taxon_family_name', 'varchar(64)', None),
	('taxon_subfamily_name', 'varchar(64)', None),
	('taxon_supertribe_name', 'varchar(64)', None),
	('taxon_tribe_name', 'varchar(64)', None),
	('taxon_subtribe_name', 'varchar(64)', None),
	('taxon_genus_name', 'varchar(64)', None),
	('taxon_genushybrid_name', 'varchar(64)', None),
	('taxon_species_name', 'varchar(64)', None),
	('taxon_hybrid_name', 'varchar(64)', None),
	('taxon_subspecies_name', 'varchar(64)', None),
	('taxon_variety_name', 'varchar(64)', None),
	('taxon_form_name', 'varchar(64)', None),
)

# Mapping between the taxonomy rank columns and the rank IDs in the database.
taxon_levels = {
	'taxon_kingdom_name': 4,
	'taxon_phylum_name': 6,
	'taxon_subphylum_name': 8,
	'taxon_superclass_name': 16,
	'taxon_class_name': 7,
	'taxon_subclass_name': 9, 
	'taxon_superorder_name': 18,
	'taxon_order_name': 3,
	'taxon_suborder_name': 10, 
	'taxon_superfamily_name': 12, 
	'taxon_family_name': 1, 
	'taxon_subfamily_name': 11,
	'taxon_supertribe_name': 22,
	'taxon_tribe_name': 23,
	'taxon_subtribe_name': 24, 
	'taxon_genus_name': 2, 
	'taxon_genushybrid_name': 25, 
	'taxon_species_name': 5, 
	'taxon_hybrid_name': 19, 
	'taxon_subspecies_name': 13, 
	'taxon_variety_name': 26, 
	'taxon_form_name': 27
}

class Node:
	'''
	A node in the taxonomic tree.
	'''

	def __init__(self, name = 'Life', id = 0, rank = 0, otu = None, parent = None):
		'''
		Create a Node.
		- name - The name of the entity.
		- id - The iNaturalist taxonomy ID.
		- rank - The taxonomic rank ID.
		- otu - The OTU (only applies to species).
		- parent - The parent Node.
		'''
		self.nodes = {}			# List of child nodes indexed by name.
		self.name = name 		# The entity name.
		self.id = id 			# The iNaturalist taxon ID.
		self.rank = rank 		# The taxonomic rank ID.
		self.otu = otu 			# The OTU.
		self.parent = parent	# The parent Node
		self.model = None		# A reference to the Django model entity associated with this node.

	def add(self, name, id, rank, otu):
		'''
		Add a child to the Node.
		- name - The name of the entity.
		- id - The iNaturalist taxonomy ID.
		- rank - The taxonomic rank ID.
		- otu - The OTU (only applies to species).
		'''
		if not self.nodes.get(name):
			self.nodes[name] = Node(name, id, rank, otu, self)
		return self.nodes[name]

	def get(self, name):
		'''
		Return the child node by name.
		'''
		return self.nodes.get(name)

	def __repr__(self):
		items = []
		n = self
		while n:
			items.append(n.name)
			n = n.parent
		return ' > '.join(items[::-1])


def create_taxa(row, tree):
	'''
	Create a taxon by recursively creating or loading the parent taxa.
	'''
	debug = []
	level = None
	node = tree
	for rank, rank_id in taxon_levels.items():
		
		name = row.get(rank)
		otu = row['field:otu']
		taxon_id = util.check_int(row['taxon_id'])

		if name:
			node = node.add(name, taxon_id, rank_id, otu)


def create_observation(row):
	'''
	Save the observation rows. Not implemented.
	'''
	pass

def load_taxa(infile, dbconfig):

	with util.db_connect(dbconfig) as conn:

		cur = conn.cursor()

		with open(infile, 'r') as f:
			db = csv.reader(f)
			head = next(db)
			for row in db:
				row = dict(zip(head, row))
				
				taxon_id = row['taxon_id']
				
				if not taxon_id:
					continue
				try:
					taxon = iNaturalistTaxon.objects.get(taxon_id)
					del row['taxon_id']
					for k, v in row.items():
						setattr(taxon, k, v)
					taxon.save()
				except:
					taxon = iNaturalistTaxon.objects.create(**row)

if __name__ == '__main__':

	try:
		infile = sys.argv[1]
		config = sys.argv[2]

		config = util.load_config(config)
		load_taxa(infile, config)
	except Exception as e:
		logging.warning(str(e))
		print('Usage: python import_taxonomy_inaturalist.py <inat csv> <db config file>')
