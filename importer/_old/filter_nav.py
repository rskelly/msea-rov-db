#!/usr/bin/env python

'''
This script filters tracking data to a reasonable time period and optionally slices 
it up into "dives" corresponding to where the track approaches 0m depth.
'''

import csv
from datetime import datetime, timezone
import os
import sys

filename = sys.argv[1]
period = int(sys.argv[2]) # Seconds

dives = {}

with open(filename, 'r') as f:
	db = csv.reader(f)
	head = next(db)

	_t = None
	_dt = None
	i = 0
	d = 1

	with open('tmp.txt', 'w') as o:
		o.write('id,diveid,date,time,lat,lon,depth\n')

		for row in db:
			row = dict(zip(head, row))

			dt = datetime.strptime(row['time'][:row['time'].find('.')], '%H:%M:%S').replace(tzinfo = timezone.utc)
			t = dt.timestamp() / period
			if t == _t:
				continue

			if _dt and (dt - _dt).total_seconds() > 600:
				d += 1
				i = 0

			if not dives.get(d):
				dives[d] = {
					'dive': d,
					'date': row['date'],
					'start_time': dt,
					'end_time': dt,
					'dif': (dt - _dt).total_seconds() if _dt else 0
				}
			else:
				dives[d]['end_time'] = dt

			row['dive'] = d

			o.write('{id},{dive},{date},{time},{lat},{lon},{depth}\n'.format(**row))

			_t = t
			_dt = dt
			i += 1

print('dive,date,start_time,end_time')

for k in sorted(dives.keys()):
	print('{dive},{date},{start_time:%H:%M:%S},{end_time:%H:%M:%S}'.format(**dives[k]))

with open('tmp.txt', 'r') as f:
	line = f.readline()
	while line:
		print(line, end = '')
		line = f.readline()
