#!/usr/bin/env python

"""
Read the filename and frames from a video/photo report and calculate the time of the annotation
in ISO format.
"""

import os
import sys
import csv
import json
from datetime import datetime, timezone, timedelta

try:
    cruise = sys.argv[1]
    leg = sys.argv[2]
    infile = sys.argv[3]
    outfile = sys.argv[4]
    media_type = sys.argv[5]
except:
    print('Usage: biigle_datetimes.py <cruise> <leg> <infile> <outfile> <video|photo>')

if media_type == 'photo':
    medium_field = 'photo_filename'
else:
    medium_field = 'video_filename'


def get_offset(row):
    if media_type == 'video':
        try:
            frames = json.loads(row['frames'])
            return float(frames[0])
        except: pass
    return 0


with open(infile, 'r') as fi:
    dbi = csv.reader(fi)
    head = next(dbi)
    new_head = ['cruise_name', 'leg', 'dive_name', 'transect_name', 'media_config', 'timestamp']
    with open(outfile, 'w', newline='') as fo:
        dbo = csv.writer(fo)
        dbo.writerow(new_head + head)
        for row in dbi:
            row = dict(zip(head, row))
            filename = row[medium_field]
            offset = get_offset(row)
            try:
                ac = row['firstname'][0] + row['lastname'][0]
            except:
                ac = ''

            preamble, dive_name, date, time = os.path.splitext(filename)[0].split('_')

            d = datetime.strptime(date, '%Y%m%d')
            t = datetime.strptime(time, '%H%M%S')
            dt = d.replace(tzinfo=timezone.utc) + timedelta(hours=t.hour, minutes=t.minute,  seconds=t.second + offset)

            dbo.writerow([cruise, leg, dive_name, '', ac, '', dt.strftime('%Y-%m-%dT%H:%M:%SZ')] + [row[k] for k in head])
