#!/usr/bin/env python

'''
A simple script to collect the minisone files into one CSV file. 
Prints output to stdout.

Usage: minisonde2csv.py [dir path] [col1,col2,col3] > outfile.csv
'''

import sys
import os

dir = sys.argv[1]
head = sys.argv[2]

print(head)

for fn in [x for x in os.listdir(dir) if x.endswith('txt')]:
	with open(os.path.join(dir, fn), 'r') as f:
		line = f.readline()
		while line:
			try:
				int(line[0])	# Just check if the first char is a digit.
				print(line.strip())
			except: pass
			line = f.readline()
