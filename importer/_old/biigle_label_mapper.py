#!/usr/bin/env python

"""
This script extracts the list of unique hierarchic labels from a Biigle export
to assist in mapping from labels to Taxon entities. The required input is a Biigle
CSV export file. The output is a CSV file with the label in the first column, and
space for the Hart code in the second. The third column, if populated, shows whether
the program has found a canidate for the entity.

The program will attempt to lookup the entity based on the label.
"""

import os
import sys
import csv
import logging

import django
from django.db import transaction, connection
from datetime import datetime, timedelta, timezone

# Import the utils to support this program.
from util import *
import util

# Setup the Django environment.
django_setup()

# Import the MSEA models. Has to be done after setting up Django.
from msea.models import *
from msea.forms import *

filename = sys.argv[1]
outfile = sys.argv[2]

data = {}

with open(filename, 'r') as f:
	db = csv.reader(f)
	#next(db) # Skip one
	head = next(db)
	for row in db:
		row = dict(zip(head, row))
		label = row['label_hierarchy']
		if not data.get(label):
			data[label] = {'label': label, 'common_name': '', 'scientific_name': '', 'hart_code': '', 'inaturalist_taxon_id': '', 'auto': '', 'matched_name': ''}

for label, obj in data.items():

	# Get the rightmost element.
	labels = list(map(str.strip, label.split('>')))

	# For each space-delimited part of the label, try to find a match in the table. If the
	# match can't be made, strip the rightmost part and try with the shorter label, until
	# no label is left. Then try the next higher level. Finally, leave the guess blank.
	found = False
	while labels:

		label = labels.pop()

		while label and not found:
			# If a '-' is used to delineate a note within the label, remove it.
			label = label.split('-')[0].strip()
			try:
				t = Taxon.objects.get(scientific_name__iexact = label)
				obj['hart_code'] = t.hart_code
				obj['inaturalist_taxon_id'] = t.inaturalist_taxon_id
				obj['common_name'] = t.common_name
				obj['scientific_name'] = t.scientific_name
				obj['auto'] = 'true'
				obj['matched_name'] = label
				found = True
				break
			except Exception as e:
				logging.warning(str(e))
				pass
			try:
				tr = Taxon.objects.filter(scientific_name__icontains = label)
				for t in tr:
					obj['hart_code'] = t.hart_code
					obj['inaturalist_taxon_id'] = t.inaturalist_taxon_id
					obj['common_name'] = t.common_name
					obj['scientific_name'] = t.scientific_name
					obj['auto'] = 'true'
					obj['matched_name'] = label
					found = True
					break
			except Exception as e:
				logging.warning(str(e))
				pass
			# Split the last word off of the label.
			if label.find(' ') < 0:
				break
			label = ' '.join(label.split(' ')[:-1])

with open(outfile, 'w') as f:
	f.write('label,common_name,scientific_name,hart_code,inaturalist_taxon_id,auto,matched_name\n')
	for label, obj in data.items():
		for k, v in obj.items():
			if not v: 
				obj[k] = ''
		f.write('"{label}","{common_name}","{scientific_name}","{hart_code}","{inaturalist_taxon_id}","{auto}","{matched_name}"\n'.format(**obj))

