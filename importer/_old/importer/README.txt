ROV Import Program

The program has a single view with the following fields:

Configuration file selector -- Use to select the configuration file described below.
Destination selector -- The destination for the import, staging or development.
Download media files -- If selected, allow the importer to download media files from the configured location.
Recreate cruises -- If selected, delete and rebuild cruise entities on import. Otherwise, re-use and update the existing entities.
Recreate dives -- If selected, delete and rebuild dive entities on import. Otherwise, re-use and update the existing entities.
Recreate transects -- If selected, delete and rebuild transect entities on import. Otherwise, re-use and update the existing entities.
Do streams -- If selected, import the data and telemetry streams (measurements, positions). Existing data will be deleted.
Do annotations -- If selected, import the annotations. This will create observation, habitat, status and comment event events. Existing annotations are not deleted.
Do comments -- If selected, import comment events.
Do status -- If selected, import status events.


Stream data are deleted for each instrument config when a file is loaded. To avoid this with multiple files, the files cannot have measurements from the same instrument config. Data from one instrument config must all be in the same file.