#!/usr/bin/env python

"""
Import cruise and annotation data from logs and Biigle exports, configured
by import queue job data.

1. The cruise is already created.
2. The users are already created and mapped.
3. The label entities are already created and mapped.
4. The annotation protocol is already created.
5. Process equipment, dives, transects.
6. Process the annotations.
  a. Get the label and personnel maps.
  b. For each label create an observation, habitat, comment, status or measurement event.
7. Process positions.
8. Process CTD.
"""

import json
import csv
import os
import sqlite3
import sys
import importlib
import traceback
import argparse
from intervaltree import IntervalTree, Interval
from datetime import datetime, timedelta, timezone

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) , 'app'))

# Need the Django DB stuff for creating/saving entities.
from django.db import transaction

from utils import _status
from utils import *
from msea.util import util


class importer:
    """
    The importer application.
    """

    """
    Stores a mapping from the property name to the model class that will represent it.
    The value associated with the property will be an ID.
    """
    model_map = None

    def __init__(self, config_file):
        """
        Initialize the importer. 
        The mode determines where information is imported to: staging, production or development.
        The mode parameter is passed to Django, causing it to load the correct configuration.

        :param mode: The configuration mode that determines where the data imported to.
        """
        self.config_file = config_file

        self.tag_handlers = []          # Callables for tag handling.
        self.property_handlers = []     # Callables for property handling.
        self.iqpi = None                # Import queue data for principal investigators.
        self.iqas = {}                  # Import queue data for annotators. Keyed by the filename of the IQA.
        self.lookups = {}               # A cache of lookup class instances.
        self.cruise = None              # The cruise.
        self.dives = {}                 # Lookup of dives by name.
        self.transects = {}             # Lookup of transects by name.
        self.instrument_configs = {}    # Lookup of instrument configs by name.
        self.platform_configs = {}      # Lookup of platform configs.
        self.dive_tree = None           # An interval tree for dives; indexed on start/end times.
        self.transect_tree = None       # An interval tree for transects; indexed on start/end times.

        self.run_config = {}            # A config passed in at run.
        
        self.entities = {}              # A dictionary of loaded lookup entities.

        self.log = None                 # A reference to a logging object.


    def build_model_map(self):
        from msea.models import rov, shared
        self.model_map = {
            'observation/abundance': rov.Abundance,
            'observation/confidence': rov.ObservationConfidence,
            'observation/restriction': shared.Restriction,
            'observation/confidence': rov.ObservationConfidence,
            'observation/restriction': shared.Restriction,
            'habitat/substrate': rov.Substrate,
            'habitat/complexity': rov.Complexity,
            'habitat/flow': rov.Flow,
            'habitat/relief': rov.Relief,
            'habitat/biocover': rov.Biocover,
            'habitat/coverage': rov.Coverage,
            'habitat/disturbance': rov.Disturbance,
            'habitat/thickness': rov.Thickness,
            'habitat/restriction': shared.Restriction,           
            'status/detail': rov.StatusTypeDetail,
        }

    def get_tag_handler(self, name):
        """
        Get the tag handler corresponding to the given name(s).
        
        The name could be a simple string, or a slash-delimited property path.
        
        The name will be searched. If nothing is found, and the name is a path,
        the last element in the path will also be searched.

        :param name: The name of a tag for which handlers will be searched.
        :return: A handler corresponding to the tag name.
        :rtype: tag_handler
        """
        # Try first with the full name.
        for h in self.tag_handlers:
            if h.matches(name):
                return h
        # Try again with the last part.
        _, _, name = name.rpartition('/')
        for h in self.tag_handlers:
            if h.matches(name):
                return h
        return None
    
    def get_property_handler(self, name):
        """
        Get the property handler corresponding to the given name(s).

        :param name: The name of a tag for which handlers will be searched.
        :return: A handler corresponding to the tag name.
        :rtype: property_handler
        """
        for h in self.property_handlers:
            if h.matches(name):
                return h
        return None
    
    def load_handlers(self):
        """
        Load the handler modules from the tag and tag data handlers packages.
        :param log: An instance of the logger class.
        """
        self.log.trace(self, 'Loading tag handlers.')
        from tag_handlers.tag_handler import tag_handler
        for p in tag_handler.get_handlers():
            # Load the module from the handlers dir.
            mod = importlib.import_module('.' + p, 'tag_handlers')
            # Instantiate the handler if one is present.
            if hasattr(mod, 'tag_handler'):
                inst = mod.tag_handler()
                self.tag_handlers.append(inst)

        self.log.trace(self, 'Loading tag data handlers.')
        from property_handlers.property_handler import property_handler
        for p in property_handler.get_handlers():
            # Load the module from the handlers dir.
            mod = importlib.import_module('.' + p, 'property_handlers')
            # Instantiate the handler if one is present.
            if hasattr(mod, 'property_handler'):
                inst = mod.property_handler()
                self.property_handlers.append(inst)

    def load_iqpi(self, iqpi_file):
        """
        Load data from the principal investigators import queue job file (JSON).

        :param iqpi_file: The JSON file containing the data.
        :param log: An instance of the logger class.
        """
        self.log.trace(self, f'Loading import queue from PI: {iqpi_file}.')

        from msea.models.rov import Cruise

        # Load the file.
        with open(iqpi_file, 'r', encoding='utf-8-sig') as f:
            self.iqpi = json.loads(f.read())

        # The cruise from the PI is used, but the times from NDST override these,
        # and the note, summary and objective are appended.
        try:
            self.cruise = Cruise.objects.get(pk=self.iqpi['cruise']['id'])
        except:
            raise Exception(f"No cruise found with ID: {self.iqpi['cruise']['id']}.")

    def load_iqa(self, iqa_file):
        """
        Load data from the annotators import queue job file (JSON).

        Can store multiple IQA configurations. If there is more than one, annotation configurations
        must refer to the IQA using an "iqa_config" property. Otherwise the property is unnecessary.

        :param iqa_file: The JSON file containing the data.
        :param log: An instance of the logger class.
        """
        self.log.trace(self, f'Loading import queue from annotators: {iqa_file}.')

        from msea.models.rov import AnnotationProtocol, AnnotationJob, AnnotationJobAnnotationProtocol

        # Load the file.
        with open(iqa_file, 'r', encoding='utf-8-sig') as f:
            iqa = json.loads(f.read())

        # Convert the label keys into integers.
        labels = iqa['data']['project']['labels'] = {}
        for labelTree in iqa['data']['project']['labelTrees']:
            for label in labelTree['labels']:
                labels[int(label['id'])] = label

        # Create the job from fields in the import queue object.
        a_job = AnnotationJob.objects.create(
            **{k: iqa['data']['project'].get(k) for k in ('name', 'objective', 'note', 'start_date', 'end_date')}
        )

        # Load the referenced annotation protocols and save it for later. Exception on failure.
        # TODO: Loop format because a future version should accomodate multiple protocols.
        for ap in [iqa['data']['annotationProtocol']]:
            ap = AnnotationProtocol.objects.get(pk=ap['id'])
            AnnotationJobAnnotationProtocol.objects.create(annotation_job=a_job, annotation_protocol=ap)

        # Remember the objects for later use.
        iqa_file = os.path.basename(iqa_file)
        self.iqas[iqa_file] = {
            'iqa': iqa, 
            'annotation_job': a_job
        }

    def delete_dive_associated(self, dive, all):
        """
        Delete the associated events, measurements and configurations of a dive.

        :param dive: The dive object.
        :param all: True to delete all subsidiary objects, whether or not the recreate flag for that object is set.
        :param log: An instance of the logger class.
        """
        from msea.models.rov import Event, InstrumentConfig, Measurement, Position

        # Mapping of recreate switches to event types.
        evt_types = ('observation', 'comment', 'habitat', 'comment', 'status', 'measurement')

        # Delete any events slated for recreation, or all if all is set.
        if all or any([self.run_config.get(k) for k in evt_types]):
            self.log.trace(self, f'Deleting {", ".join([x for x in evt_types])} events for dive {dive}.')
        for k in evt_types:
            if all or self.run_config.get(k):
                Event.objects.filter(dive=dive, properties__event_types__icontains=k).delete()

        # If streams are set to recreate, delete them.
        if all or self.run_config.get('recreate_streams'):
            self.log.trace(self, f'Deleting streams for dive {dive}.')
            pconfigs = []
            # Configs may not exist on corrupt entities.
            try:
                pconfigs.append(dive.sub_config)
            except Exception as e:
                print(e)
            try:
                pconfigs.append(dive.ship_config)
            except Exception as e:
                print(e)
            for pconfig in pconfigs:
                ics = InstrumentConfig.objects.filter(platform_config=pconfig)
                measurements = Measurement.objects.filter(instrument_config__in=ics)
                positions = Position.objects.filter(instrument_config__in=ics)
                measurements.delete()
                positions.delete()

        # If dives are to be recreated, delete the platform configs (and instrument configs, etc.)            
        if all or self.run_config.get('recreate_dives'):
            self.log.trace(self, f'Deleting platform configs for dive {dive}.')
            # The configs may not exist on a corrupted entity.
            try:
                dive.sub_config.delete()
            except: pass
            try:
                dive.ship_config.delete()
            except: pass
            

    def load_ndst(self, ndst_file):
        """
        Load the cruise data from NDST, which includes dives, transects,
        configurations and personnel.

        :param ndst_file: The JSON file name containing cruise data.
        :param log: An instance of the logger class.
        """
        self.log.trace(self, f'Loading NDST data: {ndst_file}.')

        from msea.models.rov import DiveRole, Dive, Platform, PlatformConfig, \
            InstrumentConfig, DiveCrew, Transect, Instrument
        from msea.models.shared import Site, Person

        # Pre-load the role for pilots.
        pilot_role = DiveRole.objects.get(name='Pilot')

        # Get the configuration.
        with open(ndst_file, 'r', encoding='utf-8-sig') as f:
            cruise_obj = json.loads(f.read())

        # Delete dives attached to the cruise if required.
        if self.run_config.get('recreate_dives'):
            dives = Dive.objects.filter(cruise=self.cruise)
            for dive in dives:
                self.delete_dive_associated(dive, True)
            dives.delete()

        # To store dives and transects to add to interval trees.
        dive_intervals = []
        transect_intervals = []

        # These are used to track whether a name has appeared more than once. Duplicates are illegal.
        dive_names = set()

        # A dict to store entities that have been accounted for: instrument configs on platform configs and 
        # instrument config instruments on instrument configs. Items that are touched (created, loaded) are
        # added to the set. Those that exist in the database and are not in the set are deleted, along with
        # all subsidiary data.
        visited = {'platform_configs': {}, 'instrument_configs': {}}

        # Iterate over the dive objects and load or (re-)create them.
        for dive_obj in sorted(cruise_obj['dives'], key=lambda a: a['name']):

            # True if the dive should be recreated.
            recreate_dives = self.run_config['recreate_dives']

            self.log.trace(self, f"Processing dive {dive_obj['name']}.")

            if dive_obj['name'] in dive_names:
                raise Exception(f"The dive name {dive_obj['name']} was found more than once.")
            
            # Store the dive name.
            dive_names.add(dive_obj['name'])

            # Convenient references to configuration properties.
            pilots_obj = dive_obj['pilots']
            sub_config_obj = dive_obj.get('sub_config')
            if not sub_config_obj:
                raise Exception(f'Dive {dive_obj["name"]} does not have a sub configuration.')
            ship_config_obj = dive_obj.get('ship_config')
            if not ship_config_obj:
                raise Exception(f'Dive {dive_obj["name"]} does not have a ship configuration.')
            site_name = (dive_obj.get('site_name') or '').strip()

            # Stores instances of instrument configs.
            instrument_configs = {}

            # Locate the dive. If force is set, it will be deleted. If not it will be reused.
            try:
                dive = Dive.objects.get(cruise=self.cruise, name=dive_obj['name'])
            except Exception as e:
                self.log.debug(self, str(e))
                dive = None

            if not dive:
                # If there's no dive, we have to create everything anyway.
                recreate_dives = True

            if recreate_dives:
                # Delete the existing dive. Related objects will also be deleted either by cascade or triggers.
                if dive:
                    self.log.trace(self, f"Deleting dive {dive_obj['name']}")
                    self.delete_dive_associated(dive, True)
                    dive.delete()
                    dive = None

                # Create a site if it's given and there isn't one.
                site = None
                if site_name:
                    try:
                        site = Site.objects.get(name__iexact=site_name)
                    except Exception as e:
                        self.log.debug(self, str(e))
                        site = Site.objects.create(name=site_name)

            # Delete entities associated with the dive that are to be recreated.
            if dive:
                self.delete_dive_associated(dive, False)

            # Store the ship and sub configuration objects and instances, if available.
            pconfigs = {
                'sub_config': {'config': sub_config_obj, 'inst': dive.sub_config if dive else None},
                'ship_config': {'config': ship_config_obj, 'inst': dive.ship_config if dive else None}
            }

            # Iterate over and process the platform configurations.
            for pconfig_item in pconfigs.values():

                # Get the platform config object and instance (which may be null here).
                pconfig_obj = pconfig_item['config']
                pconfig_inst = pconfig_item['inst']

                if not pconfig_inst or recreate_dives:
                    # Create the sub configuration if null.
                    try:
                        sc = pconfig_obj['platform']['short_code']
                        platform = None
                        try:
                            platform = Platform.objects.get(short_code=sc)
                        except:
                            raise Exception(f'Failed to find platform, {sc}.')
                        pconfig_inst = PlatformConfig.objects.create(
                            platform=platform,
                            configuration=pconfig_obj['configuration'],
                            note=pconfig_obj['note']
                        )
                    except Exception as e:
                        raise Exception(f'Failed to create platform config: {str(e)}.')
                
                pconfig_item['inst'] = pconfig_inst

                # Add the instrument configurations to the sub config.
                for ic_obj in pconfig_obj['instrument_configs']:
                    try:
                        # Find the existing config.
                        ic = InstrumentConfig.objects.get(platform_config=pconfig_inst, name=ic_obj['name'])
                    except: 
                        ic = None
                    
                    if not ic or recreate_dives:
                        # If there was no config, or we're forcing it, create the config.
                        try:
                            instrument = Instrument.objects.get(short_code=ic_obj['instrument']['short_code_mapped'])
                        except Exception as e:
                            raise Exception(f'Could not find instrument {ic_obj["instrument"]["short_code_mapped"]} ({ic_obj["short_code"]}).: ' + str(e))
                        
                        ic = InstrumentConfig.objects.create(
                            instrument=instrument,
                            platform_config=pconfig_inst,
                            configuration=ic_obj['configuration'],
                            name=ic_obj['name'],
                            note=ic_obj['note']
                        )

                    # Save the config for the sub config.
                    if not visited['platform_configs'].get(pconfig_inst):
                        visited['platform_configs'][pconfig_inst] = set()
                    visited['platform_configs'][pconfig_inst].add(ic)

                    # Save the config for later use.
                    instrument_configs[ic.name] = ic

            if recreate_dives:
                # Create the dive object.
                try:
                    start_time = parse_time(dive_obj['start_time'])
                    end_time = parse_time(dive_obj['end_time'])
                    if start_time >= end_time:
                        raise Exception('End time must be later than start time. Dive name: ' + dive_obj.get('name'))

                    dive = Dive.objects.create(
                        cruise=self.cruise,
                        site=site,
                        sub_config=pconfigs['sub_config']['inst'],
                        ship_config=pconfigs['ship_config']['inst'],
                        name=dive_obj['name'],
                        objective=dive_obj['objective'],
                        summary=dive_obj['summary'],
                        note=dive_obj['note'],
                        start_time=start_time,
                        end_time=end_time,
                    )
                except Exception as e:
                    raise Exception(f'Failed to create dive: {str(e)}.')

            # Add the dive to the interval tree. Add 1s to 
            dive_intervals.append(Interval(to_epoch(dive.start_time), to_epoch(dive.end_time) + 1, dive))
            self.dives[dive.id] = dive

            # Save the instrument configs for this dive to use for pos/meas/ori creation later.
            self.instrument_configs[dive.id] = instrument_configs

            if recreate_dives:
                # Create the pilot relationships.
                for pilot_obj in pilots_obj:
                    person = Person.objects.get(pk=pilot_obj['person_id'])
                    DiveCrew.objects.create(dive=dive, person=person, dive_role=pilot_role)

                # Create the transect objects and link to the dive.
                for trans_obj in dive_obj['transects']:
                    start_time = parse_time(trans_obj['start_time'])
                    end_time = parse_time(trans_obj['end_time'])
                    if start_time >= end_time:
                        raise Exception(f'End time for {trans_obj["dive_name"]}, {trans_obj["name"]} must be later than start time.')

                    trans = Transect.objects.create(
                        dive=dive,
                        name=trans_obj['name'],
                        objective=trans_obj['objective'],
                        summary=trans_obj['summary'],
                        note=trans_obj['note'],
                        start_time=start_time,
                        end_time=end_time,
                    )

                    # Add the transect to the interval tree.
                    transect_intervals.append(Interval(to_epoch(trans.start_time), to_epoch(trans.end_time) + 1, trans))
                    self.transects[trans_obj['name']] = trans

        # Initialize the interval trees.
        self.dive_tree = IntervalTree(dive_intervals)
        for di in dive_intervals:
            try:
                res = self.dive_tree.overlap(di.begin, di.end)
            except Exception as e:
                self.log.debug(self, str(e))
            for r in res:
                if r.data != di.data:
                    raise Exception(f"There are overlapping dives: {r.data.name} and {di.data.name}.")
                
        self.transect_tree = IntervalTree(transect_intervals)
        for ti in transect_intervals:
            try:
                res = self.transect_tree.overlap(ti.begin, ti.end)
            except Exception as e:
                self.log.debug(self, str(e))
            for r in res:
                if r.data != ti.data:
                    raise Exception(f"There are overlapping transects from dives {r.data.dive.name} and {ti.data.dive.name}: {r.data.name} and {ti.data.name}.")


    def get_personnel_map(self, iqa_file=None):
        """
        Load the users in the personnel map and create a mapping
        from Biigle user ID to Person entity.

        :param iqa_file: An optional filename to retreive the correct IQA object, if there is more than one. Not required if there is only one.
        :return: A dict containing the Biigle ID to Person mapping.
        :rtype: dict
        """
        from msea.models.shared import Person

        iqa = self.get_iqa(iqa_file)

        map = {}
        for k, pm in iqa['data']['project']['members'].items():
            map[int(pm['member']['id'])] = Person.objects.get(pk=pm['person']['id'])
        return map

    def resolve_lookup(self, prop, value):
        """
        Given an object with an id and cls fields, resolve the model
        instance and return it.

        :param obj: An object with an id and cls fields.
        :param log: An instance of the logger class.
        :return: A model instance.
        :rtype: django.db.models.Model
        """

        self.log.trace(self, 'Resolve lookup.')

        # Get the ID from the value. If it's legit, save it as the ID.
        id = 0
        try:
            id = int(value)
        except:
            return value

        # Find the model class for the property. If there isn't one, return the original value. 
        cls = self.model_map.get(prop)
        if not cls:
            return value
        
        # Make sure there's space in the cache.
        if not self.lookups.get(cls):
            self.lookups[cls] = {}

        # If the instance is in the cache return it.
        inst = self.lookups.get(cls, {}).get(id)
        if inst:
            return inst

        # Try to instantiate the class.
        try:
            inst = cls.objects.get(pk=id)
        except Exception as e:
            # Avoid a failed lookup by storing None.
            self.lookups[cls][id] = None
            return None

        # Save to the cache.
        self.lookups[cls][id] = inst

        return inst

    def resolve_label_map(self, iqa_file=None):
        """
        For any tag data item that references a lookup, resolve the lookup
        to the appropriate model instance.

        :param iqa_file: If more than one IQA configuration is present, an IQA file is necessary to retrieve it, otherwise not.
        :param log: An instance of the logger class.
        """
        self.log.trace(self, 'Resolving label map.')
        
        iqa = self.get_iqa(iqa_file)

        if iqa:
            for id, data in iqa['data']['project']['labels'].items():
                for k, v in data['properties'].items():
                    data['properties'][k] = self.resolve_lookup(k, v)

    def make_temp_video_biigle_db(self, db_file):
        """
        Create a temporary database to hold the annotations. This allows sorting and filtering.

        :param db_file: The file name of the annotation report.
        :param log: An instance of the logger class.
        :return: The database connection.
        """

        self.log.trace(self, 'Creating temporary Biigle database.')

        # Make a temp file and close the handle.
        tmp = util.maketempfile()
        name = tmp.path

        # Open the database.
        conn = sqlite3.connect(name)
        cur = conn.cursor()

        # Open the CSV file and load it.
        with open(db_file, 'r', encoding='utf-8-sig') as f:
            db = csv.reader(f)
            head = next(db)

            # Column names, add start and end times, comma delimited.
            col_names = ','.join(head + ['start_time', 'end_time'])
            # String of comma-delimited wildcards, one for each column.
            col_values = ','.join(['?'] * (len(head) + 2))
            # Colum DDL for table creation. The field type comes from the ddl_type function.
            cols_ddl = ','.join([f'{col} {ddl_type(col)}' for col in head] + ['start_time timestamp', 'end_time timestamp'])
            # Create the table.
            cur.execute(f'create table tmp ({cols_ddl})')

            # Get indices of important columns.
            frames_idx = head.index('frames')
            video_idx = head.index('video_filename')

            def commit(t0, t1, dt):
                """
                Called below. Insert an observation row in the temp DB.
                """
                start_time = dt + timedelta(seconds=t0)
                end_time = dt + timedelta(seconds=t1) if t1 else None
                if end_time and start_time >= end_time:
                    raise Exception(f'End time must be later than start time: {start_time}, {end_time}, frames: {frames}.')

                # Insert the record into the table with the start and end times.
                sql = f'insert into tmp ({col_names}) values ({col_values})'
                cur.execute(sql, row + [start_time, end_time])

            for row in db:
                if not any(row):
                    continue

                # Calculate the event start and end times from the datetime in the video and the frames (which are seconds).
                # If there's one frame, there's only a start time, and end time is null. Otherwise, the last frame is end time.
                # If there is a null in the list, there's a break between segments 
                dt = parse_file_datetime(row[video_idx])
                frames = sorted(json.loads(row[frames_idx]))

                t0 = frames[0]
                t1 = frames[-1] if len(frames) > 1 else None

                commit(t0, t1, dt)

                # TODO: Enable this if frame lists with gaps (nulls) should be recorded as multiple observations.
                # t0 = frames[0]
                # t1 = None
                # for i in range(1, len(frames)):
                #     t = frames[i]
                #     if t is None:
                #         if not t0 is None:
                #             commit(t0, t1, dt)
                #             t0 = None
                #             t1 = None
                #     else:
                #         if t0 is None:
                #             t0 = t
                #         else:
                #             t1 = t
                # if not t0 is None:
                #     commit(t0, t1, dt)

        return conn

    def make_temp_video_vm_db(self, db_file):
        """
        Create a temporary database to hold the annotations. This allows sorting and filtering.

        :param db_file: The file name of the annotation report.
        :param log: An instance of the logger class.
        :return: The database connection.
        """

        self.log.trace(self, 'Creating temporary VideoMiner database.')

        tmp = util.maketempfile()
        name = tmp.path

        conn = sqlite3.connect(name)
        cur = conn.cursor()
        with open(db_file, 'r', encoding='utf-8-sig') as f:
            db = csv.reader(f)
            head = next(db)

            # Column names, add start and end times, comma delimited.
            col_names = ','.join(head)
            # String of comma-delimited wildcards, one for each column.
            col_values = ','.join(['?'] * len(head))
            # Colum DDL for table creation. The field type comes from the ddl_type function.
            cols_ddl = ','.join([f'{col} {ddl_type(col)}' for col in head])
            # Create the table.
            cur.execute(f'create table tmp ({cols_ddl})')

            for row in db:
                if not any(row):
                    continue
                # Insert the record into the table with the start and end times.
                sql = f'insert into tmp ({col_names}) values ({col_values})'
                cur.execute(sql, row)

        return conn

    def create_non_annotated_tree(self, label_map, cur):
        """
        Create an interval tree of not annotated labels. This enables the application to search
        by timestamp to see whether the current segment should be annotated.

        :param label_map: A map of labels, keyed by label ID.
        :param cur: An open cursor on the temporary database connection.
        :return: An interval tree.
        :rtype: IntervalTree
        """
        not_annotated = []

        # Search the DB for non-annotated sections.
        cur.execute('select * from tmp order by annotation_id, start_time, end_time')
        cols = [col[0] for col in cur.description]
        for row in cur:
            # Format row as dict and retrieve the label.
            row = dict(zip(cols, row))
            label = label_map.get(row['label_id'])
            # TODO: Removed to handle user-generated tags.
            # if not label:
            #    raise Exception(f"The label map doesn't have an entry for label ID {row['label_id']}.")
            if has_tag(label, 'not_annotated'):
                # If a tag corresponding to the label is available, add the section tot he tree.
                not_annotated.append(Interval(to_epoch(parse_time(row['start_time'])), to_epoch(parse_time(row['end_time'])) + 1))
        return IntervalTree(not_annotated)

    def trim_label(self, start_time, end_time, not_annotated):
        """
        Trim or remove labels according to their intersections with non-annotated zones.

        :param start_time: The start time.
        :param end_time: The end time.
        :param not_annotated: An interval tree containing the start/end times of non-annotated zones.
        :param log: An instance of the logger class.
        :return: A tuple containing whether the record should be kept and the new start and end times.
        """
        if end_time and start_time >= end_time:
            raise Exception('End time must be later than start time.')

        # If the label isn't in the keeps list, trim or filter it based on non-annotated zones.
        # Get NA zones overlapping the start and end times. End can be None.
        try:
            na_start = not_annotated.at(to_epoch(start_time))
            na_end = not_annotated.at(to_epoch(end_time)) if end_time else None
            # If there's a start overlap, calculate its timespan, else None.
            if na_start and len(na_start) > 0:
                na_start = (min([x.begin for x in iter(na_start)]), max([x.end for x in iter(na_start)]))
            else:
                na_start = None
        except Exception as e:
            self.log.debug(self, str(e))
        # If there's an end overlap, calculate its timespan, else None.
        try:
            if na_end and len(na_end) > 0:
                na_end = (min([x.begin for x in iter(na_end)]), max([x.end for x in iter(na_end)]))
            else:
                na_end = None
        except Exception as e:
            self.log.debug(self, str(e))
        na_end = [from_epoch(x) for x in na_end] if na_end else None
        na_start = [from_epoch(x) for x in na_start] if na_start else None
        # If the label only has a start time, and it overlaps an NA zone, filter it out.
        if start_time and not end_time and na_start:
            if (start_time >= na_start[0]) and (start_time <= na_start[1]):
                return False, start_time, end_time
        # If the label has a start time and it is lower than the NA's end time, trim it.
        if na_start and start_time < na_start[1]:
            start_time = na_start[1]
        # If the label has an end time and it is higher than the NA's start time, trim it.
        if na_end and end_time > na_end[0]:
            end_time = na_end[0]
        # If the label has an end time and the span is <=0, filter it out.
        if end_time and (end_time - start_time).total_seconds() <= 0:
            return False, start_time, end_time

        return True, start_time, end_time

    def get_dive(self, time):
        """
        Return the dive at the specific time. If there is no dive, or more than one dive,
        raise an exception.

        :param time: The time.
        :param log: An instance of the logger class.
        :return: A dive.
        """
        try:
            result = self.dive_tree.at(to_epoch(time))
        except Exception as e:
            self.log.debug(self, str(e))
        dive = None
        if len(result) == 1:
            # If one dive is found, it is the result.
            dive = list(result)[0].data
        elif len(result) == 0:
            # If no dives are found, there is an error.
            raise Exception(f'No dive was found at time {time}. This is an input problem.')
        elif len(result) > 1:
            # If more than one dive is found, there is an error.
            dive_names = ', '.join([dive.data.name for dive in list(result)])
            raise Exception(f'More than one dive was found at {time} ({dive_names}). This is an input problem.')
        return dive

    def get_transect(self, dive, time):
        """
        Get the transect at the current time. If there is more than one transect, or the transect
        is not a child of the given dive, an Exception is raise. No transect is a permissible result.

        :param dive: The dive, a parent of the transect.
        :param time: The time.
        :param log: An instance of the logger class.
        :return: A transect or None.
        """
        # Zero transects is allowed.
        try:
            result = self.transect_tree.at(to_epoch(time))
        except Exception as e:
            self.log.debug(self, str(e))
        transect = None
        if len(result) == 1:
            # If one transect is found, it is the result.
            transect = list(result)[0].data
        elif len(result) > 1:
            # If more than one transect is found, there is an error.
            transect_names = [transect.data.name for transect in list(result)]
            raise Exception(
                f'More than one transect was found at {time} during dive {dive.name} ({transect_names}). This is an input problem.')
        # Sanity check: the transect must belong to the dive.
        if transect and transect.dive != dive:
            raise Exception(
                f'The transect {transect.name} should belong to dive {transect.dive.name} but {dive.name} was found.')
        return transect

    def get_iqa(self, iqa_file=None):
        """
        Returns the configured IQA for the given file.
        If there are more than one configured IQA objects, the one corresponding to the file is returned.
        If a file is given and there are no IQA objects, an exception is raised.
        If a file is not given and there are more than one IQA objects an exception is raised.
        :param iqa_file: An IQA configuration file or None.
        :return: An IQA object.
        """
        if iqa_file:
            iqa_file = os.path.basename(iqa_file)
            iqa = self.iqas[iqa_file]['iqa']
        elif len(self.iqas) > 1:
            raise Exception('No IQA file was given but there are more than one IQAs configured. This is an input error.')
        elif self.iqas:
            k = list(self.iqas.keys())[0]
            iqa = self.iqas[k]['iqa']
        else:
            iqa = None
        return iqa
    
    def get_annotation_protocol(self, iqa_file=None):
        """
        Returns the configured AnnotationProtocol for the given IQA file.
        If there are more than one configured IQA objects, the AP corresponding to the file is returned.
        If a file is given and there are no IQA objects, an exception is raised.
        If a file is not given and there are more than one IQA objects an exception is raised.
        :param iqa_file: An IQA configuration file or None.
        :return: An AnnotationProtocol object.
        """
        if iqa_file:
            iqa_file = os.path.basename(iqa_file)
            iqa = self.iqas[iqa_file]['annotation_protocol']
        elif len(self.iqas) > 1:
            raise Exception('No IQA file was given but there are more than one IQAs configured. This is an input error.')
        else:
            iqa = next(self.iqas)['annotation_protocol']
        return iqa
    
    def get_label_map(self, iqa_file=None):
        """
        Retrieve the label map from the IQA object.
        
        :param iqa_file: If there is more than one IQA configured, the filename is required to retreive the correct one, otherwise not required.
        :return: The label map.
        """
        
        iqa = self.get_iqa(iqa_file)

        return iqa['data']['project']['labels']

    def process_biigle_annotations(self, config_file):
        """
        Process video annotations.

        Previous versions of this method would resolve conflicts between labels by using the one with the higher label ID,
        which usually implies a more specific label. However, this isn't always true. Now, an annotation with more than one 
        label will be flagged, and a list of conflicting labels will be saved. This should be used by the annotator
        to resolve the conflicts.

        :param config_file: The file a Biigle config with an array named biigle_annotation_reports containing Biigle report file names.
        :param log: An instance of the logger class.
        :return: 
        """
        
        self.log.trace(self, 'Processing Biigle annotations.')

        config, _ = load_config(config_file)
        for db_file in config['biigle_annotation_reports']:

            self.log.trace(self, f'Processing Biigle annotations: {db_file}.')

            # Get the label and user maps format the label map keys into integers.
            label_map = self.get_label_map(config.get('iqa_file'))
            user_map = self.get_personnel_map(config.get('iqa_file'))

            # Skip records with these labels.
            skip_labels = ('ignore', 'not_annotated')
            # Keep records with these labels and protect from trimming and filtering.
            keep_labels = ('laser_point',)

            with self.make_temp_video_biigle_db(os.path.join(os.path.dirname(config_file), db_file)) as conn:
                cur = conn.cursor()

                # Store collections of annotations with the same ID until the ID changes, or
                # the table is finished. Track by last_annotation_id.
                label_collection = {}
                last_annotation_id = -1

                # Construct not-annotated tree. Labels that occure during non-annotated regions will be truncated
                # or ignored.
                not_annotated = self.create_non_annotated_tree(label_map, cur)

                # Select ordered rows to process.
                cur.execute('select * from tmp order by annotation_id, start_time, end_time')
                cols = [col[0] for col in cur.description]
                
                # A set to contain general label IDs that should be deleted after this loop.
                # General items are labels that apply to every label in the segment.
                # TODO: Is this still a thing?
                gen_ids = set()

                for row in cur:
                    # Format row as dict.
                    row = dict(zip(cols, row))

                    # Get the label ID and label dictionary.
                    label_id = row['label_id']
                    label = label_map.get(label_id)
                    if not label:
                        raise Exception(f"There is no  label for label ID {label_id} ({row['label_hierarchy']}).")

                    # Parse the label start and end times.
                    start_time = parse_time(row['start_time'])
                    end_time = parse_time(row['end_time'])
                    if end_time and start_time >= end_time:
                        raise Exception('Biigle annotation end time must be later than start time.')

                    # If the label is marked general, it is intended to be applied to any habitat or observation
                    # event (TODO: maybe others?) that falls within its time span. These are added to an interval tree
                    # to make them available to the handlers.
                    if label.get('general'):
                        gen_ids.add(label_id)
                        if map(lambda a: a['label'] == 'habitat', label['tags']):
                            self.get_tag_handler('habitat').add_general_tags(label['tags'], start_time, end_time)
                        elif map(lambda a: a['label'] == 'observation', label['tags']):
                            self.get_tag_handler('observation').add_general_tags(label['tags'], start_time, end_time)
                        continue

                # Delete the general labels.
                gen_ids = list(gen_ids)
                for i in range(0, len(gen_ids), 100):
                    cur.execute('delete from tmp where label_id in ({})'.format(','.join(list(map(str, gen_ids[i:i+100])))))

                # Select ordered rows to process.
                cur.execute('select * from tmp order by annotation_id, start_time, end_time')
                cols = [col[0] for col in cur.description]
                ridx = 0
                for row in cur:
                    # Format row as dict.
                    row = dict(zip(cols, row))

                    # Get the label ID and label dictionary.
                    label_id = row['label_id']
                    label = label_map[label_id]
                    if not label:
                        raise Exception(f"There is no  label for label ID {label_id} ({row['label_hierarchy']}).")

                    # Parse the label start and end times.
                    start_time = parse_time(row['start_time'])
                    end_time = parse_time(row['end_time'])
                    if end_time and start_time >= end_time:
                        raise Exception('End time must be later than start time.')

                    # Skip labels that should be ignored or that have already been processed.
                    if has_tag(label, skip_labels):
                        continue

                    # Filter or trim labels by non-annotated zones.
                    if not has_tag(label, keep_labels):
                        keep, st, et = self.trim_label(start_time, end_time, not_annotated)
                        if not keep:
                            continue
                        start_time = st
                        end_time = et

                    # Check for a change in annotation ID, finalize annotations on change.
                    annotation_id = row['annotation_id']
                    if annotation_id != last_annotation_id:
                       if last_annotation_id > 0:
                           self.finalize_annotation_collection([label_collection[last_annotation_id]])
                           del label_collection[last_annotation_id]
                    last_annotation_id = annotation_id

                    # Get the dive and transect at the current time.
                    try:
                        dive = self.get_dive(start_time)
                    except Exception as e:
                        raise Exception(f'Error in file {os.path.basename(db_file)}: ' + str(e))
                    transect = self.get_transect(dive, start_time)

                    # Get the medium file name.
                    medium_filename = row.get('video_filename', row.get('photo_filename'))

                    # TODO: Should shape be in m or screen coordinates?
                    if not label_collection.get(annotation_id):
                        if not self.main_config.get('iqa_files'):
                            raise Exception('At least one IQA file is required.')
                        if len(self.main_config.get('iqa_files', [])) > 1:
                            raise Exception("Can't deal with multipl iqa files yet.")
                        for iqa_file in self.main_config.get('iqa_files'):
                            ctx = label_collection[annotation_id] = {
                                '_iqa_file': iqa_file,        # The IQA file if there is one, to select the correct annotation job.
                                '_rows': [row],
                                'original_id': annotation_id,
                                'dive': dive,
                                'transect': transect,
                                'start_time': start_time,
                                'end_time': end_time,
                                'shape': {'type': row['shape_name'].lower(), 'points': json.loads(row['points'])},
                                'frames': row['frames'],
                                'original_labels': [row['label_hierarchy']],
                                'annotation_job': self.iqas[iqa_file]['annotation_job'],
                                'medium_filename': medium_filename,
                            }
                    else:
                        ctx = label_collection[annotation_id]

                    # Get the mapped user.
                    mapped_user = user_map.get(int(row['user_id']))
                    if not mapped_user:
                        #raise Exception(f"There is no mapped user for user {row['user_id']} ({row['firstname']} {row['lastname']})")
                        # TODO: Requires an update to Biigle API.
                        self.log.error(self, f"There is no mapped user for user {row['user_id']} ({row['firstname']} {row['lastname']})")
                    
                    # Check the tags.
                    for tag in label['tags']:
                        try:
                            # TODO: For the old version which had propName.
                            prop = tag['propName']
                        except:
                            prop = tag
                        handler = self.get_tag_handler(prop)
                        if handler:
                            handler(ctx, tag, label, mapped_user, [row], self.log)

                    # Check the tag data.
                    for prop_name, data in label['properties'].items():
                        handler = self.get_property_handler(prop_name)
                        if handler:
                            handler(ctx, prop_name, data, label, mapped_user, [row], self.log, self.main_config)

                    ridx += 1

                # Finalize the remaining labels.
                self.finalize_annotation_collection(list(label_collection.values()))
        
            from property_handlers import property_handler
            property_handler.write_conflicts(os.path.splitext(db_file)[0] + '_conflicts.csv')

    def get_entity(self, cls, prop, value):
        """
        Get the class instance from a registry keyed by class name and a property value.
        If the instance is not in the cache, it is loaded.

        :param cls: The name of a class, or the class itself.
        :param prop: The name of a property to match on (e.g., short_code or id).
        :param value: The value of the key. Should return one instance.
        :return: The cached instance.
        """
        if not value:
            return None
        # Get the class name: if it's a string use it; a class, get its name.
        cname = cls if type(cls) == 'str' else cls.__name__
        # Create the dictionary for the type.
        if not self.entities.get(cname):
            self.entities[cname] = {}
        # Format the key; if the entity isn't found, load and register it.
        key = f'{prop}:{value}'
        if not self.entities[cname].get(key):
            self.entities[cname][key] = cls.objects.get(**{prop: value})
        # Return the item.
        return self.entities[cname][key]

    def process_vm_annotations(self, config_file, precheck=False):
        """
        Convert the access file to a CSV file, replacing column lookup values with the real values.

        1) Load configurations:
            a) config is the VideoMiner label config file (vm_label_config.json), which contains column information
               about the database, a mapping of data codes to action names and a dictionary of label groups.
               See label_groups below.
            b) The filename is the filename of the VideoMiner database (MS Access).
            c) The label_map is the user-configured label map from the import queue. label_map_ is a dictionary
               form of the list.
            d) The skip_labels list is a list of labels that should be skipped. Any object that contains one of
               these labels will be ignored.
            e) The keep_labels list is a list of labels that will not be skipped, even if there's no other data.
            f) A label_group describes an event which is created for each action along with a list of fields that
               will be read to build up the event from column values. The value_columns property reads values directly
               from the database into a property on the event (e.g., comments); the labels property builds single-field
               labels in the form [category] > [name]: [value], while the columns property builds multiple-field labels
               in the form, [category] > [name]: [value], [name]: [value], etc. These last to correspond to the labels
               mapped in the label mapping application.
        2) Load the lookups from the database. Some database columns contain lookup values. These are looked up
           on the fly from a pre-loaded table of lookups.
        2) Sort the input database on the configured fields.
        3) Iterate over the rows.
            a) For each row, get the action names for the row's data code from the data_code_map. Some actions apply
               directly to the action code, others apply to every row.
            b) Get the labels that can be applied to the current row.
            c) If the row is a transect start or end, persist all the events currently stored in state with
               end times set to the time of the previous row. At this stage, tags and tag data are used to populate
               the event with row values before persisiting.
            c) Iterate over the actions in the action list and persist any event contexts in state with those names.
               Delete each one so that a new one will be created for that action.
            d) Get the row timestamp and the dive and transect for that time.
            e) For each action/labels pair (step b), process the label group to see if a context can be created.
               If the context represents a valid comment or observation, they are saved immediately. Otherwise
               they are preserved to be closed by a later row/time. Label lists that include skip labels are skipped.
        4) Persist any objects in state with the time of the last row.

        :param config_file: The configuration file.
        :param out_file: The output file.
        """
        from utils import vm_extract_labels as vm

        # Load the configuration.
        config = vm.load_label_config(config_file)

        # If the file isn't a list turn it into one.
        db_file = config['db_file']
        if not type(db_file) == list:
            db_file = [db_file]
        
        # Iterate over the files.
        for db_file in db_file:
            # Extract the DB file name.
            db_file = os.path.join(os.path.dirname(config_file), db_file)

            self.log.trace(self, f'Processing VideoMiner annotations: {db_file}.')

            # Get the label map and tansform the dictionary from integer keys to string keys:
            # the VM labels don't have IDs so we have to use the labels themselves as keys.
            label_map = {v['name']: v for v in self.get_label_map(config.get('iqa_file')).values()}

            # Skip records with these labels.
            skip_labels = ('ignore', 'not_annotated')
            # Keep records with these labels and protect from trimming and filtering.
            keep_labels = ('laser_point',)
            # Get the configured label groups.
            groups = config['label_groups']
            # The location where videos/photos should be stored.
            media_path = config.get('media_path')
            if media_path:
                try:
                    os.makedirs(media_path)
                except: pass

            # Stores the context objects used to create events.
            state = {}

            # Query the database. Sort using the configured sort columns or the ID column.
            if config.get('sort_columns'):
                sort_columns = ','.join([f'[{v}]' for v in config['sort_columns']])
            else:
                sort_columns = f"[{config['id_column']}]"

            # Connect to the DB and get a cursor. Query the sorted data.
            conn = vm.connect_access(db_file)
            cur = conn.cursor()

            # Load values for all the configured lookups.
            lookups = vm.get_lookups(cur, config)

            if precheck:
                # Perform some checks before starting.
                # 1) Check that all the dates in the date column are valid.
                # 2) Check that a dive is available for every datetime.
                # 3) TODO: More?
                cur.execute(f"select * from [{config['data_table']}]")
                # Get a list of column names.
                cols = [col[0] for col in cur.description]
                errors = []
                ids = set()
                for row in cur:
                    # Turn the row into a dict.
                    row = dict(zip(cols, row))
                    # Get the timestamp. Previous events have been closed using the previous timestamp.
                    id = row[config['id_column']]
                    if id in ids:
                        errors.append({'rowid': id, 'error': 'Duplicate ID: ' + str(id)})
                        continue
                    try:
                        dt = vm.get_timestamp(row, config)
                    except Exception:
                        errors.append({'rowid': row[config['id_column']], 'error': 'Invalid datetime.'})
                        continue
                    try:
                        self.get_dive(dt)
                    except:
                        errors.append({'rowid': row[config['id_column']], 'error': 'No dive available for datetime: ' + str(dt)})
                        continue
                if len(errors):
                    print(errors)
                    sys.exit(1)

            cur.execute(f"select * from [{config['data_table']}] order by {sort_columns}")
            # Get a list of column names.
            cols = [col[0] for col in cur.description]

            # On the first row of a transect, we check all the possible field sets to start events.
            first = False

            # Store the timestamp from each row. When events are committed, they are closed using the timestamp
            # from the previous row.
            dt = None

            for row in cur:
                # Turn the row into a dict.
                row = dict(zip(cols, row))

                # Get the data code actions.
                dc = vm.get_actions(row, config)
                # Get labels that apply to this row. Make labels for all available entities in the first row,
                # but filter subsequent rows using the data code.
                labels = vm.get_group_labels(row, groups, lookups, config)
                # Get the user that was responsible for annotating these records.
                mapped_user = None

                if 'transect_start' in dc or 'transect_end' in dc:
                    # When a transect starts or ends, persist the existing state objects and reset.
                    # Sometimes an end trigger isn't given.
                    for ctx in state.values():
                        if ctx['start_time'] > dt:
                            raise Exception(f'VideoMiner annotation end time must be later than start time: {ctx["start_time"]} - {dt}.')
                        ctx['end_time'] = dt
                        self.process_ctx(ctx, label_map, skip_labels, mapped_user)
                    if 'transect_start' in dc:
                        # If this is a start, set the first flag.
                        first = True
                    # Reset state.
                    state = {}

                # Iterate over each of the actions and finalize any existing objects associated with them.
                for d in dc:
                    if state.get(d):
                        ctx = state[d]
                        if ctx['start_time'] > dt:
                            raise Exception(f'End time must be later than start time: {ctx["start_time"]} - {dt}.')
                        ctx['end_time'] = dt
                        self.process_ctx(ctx, label_map, skip_labels, mapped_user)
                        del state[d]

                # Get the timestamp. Previous events have been closed using the previous timestamp.
                dt = vm.get_timestamp(row, config)

                # Get the dive and transect for the time. If no dive, exception is raised.
                try:
                    dive = self.get_dive(dt)
                except Exception as e:
                    raise Exception(f'Error in file {os.path.basename(db_file)}: ' + str(e))
                transect = self.get_transect(dive, dt)

                def process_label(dc, labels):

                    if state.get(dc):
                        # If there's a context with the given action, add the label to its list.
                        state[dc]['_labels'].extend(labels)
                        state[dc]['original_labels'].extend(labels)

                    if not state.get(dc):
                        # Get the original ID of the row for auditing purposes.
                        original_id = vm.int_or_str(row[config['id_column']])

                        # Get the medium file name.
                        medium_filename = row.get(config.get('medium_filename_column'))

                        # Create the state for the next event.
                        if self.main_config.get('iqa_file'):
                            iqa_files = [self.main_config['iqa_file']]
                        else:
                            if len(self.main_config.get('iqa_files')) > 1:
                                raise Exception(f"Can't deal with multiple iqa files in the main config. Declare one ({config_file}).")
                            iqa_files = self.main_config.get('iqa_files')

                        for iqa_file in iqa_files:
                            ctx = {
                                '_iqa_file': iqa_file,                                      # Get the IQA file if there is one, to select the correct annotation job.
                                '_labels': labels,                                          # The original label.
                                '_rows': [{k: v for k, v in row.items()}],                  # The original row is used by the ctx processor, not the current one.
                                '_value_columns': groups[dc].get('value_columns'),          # The value columns for items that need them.
                                '_attribute_columns': groups[dc].get('attribute_columns'),  # The attribute columns for items that need them.
                                'dive': dive,
                                'transect': transect,
                                'start_time': dt,
                                'end_time': None,
                                'original_labels': labels,
                                'original_id': original_id,
                                'medium_filename': medium_filename,
                                'annotation_job': self.iqas[iqa_file]['annotation_job'],
                            }

                            if dc in ('comment', 'observation'):
                                # These don't have end times. Process immediately.
                                self.process_ctx(ctx, label_map, skip_labels, mapped_user)
                            else:
                                # Save these until the values change or until the end.
                                state[dc] = ctx

                if labels:
                    # If there are labels, process them. dc_ is the action name, labels_ is the list
                    # of associated labels.
                    for dc_, labels_ in labels.items():
                        if dc_ in dc or first:
                            # If the action is within the list of actions for the current data code,
                            # or if this is the first row, process the labels.
                            #for label in labels_:
                            process_label(dc_, labels_)

                # Unset first before the next row. If the next row is a transect start, it will be set again.
                first = False

        # Finalize remaining events.
        for ctx in state.values():
            if ctx['start_time'] > dt:
                raise Exception(f'End time must be later than start time: {ctx["start_time"]} - {dt}.')
            ctx['end_time'] = dt
            self.process_ctx(ctx, label_map, skip_labels, mapped_user)


    def process_ctx(self, ctx, label_map, skip_labels, mapped_user):
        """
        Process an event context to create an event.

        :param ctx: The event context dictionary.
        :param label_map: The mapping of labels.
        :param skip_labels: A list of labels to skip.
        :param mapped_user: The user map.
        :param log: An instance of the logger class.
        """
        # Counts the number of times a handler is called. If none, the context is discarded.
        count = 0
        # Iterate over the labels in the context, extract the tags and tag data.
        for label_txt in ctx['_labels']:
            label = label_map.get(label_txt, {})
            tags = label.get('tags', [])
            properties = label.get('properties', {})

            # Check the tags. Tags just add tags to an event.
            for tag in tags:
                if tag in skip_labels:
                    continue
                handler = self.get_tag_handler(tag)
                if handler:
                    # If there's a handler, call it.
                    handler(ctx, tag, label, mapped_user, ctx['_rows'], self.log)
                    count += 1
            # Check the tag data. Tag data adds data to an event, read from the table.
            for prop_name, data in properties.items():
                handler = self.get_property_handler(prop_name)
                if handler:
                    # If there's a handler, call it.
                    handler(ctx, prop_name, data, label, mapped_user, ctx['_rows'], self.log)
                    count += 1
        if count:
            self.finalize_annotation_collection([ctx])


    def process_csv_annotations(self, config_file):
        """
        Process a list of CSV annotations.
        :param config_file: The configuration file path.
        :param log: An instance of the logger class.
        """
        from utils import csv_extract_labels as cs

        # Load the configuration.
        config = cs.load_label_config(config_file)
        # Load the label tree.
        label_tree, _ = load_config(os.path.join(os.path.dirname(config_file), config['label_tree_file']))

        # If the file isn't a list turn it into one.
        db_file = config['db_file']
        if not type(db_file) == list:
            db_file = [db_file]
        
        # Iterate over the files.
        for db_file in db_file:
            # Extract the DB file name.
            db_file = os.path.join(os.path.dirname(config_file), db_file)

            self.log.trace(self, f'Processing CSV annotations: {db_file}.')

            # Get the label map and tansform the dictionary from integer keys to string keys:
            # the VM labels don't have IDs so we have to use the labels themselves as keys.
            if len(config.get('iqa_files')) > 1:
                raise Exception("Can't deal with multipl iqa files yet.")
            label_map = {v['name']: v for v in self.get_label_map(config.get('iqa_files')[0]).values()}

            # Skip records with these labels.
            skip_labels = ('ignore', 'not_annotated')
            # Keep records with these labels and protect from trimming and filtering.
            keep_labels = ('laser_point',)

            # The location where videos/photos should be stored.
            media_path = config.get('media_path')
            if media_path:
                try:
                    os.makedirs(media_path)
                except: pass

            # Stores the context objects used to create events.
            state = {}
            # Stores the last original_id. If the ID changes the current context is processed. The IDs must be in order in the file.
            last_id = -1

            # Create a sorted list of rows.
            rows = []
            with open(db_file, 'r', encoding='utf-8-sig') as f:
                db = csv.reader(f)
                head = list(map(str.lower, next(db)))
                for row in db:
                    if not any(row):
                        continue
                    row = dict(zip(head, row))
                    rows.append(row)
                sort_column = config.get('sort_columns', [None])[0]
                if sort_column:
                    rows.sort(key=lambda a: a[sort_column])

            # Prevent repetition of labels.
            label_times = {}

            for row in rows:
                if not any(row):
                    continue

                # Get the timestamp. Previous events have been closed using the previous timestamp.
                dt = cs.get_timestamp(row, config)

                # Get the dive and transect for the time. If no dive, exception is raised.
                try:
                    dive = self.get_dive(dt)
                except Exception as e:
                    raise Exception(f'Error in file {os.path.basename(db_file)}: ' + str(e))
                transect = self.get_transect(dive, dt)

                mapped_user = None

                # Get the label columns. If it's a string, turn it into an array.
                label_columns = config['label_column']
                if not type(label_columns) == list:
                    label_columns = [label_columns]
                label_columns = list(map(str.lower, label_columns))

                # Loop over the label columns.
                for label_column in label_columns:
                    label = row[label_column]

                    # Remember the label time so we don't repeat.
                    if label_times.get(label) == dt:
                        continue
                    label_times[label] = dt

                    # Get the medium file name.
                    if config.get('medium_filename_column'):
                        medium_filename = row[config['medium_filename_column'].lower()]
                    else:
                        medium_filename = None

                    # Get the original ID of the row for auditing purposes.
                    try:
                        original_id = cs.int_or_str(row[config['id_column'].lower()])
                    except Exception as e:
                        raise Exception(f'The original ID field was not found in file {os.path.basename(db_file)}:' + str(e))

                    # A current ID that includes the source column.
                    current_id = f'{label_column}_{original_id}'

                    if last_id != current_id and state.get(last_id):
                        self.process_ctx(state[last_id], label_map, skip_labels, mapped_user)
                        del state[last_id]
                    
                    last_id = current_id

                    if state.get(current_id):
                        ctx = state[current_id]
                        ctx['_labels'].append(label)
                        ctx['_rows'].append({k: v for k, v in row.items()})
                    else:
                        # Create the state for the next event.
                        for iqa_file in self.main_config.get('iqa_files'):
                            ctx = state[current_id] = {
                                '_iqa_file': iqa_file,                         # The IQA file, if there is one, to select the correct annotation configuration.
                                '_labels': [label],                                          # The original label.
                                '_rows': [{k: v for k, v in row.items()}],                   # The original row is used by the ctx processor, not the current one.
                                #'_value_columns': groups[dc].get('value_columns'),          # The value columns for items that need them.
                                #'_attribute_columns': groups[dc].get('attribute_columns'),  # The attribute columns for items that need them.
                                'dive': dive,
                                'transect': transect,
                                'start_time': dt,
                                'end_time': None,
                                'original_labels': [label],
                                'original_id': original_id,
                                'medium_filename': medium_filename,
                                'annotation_job': self.iqas[iqa_file]['annotation_job'],
                            }

        for ctx in state.values():
            self.process_ctx(ctx, label_map, skip_labels, mapped_user)


    def create_taxon(self, obj):
        """
        Create a taxon with subsidiary IDs and restrictions.
        """
        from msea.models.shared import Taxon, HartTaxon, WoRMSTaxon, TaxonRestriction, Restriction

        def part(v):
            """
            Extracts the last part of a slash-delimited string, or returns the original string. 
            Does not handle /-terminated strings.
            """
            _, _, v2 = v.rpartition('/')
            return v2 or v
        
        # A list of fields that can be inserted into the taxonomy table.
        taxon_fields = ('hart_code', 'aphia_id', 'inaturalist_id', 'original_label', 'label', 'scientific_name', 'common_name', 'otu', 'restrictions')
        # A dict containing values from the fields list. If the keys are slash-delimited, the last part of the key is used.
        taxon_data = {part(k): v for k, v in obj.items() if part(k) in taxon_fields and v}

        # Get the list of restrictions and delete from the taxon data.
        restrictions = taxon_data.get('restrictions', [])
        try:
            del taxon_data['restrictions']
        except: pass

        # If there is taxon data, create the taxonomy object and link it to the annotation protocol 
        # and any restrictions.
        taxon = None
        if len(taxon_data) > 0:

            # Find the objects associated with these IDs.
            if taxon_data.get('hart_code'):
                taxon_data['hart'] = HartTaxon.objects.get(species_code=taxon_data['hart_code'])
                del taxon_data['hart_code']

            if taxon_data.get('aphia_id'):
                aphia_id = taxon_data['aphia_id']
                try:
                    taxon_data['aphia'] = WoRMSTaxon.objects.get(aphiaid=aphia_id)
                except:
                    self.log.warn(self, f'WoRMS taxon for Aphia ID {aphia_id} not found. Creating.')
                    taxon_data['aphia'] = WoRMSTaxon.get_from_aphiaid(aphia_id)
                del taxon_data['aphia_id']

            # Create the taxon.
            taxon = Taxon.objects.create(**taxon_data)

            # Create a link to each of the restrictions.
            for restriction in restrictions:
                restriction = Restriction.objects.get(pk=restriction['id'])
                TaxonRestriction.objects.create(taxon=taxon, restriction=restriction)

        return taxon

    def finalize_annotation_collection(self, collection):
        """
        Finalize a collection of annotations with the same ID.

        :param collection: The annotation collection.
        """

        from msea.models.rov import Event

        # A list of required event data fields.
        fields = ('dive', 'transect', 'start_time', 'end_time', 'shape', 'frame', 'original_id', 
                  'original_labels', 'attributes', 'annotation_job', 'medium_filename', 'original_id')

        for ctx in collection:

            # Produce a data dictionary from the event-related fields in the object.
            event_data = {k: v for k, v in ctx.items() if k in fields}

            if ctx.get('habitat_event') and self.run_config.get('do_habitats'):

                # Get the annotation protocol taxon.
                taxon = self.create_taxon(ctx['habitat_event'])

                # Get the tags list.
                tags = None
                if ctx['habitat_event'].get('tags'):
                    # Check that a habitat is not both dominant and subdominant, which would be a label-mapping error.
                    if 'dominant' in ctx['habitat_event']['tags'] and 'subdominant' in ctx['habitat_event']['tags']:
                        raise Exception('A habitat event must be marked either dominant or subdominant, not both.')
                    # Get the unique tags list.
                    tags = list(ctx['habitat_event']['tags'])

                data = ctx['habitat_event']
                if not len(data):
                    raise Exception('The habitat event has no data.')
                
                try:
                    HabitatEvent.objects.create(
                        substrate=data.get('habitat/substrate'),
                        substrate_coverage=data.get('habitat/coverage'),
                        biocover=data.get('habitat/biocover'),
                        biocover_coverage=data.get('habitat/coverage'),
                        complexity=data.get('habitat/complexity'),
                        thickness=data.get('habitat/thickness'),
                        disturbance=data.get('habitat/disturbance'),
                        relief=data.get('habitat/relief'),
                        flow=data.get('habitat/flow'),
                        observation_confidence=data.get('habitat/confidence'),
                        tags=tags,
                        taxon=taxon,
                        **event_data,
                    )
                except Exception as e:
                    raise Exception('Failed to create habitat event: ' + str(e))

            if ctx.get('observation_event') and self.run_config.get('do_observations'):

                # Get tha annotation protocol taxon for the observation.
                taxon = self.create_taxon(ctx['observation_event'])
                
                # Get the unique tags list.
                tags = list(ctx['observation_event']['tags']) if ctx['observation_event'].get('tags') else None

                data = ctx['observation_event']
                if not len(data):
                    raise Exception('The observation event has no data.')

                try:
                    ObservationEvent.objects.create(
                        observation_confidence=data.get('observation/confidence'),
                        abundance=data.get('observation/abundance'),
                        coverage=data.get('observation/coverage'),
                        count=data.get('observation/count'),
                        tags=tags,
                        taxon=taxon,
                        **event_data,
                    )
                except Exception as e:
                    raise Exception('Failed to create observation event: ' + str(e))

            if ctx.get('status_event') and self.run_config.get('do_statuses'):

                # Get the unique tags list.
                tags = list(set(ctx['status_event']['tags'])) if ctx['status_event'].get('tags') else None

                data = ctx['status_event']
                if not len(data):
                    raise Exception('The status event has no data.')

                try:
                    StatusEvent.objects.create(
                        status_type_detail=data['status_type_detail'],
                        tags=tags,
                        **event_data
                    )
                except Exception as e:
                    raise Exception('Failed to create status event: ' + str(e))

            if ctx.get('comment_event') and self.run_config.get('do_comments'):

                # Get the unique tags list.
                tags = list(set(ctx['comment_event']['tags'])) if ctx['comment_event'].get('tags') else None

                data = ctx['comment_event']
                if not len(data):
                    raise Exception('The comment event has no data.')

                try:
                    CommentEvent.objects.create(
                        note=data['comment'],
                        tags=tags,
                        **event_data,
                    )
                except Exception as e:
                    raise Exception('Failed to create status event: ' + str(e))

            if ctx.get('measurement_event'):
                pass

    def process_stream(self, config_file):
        """
        Process a tracking/ctd data file (can contain both).

        :param config_file: A configuration file for a data stream.
        :param log: An instance of the logger class.
        """
        from msea.models.rov import PositionType, Position, MeasurementType, \
            Measurement, InstrumentConfig, Dive

        config, _ = load_config(config_file)

        # The db files can be a string or list. If the former, wrap it.
        db_files = config['db_file']
        if not type(db_files) is list:
            db_files = [db_files]

        for db_file in db_files:
            try:
                db_file = os.path.join(os.path.dirname(config_file), db_file)
            except Exception as e:
                raise Exception('Cannot find database file in stream config: ' + str(e))

            self.log.trace(self, f'Processing data file: {db_file}.')

            # Configure the timezone offset.
            timezone_offset = 0
            if config.get('timezone_offset', None) is not None:
                timezone_offset = config['timezone_offset']

            # Cache for entity types.
            try:
                position_types = {}
                for c in config['stream_configs']:
                    if c['data_config'][0] == 'position':
                        position_types[c['data_config'][1]] = PositionType.objects.get(short_code=c['data_config'][1])
            except Exception as e:
                raise Exception(f'Failed to find position type {c["data_config"][1]}: ' + str(e))
            try:
                measurement_types = {}
                for c in config['stream_configs']:
                    if c['data_config'][0] == 'measurement':
                        measurement_types[c['data_config'][1]] = MeasurementType.objects.get(short_code=c['data_config'][1])
            except Exception as e:
                raise Exception(f'Failed to find measurement type {c["data_config"][1]}: ' + str(e))

            # Batches for entities.
            positions = []
            measurements = []
            last_times = {'position': {}, {}, 'measurement': {}}

            time_col = config['timestamp_column'].lower()
            time_format = config.get('timestamp_format')

            # Load the source filter. If given, columns with the value in the keep list are
            # kept, columns with the value in the discard list are discarded.
            filters = None
            if config.get('source_filter'):
                filters = []
                sf = config['source_filter']
                if sf.get('column_name'):
                    filters.append({
                        'column': sf['column_name'].lower(), 
                        'keep': list(map(lambda a: str(a).lower(), sf['keep'])) if sf.get('keep') else None, 
                        'discard': list(map(lambda a: str(a).lower(), sf['discard'])) if sf.get('discard') else None,
                    })
                if sf.get('columns'):
                    for col in sf['columns']:
                        filters.append({
                            'column': col['column_name'].lower(), 
                            'keep': list(map(lambda a: str(a).lower(), col['keep'])) if col.get('keep') else None, 
                            'discard': list(map(lambda a: str(a).lower(), col['discard'])) if col.get('discard') else None,
                        })

            with open(db_file, 'r', encoding='utf-8-sig') as f:
                db = csv.reader(f)
                # Get the column names as a list of lower-case strings.
                head = list(map(str.lower, next(db)))
                for row in db:
                    if not any(row):
                        # Skip empty rows.
                        continue

                    # Format the row as a dict.
                    row = dict(zip(head, row))

                    # Filter the rows.
                    if filters:
                        skip = False
                        for filter in filters:
                            v = row.get( filter['column'])
                            if v:
                                v = str(v).lower()
                                keep = filter['keep']
                                discard = filter['discard']
                                if (keep and not v in keep) or (discard and v in discard):
                                    skip = True
                                    break
                        if skip:
                            continue

                    # Extract and format the timestamp.
                    timestamp = row[time_col]
                    timestamp = parse_time(timestamp, time_format, timezone_offset)

                    # Get the epoch (time since epoch in seconds).
                    epoch = int((timestamp - datetime.utcfromtimestamp(0).replace(tzinfo=timezone.utc)).total_seconds())

                    # Get the dive for this timestamp.
                    try:
                        dive = self.dive_tree.at(to_epoch(timestamp))
                    except Exception as e:
                        self.log.debug(self, str(e))
                    if not dive or len(dive) == 0:
                        if self.run_config.get('stream_time_strict'):
                            # If a dive is required for every dive, fail.
                            raise Exception(f'Streams: dive not found in file {os.path.basename(db_file)} for timestamp {timestamp}.')
                        else:
                            # Otherwise, warn.
                            self.log.warn(self, f'Streams: dive not found in file {os.path.basename(db_file)} for timestamp {timestamp}.')
                            continue
                    dive = list(dive)[0].data

                    # Get the instrument configs for this dive.
                    instrument_configs = self.instrument_configs[dive.id]

                    for stream_config in config['stream_configs']:
                        # Format the values. Use lower case field names.
                        try:
                            values = format_values([row[k] for k in list(map(str.lower, stream_config['columns']))])
                        except Exception as e:
                            raise Exception(f'Could not find stream column: ' + str(e))

                        if values is None:
                            # The value is invalid. Skip the entity.
                            continue

                        # Get the data type instance and its short code.
                        data_type, short_code = stream_config['data_config']
                        # Get the instrument config object.
                        inst_config_name = stream_config.get('instrument_config')

                        if not inst_config_name:
                            # If the inst_config isn't available, try to load from the map.
                            # Get the config map.
                            ic_map = stream_config['instrument_config_map']
                            # Get the value of the field.
                            field_value = row[ic_map['column_name'].lower()]
                            # Set the inst_config to the mapped value.
                            inst_config_name = ic_map['value_map'][field_value]

                        # Get the instrument config instance.
                        inst_config = instrument_configs[inst_config_name]

                        # If the last instance of the datatype occurred during the same second
                        # as the current one, skip it.
                        if last_times[data_type].get(inst_config.id) == epoch:
                            continue
                        last_times[data_type][inst_config.id] = epoch

                        if data_type == 'position':

                            positions.append(Position(
                                instrument_config=inst_config,
                                position_type=position_types[short_code],
                                geom=point_to_wkb(values),
                                timestamp=timestamp,
                                signal_quality=None,
                                is_modelled=False, # TODO: Configure this.
                            ))

                        elif data_type == 'measurement':

                            measurements.append(Measurement(
                                instrument_config=inst_config,
                                measurement_type=measurement_types[short_code],
                                quantity=values[0],
                                timestamp=timestamp,
                                signal_quality=None,
                            ))

                    # Batch create entities.
                    batch = 1000
                    if len(positions) > batch:
                        self.log.trace(self, 'Saving positions.')
                        Position.objects.bulk_create(positions, batch_size=batch)
                        positions = []
                    if len(measurements) > batch:
                        self.log.trace(self, 'Saving measurements.')
                        Measurement.objects.bulk_create(measurements, batch_size=batch)
                        measurements = []

            # Batch create entities.
            Position.objects.bulk_create(positions)
            Measurement.objects.bulk_create(measurements)

    def process_comment_file(self, config_file):
        """
        Process a comment config file. Should contain a db_file, timestamp_column,
        comment_column and original_id_column.
        
        The comment files should have at least two columns, one containing the timestamp
        and one containing the comment content.
        
        :param config_file: The configuration file.
        :param log: An instance of the logger class.
        """
        from msea.models.rov import CommentEvent

        # Load the status configuration.
        config, _ = load_config(config_file)
        db_file = os.path.join(os.path.dirname(config_file), config['db_file'])

        self.log.trace(self, f'Processing comment file: {db_file}.')

        # Get column names.
        t_col = config['timestamp_column'].lower()
        c_col = config['comment_column'].lower()
        i_col = config['original_id_column'].lower()

        t_fmt = config.get('timestamp_format')
        t_off = int(config.get('timezone_offset', 0))

        with open(db_file, 'r', encoding='utf-8-sig') as f:
            db = csv.reader(f)
            head = list(map(str.lower, next(db)))

            for row in db:
                if not any(row):
                    continue
                row = dict(zip(head, row))

                # Get and check the comment field. If it's empty skip.
                comment = row[c_col].strip()
                if not comment:
                    continue

                try:
                    timestamp = parse_time(row[t_col], t_fmt, t_off)
                except Exception as e:
                    raise Exception(f'Failed to parse timestamp in file {os.path.basename(db_file)}: ' + str(e))
                
                try:
                    dive = self.get_dive(timestamp)
                except Exception as e:
                    raise Exception(f'Error in file {os.path.basename(db_file)}: ' + str(e))
                
                transect = self.get_transect(dive, timestamp)

                CommentEvent.objects.create(
                    start_time=timestamp,
                    dive=dive,
                    transect=transect,
                    note=comment,
                    original_id=row.get(i_col, 0)
                )

    def process_status_file(self, config_file):
        """
        Process a status file. The interpretation of columns is rigid:
         - on_bottom: 1 for true, 0 for false; registers an event on start and change.

        :param config_file: The status file filename.
        :param log: An instance of the logger class.
        """
        from msea.models.rov import StatusEvent, StatusTypeDetail

        # Load the status configuration.
        config, _ = load_config(config_file)
        db_file = os.path.join(os.path.dirname(config_file), config['db_file'])

        self.log.trace(self, f'Processing status file: {db_file}.')

        # Get the column names.
        id_column = config['id_column'].lower()
        timestamp_column = config['timestamp_column'].lower()
        # If there's an on_bottom column, it'll be collected, otherwise ignored.
        try:
            on_bottom_column = config['on_bottom_column'].lower()
        except:
            on_bottom_column = None

        on_bottom = None        # On bottom on this loop.
        on_bottom_last = None   # On bottom on the last loop.
        on_bottom_evt = None    # An on bottom event persistent from previous loops.
        timestamp = None        # Timestamp.
        dive_last = None        # The dive from the last loop.

        with open(db_file, 'r', encoding='utf-8-sig') as f:
            db = csv.reader(f)
            head = list(map(str.lower, next(db)))

            for row in db:
                if not any(row):
                    continue
                row = dict(zip(head, row))

                timestamp = parse_time(row[timestamp_column])
                try:
                    dive = self.get_dive(timestamp)
                except Exception as e:
                    raise Exception(f'Error in file {os.path.basename(db_file)}: ' + str(e))
                transect = self.get_transect(dive, timestamp)

                # Parse on bottom as an int. 1 is true,
                try:
                    on_bottom = int(row[on_bottom_column])
                except:
                    on_bottom = None

                # If the on bottom property or dive is different than before.
                if on_bottom_column and on_bottom_column in row.keys() and (on_bottom_last != on_bottom or dive_last != dive):
                    
                    if on_bottom_evt:
                        # If there's a previous event save it before starting the new one.
                        on_bottom_evt.end_time = timestamp
                        on_bottom_evt.save()
                        on_bottom_evt = None

                    # Create the on bottom event.
                    on_bottom_evt = StatusEvent(
                        start_time=timestamp,
                        status_type_detail=StatusTypeDetail.objects.get(short_code='on_bottom' if on_bottom == 1 else 'off_bottom'),
                        dive=dive,
                        transect=transect,
                        original_id=row[id_column]
                    )

                # Reset the last on bottom and dive to the current one.
                on_bottom_last = on_bottom
                dive_last = dive

            if on_bottom_evt:
                # Persist the leftover status event.
                on_bottom_evt.end_time = timestamp
                on_bottom_evt.save()

    def process_measurement_file(self, config_file):
        """
        Process a measurement file. These are human-made measurements that become measurement events.

        :param config_file: The configuration file.
        :param log: An instance of the logger class.
        """
        from msea.models.rov import MeasurementEvent, MeasurementType

        # Load the measurement configuration.
        config, _ = load_config(config_file)
        db_file = os.path.join(os.path.dirname(config_file), config['db_file'])

        self.log.trace(self, f'Processing measurement file: {db_file}.')

        meas_types = {} # A cache of measurement types.

        last_times = {} # Tracks the last time of a measurement of this type and rejects the record if it's the same time, same time and same quantity.

        with open(db_file, 'r', encoding='utf-8-sig') as f:
            db = csv.reader(f)
            head = list(map(str.lower, next(db)))
            for row in db:
                if not any(row):
                    continue
                row = dict(zip(head, row))

                timestamp = parse_time(row[config['timestamp_column']])
                try:
                    dive = self.get_dive(timestamp)
                except Exception as e:
                    raise Exception(f'Error in file {os.path.basename(db_file)}: ' + str(e))
                transect = self.get_transect(dive, timestamp)
                
                # Iterate over the configurations.
                for mconfig in config['configs']:

                    # The column containing the quantity to be recorded.
                    column_name = mconfig['column_name'].lower()

                    if row.get(column_name) != None:

                        # Get the record ID.
                        id_column = mconfig['id_column'].lower()

                        # Try to parse the quantity. Skip on failure.
                        try:
                            quantity = float(row[column_name])
                        except: 
                            self.log.warn(self, f'Failed to parse quantity: {row[column_name]}.')
                            continue

                        # Try to parse the the ID. Skip on failure.
                        try:
                            id = int(row[id_column])
                        except: 
                            self.log.warn(self, f'Failed to parse ID: {row[id_column]}.')
                            continue

                        # Retreive or create the measurement type.
                        meas_type_name = mconfig['measurement_type']
                        meas_type = meas_types.get(meas_type_name)
                        if not meas_type:
                            meas_type = meas_types[meas_type_name] = MeasurementType.objects.get(short_code=meas_type_name)

                        # Check that this is not a repeat of the same measurement.
                        if last_times.get(meas_type_name) and last_times[meas_type_name]['quantity'] == quantity and last_times[meas_type_name]['timestamp'] == timestamp:
                            continue
                        last_times[meas_type_name] = {'quantity': quantity, 'timestamp': timestamp}

                        MeasurementEvent.objects.create(
                            start_time=timestamp,
                            transect=transect,
                            dive=dive,
                            measurement_type=meas_type,
                            quantity=quantity,
                            original_id=id
                        )


    def run(self, config_file, status=None, run_config={}):
        """
        Run the import.
        :param config_file: The JSON file containing the import data.
        :param status:      An callable with three arguments, a message, the current step and the total 
                            number of steps. Also has a cancel() method which changes a running Boolean to false.
        :param config:      A dictionary of configurations and data sources to process. 
                            If any of 'streams', 'annotations', 'comments', 'status' are true, they are processed. 
                            If none are true, everything is processed.
                            If dry_run is true, changes are not committed.
        """

        print('Run:', config_file, status, run_config)

        self.run_config = run_config

        # Save the runtime config to a file. If save_config is set, exit immediately.
        with open(os.path.join(os.path.dirname(config_file), 'import_config.json'), 'w', encoding='utf-8-sig') as f:
            json.dump(run_config, f)
        if run_config.get('save_config'):
            print('Save config')
            return 0

        # Create status if not given.
        if not status:
            status = _status()

        # Initial steps count for config load.
        status.steps = 2

        # Load config.
        status('Loading configuration...')
        if not status.running:
            return
        config, config_dir = load_config(config_file)
        self.main_config = config
        
        # Set up the status callable with the expected number of steps.
        status.steps = 5
        do_annotations = False
        if run_config.get('do_observations') or run_config.get('do_habitats'):
            do_annotations = True
            status.steps += len(config.get('biigle_configs', [0])) + len(config.get('vm_configs', [0])) + len(config.get('csv_configs', [0]))
        if run_config.get('do_streams'):
            status.steps += len(config.get('stream_configs', [0]))
        if run_config.get('do_comments'):
            do_annotations = True
            status.steps += len(config.get('comment_configs', [0]))
        if run_config.get('do_statuses'):
            do_annotations = True
            status.steps += len(config.get('status_configs', [0]))
        if run_config.get('do_measurements'):
            status.steps += len(config.get('measurement_configs', [0]))

        # Set up a log file in the same directory as the config file.
        status('Setting up logger...')
        if not status.running:
            return
        self.log = logger(os.path.join(config_dir, 'log.txt'))

        # Initialize Django and models.
        from msea.util.django import django_setup
        django_setup(self.config_file)

        # Prepare the property-to-model mapping.
        self.build_model_map()

        # Load the tag/data handlers.
        self.load_handlers()

        try:
            # Everything happens in a transaction.
            with transaction.atomic():

                # Load IQPI.
                status('Importing principle investigator data.')
                if not status.running:
                    return
                self.load_iqpi(os.path.join(config_dir, config['iqpi_file']))

                # Load IQA.
                iqas = config.get('iqa_files', [])
                if config.get('iqa_file'):
                    iqas.append(config['iqa_file'])
                if iqas:
                    status('Importing annotator data.')
                    for iqa in iqas:
                        if not status.running:
                            return
                        self.load_iqa(os.path.join(config_dir, iqa))

                # Load NDST (cruise, dives, transects, people, equipment).
                status('Importing NDST data.')
                if not status.running:
                    return
                self.load_ndst(os.path.join(config_dir, config['ndst_file']))

                # Resolve the label lookups to model instances.
                if not status.running:
                    return
                if len(self.iqas):
                    for iqa_file in self.iqas.keys():
                        self.resolve_label_map(iqa_file)
                else:
                    self.resolve_label_map()

                # Process annotation reports.
                if do_annotations and config.get('biigle_configs'):
                    for config_file in config['biigle_configs']:
                        status('Importing Biigle annotations.')
                        if not status.running:
                            return
                        self.process_biigle_annotations(os.path.join(config_dir, config_file))

                # Process VideoMiner data.
                if do_annotations and config.get('vm_configs'):
                    for config_file in config['vm_configs']:
                        status('Importing VideoMiner annotations.')
                        if not status.running:
                            return
                        self.process_vm_annotations(os.path.join(config_dir, config_file))

                # Process CSV annotations.
                if do_annotations and config.get('csv_configs'):
                    for config_file in config['csv_configs']:
                        status('Importing CSV annotations.')
                        if not status.running:
                            return
                        self.process_csv_annotations(os.path.join(config_dir, config_file))

                # Process tracking/ctd files.
                if run_config.get('do_streams') and config.get('stream_configs'):
                    for config_file in config['stream_configs']:
                        status('Importing sensor data.')
                        if not status.running:
                            return
                        self.process_stream(os.path.join(config_dir, config_file))

                # Process comment data.
                if run_config.get('do_comments') and config.get('comment_configs'):
                    for config_file in config['comment_configs']:
                        status('Importing comments.')
                        if not status.running:
                            return
                        self.process_comment_file(os.path.join(config_dir, config_file))

                # Process status events.
                if run_config.get('do_statuses') and config.get('status_configs'):
                    for config_file in config['status_configs']:
                        status('Importing status.')
                        if not status.running:
                            return
                        self.process_status_file(os.path.join(config_dir, config_file))

                # Process measurement events.
                if run_config.get('do_measurements') and config.get('measurement_configs'):
                    for config_file in config['measurement_configs']:
                        status('Importing measurements.')
                        if not status.running:
                            return
                        self.process_measurement_file(os.path.join(config_dir, config_file))

                if run_config.get('dry_run'):
                    raise DryRun('Dry run.')
                
            # try:
            #     # Vacuum the database to hopefully make things more efficient.
            #     from django.db import connection
            #     cur = connection.cursor()
            #     conn = connection.connection
            #     il = conn.isolation_level
            #     conn.set_isolation_level(0)
            #     cur0 = conn.cursor()
            #     cur0.execute('vacuum analyze')
            #     conn.set_isolation_level(il)
            # except Exception as e:
            #     self.log.warn('Failed on vacuum: ' + str(e))

            self.log.trace(self, 'Finished.')
            status('Finished.', 1, 1)

        except DryRun as e:
            self.log.trace(self, 'Dry run -- Finished.')
            status('DryRun -- Finished.', 1, 1)
        except Exception as e:
            self.log.error(self, traceback.format_exc())
            status('Error.', 0, 0)
            raise e

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(
        prog='Import program for loading video/photo annotations.',
        description='Using a prepared import file, this program loads video and photo annotation data into the MSEA database.',
    )
    parser.add_argument('-g', '--gui', action='store_true',
        help='Open the graphical user interface.')
    parser.add_argument('config_file',
        nargs='?',
        help='The server configuration file.')
    parser.add_argument('run_config_file', 
        nargs='?',
        help='The importer configuration file for this run.')
    parser.add_argument('app_config_file', 
        nargs='?',
        help='The runtime configuration file for this run, which selects which sections to import and which to delete.')
    args = parser.parse_args()

    try:
        # Load the runtime configuration if set.
        app_config = None
        if args.run_config_file:
            app_config, _ = load_config(args.app_config_file)

        if args.gui:
            # Run in gui mode.
            print('Run gui.')
            import importer_gui
            importer_gui.run()
        else:
            # Create and run the importr.
            print('Run importer')
            imp = importer(args.config_file)
            imp.run(args.run_config_file, None, app_config)
    except Exception as e:
        print(traceback.format_exc())
        parser.print_usage()
        parser.print_help()
