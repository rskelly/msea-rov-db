import os
import pkgutil

from utils import vm_extract_labels as vm

class property_handler:
    """
    The tag data handler handles the application of record data to an event.
    Callable instances receive a context (which represents the event's data), a property name,
    data, the mapped label that generated the call, the user who created the label, the originating
    data row, and a reference to the output log.
    """
    name = '__none__'

    # List of conflicting rows.
    conflicts = {}

    @staticmethod    
    def add_conflict(row, config):
        """
        Record rows that conflict with other rows based on the annotation ID.
        """
        annotation_id = row['annotation_id']
        lst = property_handler.conflicts.get(annotation_id)
        if lst == None:
            lst = property_handler.conflicts[annotation_id] = []
        timestamp = vm.get_timestamp(row, config)
        lst.append([timestamp, annotation_id] + row)

    def matches(self, name):
        """
        Return true if the given string is a match for this handler.
        """
        if type(self.name) == str:
            return name.lower().startswith(self.name)
        else:
            for n in self.name:
                if name.lower().startswith(n):
                    return True
            return False

    def __call__(self, ctx, prop_name, data, mapped_label, mapped_user, rows, log, config):
        """
        Configure data on the context object according to the property name and data.
        :param ctx: The context object representing the event.
        :param prop_name: The name of the property, a '/'-delimited string.
        :param data: A value to assign according to the prop_name.
        :param mapped_label: The label that generated this call.
        :param mapped_user: The user that created the label.
        :param rows: The original data row.
        :param log: A reference to the output log.
        """
        raise Exception('Not implemented')

    @staticmethod
    def get_handlers():
        """
        Return a list of handlers in this package, including this one.
        """
        path = os.path.join(os.path.dirname(__file__))
        return [modname for importer, modname, ispkg in pkgutil.walk_packages(path=(path,))]
