import property_handlers

class property_handler(property_handlers.property_handler.property_handler):
    """
    Record habitat events, etc.
    """
    name = 'habitat'

    def __init__(self):
        super()

    def __call__(self, ctx, prop_name, data, mapped_label, mapped_user, rows, log, config):

        if not data:
            # Don't bother trying to set empty data.
            return
        
        evt_data = ctx.get('habitat_event')
        if not evt_data:
            evt_data = ctx['habitat_event'] = {}

        if rows[0].get('label_id') and rows[0]['label_id'] > ctx['_rows'][0]['label_id']:
            # This might happen if more than one label is applied to an annotation in Biigle (but shouldn't if QC is good).
            log.warn(self, f"The annotation {rows[0]['annotation_id']} has conflicting labels. The one with the later label ID has been used.")
            self.conflict += 1
            log.warn(self, f'label_conflict:{self.conflict};label_id:{rows[0].get("label_id")};annotation_id:{rows[0].get("label_id")}') #;row:{ctx["_rows"][0].join(",")}')
            log.warn(self, f'label_conflict:{self.conflict};label_id:{rows[0].get("label_id")};annotation_id:{rows[0].get("label_id")}') #;row:{rows[0].join(",")}')
            property_handlers.property_handler.property_handler.add_conflict(rows[0], config)

        if evt_data.get(prop_name) and evt_data[prop_name] != data:
            # This may happen if the label map is updated so that it has a new path for the same value.
            # Could also happen when two labels are applied to one event, one containing an invalid value and one containing a valid value.
            log.warn(self, f"The value for {prop_name} was reset to a different value.")
            self.conflict += 1
            log.warn(self, f'prop_conflict:{self.conflict};annotation_id:{rows[0].get("annotation_id")};prop_name:{prop_name};value:{evt_data[prop_name]}')
            log.warn(self, f'prop_conflict:{self.conflict};annotation_id:{rows[0].get("annotation_id")};prop_name:{prop_name};value:{data}')
            property_handlers.property_handler.property_handler.add_conflict(rows[0], config)

        evt_data[prop_name] = data


