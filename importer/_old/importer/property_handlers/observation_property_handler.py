import property_handlers

class property_handler(property_handlers.property_handler.property_handler):
    """
    Record observation events with species, etc.
    """
    name = 'observation'

    def __init__(self):
        super()
        self.conflict = 0

    def __call__(self, ctx, prop_name, data, mapped_label, mapped_user, rows, log, config):

        if not data:
            return
        
        evt_data = ctx.get('observation_event')
        if not evt_data:
            # If the context doesn't have an obs evt dictionary add one.
            evt_data = ctx['observation_event'] = {}

        if rows[0].get('label_id') and rows[0]['label_id'] > ctx['_rows'][0]['label_id']:
            # If the property is set but the label has a higher ID, set it. Because the top-level label in a hierarchy is necessarily
            # created first, it has a lower ID and more specific labels will have higher IDs. This does not protect against labels
            # from a different tree, which would be an annotation error.
            # This is specific to Biigle which may have multiple species labels (but shouldn't if QC is done).
            log.warn(self, f"The annotation {rows[0]['annotation_id']} has conflicting labels. The one with the later label ID has been used.")
            log.debug(self, f'new label: {self.conflict}; label_id: {rows[0].get("label_id")}; annotation_id: {rows[0].get("annotation_id")}') 
            log.debug(self, f'old label: {self.conflict}; label_id: {ctx["_rows"][0].get("label_id")}; annotation_id: {ctx["_rows"][0].get("annotation_id")}') 
            self.conflict += 1
            property_handlers.property_handler.property_handler.add_conflict(rows[0], config)
            
        if evt_data.get(prop_name) and evt_data[prop_name] != data:
            if rows[0]['label_id'] > ctx['_rows'][0]['label_id']:
                # This may happen if the label map is updated so that it has a new path for the same value.
                # See the note above about label IDs.
                log.warn(self, f"The value for {prop_name} was reset to a different value. The one with the later label ID has been used. Annotation ID: {rows[0].get('annotation_id')}; Label IDs: {rows[0]['label_id']}, {ctx['_rows'][0]['label_id']}")
                evt_data[prop_name] = data
                self.conflict += 1
            else:
                log.warn(self, f"The value for {prop_name} was not reset because its label ID was lower than the previously-set one.")
                log.debug(self, f'new prop: {self.conflict}; annotation_id: {rows[0].get("annotation_id")}; prop_name:{prop_name}; value: {data}')
                log.debug(self, f'old prop: {self.conflict}; annotation_id: {ctx["_rows"][0].get("annotation_id")}; prop_name:{prop_name}; value: {evt_data[prop_name]}')
                self.conflict += 1
            property_handlers.property_handler.property_handler.add_conflict(rows[0], config)

        evt_data[prop_name] = data

        if not ctx.get('_attr_set'):
            # This need only happen once.
            ctx['_attr_set'] = True

            # First, set the values in the value columns.
            value_cols = ctx.get('_value_columns')
            if value_cols:
                for k, v in value_cols.items():
                    if rows[0].get(v):
                        evt_data[f'observation/{k}'] = rows[0][v]

            # Then, set the attributes.
            if not 'attributes' in ctx.keys():
                ctx['attributes'] = {}

            attr_cols = ctx.get('_attribute_columns')
            if attr_cols:
                for k, v in attr_cols.items():
                    if rows[0].get(v):
                        ctx['attributes'][k] = rows[0][v]

