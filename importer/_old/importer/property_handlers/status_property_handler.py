import property_handlers

from msea.models.rov import StatusTypeDetail

class property_handler(property_handlers.property_handler.property_handler):
    """
    Record status events.
    """
    name = 'status'

    def __call__(self, ctx, prop_name, data, mapped_label, mapped_user, rows, log, config):

        if not data:
            return
        
        if not ctx.get('status_event'):
            ctx['status_event'] = {}

        try:
            if prop_name == 'status/detail':
                ctx['status_event']['status_type_detail'] = data
        except Exception as e:
            raise Exception(f'Status detail for {data} not found.')

