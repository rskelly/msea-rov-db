#!/usr/bin/end python

"""
This is the GUI version of the MSEA sumersible database import program.
"""

from tkinter import *
from tkinter.ttk import *
from tkinter.filedialog import askopenfilename
from tkinter import messagebox
from threading import Thread

class app:

    def __init__(self, root):
        super().__init__()
        self.commit = BooleanVar(root, value=False)         # If true, commit the modifications to the DB. Otherwise, is a dry run and no changes are made.
        self.force = BooleanVar(root, value=False)          # If true, force the creation of entities by deleting the old ones. For related entities, this
                                                            # implies a cascading delete!

        self.server_config = StringVar(root, value="")        # The configuration file. Must contain properties for the remote version, for remote servers.

        self.download = BooleanVar(root, value=False)       # If true, media files will not be downloaded for metadata extraction.

        self.recreate_cruises = BooleanVar(root, value=False)       # If true, causes entities to be dropped before (re)creation.
        self.recreate_dives = BooleanVar(root, value=False)         # If true, causes entities to be dropped before (re)creation.
        self.recreate_transects = BooleanVar(root, value=False)     # If true, causes entities to be dropped before (re)creation.
        self.recreate_measurements = BooleanVar(root, value=False)   # Drop and recreate measurement events.
        self.recreate_comments = BooleanVar(root, value=False)       # Drop and recreate comment events.
        self.recreate_statuses = BooleanVar(root, value=False)        # Drop and recreate status events.
        self.recreate_habitats = BooleanVar(root, value=False)       # Drop and recreate habitat events.
        self.recreate_observations = BooleanVar(root, value=False)   # Drop and recreate habitat events.
        self.recreate_streams = BooleanVar(root, value=False)        # Drop and recreate streams.

        self.dry_run = BooleanVar(root, value=True)         # If false, causes the import to update the database. Otherwise, a dry run is executed and no changes made.

        self.config_file = StringVar(root, value='')        # If the file is a txt file, it is assumed to be a list of folders to import, relative to the file 
                                                            # itself. If it is a CSV, it is considered to be an import file. Other files are rejected.''')

        # If none of these are selected, all data are processed.
        self.do_streams = BooleanVar(root, value=False)         # If true, the data streams (CTD, nav, etc.) are processed.
        self.do_observations = BooleanVar(root, value=False)    # If true, the observations are processed.
        self.do_habitats = BooleanVar(root, value=False)        # If true, the habitats are processed.
        self.do_comments = BooleanVar(root, value=False)        # If true, the comments are processed (excludes those in annotations).
        self.do_status = BooleanVar(root, value=False)          # If true, the status events are processed (excludes those in annotations).
        self.do_measurements = BooleanVar(root, value=False)    # If true, the measurement events are processed (excludes those in annotations).

        self.stream_time_strict = BooleanVar(root, value=True)  # If true, all stream values must be within the time span of a dive. Otherwise, they'll be discarded.
        self.save_config = BooleanVar(root, value=False)        # If true, save the runtime configuration and exit.

        self.status = IntVar(root, value=0)
        self.status_message = StringVar(root, value='')
        self.status_object = None

        self.all_checks = [self.recreate_cruises, self.recreate_dives, self.recreate_transects, self.recreate_measurements, self.recreate_comments,
                           self.recreate_statuses, self.recreate_habitats, self.recreate_observations,
                           self.do_streams, self.do_observations, self.do_habitats, self.do_comments, self.do_status, self.do_measurements]
    
    def set_all(self):
        """
        Set all of the check boxes in all_checks to true.
        """
        for chk in self.all_checks:
            chk.set(True)

    def select_input_file(self):
        """
        Callback function for selecting the filename.
        """
        self.config_file.set(askopenfilename())

    def select_server_config(self):
        """
        Callback function for selecting the filename.
        """
        self.server_config.set(askopenfilename())

    def status_callback(self, message, step, steps):
        """
        A callback which receives a message, the current step and the number of steps from 
        a running importer.
        """
        if steps < step:
            steps = step
        if steps == 0:
            step = steps = 1
        self.status.set(int(float(step * 100) / steps))
        self.status_message.set(message)

    def cancel(self):
        """
        Cancel the running program.
        """
        if self.status_object:
            self.status_object.cancel()
            
    def run(self):
        """
        Run the program.
        """
        import importer

        class _status:
            """
            A callable which will either update the status callback or print the status.
            """
            def __init__(self, status_callback):
                self.steps = 0
                self.step = 1
                self.running = True
                self.status_callback = status_callback

            def __call__(self, message, step=-1, steps=-1):
                if step == -1:
                    step = self.step
                if steps == -1:
                    steps = self.steps
                self.status_callback(message, step, steps)
                self.step += 1
                
            def cancel(self):
                self.running = False
                self.status_callback('Cancelled', 0, 1)

        self.status_object = _status(self.status_callback)

        config = {
            'do_streams': self.do_streams.get(),
            'do_observations': self.do_observations.get(),
            'do_habitats': self.do_habitats.get(),
            'do_comments': self.do_comments.get(),
            'do_statuses': self.do_status.get(),
            'do_measurements': self.do_measurements.get(),
            'recreate_cruises': self.recreate_cruises.get(),
            'recreate_dives': self.recreate_dives.get() or self.recreate_cruises.get(),
            'recreate_transects': self.recreate_transects.get() or self.recreate_dives.get() or self.recreate_cruises.get(),
            'recreate_measurements': self.recreate_measurements.get() or self.recreate_dives.get() or self.recreate_cruises.get(),
            'recreate_statuses': self.recreate_statuses.get() or self.recreate_dives.get() or self.recreate_cruises.get(),
            'recreate_comments': self.recreate_comments.get() or self.recreate_dives.get() or self.recreate_cruises.get(),
            'recreate_habitats': self.recreate_habitats.get() or self.recreate_dives.get() or self.recreate_cruises.get(),
            'recreate_observations': self.recreate_observations.get() or self.recreate_dives.get() or self.recreate_cruises.get(),
            'recreate_streams': self.recreate_streams.get() or self.recreate_dives.get() or self.recreate_cruises.get(),
            'stream_time_strict': self.stream_time_strict.get(),
            'save_config': self.save_config.get(),
            'dry_run': self.dry_run.get(),
        }

        try:
            server_config = self.server_config.get()
            config_file = self.config_file.get()
            imp = importer.importer(server_config)
            imp.run(config_file, self.status_object, config)
            messagebox.showinfo("Done!")
        except Exception as e:
            messagebox.showerror("Importer", str(e))

def run():
        
    root = Tk()
    data = app(root)
    frm = Frame(root)
    frm.grid()

    def open_vm_label():
        w = Toplevel(root)
        w.wm_title('Convert a VideoMiner Database')

    def open_csv_label():
        w = Toplevel(root)
        w.wm_title('Convert a CSV File')

    row = 0

    Label(frm, text='Configuration File').grid(column=0, row=row, sticky="ew")
    Entry(frm, textvariable=data.config_file).grid(column=1, row=row, sticky="w")
    Button(frm, text="Select File...", command=data.select_input_file).grid(column=2, row=row, sticky="w")
    row += 1

    Label(frm, text='Destination').grid(column=0, row=row, sticky="ew")
    Entry(frm, textvariable=data.server_config).grid(column=1, row=row, sticky="w")
    Button(frm, text="Select File...", command=data.select_server_config).grid(column=2, row=row, sticky="w")
    row += 1

    Label(frm, text='Download Media Files').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.download).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Cruises').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_cruises).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Dives').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_dives).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Transects').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_transects).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Do Observations').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.do_observations).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Observation Events').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_observations).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Do Habitat').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.do_habitats).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Habitat Events').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_habitats).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Do Comments').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.do_comments).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Comment Events').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_comments).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Do Status').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.do_status).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Status Events').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_statuses).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Do Measurements').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.do_measurements).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Measurement Events').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_measurements).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Do Streams').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.do_streams).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Recreate Streams').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.recreate_streams).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Require Dive for Stream').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.stream_time_strict).grid(column=1, row=row, sticky="w")
    row += 1

    Button(frm, text='Set All', command=data.set_all).grid(column=1, row=row, sticky="ew")
    row += 1

    Label(frm, text='Dry Run').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.dry_run).grid(column=1, row=row, sticky="w")
    row += 1

    Label(frm, text='Save Config and Exit').grid(column=0, row=row, sticky="ew")
    Checkbutton(frm, variable=data.save_config).grid(column=1, row=row, sticky="w")
    row += 1

    pb = Progressbar(frm, orient=HORIZONTAL, length=100, mode='indeterminate', maximum=100, variable=data.status)
    pb.grid(column=0, row=row, columnspan=3, sticky='ew')
    row += 1

    grp = Frame(root)
    Button(grp, text="VideoMiner Label Tree", command=open_vm_label).grid(column=0, row=row, sticky="w")
    Button(grp, text="CSV Label Tree", command=open_csv_label).grid(column=0, row=row, sticky="w")
    grp.grid(column=0, row=row, stick="e")
    row +=1 

    grp = Frame(root)
    Button(grp, text="Run", command=data.run).grid(column=0, row=row, sticky="w")
    Button(grp, text="Cancel", command=data.cancel).grid(column=1, row=row, sticky="w")
    Button(grp, text="Quit", command=root.destroy).grid(column=2, row=row, sticky="w")
    grp.grid(column=0, row=row, stick="e")

    root.mainloop()

if __name__ == '__main__':
    run()
