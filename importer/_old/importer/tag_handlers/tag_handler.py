import os
import pkgutil

class tag_handler:
    """
    This callable object applies tags to a context object which represents an event.
    """

    name = '__none__'

    def matches(self, name):
        """
        Return true if the given string is a match for this handler.
        """
        if type(self.name) == str:
            return name.lower().startswith(self.name)
        else:
            for n in self.name:
                if name.lower().startswith(n):
                    return True
            return False
        
    def __call__(self, ctx, tag, mapped_label, mapped_user, rows, log):
        """
        Applies tags to the context object which represents an event.
        :param ctx: The context object.
        :param tag: The tag to apply.
        :param mapped_label: The label that originated this call.
        :param mapped_user: The user that generated the label.
        :param rows: The original row data.
        :param log: A reference to the log.
        """
        raise Exception('Not implemented')

    @staticmethod
    def get_handlers():
        """
        Return a list of tag handlers in this package, including this one.
        """
        path = os.path.join(os.path.dirname(__file__))
        return [modname for importer, modname, ispkg in pkgutil.walk_packages(path=(path,))]
