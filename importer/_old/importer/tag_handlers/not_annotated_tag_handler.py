import tag_handlers

class tag_handler(tag_handlers.tag_handler.tag_handler):
    """
    Handle not annotated annotations.
    """
    name = 'status/not_annotated'

    def __call__(self, ctx, tag, mapped_label, mapped_user, rows, log):
        ctx['not_annotated'] = True


