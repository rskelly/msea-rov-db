import tag_handlers
from msea.models.rov import StatusTypeDetail

class tag_handler(tag_handlers.tag_handler.tag_handler):
    """
    Handle laser point annotations.
    """
    name = 'status/laser_point'

    def __init__(self):
        self.detail = StatusTypeDetail.objects.get(short_code='laser_point')

    def __call__(self, ctx, tag, mapped_label, mapped_user, rows, log):
        if not ctx.get('status_event'):
            ctx['status_event'] = {}
        ctx['status_event']['status_type_detail'] = self.detail


