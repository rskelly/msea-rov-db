import tag_handlers

class tag_handler(tag_handlers.tag_handler.tag_handler):
    """
    Handle comments.
    """
    name = 'comment'

    def __call__(self, ctx, tag, mapped_label, mapped_user, rows, log):
        if not ctx.get('comment_event'):
            ctx['comment_event'] = {}
        comment = rows[0][ctx['_value_columns']['comment']]
        if comment is not None and comment.strip():
            ctx['comment_event']['comment'] = comment.strip()



