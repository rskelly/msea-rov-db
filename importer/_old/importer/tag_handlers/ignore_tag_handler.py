import tag_handlers

class tag_handler(tag_handlers.tag_handler.tag_handler):
    """
    Handle ignore annotations.
    """
    name = 'ignore'

    def __call__(self, ctx, tag, mapped_label, mapped_user, rows, log):
        ctx['ignore'] = True


