import tag_handlers
from msea.models.rov import StatusTypeDetail

class tag_handler(tag_handlers.tag_handler.tag_handler):
    """
    Handle on/off bottom events.
    """
    name = ('status/on_bottom', 'status/off_bottom')

    def __call__(self, ctx, tag, mapped_label, mapped_user, rows, log):
        if not ctx.get('status_event'):
            ctx['status_event'] = {}
        short_code = 'on_bottom' if int(rows[0][ctx['_value_columns']['on_bottom']]) == 1 else 'off_bottom'
        std = StatusTypeDetail.objects.get(short_code=short_code)
        ctx['status_event']['status_type_detail'] = std



