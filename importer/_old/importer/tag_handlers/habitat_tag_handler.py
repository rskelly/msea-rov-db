from intervaltree import IntervalTree, Interval
import tag_handlers

def part(v):
    """
    Return the last part of a slash-delimited string, or the string itself.
    """
    _, _, v2 = v.rpartition('/')
    return v2 or v

class tag_handler(tag_handlers.tag_handler.tag_handler):
    """
    Handle the labels on habitat annotations.
    """

    name = ('habitat/substrate', 'habitat/biocover')

    # These values come from the tags.js file.
    tags = (
        'dominant', 'subdominant', 
        'alive', 'dead', 'toppled', 'attached', 'detached', 'habitat_forming',
        'large', 'medium', 'small', 'rubble', 'clumps', 'blocks', 'reef',
        'silt', 'slope', 'wall', 'cliff', 'group', 'mat', 'patches', 'solitary', 'school'
    )

    tag_tree = IntervalTree()

    @staticmethod
    def add_general_tags(tags, start_time, end_time):
        """
        Add the general tags to the interval tree if they're in the tags list.
        """
        for tag in tags:
            try:
                # TODO: For the old version which had propName.
                prop = tag['propName']
                label = tag['label']
            except:
                prop = label = tag
            name = part(prop)
            if name in tag_handler.tags:
                tag_handler.tag_tree.append(Interval(start_time, end_time, label))

    def __call__(self, ctx, tag, mapped_label, mapped_user, rows, log):
        
        if not ctx.get('habitat_event'):
            ctx['habitat_event'] = {}
        
        _tags = ctx['habitat_event'].get('tags')
        if not _tags:
            _tags = ctx['habitat_event']['tags'] = set()

        # Find the tags in the tag tree.
        try:
            if ctx['end_time']:
                res = tag_handler.tag_tree.overlap(ctx['start_time'], ctx['end_time'])
            else:
                res = tag_handler.tag_tree.at(ctx['start_time'])
            for r in res:
                _tags.add(r.data)
        except Exception as e:
             print(e)

        for _tag in mapped_label['tags']:
            try:
                # TODO: For the old version which had propName.
                prop = _tag['propName']
                label = _tag['label']
            except:
                prop = label = _tag
            if part(prop) in tag_handler.tags:
                    _tags.add(label)


