#!/usr/bin/env python

'''
Parse and compile the txt files in this dir to make one spreadsheet.
'''

import os
import sys
import csv
from datetime import datetime, timezone

dirname = sys.argv[1]

def parse_dt(dt):
	date, time = dt.split()
	date = datetime.strptime(date, '%m-%d-%y').replace(tzinfo = timezone.utc)
	time, s = time.split('.')
	time = datetime.strptime(time, '%H:%M:%S').replace(tzinfo = timezone.utc)
	return date.strftime('%m/%d/%Y'), time.strftime('%H:%M:%S')

def parse_latlon(lat, lon):
	a, b = lat[1:].split()
	lat = float(a) + float(b) / 60.
	a, b = lon[1:].split()
	lon = -float(a) - float(b) / 60.
	return lat, lon

print('diveid,id,date,time,lat,lon,depth')

for f in [x for x in os.listdir(dirname) if x.endswith('.txt')]:
	
	# The dive ID from filename.
	diveid = f[1:f.find('_')]

	with open(os.path.join(dirname, f), 'r') as fd:
		db = csv.reader(fd)
		for row in db:
			id, dt, lat, lon, depth, utmy, utmx, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o = row
			date, time = parse_dt(dt)
			lat, lon = parse_latlon(lat, lon)
			depth = -float(depth)
			print('{},{},{},{},{},{},{}'.format(diveid, id, date, time, lat, lon, depth))

