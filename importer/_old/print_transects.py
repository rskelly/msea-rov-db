#!/usr/bin/env python

'''
Extracts the date, time and transect fields and prints the start and end times of each distinct transect.
'''

import csv
import sys
from datetime import datetime, timezone

infile = sys.argv[1]

data = {}

with open(infile, 'r') as f:
	db = csv.reader(f)
	head = next(db)
	while not 'date' in head or not 'time' in head:
		head = next(db)
	for row in db:
		row = dict(zip(head, row))
		date = row['date']
		time, _, _ = row['time'].rpartition('.')
		dt = datetime.strptime(date + ' ' + time, '%d/%m/%Y %H:%M:%S').replace(tzinfo = timezone.utc)
		tr = row['transectid']
		if not data.get(tr):
			data[tr] = {'start_time': None, 'end_time': None}
		if not data[tr]['start_time'] or dt < data[tr]['start_time']:
			data[tr]['start_time'] = dt
		if not data[tr]['end_time'] or dt > data[tr]['end_time']:
			data[tr]['end_time'] = dt

for k, v in data.items():
	print(','.join((k, v['start_time'].strftime('%d/%m/%Y,%H:%M:%S'), v['end_time'].strftime('%H:%M:%S'))))