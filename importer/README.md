# ROV Database Imports

This folder contains the script to import ROV data into the database. 

Data are provided by various groups in a specified way so that they can be efficiently merged and loaded by the script.
A wiki article on data preparation for import is [here](https://msea.science/w/DataProviders).

The entry point script is `importer/import.py`, and requires a configuration file for the target database, by convention 
found in `~/configs/db_[target].conf`.

The folder structure is as follows:

* `importer.py` - The import script entry-point.
* `reader.py` - Reads the CSV files that comprise a data import. Invoked by `import.py`, not directly.
* `utils.py` - Utilities used by `import.py_.
* `handlers` - A folder containing "handlers" which are responsible for loading individual import sections. Each
  file is named according to the section(s) that it imports.
  * `handler.py` - The base handler file, from which all others derive.
  * `[name].py` - The handler files.



