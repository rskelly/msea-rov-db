import os
import json
import pyodbc
from referencing import Registry, Resource
import jsonschema
import re
from datetime import datetime
import csv

class CSVValidationException(Exception):
    pass

class csv_str:

    def __init__(self, null=True, min_length=0, max_length=999999, pattern=None):
        self.null = True
        self.min_length = min_length
        self.max_length = max_length
        self.pattern = pattern
        if pattern:
            self.re = re.compile(pattern)

    def validate(self, value):
        if not self.null and value == None:
            raise CSVValidationException('String must not be null.')
        value = str(value)
        if len(value) < self.min_length:
            raise CSVValidationException('String is too short:', len(value), '<', self.min_length)
        if len(value) > self.max_length:
            raise CSVValidationException('String is too long:', len(value), '>', self.max_length)
        if self.pattern:
            if not self.re.match(value):
                raise CSVValidationException(f'String does not match the given pattern:', self.pattern)
            
class csv_int:

    def __init__(self, null=False, min_value=-999999, max_value=999999):
        self.null = null
        if min_value > max_value:
            tmp = min_value
            min_value = max_value
            max_value = tmp
        self.min_value = min_value
        self.max_value = max_value

    def validate(self, value):
        if not self.null and value == None:
            raise CSVValidationException('Integer must not be null.')
        value = int(value)
        if value < self.min_value:
            raise CSVValidationException('Integer is too small:', value, '<', self.min_value)
        if value > self.max_value:
            raise CSVValidationException('Integer is too large:', value, '>', self.max_value)
        
class csv_float:

    def __init__(self, null=False, min_value=-99999999., max_value=99999999.):
        self.null = null
        if min_value > max_value:
            tmp = min_value
            min_value = max_value
            max_value = tmp
        self.min_value = min_value
        self.max_value = max_value

    def validate(self, value):
        if not self.null and value == None:
            raise CSVValidationException('Float must not be null.')
        value = float(value)
        if value < self.min_value:
            raise CSVValidationException('Float is too small:', value, '<', self.min_value)
        if value > self.max_value:
            raise CSVValidationException('Float is too large:', value, '>', self.max_value)
        
class csv_datetime:

    def __init__(self, null=False, pattern='%Y-%m-%dT%H:%M:%SZ'):
        self.null = null
        self.pattern = pattern

    def validate(self, value):
        if not self.null and value == None:
            raise CSVValidationException('Datetime must not be null.')
        value = str(value)
        try:
            datetime.strptime(value, self.pattern)
        except:
            raise CSVValidationException('Failed to parse datetime using pattern,', self.pattern)
        
class csv_validator:

    def __init__(self):
        self.registry = {}

    def register_schema(self, schema):
        """
        Register a schema, which is a dictionary containing field names and their validators.
        The schema must have an '$id' property which identifies the schema for lookups.
        {
            '$id': 'coordinate',
            'x': csv_float(False, -135., -125.),
            'y': csv_float(False, -48., -53.),
            'timestamp': csv_datetime(False, '%Y-%m-%dT%H:%M:%SZ')
        }
        """
        self.registry[schema['name']] = schema

    def validate(self, schema_id, document):
        """
        Validate the given document. If it is a string, it is assumed to be the filename of a CSV file. 
        if it is a list, it is assumed to be a loaded CSV file with lists for rows, and the first
        row as column headings. Any other type will raise an exception. Columns in the document will
        be checked against the schema identified by schema_id, which should already have been loaded.
        :param  schema_id: The ID of a schema.
        :param document: The CSV document or a filename pointing to one.
        """
        if type(document) == str:
            db = csv.reader(open(document, 'r'))
        elif type(document) == list:
            db = csv.reader(document)
        else:
            raise Exception('The given document is not of the correct type.')
        
        schema = self.registry[schema_id]
        head = next(db)
        
        valid_cols = []
        for k, v in schema.items():
            if k != '$id':
                try: 
                    head.index(k)
                    valid_cols.append(k)
                except:
                    raise CSVValidationException(f'The required column {k} is missing from the document.')

        for row in db:
            for col in valid_cols:
                schema[col].validate(row[head.index(col)])

class biigle_report_validator(csv_validator):
    """
    Validates a Biigle annotation report using the standard fields.
    """
    def __init__(self):
        super().__init__()
        self.register_schema({
            'video_annotation_label_id': csv_int(False, 1, 99999999),
            'label_id': csv_int(False, 1, 99999999),
            'label_name': csv_str(False),
            'label_hierarchy': csv_str(False),
            'user_id': csv_int(False, 1, 99999999),
            'firstname': csv_str(False),
            'lastname': csv_str(False),
            'video_id': csv_int(False, 1, 99999999),
            'video_filename': csv_str(False),
            'shape_id': csv_int(False, 1, 99999999),
            'shape_name': csv_str(False),
            'points': csv_str(False),
            'frames': csv_str(False),
            'annotation_id': csv_int(False, 1, 99999999),
            'created_at': csv_datetime(False),
            # 'attributes'
        })

class json_validator:

    def __init__(self):
        self.registry = Registry()

    def register_schema(self, schema):
        """
        Register a schema with the validator. If the schema is a string, it is assumed to be a filename,
        which points to a JSONSchema document which contains an $id property with its identifier. This 
        is used to identify the schema in the registry. If the schema is a dict, it is assumed to be a 
        pre-loaded schema. Other types will raise an error. If the object is invalid, it will also raise
        an error.
        
        https://tour.json-schema.org/content/06-Combining-Subschemas/02-id-and-schema

        :param filename: The filename of a JSONSchema document.
        """
        if type(schema) == str:
            with open(schema, 'r') as f:
                schema = json.load(f)
        if type(schema) == dict:
            resource = Resource.from_contents(schema)
            self.registry = self.registry.combine(Registry().with_resource(uri=schema['$id'], resource=resource))
        else:
            raise Exception('The schema parameter is of the wrong type:', str(type(schema)))

    def validate(self, schema_id, document):
        """
        Validate the JSON document against the schema identified by schema_id. The schemas should 
        have already been loaded using the register_schema method. If the document is a string, it 
        is assumed to be a filename pointing to a JSON document. If it is a Python object, it is
        assumed to be a loaded document and will be validated as-is.

        Because a string is assumed to be a filename, a document comprising a single string is
        not an acceptable document.
        
        Will throw exceptions on any validation error. 

        :param schema_id: The schema ID.
        :param document: The document or a filename pointing to a JSON document.
        """
        if type(document) == str:
            with open(document, 'r') as f:
                document = json.load(f)
        jsonschema.validate(document, self.registry.contents(schema_id), registry=self.registry)


def decode_sketchy_utf16(raw_bytes):
    s = raw_bytes.decode("utf-16le", "ignore")
    try:
        n = s.index('\u0000')
        s = s[:n]  # respect null terminator
    except ValueError:
        pass
    return s

class access:

    def __init__(self, path):
        """
        Initialize the instance and open a connection to the database.
        """
        self.conn = pyodbc.connect(rf'DRIVER={{Microsoft Access Driver (*.mdb, *.accdb)}};DBQ={path};')
        self.conn.add_output_converter(pyodbc.SQL_WVARCHAR, decode_sketchy_utf16)

    def get_tables(self):
        """
        Get a list of the tables.
        """
        cur = self.conn.cursor()
        return [r.table_name for r in cur.tables()]

    def executemany(self, query, params=None):
        """
        Pass the query and return a generator of rows consisting of dictionaries.
        """
        tmp = self.conn.autocommit
        self.conn.autocommit = False
        cur = self.conn.cursor()
        #cur.fast_executemany = True
        cur.executemany(query, params)
        self.conn.commit()
        self.conn.autocommit = tmp

    def columns(self, table):
        cur = self.conn.cursor()
        result = []
        for row in cur.columns(table=table):
            result.append((row.column_name, row.type_name))
        return result

    def execute(self, query, params=None):
        """
        Pass the query and return a generator of rows consisting of dictionaries.
        """
        cur = self.conn.cursor()
        if params:
            cur.execute(query, params)
        else:
            cur.execute(query)

    def query_dict(self, query, params=None):
        """
        Pass the query and return a generator of rows consisting of dictionaries.
        """
        cur = self.conn.cursor()
        if params:
            cur.execute(query, params)
        else:
            cur.execute(query)
        cols = [x[0] for x in cur.description]
        for row in cur.fetchall():
            yield dict(zip(cols, row)), cols

    def query_list(self, query, params=None):
        """
        Pass the query and return a generator of rows consisting of tuples with the column value and column name lists.
        """
        cur = self.conn.cursor()
        if params:
            cur.execute(query, params)
        else:
            cur.execute(query)
        cols = [x[0] for x in cur.description]
        for row in cur.fetchall():
            yield row, cols

    def __del__(self):
        try:
            self.conn.close()
        except: pass