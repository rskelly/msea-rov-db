#!/usr/bin/env python

"""
Extract the videominer database at c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/Jon's Bowie Video Database.mdb
to a csv file with the references correctly mapped to live database entities.
"""
import os
import sys
import csv
import json
import requests
from datetime import timedelta, datetime
import sqlite3

sys.path.append('./import/scripts')

import util

user = 'robert.skelly@dfo-mpo.gc.ca'
pw = 'Lophelia1'
res = requests.post('https://msea.science/rov/api/v1/account/login/', {'email': user, 'password': pw})
result = res.json()
token = result['result']['token']
time_format = '%Y-%m-%dT%H:%M:%SZ'

min_date = datetime(2000, 1, 1)
max_date = datetime(2001, 1, 1)

id_column = 'id'
medium_filename_column = 'photo_name'
comment_column = 'Comment'
protocol_column = 'protocol_id'
image_quality_column = 'video_id'
laser_width_column = 'width'
datacode_column = 'datacode'
transect_column = 'transect'
date_column = 'TRANSECT_DATE'
time_column = 'timecode'
dom_substrate_column = 'substrate_id'
sub_substrate_column = 'subdominant_substrate'
dom_coverage_column = 'dominant_percent'
sub_coverage_column = 'subdominant_percent'
relief_column = 'relief_id'
disturbance_column = 'disturbance_id'
survey_mode_column = 'surveymode_id'
species_id_column = 'species_id'
species_name_column = 'species_name'
count_column = 'species_count'
confidence_column = 'id_confidence'
range_column = 'range'
height_column = 'height'
length_column = 'length'
width_column = 'width'
on_bottom_column = 'onBottom'
abundance_column = 'abundance'

base_columns = (id_column, medium_filename_column, confidence_column)
habitat_columns = base_columns + (relief_column, disturbance_column)
sub_habitat_columns = habitat_columns + (sub_substrate_column, sub_coverage_column)
dom_habitat_columns = habitat_columns + (dom_substrate_column, dom_coverage_column)
species_columns = base_columns + (species_id_column, species_name_column, count_column, range_column, height_column, length_column, width_column)
quality_columns = base_columns + (image_quality_column,)
protocol_columns = base_columns + (protocol_column,)

db_path = r"c:\Users\SKELLYR\Documents\database\datasets\import\PAC2000-031\_import\Jon's Bowie Video Database.accdb"

lookups = {
    protocol_column: ('C_Protocol', 'protocol_id', 'name'),
    image_quality_column: ('C_Video', 'Video_id', 'Label'),
    dom_substrate_column: ('C_Substrate_Type', 'substrate_id', 'Substrate_Type'),
    sub_substrate_column: ('C_Substrate_Type', 'substrate_id', 'Substrate_Type'),
    dom_coverage_column: ('C_Percent', 'subdominant_percent', 'Percent_Description'),
    sub_coverage_column: ('C_Percent', 'subdominant_percent', 'Percent_Description'),
    relief_column: ('C_Relief', 'relief_id', 'Relief_Type'),
    disturbance_column: ('C_Disturbance', 'disturbance_id', 'Label'),
    survey_mode_column: ('C_SurveyMode', 'surveymode_id', 'name'),
    confidence_column: ('C_ConfidenceIds', 'confidence_id', 'Confidence_id_Description'),
}

lookup_cache = {}

def do_lookup(db, column, id):
    if lookups.get(column) == None:
        # Return the value if there's no lookup for the column.
        return id
    try:
        # The ID has to be an integer.
        id = int(id)
    except:
        return None
    if not lookup_cache.get(column):
        lookup_cache[column] = {}
    if not lookup_cache[column].get(id):
        table, id_col, name_col = lookups[column]
        res = db.query_dict(f'select "{id_col}" as id, "{name_col}" as nm from "{table}" where "{id_col}"={id}')
        row = None
        try:
            row, _ = next(res)
            lookup_cache[column][id] = row['nm']
        except Exception as e:
            print(e)
    return lookup_cache[column][id]

column_map = {
    'id': 'original_id', 
    'relief_id': 'relief',
    'species_id': 'hart_code',
    'species_name': 'original_label',
    'species_count': 'count',
    'disturbance_id': 'disturbance',
    'substrate_id': 'substrate',
    'dominant_percent': 'coverage',
    'subdominant_substrate': 'substrate',
    'subdominant_percent': 'coverage',
}

def format_data(row, columns, dt=None):
    """
    Format the row using the columns list. For columns in the column_map,
    replace the key with the mapped key.
    """
    data = {column_map.get(k, k): row[k] for k in columns if row[k]}
    if dt:
        data['start_time'] = dt
        data['end_time'] = None
    return data

# Tracks entities that have a time span. When the object
# changes from the previous object, it is returned as 
# an object with a time span.
tracking = {}

def transect_start(dt, db, row):
    pass
    #print('transect start', row['transect'])

def transect_end(dt, db, row):
    pass
    #print('transect end', row['transect'])

def update_tracking(name, dt, data):
    if tracking.get(name) == None:
        # First item with this name. Create it with a start time.
        tracking[name] = {'data': data, 'start_time': dt, 'end_time': None}
    elif tracking[name]['data'] != data:
        # The data have changed. Update the time on the old one, save it
        # create a new packet and return the old one.
        tracking[name]['end_time'] = dt
        ret = tracking[name]
        tracking[name] = {'data': data, 'start_time': dt, 'end_time': None}
        return ret
    else:
        # The data are the same. There's nothing to do.
        pass
    return None

def resolve_lookups(db, row, columns):
    for col in columns:
        row[col] = do_lookup(db, col, row[col])

def dominant(dt, db, row):
    print('dominant')
    resolve_lookups(db, row, dom_habitat_columns)
    data = format_data(row, dom_habitat_columns)
    return update_tracking('dominant', dt, data)    


def subdominant(dt, db, row):
    pass
    print('subdominant')
    resolve_lookups(db, row, sub_habitat_columns)
    data = format_data(row, sub_habitat_columns)
    return update_tracking('subdominant', dt, data)    

def species(dt, db, row):
    pass
    print('species')
    resolve_lookups(db, row, species_columns)
    return format_data(row, species_columns, dt)

def cruise_number(dt, db, row):
    pass
    #print('cruise number')

def video_quality(dt, db, row):
    pass
    print('video quality')
    resolve_lookups(db, row, quality_columns)
    data = format_data(row, quality_columns)
    return update_tracking('quality', dt, data)

def relief(dt, db, row):
    pass
    print('relief')
    return subdominant(dt, db, row), dominant(dt, db, row)

def disturbance(dt, db, row):
    pass
    print('disturbance')
    return subdominant(dt, db, row), dominant(dt, db, row)

def protocol(dt, db, row):
    pass
    print('protocol')
    resolve_lookups(db, row, protocol_columns)
    data = format_data(row, protocol_columns)
    return update_tracking('protocol', dt, data)

# Maps datacodes onto a handler function.
data_codes = {
    1: transect_start,
    2: transect_end,
    3: dominant,
    4: species,
    5: subdominant,
    6: dominant,
    7: cruise_number,
    8: subdominant,
    11: video_quality,
    12: relief,
    13: disturbance,
    14: protocol,
    999: (dominant, subdominant)
}

def clean_row(row):
    """
    Remove crud from the row.
    """
    for k, v in row.items():
        if v in ('NULL',):
            row[k] = None

def format_datetime(date, time=None):
    """
    Format the date ad time as a string after appending them. If the time string is not
    given it is assumed that the date contains time information.
    """
    dt = date
    if not dt:
        return None
    if time:
        dt += timedelta(hours=time.hour, minutes=time.minute, seconds=time.second)
    return dt.strftime('%Y-%m-%dT%H:%M:%SZ')

def save_observation(data):
    """
    Save the observation with the timestamp.
    """
    if type(data) != tuple:
        data = (data,)
    for d in data:
        if d:
            print(d)

def run():
    import tempfile
    tmp_file = os.path.join(tempfile.gettempdir(), 'tmp.sqlite')
    if os.path.exists(tmp_file):
        os.unlink(tmp_file)

    sconn = sqlite3.connect(tmp_file)
    scur = sconn.cursor()

    if True:
        # Get a database handle.
        db = util.access(db_path)
        
        # Create a temporary table and add a datetime column to it that can be fixed.
        coldefs = db.columns('data')
        cols = [x for x, _ in coldefs]
        types = [y for _, y in coldefs]
        
        # For selecting from Access.
        sql_cols = ','.join(cols)
        
        # Remove date, time columns for inserting into sqlite. Just keep dt.
        while 'DATETIME' in types:
            i = types.index('DATETIME')
            cols = cols[:i] + cols[i+1:]
            types = types[:i] + types[i+1:]
        cols.append('dt')
        types.append('timestamp')
        insert_cols = ','.join(cols)
        param_cols = ','.join(['?' for _ in types])
        ddl_cols = ','.join([x + ' ' + y for x, y in list(zip(cols, types))])
        ddl = 'create table tmp (' + ddl_cols + ')'
        scur.execute('drop table if exists tmp')
        scur.execute(ddl)

        # Iterate over the rows from the source database.
        for row, _ in db.query_dict(f'select {sql_cols} from data'):
            clean_row(row)
            try:
                dt = format_datetime(row.get(date_column), row.get(time_column))
            except: 
                dt = None
            if not dt:
                continue
            dt = fix_date(datetime.strptime(dt, time_format))
            if dt and dt >= min_date and dt <= max_date:
                row['dt'] = dt
                scur.execute(f'insert into tmp ({insert_cols}) values ({param_cols})', [row[c] for c in cols])

    # Iterate over the temporary table.
    scur.execute(f'select * from tmp order by dt')
    cols = [x[0] for x in scur.description]
    last_dt = None
    for row in scur:
        row = dict(zip(cols, row))
        clean_row(row)

        if row.get('datacode'):
            dt = datetime.strptime(row['dt'], '%Y-%m-%d %H:%M:%S').strftime(time_format)
            dc = int(row['datacode'])
            fn = data_codes.get(dc)
            if not fn:
                continue
            last_dt = dt
            if type(fn) == tuple:
                for fn_ in fn:
                    save_observation(fn_(dt, db, row))
            else:
                save_observation(fn(dt, db, row))
        
    # Handle the remaining timespan events.
    for v in tracking.values():
        if not v['end_time']:
            v['end_time'] = last_dt
            print(v)

        
def fix_date(d):
    """
    Some files have screwed up dates, like the month and day are swapped. This function fixes that.
    """
    try:
        return datetime(year=d.year, month=d.day, day=d.month, hour=d.hour, minute=d.minute, second=d.second)
    except:
        return d

def parse_labels(v):
    """
    If the value is valid, return it as a json array string.
    """
    if v:
        return json.dumps([v])
    return None


if __name__ == '__main__':

    run()
    
    # import argparse

    # argv = sys.argv

    # argv = ['-v', "c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2/Jon's Bowie Video Database.accdb",
    #         '-o', 'c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2'
    # ]
    
    # # argv = [
    # #         '-l',
    # #         '-v', "c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2/Jon's Bowie Video Database.accdb",
    # #         '-o', 'c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2/'
    # # ]

    # args = argparse.ArgumentParser('vm_extract.py', 'python vm_extract.py [-f] <<-l <lookups file>> | <-v <db file> -o <output file>>>',
    #                                "Extracts VideoMiner annotations from an Access database, resolving lookups and translating to DB IDs in the process.")
    # args.add_argument('-l', '--lookups', dest='lookups', action='store_true', help='The lookups filename. A CSV file that will be created, containing target database lookup IDs.')
    # args.add_argument('-v', '--videominer', dest='videominer', help='The VideoMiner database filename. An access database.')
    # args.add_argument('-o', '--output', dest='output', help='The output directory. All files created will be saved here. Will be created if it does not exist.')
    # args.add_argument('-f', '--force', dest='force', action='store_true', default=False, help='Force. Forces recreation of an existing file.')


    # args = args.parse_args(argv)
    # if args.lookups:
    #     get_lookups(args.videominer, args.output)
    # elif args.videominer:
    #     process_vm(args.videominer, args.output, args.force)



