import os

from util import json_validator

v = json_validator()

for f in [f for f in os.listdir(os.path.realpath('import/schema')) if f.endswith('_schema.json')]:
    v.register_schema(os.path.join('import/schema', f))

v.validate('https://msea.science/2024-09/schema/cruise_import', "import/test/import.json")
