There are unmapped IDs in the data table for:
- Protocol
- Percent

There are two other MDB files with annotations that were not used. One has only records from 2011. One has no lookups.