#!/usr/bin/env python

"""
Extract the videominer database at c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/Jon's Bowie Video Database.mdb
to a csv file with the references correctly mapped to live database entities.
"""
import os
import sys
import csv
import json
import requests
from datetime import timedelta, datetime

sys.path.append('./import/scripts')

import util

user = 'robert.skelly@dfo-mpo.gc.ca'
pw = 'Lophelia1'
res = requests.post('https://msea.science/rov/api/v1/account/login/', {'email': user, 'password': pw})
result = res.json()
token = result['result']['token']
time_format = '%Y-%m-%dT%H:%M:%SZ'

min_date = datetime(2000, 1, 1)
max_date = datetime(2001, 1, 1)

def fix_date(d):
    """
    Some files have screwed up dates, like the month and day are swapped. This function fixes that.
    """
    try:
        return datetime(year=d.year, month=d.day, day=d.month, hour=d.hour, minute=d.minute, second=d.second)
    except:
        return d

def parse_labels(v):
    """
    If the value is valid, return it as a json array string.
    """
    if v:
        return json.dumps([v])
    return None


class runner:

    def __init__(self, output_path, db_path):

        # Load the lookup map.
        with open(os.path.join(output_path, 'lookup_map.json'), 'r') as f:
            self.lookup_map = json.load(f)

        self.output_path = output_path
        self.db_path = db_path

        # Columns common to all events.
        self.event_cols = ['start_time', 'end_time', 'medium_filename', 'attributes', 'original_labels', 'original_id', 'shape', 'shape_area', 'frames', 'note', 'tags']

        self.transects = {}
        self.habitat_db = csv.writer(open(os.path.join(output_path, 'habitat.csv'), 'w', newline='', encoding='utf8', buffering=1))
        self.transects_db = csv.writer(open(os.path.join(output_path, 'transects.csv'), 'w', newline='', encoding='utf8', buffering=1))
        self.observations_db = csv.writer(open(os.path.join(output_path, 'observations.csv'), 'w', newline='', encoding='utf8', buffering=1))
        self.status_db = csv.writer(open(os.path.join(output_path, 'status.csv'), 'w', newline='', encoding='utf8', buffering=1))
        self.comments_db = csv.writer(open(os.path.join(output_path, 'comments.csv'), 'w', newline='', encoding='utf8', buffering=1))
        self.habitat_cols = self.event_cols + ['substrate', 'substrate_coverage', 'biocover', 'biocover_coverage', 'complexity', 'thickness', 'flow', 'observation_confidence', 'relief', 'disturbance']
        self.observations_cols = self.event_cols + ['hart_code', 'count', 'observation_confidence', 'range', 'height', 'length', 'width', 'abundance']
        self.status_cols = self.event_cols + ['status_type_detail']
        self.comments_cols = self.event_cols + ['comment']
        self.transects_cols = ['name', 'start_time', 'end_time', 'objective', 'summary', 'note']
        self.dominant_habitat = None
        self.subdominant_habitat = None
        self.on_bottom_status = None
        self.survey_mode_status = None
        self.protocol_status = None
        self.image_quality_status = None

        self.id_column = 'id'
        self.medium_filename_column = 'photo_name'
        self.comment_column = 'Comment'
        self.protocol_column = 'protocol_id'
        self.image_quality_column = 'video_id'
        self.laser_width_column = 'width'
        self.datacode_column = 'datacode'
        self.transect_column = 'transect'
        self.date_column = 'TRANSECT_DATE'
        self.time_column = 'timecode'
        self.substrate_column = 'substrate_id'
        self.coverage_column = 'dominant_percent'
        self.relief_column = 'relief_id'
        self.disturbance_column = 'disturbance_id'
        self.survey_mode_column = 'surveymode_id'
        self.medium_filename_column = 'photo_name'
        self.species_id_column = 'species_id'
        self.species_name_column = 'species_name'
        self.count_column = 'species_count'
        self.confidence_column = 'id_confidence'
        self.range_column = 'range'
        self.height_column = 'height'
        self.length_column = 'length'
        self.width_column = 'width'
        self.on_bottom_column = 'onBottom'
        self.abundance_column = 'abundance'

        # The code that indicates that the sub is on the bottom.
        self.on_bottom = 1

        # Maps the data code to a handler function.
        self.data_codes = {
            1: (self.handle_transect_start,),      # Transect start
            2: (self.handle_transect_end,),        # Transect end
            3: (self.handle_dominant_habitat,),    # Dominant substrate code
            4: (self.handle_species,),             # Species event
            5: (self.handle_subdominant_habitat,), # Subdominant substrate code
            6: (self.handle_dominant_habitat,),    # Dominant substrate %
            7: (self.handle_cruise_number,),       # Cruise number
            8: (self.handle_subdominant_habitat,), # Subdominant substrate %
            9: (self.handle_on_bottom,),           # On bottom
            10: (self.handle_off_bottom,),         # Off bottom
            11: (self.handle_image_quality,),      # Video quality
            12: (self.handle_subdominant_habitat, self.handle_dominant_habitat),             # Relief
            13: (self.handle_subdominant_habitat, self.handle_dominant_habitat),        # Disturbance
            14: (self.handle_protocol,),           # Protocol
            999: (self.handle_subdominant_habitat, self.handle_dominant_habitat),             # All habitats (assumed)
        }

    def format_datetime(self, date, time=None):
        """
        Format the date ad time as a string after appending them. If the time string is not
        given it is assumed that the date contains time information.
        """
        dt = date
        if not dt:
            return None
        if time:
            dt += timedelta(hours=time.hour, minutes=time.minute, seconds=time.second)
        return dt.strftime('%Y-%m-%dT%H:%M:%SZ')
    
    def handle_event(self, row):
        """
        Create the base event object from the row. Establishes the start and end times, and any other global event properties.
        :param row: The row.
        :return: The base event dictionary.
        """
        event = {k: None for k in self.event_cols}
        event['medium_filename'] = None if not row.get(self.medium_filename_column) else row.get(self.medium_filename_column)
        try:
            event['start_time'] = row['dt']
            event['end_time'] = None
            event['original_id'] = int(row[self.id_column])
        except Exception as e:
            print(e)
        return event

    def handle_transect_start(self, row):
        """
        Start a transect. Dives aren't separately recorded.
        """
        try:
            self.transects[row[self.transect_column]] = {
                'name': row[self.transect_column], 
                'start_time': row['dt'],
                'end_time': None
            }
        except: pass

    def handle_transect_end(self, row):
        """
        End a transect.
        """
        try:
            self.transects[row[self.transect_column]]['end_time'] = row['dt']
        except: pass

    def write_transects(self):
        """
        Write the dives and transects to a file as a JSON fragment.
        There are no dives in the VideoMiner file, so each transect belongs to a dive with 
        """
        for trans in sorted(self.transects.values(), key=lambda a: a['start_time']):
            self.transects_db.writerow([trans.get(k, '') for k in self.transects_cols])

    def handle_habitat(self, row, tags=[]):
        """
        Handle a habitat event.
        """
        habitat = self.handle_event(row)
        habitat.update({
            'original_labels': parse_labels(row[self.species_name_column]),
            'substrate': row[self.substrate_column],
            'substrate_coverage': row[self.coverage_column],
            'relief': row[self.relief_column],
            'disturbance': row[self.disturbance_column],
            'medium_filename': row[self.medium_filename_column],
            'tags': tags
        })
        return habitat

    def handle_laser(self, row):
        """
        Handle a laser point measurement.
        """
        if row[self.laser_width_column]:
            status = self.handle_event(row)
            status.update({
                'status_type_detail': 86,
                'attributes': {'width': float(row[self.laser_width_column])}
            })
            if not status['status_type_detail']:
                raise Exception(f'Status type detail is empty in laser. Failed to map {row[self.laser_width_column]}.')
            self.status_db.writerow([status.get(k, '') for k in self.status_cols])

    def handle_species(self, row):
        """
        Handle"""
        if row['species_id'] != 'view':
            species = self.handle_event(row)
            species.update({
                'hart_code': row[self.species_id_column],
                'original_labels': parse_labels(row[self.species_name_column]),
                'count': row[self.count_column],
                'observation_confidence': row[self.confidence_column],
                'range': row[self.range_column],
                'height': row[self.height_column],
                'length': row[self.length_column],
                'width': row[self.width_column],
                'note': row[self.comment_column],
                'abundance': row[self.abundance_column],
            })
            self.observations_db.writerow([species.get(k, '') for k in self.observations_cols])

    def handle_subdominant_habitat(self, row):
        """
        Handle a subdominant habitat. Use handle_habitat to create the event,
        compare it to the last saved event and write it if there's a change.
        """
        print('subdominant habitat')
        habitat = self.handle_habitat(row, ['Subdominant'])
        if self.subdominant_habitat:
            self.subdominant_habitat['end_time'] = habitat['start_time']
            self.subdominant_habitat['tags'] = json.dumps(self.subdominant_habitat['tags'])
            self.habitat_db.writerow([self.subdominant_habitat.get(k, '') for k in self.habitat_cols])
        self.subdominant_habitat = habitat

    def handle_dominant_habitat(self, row):
        """
        Handle a dominant habitat. Use handle_habitat to create the event,
        compare it to the last saved event and write it if there's a change.
        """
        print('dominant habitat')
        habitat = self.handle_habitat(row, ['Dominant'])
        if self.dominant_habitat and self.dominant_habitat != habitat:
            self.dominant_habitat['end_time'] = habitat['start_time']
            self.dominant_habitat['tags'] = json.dumps(self.dominant_habitat['tags'])
            self.habitat_db.writerow([self.dominant_habitat.get(k, '') for k in self.habitat_cols])
        self.dominant_habitat = habitat

    def handle_survey_mode(self, row):
        id = row.get(self.survey_mode_column)
        if id:
            status = self.handle_event(row)
            status.update({
                'status_type_detail': id
            })
            if not status['status_type_detail']:
                raise Exception(f'Status type detail is empty in survey mode. Failed to map {row[self.survey_mode_column]}.')
            if self.survey_mode_status:
                self.survey_mode_status['end_time'] = status['start_time']
                self.status_db.writerow([self.survey_mode_status.get(k, '') for k in self.status_cols])
            self.survey_mode_status = status

    def handle_on_bottom(self, row):
        status = self.handle_event(row)
        status.update({
            'status_type_detail': 1
        })
        if not status['status_type_detail']:
            raise Exception(f'Status type detail is empty in on bottom.')
        if self.on_bottom_status:
            self.on_bottom_status['end_time'] = status['start_time']
            self.status_db.writerow([self.on_bottom_status.get(k, '') for k in self.status_cols])
            self.on_bottom_status = None
        self.on_bottom_status = status

    def handle_off_bottom(self, row):
        status = self.handle_event(row)
        status.update({
            'status_type_detail': 2
        })
        if not status['status_type_detail']:
            raise Exception(f'Status type detail is empty in off bottom.')
        if self.on_bottom_status:
            self.on_bottom_status['end_time'] = status['start_time']
            self.status_db.writerow([self.on_bottom_status.get(k, '') for k in self.status_cols])
            self.on_bottom_status = None
        self.on_bottom_status = status

    def handle_cruise_number(self, row):
        print('cruise number')
        pass

    def handle_image_quality(self, row):
        print('image quality', row['video_id'])
        id = row.get(self.image_quality_column)
        if id:
            status = self.handle_event(row)
            status.update({
                'status_type_detail': id
            })
            if not status['status_type_detail']:
                raise Exception(f'Status type detail is empty in image quality. Failed to map: {id}.')
            if self.image_quality_status:
                self.image_quality_status['end_time'] = status['start_time']
                self.status_db.writerow([self.image_quality_status.get(k, '') for k in self.status_cols])
            self.image_quality_status = status

    def handle_protocol(self, row):
        """
        Write a status event with a protocol change.
        """
        id = row.get(self.protocol_column)
        if id:
            status = self.handle_event(row)
            status.update({
                'status_type_detail': id
            })
            if not status['status_type_detail']:
                raise Exception(f'Status type detail is empty in protocol. Failed to map {id}.')
            if self.protocol_status:
                self.protocol_status['end_time'] = status['start_time']
                self.status_db.writerow([self.protocol_status.get(k, '') for k in self.status_cols])
            self.protocol_status = status

    def handle_fail(self, row):
        raise Exception('Fail')

    def handle_skip(self, row):
        print('skip')

    def handle_comment(self, row):
        """
        Write the comment event.
        """
        if row[self.comment_column]:
            comment = self.handle_event(row)
            comment.update({
                'comment': row.get(self.comment_column)
            })
            self.comments_db.writerow([comment.get(k, '') for k in self.comments_cols])

    def clean_row(self, row):
        """
        Remove crud from the row.
        """
        for k, v in row.items():
            if v in ('NULL',):
                row[k] = None

    def run(self):
        """
        Extract the VideoMiner database into a set of data files that can be imported into the database.

        There are several types of events created:
        1) Observation Event - created for each species observation, usually by code 4.
        2) Habitat Event - created when a habitat code is received, or when one of the relevant column values changes. A
        habitat event covers a time span, so when something changes the current event is closed and a new one is opened.
        There are usually two habitat events, dominant and subdominant.
        3) Status Event - on/off bottom, laser points, etc.
        4) Comment Event - comments by the operator.
        """

        # Write the column headers to the output files.
        for k in ('habitat', 'transects', 'observations', 'status', 'comments'):            
            getattr(self, k + '_db').writerow(getattr(self, k + '_cols'))

        # Track values that dispatch events on change.
        on_bottom = -9999
        survey_mode = -9999
        protocol = -9999
        transect_name = None
        last_row = None
        force_start = False

        import sqlite3

        sconn = sqlite3.connect("tmp.sqlite")
        scur = sconn.cursor()

        if True:
            # Get a database handle.
            db = util.access(self.db_path)
            # Create a temporary table and add a datetime column to it that can be fixed.
            
            coldefs = db.columns('data')
            cols = [x for x, _ in coldefs]
            types = [y for _, y in coldefs]
            # For selecting from Access.
            sql_cols = ','.join(cols)
            # Remove date, time columns for inserting into sqlite. Just keep dt.
            while 'DATETIME' in types:
                i = types.index('DATETIME')
                cols = cols[:i] + cols[i+1:]
                types = types[:i] + types[i+1:]
            cols.append('dt')
            types.append('timestamp')
            insert_cols = ','.join(cols)
            param_cols = ','.join(['?' for _ in types])
            ddl_cols = ','.join([x + ' ' + y for x, y in list(zip(cols, types))])
            ddl = 'create table tmp (' + ddl_cols + ')'
            scur.execute('drop table if exists tmp')
            scur.execute(ddl)

            # Iterate over the rows from the source database.
            for row, _ in db.query_dict(f'select {sql_cols} from data'):
                self.clean_row(row)
                try:
                    dt = self.format_datetime(row.get(self.date_column), row.get(self.time_column))
                except: 
                    dt = None
                if not dt:
                    continue
                dt = fix_date(datetime.strptime(dt, time_format))
                if dt and dt >= min_date and dt <= max_date:
                    row['dt'] = dt
                    scur.execute(f'insert into tmp ({insert_cols}) values ({param_cols})', [row[c] for c in cols])

        # Iterate over the temporary table.
        scur.execute(f'select * from tmp order by dt')
        cols = [x[0] for x in scur.description]
        for row in scur:
            row = dict(zip(cols, row))
            self.clean_row(row)

            row['dt'] = datetime.strptime(row['dt'], '%Y-%m-%d %H:%M:%S').strftime(time_format)

            # If the transect name has changed, finalize events.
            tn = row[self.transect_column]
            if not transect_name or transect_name != tn:
                # This is the first row of a new transect. Finalize the status events
                # and force the creation of new ones.
                if last_row:
                    # Finalize if available.
                    self.finalize_events(last_row)
                force_start = True
            transect_name = tn

            # For the columns that map to a related entity, get the mapped ID.
            for col in self.lookup_map.keys():
                val = row[col]
                if val:
                    obj = self.lookup_map[col].get(str(val))
                    if obj:
                        row[col] = obj['db_id']
                    else:
                        raise Exception(f'Failed to map the column value {row[col]} for {col}.')
                else:
                    row[col] = None

            # These are handlers that were already called. If one of them is called because the 
            # relevant datacode appears, this will prevent them from being called again.
            handled = []

            if row.get(self.comment_column) and not self.handle_species in self.data_codes[int(row[self.datacode_column])]:
                # If there's a comment but this is not a species row, record a comment.
                self.handle_comment(row)
                handled.append(self.handle_comment)

            if row.get(self.laser_width_column):
                # If there's a laser width make a status event.
                self.handle_laser(row)
                handled.append(self.handle_laser)

            # Handle image quality.
            iq = row.get(self.image_quality_column)
            if force_start or iq != image_quality:
                self.handle_image_quality(row)
                image_quality = iq
                handled.append(self.handle_image_quality)

            # Handle on/off bottom status.
            ob = row.get(self.on_bottom_column)
            if force_start or ob != on_bottom:
                if ob == self.on_bottom:
                    self.handle_on_bottom(row)
                else:
                    self.handle_off_bottom(row)
                on_bottom = ob
                handled.extend([self.handle_on_bottom, self.handle_off_bottom])

            # Handle survey mode.
            sm = row.get(self.survey_mode_column)
            if force_start or sm != survey_mode:
                self.handle_survey_mode(row)
                survey_mode = sm
                handled.append(self.handle_survey_mode)

            # Handle survey protocol.
            sp = row.get(self.protocol_column)
            if force_start or sp != protocol:
                self.handle_protocol(row)
                protocol = sp
                handled.append(self.handle_protocol)

            if row[self.datacode_column]:
                # Call the functions registered to handle the data code. Only call
                # if the function isn't in the list of those that have already been handled.
                for fn in [fn for fn in self.data_codes[int(row[self.datacode_column])] if not fn in handled]:
                    fn(row)

            last_row = row
            force_start = False

        self.finalize_events(last_row)

        # Write the transects.
        self.write_transects()

        for db in ['habitat_db', 'transects_db', 'observations_db', 'status_db', 'comments_db']:
            setattr(self, db, None)
    
    def finalize_events(self, row):
        """
        Finalize dangling status events by seting the end time and saving them.
        """
        # Write the dangling on bottom status.
        if self.on_bottom_status:
            self.on_bottom_status['end_time'] = row['dt']
            self.status_db.writerow([self.on_bottom_status.get(k, '') for k in self.status_cols])
            self.on_bottom_status = None

        # Write the dangling survey mode status.
        if self.survey_mode_status:
            self.survey_mode_status['end_time'] = row['dt']
            self.status_db.writerow([self.survey_mode_status.get(k, '') for k in self.status_cols])
            self.survey_mode_status = None

        if self.protocol_status:
            self.protocol_status['end_time'] = row['dt']
            self.status_db.writerow([self.protocol_status.get(k, '') for k in self.status_cols])
            self.protocol_status = None

        if self.image_quality_status:
            self.image_quality_status['end_time'] = row['dt']
            self.status_db.writerow([self.image_quality_status.get(k, '') for k in self.status_cols])
            self.image_quality_status = None

        if self.dominant_habitat:
            self.dominant_habitat['end_time'] = row['dt']
            self.dominant_habitat['tags'] = json.dumps(self.dominant_habitat['tags'])
            self.habitat_db.writerow([self.dominant_habitat.get(k, '') for k in self.habitat_cols])
            self.dominant_habitat = None

        if self.subdominant_habitat:
            self.subdominant_habitat['end_time'] = row['dt']
            self.subdominant_habitat['tags'] = json.dumps(self.subdominant_habitat['tags'])
            self.habitat_db.writerow([self.subdominant_habitat.get(k, '') for k in self.habitat_cols])
            self.subdominant_habitat = None

def process_vm(db_path, output_path, force):

    inst = runner(output_path, db_path)
    inst.run()


def get_lookups(db_path, output_dir):
    """
    Write a JSON file containing a mapping from the VM database IDs to the live database entities.
    If the file exists, it will be updated with the current values of the entites if possible.
    """
    # A comma-delimited list of entity names and urls. The entity names are from lookup_tables.txt.
    url_map = {}
    with open(os.path.join(output_dir, 'lookup_urls.txt'), 'r') as f:
        line = f.readline()
        while line:
            try:
                db_table, url = line.strip().split(',')
                url_map[db_table] = (db_table, url)
            except: pass
            line = f.readline()

    # A comma-delimited list of VM lookup tables to live database entities.
    table_map = {}
    with open(os.path.join(output_dir, 'lookup_tables.txt'), 'r') as f:
        line = f.readline()
        while line:
            try:
                vm_column, vm_table, db_table, id_col, name_col = line.strip().split(',')
                table_map[vm_column] = (vm_column, vm_table, db_table, id_col, name_col)
            except: pass
            line = f.readline()

    # Load or create the lookup map.
    try:
        with open(os.path.join(output_dir, 'lookup_map.json'), 'r') as f:
            lookups = json.load(f)
    except:
        lookups = {}

    # Get a database handle.
    db = util.access(db_path)

    # Iterate over the table map, loading entities.
    for (vm_column, vm_table, db_table, id_col, name_col) in table_map.values():
        if lookups.get(vm_column) == None:
            lookups[vm_column] = {}
        for (id, name), _ in db.query_list(f'select "{id_col}", "{name_col}" from "{vm_table}"'):
            k = str(int(id))
            if lookups[vm_column].get(k) == None:
                lookups[vm_column][k] = {
                    'db_id': 0,
                    'vm_id': int(id),
                    'vm_table': vm_table,
                    'vm_name': name,
                }
        # res = requests.get(entities[v], headers={'Authorization': f'Token {token}'})
        # res = res.json()

    with open(os.path.join(output_dir, 'lookup_map.json'), 'w', encoding='utf8') as f:
        json.dump(lookups, f, indent=2)

if __name__ == '__main__':

    import argparse

    argv = sys.argv

    argv = ['-v', "c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2/Jon's Bowie Video Database.accdb",
            '-o', 'c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2'
    ]
    
    # argv = [
    #         '-l',
    #         '-v', "c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2/Jon's Bowie Video Database.accdb",
    #         '-o', 'c:/Users/SKELLYR/Documents/database/datasets/import/_done/PAC2000-031/_import2/'
    # ]

    args = argparse.ArgumentParser('vm_extract.py', 'python vm_extract.py [-f] <<-l <lookups file>> | <-v <db file> -o <output file>>>',
                                   "Extracts VideoMiner annotations from an Access database, resolving lookups and translating to DB IDs in the process.")
    args.add_argument('-l', '--lookups', dest='lookups', action='store_true', help='The lookups filename. A CSV file that will be created, containing target database lookup IDs.')
    args.add_argument('-v', '--videominer', dest='videominer', help='The VideoMiner database filename. An access database.')
    args.add_argument('-o', '--output', dest='output', help='The output directory. All files created will be saved here. Will be created if it does not exist.')
    args.add_argument('-f', '--force', dest='force', action='store_true', default=False, help='Force. Forces recreation of an existing file.')


    args = args.parse_args(argv)
    if args.lookups:
        get_lookups(args.videominer, args.output)
    elif args.videominer:
        process_vm(args.videominer, args.output, args.force)



