#!/usr/bin/env python

"""
This program loads a cruise import specification JSON file and creates or updates the cruise along with all associated metadata and entities.
"""

import os
import re
import sys
import csv
import json
import time
from osgeo import ogr
from datetime import datetime, timedelta

import django.db
from django.db import transaction

class CruiseImporter:

    # Generic time formats for stream, annotation and event files. They'll be
    # attempted in order, and the one that succeeds will return.
    time_format = ('%Y-%m-%dT%H:%M:%SZ', '%Y-%m-%dT%H:%M:%S.%fZ')
    date_format = ('%Y-%m-%d', '%Y-%m-%dT%H:%M:%SZ', '%Y-%m-%dT%H:%M:%S.%fZ')

    # A list of date extraction regexes. Each will be tried in turn until one works.
    biigle_time_formats = (
        r'^.+?_([0-9]{4})-([0-9]{2})-([0-9]{2})-([0-9]{2})([0-9]{2})([0-9]{2})Z\..+$',              # YYYY-mm-dd-HHMMSSZ
        r'^.+?_([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})Z\..+$',                 # YYYYmmddHHMMSSZ
        r'^.+?_([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})Z\..+$',                # YYYYmmddTHHMMSSZ
        r'^.+?_([0-9]{4})([0-9]{2})([0-9]{2})T([0-9]{2})([0-9]{2})([0-9]{2})\.([0-9]+)Z\..+$',      # YYYYmmddTHHMMSS.fffffZ
    )

    def __init__(self):
        # Minimum distance between two stream records. Used to cull overly-dense
        # timestamped streams.
        self.stream_resolution = timedelta(seconds=1)

        # The root path is the repo root.
        self.root_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

        # Cache for looked-up instances indexed by the source column name, then the ID of the lookup.
        self.lookup_cache = {}

        # Must be overwritten by subclasses.    
        self.version = None

        # Will be set in init.
        self.upload_dir = os.environ['MSEA_UPLOAD_DIR']
        self.documents_dir = os.environ['MSEA_DOCUMENTS_DIR']

        # Set the Django application path.
        sys.path.append(os.path.join(self.root_path, 'app'))

        # Initial status for job tracking.
        self.status = 'Initialized'

    def fix_event_types(self, obj):
        """
        Find the event_type property on the object and convert it to an array called
        event_types. If there's already an event_types array, add the event_type to it
        without duplicates. All types will be lower-cased.
        """
        types = set(obj.get('event_types', []))
        try:
            types.add(obj.pop('event_type'))
        except: pass
        obj['event_types'] = list(map(str.lower, types))

    def format_time(self, t):
        """
        Parse a timestamp from string form. Try each of the time format strings 
        in turn, return the datetime on success.
        """
        for tf in self.time_format:
            try:
                return datetime.strptime(t, tf)
            except: continue
        raise Exception(f'Failed to parse time {t}.')

    def format_date(self, t):
        """
        Parse a timestamp from string form. Try each of the date format strings 
        in turn, return the datetime on success.
        """
        for df in self.date_format:
            try:
                dt = datetime.strptime(t, df)
                return datetime(year=dt.year, month=dt.month, day=dt.day)
            except: continue
        raise Exception(f'Failed to parse date {t}.')

    def point_to_wkb(self, p):
        """
        Convert the tuple/list of coordinates to a wkb string. Coorinates must
        be in the order x, y, z.

        :param p: A tuple or list containing at least 2 coordinates.
        :return: A well-known binary string representing and OGR point geometry.
        :rtype: str
        """
        p = [float(c) for c in p]
        x = p[0]
        y = p[1]
        if x < -140 or x > -110 or y < 30 or y > 60:
            raise Exception(f'The x or y coordinates are out of range: x: {x}, y: {y}.')
        pt = ogr.Geometry(ogr.wkbPoint if len(p) == 2 else ogr.wkbPoint25D)
        pt.AddPoint(*p)
        return pt.ExportToWkb()

    def clean(self, v):
        """
        Clean the value: if it is a numeric type, return it. If it is not None (i.e., is assumed 
        to be a string), strip it. These are presumably loaded from a CSV file, so they're all
        going to be one of those three types.
        """
        if type(v) in (int, float, dict, list):
            return v
        if v != None:
            v = v.strip()
        if v == '':
            v = None
        return v

    def load_json(self, filename):
        """
        Load and parse a JSON file.
        :param filename: The JSON file path.
        :return The JSON object.
        """
        with open(filename, 'r', encoding='utf-8-sig') as f:
            return json.load(f)

    def check_header(self, header, cols):
        """
        Check the header row (a list of strings) against the list of required 
        columns. The check is case insensitive. The return value is a list of 
        lower-cased column names from the header.
        :param header: The header list.
        :param cols: A list of columns that must be present.
        """
        header = [h.lower() for h in header]
        cols = [c.lower() for c in cols]
        for c in cols:
            if not c in header:
                raise Exception(f'The header does not contain the column {c}.')
        return header

    def map_columns(self, head, cols, map):
        """
        For each name in the header, assign the value in cols to the 
        result dictionary, using the mapped value for the header name
        as the key.
        """
        row = {}
        for i in range(len(head)):
            if map.get(head[i]):
                row[map[head[i]]] = cols[i]
        return row

    def resolve_casts(self, props, cast_map):
        """
        Cast the items in the props list to the correct type, or an alternate.
        :param props: The dictionary of properties.
        :param cast_map: A list of tuples containing the column name, a callable which can cast the item, 
        and an alternate value (e.g., None).
        """
        try:
            for col, cls, alt in cast_map:
                try:
                    props[col] = cls(props[col])
                except:
                    props[col] = alt
        except Exception as e:
            raise e
        
    def resolve_lookups(self, props, class_map):
        """
        We have to look up the lookup entity instances for each relevant column and cache them.
        :param props: The dictionary of row properties. The lookup IDs have already been mapped to DB IDs.
        :param class_map: A mapping of column names to lookup classes.
        """
        try:
            # Get column name and class.
            for col, cls in class_map:
                try:
                    # Get the ID of the instance.
                    id = int(props[col])
                    if self.lookup_cache.get(col) == None:
                        # If the type cache is None, create it.
                        self.lookup_cache[col] = {}
                    if id > 0:
                        if self.lookup_cache[col].get(id) == None:
                            # If the instance doesn't exist, load and cache it. 
                            self.lookup_cache[col][id] = cls.objects.get(id=id)
                        # Assign the prop.
                        props[col] = self.lookup_cache[col][id]
                    else:
                        props[col] = None
                except Exception as e:
                    # If the lookup can't be loaded, set the field to None to avoid an error.
                    props[col] = None
        except Exception as e:
            raise e
                    
    def resolve_list(self, props, name):
        """
        Attempt to load the row value from a JSON list to a list and re-set that to the
        row value. 
        :param props: The dict containing the row values.
        :param name: The name of the column to parse.
        """
        try:
            # Try to parse a list from the value.
            labels = json.loads(props[name])
            if type(labels) != list or len(labels) == 0:
                # The result is not a list.
                raise
            # Set the list on the property.
            props[name] = labels
            return True
        except:
            return False

    def in_time(self, inst, dt):
        """
        If the dt is in range of the start_time and end_time of the instance, return true.
        :param inst: The instance to check the time against.
        :param dt: The date-time to compare.
        """
        return inst.start_time <= dt and inst.end_time >= dt

    def import_cruise(self, cruise_import):
        """
        Import the cruise.
        """
        cruise_obj = cruise_import.data

        cruise_import.clear_log()
        cruise_import.log(f'Starting import on cruise {cruise_import.name}.')
        cruise_import.set_running()

        # Do the import.
        self.do_cruise(cruise_import, cruise_obj)

        cruise_import.log(f'Finished importing cruise {cruise_import.name}.')
        cruise_import.set_completed()


class CruiseImportCanceled(Exception):
    """
    Thrown when the current import should be canceled.
    """
    pass


class CruiseImporterV1(CruiseImporter):
    """
    Cruise importer version 1.
    """

    def do_cruise_crew(self, cruise_import, cruise, cruise_obj):
        """
        Record the crew members of the cruise. Remove crew members that are not
        in the current set of crew members.
        :param cruise: The cruise.
        :param crew_objs: A list of crew member configuration objects.
        """
        from msea.models.rov import CruiseCrew, CruiseRole
        from msea.models.shared import Person

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log('Processing cruise crew.')

        crew_objs = cruise_obj['crew']
        keep = []   # Stores the IDs of existing cruise members that will be kept.

        if cruise_obj.get('processCrew'):
        
            for crew_obj in crew_objs:
                cruise_import.log(f'Processing cruise crew member {crew_obj["id"]}.')
                
                # Configure properties. Instantiate person and role entities.
                props = {k: self.clean(crew_obj.get(k)) for k in ('person', 'role', 'note')}
                props['person'] = Person.objects.get(id=props['person']['id'])
                props['role'] = CruiseRole.objects.get(id=props['role']['id'])

                try:
                    # Update the existing crew member.
                    member = CruiseCrew.objects.get(
                        cruise=cruise, 
                        person=props['person'], 
                        cruise_role=props['role']
                    )
                    member.note = props['note']
                    member.save()
                    cruise_import.log('Updated existing cruise crew member.')
                except:
                    # Create the crew member.
                    member = CruiseCrew.objects.create(
                        cruise=cruise, 
                        person=props['person'],
                        cruise_role=props['role'],
                        note=props['note']
                    )
                    cruise_import.log('Created cruise crew member.')

                # Remember the crew members to keep.
                keep.append(member.id)
        
        else:
            cruise_import.log('Not processing cruise crew.')

        if not cruise_obj.get('keepCrew'):
            # Delete crew that are not part of the new set.
            cruise_import.log('Deleting existing cruise crew members.')
            CruiseCrew.objects.filter(cruise=cruise).exclude(id__in=keep).delete()
        else:
            cruise_import.log('Keeping existing cruise crew memembers.')

    def do_dive_crew(self, cruise_import, dive, dive_obj):
        """
        Record the crew members of the dive. Remove crew members that are not
        in the current set of crew members.
        :param dive: The dive.
        :param crew_objs: A list of crew member configuration objects.
        """
        from msea.models.rov import DiveCrew, DiveRole
        from msea.models.shared import Person

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log('Processing dive crew.')

        crew_objs = dive_obj['crew']
        keep = []   # Stores the IDs of existing dive members that will be kept.

        if dive_obj.get('processCrew'):

            for crew_obj in crew_objs:
                cruise_import.log(f'Processing cruise crew member {crew_obj["id"]}.')

                # Configure properties. Instantiate person and role entities.
                props = {k: self.clean(crew_obj.get(k)) for k in ('person', 'role', 'note')}
                props['person'] = Person.objects.get(id=props['person']['id'])
                props['role'] = DiveRole.objects.get(id=props['role']['id'])

                try:
                    # Update the existing crew member.
                    member = DiveCrew.objects.get(
                        dive=dive,
                        person=props['person'], 
                        dive_role=props['role'],
                    )
                    member.note = props['note']
                    member.save()
                    cruise_import.log('Updated existing dive crew member.')
                except:
                    # Create the crew member.
                    member = DiveCrew.objects.create(
                        dive=dive,
                        person=props['person'],
                        dive_role=props['role'],
                        note=props['note']
                    )
                    cruise_import.log('Created dive crew member.')

                # Remember the crew members to keep.
                keep.append(member.id)

        else:
            cruise_import.log('Not processing dive crew.')

        if not dive_obj.get('keepCrew'):
            # Delete crew that are not part of the new set.
            cruise_import.log('Deleting keeping dive crew members.')
            DiveCrew.objects.filter(dive=dive).exclude(id__in=keep).delete()
        else:
            cruise_import.log('Keeping existing dive crew members.')

    def do_transects(self, cruise_import, dive, trans_objs):
        """
        Create or update the transects on the dive. Existing transects that are not in the list are removed.
        """
        from msea.models.rov import Transect

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing transects for dive {dive.name}.')

        # Cast map for cell values.
        cast_map = (('start_time', self.format_time, None), ('end_time', self.format_time, None))

        for trans_obj in trans_objs:
            cruise_import.log(f'Processing transect {trans_obj["name"]}.')

            # Clean the properties and resolve casts.
            props = {k: self.clean(trans_obj.get(k)) for k in ('name', 'start_time', 'end_time', 'objective', 'summary', 'note')}
            self.resolve_casts(props, cast_map)

            try:
                # Locate an existing transect by name.
                trans = Transect.objects.get(dive=dive, name__iexact=str(props['name']))
                if trans_obj['doUpdate']:
                    for k, v in props.items():
                        setattr(trans, k, v)
                    trans.save()
                    cruise_import.log('Updated existing transect.')
                else:
                    cruise_import.log('Not updating transect.')
            except:
                # Create the transect.
                trans = Transect.objects.create(dive=dive, **props)
                cruise_import.log('Created new transect.')

    def do_positions(self, cruise_import, dive, instrument_config, pos_obj, dim=3):
        """
        Record 3d positions. The coordinates are decimal degrees, relative to WGS84. Depths are in metres relative to the ocean surface.

        Required file format is CSV with columns timestamp, x, y and z.
        :param instrument_config: The instrument configuration instance.
        :param pos_obj: The events file object.
        :param dim: The number of dimensions. 2 for x, y; 3 for x, y, z.
        """
        from msea.models.shared import UploadedFile
        from msea.models.rov import Position, PositionType

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing positions for instrument configuration {instrument_config.id} on dive {dive.name} from file {pos_obj['file']['id']}.')

        # Get the uploaded file object.
        try:
            upload = UploadedFile.objects.get(id=pos_obj['file']['id'])
        except Exception as e:
            raise Exception(f'The uploaded file, {pos_obj['file']['name']} ({pos_obj['file']['path']}), was not found.')
        
        # Get the position type. TODO: Soon to be obsolete.
        pt = PositionType.objects.get(id=1)
        cruise_import.log(f'Position type is {pt.name}.')
        
        # Make the columns list and  trim it depending on the number of dimensions.
        cols = ['timestamp', 'x', 'y', 'z'][:dim + 1]
        
        try:
            with open(os.path.join(self.upload_dir, upload.path), 'r', encoding='utf-8-sig') as f:
                db = csv.reader(f)
                # Check that the header has the necessary fields.
                head = self.check_header(next(db), cols)
                # Get the field indices.
                ti = head.index('timestamp')
                ci = [head.index(c) for c in cols[1:]]  # Get the column indices by trimming the columns list.

                # Load the rows so they can be sorted.
                rows = []
                for row in db:
                    dt = self.format_time(row[ti])
                    # If this record is not within the configured dive's start and end times, skip it.
                    if not self.in_time(dive, dt):
                        continue
                    rows.append(row)

                # Batch storage.
                positions = []
                # The last row's timestamp.
                last_dt = None

                # Process the sorted rows.
                for row in sorted(rows, key=lambda a: a[ti]):
                    dt = self.format_time(row[ti])
                    # If this record is less than the configured time after the last one, 
                    # and is not the first record, skip it.
                    if last_dt and (dt - self.stream_resolution) < last_dt:
                        continue

                    # Store current time for next row.
                    last_dt = dt

                    # Create the position object and append to the batch. 
                    # If the creation fails, the input data are invalid.
                    positions.append(Position(
                        instrument_config = instrument_config,
                        timestamp = dt, 
                        geom = self.point_to_wkb([row[i] for i in ci]),
                        position_type = pt
                    ))

                    if len(positions) > 1000:
                        # Save a batch.
                        cruise_import.log('Saving a batch of positions.')
                        Position.objects.bulk_create(positions)
                        positions = []

                # Save the last batch.
                cruise_import.log('Saving final batch of positions.')                    
                Position.objects.bulk_create(positions)
        except Exception as e:
            raise Exception(f'Error parsing positions in {upload.name} for dive {dive.name}: ' + str(e))

    def do_measurements(self, cruise_import, dive, instrument_config, meas_obj):
        """
        Record measurements. The measurement contains a scalar value and a timestamp. The type of measurement is given by the measurement_type.

        Required file format is CSV with columns timestamp and value.
        :param instrument_config: The instrument configuration instance.
        :param meas_obj: The measurements file object.
        """
        from msea.models.shared import UploadedFile
        from msea.models.rov import Measurement, MeasurementType

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing measurements for instrument configuration {instrument_config.id} on dive {dive.name} from file {meas_obj['file']['id']}.')

        # Get the uploaded file.
        try:
            upload = UploadedFile.objects.get(id=meas_obj['file']['id'])
        except Exception as e:
            raise Exception(f'The uploaded file, {meas_obj['file']['name']} ({meas_obj['file']['path']}), was not found.')
        
        # Retrieve the measurement type.
        mt = MeasurementType.objects.get(id=meas_obj['measurement_type']['id'])
        
        cruise_import.log(f'Measurement type is {mt.name}.')
        try:
            with open(os.path.join(self.upload_dir, upload.path), 'r', encoding='utf-8-sig') as f:
                db = csv.reader(f)
                # Check that the header has the necessary fields.
                head = self.check_header(next(db), ('timestamp', 'value'))
                # Get the field indices.
                ti = head.index('timestamp')
                vi = head.index('value')

                # Load the rows for the dive so they can be sorted.
                rows = []
                for row in db:
                    dt = self.format_time(row[ti])
                    # Check if time is within the dive's times.
                    if not self.in_time(dive, dt):
                        continue
                    rows.append(row)
                rows = sorted(rows, key=lambda a: a[ti])

                # Measurement batch storage.
                measurements = []
                # The time of the previous row.
                last_dt = None

                # Process the sorted rows.
                for row in sorted(rows, key=lambda a: a[ti]):
                    dt = self.format_time(row[ti])

                    # If current row is less than the configured time after the current
                    # one, skip it.
                    if last_dt and (dt - self.stream_resolution) < last_dt:
                        continue

                    # Record the time of the current row for the next loop.
                    last_dt = dt
                    
                    # Create the measurement.
                    # If the data are not valid, this will fail.
                    measurements.append(Measurement(
                        instrument_config = instrument_config,
                        timestamp = dt, 
                        quantity = float(row[vi]),
                        measurement_type = mt
                    ))

                    # Persist the measurements in the cache.
                    if len(measurements) > 1000:
                        cruise_import.log('Saving a batch of measurements.')
                        Measurement.objects.bulk_create(measurements)
                        measurements = []

                # Persist the remaining measurements.
                cruise_import.log('Saving the final batch of measurements.')
                Measurement.objects.bulk_create(measurements)
        except Exception as e:
            raise Exception(f'Error parsing measurements in {upload.name} for dive {dive.name}: ' + str(e))                

    def do_streams(self, cruise_import, dive, instrument_config, streams_obj):
        """
        Handle data streams in according to the declared type.
        """
        if streams_obj['stream_type'] == 'position3d':
            self.do_positions(cruise_import, dive, instrument_config, streams_obj, 3)
        elif streams_obj['stream_type'] == 'position2d':
            self.do_positions(cruise_import, dive, instrument_config, streams_obj, 2)
        elif streams_obj['stream_type'] == 'measurement':
            self.do_measurements(cruise_import, dive, instrument_config, streams_obj)
        else:
            raise Exception(f"Invalid stream type: {streams_obj['stream_type']}.")
                    
    def do_generic_observations(self, cruise_import, dive, instrument_config, events_obj):
        """
        Generic observations are created from a JSON file, which is an array of objects. Each object is 
        an unspecified dictionary of keys and values.

        These can be comments (a 'comment' property), status events (a 'status' property), measurements
        (a 'unit' and a 'quantity' property), or habitat/species/anthropogenic, which entail an unbounded
        set of possible properties (substrate, aphia_id, etc.).
        """
        from msea.models.shared import UploadedFile
        from msea.models.rov import AnnotationJob, Event
        from msea.models.rov.imports import GenericLabelMap

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing generic observations for instrument configuration {instrument_config.id} on dive {dive.name}.')

        # Get the uploaded file object.
        try:
            upload = UploadedFile.objects.get(id=events_obj['file']['id'])
        except Exception as e:
            raise Exception(f'The uploaded file, {events_obj['file']['name']} ({events_obj['file']['path']}), was not found.')

        # Get the annotation job.
        try:
            annotation_job = AnnotationJob.objects.get(pk=events_obj['annotation_job']['id'])
        except: 
            annotation_job = None

        # Store GenericLabelMap items in the cache keyed by label.
        label_cache = {}

        try:
            with open(os.path.join(self.upload_dir, upload.path), 'r', encoding='utf-8-sig') as f:
                observations = json.load(f)

                for obs in observations:

                    # Filter objects that are not in the dives time frame.
                    start_time = datetime.strptime(obs['start_time'], '%Y-%m-%dT%H:%M:%SZ')
                    end_time = datetime.strptime(obs['end_time'], '%Y-%m-%dT%H:%M:%SZ') if obs.get('end_time') else None
                    if not (self.in_time(dive, start_time) or (end_time and self.in_time(dive, end_time))):
                        continue

                    del obs['start_time']
                    del obs['end_time']
                    label = obs['label']

                    # Instantiate the label map and cache it.
                    if not label_cache.get(label):
                        try:
                            label_cache[label] = GenericLabelMap.objects.get(label_text=label)
                        except GenericLabelMap.DoesNotExist as e:
                            raise Exception(f'Could not find generic mapping for label: {label}')

                    # Assign the label map properties.
                    for k, v in label_cache[label].properties.items():
                        obs[k] = v

                    self.fix_event_types(obs)
                    
                    # Create the event. Can't bulk create because of the inheritance chain.
                    Event.objects.create(
                        start_time=start_time,
                        end_time=end_time,
                        dive = dive,
                        annotation_job=annotation_job,
                        instrument_config=instrument_config,
                        properties=obs
                    )
        except Exception as e:
            raise Exception(f'Error parsing observations in {upload.name} for dive {dive.name}: ' + str(e))

    def parse_biigle_time(self, row):
        """
        Parse the Biigle times from the filename and frames array.
        """
        
        # Get the frames list.
        frames = row.get('frames')
        if frames:
            # If there are frames, parse into an array.
            frames = json.loads(frames)
        else:
            # If there are no frames, just use zero.
            frames = [0]
        
        # Get the video or photo filename.
        filename = row.get('filename')
        
        for format in self.biigle_time_formats:
            try:
                # Try to match the current format with the filename.
                matchiter = re.finditer(format, filename)
                # Extract the date time parts.
                Y, m, d, H, M, S = list(map(int, next(matchiter).groups()))
                # Construct the datetime and extract start and end from the frames array.
                dt = datetime(year=Y, month=m, day=d, hour=H, minute=M, second=S)
                s = dt + timedelta(seconds=frames[0])
                e = dt + timedelta(seconds=frames[-1])
                # Return start and end if they're not equal, otherwise start and None.
                return (s, e) if s != e else (s, None)
            except Exception as e: 
                pass
        raise Exception(f'Cannot format file date time {filename}.')

    def do_biigle_observations(self, cruise_import, dive, instrument_config, events_obj):
        """
        Record observations events derived from Biigle annotations. The raw Biigle video or photo
        report can be loaded.

        Labels must be mapped in the label mapper tool so that when the Biigle report is
        ingested, the mapped labels can be looked up and applied to the observations. An annotation
        can have more than one label. If there is a conflict between properties, an exception is raised.
        TODO: The latter is provisional.

        All observations are stored by a schemaless type, observation. The properties field
        is a JSON object (jsonb) type that stores name-value pairs. Anything can be an observation,
        a habitat, an organism, an anthropogenic object, a comment or a measurement.

        The work of distinguishing types of observations falls on the consumer of the object table, 
        so that the presence of a biocover or substrate, for example, signifies a habitat, and the
        presence of a scientific name signifies a species. But habitats can be species too, so one
        must be careful.

        :param dive: The dive. Used if the instrument config is not set.
        :param instrument_config: The instrument configuration. Used if the dive is not set.
        :param events_obj: The events file description.
        """
        from msea.models.shared import UploadedFile
        from msea.models.rov import AnnotationJob, Event
        from msea.models.rov.imports import BiigleLabelMap

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing Biigle observations for instrument configuration {instrument_config.id} on dive {dive.name}.')

        # Get the uploaded file object.
        try:
            upload = UploadedFile.objects.get(id=events_obj['file']['id'])
        except Exception as e:
            raise Exception(f'The uploaded file, {events_obj['file']['name']} ({events_obj['file']['path']}), was not found.')

        # Get the annotation job.
        try:
            annotation_job = AnnotationJob.objects.get(pk=events_obj['annotation_job']['id'])
        except: 
            annotation_job = None

        # Biigle video columns.
        video_cols = {
            'video_annotation_label_id': 'annotation_label_id',
            'label_id': 'label_id', 
            'video_filename': 'filename', 
            'shape_name': 'shape_name', 
            'points': 'points', 
            'frames': 'frames', 
            'annotation_id': 'annotation_id', 
            'created_at': 'created_at'
        }

        # Biigle photo columns.
        photo_cols = {
            'annotation_label_id': 'annotation_label_id',
            'label_id': 'label_id', 
            'filename': 'filename',
            'shape_name': 'shape_name', 
            'points': 'points', 
            'attributes': 'attributes', 
            'annotation_id': 'annotation_id', 
            'created_at': 'created_at'
        }
        
        # Label map object cache to avoid loading label maps on each row.
        label_map_cache = {}
        try:
            with open(os.path.join(self.upload_dir, upload.path), 'r', encoding='utf-8-sig') as f:
                db = csv.reader(f)
                
                # Check that the required columns are in the header.
                head = next(db)
                column_map = None
                try:
                    head = self.check_header(head, list(video_cols.keys()))
                    column_map = video_cols
                except:
                    head = self.check_header(head, list(photo_cols.keys()))
                    column_map = photo_cols

                # Observations are cached here as dictionaries so that more than one label can be
                # applied to an annotation. 
                obs_cache = {}

                for row_raw in db:
                    # Build the row with the column map.
                    row = self.map_columns(head, row_raw, column_map)
                    
                    # Extract the time range.
                    start_time, end_time = self.parse_biigle_time(row)

                    if not (self.in_time(dive, start_time) or (end_time != None and self.in_time(dive, end_time))):
                        # If the event is not in the dive's timeframe, skip it.
                        continue

                    # Extract some necessary IDs.
                    annotation_label_id = int(row['annotation_label_id'])
                    annotation_id = int(row['annotation_id'])
                    label_id = int(row['label_id'])

                    # Get the label map object and add it to the cache if it's not there.
                    label_map = label_map_cache.get(label_id)
                    if not label_map:
                        try:
                            label_map = label_map_cache[label_id] = BiigleLabelMap.objects.get(label_id=label_id)
                        except Exception as e:
                            raise Exception(f'No label map for label ID, {label_id}.')

                    # If it's set to one of the excluded types, skip it.
                    if label_map.properties['event_type'].lower() in ('ignore',):
                        continue
                    
                    obs = obs_cache.get(annotation_id)
                    if obs == None or obs['properties']['label_id'] > label_id:
                        # If the observation does not exist, or the label ID is smaller (signifying
                        # a higher-level label) (re-)create it.
                        obs = obs_cache[annotation_id] = {
                            'start_time': start_time,
                            'end_time': end_time,
                            'dive': dive, 
                            'annotation_job': annotation_job,
                            'instrument_config': instrument_config,
                            'properties': {
                                'annotation_id': annotation_id,
                                'label_id': label_id,
                                'biigle_rows': {}
                            }, 
                        }
                    else:
                        print('x')

                    # Preserve the original Biigle annotations.
                    obs['properties']['biigle_rows'][annotation_label_id] = dict(zip(head, row_raw))

                    # Apply the properties to the observation's properties.
                    obs_props = obs['properties']
                    for k, v in label_map.properties.items():
                        if not v:
                            continue
                        if k == 'tags':
                            tags = set(obs_props.get('tags', [])) | set(v)
                            obs_props['tags'] = list(tags)
                        elif obs_props.get(k):
                            raise Exception(f'The properties object already has an item for {k}.')
                        else:
                            obs['properties'][k] = v

                    self.fix_event_types(obs['properties'])

                for obs in obs_cache.values():
                    # Create the event. Can't bulk create because of the inheritance chain.
                    Event.objects.create(
                        start_time=obs['start_time'],
                        end_time=obs['end_time'],
                        dive=obs['dive'],
                        annotation_job=obs['annotation_job'],
                        instrument_config=obs['instrument_config'],
                        properties=obs['properties']
                    )
        except Exception as e:
            raise Exception(f'Error parsing observations in {upload.name} for dive {dive.name}: ' + str(e))
        
        
    def do_annotations(self, cruise_import, dive, instrument_config, events_obj):
        """
        Record events.
        :param dive: The dive.
        :param events_obj: The object containing the events file description.
        """
        if events_obj['annotation_type'] == 'comment':
            self.do_comments(cruise_import, dive, instrument_config, events_obj)
        elif events_obj['annotation_type'] == 'status':
            self.do_status(cruise_import, dive, instrument_config, events_obj)
        elif events_obj['annotation_type'] == 'generic':
            self.do_generic_observations(cruise_import, dive, instrument_config, events_obj)
        elif events_obj['annotation_type'] == 'biigle':
            self.do_biigle_observations(cruise_import, dive, instrument_config, events_obj)
        else:
            raise Exception(f"Invalid annotation type: {events_obj['annotation_type']}.")

    def do_comments(self, cruise_import, dive, instrument_config, events_obj):
        """
        Record comment events from an uploaded CSV file.
        :param dive: The dive. Used if the instrument config is not set.
        :param instrument_config: The instrument configuration. Used if the dive is not set.
        :param events_obj: The events file description.
        """
        from msea.models.shared import UploadedFile
        from msea.models.rov import Event

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing comments on dive {dive.name}.')

        # Get the uploaded file object.
        try:
            upload = UploadedFile.objects.get(id=events_obj['file']['id'])
        except Exception as e:
            raise Exception(f'The uploaded file, {events_obj['file']['name']} ({events_obj['file']['path']}), was not found.')

        # The list of column names.
        cols = ('start_time', 'end_time', 'original_id', 'note')

        # Cast and format columns.
        cast_map = [
            ('original_id', int, None), ('start_time', self.format_time, None), ('end_time', self.format_time, None)
        ]

        try:
            with open(os.path.join(self.upload_dir, upload.path), 'r', encoding='utf-8-sig') as f:
                db = csv.reader(f)

                # Check the header against the column list, retrieve the column indices.
                head = self.check_header(next(db), cols)
                idx = [(i, head.index(cols[i])) for i in range(len(cols))]

                for row in db:
                    row_ = dict(head, row)

                    # Get the properties object using the column names and indices and resolve casts.
                    props = {cols[i]: row[k] for i, k in idx}
                    self.resolve_casts(props, cast_map)

                    if not (self.in_time(dive, props['start_time']) or (props.get('end_time') and self.in_time(dive, props['end_time']))):
                        # If the event isn't within the dive's time, discard it.
                        continue

                    properties = {
                        'event_types': ['comment'],
                        'comment': props.pop('note'),
                        'original_id': props.pop('original_id'),
                        **{k:row_[k] for k in head if not k in cols}
                    }
                    self.fix_event_types(properties)
                    props['properties'] = properties

                    # Create the event.
                    Event.objects.create(
                        dive = dive,
                        instrument_config = instrument_config,
                        **props,
                    )
        except Exception as e:
            raise Exception(f'Error parsing comments in {upload.name} for dive {dive.name}: ' + str(e))

    def do_status(self, cruise_import, dive, instrument_config, events_obj):
        """
        Record status events from an uploaded CSV file. The status 
        :param dive: The dive. Used if the instrument config is not set.
        :param instrument_config: The instrument configuration. Used if the dive is not set.
        :param events_obj: The events file description.
        """
        from msea.models.shared import UploadedFile
        from msea.models.rov import Event, StatusTypeDetail

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing statuses on dive {dive.name}.')

        # Get the uploaded file object.
        try:
            upload = UploadedFile.objects.get(id=events_obj['file']['id'])
        except Exception as e:
            raise Exception(f'The uploaded file, {events_obj['file']['name']} ({events_obj['file']['path']}), was not found.')

        # The required columns list.
        cols = ('start_time', 'end_time', 'original_id', 'status_type_detail')

        # Cast the columns.
        cast_map = [
            ('original_id', int, None), ('start_time', self.format_time, None), ('end_time', self.format_time, None)
        ]

        # The status type lookup.
        class_map = [
            ('status_type_detail', StatusTypeDetail)
        ]

        try:
            # A cache for the StatusTypeDetail objects.
            with open(os.path.join(self.upload_dir, upload.path), 'r', encoding='utf-8-sig') as f:
                db = csv.reader(f)

                # Check the header columns and find the indices.
                head = self.check_header(next(db), cols)
                idx = [(i, head.index(cols[i])) for i in range(len(cols))]

                for row in db:

                    # Get the properties object using the column names and indices. Resolve lookups and casts.
                    props = {cols[i]: row[k] for i, k in idx}
                    self.resolve_lookups(props, class_map)
                    self.resolve_casts(props, cast_map)

                    if not (self.in_time(dive, props['start_time']) or (props.get('end_time') and self.in_time(dive, props['end_time']))):
                        # If the events aren't within the dive's start and end times, skip them.
                        continue

                    props['event_types'] = ['status']
                    self.fix_event_types(props)

                    # Create the event.
                    Event.objects.create(
                        dive = dive,
                        instrument_config = instrument_config,
                        **props,
                    )
        except Exception as e:
            raise Exception(f'Error parsing statuses in {upload.name} for dive {dive.name}: ' + str(e))

    def do_events(self, cruise_import, dive, events_obj):
        """
        Record events.
        :param dive: The dive.
        :param events_obj: The object containing the events file description.
        """
        if events_obj['event_type'] in ('comments', 'comment'):
            self.do_comments(cruise_import, dive, None, events_obj)
        elif events_obj['event_type'] == 'status':
            self.do_status(cruise_import, dive, None, events_obj)
        else:
            raise Exception(f"Invalid event type: {events_obj['event_type']}.")

    def do_instrument_configs(self, cruise_import, dive, platform_config, ic_objs):
        """
        Create or update the instrument configs. Do not delete configurations that already exist but aren't represented here.

        Instrument configs are matched by the referenced instrument.
        """
        from msea.models.rov import Instrument, InstrumentConfig, Event, Measurement, Position

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing an instrument configuration on platform configuration {platform_config.id}.')
        
        keep = []   # Store the IDs to keep.

        for ic_obj in ic_objs:
            cruise_import.log(f'Processing instrument configuration {ic_obj["id"]}')

            # Clean the properties, and retrieve linked entities.
            props = {k: self.clean(ic_obj.get(k)) for k in ('instrument', 'note', 'configuration')}
            props['instrument'] = Instrument.objects.get(id=props['instrument']['id'])

            try:
                # Retrieve and configure the instrument config.
                ic = InstrumentConfig.objects.get(
                    platform_config=platform_config,
                    instrument=props['instrument']
                )
                if ic_obj['doUpdate']:
                    ic.note = props['note']
                    ic.configuration = props['configuration']
                    ic.save()
                    cruise_import.log('Updated instrument configuration.')
                else:
                    cruise_import.log('Not updating instrument configuration.')
            except Exception as e:
                cruise_import.log(f'Instrument config not found. Creating a new one ({e}).')
                # Create the instrument config.
                ic = InstrumentConfig.objects.create(
                    platform_config=platform_config,
                    **props
                )
                cruise_import.log('Created a new instrument configuration.')

            # Record the ID to prevent deletion.
            keep.append(ic.id)

            # Delete existing data related to this configuration.
            if not ic_obj.get('keepEvents'):
                cruise_import.log('Deleting existing events for the instrument configuration.')
                Event.objects.filter(instrument_config=ic, dive=dive).delete()
            else:
                cruise_import.log('Keeping existing events for the instrument configuration.')
            if not ic_obj.get('keepPositions'):
                cruise_import.log('Deleting existing positions for the instrument configuration.')
                Position.objects.filter(instrument_config=ic).delete()
            else:
                cruise_import.log('Keeping existing positions for the instrument configuration.')
            if not ic_obj.get('keepMeasurements'):
                cruise_import.log('Deleting existing measurements for the instrument configuration.')
                Measurement.objects.filter(instrument_config=ic).delete()
            else:
                cruise_import.log('Keeping existing measurements for the instrument configuration.')

            if ic_obj.get('processEvents'):
                cruise_import.log('Processing events.')

                # Record the events.
                for evt_obj in ic_obj.get('events', []):
                    self.do_events(cruise_import, ic, evt_obj)
            else:
                cruise_import.log('Not processing events.')

            if ic_obj.get('processAnnotations'):
                cruise_import.log('Processing annotations.')

                # Record the annotations.
                for annot_obj in ic_obj.get('annotations', []):
                    self.do_annotations(cruise_import, dive, ic, annot_obj)
            else:
                cruise_import.log('Not processing annotations.')

            if ic_obj.get('processStream'):
                cruise_import.log('Processing streams.')

                # Record the streams.
                for stream_obj in ic_obj.get('streams', []):
                    self.do_streams(cruise_import, dive, ic, stream_obj)
            else:
                cruise_import.log('Not processing streams.')
    
    def do_platform_config(self, cruise_import, platform, platform_config, platform_config_obj):
        """
        Create or update the platform config. A dive will always have exactly two: one for the ship and one for the sub.
        The instrument configs are not automatically deleted from an existing configuration. 
        :param dive: The dive instance.
        :param platform_config_obj: The configuration information.
        """
        from msea.models.rov import Platform, PlatformConfig

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing platform configuration.')

        # Clean the properties and retrieve linked entities.
        props = {k: self.clean(platform_config_obj.get(k)) for k in ('configuration', 'note', 'platform')}
        if not props['platform']:
            props['platform'] = platform
        else:
            props['platform'] = Platform.objects.get(id=props['platform']['id'])

        if not platform_config:
            # If the config doesn't already exist, create it.
            platform_config = PlatformConfig.objects.create(**props)
            cruise_import.log('Created a new platform configuration.')
        else:
            if platform_config_obj['doUpdate']:
                for k, v in props.items():
                    setattr(platform_config, k, v)
                platform_config.save()
                cruise_import.log('Updated platform configuration.')
            else:
                cruise_import.log('Not updating platform configuration.')

        return platform_config

    def fix_configs(self, dive):
        """
        Older imports duplicated platform and instrument configs across dives. 
        This needs to be fixed by retrieving the platform configs shared between
        a dive and duplicating them, reassigning them to the other dives. The
        instrument configs must then be duplicated and reassigned to the platform
        configs.
        """
        from django.db.models import Q
        from msea.models.rov import Dive, InstrumentConfig, PlatformConfig

        def update_instrument_configs(dive, pconfig, dive_pconfigs):
            """
            For each instrument config in the template platform config,
            create a duplicate and assign all the events that correspond to
            the dives time range to it.

            Finally, filter the remaining entities on the original instrument
            config.
            """
            
            for iconfig in pconfig.instrument_configs.all():
                for dive_, pconfig_ in dive_pconfigs:
                    # Duplicate the IC, set ID to none to save new, and set the platform config. 
                    ic = InstrumentConfig.objects.get(pk=iconfig.id)
                    ic.id = None
                    ic.platform_config = pconfig_
                    ic.save()
                    # Reassign entities to the new IC.
                    ic.measurements.set(iconfig.measurements.filter(timestamp__gte=dive_.start_time, timestamp__lte=dive_.end_time))
                    ic.positions.set(iconfig.positions.filter(timestamp__gte=dive_.start_time, timestamp__lte=dive_.end_time))
                    ic.events.set(iconfig.events.filter(
                        Q(start_time__gte=dive_.start_time, start_time__lte=dive_.end_time)
                        | (~Q(end_time=None) & Q(end_time__gte=dive_.end_time, end_time__lte=dive_.end_time)))
                    )
                # Filter the remaining entities on the original config.
                iconfig.measurements.set(iconfig.measurements.filter(timestamp__gte=dive.start_time, timestamp__lte=dive.end_time))
                iconfig.positions.set(iconfig.positions.filter(timestamp__gte=dive.start_time, timestamp__lte=dive.end_time))
                iconfig.events.set(iconfig.events.filter(
                    Q(start_time__gte=dive.start_time, start_time__lte=dive.end_time)
                    | (~Q(end_time=None) & Q(end_time__gte=dive.end_time, end_time__lte=dive.end_time)))
                )

        def update_platform_config(dive, name):
            """
            Create distinct platform configs for dives that currently own
            the same one as the first dive. Only operates on dives that share
            configs. If they don't, there is no effect (which is what we want).
            """
            config = getattr(dive, name)
            configs = [(dive, config)]
            # Get the dives that are not the given dive, but have the same config.
            # If the dives have distinct configs, only no dives will be returned
            # (because the first dive owns the config and it is excluded.
            params = {name: config}
            dives = Dive.objects.filter(**params).exclude(id=dive.id)
            for dive_ in dives:
                config_ = PlatformConfig.objects.get(pk=config.id)
                config_.id = None
                config_.dive = dive_
                config_.save()
                setattr(dive_, name, config_)
                dive_.save()
                configs.append((dive_, config_))

            if len(dives):
                # Update the instrument configs on the list of new platform configs
                # but only if there were dives to update.
                update_instrument_configs(dive, config, configs)

        update_platform_config(dive, 'sub_config')
        update_platform_config(dive, 'ship_config')

    def do_dives(self, cruise_import, cruise, dive_objs):
        """
        Create or update the dives associated with the cruise. Existing dives are not deleted.
        :param cruise: The cruise.
        :param dive_objs: A list of dive configuration objects.
        """
        from msea.models.rov import Dive, Measurement, Position, InstrumentConfig, Event

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log('Processing dives.')

        # Format columns.
        cast_map = (('start_time', self.format_time, None), ('end_time', self.format_time, None))
        
        for dive_obj in dive_objs:
            cruise_import.log(f'Processing dive {dive_obj["name"]}.')
            
            # Clean the properties and resolve casts.
            props = {k: self.clean(dive_obj.get(k)) for k in ('name', 'start_time', 'end_time', 'objective', 'summary', 'note')}
            self.resolve_casts(props, cast_map)

            try:
                # Retrieve and update an existing dive.
                dive = Dive.objects.get(cruise=cruise, name__iexact=str(props['name']))

                # Fix duplicate configs resulting from broken imports.
                self.fix_configs(dive)

                # Configure the ship.
                props['ship_config'] = self.do_platform_config(cruise_import, cruise.ship, dive.ship_config, dive_obj['ship_config'])
                # Configure the sub.
                props['sub_config'] = self.do_platform_config(cruise_import, None, dive.sub_config, dive_obj['sub_config'])
                if dive_obj['doUpdate']:
                    # Set properties and save.
                    for k, v in props.items():
                        setattr(dive, k, v)
                    dive.save()
                    cruise_import.log('Updated existing dive.')
                else:
                    cruise_import.log('Not updating existing dive.')
            except Exception as e:
                # Configure the ship.
                props['ship_config'] = self.do_platform_config(cruise_import, cruise.ship, None, dive_obj['ship_config'])
                # Configure the sub.
                props['sub_config'] = self.do_platform_config(cruise_import, None, None, dive_obj['sub_config'])
                # Create a new dive.
                dive = Dive.objects.create(cruise=cruise, **props)
                cruise_import.log('Created new dive.')

            # Delete existing data related to this configuration.
            if not dive_obj.get('keepEvents'):
                cruise_import.log('Deleting existing events from dive.')
                Event.objects.filter(dive=dive).delete()
            else:
                cruise_import.log('Keeping existing events from dive.')

            # Configure the instrument configurations.
            self.do_instrument_configs(cruise_import, dive, dive.ship_config, dive_obj['ship_config']['instrument_configs'])

            # Configure the instrument configurations.
            self.do_instrument_configs(cruise_import, dive, dive.sub_config, dive_obj['sub_config']['instrument_configs'])

            # Attach the dive crew.
            self.do_dive_crew(cruise_import, dive, dive_obj)

            if dive_obj.get('processEvents'):
                cruise_import.log('Processing events.')
                # Do the dive events.
                for evt_obj in dive_obj['events']:
                    self.do_events(cruise_import, dive, evt_obj)
            else:
                cruise_import.log('Not processing events.')

            # Attach the transects.
            self.do_transects(cruise_import, dive, dive_obj['transects'])

    def do_cruise_documents(self, cruise_import, cruise, cruise_obj):
        """
        Add the documents to the cruise. If it's an existing cruise, do not delete
        the excluded documents. It is too difficult to determine which to keep based on file name alone.
        :param cruise: The cruise.
        :param doc_objs: A list of document objects.
        """
        from msea.models.rov import CruiseDocument
        from msea.models.shared import UploadedFile

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log('Processing documents for cruise.')

        doc_objs = cruise_obj.get('documents', [])

        if not cruise_obj.get('keepDocuments'):
            # Delete existing documents.
            cruise_import.log('Deleting existing documents for the cruise.')
            CruiseDocument.objects.filter(cruise=cruise).delete()
        else:
            cruise_import.log('Keeping existing documents for the cruise.')

        if cruise_obj.get('processDocuments'):
            for doc_obj in doc_objs:

                cruise_import.log(f'Processing document {doc_obj["title"]}.')

                # Clean the properties.
                props = {k: self.clean(doc_obj.get(k)) for k in ('title', 'note', 'url')}

                # Get the uploaded file.
                if doc_obj.get('file'):
                    file_obj = UploadedFile.objects.get(pk=doc_obj['file']['id'])
                else:
                    file_obj = None

                # Create the document.
                CruiseDocument.objects.create(
                    cruise=cruise,
                    file=file_obj,
                    title=props['title'],
                    note=props['note'],
                    url=props['url']
                )
                cruise_import.log('Created document.')
        else:
            cruise_import.log('Not processing documents.')

    def do_cruise(self, cruise_import, cruise_obj):
        """
        Check for the existence of the cruise, then update or create it. Then handle the subsidiary entities.
        :param cruise_obj: An object containing cruise information.
        """
        from msea.models.rov import Cruise, Platform

        if cruise_import.is_canceled():
            raise CruiseImportCanceled()

        cruise_import.log(f'Processing cruise {cruise_obj["name"]}, {cruise_obj["leg"]}.')

        # Format the time columns.
        cast_map = (('start_time', self.format_date, None), ('end_time', self.format_date, None))

        # Clean the props object and resolve casts and lookups.
        props = {k: self.clean(cruise_obj.get(k)) for k in ('name', 'leg', 'start_time', 'end_time', 'objective', 'summary', 'note', 'ship')}
        self.resolve_casts(props, cast_map)
        props['ship'] = Platform.objects.get(id=props['ship']['id'])

        # Flag to indicate that the existing cruise was updated, even if doUpdate is not set.
        try:
            # Try to retrieve and update the cruise.
            cruise = Cruise.objects.get(name__iexact=str(props['name']), leg=props['leg'])
            if cruise_obj['doUpdate']:
                # If cruise is set to save, update it.
                for k, v in props.items():
                    setattr(cruise, k, v)
                cruise.save()
                cruise_import.log('Updated existing cruise.')
            else:
                cruise_import.log('Not updating cruise.')
        except:
            # Create the cruise.
            cruise = Cruise.objects.create(**props)
            cruise_import.log('Created new cruise.')

        # Populate the crew.
        self.do_cruise_crew(cruise_import, cruise, cruise_obj)

        # Attach the upload documents.
        self.do_cruise_documents(cruise_import, cruise, cruise_obj)

        # Populate the dives.
        self.do_dives(cruise_import, cruise, cruise_obj['dives'])


class CruiseImporterRunner:

    def __init__(self):
        self.versions = {
            1: CruiseImporterV1
        }

    def initialize(self, filename):
        """
        Load an environment variables file and initialize Django.
        """
        # Configure the Django settings module.
        os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'

        with open(filename, 'r') as f:
            line = f.readline()
            while line:
                try:
                    k, v = line.strip().split('=')
                    os.environ[k] = v
                except: pass
                line = f.readline()

        # Import and setup Django. Must happen before model classes are imported.
        import django

        django.setup()

    def run_queue(self, env_file):
        """
        Load the queue of incomplete, valid jobs and execute the first.
        """
        root_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        self.initialize(os.path.normpath(os.path.join(root_path, env_file)))

        from msea.models.rov.imports import CruiseImport

        # Turn off debug to prevent SQL output.
        django.conf.settings.DEBUG = True

        while True:
            try:
                # Select the first import that has not been started.
                cruise_import = CruiseImport.objects.filter(status=CruiseImport.NOT_STARTED).order_by('updated_on').first()
                if not cruise_import:
                    break
                try:
                    version = cruise_import.version
                    cls = self.versions.get(version)
                    if not cls:
                        raise Exception(f'No importer class is available for version {version}.')
                    # Start the import.
                    cls().import_cruise(cruise_import)
                except CruiseImportCanceled as c:
                    cruise_import.log('Import canceled.')
                except Exception as e:
                    # Log the failure.
                    cruise_import.log(str(e), level=CruiseImport.ERROR)
                    cruise_import.set_failed()
            except Exception as e:
                print(e)
                time.sleep(1)

def run(env_file):
    """
    This method allows the import to be run from the runner script (runner.runner).
    """
    print('Importing cruises.')
    CruiseImporterRunner().run_queue(env_file)

if __name__ == '__main__':

    env_file = sys.argv[1]
    run(env_file)
