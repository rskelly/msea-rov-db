#!/bin/bash

cd ..
docker build -t msea:rov_app_base -f docker/docker/Dockerfile_app_base .

# Build and run the dev images/containers.
cd docker
docker compose -f docker-compose_dev.yaml up rov_db -d --build --remove-orphans
docker compose -f docker-compose_dev.yaml up rov_app -d --build --remove-orphans --force-recreate
docker compose -f docker-compose_dev.yaml up rov_runner -d --build --remove-orphans --force-recreate
docker compose -f docker-compose_dev.yaml up rov_proxy -d --build --remove-orphans
