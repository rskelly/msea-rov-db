# Initializing the PostgreSQL Database for Development

The PostgreSQL/PostGIS database is based on the [postgis/postgis](https://github.com/postgis/docker-postgis) image which is in turn based on [postgres](https://hub.docker.com/_/postgres/).

The initialization works by copying SQL file into the `/docker-entrypoint-initdb.d/` in the image which can be mapped from the local folder in the `volumes` section of the compose file. In this instance, `dump.sql` (which is a downloaded backup of the online database) is mapped to `/docker-entrypoint-initdb.d/100_dump.sql`. The 100 ensures that the dump file is executed after all other initializations have occurred.

The `postgres` initialization routine will automatically create the database and admin user based on the configuration in `configs/.env_rov`, so the `drop`m `create` and `created role` (for `msea_admin`) lines should be removed from the dump file. However, the `msea` user is not created in the dump file, nor in the initialization process, so it must be created by adding a `create role` command to the dump file.

Note that if any of the database files persist on disk when the `rov_db` image is deleted and recreated, the database will not initialize. All of the extant PostgreSQL files must be deleted. Of course this destroys any data that have been added.

When a dump is loaded into the dev database, the database version in `shared.db_version` should be updated to the same version as the baseline DDL file (the version is in the filename). This allows the upgrade to proceed from that point. Note that if the dump that is loaded, is not structurally identical to the baseline, manual edits must be made before upgrades can be run, so don't do that.

# Production Database

Currently, the live database is not containerized.