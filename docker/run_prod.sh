#!/bin/bash

pushd ../app/vue
npm run build
popd

cd ..
docker build -t msea:rov_app_base -f docker/docker/Dockerfile_app_base .

# Build and run the dev images/containers.
cd docker
docker compose -f docker-compose_prod.yaml up -d --build --force-recreate

