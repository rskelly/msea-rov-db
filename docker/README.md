# Docker

This folder contains scripts and configurations for building and deploying containerized applications
for development and production. The main difference between the two is that, in production, the PostgreSQL
database runs on the server itself, and in development it runs in a container.

The development environment can be built using,

    ./run_dev.sh

This depends on Docker Desktop (on Windows). The production version can be built and deployed using,

    ./run_prod.sh

*Note: the Django app does not run in development until the debugger is attached. Occasionally, the debugger
refuses to attach unless you restart `rov_app` or even Docker Desktop itself. Why this occurs isn't known.