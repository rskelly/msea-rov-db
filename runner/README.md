# runner

This is a simple python script that runs in a container performing tasks periodically.

It is very simple: the loop runs once per second, calling the `run()` method on each task,
which calls the `run(*args)` method on a configured module, passing the configured arguments.
Each task has a delay. If the delay has not expired, the `run()` method skips and waits
for the next loop. If it has expired, it executes the task. If it fails, it keeps running.

The `config.json` file contains configurations for the runner.

This is a cheap substitute for `cron` but it's convenient because it can be deployed with
the other containers, and it restarts automatically on failure.