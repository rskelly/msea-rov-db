#!/usr/bin/env

"""
Commands are loaded from a config file. The module is a python module which
must have a method called run() that accepts arguments. The args
property is an array of arguments passed to run. Delay is the number of seconds
between runs. The script is not run right away, but after delay seconds have passed.
[
    {
        "delay": 0,
        "module": "...",
        "args": [...],
    }
]
"""

import sys
import os
import json
from datetime import datetime, timedelta
from time import sleep
from importlib import import_module
from util import initialize
import debugpy

# The root directory is added as a search path for modules.
sys.path.append(os.path.realpath(os.path.dirname(os.path.dirname(__file__))))

# Get the config file from the argument.
config_file = sys.argv[1]

initialize(config_file)

class task:
    """
    Maintains configuration and state for each task.
    """

    # The global task ID. Incremented on each construction.
    task_id = 0

    def next_id():
        """
        Increment the task ID and return.
        """
        task.task_id += 1
        return task.task_id
    
    def __init__(self, delay, module, args):
        """
        Construct a task. 
        :param delay: The number of seconds between runs.
        :param module: The python module containing the run method.
        :param args: An array of arguments for the run method.
        """
        self.delay = timedelta(seconds=float(delay))
        self.module = module
        self.args = args
        self.running = False
        self.task_id = task.next_id()
        self.next_run = datetime.now() + self.delay

    def run(self):
        """
        Run the task if it is time, and if it is not already running.
        """
        dt = datetime.now()
        if dt > self.next_run:
            self.next_run = dt + self.delay
            if not self.running:
                self.running = True
                try:
                    mod = import_module(self.module)
                    mod.run(*self.args)
                except Exception as e:
                    print(e)
                self.running = False
            else:
                print('Already running.')


if os.environ['DEBUG'] == 'True':
    debugpy.listen(5679)
    print("Waiting for debugger attach in runner.")
    debugpy.wait_for_client()


# Load the configuration and initialize tasks.
tasks = []
try:
    with open(config_file, 'r') as f:
        config = json.load(f)
        for c in config:
            if c['run']:
                del c['run']
                tasks.append(task(**c))
except Exception as e:
    print(e)

# Run tasks.
while True:
        
    # Run the tasks.
    for t in tasks:
        try:
            t.run()
        except Exception as e:
            print(e)
    sleep(1)


