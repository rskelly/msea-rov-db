#!/usr/bin/env python

"""
Delete files that need deleting.
"""
import os
import stat
import time
from datetime import datetime, timedelta

# Get the upload dir for temporary uploaded files.
upload_dir = os.environ['MSEA_UPLOAD_DIR']

# Length of time a file is allowed to live.
# expired = timedelta(days=1)


def file_age(filename):
    """
    Get the file's age in seconds.
    :param filename: The full path to the file.
    :return: The file's age in seconds.
    """
    return time.time() - os.stat(filename)[stat.ST_MTIME]

def can_delete(filename):
    """
    If the path points to a file and it is more than a minute old, 
    the given file can be deleted.
    """
    return os.path.isfile(filename) # and (file_age(filename) >= expired.total_seconds())

def run():
    print('Cleaning up uploads.')

    from msea.models.shared import UploadedFile

    # Get the current list of files can be deleted (see can_delete).
    uploaded_files = set([f for f in os.listdir(upload_dir) if can_delete(os.path.join(upload_dir, f))])

    # A list of UploadedFiles that can be deleted.
    entities = []

    # Iterate over the list of files in the DB.
    for uploaded_file in UploadedFile.objects.all():
        if uploaded_file.path in uploaded_files:
            # If it's in the list, remove it.
            uploaded_files.remove(uploaded_file.path)
        if uploaded_file.marked_for_delete:
            # If it's expired or marked for delete, add it.
            uploaded_files.add(uploaded_file.path)
            entities.append(uploaded_file.id)
    
    # Delete the files.
    print('Cleaning up', len(uploaded_files), 'files.')
    UploadedFile.objects.filter(id__in=entities).delete()
    for uploaded_file in uploaded_files:
        try:
            os.unlink(os.path.join(upload_dir, uploaded_file))
        except: pass



