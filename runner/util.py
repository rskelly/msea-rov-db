
import os
import django

def initialize(filename):
    """
    Load an environment variables file and initialize Django.
    """
    with open(filename, 'r') as f:
        line = f.readline()
        while line:
            try:
                k, v = line.strip().split('=')
                os.environ[k] = v
            except: pass
            line = f.readline()

    django.setup()
