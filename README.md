# MSEA Imagery Annotation Database

## Objective

To develop a data model and associated applications for storing data related to ROV groundfish surveys, including but 
not limited to,

1. an equipment inventory, including instruments and platforms;
2. trips/cruises, personnel, scientific programs and objectives;
3. a record of the configurations and settings of instruments and platforms used for individual dives and transects;
4. 'raw' data produced by environmental and position sensors, video and still cameras;
5. human-generated observations based on video or photographic output (e.g., using Biigle);

Ultimately, the database will hold both historical and new data, and it will be possible to reconstruct historical data 
formats and new products using SQL queries.

## Project Structure

The project consists of four main sections:

* [administration](./administration) - Scripts for compiling and deploying the site and configuring the server.
* [app](./app) - A Django app containing the site and a customized administration interface.
* [app/vue](./app/vue) - A VueJS single-page client application that interfaces with the Django app via ReST services.
* [docker](./docker) - Docker scripts and configurations for building and deploying the applications for both development and production. Note: In development, the database is containerized while in production it runs on bare metal, serving this and other applications.
* [importer](./importer) - Scripts and resources for importing data into the database.
* [model](./model) - The database structure represented in data definition (DDL) files and an upgrade script.
* [runner](./runner) - A containerized, Python-based `cron` substitute that runs important background services.

## The Data Model

The data model is defined using cumulative data-definition language (DDL) scripts applied by the `db_manager.py` script.

The model is meant to be portable and client-agnostic. Whereas entities are usually declared as `managed` models in an 
object-relational system or ORM (such as that provided by Django), here the entities are declared by the data model 
and Django interfaces with them in `unmanaged` mode. This allows the evolution of the database apart from any client 
software -- Django was being used for prototyping, but it has become entrenched; the database itself can stand alone.

There's a constantly-updated web interface and Wiki at [http://msea.science/wiki/](http://msea.science/wiki/).

The data model and scripts are located [here](./model).

## Data Import

New and historical data are imported into the database using a special import script and structured import data files.

More information is available [here](./importer).

## Participants

Rob Skelly <Robert.Skelly@dfo-mpo.gc.ca> - Developer

Jessica Nephin <Jessica.Nephin@dfo-mpo.gc.ca> - Scientist

Tammy Norgar <Tammy.Norgar@dfo-mpo.gc.ca> - Supervisor
