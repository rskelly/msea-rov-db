# Server Setup

## Server

The database is hosted on a Debian Linux server. The database is PostgreSQL with the PostGIS spatial extension, and the Web server is written in Python using the Django framework. The server is currently hosted on Linode, but these instructions hold for pretty much any Debian (or similar Linux) on any server.

## Python

Python3 is used, with `venv` for managing a "virtual environment". This allows the maintenance of a set of libraries and configurations separate from the operating system's configuration (the operating system uses Python extensively and modifying its Python install is risky). Python is first installed using the operating system's default package manager:

    $ sudo apt install python3 python3-venv

Then, initialize locally from within the repository folder. Assuming the user is currently `msea` and in the home directory (denoted by `~`), with the `msea-rov-db` repository cloned into the `git` folder:

    $ cd ~/git/msea-rov-db
    $ python3 -m venv .venv

This creates a folder called `.venv` in the repository root. Each time a command line is started, The environment must be started by invoking,

    $ source .venv/bin/activate

At this point, the command line will change to reflect that the environment is active:

    (.venv) msea@localhost:~/git/msea-rov-db$

Note that the `.venv` files is explicitly ignored in the `git` configuration, so it will not be included in the respository. This should never be modified.

## Required Libraries

With the venv environment running, the required Python libraries can be installed by invoking,

    $ pip install -r requirements_debian.txt

on Debian, or 

    $ pip install -r requirements_windows.txt

on Windows.

It does occassionally happen that development on Windows and deployment on Linux cause some mismatches in versions of libraries. It may be necessary to occasionally tweak the library versions, or install native libraries using the system's package manager.

A convenient way to update all of the Python packages using `pip` is to run the script below, shamelessly stolen from [here](https://stackoverflow.com/a/5839291/1050386):

    import pkg_resources
    from subprocess import call
     
    packages = [dist.project_name for dist in pkg_resources.working_set]
    call("pip install --upgrade " + ' '.join(packages), shell=True)

The `OpenCV` library will likely require the installation of `libGL`, which comes with:

    $ sudo apt install libgl1-mesa-glx

`GDAL` is frequently a challenge. It can be made to work by installing the libraries using the system package manager, then checking the version and installing the correpsonding Python version. So, first install and print the `GDAL` version:

    $ sudo apt install libgdal-dev gdal-bin
    $ gdalinfo --version

This might print out something like '3.2.2'. You can then install the closest Python version through `pip`:

    $ pip install "gdal==3.2.2.10"

## WSGI

`uwsgi` is a Python implementation of the WSGI standard, which defines a service for proxying traffic from a front-end webserver, through to a back-end server. In this case, the front-end server is Nginx and the back-end is Python and Django. `uwsgi` is installed with the Python libraries above, and configured by files in the `app/` folder.

When the respository is first checked out, or the WSGI configuration is modified it must by (re-)installed by invoking the `uwsgi_setup.sh` script (only on Linux). This script does several things:

config files need to be in www-data's home dir which is /var/www

