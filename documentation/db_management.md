# The Database Bible

## Server Installation

The database is hosted on a Debian (Linux) server with the database and PostGIS extension installed using the defaul package manager (apt). The packages are installed using,

     $ sudo apt install -y postgresql postgis

## Server Configuration

The database server must be configured to accept user logins. This is done in the pg_hba.conf file. For the MSEA database, we need two users, `msea` for regular access and `msea_admin` for maintenance. Each of these will be able to log in from remote locations (i.e., their desktop machines). `msea` will be able to access the `msea` and `msea_dev` databases (with different permissions on different relations, TBD) and `msea_admin` will have superuser access to all databases.

The users configured as follows:

    # IPv4 local connections:
    host    msea        msea        0.0.0.0/0    md5
    host    msea_dev    msea        0.0.0.0/0    md5
    host    all         msea_admin  0.0.0.0/0    md5

The columns are, the context, the database, the user, the (foreign) address and the authentication method. Here, user `msea` is given the right to connect to `msea` and `msea_dev` from any address with a password encoded using the `md5` hashing algorithm. The user `msea_admin` can access any database. The IPs from which users can connect are limited, but not here; this is done by the machine's firewall.

The users are acutally created from the command line using the `createuser` tool:

    $ sudo -u postgres createuser -s -P msea_admin
    $ sudo -u postgres createuser -P msea

Here, the `sudo -u postgres` causes the following command to be executed using the `postgres` user. The `-s` switch signifies that `msea_admin` is a superuser and can create or drop databases, modify the structure, create users, grant or revoke privileges, etc. The `-P` switch triggers a request for the new user's password.

Finally, the database is configured to accept connections from all addresses to a specific port. Because DFO limits traffic to standard HTTP ports (80, 8080, 8000, etc.) and blocks traffic to the default PostgreSQL port (5432), we set it to listen on 8000. This is a hack, and we don't like it.

In the `postgresql.conf` file, the following configuration values are set:

    listen_addresses = '*'
    port = 8000

After all this, the server is restarted:

    $ sudo service postgresql restart

## Firewall Configuration

When users connect to the database from a script or SQL Management Studio (etc.) they will specify the domain name (currently msea.science) and the port (8000). The server's firewall must be configured to allow connections from within the DFO subnet and any other subnets where DFO users will connect from. This is done using the `ufw` tool. First, install it:

    $ sudo apt install -y ufw

Then, we open a port to the subnet:

    $ sudo ufw allow proto tcp from 205.193.0.0/16 to any port 8000
    $ sudo ufw enable

Now, users on the subnet 205.193.0.0 have access to TCP port 8000 and can access the database server. 

## Background

There are three databases.

 - `msea` - The main database, with read-only access for everyone and write access for selected users. Accessed by the Web application and ReST endpoints.
 - `msea_dev` - The development database. Created locally on development machines for development. 
 - `msea_stage` - Used for testing modifications to the database structure or data before going live.

The database structure is applied by the `db_manager.py` script located in the `model` folder of the repository. `db_manager.py` reads the DDL scripts in the `model/ddl` folder, each of which has the database version embedded in its file name, and applies them sequentially to the database, beginning with the first version after the database's version and ending with either the latest version or the final one specified by the user. Database versions are specified in the format `major.minor.revision`. Note that the version string is not a floating-point number -- `1.10.0` is later than `1.2.0`. 

The DDL filename format is, 

    [major]_[minor]_[revision]_[order]_[extra].sql

`major`, `minor` and `revision` specify the version, `order` specifies the order in which multiple files with the same version should be applied, and `extra` is a string that hints at the purpose of the file (e.g. `views` hints that the file defines a set of database views).

The first script in the `ddl` folder, `db_1_0_0_0_baseline.sql`, is the "baseline" database definition file. This creates the original database structure to which all upgrades are applied. Upgrade scripts that apply structural changes *must also apply data transformations*. These may not be possible if the new structure requires missing information (such as missing references or disallowed null values), and will usually not be reversible. 

*It's is imperative that a backup is performed before the upgrade*.

To create the database from scratch, use the following procedure:

1. `$ createdb -U msea_admin -T template0 -E 'UTF8' msea_dev`
2. `$ python db_manager.py db_dev.conf`

The first command creates a database,'msea_dev' using the `msea_admin` user, with the PostgreSQL command-line tool, `createdb`. The `-T template0` and `-E 'UTF8'` argument create a database with the UTF-8 character encoding. The second line creates and updates the database to the latest version using the `db_manager.py` tool and the given credential file. The credential file contains the following:

    dbname:msea_dev
    host:localhost
    user:msea_admin
    password:******
    port:8000
    ddl_dir:ddl

The final parameter, `ddl_dir`, is the directory, relative to the `model` directory, where the DDL files are stored.

Upgrades to the database require only step 2.

From this point, the data can be loaded into the newly-created database. This is done using the `psql` tool:

- `$ psql -U msea_admin -d msea_dev -f '../backups/db_20020202_data.sql'`

This command simply loads the data contained in the `sql` file. It must have a compatible structure with the new database.

## Permissions

msea_admin -- all
msea -- select plus?

    grant usage on schema rov to msea;
    grant select on all tables in schema rov to msea;

etc.

## Backups

A backup is run on the database each night using a python script (`utilities\db_backup.py`) and a `cron` job.

The python script does two important things:

1. Run a backup of the `msea` database using the `pg_dump` tool.
2. Checks the `md5` checksum of the previous backups and removes those that match the new one.

Step 2 causes identical backups to be removed. If the database is backed up each evening and new data is added relatively rarely, disk space is wasted on identical backups.

The `cron` job is run under the `msea` user and backups are stored in `/home/msea/backups`.

In order to run `pg_dump` without supplying a password, the user who runs the backup script must have a file named `.pgpass` in their home directory, in the format, 

    host:port:database:username:password

The permissions on the file must be set to `0600`. The file for the msea database is,

    *:*:msea:msea_admin:password

Here, the `*` entries signify a match to any host and any port, and the 'password' string is replaced with the real password.

`cron` is a standard Linux program that executes programs on a schedule. For the DB backup it is set to run the backup script at midnight, local time, each day (the server is located in Toronto). Execute `crontab -e` to open the configuration for the current user, `msea`.

The relevant lines are:

    # m h  dom mon dow   command
    0 1 * * * /home/msea/git/msea-rov-db/utilities/db_backup.py

The `m`, `h`, `dom`, `mon`, `dow` columns are minute, hour, day-of-month, month, and day-of-week, respectively. The backup configuration is set to run at 1AM (1:00) on every day of every month.

## Replication

This is a future thing.

## Utilities

### Refresh Materialized Views

There are several *materialized views* in the database, which are pre-defined queries 'materialized' as tables. They need to be refreshed when new data are added. This is a very time-consuming activity on views that use time-proximity matching to create rows. The `rov.obs_evt_pos` view does this by matching the times of observation events to those of ROV positions.

The `db_refresh_views.py` script in the `utilities` folder performs this action on all the materialized views. It is set to run in a cron job before the backup.
