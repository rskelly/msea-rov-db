# Database Initialization

The database's structure (the *schema*) works together with application programming logic to provide data and services to users. The database schema evolves along with the application code. Changes to the DB can break the application code and vice versa. Versioning the database and code together makes it easier to keep things working as the database evolves, and to roll back breaking changes when necessary.

The process for versioning a database isn't "solved" to the degree that code versioning is, partly because the database is full of data, and both the structure *and* data have to be upgraded when anything changes. For example, if you have two tables joined one-to-one using a foreign key and you want to refactor the structure to allow many-to-one joins, you must change the structure, move the keys into the new table *and* create references from the old table to the new join table. And the data are changing all the time.

A reasonable way to do this is to have a "baseline" definition of the database and apply changes, or migrations, to the database as it evolves. Each migration is a patch that creates new required structures, copies data, then deletes the old structures. With this strategy, a "rollback" requires the developer to restore the baseline database and rebuild it to the last good version. A more sophisticated strategy would include migrations to go both forward and backwards.

## The Baseline

The database is designed using a commercial program called [Visual Paradigm](https://www.visual-paradigm.com/). It allows the user to graphically design entities and relationships. VP can automatically update the database schema, and can reverse-engineer the live database structure to update the ERD. MSEA has a perpetual one-seat license for the standard edition of VS.

Another tool that comes in handy is [pgAdmin](https://www.pgadmin.org/). This (free) tool allows the user to explore and control databases, but also to perform "diffs" or differencing between databases. This means that when you make a change to the local development database using Visual Paradigm, you can "diff" the change with an older copy of the database and generate a patch to update the structure. This patch can be modified with `insert` and `update` statements and checked into version control. This comes in handy when changes are made using VS -- perform the changes, generate the diff, *test it* and then check it in.

The baseline data definition is in SQL form and was generated from the development database using pgAdmin. When a new instance of the database is created, this baseline definition is installed and upgrades performed from there.

## Creating the Database

First a database user must be created using the PostgreSQL user-creation program, `createuser`:

    createuser -U [user] -s -W msea_admin

This creates a user, `msea_admin` under the authority of the current database superuser (`-U [user]`). The `-s` switch designates the user as a superuser. This command is *only* for admins. Regular users should get an account with degraded access privileges on specific relations.

The `-W` switch will cause a password prompt, where you will enter the new user's password.

The database is initially created from the command line using the following command:

    createdb -U msea_admin -T template0 -E 'UTF8' msea_dev

This calls the PostgreSQL `createdb` command under the authority of the user `msea_admin`. The `-T` switch tells `createdb` to use `template0`, which allows the use of the UTF-8 encoding (the default `template` does not) enabled by switch, `-E`. The last argument, `msea_dev` is the name of the database. UTF-8 encoding will help to handle some of the weird characters that show up in imported data, or characters from languages other than English.

This done, the baseline copy of the database is installed by navigating to the `model` folder of the source repository and invoking,

    python db_manager.py <configuration file>

The configuration file contains DB connection parameters, and the location of the database creation scripts. An example is printed here:

    dbname:msea_dev
    host:localhost
    user:msea_admin
    password:*****
    port:5432
    ddl_dir:ddl

When this script is run, it looks for the latest version of the database schema in the `shared.db_version` table. If the table doesn't exist or is empty, the version 0.0 is returned. Then it looks through the folder of database creation scripts and sorts them according to version. The version is stored in the file name.

    db_[major]_[minor]_[revision]_[sort]_[extra].sql

The `major`, `minor` and `revision` parts of the file name are integers denoting the file's version (these are named sequentially from 1.0.0). Note that the version isn't a rational number. N.B.: each part is a distinct integer, so 1.10.0 is later than 1.9.0. The `sort` part is required, and is used to sort files that belong to the same version but must be executed in sequence. The `extra` part is just the remainder of the file's name, and may be used to hint at the purpose or contents of the file.

The upgrade script looks through the list of available files, keeps those that are greater to or equal to the target version (or the latest version) and executes them in order. When the script is finished, it updates the version table. If any part of the upgrade to each version fails, no changes are made to the database beyond the previous successfully-updated version. A total roll-back isn't possible at this time because the changes must be committed for each version, and reverse migration isn't implemented.

In a perfect world, these updates will be performed seamlessly and data will never be corrupted. This should be tested rigorously on the developer's machine and on a staging database before being made public. 

## Initial Data

Some data must exist in the database before other data can be loaded. This data is stored in files called "fixtures." The [Django](https://www.djangoproject.com/) framework, upon which the website is built, provides a facility for dumping and loading this data through the `dumpdata` and `loaddata` commands. The built-in commands need a little help though. A custom script in the `utilities` folder helps with the process:

<ol>
    <li>Dump the persistent data to a fixture by running,
        
        <pre><code>python utilities/generate_fixtures.py &lt;fixture file&gt;</code></pre>
        
        This script runs Django's built-in `dumpdata` program, but only on models marked with the non-standard property, `__permanent = True`. It runs a dependency sort on marked entities to ensure that they are loaded in the correct order.
    </li>

    <li>Drop, recreate and update the database to the correct version as described above.</li>

    <li>Migrate the database from Django using `python manage.py migrate`. This doesn't touch the data tables (which are "unmanaged" to keep the DB model separate from the application code), but it does generate Django-specific tables used for user management and authentication.</li>
    
    <li>Load the fixtures using,
        <pre><code>python app/manage.py loaddata <fixture file></code></pre>
    </li>

    <li>Import observation (etc.) data.</li>
</ol>

## Next

With the database initialized, data can now be imported, using the inport script, `import/import.py` or direct ingestion from Biigle.de, etc.
