-- Delete unneeded schema.
drop schema iform cascade;
drop schema pa cascade;

drop table if exists nyt_notification cascade;
drop table if exists nyt_notificationtype cascade;
drop table if exists nyt_settings cascade;
drop table if exists nyt_subscription cascade;