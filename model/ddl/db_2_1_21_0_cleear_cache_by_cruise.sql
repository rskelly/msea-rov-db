-- Clear cache entities by cruise ID.

create or replace procedure cache_clear_cruise(c_id integer)
as $$
    begin
        delete from cache.dive_track where cruise_id=c_id;
        delete from cache.transect_track where cruise_id=c_id;
        delete from cache.cruise_track where cruise_id=c_id;
        delete from cache.evt_pos where event_id in (
            select e.id 
            from rov.event e 
                inner join rov.dive d on d.id=e.dive_id
            where d.cruise_id=c_id
        );
        delete from cache.evt_depth where event_id in (
            select e.id 
            from rov.event e 
                inner join rov.dive d on d.id=e.dive_id
            where d.cruise_id=c_id
        );
        delete from cache.evt_measurement where event_id in (
            select e.id 
            from rov.event e 
                inner join rov.dive d on d.id=e.dive_id
            where d.cruise_id=c_id
        );
    end;
$$ language 'plpgsql';

comment on procedure cache_clear_cruise is 'Clear cached entities associated with the given cruise.';
