-- Drop short code restriction.
--alter table shared.restriction alter column short_code drop not null;

alter table shared.taxon drop column if exists hart_code;
alter table shared.taxon add column hart_code varchar(8);
update shared.taxon a set hart_code=b.species_code from taxonomy.hart_taxon b where a.hart_id=b.id;
alter table shared.taxon drop column hart_id;

alter table shared.taxon drop constraint if exists f_taxon_worms;
alter table shared.taxon drop constraint if exists fk_taxon_hart_taxon;
alter table shared.taxon drop constraint if exists fk_taxon_inaturalist_taxon_id;

drop table taxonomy.worms_taxon;
drop table taxonomy.inaturalist_taxon;
drop table taxonomy.hart_taxon;
drop table taxonomy.taxonomic_grouping;
drop table taxonomy.taxonomic_rank;

alter table taxonomy.taxon add column no_common_name boolean default 'f';

comment on column taxonomy.taxon.no_common_name is 'If a search has been performed for a common name and none was found, this prevents the processor from trying again.';

alter table taxonomy.taxon alter column scientific_name drop not null;
alter table taxonomy.taxon add constraint chk_scientific_common_name check(not (common_name is null and scientific_name is null));