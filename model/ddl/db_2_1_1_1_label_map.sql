--
-- Name: biigle_label_map; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.biigle_label_map (
    id serial primary key NOT NULL,
    label_tree_id integer NOT NULL,
    label_id integer NOT NULL,
    label_hierarchy text NOT NULL,
    label_text text NOT NULL,
    properties jsonb DEFAULT '{}'::jsonb NOT NULL,
    note text,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL
);

COMMENT ON TABLE rov.biigle_label_map IS 'Contains mappings from Biigle labels to properties that apply to events.';
COMMENT ON COLUMN rov.biigle_label_map.label_tree_id IS 'The ID of the Biigle label tree.';
COMMENT ON COLUMN rov.biigle_label_map.label_id IS 'The ID of the Biigle label.';
COMMENT ON COLUMN rov.biigle_label_map.label_hierarchy IS 'The full hierarchical text of the Biigle label.';
COMMENT ON COLUMN rov.biigle_label_map.label_text IS 'The text of the final element of the label.';
COMMENT ON COLUMN rov.biigle_label_map.properties IS 'Stores the properties of the observation.';
COMMENT ON COLUMN rov.biigle_label_map.note IS 'A textual note or comment.';
COMMENT ON COLUMN rov.biigle_label_map.created_on IS 'The time of creation of the record.';
COMMENT ON COLUMN rov.biigle_label_map.updated_on IS 'Time of last update of the record.';

CREATE TRIGGER biigle_label_map_updated_on BEFORE UPDATE ON rov.biigle_label_map FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


