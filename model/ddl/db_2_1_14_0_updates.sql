-- Sites should have unique names.
create unique index if not exists site_name_unique on shared.site (lower(name));

-- Cruise program join table should be unique.
create unique index if not exists cruise_program_unique on rov.cruise_program (cruise_id, program_id);

-- People's emails and names should be unique (we cannot have the same person with different emails).
create unique index if not exists person_email_unique on shared.person((lower(email)));
create unique index if not exists person_name_unique on shared.person((lower(first_name)), (lower(last_name)));

-- Only one BiigleLabelMap per label ID.
create unique index if not exists biigle_label_map_label_id_unique on rov.biigle_label_map(label_id);

-- Restrictions must be uniquely named.
create unique index if not exists shared_restriction_name_unique on shared.restriction(lower(name));

-- The cruise import needs a version to determine which import strategy to use.
alter table rov.cruise_import add column if not exists version integer not null default 0;


-- Create a useful index.
create index measurement_measurement_type_id on rov.measurement(measurement_type_id);

-- Create a table for event measurements.
drop table if exists cache.evt_measurement;
create table cache.evt_measurement (
    event_id integer not null references rov.event(id) on delete cascade on update cascade,
    instrument_config_id integer not null references rov.instrument_config(id) on delete restrict on update cascade,
    measurement_type_id integer not null references rov.measurement_type(id) on delete restrict on update cascade,
    timestamps timestamp without time zone[],
    measurements float[] not null
);

-- Unique events.
alter table cache.evt_measurement add constraint cache_evt_measurement_unique_idx unique(event_id, instrument_config_id, measurement_type_id);

-- Documentation comments.
comment on table cache.evt_measurement is 'Creates a relation between an [event](#rov.event) and the [measurement](#rov.measurement) nearest the start and end times of the event.';
comment on column cache.evt_measurement.event_id is 'The link to the [event''s](#rov.event) event ID.';
comment on column cache.evt_measurement.instrument_config_id is 'The link to the [instrument configuration](#rov.instrument_config).';
comment on column cache.evt_measurement.measurement_type_id is 'The link to the [measurement type](#rov.measurement_type).';
comment on column cache.evt_measurement.timestamps is 'The array of timestamps corresponding to the measurements.';
comment on column cache.evt_measurement.measurements is 'The array of [measurements](#rov.measurement) between the event''s start and end times. If the event covers a span of time, multiple measurements are returned.';

-- Create indices.
create index cache_evt_measurement_event_id_idx on cache.evt_measurement(event_id);
create index cache_evt_measurement_instrument_config_id_idx on cache.evt_measurement(instrument_config_id);
create index cache_evt_measurement_measurement_type_id_idx on cache.evt_measurement(measurement_type_id);
create index cache_evt_measurement_timestamp_idx on cache.evt_measurement using gin(timestamps);
create index cache_evt_measurement_measurement_idx on cache.evt_measurement using gin(measurements);

-- A function to refresh the event measurement cache.
drop procedure if exists cache_refresh_evt_measurement;
create procedure cache_refresh_evt_measurement()
as $$
    declare 
        -- Records for the outer and inner loops.
        row0 record;
        -- Counter for logging.
		counter integer;
    begin
        
        -- Initialize to 0 so there will be an output on start.
		counter := 0;

        raise info 'Processing event measurements with end time.';

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id
            from rov.event e
                left join cache.evt_measurement o on o.event_id = e.id
            where o.event_id is null and e.end_time is not null
            order by e.id, e.start_time
        loop

            -- Output status.
			if counter > 0 and counter % 1000 = 0 then
				raise info 'Rows processed with end time: %.', counter;
			end if;

			counter := counter + 1;

            -- If the end time is available, seek positions between the start and end times.
            insert into cache.evt_measurement (event_id, instrument_config_id, measurement_type_id, 
                    timestamps, measurements)
                select e.id, i.id, m.measurement_type_id, 
                    array_agg(m.timestamp order by m.timestamp), array_agg(m.quantity order by m.timestamp)
                from rov.event e
                    inner join rov.dive d on d.id = e.dive_id
                    inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
                    inner join rov.measurement m on m.instrument_config_id = i.id
                where e.id = row0.event_id 
                    and m.timestamp >= e.start_time and m.timestamp <= e.end_time
                group by e.id, i.id, m.measurement_type_id
                having count(m.timestamp) > 0;

        end loop;

        commit; 

        raise info 'Processing event measurements without end time.';

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id
            from rov.event e
                left join cache.evt_measurement o on o.event_id = e.id
            where o.event_id is null and e.end_time is null
            order by e.id, e.start_time
        loop

            -- Output status.
			if counter > 0 and counter % 1000 = 0 then
				raise info 'Rows processed without end time: %.', counter;
			end if;

			counter := counter + 1;

            -- If the end time is available, seek positions between the start and end times.
            insert into cache.evt_measurement (event_id, instrument_config_id, measurement_type_id, 
                    timestamps, measurements)
                select e.id, i.id, m.measurement_type_id, 
                    array_agg(m.timestamp order by m.timestamp), array_agg(m.quantity order by m.timestamp)
                from rov.event e
                    inner join rov.dive d on d.id = e.dive_id
                    inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
                    inner join lateral (
                        select * 
                        from rov.measurement m 
                        where m.instrument_config_id = i.id
                            and timestamp < (e.start_time + interval '1 minute')
                            and timestamp > (e.start_time - interval '1 minute')
                        order by timestamp <-> e.start_time
                        limit 1
                    ) m on 't'::boolean
                where e.id = row0.event_id
                group by e.id, i.id, m.measurement_type_id
                having count(m.timestamp) > 0;

        end loop;

    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_evt_measurement is 'Populates the cache table with measurements corresponding '
    'to each event. For events that cover a time span, the timestamp and '
    'measurement will be stored as multiple ordered values in the array. For events '
    'without an end time a single element will be contained in the array.';


-- A function to refresh the event posotion cache.
drop procedure if exists cache_refresh_evt_pos;
drop function if exists cache_refresh_evt_pos;
create procedure cache_refresh_evt_pos()
as $$
    declare 
        -- Records for the outer and inner loops.
        row0 record;
        -- Counter for logging.
		counter integer;
    begin
        
        -- Initialize to 0 so there will be an output on start.
		counter := 0;

        raise info 'Processing event positions with end time.';

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id
            from rov.event e
                left join cache.evt_pos o on o.event_id = e.id
            where o.event_id is null and e.end_time is not null
            order by e.id, e.start_time
        loop

            -- Output status.
			if counter > 0 and counter % 1000 = 0 then
				raise info 'Rows processed with end time: %.', counter;
			end if;

			counter := counter + 1;

            -- If the end time is available, seek positions between the start and end times.
            insert into cache.evt_pos (event_id, instrument_config_id, timestamps, geoms)
                select e.id, i.id, array_agg(p.timestamp order by p.timestamp), 
                    st_collect(st_force2d(p.geom::geometry) order by p.timestamp)
                from rov.event e
                    inner join rov.dive d on d.id = e.dive_id
                    inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
                    inner join rov.position p on p.instrument_config_id = i.id
                where e.id = row0.event_id 
                    and p.timestamp >= e.start_time and p.timestamp <= e.end_time
                group by e.id, i.id 
                having count(p.timestamp) > 0;

        end loop;

        commit; 

        raise info 'Processing event positions without end time.';

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id
            from rov.event e
                left join cache.evt_pos o on o.event_id = e.id
            where o.event_id is null and e.end_time is null
            order by e.id, e.start_time
        loop

            -- Output status.
			if counter > 0 and counter % 1000 = 0 then
				raise info 'Rows processed without end time: %.', counter;
			end if;

			counter := counter + 1;

            -- If the end time is available, seek positions between the start and end times.
            insert into cache.evt_pos (event_id, instrument_config_id, timestamps, geoms)
                select e.id, i.id, array_agg(p.timestamp order by p.timestamp), 
                    st_collect(st_force2d(p.geom::geometry) order by p.timestamp)
                from rov.event e
                    inner join rov.dive d on d.id = e.dive_id
                    inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
                    inner join lateral (
                        select * 
                        from rov.position p 
                        where p.instrument_config_id = i.id
                            and timestamp < (e.start_time + interval '1 minute')
                            and timestamp > (e.start_time - interval '1 minute')
                        order by timestamp <-> e.start_time
                        limit 1
                    ) p on 't'::boolean
                where e.id = row0.event_id
                group by e.id, i.id
                having count(p.timestamp) > 0;

        end loop;

    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_evt_pos is 'Populates the cache table with depths corresponding '
    'to each event. For events that cover a time span, the timestamp and '
    'position will be stored as multiple ordered values in the array. For events '
    'without an end time a single element will be contained in the array.';

-- A function to refresh the event depth cache.
drop procedure if exists cache_refresh_evt_depth;
drop function if exists cache_refresh_evt_depth;
create procedure cache_refresh_evt_depth()
as $$
    declare 
        -- Records for the outer and inner loops.
        row0 record;
        -- Counter for logging.
		counter integer;
        -- Depth type ID.
        type_id integer;
    begin

        -- Get the depth ID.
        select id into type_id from rov.measurement_type where name='Depth' and unit='m';
        
        -- Initialize to 0 so there will be an output on start.
		counter := 0;

        raise info 'Processing event depths with end time.';

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id
            from rov.event e
                left join cache.evt_depth o on o.event_id = e.id
            where o.event_id is null and e.end_time is not null
            order by e.id, e.start_time
        loop

            -- Output status.
			if counter > 0 and counter % 1000 = 0 then
				raise info 'Rows processed with end time: %.', counter;
			end if;

			counter := counter + 1;

            -- If the end time is available, seek positions between the start and end times.
            insert into cache.evt_depth (event_id, instrument_config_id, timestamps, depths)
                select e.id, i.id, array_agg(m.timestamp order by m.timestamp), 
                    array_agg(m.quantity order by m.timestamp)
                from rov.event e
                    inner join rov.dive d on d.id = e.dive_id
                    inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
                    inner join rov.measurement m on m.instrument_config_id = i.id
                where e.id = row0.event_id 
                    and m.measurement_type_id = type_id
                    and m.timestamp >= e.start_time and m.timestamp <= e.end_time
                group by e.id, i.id 
                having count(m.timestamp) > 0;

        end loop;

        commit; 

        raise info 'Processing event depths without end time.';

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id
            from rov.event e
                left join cache.evt_depth o on o.event_id = e.id
            where o.event_id is null and e.end_time is null
            order by e.id, e.start_time
        loop

            -- Output status.
			if counter > 0 and counter % 1000 = 0 then
				raise info 'Rows processed without end time: %.', counter;
			end if;

			counter := counter + 1;

            -- If the end time is available, seek positions between the start and end times.
            insert into cache.evt_depth (event_id, instrument_config_id, timestamps, depths)
                select e.id, i.id, array_agg(m.timestamp order by m.timestamp), 
                    array_agg(m.quantity order by m.timestamp)
                from rov.event e
                    inner join rov.dive d on d.id = e.dive_id
                    inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
                    inner join lateral (
                        select * 
                        from rov.measurement
                        where instrument_config_id = i.id
                            and measurement_type_id = type_id
                            and timestamp < (e.start_time + interval '1 minute')
                            and timestamp > (e.start_time - interval '1 minute')
                        order by timestamp <-> e.start_time
                        limit 1
                    ) m on 't'::boolean
                where e.id = row0.event_id
                group by e.id, i.id
                having count(m.timestamp) > 0;

        end loop;

    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_evt_depth is 'Populates the cache table with depths corresponding '
    'to each event. For events that cover a time span, the timestamp and '
    'position will be stored as multiple ordered values in the array. For events '
    'without an end time a single element will be contained in the array.';



drop function if exists cache_refresh_dive_track;
drop procedure if exists cache_refresh_dive_track;
create procedure cache_refresh_dive_track()
as $$
    begin
        with t as (
            select 
                f.id as cruise_id, 
                d.id as dive_id, 
                p.id as position_id, 
                p.timestamp, 
                p.geom::geometry as geom,
                concat(f.name, '-', f.leg) as cruise_name, 
                d.name as dive_name, 
                concat('#', substring(md5(f.id::text) from 1 for 6)) as colour
            from rov.dive d
                left join cache.dive_track v on v.dive_id=d.id
                inner join rov.platform_config c on c.id=d.sub_config_id
                inner join rov.instrument_config b on b.platform_config_id=c.id
                inner join rov.position p on p.instrument_config_id=b.id
                inner join rov.cruise f on f.id=d.cruise_id
            where v.dive_id is null
                and d.start_time <= p.timestamp and d.end_time > p.timestamp
            order by p.timestamp
        )
        insert into cache.dive_track (dive_id, cruise_id, dive_name, cruise_name, colour, geom)
            select dive_id, cruise_id, dive_name, cruise_name, colour,
                st_force2d(st_makeline(geom::geometry order by timestamp)) as geom
            from t
            group by cruise_id, dive_id, dive_name, cruise_name, colour;
    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_dive_track is 'Stores a linestring representing the path '
    'of a platform over the course of a dive.';


-- Create function to refresh transect tracks.
drop function if exists cache_refresh_transect_track;
drop procedure if exists cache_refresh_transect_track;
create procedure cache_refresh_transect_track()
as $$
    begin
        with t as (
            select 
                f.id as cruise_id,
                d.id as dive_id, 
                e.id as transect_id, 
                a.id as position_id, 
                a.timestamp, 
                a.geom::geometry as geom,
                concat(f.name, '-', f.leg) as cruise_name,
                d.name as dive_name, 
                e.name as transect_name,
                concat('#', substring(md5(f.id::text) from 1 for 6)) as colour
            from rov.transect e
                left join cache.transect_track v on v.transect_id=e.id
                inner join rov.dive d on d.id=e.dive_id
                inner join rov.platform_config c on c.id=d.sub_config_id
                inner join rov.instrument_config b on b.platform_config_id=c.id
                inner join rov.position a on a.instrument_config_id=b.id
                inner join rov.cruise f on f.id=d.cruise_id
            where v.transect_id is null
                and e.start_time <= a.timestamp and e.end_time > a.timestamp
            order by a.timestamp
        )
        insert into cache.transect_track (transect_id, dive_id, cruise_id, transect_name, dive_name, cruise_name, colour, geom)
            select transect_id, dive_id, cruise_id, transect_name, dive_name, cruise_name, colour,
                st_force2d(st_makeline(geom::geometry order by timestamp)) as geom
            from t
            group by cruise_id, dive_id, transect_id, cruise_name, dive_name, transect_name, colour;
    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_transect_track is 'Stores a linestring representing the path '
    'of a platform over the course of a transect.';


-- Create function to refresh cruise tracks.
drop function if exists cache_refresh_cruise_track;
drop procedure if exists cache_refresh_cruise_track;
create procedure cache_refresh_cruise_track()
as $$
    begin
        insert into cache.cruise_track (cruise_id, cruise_name, geom, colour)
            select 
                a.id as cruise_id, 
                concat(a.name, '-', a.leg) as cruise_name, 
                st_makeline(st_force2d(f.geom::geometry)) as geom,
                concat('#', substring(md5(a.id::text) from 1 for 6)) as colour
            from rov.cruise a
                left join cache.cruise_track v on v.cruise_id=a.id
                inner join rov.dive b on b.cruise_id=a.id
                inner join rov.platform_config d on d.id=b.ship_config_id
                inner join rov.instrument_config e on e.platform_config_id=d.id
                inner join (select * from rov."position" order by "timestamp") f on f.instrument_config_id=e.id
            where v.cruise_id is null
                and f."timestamp" >= a.start_time and f."timestamp" <= a.end_time
            group by a.id;
    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_cruise_track is 'Stores a linestring representing the path '
    'of a platform over the course of a cruise.';


-- A function to refresh or rebuild the cache tables.
drop function if exists cache_refresh_tables(boolean);
drop procedure if exists cache_refresh_tables(boolean);
create procedure cache_refresh_tables(clean boolean)
as $$
    begin
        if clean = 't' then
            delete from cache.evt_pos;
            delete from cache.evt_depth;
            delete from cache.cruise_track;
            delete from cache.dive_track;
            delete from cache.transect_track;
        end if;
        call cache_refresh_evt_pos();
        call cache_refresh_evt_depth();
        call cache_refresh_evt_measurement();
        call cache_refresh_cruise_track();
        call cache_refresh_dive_track();
        call cache_refresh_transect_track();
    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_tables(boolean) is 'Refreshes all of the cache tables: event positions, '
    'event depths, event measurements, dive tracks, transect tracks and cruise tracks. '
    'If the given parameter is true, deletes all records before rebuilding from scratch.';


-- Refresh tables without clearing first.
drop function if exists cache_refresh_tables();
drop procedure if exists cache_refresh_tables();
create procedure cache_refresh_tables()
as $$
    begin
        call cache_refresh_tables('f');
    end;
$$ language 'plpgsql';

comment on procedure cache_refresh_tables() is 'Refreshes all of the cache tables: event positions, '
    'event depths, event measurements, dive tracks, transect tracks and cruise tracks.';
