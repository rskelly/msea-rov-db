-- Move the properties of other types of event into event itself to create a schemaless system
-- for storing observation, comment, measurement and status events. Event will get a properties
-- dictionary that stores all the data as key-value pairs; the event subclasses will be eliminated.

-- First, create a tempoary event table with appropriate columns, then copy all the event subclass data
-- into it, then delete the event and subclasses, and rename the temporary table to event.

alter table rov.event rename to __event;

create table rov.event (
    id serial not null primary key,
    dive_id integer not null references rov.dive(id) on delete restrict,
    annotation_job_id integer references rov.annotation_job(id) on delete restrict,
    instrument_config_id integer references rov.instrument_config(id) on delete restrict,
    start_time timestamp without time zone not null,
    end_time timestamp without time zone,
    properties jsonb not null default '{}',
    note text,
    created_on timestamp without time zone not null default now(),
    updated_on timestamp without time zone not null default now()
);

create index rov_event_properties_sn_idx on rov.event using gist((properties->>'scientific_name') gist_trgm_ops);
create index rov_event_properties_cn_idx on rov.event using gist((properties->>'common_name') gist_trgm_ops);
create index rov_event_properties_ol_idx on rov.event using gist((properties->>'original_labels') gist_trgm_ops);
create index rov_event_properties_cf_idx on rov.event using gist((properties->>'cf') gist_trgm_ops);
create index rov_event_start_time_idx on rov.event(start_time);
create index rov_event_end_time_idx on rov.event(end_time);

comment on table rov.event is 'The event table, a schemaless table storing all recorded observations and statuses during operations.';
comment on column rov.event.dive_id is 'A reference to the dive during which this event occurred.';
comment on column rov.event.start_time is 'The start time of the event.';
comment on column rov.event.end_time is 'The end time of the event. Null, if the event is discrete.';
comment on column rov.event.properties is 'A JSON object containing name-value pairs describing the event.';
comment on column rov.event.note is 'An optional note about the event. Do not use to store event data.';
comment on column rov.event.created_on is 'The time of creation of the event.';
comment on column rov.event.updated_on is 'The time of update of the event. Automatically updated by a trigger.';

drop trigger if exists rov_event_updated_on on rov.event;
CREATE TRIGGER rov_event_updated_on BEFORE UPDATE ON rov.event FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();

-- Get the observation events.
with t as (
    select 
        e.dive_id,
        e.start_time,
        e.end_time,
        e.original_labels,
        e.frames,
        e.shape,
        e.shape_area,
        e.attributes,
        e.medium_filename,
        e.tags,
        e.annotation_job_id,
        e.instrument_config_id,
        b.name as observation_confidence,
        a.name as abundance,
        o.count,
        o.category,
        v.name as coverage,
        t.common_name,
        t.scientific_name,
        t.aphia_id,
        t.inaturalist_id,
        t.hart_code,
        t.original_label,
        t.label,
        t.otu,
        concat(r.first_name, ' ', r.last_name) as reviewed_by,
        t.reviewed_by_id,
        t.reviewed_on,
        t.review_note
    from rov.__event e
        inner join rov.observation_event o on o.event_id = e.id
        left join rov.abundance a on a.id=o.abundance_id
        left join rov.coverage v on v.id=o.coverage_id
        left join rov.observation_confidence b on b.id=o.observation_confidence_id
        left join shared.taxon t on t.id=o.taxon_id
        left join shared.person r on r.id=t.reviewed_by_id
)
insert into rov.event (dive_id, start_time, end_time, annotation_job_id, instrument_config_id, properties) 
select dive_id, start_time, end_time, annotation_job_id, instrument_config_id, 
    jsonb_strip_nulls(coalesce(attributes, '{}')::jsonb || jsonb_build_object(
        'original_labels', coalesce(original_labels, '[]'::jsonb) || jsonb_build_array(trim(original_label)),
        'event_types', json_build_array('observation'),
        'frames', frames,
        'shape', shape,
        'shape_area', shape_area,
        'medium_filename', nullif(trim(medium_filename), ''),
        'tags', tags,
        'observation_confidence', nullif(trim(observation_confidence), ''),
        'abundance', nullif(trim(abundance), ''),
        'count', count,
        'category', nullif(trim(category), ''),
        'coverage', nullif(trim(coverage), ''),
        'common_name', nullif(trim(common_name), ''),
        'scientific_name', nullif(trim(scientific_name), ''),
        'aphia_id', aphia_id,
        'inaturalist_id', inaturalist_id,
        'hart_code', nullif(trim(hart_code), ''),
        'label', nullif(trim(label), ''),
        'otu', nullif(trim(otu), ''),
        'reviewed_by', nullif(trim(reviewed_by), ''),
        'reviewed_by_id', reviewed_by_id,
        'reviewed_on', reviewed_on,
        'review_note', nullif(trim(review_note), '')
    ))
from t;


-- Get the status events.
with t as (
    select  
        e.dive_id,
        e.instrument_config_id,
        e.start_time,
        e.end_time,
        e.original_labels,
        e.frames,
        e.shape,
        e.shape_area,
        e.attributes,
        e.medium_filename,
        e.tags,
        e.annotation_job_id,
        t.name as status_type,
        sd.detail as status_detail
    from rov.__event e
        inner join rov.status_event s on s.event_id = e.id
        inner join rov.dive d on d.id = e.dive_id
        inner join rov.status_type_detail sd on sd.id = s.status_type_detail_id
        inner join rov.status_type t on t.id = sd.status_type_id
)
insert into rov.event (dive_id, start_time, end_time, annotation_job_id, instrument_config_id, properties) 
select dive_id, start_time, end_time, annotation_job_id, instrument_config_id, 
    jsonb_strip_nulls(coalesce(attributes, '{}')::jsonb || jsonb_build_object(
        'original_labels', coalesce(original_labels, '[]'::jsonb),
        'event_types', json_build_array('status'),
        'frames', frames,
        'shape', shape,
        'shape_area', shape_area,
        'medium_filename', nullif(trim(medium_filename), ''),
        'tags', tags,
        'status_type', nullif(trim(status_type), ''), 
        'status_detail', nullif(trim(status_detail), '')
    ))
from t;


-- Get the measurement events.
with t as (
    select  
        e.dive_id,
        e.start_time,
        e.end_time,
        e.original_labels,
        e.frames,
        e.shape,
        e.shape_area,
        e.attributes,
        e.medium_filename,
        e.tags,
        e.annotation_job_id,
        e.instrument_config_id,
        t.unit,
        t.name as "type",
        m.quantity
    from rov.__event e
        inner join rov.measurement_event m on m.event_id=e.id
        inner join rov.measurement_type t on t.id=m.measurement_type_id
        inner join rov.dive d on d.id=e.dive_id
)
insert into rov.event (dive_id, start_time, end_time, annotation_job_id, instrument_config_id, properties) 
select dive_id, start_time, end_time, annotation_job_id, instrument_config_id, 
    jsonb_strip_nulls(coalesce(attributes, '{}')::jsonb || jsonb_build_object(
        'original_labels', coalesce(original_labels, '[]'::jsonb),
        'event_types', json_build_array('measurement'),
        'frames', frames,
        'shape', shape,
        'shape_area', shape_area,
        'medium_filename', nullif(trim(medium_filename), ''),
        'tags', tags,
        'unit', nullif(trim(unit), ''), 
        'type', nullif(trim("type"), ''),
        'quantity', quantity
    ))
from t;


-- Get the habitat events.
with t as (
    select 
        e.dive_id,
        e.start_time,
        e.end_time,
        e.original_labels,
        e.frames,
        e.shape,
        e.shape_area,
        e.attributes,
        e.medium_filename,
        e.tags,
        e.annotation_job_id,
        e.instrument_config_id,
        c.name as complexity,
        s.name as substrate,
        sc.name as substrate_coverage,
        b.name as biocover,
        t.name as thickness,
        dt.name as disturbance,
        r.name as relief,
        bc.name as biocover_coverage,
        f.name as flow,
        o.name as observation_confidence,
        x.common_name,
        x.scientific_name,
        x.aphia_id,
        x.inaturalist_id,
        x.hart_code,
        x.original_label,
        x.label,
        x.otu,
        concat(p.first_name, ' ', p.last_name) as reviewed_by,
        x.reviewed_by_id,
        x.reviewed_on,
        x.review_note
    from rov.__event e
        inner join rov.habitat_event h on h.event_id=e.id
        inner join rov.dive d on d.id=e.dive_id
        left join rov.complexity c on c.id=h.complexity_id
        left join rov.substrate s on s.id=h.substrate_id
        left join rov.coverage sc on sc.id=h.substrate_coverage_id
        left join rov.biocover b on b.id=h.biocover_id
        left join rov.thickness t on t.id=h.thickness_id
        left join rov.disturbance dt on dt.id=h.disturbance_id
        left join rov.relief r on r.id=h.relief_id
        left join rov.coverage bc on bc.id=h.biocover_coverage_id
        left join shared.taxon x on x.id=h.taxon_id
        left join rov.flow f on f.id=h.flow_id
        left join rov.observation_confidence o on o.id=h.observation_confidence_id
        left join shared.person p on p.id=x.reviewed_by_id
)
insert into rov.event (dive_id, start_time, end_time, annotation_job_id, instrument_config_id, properties) 
select dive_id, start_time, end_time, annotation_job_id, instrument_config_id, 
    jsonb_strip_nulls(coalesce(attributes, '{}')::jsonb || jsonb_build_object(
        'original_labels', coalesce(original_labels, '[]'::jsonb) || jsonb_build_array(trim(original_label)),
        'event_types', json_build_array('habitat'),
        'frames', frames,
        'shape', shape,
        'shape_area', shape_area,
        'medium_filename', nullif(trim(medium_filename), ''),
        'tags', tags,
        'complexity', nullif(trim(complexity), ''),
        'substrate', nullif(trim(substrate), ''),
        'substrate_coverage', nullif(trim(substrate_coverage), ''),
        'biocover', nullif(trim(biocover), ''),
        'thickness', nullif(trim(thickness), ''),
        'disturbance', nullif(trim(disturbance), ''),
        'relief', nullif(trim(relief), ''),
        'biocover_coverage', nullif(trim(biocover_coverage), ''),
        'flow', nullif(trim(flow), ''),
        'observation_confidence', nullif(trim(observation_confidence), ''),
        'common_name', nullif(trim(common_name), ''),
        'scientific_name', nullif(trim(scientific_name), ''),
        'aphia_id', aphia_id,
        'inaturalist_id', inaturalist_id,
        'hart_code', nullif(trim(hart_code), ''),
        'label', nullif(trim(label), ''),
        'otu', nullif(trim(otu), ''),
        'reviewed_by', nullif(trim(reviewed_by), ''),
        'reviewed_by_id', reviewed_by_id,
        'reviewed_on', reviewed_on,
        'review_note', nullif(trim(review_note), '')
    ))
from t;


-- Get the comment events.
with t as (
    select  
        e.dive_id,
        e.start_time,
        e.end_time,
        e.original_labels,
        e.frames,
        e.shape,
        e.shape_area,
        e.attributes,
        e.medium_filename,
        e.tags,
        e.annotation_job_id,
        e.instrument_config_id,
        c.note
    from rov.__event e
        inner join rov.comment_event c on c.event_id=e.id
        inner join rov.dive d on d.id=e.dive_id
)
insert into rov.event (dive_id, start_time, end_time, annotation_job_id, instrument_config_id, properties) 
select dive_id, start_time, end_time, annotation_job_id, instrument_config_id, 
    jsonb_strip_nulls(coalesce(attributes, '{}')::jsonb || jsonb_build_object(
        'original_labels', coalesce(original_labels, '[]'::jsonb),
        'event_types', json_build_array('comment'),
        'frames', frames,
        'shape', shape,
        'shape_area', shape_area,
        'medium_filename', nullif(trim(medium_filename), ''),
        'tags', tags,
        'comment', nullif(trim(note), '')
    ))
from t;
