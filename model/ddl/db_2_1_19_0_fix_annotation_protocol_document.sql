-- Fix annotation protocol document to use uploaded file.

-- First, add the file type and name to notes, if they exist.
create or replace procedure tmp()
as $$
      declare 
            cnt integer;
      begin
            select count(*) into cnt
            from information_schema.columns 
            where table_schema='rov' AND table_name='annotation_protocol_document' AND column_name='file_name';
            if cnt > 0
            then
                  update rov.annotation_protocol_document set note=trim(coalesce(note, '') || E'\nDocument name: ' || file_name) where file_name is not null;
                  update rov.annotation_protocol_document set note=trim(coalesce(note, '') || E'\nDocument type: ' || file_type) where file_type is not null;
            end if;
      end;
$$ language 'plpgsql';

call tmp();
drop procedure tmp();

-- Delete obsolete columns.
alter table rov.annotation_protocol_document drop column if exists file_type;
alter table rov.annotation_protocol_document drop column if exists file_name;

-- Add file reference.
alter table rov.annotation_protocol_document add column if not exists file_id integer references shared.uploaded_file(id);

comment on column rov.annotation_protocol_document.file_id is 'A reference to the [uploaded file](#shared.uploaded_file).';

-- A functon to clean up uploaded files that are not referenced by any other entity.
-- A server-side script must be run that deletes files that do not have a 
-- corresponding uploaded_file record.
create or replace procedure clean_uploaded_files()
as $$
declare 
      table_schema text;
      table_name text;
      column_name text;
      query text;
begin
      query := '';

      for table_schema, table_name, column_name in select distinct 
                        fk_tco.table_schema, fk_tco.table_name, fk_kcu.column_name
                        -- pk_tco.table_schema, pk_tco.table_name
                  from information_schema.referential_constraints rco
                  join information_schema.table_constraints fk_tco 
                        on rco.constraint_name = fk_tco.constraint_name
                        and rco.constraint_schema = fk_tco.table_schema
                  join information_schema.table_constraints pk_tco
                        on rco.unique_constraint_name = pk_tco.constraint_name
                        and rco.unique_constraint_schema = pk_tco.table_schema
                  join information_schema.key_column_usage fk_kcu
						on fk_kcu.constraint_name = fk_tco.constraint_name
                  where pk_tco.table_name = 'uploaded_file' and pk_tco.table_schema = 'shared'
      loop
            raise notice '% %', table_schema, table_name;            

            if length(query) > 0
            then
                  -- Add and between clauses.
                  query := query || 'and ';
            end if;

            -- Append the query.
            query := query || 'id not in (select ' || 
				quote_ident(column_name) 
				|| ' from ' || quote_ident(table_schema) || '.' || quote_ident(table_name) 
				|| ' where ' || quote_ident(column_name) || ' is not null) ';

      end loop;

      -- Assemble the query.
      query := 'delete from shared.uploaded_file where id in (select id from shared.uploaded_file where ' || query || ')';

      -- Execute the query.
      execute query;

end;
$$ language 'plpgsql';

comment on procedure clean_uploaded_files is 'Removes [uploaded file](#shared.uploaded_file) instances that are not referenced by any other entity. A server-side script must run which deletes files with no corresponding uploaded file record.';