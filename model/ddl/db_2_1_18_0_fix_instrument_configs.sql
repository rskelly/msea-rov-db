-- Create a function to move child entities from duplicate instrument configs
-- to the first config and delete the extras.
create or replace procedure remove_instrument_config_duplicates()
as $$
declare 
    first_ic_id integer;
    loop_ic_id integer;
    ic_ids integer[];
begin

	raise notice 'Starting...';

    -- Select the instrument config IDs, grouped by platform config and instrument.
    for ic_ids in select t.ids from (
			select array_agg(i.id) as ids 
	        from rov.instrument_config i 
	        group by i.platform_config_id, i.instrument_id
			order by i.platform_config_id
		) t
        where cardinality(t.ids) > 1
	loop 

        -- Get the first ID from the array.
	    first_ic_id := ic_ids[1];

		raise notice '% % %', first_ic_id, ic_ids, ic_ids[2:];

		-- Delete events from cache tables.	
        delete from cache.evt_pos where instrument_config_id=first_ic_id;
        delete from cache.evt_depth where instrument_config_id=first_ic_id;
        delete from cache.evt_measurement where instrument_config_id=first_ic_id;

        -- Iterate over the remaining IDs.
	    foreach loop_ic_id in array (ic_ids)[2:]
	    loop
			raise notice '% %', loop_ic_id, first_ic_id;
	        
            -- Reassign measurment instrument configs.
	        update rov.measurement m
	        set instrument_config_id=first_ic_id
	        where exists(
	            select 1 
				from rov.measurement m0
	            where m0.timestamp=m.timestamp 
	                and m0.quantity=m.quantity 
	                and m0.instrument_config_id=loop_ic_id
	        );
	        
            -- Reassign position instrument configs.
	        update rov.position p
	        set instrument_config_id=first_ic_id
	        where exists(
	            select 1 
				from rov.position p0
	            where p0.timestamp=p.timestamp 
	                and p0.geom=p.geom 
	                and p0.instrument_config_id=loop_ic_id
	        );
	        
            -- Delete affected events from cache tables.
	        delete from cache.evt_pos where instrument_config_id=loop_ic_id;
	        delete from cache.evt_depth where instrument_config_id=loop_ic_id;
	        delete from cache.evt_measurement where instrument_config_id=loop_ic_id;
	        
            -- Reassign event instrument configs.
	        update rov.event e
	        set instrument_config_id=first_ic_id
	        where exists(
	            select 1 
				from rov.event e0
	            where e0.start_time=e.start_time
	                and e0.end_time=e.end_time
	                and e0.properties=e.properties
	                and e0.instrument_config_id=loop_ic_id
	        );
	        
            -- Delete the config and remaining linked entities.
	        delete from rov.instrument_config where id=loop_ic_id;
            
	    end loop;
	end loop;
	
end;
$$ language 'plpgsql';

begin transaction;
-- Call the procedure. This could take a long time.
call remove_instrument_config_duplicates();

-- Re-populate the cache tables.
call cache_refresh_evt_pos();
call cache_refresh_evt_depth();
call cache_refresh_evt_measurement();
call cache_refresh_dive_track();
call cache_refresh_transect_track();
call cache_refresh_cruise_track();

commit;

-- Unique constraint on instrument config.
create unique index if not exists rov_instrument_config_platform_config_instrument_unique 
    on rov.instrument_config(platform_config_id, instrument_id);

-- Don't need the procedure any more.
drop procedure remove_instrument_config_duplicates();
