# Database Definition

These are data-definition language (SQL) scripts used to run updates on the database structure. 

These MUST be applied by the `db_manager.py` script in the parent directory, as it will take care of comparing versions and applying the appropriated patches.

Run `db_manager.py` with the path to the configuration file (in .env format) to perform a dry run. If there are no errors, run again with the -c switch (to commit) and the -p switch (to update permissions). This will permanently modify the database.

Note: there is no way to reverse the changes!

