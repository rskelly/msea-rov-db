
-- These are stored in the shared.uploaded_file table.
alter table rov.cruise_document drop column file_type;
alter table rov.cruise_document drop column file_name;
alter table rov.cruise_document rename column uploaded_file_id to file_id;

-- Add a reference to the file.
alter table rov.cruise_document add constraint rov_cruise_document_file 
    foreign key (file_id) references shared.uploaded_file(id);