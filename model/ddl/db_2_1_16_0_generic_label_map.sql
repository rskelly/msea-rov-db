-- A generic label map table.
create table if not exists rov.generic_label_map (
    id serial primary key not null,
    name varchar(128) not null,
    label_text text not null,
    properties jsonb not null default '{}'::jsonb,
    note text,
    created_on timestamp without time zone not null default now(),
    updated_on timestamp without time zone not null default now()
);

create unique index generic_label_map_name_label_unique on rov.generic_label_map(name, label_text);

CREATE TRIGGER rov_generic_label_map_updated_on BEFORE UPDATE ON rov.generic_label_map FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();

comment on table rov.generic_label_map is 'A table for storing mappings from annotation labels to property sets.';
comment on column rov.generic_label_map.name is 'A name for the grouping of label mappings. Can be a cruise or some other arbitrary name.';
comment on column rov.generic_label_map.label_text is 'The text of the label.';
comment on column rov.generic_label_map.properties is 'A dictionary of mapped properties.';
comment on column rov.generic_label_map.note is 'A note about the mapping.';
comment on column rov.generic_label_map.created_on is 'The date of creation.';
comment on column rov.generic_label_map.updated_on is 'The date of last update.';

-- Link the generic label map to the restriction table.
create table if not exists rov.generic_label_map_restriction (
    id serial primary key not null,
    restriction_id integer not null references shared.restriction(id) on update cascade on delete cascade,
    generic_label_map_id integer not null references rov.generic_label_map(id) on update cascade on delete cascade
);

comment on table rov.generic_label_map_restriction is 'Links a [generic label mapping](#rov.generic_label_map) to a [restriction](#shared.restriction).';
comment on column rov.generic_label_map_restriction.restriction_id is 'A reference to a [restriction](#shared.restriction).';
comment on column rov.generic_label_map_restriction.generic_label_map_id is 'A reference to a [generic label mapping](#rov.generic_label_map).';

-- Delete unnecessary columns from the Biigle map.
alter table rov.biigle_label_map drop column status_type_id;
alter table rov.biigle_label_map drop column scientific_name;
alter table rov.biigle_label_map drop column common_name;
alter table rov.biigle_label_map drop column otu;
alter table rov.biigle_label_map drop column cf;

-- Change the uniqe constaint on user emails to allow null.
CREATE EXTENSION IF NOT EXISTS citext;  
drop index shared.person_email_unique;
alter table shared.person alter column email type citext;
create unique index person_email_unique on shared.person(email) where email is not null;

-- Remove null constraint from is_template.
alter table rov.annotation_protocol alter column is_template drop not null;

alter table rov.annotation_protocol drop column if exists short_code;
alter table rov.annotation_software drop column if exists short_code;
alter table rov.equipment_type drop column if exists short_code;
alter table rov.instrument drop column if exists short_code;
alter table rov.cruise_crew drop column if exists short_code;
alter table rov.dive_crew drop column if exists short_code;
alter table shared.organisation drop column if exists short_code;
