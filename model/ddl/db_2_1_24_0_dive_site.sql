-- Unlink the dive from site. The site is still linked by intertidal data and the spatial library.

alter table rov.dive drop table site_id;
