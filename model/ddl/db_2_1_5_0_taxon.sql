create table taxonomy.taxon (
    id serial primary key,
    taxon_id varchar(8) not null, 
    source varchar(32) not null,
    rank varchar(64),
    scientific_name varchar(256) not null,
    common_name varchar(256), 
    superdomain varchar(256),
    domain varchar(256),
    kingdom varchar(256), 
    subkingdom varchar(256), 
    infrakingdom varchar(256), 
    phylum varchar(256), 
    phylum_division varchar(256), 
    subphylum_subdivision varchar(256), 
    subphylum varchar(256), 
    infraphylum varchar(256), 
    parvphylum varchar(256), 
    gigaclass varchar(256),
    megaclass varchar(256),
    superclass varchar(256), 
    "class" varchar(256), 
    subclass varchar(256), 
    infraclass varchar(256), 
    subterclass varchar(256), 
    superorder varchar(256), 
    "order" varchar(256), 
    suborder varchar(256), 
    infraorder varchar(256), 
    parvorder varchar(256), 
    superfamily varchar(256), 
    family varchar(256), 
    subfamily varchar(256),
    supertribe varchar(256), 
    tribe varchar(256), 
    subtribe varchar(256), 
    genus varchar(256), 
    genus_hybrid varchar(256), 
    subgenus varchar(256), 
    section varchar(256),
    subsection varchar(256),
    series varchar(256), 
    species varchar(256), 
    hybrid varchar(256), 
    subspecies varchar(256), 
    natio varchar(256),
    variety varchar(256), 
    subvariety varchar(256), 
    form varchar(256),
    subform varchar(256),
    unique(source, taxon_id)
);

create index taxon_scientific_name_idx on taxonomy.taxon using gist (scientific_name gist_trgm_ops);
create index taxon_common_name_idx on taxonomy.taxon using gist (common_name gist_trgm_ops);
create index taxon_source_idx on taxonomy.taxon(source);
create index taxon_rank_idx on taxonomy.taxon(rank);

comment on table taxonomy.taxon is 'Stores taxonomic names from a variety of databases in a common format distinguished by source and taxon_id.';
comment on column taxonomy.taxon.taxon_id is 'The taxonomic ID from the source database.';
comment on column taxonomy.taxon.source is 'The name of the source database: "inaturalist", "worms", "obis", etc.';
comment on column taxonomy.taxon.rank is 'The name of the taxonomic rank of the record, e.g., "class", "subspecies", etc.';
comment on column taxonomy.taxon.scientific_name is 'The scientific name of the species.';
comment on column taxonomy.taxon.common_name is 'The common name of the species.';
comment on column taxonomy.taxon.superdomain is 'The superdomain name.';
comment on column taxonomy.taxon.domain is 'The domain name.';
comment on column taxonomy.taxon.kingdom is 'The kingdom name.'; 
comment on column taxonomy.taxon.subkingdom is 'The subkingdom name.'; 
comment on column taxonomy.taxon.infrakingdom is 'The infrakingdom name.'; 
comment on column taxonomy.taxon.phylum is 'The phylum name.'; 
comment on column taxonomy.taxon.phylum_division is 'The phylum division name.'; 
comment on column taxonomy.taxon.subphylum_subdivision is 'The subphylum division name.'; 
comment on column taxonomy.taxon.subphylum is 'The subphylum name.'; 
comment on column taxonomy.taxon.infraphylum is 'The infraphylum name.'; 
comment on column taxonomy.taxon.parvphylum is 'The parvphylum name.'; 
comment on column taxonomy.taxon.gigaclass is 'The gigaclass name.';
comment on column taxonomy.taxon.megaclass is 'The megaclass name.';
comment on column taxonomy.taxon.superclass is 'The superclass name.'; 
comment on column taxonomy.taxon."class" is 'The "class" name.'; 
comment on column taxonomy.taxon.subclass is 'The subclass name.'; 
comment on column taxonomy.taxon.infraclass is 'The infraclass name.'; 
comment on column taxonomy.taxon.subterclass is 'The subterclass name.'; 
comment on column taxonomy.taxon.superorder is 'The superorder name.'; 
comment on column taxonomy.taxon."order" is 'The "order" name.'; 
comment on column taxonomy.taxon.suborder is 'The suborder name.'; 
comment on column taxonomy.taxon.infraorder is 'The infraorder name.'; 
comment on column taxonomy.taxon.parvorder is 'The parvorder name.'; 
comment on column taxonomy.taxon.superfamily is 'The superfamily name.'; 
comment on column taxonomy.taxon.family is 'The family name.'; 
comment on column taxonomy.taxon.subfamily is 'The subfamily name.';
comment on column taxonomy.taxon.supertribe is 'The supertribe name.'; 
comment on column taxonomy.taxon.tribe is 'The tribe name.'; 
comment on column taxonomy.taxon.subtribe is 'The subtribe name.'; 
comment on column taxonomy.taxon.genus is 'The genus name.'; 
comment on column taxonomy.taxon.genus_hybrid is 'The genus hybrid name.'; 
comment on column taxonomy.taxon.subgenus is 'The subgenus name.'; 
comment on column taxonomy.taxon.section is 'The section name.';
comment on column taxonomy.taxon.subsection is 'The subsection name.';
comment on column taxonomy.taxon.series is 'The series name.'; 
comment on column taxonomy.taxon.species is 'The species name.'; 
comment on column taxonomy.taxon.hybrid is 'The hybrid name.'; 
comment on column taxonomy.taxon.subspecies is 'The subspecies name.'; 
comment on column taxonomy.taxon.natio is 'The natio name.';
comment on column taxonomy.taxon.variety is 'The variety name.'; 
comment on column taxonomy.taxon.subvariety is 'The subvariety name.'; 
comment on column taxonomy.taxon.form is 'The form name.';
comment on column taxonomy.taxon.subform is 'The subform name.';
