-- Add the status type ID to the biigle label.
alter table rov.biigle_label_map add column status_type_id integer references rov.status_type(id) on delete restrict on update cascade;

comment on column rov.biigle_label_map.status_type_id is 'The ID of the (status type)[#rov.status_type].';

alter table rov.biigle_label_map add column scientific_name varchar(256);
alter table rov.biigle_label_map add column common_name varchar(256);
alter table rov.biigle_label_map add column otu varchar(16);
alter table rov.biigle_label_map add column cf varchar(256);

comment on column rov.biigle_label_map.scientific_name is 'The scientific name of the observed taxon.';
comment on column rov.biigle_label_map.common_name is 'The common name of the observed taxon.';
comment on column rov.biigle_label_map.otu is 'The operational taxonomic unit (OTU) of the observed taxon.';
comment on column rov.biigle_label_map.cf is 'The name of a taxon that is not definitive. The identifiers are selected for the next higher taxonomic level.';