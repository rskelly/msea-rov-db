-- Add parent and accepted name records.
alter table taxonomy.taxon add column accepted_taxon_id integer;
alter table taxonomy.taxon add column parent_taxon_id integer;

comment on column taxonomy.taxon.accepted_taxon_id is 'The taxon ID of the accepted taxon for this unaccepted taxon. If this column is null, the taxon is accepted.';
comment on column taxonomy.taxon.parent_taxon_id is 'The taxon ID of the taxon''s parent taxon. If this column is null, the taxon has no parents.';
