--
-- PostgreSQL database dump
--

-- Dumped from database version 16.4 (Debian 16.4-1.pgdg110+2)
-- Dumped by pg_dump version 16.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: msea; Type: DATABASE; Schema: -; Owner: msea_admin
--

CREATE DATABASE msea WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';


ALTER DATABASE msea OWNER TO msea_admin;

\connect msea

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: iform; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA iform;


ALTER SCHEMA iform OWNER TO msea_admin;

--
-- Name: intertidal; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA intertidal;


ALTER SCHEMA intertidal OWNER TO msea_admin;

--
-- Name: SCHEMA intertidal; Type: COMMENT; Schema: -; Owner: msea_admin
--

COMMENT ON SCHEMA intertidal IS 'Stores data for the intertidal database. Contains information about UAS flights, surveys and other derivative data.';


--
-- Name: maxmind; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA maxmind;


ALTER SCHEMA maxmind OWNER TO msea_admin;

--
-- Name: ndst; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA ndst;


ALTER SCHEMA ndst OWNER TO msea_admin;

--
-- Name: SCHEMA ndst; Type: COMMENT; Schema: -; Owner: msea_admin
--

COMMENT ON SCHEMA ndst IS 'Contains tables that store transient data loaded from the [Dive Logging App](https://msea.science/Dive-Logging-App) so it can be loaded into the ROV database.';


--
-- Name: pa; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA pa;


ALTER SCHEMA pa OWNER TO msea_admin;

--
-- Name: SCHEMA pa; Type: COMMENT; Schema: -; Owner: msea_admin
--

COMMENT ON SCHEMA pa IS 'Stores data about protected areas, including RCAs, parks, etc.';


--
-- Name: public; Type: SCHEMA; Schema: -; Owner: msea_admin
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO msea_admin;

--
-- Name: rov; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA rov;


ALTER SCHEMA rov OWNER TO msea_admin;

--
-- Name: SCHEMA rov; Type: COMMENT; Schema: -; Owner: msea_admin
--

COMMENT ON SCHEMA rov IS 'The main schema of the ROV database, stores information about all relevant entities including cruise metadata, personnel, dives, transects, equipment configuration, telemetry, water properties and observations.';


--
-- Name: shared; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA shared;


ALTER SCHEMA shared OWNER TO msea_admin;

--
-- Name: SCHEMA shared; Type: COMMENT; Schema: -; Owner: msea_admin
--

COMMENT ON SCHEMA shared IS 'Stores data that are shared between other schemas, such as personnel information, taxonomy, etc.';


--
-- Name: taxonomy; Type: SCHEMA; Schema: -; Owner: msea_admin
--

CREATE SCHEMA taxonomy;


ALTER SCHEMA taxonomy OWNER TO msea_admin;

--
-- Name: wiki; Type: SCHEMA; Schema: -; Owner: msea
--

CREATE SCHEMA wiki;


ALTER SCHEMA wiki OWNER TO msea;

--
-- Name: SCHEMA wiki; Type: COMMENT; Schema: -; Owner: msea
--

COMMENT ON SCHEMA wiki IS 'Stores data related to the Wiki using the standard [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki) structure.';


--
-- Name: btree_gist; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS btree_gist WITH SCHEMA public;


--
-- Name: EXTENSION btree_gist; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION btree_gist IS 'support for indexing common datatypes in GiST';


--
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry and geography spatial types and functions';


--
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;


--
-- Name: EXTENSION tablefunc; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


--
-- Name: equipment_category; Type: TYPE; Schema: public; Owner: msea_admin
--

CREATE TYPE public.equipment_category AS ENUM (
    'Platform',
    'Instrument',
    'Unknown'
);


ALTER TYPE public.equipment_category OWNER TO msea_admin;

--
-- Name: us_media_type_enum; Type: TYPE; Schema: wiki; Owner: msea
--

CREATE TYPE wiki.us_media_type_enum AS ENUM (
    'UNKNOWN',
    'BITMAP',
    'DRAWING',
    'AUDIO',
    'VIDEO',
    'MULTIMEDIA',
    'OFFICE',
    'TEXT',
    'EXECUTABLE',
    'ARCHIVE',
    '3D'
);


ALTER TYPE wiki.us_media_type_enum OWNER TO msea;

--
-- Name: _copy_habitat_events(); Type: PROCEDURE; Schema: public; Owner: msea_admin
--

CREATE PROCEDURE public._copy_habitat_events()
    LANGUAGE plpgsql
    AS $$
declare
    row1 record;
begin

    -- Loop over the rows with both subdom and dom substrates.
    for row1 in
        select *
        from rov.event a
            inner join rov.habitat_event b on b.event_id=a.id
        where subdominant_substrate_id is not null and substrate_id is not null
    loop
        -- Create a new event for each one and return its ID.
        with row2 as (
            insert into rov.event (dive_id, transect_id, start_time, end_time, original_id,
                    annotation_config_id, shape, shape_area, frames)
            values (row1.dive_id, row1.transect_id, row1.start_time, row1.end_time, row1.original_id,
                    row1.annotation_config_id, row1.shape, row1.shape_area, row1.frames)
            returning id as new_id
        ),
        -- Select the important fields from the existing habitat event.
        row3 as (
            select complexity_id, subdominant_substrate_id, subdominant_coverage_id,
                biocover_id, thickness_id, disturbance_id, relief_id, annotation_protocol_taxon_id, 't'
            from rov.habitat_event
            where event_id=row1.id
        )
        -- Create the new habitat event, populate it and associate it with the new event.
        insert into rov.habitat_event (event_id, complexity_id, substrate_id, coverage_id, biocover_id,
            thickness_id, disturbance_id, relief_id, annotation_protocol_taxon_id, subdominant)
        select row2.new_id, row3.complexity_id, row3.subdominant_substrate_id, row3.subdominant_coverage_id,
            row3.biocover_id, row3.thickness_id, row3.disturbance_id, row3.relief_id,
            row3.annotation_protocol_taxon_id, 't'
        from row3, row2;
    end loop;
end;
$$;


ALTER PROCEDURE public._copy_habitat_events() OWNER TO msea_admin;

--
-- Name: _copy_status_events(); Type: PROCEDURE; Schema: public; Owner: msea_admin
--

CREATE PROCEDURE public._copy_status_events()
    LANGUAGE plpgsql
    AS $$
declare
    row1 record;
    row2 record;
    detail_id integer;
    new_id integer;
begin
    -- Select the rows that have an image quality record.
    for row1 in select * from rov.event where image_quality_id is not null
    loop
        -- Get the detail ID that corresponds to the image quality lookup.
        select b.id into detail_id
            from rov.image_quality a
                inner join rov.status_type_detail b on b.detail=a.name
                inner join rov.status_type c on c.id=b.status_type_id
            where c.name='Image Quality' and b.detail=a.name and a.id=row1.image_quality_id;

        -- Create the status event with the status detail instead of image quality.
        with row2 as (
            -- Create an event to relate to the status event.
            insert into rov.event (dive_id, transect_id, start_time, end_time, original_id,
                    annotation_config_id, shape, shape_area, frames)
            values (row1.dive_id, row1.transect_id, row1.start_time, row1.end_time, row1.original_id,
                    row1.annotation_config_id, row1.shape, row1.shape_area, row1.frames)
            returning id
        )
        -- Create the actual status event.
        insert into rov.status_event(event_id, status_type_detail_id)
        values ((select id from row2), detail_id);
    end loop;
end;
$$;


ALTER PROCEDURE public._copy_status_events() OWNER TO msea_admin;

--
-- Name: f_geoip_update_geoms(); Type: FUNCTION; Schema: public; Owner: msea_admin
--

CREATE FUNCTION public.f_geoip_update_geoms() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    begin
        -- Update the location if coords are present.
        if new.latitude is not null and new.longitude is not null and new.location is null
            then

            update maxmind.geoip_city_blocks 
                set location=st_setsrid(st_makepoint(new.longitude, new.latitude), 4326)::geography(POINT) 
            where network=new.network;

            -- Update the region if radius is present.
            if new.accuracy_radius is not null
                then

                update maxmind.geoip_city_blocks 
                    set region=st_buffer(location, new.accuracy_radius)::geography(POLYGON) 
                where network=new.network;

            end if;

        end if;
        return new;
    end;
$$;


ALTER FUNCTION public.f_geoip_update_geoms() OWNER TO msea_admin;

--
-- Name: inat_taxon_level(integer); Type: FUNCTION; Schema: public; Owner: msea_admin
--

CREATE FUNCTION public.inat_taxon_level(id integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
declare 
	cols text[];	-- A list of column names in order of specificity from least to most.
	ranks text[];	-- A list of rank names corresponding to the columns.
	i integer;		-- A counter.
	v text;			-- A value holder.
	rank text;		-- The returned rank name.
begin
	-- Fill the cols with an array of column names.
	cols := array['kingdom', 'phylum', 'subphylum', 'superclass', '"class"',
	'subclass', 'superorder',
	'order', 'suborder', 'superfamily', 'family', 'subfamily',
	'supertribe', 'tribe', 'subtribe', 'genus', 'genushybrid',
	'species', 'hybrid', 'subspecies', 'variety', 'form'];

	-- Fill the ranks with an array of rank names.
	ranks := array['Kingdom', 'Phylum', 'Subphylum', 'Superclass', 'Class', 'Subclass', 'Superorder', 'Order', 'Suborder', 
	'Superfamily', 'Family', 'Subfamily', 'Supertribe', 'Tribe', 'Subtribe', 'Genus', 'Genushybrid', 'Species', 'Hybrid',
	'Supspecies', 'Variety', 'Form'];

	-- Get the length of the columns array.
	i := array_length(cols, 1);

	-- Set the current rank.
	rank := null;

	loop

		-- If we get to zero (beyond the top rank) give up.
		if i <= 0 then
			exit;
		end if;
	
		-- Select the current taxonomic level.
		execute 'select "' || cols[i] || '" from taxonomy.inaturalist_taxon where taxon_id=$1 limit 1' into v using id;
	
		-- raise notice '% % %', i, cols[i], v;
		if v is not null and v <> '' then
			-- If the column is valid, set the rank name and exit the loop.
			rank := ranks[i];
			exit;
		end if;

		-- Decrement the counter and continue.
		select i - 1 into i;
	end loop;

	-- Return the rank name.
	return rank;
	
end;
$_$;


ALTER FUNCTION public.inat_taxon_level(id integer) OWNER TO msea_admin;

--
-- Name: lookup_position(integer, timestamp without time zone, double precision); Type: FUNCTION; Schema: public; Owner: msea_admin
--

CREATE FUNCTION public.lookup_position(integer, timestamp without time zone, double precision) RETURNS TABLE(dive_id integer, ts timestamp without time zone, geom public.geography, tdiff double precision)
    LANGUAGE plpgsql
    AS $_$
begin
    return query select a.dive_id, a."timestamp", a.geom, date_part('epoch'::text, $2 - a."timestamp") as tdiff
    from rov."position" a
    where a.dive_id=$1 and abs(date_part('epoch'::text, $2 - "timestamp")) <= $3
    order by "timestamp" <-> $2
    limit 1;
end;
$_$;


ALTER FUNCTION public.lookup_position(integer, timestamp without time zone, double precision) OWNER TO msea_admin;

--
-- Name: measurement_position_maintain_dive(integer); Type: PROCEDURE; Schema: public; Owner: msea_admin
--

CREATE PROCEDURE public.measurement_position_maintain_dive(IN _dive_id integer)
    LANGUAGE sql
    AS $$
    -- Records don't have to be deleted
    -- Insert the new records.
    insert into rov.measurement_position (dive_id, m_id, p_id, tdiff)
    select a.dive_id, a.id, b.id, date_part('epoch'::text, a."timestamp"-b."timestamp")
    from rov.measurement a
        inner join lateral (
            select dive_id, id, "timestamp"
            from rov."position"
            where a.dive_id=dive_id and abs(date_part('epoch'::text, a."timestamp"-"timestamp")) < 60
            group by dive_id, id, "timestamp", instrument_config_id
            order by a."timestamp" <-> "timestamp"
            limit 1
        ) b on true
    where a.dive_id = _dive_id
    on conflict do nothing
$$;


ALTER PROCEDURE public.measurement_position_maintain_dive(IN _dive_id integer) OWNER TO msea_admin;

--
-- Name: measurement_position_maintain_dives(); Type: PROCEDURE; Schema: public; Owner: msea_admin
--

CREATE PROCEDURE public.measurement_position_maintain_dives()
    LANGUAGE sql
    AS $$
    -- Records don't have to be deleted
    -- Insert the new records.
    insert into rov.measurement_position (dive_id, m_id, p_id, tdiff)
    select a.dive_id, a.id, b.id, date_part('epoch'::text, a."timestamp"-b."timestamp")
    from rov.measurement a
        inner join lateral (
            select dive_id, id, "timestamp"
            from rov."position"
            where a.dive_id=dive_id and abs(date_part('epoch'::text, a."timestamp"-"timestamp")) < 60
            group by dive_id, id, "timestamp", instrument_config_id
            order by a."timestamp" <-> "timestamp"
            limit 1
        ) b on true
    on conflict do nothing
$$;


ALTER PROCEDURE public.measurement_position_maintain_dives() OWNER TO msea_admin;

--
-- Name: p_measurement_position_crosstab(); Type: PROCEDURE; Schema: public; Owner: msea_admin
--

CREATE PROCEDURE public.p_measurement_position_crosstab()
    LANGUAGE plpgsql
    AS $$
    declare
        fields text;
        rec record;
    begin
        -- Assemble the value field set.
        for rec in select distinct concat(a.name, ' (', a.unit ,')') as type
            from rov.measurement_type a
                inner join rov.measurement b on b.measurement_type_id=a.id
            order by concat(a.name, ' (', a.unit ,')')
        loop
                fields := concat(fields, ', "', rec.type, '" real');
        end loop;

        -- raise notice '%', fields;

        execute 'drop materialized view if exists rov.view_measurement_position_crosstab';
        execute 'create materialized view rov.view_measurement_position_crosstab as '
            'select * from crosstab( '
            '''with t as ('
            '    select a.id as dive_id, a.name as dive_name, b.name as cruise_name '
            '    from rov.dive a '
            '        inner join rov.cruise b on b.id=a.cruise_id '
            '), u as ('
            '    select dive_id, m_id, p_id, tdiff '
            '    from rov.measurement_position '
            ') '
            'select b."timestamp", t.cruise_name, t.dive_name, u.tdiff, '
            '    b.geom, st_x(b.geom::geometry) as lon, st_y(b.geom::geometry) as lat, '
            '    c.name || '''' ('''' || c.unit || '''')'''' as type, a.quantity '
            'from t '
            '    inner join u on u.dive_id=t.dive_id '
            '    inner join rov.measurement a on a.id=u.m_id '
            '    inner join rov.position b on b.id=u.p_id '
            '    inner join rov.measurement_type c on c.id=a.measurement_type_id '
            'order by t.cruise_name, t.dive_name, b."timestamp" '
        ''','''
            'select distinct a.name || '''' ('''' || a.unit || '''')'''' as type '
            'from rov.measurement_type a '
            '   inner join rov.measurement b on b.measurement_type_id=a.id '
            'order by a.name || '''' ('''' || a.unit || '''')'''' '
        ''') as ("timestamp" timestamp(6) without time zone, cruise_name varchar(32), dive_name varchar(32), '
            'tdiff real, geom geography, lon real, lat real' || fields || ')';
        execute 'create index mpc_cruise_name_idx on rov.view_measurement_position_crosstab(cruise_name)';
        execute 'create index mpc_dive_name_idx on rov.view_measurement_position_crosstab(dive_name)';
        execute 'create index mpc_geom_idx on rov.view_measurement_position_crosstab using gist(geom)';
        execute 'create index mpc_timestamp_idx on rov.view_measurement_position_crosstab("timestamp")';
    end
$$;


ALTER PROCEDURE public.p_measurement_position_crosstab() OWNER TO msea_admin;

--
-- Name: p_measurement_position_maintain_dive(integer); Type: PROCEDURE; Schema: public; Owner: msea_admin
--

CREATE PROCEDURE public.p_measurement_position_maintain_dive(IN _dive_id integer)
    LANGUAGE sql
    AS $$
    -- Records don't have to be deleted
    -- Insert the new records.
    insert into rov.measurement_position (dive_id, m_id, p_id, tdiff)
    select a.dive_id, a.id, b.id, date_part('epoch'::text, a."timestamp"-b."timestamp")
    from rov.measurement a
        inner join lateral (
            select dive_id, id, "timestamp"
            from rov."position"
            where a.dive_id=dive_id and abs(date_part('epoch'::text, a."timestamp"-"timestamp")) < 60
            group by dive_id, id, "timestamp", instrument_config_id
            order by a."timestamp" <-> "timestamp"
            limit 1
        ) b on true
    where a.dive_id = _dive_id
    on conflict do nothing
$$;


ALTER PROCEDURE public.p_measurement_position_maintain_dive(IN _dive_id integer) OWNER TO msea_admin;

--
-- Name: p_measurement_position_maintain_dives(); Type: PROCEDURE; Schema: public; Owner: msea_admin
--

CREATE PROCEDURE public.p_measurement_position_maintain_dives()
    LANGUAGE plpgsql
    AS $$
    -- Records don't have to be deleted
    -- Insert the new records because measurement deletes cascade.
    declare
        rec record;
    begin
    raise notice 'Processing measurements for measurement_position table.';
    for rec in select dive_id, id from rov.measurement where id not in (
        select m_id from rov.measurement_position
    )
    loop
        call p_measurement_position_maintain_dive(rec.dive_id, rec.id);
    end loop;
    end;
$$;


ALTER PROCEDURE public.p_measurement_position_maintain_dives() OWNER TO msea_admin;

--
-- Name: updated_on_column(); Type: FUNCTION; Schema: public; Owner: msea_admin
--

CREATE FUNCTION public.updated_on_column() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
   NEW.updated_on = now();
   RETURN NEW;
END;
$$;


ALTER FUNCTION public.updated_on_column() OWNER TO msea_admin;

--
-- Name: FUNCTION updated_on_column(); Type: COMMENT; Schema: public; Owner: msea_admin
--

COMMENT ON FUNCTION public.updated_on_column() IS 'Updates the updated_on column of a relation to the current time on update.';


--
-- Name: ts2_page_text(); Type: FUNCTION; Schema: wiki; Owner: msea
--

CREATE FUNCTION wiki.ts2_page_text() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
 IF TG_OP = 'INSERT' THEN
 NEW.textvector = to_tsvector(NEW.old_text);
 ELSIF NEW.old_text != OLD.old_text THEN
 NEW.textvector := to_tsvector(NEW.old_text);
 END IF;
 RETURN NEW;
 END;
 $$;


ALTER FUNCTION wiki.ts2_page_text() OWNER TO msea;

--
-- Name: ts2_page_title(); Type: FUNCTION; Schema: wiki; Owner: msea
--

CREATE FUNCTION wiki.ts2_page_title() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
 BEGIN
 IF TG_OP = 'INSERT' THEN
 NEW.titlevector = to_tsvector(REPLACE(NEW.page_title,'/',' '));
 ELSIF NEW.page_title != OLD.page_title THEN
 NEW.titlevector := to_tsvector(REPLACE(NEW.page_title,'/',' '));
 END IF;
 RETURN NEW;
 END;
 $$;


ALTER FUNCTION wiki.ts2_page_title() OWNER TO msea;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: event_log_data; Type: TABLE; Schema: iform; Owner: msea_admin
--

CREATE TABLE iform.event_log_data (
    id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    content jsonb NOT NULL
);


ALTER TABLE iform.event_log_data OWNER TO msea_admin;

--
-- Name: event_log; Type: VIEW; Schema: iform; Owner: msea_admin
--

CREATE VIEW iform.event_log AS
 WITH t AS (
         SELECT event_log_data.content
           FROM iform.event_log_data
          WHERE (jsonb_typeof(event_log_data.content) = 'object'::text)
        UNION
         SELECT jsonb_array_elements(event_log_data.content) AS content
           FROM iform.event_log_data
          WHERE (jsonb_typeof(event_log_data.content) = 'array'::text)
        )
 SELECT (content ->> 'id'::text) AS id,
    (content ->> 'created_date'::text) AS created_date,
    (content ->> 'created_device_id'::text) AS created_device_id,
    (content ->> 'modified_date'::text) AS modified_date,
    (content ->> 'modified_device_id'::text) AS modified_device_id,
    (content ->> 'recorder'::text) AS recorder,
    (content ->> 'event_time'::text) AS event_time,
    (content ->> 'event_type'::text) AS event_type,
    (content ->> 'event_title'::text) AS event_title,
    (content ->> 'notes'::text) AS notes,
    (content ->> 'photos'::text) AS photos,
    (content ->> 'event_description'::text) AS event_description,
    (content ->> 'trip_id'::text) AS trip_id,
    (content ->> 'event_position'::text) AS "position",
    (content ->> 'event_position_json'::text) AS position_json
   FROM t;


ALTER VIEW iform.event_log OWNER TO msea_admin;

--
-- Name: event_log_data_id_seq; Type: SEQUENCE; Schema: iform; Owner: msea_admin
--

CREATE SEQUENCE iform.event_log_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE iform.event_log_data_id_seq OWNER TO msea_admin;

--
-- Name: event_log_data_id_seq; Type: SEQUENCE OWNED BY; Schema: iform; Owner: msea_admin
--

ALTER SEQUENCE iform.event_log_data_id_seq OWNED BY iform.event_log_data.id;


--
-- Name: quadrat; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.quadrat (
    id integer NOT NULL,
    survey_id integer NOT NULL,
    name character varying(128) NOT NULL,
    flag_number character varying(16) NOT NULL,
    "time" timestamp(6) without time zone NOT NULL,
    transect integer NOT NULL,
    zone integer NOT NULL,
    corner_lon1 double precision,
    corner_lat1 double precision,
    corner_height1 real,
    corner_lon2 double precision,
    corner_lat2 double precision,
    corner_height2 real,
    corner_lon3 double precision,
    corner_lat3 double precision,
    corner_height3 real,
    corner_lon4 double precision,
    corner_lat4 double precision,
    corner_height4 real,
    centroid_lon double precision,
    centroid_lat double precision,
    centroid_height real,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    geom public.geography
);


ALTER TABLE intertidal.quadrat OWNER TO msea_admin;

--
-- Name: TABLE quadrat; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.quadrat IS 'A record of observations within an intertidal quadrat.';


--
-- Name: COLUMN quadrat.survey_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.survey_id IS 'A reference to the [intertidal suvey](#intertidal.survey).';


--
-- Name: COLUMN quadrat.name; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.name IS 'A unique name for the quadrat.';


--
-- Name: COLUMN quadrat.flag_number; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.flag_number IS 'The number of the flag used to mark the quadrat. May be non-numeric if necessary.';


--
-- Name: COLUMN quadrat."time"; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat."time" IS 'The time of the quadrat (UTC).';


--
-- Name: COLUMN quadrat.transect; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.transect IS 'The transect number. This is an integer corresponding to whatever rubric was chosen for the survey.';


--
-- Name: COLUMN quadrat.zone; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.zone IS 'The zone number. This is an integer corresponding to whatever rubric was chosen for the survey.';


--
-- Name: COLUMN quadrat.corner_lon1; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lon1 IS 'The corner longitude.';


--
-- Name: COLUMN quadrat.corner_lat1; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lat1 IS 'The corner latitude.';


--
-- Name: COLUMN quadrat.corner_height1; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_height1 IS 'The elevation of the corner position in m (ellipsoidal).';


--
-- Name: COLUMN quadrat.corner_lon2; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lon2 IS 'The corner longitude.';


--
-- Name: COLUMN quadrat.corner_lat2; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lat2 IS 'The corner latitude.';


--
-- Name: COLUMN quadrat.corner_height2; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_height2 IS 'The elevation of the corner position in m (ellipsoidal).';


--
-- Name: COLUMN quadrat.corner_lon3; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lon3 IS 'The corner longitude.';


--
-- Name: COLUMN quadrat.corner_lat3; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lat3 IS 'The corner latitude.';


--
-- Name: COLUMN quadrat.corner_height3; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_height3 IS 'The elevation of the corner position in m (ellipsoidal).';


--
-- Name: COLUMN quadrat.corner_lon4; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lon4 IS 'The corner longitude.';


--
-- Name: COLUMN quadrat.corner_lat4; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_lat4 IS 'The corner latitude.';


--
-- Name: COLUMN quadrat.corner_height4; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.corner_height4 IS 'The elevation of the corner position in m (ellipsoidal).';


--
-- Name: COLUMN quadrat.centroid_lon; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.centroid_lon IS 'The corner longitude.';


--
-- Name: COLUMN quadrat.centroid_lat; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.centroid_lat IS 'The corner latitude.';


--
-- Name: COLUMN quadrat.centroid_height; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.centroid_height IS 'The elevation of the corner position in m (ellipsoidal).';


--
-- Name: COLUMN quadrat.note; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.note IS 'An optional note about the quadrat.';


--
-- Name: COLUMN quadrat.created_on; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.created_on IS 'created_on = models.DateTimeField(default = datetime.utcnow, null = False)';


--
-- Name: COLUMN quadrat.geom; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat.geom IS 'A geometry representing either the corners of the quadrat (multipoint) or the centroid.';


--
-- Name: quadrat_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.quadrat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.quadrat_id_seq OWNER TO msea_admin;

--
-- Name: quadrat_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.quadrat_id_seq OWNED BY intertidal.quadrat.id;


--
-- Name: quadrat_observation; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.quadrat_observation (
    id integer NOT NULL,
    quadrat_id integer NOT NULL,
    surveytaxon_id integer NOT NULL,
    count integer,
    coverage real,
    type integer,
    cell integer,
    length real
);


ALTER TABLE intertidal.quadrat_observation OWNER TO msea_admin;

--
-- Name: TABLE quadrat_observation; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.quadrat_observation IS 'Records observations within [quadrats](#intertidal.quadrat). The observation type enumeration is defined in the database export panel.';


--
-- Name: COLUMN quadrat_observation.quadrat_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_observation.quadrat_id IS 'A link to the [quadrat](#intertidal.quadrat).';


--
-- Name: COLUMN quadrat_observation.surveytaxon_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_observation.surveytaxon_id IS 'A link to the [survey taxon](#intertidal.survey_taxon).';


--
-- Name: COLUMN quadrat_observation.count; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_observation.count IS 'The number of organisms observed.';


--
-- Name: COLUMN quadrat_observation.coverage; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_observation.coverage IS 'The percent coverage by the organism, 0-100. TODO: Not part of the specification.';


--
-- Name: COLUMN quadrat_observation.type; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_observation.type IS 'The type of observation; one of quadrat_obs_type.';


--
-- Name: COLUMN quadrat_observation.cell; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_observation.cell IS 'The quadrat cell, typically from 0 - 100.';


--
-- Name: COLUMN quadrat_observation.length; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_observation.length IS 'The measured length of an organism.';


--
-- Name: quadrat_observation_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.quadrat_observation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.quadrat_observation_id_seq OWNER TO msea_admin;

--
-- Name: quadrat_observation_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.quadrat_observation_id_seq OWNED BY intertidal.quadrat_observation.id;


--
-- Name: quadrat_substrate; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.quadrat_substrate (
    id integer NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE intertidal.quadrat_substrate OWNER TO msea_admin;

--
-- Name: TABLE quadrat_substrate; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.quadrat_substrate IS 'List substrates observed in intertidal quadrats.';


--
-- Name: COLUMN quadrat_substrate.name; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_substrate.name IS 'The name of the substrate.';


--
-- Name: quadrat_substrate_coverage; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.quadrat_substrate_coverage (
    id integer NOT NULL,
    quadrat_id integer NOT NULL,
    substrate_id integer NOT NULL,
    coverage real NOT NULL
);


ALTER TABLE intertidal.quadrat_substrate_coverage OWNER TO msea_admin;

--
-- Name: TABLE quadrat_substrate_coverage; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.quadrat_substrate_coverage IS 'Associates [quadrat substrate](#intertidal.quadrat_substrate) coverages with [quadrats](#intertidal.quadrats).';


--
-- Name: COLUMN quadrat_substrate_coverage.quadrat_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_substrate_coverage.quadrat_id IS 'A reference to the [quadrat](#intertidal.quadrat).';


--
-- Name: COLUMN quadrat_substrate_coverage.substrate_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_substrate_coverage.substrate_id IS 'A reference to the [substrate](#intertidal.quadrat_substrate).';


--
-- Name: COLUMN quadrat_substrate_coverage.coverage; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.quadrat_substrate_coverage.coverage IS 'The coverage amount between 0% and 100%.';


--
-- Name: quadrat_substrate_coverage_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.quadrat_substrate_coverage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.quadrat_substrate_coverage_id_seq OWNER TO msea_admin;

--
-- Name: quadrat_substrate_coverage_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.quadrat_substrate_coverage_id_seq OWNED BY intertidal.quadrat_substrate_coverage.id;


--
-- Name: quadrat_substrate_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.quadrat_substrate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.quadrat_substrate_id_seq OWNER TO msea_admin;

--
-- Name: quadrat_substrate_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.quadrat_substrate_id_seq OWNED BY intertidal.quadrat_substrate.id;


--
-- Name: survey; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.survey (
    id integer NOT NULL,
    site_id integer,
    name character varying(128) NOT NULL,
    objective text,
    start_date timestamp(6) without time zone DEFAULT now() NOT NULL,
    end_date timestamp(6) without time zone,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE intertidal.survey OWNER TO msea_admin;

--
-- Name: TABLE survey; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.survey IS 'Stores metadata about intertidal surveys.';


--
-- Name: COLUMN survey.site_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey.site_id IS 'Relates the survey to a [site](#shared.site) entity.';


--
-- Name: COLUMN survey.name; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey.name IS 'The name of the survey.';


--
-- Name: COLUMN survey.objective; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey.objective IS 'The survey objective.';


--
-- Name: COLUMN survey.start_date; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey.start_date IS 'The start date of the survey.';


--
-- Name: COLUMN survey.end_date; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey.end_date IS 'The end date of the survey.';


--
-- Name: COLUMN survey.note; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey.note IS 'Notes about the survey.';


--
-- Name: COLUMN survey.created_on; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey.created_on IS 'The time of creation of this record.';


--
-- Name: survey_crew; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.survey_crew (
    id integer NOT NULL,
    survey_id integer NOT NULL,
    role_id integer NOT NULL,
    person_id integer NOT NULL,
    note text
);


ALTER TABLE intertidal.survey_crew OWNER TO msea_admin;

--
-- Name: TABLE survey_crew; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.survey_crew IS 'Associates [intertidal survey](#intertidal.survey) crew members with [roles](#intertidal.survey_role).';


--
-- Name: COLUMN survey_crew.survey_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_crew.survey_id IS 'A reference to the [survey](#intertidal.survey).';


--
-- Name: COLUMN survey_crew.role_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_crew.role_id IS 'A reference to the [role](#intertidal.survey_role).';


--
-- Name: COLUMN survey_crew.person_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_crew.person_id IS 'A reference to the person with this [role](#shared.person).';


--
-- Name: COLUMN survey_crew.note; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_crew.note IS 'An optional note about the survey role.';


--
-- Name: survey_crew_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.survey_crew_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.survey_crew_id_seq OWNER TO msea_admin;

--
-- Name: survey_crew_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.survey_crew_id_seq OWNED BY intertidal.survey_crew.id;


--
-- Name: survey_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.survey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.survey_id_seq OWNER TO msea_admin;

--
-- Name: survey_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.survey_id_seq OWNED BY intertidal.survey.id;


--
-- Name: survey_role; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.survey_role (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    note text
);


ALTER TABLE intertidal.survey_role OWNER TO msea_admin;

--
-- Name: TABLE survey_role; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.survey_role IS 'A list of roles available to [intertidal survey crew members](#intertidal.survey_crew).';


--
-- Name: COLUMN survey_role.name; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_role.name IS 'The name of the role.';


--
-- Name: COLUMN survey_role.note; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_role.note IS 'A note about the role.';


--
-- Name: survey_role_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.survey_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.survey_role_id_seq OWNER TO msea_admin;

--
-- Name: survey_role_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.survey_role_id_seq OWNED BY intertidal.survey_role.id;


--
-- Name: survey_taxon; Type: TABLE; Schema: intertidal; Owner: msea_admin
--

CREATE TABLE intertidal.survey_taxon (
    taxon_id integer NOT NULL,
    survey_id integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE intertidal.survey_taxon OWNER TO msea_admin;

--
-- Name: TABLE survey_taxon; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON TABLE intertidal.survey_taxon IS 'Links [taxon](#shared.taxon) records to [intertidal surveys](#intertidal.survey).';


--
-- Name: COLUMN survey_taxon.taxon_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_taxon.taxon_id IS 'A link to the [taxon](#shared.taxon) record.';


--
-- Name: COLUMN survey_taxon.survey_id; Type: COMMENT; Schema: intertidal; Owner: msea_admin
--

COMMENT ON COLUMN intertidal.survey_taxon.survey_id IS 'A link to the [survey](#intertidal.survey).';


--
-- Name: survey_taxon_id_seq; Type: SEQUENCE; Schema: intertidal; Owner: msea_admin
--

CREATE SEQUENCE intertidal.survey_taxon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE intertidal.survey_taxon_id_seq OWNER TO msea_admin;

--
-- Name: survey_taxon_id_seq; Type: SEQUENCE OWNED BY; Schema: intertidal; Owner: msea_admin
--

ALTER SEQUENCE intertidal.survey_taxon_id_seq OWNED BY intertidal.survey_taxon.id;


--
-- Name: geoip_city_blocks; Type: TABLE; Schema: maxmind; Owner: msea_admin
--

CREATE TABLE maxmind.geoip_city_blocks (
    network inet NOT NULL,
    geoname_id integer,
    registered_country_geoname_id integer,
    represented_country_geoname_id integer,
    is_anonymous_proxy boolean,
    is_satellite_provider boolean,
    postal_code character varying(32),
    latitude real,
    longitude real,
    accuracy_radius real,
    is_anycast boolean,
    location public.geography(Point,4326),
    region public.geography(Polygon,4326)
);


ALTER TABLE maxmind.geoip_city_blocks OWNER TO msea_admin;

--
-- Name: TABLE geoip_city_blocks; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON TABLE maxmind.geoip_city_blocks IS 'Contains both IPv4 and IPv6 network addresses, which reference locations in the [geoip_city_locations](#maxmind.geoip_city_locations) table.';


--
-- Name: COLUMN geoip_city_blocks.network; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.network IS 'The network address, IPv4 or IPv6.';


--
-- Name: COLUMN geoip_city_blocks.geoname_id; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.geoname_id IS 'References the location ID in the [geoip_city_locations](#maxmind.geoip_city_locations) table.';


--
-- Name: COLUMN geoip_city_blocks.registered_country_geoname_id; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.registered_country_geoname_id IS 'The location ID in the [geoip_city_locations](#maxmind.geoip_city_locations) table which is registered to the network address.';


--
-- Name: COLUMN geoip_city_blocks.represented_country_geoname_id; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.represented_country_geoname_id IS 'The location ID in the [geoip_city_locations](#maxmind.geoip_city_locations) table from which the IP comes. ';


--
-- Name: COLUMN geoip_city_blocks.is_anonymous_proxy; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.is_anonymous_proxy IS 'True if the address is of an anonymous proxy.';


--
-- Name: COLUMN geoip_city_blocks.is_satellite_provider; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.is_satellite_provider IS 'True if the address is used by a satellite network provider.';


--
-- Name: COLUMN geoip_city_blocks.postal_code; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.postal_code IS 'The postal code of the location.';


--
-- Name: COLUMN geoip_city_blocks.latitude; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.latitude IS 'The latitude of the geographic center of a region, defined by the accuracy radius, which contains the location.';


--
-- Name: COLUMN geoip_city_blocks.longitude; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.longitude IS 'The longitude of the geographic center of a region, defined by the accuracy radius, which contains the location.';


--
-- Name: COLUMN geoip_city_blocks.accuracy_radius; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.accuracy_radius IS 'The radius (m) of a circle within which the location is located.';


--
-- Name: COLUMN geoip_city_blocks.is_anycast; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_blocks.is_anycast IS 'True if the address is that of an Anycast network.';


--
-- Name: geoip_city_locations; Type: TABLE; Schema: maxmind; Owner: msea_admin
--

CREATE TABLE maxmind.geoip_city_locations (
    geoname_id integer NOT NULL,
    locale_code character(2) NOT NULL,
    continent_code character(2) NOT NULL,
    continent_name character varying(32) NOT NULL,
    country_iso_code character(2),
    country_name character varying(64),
    subdivision_1_iso_code character(4),
    subdivision_1_name character varying(256),
    subdivision_2_iso_code character(4),
    subdivision_2_name character varying(256),
    city_name character varying(128),
    metro_code character varying(3),
    time_zone character varying(64),
    is_in_european_union boolean
);


ALTER TABLE maxmind.geoip_city_locations OWNER TO msea_admin;

--
-- Name: TABLE geoip_city_locations; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON TABLE maxmind.geoip_city_locations IS 'Stores the country, region and city data for GeoIP.';


--
-- Name: COLUMN geoip_city_locations.geoname_id; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.geoname_id IS 'The internal GeoIP city ID.';


--
-- Name: COLUMN geoip_city_locations.locale_code; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.locale_code IS 'The two-character local code.';


--
-- Name: COLUMN geoip_city_locations.continent_code; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.continent_code IS 'The two-character ISO continent code.';


--
-- Name: COLUMN geoip_city_locations.continent_name; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.continent_name IS 'The continent name';


--
-- Name: COLUMN geoip_city_locations.country_iso_code; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.country_iso_code IS 'The two-character ISO country code.';


--
-- Name: COLUMN geoip_city_locations.country_name; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.country_name IS 'The country name';


--
-- Name: COLUMN geoip_city_locations.subdivision_1_iso_code; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.subdivision_1_iso_code IS 'The ISO subdivision 1 code.';


--
-- Name: COLUMN geoip_city_locations.subdivision_1_name; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.subdivision_1_name IS 'The ISO subdivision 1 name.';


--
-- Name: COLUMN geoip_city_locations.subdivision_2_iso_code; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.subdivision_2_iso_code IS 'The ISO subdivision 2 code.';


--
-- Name: COLUMN geoip_city_locations.subdivision_2_name; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.subdivision_2_name IS 'The ISO subdivision 2 name.';


--
-- Name: COLUMN geoip_city_locations.city_name; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.city_name IS 'The city name.';


--
-- Name: COLUMN geoip_city_locations.metro_code; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.metro_code IS 'The three-digit metro code.';


--
-- Name: COLUMN geoip_city_locations.time_zone; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.time_zone IS 'The name of the time zone.';


--
-- Name: COLUMN geoip_city_locations.is_in_european_union; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_city_locations.is_in_european_union IS 'True if the location is within the European Union.';


--
-- Name: geoip_last_update; Type: TABLE; Schema: maxmind; Owner: msea_admin
--

CREATE TABLE maxmind.geoip_last_update (
    last_update timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE maxmind.geoip_last_update OWNER TO msea_admin;

--
-- Name: TABLE geoip_last_update; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON TABLE maxmind.geoip_last_update IS 'Stores the time of last update of the database.';


--
-- Name: COLUMN geoip_last_update.last_update; Type: COMMENT; Schema: maxmind; Owner: msea_admin
--

COMMENT ON COLUMN maxmind.geoip_last_update.last_update IS 'The time of last update of the database.';


--
-- Name: cruise; Type: TABLE; Schema: ndst; Owner: msea_admin
--

CREATE TABLE ndst.cruise (
    row_id text,
    name text,
    leg text,
    objective text,
    summary text,
    note text,
    status character varying(16),
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    id integer NOT NULL,
    active boolean DEFAULT false NOT NULL,
    hide integer DEFAULT 0 NOT NULL
);


ALTER TABLE ndst.cruise OWNER TO msea_admin;

--
-- Name: TABLE cruise; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON TABLE ndst.cruise IS 'Contains information about a cruise, as entered by NDST staff. Will be combined with other data to populate the [cruise](#rov.cruise) table.';


--
-- Name: COLUMN cruise.row_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.row_id IS 'A UUID providing a universally-unique identifier for the cruise.';


--
-- Name: COLUMN cruise.name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.name IS 'The name of the cruise.';


--
-- Name: COLUMN cruise.leg; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.leg IS 'The leg of the cruise. Should start at 1 and increase.';


--
-- Name: COLUMN cruise.summary; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.summary IS 'A summary of the cruise, its accomplishments, problems, etc.';


--
-- Name: COLUMN cruise.note; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.note IS 'The objective of the cruise.';


--
-- Name: COLUMN cruise.status; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.status IS 'The current status of the cruise viz. database import.';


--
-- Name: COLUMN cruise.created_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN cruise.updated_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: COLUMN cruise.active; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.active IS 'Set to true if the entity is active, that is, if it''s currently being used. If not, set to false.';


--
-- Name: COLUMN cruise.hide; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.cruise.hide IS 'True if the entity should be hidden in the UI.';


--
-- Name: cruise_id_seq; Type: SEQUENCE; Schema: ndst; Owner: msea_admin
--

CREATE SEQUENCE ndst.cruise_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE ndst.cruise_id_seq OWNER TO msea_admin;

--
-- Name: cruise_id_seq; Type: SEQUENCE OWNED BY; Schema: ndst; Owner: msea_admin
--

ALTER SEQUENCE ndst.cruise_id_seq OWNED BY ndst.cruise.id;


--
-- Name: diveconfig; Type: TABLE; Schema: ndst; Owner: msea_admin
--

CREATE TABLE ndst.diveconfig (
    row_id text,
    name text,
    ship_config text,
    sub_config text,
    ship_instrument_configs text,
    sub_instrument_configs text,
    note text,
    active boolean DEFAULT false NOT NULL,
    id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE ndst.diveconfig OWNER TO msea_admin;

--
-- Name: TABLE diveconfig; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON TABLE ndst.diveconfig IS 'Stores configurations for each dive, including the submersible, ship and any instruments on either.';


--
-- Name: COLUMN diveconfig.row_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.row_id IS 'A UUID providing a universally-unique identifier for the entity.';


--
-- Name: COLUMN diveconfig.name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.name IS 'The name of the dive config.';


--
-- Name: COLUMN diveconfig.ship_config; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.ship_config IS 'Stores the name of the [ship configuration](#ndst.equipconfig) during the dive.';


--
-- Name: COLUMN diveconfig.sub_config; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.sub_config IS 'Stores the name of the [submersible configuration](#ndst.equipconfig) during the dive.';


--
-- Name: COLUMN diveconfig.ship_instrument_configs; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.ship_instrument_configs IS 'Stores the names of the [instruments](#ndst.equipconfig) on the ship during the dive.';


--
-- Name: COLUMN diveconfig.sub_instrument_configs; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.sub_instrument_configs IS 'Stores the names of the [instruments](#ndst.equipconfig) on the sub during the dive.';


--
-- Name: COLUMN diveconfig.note; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.note IS 'A note about the dive config.';


--
-- Name: COLUMN diveconfig.active; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.active IS 'Set to true if the entity is active, that is, if it''s currently being used. If not, set to false.';


--
-- Name: COLUMN diveconfig.created_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN diveconfig.updated_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.diveconfig.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: diveconfig_id_seq; Type: SEQUENCE; Schema: ndst; Owner: msea_admin
--

CREATE SEQUENCE ndst.diveconfig_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE ndst.diveconfig_id_seq OWNER TO msea_admin;

--
-- Name: diveconfig_id_seq; Type: SEQUENCE OWNED BY; Schema: ndst; Owner: msea_admin
--

ALTER SEQUENCE ndst.diveconfig_id_seq OWNED BY ndst.diveconfig.id;


--
-- Name: dives; Type: TABLE; Schema: ndst; Owner: msea_admin
--

CREATE TABLE ndst.dives (
    row_id text,
    cruise_name text,
    leg text,
    name text,
    pilot text,
    start_time text,
    end_time text,
    site_name text,
    dive_config text,
    objective text,
    summary text,
    note text,
    active boolean DEFAULT false NOT NULL,
    hide integer DEFAULT 0 NOT NULL,
    id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE ndst.dives OWNER TO msea_admin;

--
-- Name: TABLE dives; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON TABLE ndst.dives IS 'Stores information about each dive.';


--
-- Name: COLUMN dives.row_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.row_id IS 'A UUID providing a universally-unique identifier for the entity.';


--
-- Name: COLUMN dives.cruise_name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.cruise_name IS 'The name of the [cruise](#ndst.cruise).';


--
-- Name: COLUMN dives.leg; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.leg IS 'The leg of the [cruise](#ndst.cruise).';


--
-- Name: COLUMN dives.name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.name IS 'The name of the dive.';


--
-- Name: COLUMN dives.pilot; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.pilot IS 'The [pilot](#ndst.person)(s) of the dive.';


--
-- Name: COLUMN dives.start_time; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.start_time IS 'The time of dive start.';


--
-- Name: COLUMN dives.end_time; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.end_time IS 'The time of dive ending.';


--
-- Name: COLUMN dives.site_name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.site_name IS 'The name of the dive site.';


--
-- Name: COLUMN dives.dive_config; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.dive_config IS 'The name of the [dive configuration](#ndst.diveconfig).';


--
-- Name: COLUMN dives.objective; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.objective IS 'The objective of the dive.';


--
-- Name: COLUMN dives.summary; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.summary IS 'A summary of the dive, its accomplishments, problems, etc.';


--
-- Name: COLUMN dives.note; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.note IS 'A note about the dive.';


--
-- Name: COLUMN dives.active; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.active IS 'Set to true if the entity is active, that is, if it''s currently being used. If not, set to false.';


--
-- Name: COLUMN dives.hide; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.hide IS 'True if the entity should be hidden in the UI.';


--
-- Name: COLUMN dives.created_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN dives.updated_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.dives.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: dives_id_seq; Type: SEQUENCE; Schema: ndst; Owner: msea_admin
--

CREATE SEQUENCE ndst.dives_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE ndst.dives_id_seq OWNER TO msea_admin;

--
-- Name: dives_id_seq; Type: SEQUENCE OWNED BY; Schema: ndst; Owner: msea_admin
--

ALTER SEQUENCE ndst.dives_id_seq OWNED BY ndst.dives.id;


--
-- Name: equipconfig; Type: TABLE; Schema: ndst; Owner: msea_admin
--

CREATE TABLE ndst.equipconfig (
    row_id text,
    name text,
    short_code text,
    type text,
    configuration text,
    note text,
    active boolean DEFAULT false NOT NULL,
    id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE ndst.equipconfig OWNER TO msea_admin;

--
-- Name: TABLE equipconfig; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON TABLE ndst.equipconfig IS 'Stores configurations for [equipment](#ndst.equipment), including instruments and platforms.';


--
-- Name: COLUMN equipconfig.row_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.row_id IS 'A UUID providing a universally-unique identifier for the entity.';


--
-- Name: COLUMN equipconfig.name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.name IS 'The name of the equipment config.';


--
-- Name: COLUMN equipconfig.short_code; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.short_code IS 'A short, easy-to-use identifier for the configuration.';


--
-- Name: COLUMN equipconfig.type; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.type IS 'The type of equipment.';


--
-- Name: COLUMN equipconfig.configuration; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.configuration IS 'A JSON object containing configuration properties.';


--
-- Name: COLUMN equipconfig.note; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.note IS 'A note about the equipment config.';


--
-- Name: COLUMN equipconfig.active; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.active IS 'Set to true if the entity is active, that is, if it''s currently being used. If not, set to false.';


--
-- Name: COLUMN equipconfig.created_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN equipconfig.updated_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipconfig.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: equipconfig_id_seq; Type: SEQUENCE; Schema: ndst; Owner: msea_admin
--

CREATE SEQUENCE ndst.equipconfig_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE ndst.equipconfig_id_seq OWNER TO msea_admin;

--
-- Name: equipconfig_id_seq; Type: SEQUENCE OWNED BY; Schema: ndst; Owner: msea_admin
--

ALTER SEQUENCE ndst.equipconfig_id_seq OWNED BY ndst.equipconfig.id;


--
-- Name: equipment; Type: TABLE; Schema: ndst; Owner: msea_admin
--

CREATE TABLE ndst.equipment (
    row_id text,
    short_code text,
    brand text,
    model text,
    serial_number text,
    type text,
    note text,
    instrument_id integer,
    platform_id integer,
    short_code_mapped character varying(64),
    active boolean DEFAULT false NOT NULL,
    id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE ndst.equipment OWNER TO msea_admin;

--
-- Name: TABLE equipment; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON TABLE ndst.equipment IS 'Stores equipment, including instruments and platforms.';


--
-- Name: COLUMN equipment.row_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.row_id IS 'A UUID providing a universally-unique identifier for the entity.';


--
-- Name: COLUMN equipment.short_code; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.short_code IS 'The short code for the equipment as used in the [ROV database equipment table](#rov.equipment).';


--
-- Name: COLUMN equipment.brand; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.brand IS 'The brand name of the equipment.';


--
-- Name: COLUMN equipment.model; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.model IS 'The model name of the equipment.';


--
-- Name: COLUMN equipment.serial_number; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.serial_number IS 'The serial number.';


--
-- Name: COLUMN equipment.type; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.type IS 'The type of equipment.';


--
-- Name: COLUMN equipment.note; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.note IS 'A note about the equipment.';


--
-- Name: COLUMN equipment.instrument_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.instrument_id IS 'The ID of the [instrument](#rov.instrument) in the ROV database. Mutually exclusive with platform_id.';


--
-- Name: COLUMN equipment.platform_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.platform_id IS 'The ID of the [platform](#rov.platform) in the ROV database. Mutually exclusive with instrument_id.';


--
-- Name: COLUMN equipment.short_code_mapped; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.short_code_mapped IS 'An optional mapping from the origin short code to an internal short code. Not unique because multiple source items can map to a single internal item.';


--
-- Name: COLUMN equipment.active; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.active IS 'Set to true if the entity is active, that is, if it''s currently being used. If not, set to false.';


--
-- Name: COLUMN equipment.created_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN equipment.updated_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.equipment.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: equipment_id_seq; Type: SEQUENCE; Schema: ndst; Owner: msea_admin
--

CREATE SEQUENCE ndst.equipment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE ndst.equipment_id_seq OWNER TO msea_admin;

--
-- Name: equipment_id_seq; Type: SEQUENCE OWNED BY; Schema: ndst; Owner: msea_admin
--

ALTER SEQUENCE ndst.equipment_id_seq OWNED BY ndst.equipment.id;


--
-- Name: people; Type: TABLE; Schema: ndst; Owner: msea_admin
--

CREATE TABLE ndst.people (
    row_id text,
    initials text,
    first_name text,
    last_name text,
    email text,
    person_id integer,
    active boolean DEFAULT false NOT NULL,
    id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE ndst.people OWNER TO msea_admin;

--
-- Name: TABLE people; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON TABLE ndst.people IS 'Stores the people who worked on the [cruise](#ndst.cruise).';


--
-- Name: COLUMN people.row_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.row_id IS 'A UUID providing a universally-unique identifier for the entity.';


--
-- Name: COLUMN people.initials; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.initials IS 'The initials. Used like a short code for the person.';


--
-- Name: COLUMN people.first_name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.first_name IS 'The first name.';


--
-- Name: COLUMN people.last_name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.last_name IS 'The last name.';


--
-- Name: COLUMN people.email; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.email IS 'The email address.';


--
-- Name: COLUMN people.person_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.person_id IS 'The ID of the [person](#rov.person) in the ROV database.';


--
-- Name: COLUMN people.active; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.active IS 'Set to true if the entity is active, that is, if it''s currently being used. If not, set to false.';


--
-- Name: COLUMN people.created_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN people.updated_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.people.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: people_id_seq; Type: SEQUENCE; Schema: ndst; Owner: msea_admin
--

CREATE SEQUENCE ndst.people_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE ndst.people_id_seq OWNER TO msea_admin;

--
-- Name: people_id_seq; Type: SEQUENCE OWNED BY; Schema: ndst; Owner: msea_admin
--

ALTER SEQUENCE ndst.people_id_seq OWNED BY ndst.people.id;


--
-- Name: transects; Type: TABLE; Schema: ndst; Owner: msea_admin
--

CREATE TABLE ndst.transects (
    row_id text,
    cruise_name text,
    leg text,
    dive_name text,
    name text,
    start_time text,
    end_time text,
    objective text,
    summary text,
    note text,
    active boolean DEFAULT false NOT NULL,
    hide integer DEFAULT 0 NOT NULL,
    id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE ndst.transects OWNER TO msea_admin;

--
-- Name: TABLE transects; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON TABLE ndst.transects IS 'Stores information about each transect.';


--
-- Name: COLUMN transects.row_id; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.row_id IS 'A UUID providing a universally-unique identifier for the entity.';


--
-- Name: COLUMN transects.cruise_name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.cruise_name IS 'The name of the [cruise](#ndst.cruise).';


--
-- Name: COLUMN transects.leg; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.leg IS 'The leg of the [cruise](#ndst.cruise).';


--
-- Name: COLUMN transects.dive_name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.dive_name IS 'The name of the dive.';


--
-- Name: COLUMN transects.name; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.name IS 'The name of the transect.';


--
-- Name: COLUMN transects.start_time; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.start_time IS 'The time of transect start.';


--
-- Name: COLUMN transects.end_time; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.end_time IS 'The time of transect ending.';


--
-- Name: COLUMN transects.objective; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.objective IS 'The objective of the transect.';


--
-- Name: COLUMN transects.summary; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.summary IS 'A summary of the transect, its accomplishments, problems, etc.';


--
-- Name: COLUMN transects.note; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.note IS 'A note about the transect.';


--
-- Name: COLUMN transects.active; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.active IS 'Set to true if the entity is active, that is, if it''s currently being used. If not, set to false.';


--
-- Name: COLUMN transects.hide; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.hide IS 'True if the entity should be hidden in the UI.';


--
-- Name: COLUMN transects.created_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN transects.updated_on; Type: COMMENT; Schema: ndst; Owner: msea_admin
--

COMMENT ON COLUMN ndst.transects.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: transects_id_seq; Type: SEQUENCE; Schema: ndst; Owner: msea_admin
--

CREATE SEQUENCE ndst.transects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE ndst.transects_id_seq OWNER TO msea_admin;

--
-- Name: transects_id_seq; Type: SEQUENCE OWNED BY; Schema: ndst; Owner: msea_admin
--

ALTER SEQUENCE ndst.transects_id_seq OWNED BY ndst.transects.id;


--
-- Name: protected_area_data_object; Type: TABLE; Schema: pa; Owner: msea_admin
--

CREATE TABLE pa.protected_area_data_object (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    description text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    doi character varying(64),
    isbn character varying(64),
    protected_area_id integer
);


ALTER TABLE pa.protected_area_data_object OWNER TO msea_admin;

--
-- Name: TABLE protected_area_data_object; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON TABLE pa.protected_area_data_object IS 'A data object related to an [RCA](#rca.rca). This is the abstract, or top-level object, which may contain one or more physical objects (files) or documents.';


--
-- Name: COLUMN protected_area_data_object.name; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object.name IS 'The name of the data object.';


--
-- Name: COLUMN protected_area_data_object.description; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object.description IS 'A description of the object.';


--
-- Name: COLUMN protected_area_data_object.created_on; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object.created_on IS 'The time of creation of the record.';


--
-- Name: COLUMN protected_area_data_object.updated_on; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object.updated_on IS 'The last update time of the object.';


--
-- Name: COLUMN protected_area_data_object.doi; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object.doi IS 'The DOI of the object.';


--
-- Name: COLUMN protected_area_data_object.isbn; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object.isbn IS 'The ISBN of the object.';


--
-- Name: COLUMN protected_area_data_object.protected_area_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object.protected_area_id IS 'A link to the [protected area](#pa.protected_area).';


--
-- Name: data_object_id_seq; Type: SEQUENCE; Schema: pa; Owner: msea_admin
--

CREATE SEQUENCE pa.data_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE pa.data_object_id_seq OWNER TO msea_admin;

--
-- Name: data_object_id_seq; Type: SEQUENCE OWNED BY; Schema: pa; Owner: msea_admin
--

ALTER SEQUENCE pa.data_object_id_seq OWNED BY pa.protected_area_data_object.id;


--
-- Name: mpa; Type: TABLE; Schema: pa; Owner: msea_admin
--

CREATE TABLE pa.mpa (
    ogc_fid integer NOT NULL,
    geometry public.geometry(MultiPolygon,4326),
    objectid bigint NOT NULL,
    parent_id integer,
    name_e character varying,
    name_f character varying,
    name_ind character varying,
    zone_id integer,
    zonedesc_e character varying,
    zonedesc_f character varying,
    biome character varying,
    jur_id character varying,
    aichi_t11 character varying,
    iucn_cat character varying,
    oecm character varying,
    o_area double precision,
    loc_e character varying,
    loc_f character varying,
    type_e character varying,
    type_f character varying,
    mgmt_e character varying,
    mgmt_f character varying,
    gov_type character varying,
    legisl_e character varying,
    legisl_f character varying,
    status_e character varying,
    status_f character varying,
    protdate integer,
    delisdate integer,
    owner_e character varying,
    owner_f character varying,
    subs_right character varying,
    comments character varying,
    url character varying,
    shape_length double precision,
    shape_area double precision,
    geom public.geography
);


ALTER TABLE pa.mpa OWNER TO msea_admin;

--
-- Name: mpa_data_object; Type: TABLE; Schema: pa; Owner: msea_admin
--

CREATE TABLE pa.mpa_data_object (
    id integer NOT NULL,
    mpa_id integer NOT NULL,
    name character varying(128) NOT NULL,
    description text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    doi character varying(64),
    isbn character varying(64)
);


ALTER TABLE pa.mpa_data_object OWNER TO msea_admin;

--
-- Name: TABLE mpa_data_object; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON TABLE pa.mpa_data_object IS 'A data object related to an [MPA](#mpa.mpa). This is the abstract, or top-level object, which may contain one or more physical objects (files) or documents.';


--
-- Name: COLUMN mpa_data_object.mpa_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object.mpa_id IS 'A reference to the [MPA](#mpa.mpa) record.';


--
-- Name: COLUMN mpa_data_object.name; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object.name IS 'The name of the data object.';


--
-- Name: COLUMN mpa_data_object.description; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object.description IS 'A description of the object.';


--
-- Name: COLUMN mpa_data_object.created_on; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object.created_on IS 'The time of creation of the record.';


--
-- Name: COLUMN mpa_data_object.updated_on; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object.updated_on IS 'The last update time of the object.';


--
-- Name: COLUMN mpa_data_object.doi; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object.doi IS 'The DOI of the object.';


--
-- Name: COLUMN mpa_data_object.isbn; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object.isbn IS 'The ISBN of the object.';


--
-- Name: mpa_data_object_file; Type: TABLE; Schema: pa; Owner: msea_admin
--

CREATE TABLE pa.mpa_data_object_file (
    id integer NOT NULL,
    data_object_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE pa.mpa_data_object_file OWNER TO msea_admin;

--
-- Name: TABLE mpa_data_object_file; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON TABLE pa.mpa_data_object_file IS 'Gives the ability for more than one [data object](#mpa.data_object) to own a [file](#shared.file) and vice versa.';


--
-- Name: COLUMN mpa_data_object_file.data_object_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object_file.data_object_id IS 'A reference to the [data object](#mpa.data_object).';


--
-- Name: COLUMN mpa_data_object_file.file_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.mpa_data_object_file.file_id IS 'A reference to the [file](#shared.file).';


--
-- Name: mpa_data_object_file_id_seq; Type: SEQUENCE; Schema: pa; Owner: msea_admin
--

CREATE SEQUENCE pa.mpa_data_object_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE pa.mpa_data_object_file_id_seq OWNER TO msea_admin;

--
-- Name: mpa_data_object_file_id_seq; Type: SEQUENCE OWNED BY; Schema: pa; Owner: msea_admin
--

ALTER SEQUENCE pa.mpa_data_object_file_id_seq OWNED BY pa.mpa_data_object_file.id;


--
-- Name: mpa_data_object_id_seq; Type: SEQUENCE; Schema: pa; Owner: msea_admin
--

CREATE SEQUENCE pa.mpa_data_object_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE pa.mpa_data_object_id_seq OWNER TO msea_admin;

--
-- Name: mpa_data_object_id_seq; Type: SEQUENCE OWNED BY; Schema: pa; Owner: msea_admin
--

ALTER SEQUENCE pa.mpa_data_object_id_seq OWNED BY pa.mpa_data_object.id;


--
-- Name: mpas_ogc_fid_seq; Type: SEQUENCE; Schema: pa; Owner: msea_admin
--

CREATE SEQUENCE pa.mpas_ogc_fid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE pa.mpas_ogc_fid_seq OWNER TO msea_admin;

--
-- Name: mpas_ogc_fid_seq; Type: SEQUENCE OWNED BY; Schema: pa; Owner: msea_admin
--

ALTER SEQUENCE pa.mpas_ogc_fid_seq OWNED BY pa.mpa.ogc_fid;


--
-- Name: protected_area; Type: TABLE; Schema: pa; Owner: msea_admin
--

CREATE TABLE pa.protected_area (
    id integer NOT NULL,
    type character varying(32) NOT NULL,
    name_e character varying(256) NOT NULL,
    name_f character varying(256) NOT NULL,
    original_id integer,
    area real,
    year_created integer,
    note text,
    geom public.geography NOT NULL
);


ALTER TABLE pa.protected_area OWNER TO msea_admin;

--
-- Name: TABLE protected_area; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON TABLE pa.protected_area IS 'A unified table for protected areas.';


--
-- Name: COLUMN protected_area.type; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.type IS 'The type of protected area, including "RCA", "MPA", etc.';


--
-- Name: COLUMN protected_area.name_e; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.name_e IS 'The English name of the protected area.';


--
-- Name: COLUMN protected_area.name_f; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.name_f IS 'The French name of the protected area.';


--
-- Name: COLUMN protected_area.original_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.original_id IS 'The original ID of the protected area, from the source database.';


--
-- Name: COLUMN protected_area.area; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.area IS 'The nominal area in sq. km of the protected area.';


--
-- Name: COLUMN protected_area.year_created; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.year_created IS 'The year of creation of the protected area.';


--
-- Name: COLUMN protected_area.note; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.note IS 'An optional note about the protected area.';


--
-- Name: COLUMN protected_area.geom; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area.geom IS 'The boundary geometry of the protected area.';


--
-- Name: data_object_file_id_seq; Type: SEQUENCE; Schema: public; Owner: msea_admin
--

CREATE SEQUENCE public.data_object_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.data_object_file_id_seq OWNER TO msea_admin;

--
-- Name: protected_area_data_object_file; Type: TABLE; Schema: pa; Owner: msea_admin
--

CREATE TABLE pa.protected_area_data_object_file (
    id integer DEFAULT nextval('public.data_object_file_id_seq'::regclass) NOT NULL,
    data_object_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE pa.protected_area_data_object_file OWNER TO msea_admin;

--
-- Name: TABLE protected_area_data_object_file; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON TABLE pa.protected_area_data_object_file IS 'Gives the ability for more than one [data object](#rca.data_object) to own a [file](#shared.file) and vice versa.';


--
-- Name: COLUMN protected_area_data_object_file.data_object_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object_file.data_object_id IS 'A reference to the [data object](#rca.data_object).';


--
-- Name: COLUMN protected_area_data_object_file.file_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.protected_area_data_object_file.file_id IS 'A reference to the [file](#shared.file).';


--
-- Name: protected_area_id_seq; Type: SEQUENCE; Schema: pa; Owner: msea_admin
--

CREATE SEQUENCE pa.protected_area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE pa.protected_area_id_seq OWNER TO msea_admin;

--
-- Name: protected_area_id_seq; Type: SEQUENCE OWNED BY; Schema: pa; Owner: msea_admin
--

ALTER SEQUENCE pa.protected_area_id_seq OWNED BY pa.protected_area.id;


--
-- Name: rca; Type: TABLE; Schema: pa; Owner: msea_admin
--

CREATE TABLE pa.rca (
    id integer NOT NULL,
    rca_id double precision,
    name character varying(40),
    description text,
    yr_created smallint,
    formerid smallint,
    hectares real,
    sq_km real,
    area real,
    len real,
    geom public.geography
);


ALTER TABLE pa.rca OWNER TO msea_admin;

--
-- Name: TABLE rca; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON TABLE pa.rca IS 'Stores basic information about RCAs along with a boundary geometry.';


--
-- Name: COLUMN rca.rca_id; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.rca_id IS 'The RCA ID as defined by the provider.';


--
-- Name: COLUMN rca.name; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.name IS 'The name of the RCA.';


--
-- Name: COLUMN rca.description; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.description IS 'A description of the RCA.';


--
-- Name: COLUMN rca.yr_created; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.yr_created IS 'The year the RCA was created (from source).';


--
-- Name: COLUMN rca.formerid; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.formerid IS 'The former ID of the RCA (from source).';


--
-- Name: COLUMN rca.hectares; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.hectares IS 'The number of hectares in the RCA (from source).';


--
-- Name: COLUMN rca.sq_km; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.sq_km IS 'The area of the RCA (from source).';


--
-- Name: COLUMN rca.area; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.area IS 'The area of the RCA (from source).';


--
-- Name: COLUMN rca.len; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.len IS 'The length (?) of the RCA (from source).';


--
-- Name: COLUMN rca.geom; Type: COMMENT; Schema: pa; Owner: msea_admin
--

COMMENT ON COLUMN pa.rca.geom IS 'The RCA geography.';


--
-- Name: rca_id_seq; Type: SEQUENCE; Schema: pa; Owner: msea_admin
--

CREATE SEQUENCE pa.rca_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE pa.rca_id_seq OWNER TO msea_admin;

--
-- Name: rca_id_seq; Type: SEQUENCE OWNED BY; Schema: pa; Owner: msea_admin
--

ALTER SEQUENCE pa.rca_id_seq OWNED BY pa.rca.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO msea;

--
-- Name: TABLE auth_group; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.auth_group IS 'A Django table for authorization groups.';


--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auth_group_id_seq OWNER TO msea;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO msea;

--
-- Name: TABLE auth_group_permissions; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.auth_group_permissions IS 'A Django table for permissions on authorization groups.';


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auth_group_permissions_id_seq OWNER TO msea;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO msea;

--
-- Name: TABLE auth_permission; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.auth_permission IS 'A Django table for available permissions.';


--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auth_permission_id_seq OWNER TO msea;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    biigle_username character varying(32),
    biigle_api_key character varying(32),
    organization character varying(256),
    org_type character varying(32),
    registration_reason text,
    bio text,
    verification_code character(32),
    verification_expiry timestamp without time zone,
    verification_time timestamp without time zone,
    registration_ip inet,
    ip_in_region boolean,
    registration_note text,
    allowed boolean DEFAULT true NOT NULL,
    password_reset_code character(32),
    password_reset_expiry timestamp without time zone,
    registration_location character varying(256)
);


ALTER TABLE public.auth_user OWNER TO msea;

--
-- Name: TABLE auth_user; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.auth_user IS 'A Django table for users.';


--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO msea;

--
-- Name: TABLE auth_user_groups; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.auth_user_groups IS 'A Django table to relate users to groups.';


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auth_user_groups_id_seq OWNER TO msea;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auth_user_id_seq OWNER TO msea;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO msea;

--
-- Name: TABLE auth_user_user_permissions; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.auth_user_user_permissions IS 'A Django for user permissions.';


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNER TO msea;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO msea;

--
-- Name: TABLE django_admin_log; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.django_admin_log IS 'A Django table for admin logging.';


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.django_admin_log_id_seq OWNER TO msea;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO msea;

--
-- Name: TABLE django_content_type; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.django_content_type IS 'A Django table for content types.';


--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.django_content_type_id_seq OWNER TO msea;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO msea;

--
-- Name: TABLE django_migrations; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.django_migrations IS 'Records migrations applied to database.';


--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: msea
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.django_migrations_id_seq OWNER TO msea;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: msea
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO msea;

--
-- Name: TABLE django_session; Type: COMMENT; Schema: public; Owner: msea
--

COMMENT ON TABLE public.django_session IS 'Django session management.';


--
-- Name: django_site; Type: TABLE; Schema: public; Owner: msea_admin
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO msea_admin;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: msea_admin
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.django_site_id_seq OWNER TO msea_admin;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea_admin
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: knox_authtoken; Type: TABLE; Schema: public; Owner: msea_admin
--

CREATE TABLE public.knox_authtoken (
    digest character varying(128) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    expiry timestamp with time zone,
    token_key character varying(32) NOT NULL
);


ALTER TABLE public.knox_authtoken OWNER TO msea_admin;

--
-- Name: nyt_notification; Type: TABLE; Schema: public; Owner: msea_admin
--

CREATE TABLE public.nyt_notification (
    id integer NOT NULL,
    message text NOT NULL,
    url character varying(200),
    is_viewed boolean NOT NULL,
    is_emailed boolean NOT NULL,
    created timestamp with time zone NOT NULL,
    occurrences integer NOT NULL,
    subscription_id integer,
    user_id integer,
    modified timestamp with time zone NOT NULL,
    CONSTRAINT nyt_notification_occurrences_check CHECK ((occurrences >= 0))
);


ALTER TABLE public.nyt_notification OWNER TO msea_admin;

--
-- Name: nyt_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: msea_admin
--

CREATE SEQUENCE public.nyt_notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.nyt_notification_id_seq OWNER TO msea_admin;

--
-- Name: nyt_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea_admin
--

ALTER SEQUENCE public.nyt_notification_id_seq OWNED BY public.nyt_notification.id;


--
-- Name: nyt_notificationtype; Type: TABLE; Schema: public; Owner: msea_admin
--

CREATE TABLE public.nyt_notificationtype (
    key character varying(128) NOT NULL,
    label character varying(128),
    content_type_id integer
);


ALTER TABLE public.nyt_notificationtype OWNER TO msea_admin;

--
-- Name: nyt_settings; Type: TABLE; Schema: public; Owner: msea_admin
--

CREATE TABLE public.nyt_settings (
    id integer NOT NULL,
    user_id integer NOT NULL,
    "interval" smallint NOT NULL,
    is_default boolean NOT NULL
);


ALTER TABLE public.nyt_settings OWNER TO msea_admin;

--
-- Name: nyt_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: msea_admin
--

CREATE SEQUENCE public.nyt_settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.nyt_settings_id_seq OWNER TO msea_admin;

--
-- Name: nyt_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea_admin
--

ALTER SEQUENCE public.nyt_settings_id_seq OWNED BY public.nyt_settings.id;


--
-- Name: nyt_subscription; Type: TABLE; Schema: public; Owner: msea_admin
--

CREATE TABLE public.nyt_subscription (
    id integer NOT NULL,
    settings_id integer NOT NULL,
    notification_type_id character varying(128) NOT NULL,
    object_id character varying(64),
    send_emails boolean NOT NULL,
    latest_id integer
);


ALTER TABLE public.nyt_subscription OWNER TO msea_admin;

--
-- Name: nyt_subscription_id_seq; Type: SEQUENCE; Schema: public; Owner: msea_admin
--

CREATE SEQUENCE public.nyt_subscription_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.nyt_subscription_id_seq OWNER TO msea_admin;

--
-- Name: nyt_subscription_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: msea_admin
--

ALTER SEQUENCE public.nyt_subscription_id_seq OWNED BY public.nyt_subscription.id;


--
-- Name: thumbnail_kvstore; Type: TABLE; Schema: public; Owner: msea_admin
--

CREATE TABLE public.thumbnail_kvstore (
    key character varying(200) NOT NULL,
    value text NOT NULL
);


ALTER TABLE public.thumbnail_kvstore OWNER TO msea_admin;

--
-- Name: abundance; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.abundance (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    rank integer,
    source character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.abundance OWNER TO msea_admin;

--
-- Name: TABLE abundance; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.abundance IS 'Stores a list of abundance labels from the ACFOR scale.';


--
-- Name: COLUMN abundance.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.abundance.name IS 'A descriptive label for the abundance level.';


--
-- Name: COLUMN abundance.rank; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.abundance.rank IS 'A rank used to objectively interpret the abundance label as an ordinal rank. It doesn''t matter what the values are so long as they increase monotonically and do not overlap.';


--
-- Name: COLUMN abundance.source; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.abundance.source IS 'A source label to distinguish abundance labels with the same names. TODO: Provisional, pending determination of how abundance codes are handled.';


--
-- Name: COLUMN abundance.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.abundance.note IS 'An optional note about the abundance level.';


--
-- Name: COLUMN abundance.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.abundance.short_code IS 'Contains a short code that can be used to look up an abundance (e.g., during import) without relying on the primary key.';


--
-- Name: abundance_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.abundance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.abundance_id_seq OWNER TO msea_admin;

--
-- Name: abundance_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.abundance_id_seq OWNED BY rov.abundance.id;


--
-- Name: annotation_job; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.annotation_job (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    objective text,
    note text,
    start_date date,
    end_date date,
    created_on timestamp without time zone DEFAULT now(),
    updated_on timestamp without time zone DEFAULT now()
);


ALTER TABLE rov.annotation_job OWNER TO msea_admin;

--
-- Name: TABLE annotation_job; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.annotation_job IS 'Stores information about the annotation projects that generate information from photo and video media.';


--
-- Name: COLUMN annotation_job.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job.name IS 'A name for the annotation project.';


--
-- Name: COLUMN annotation_job.objective; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job.objective IS 'A statement of the objectives of the annotation project.';


--
-- Name: COLUMN annotation_job.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job.note IS 'Operational notes about the annotation project.';


--
-- Name: COLUMN annotation_job.start_date; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job.start_date IS 'The starting date of the project.';


--
-- Name: COLUMN annotation_job.end_date; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job.end_date IS 'The ending date of the project.';


--
-- Name: COLUMN annotation_job.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job.created_on IS 'The date of creation of the entity.';


--
-- Name: COLUMN annotation_job.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job.updated_on IS 'The date of last modification of the entity.';


--
-- Name: annotation_job_annotation_protocol; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.annotation_job_annotation_protocol (
    id integer NOT NULL,
    annotation_job_id integer NOT NULL,
    annotation_protocol_id integer NOT NULL
);


ALTER TABLE rov.annotation_job_annotation_protocol OWNER TO msea_admin;

--
-- Name: TABLE annotation_job_annotation_protocol; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.annotation_job_annotation_protocol IS 'Associates the annotation job with one or more [annotation protocols](#rov.annotation_protocol), which are used to direct annotation activities.';


--
-- Name: COLUMN annotation_job_annotation_protocol.annotation_job_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job_annotation_protocol.annotation_job_id IS 'The ID of the [annotation job](#rov.annotation_job).';


--
-- Name: COLUMN annotation_job_annotation_protocol.annotation_protocol_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job_annotation_protocol.annotation_protocol_id IS 'The ID of the [annotation protocol](#rov.annotation_protocol).';


--
-- Name: annotation_job_annotation_protocol_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.annotation_job_annotation_protocol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.annotation_job_annotation_protocol_id_seq OWNER TO msea_admin;

--
-- Name: annotation_job_annotation_protocol_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.annotation_job_annotation_protocol_id_seq OWNED BY rov.annotation_job_annotation_protocol.id;


--
-- Name: annotation_job_crew; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.annotation_job_crew (
    id integer NOT NULL,
    annotation_job_id integer NOT NULL,
    person_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE rov.annotation_job_crew OWNER TO msea_admin;

--
-- Name: TABLE annotation_job_crew; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.annotation_job_crew IS 'Associates crew members with an [annotation job](#rov.annotation_job) with their respective [roles](#rov.annotation_job_role).';


--
-- Name: COLUMN annotation_job_crew.annotation_job_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job_crew.annotation_job_id IS 'The ID of the [annotation job](#rov.annotation_job).';


--
-- Name: COLUMN annotation_job_crew.person_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job_crew.person_id IS 'The ID of the [crew member](#shared.person).';


--
-- Name: COLUMN annotation_job_crew.role_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job_crew.role_id IS 'The ID of the [role](#rov.annotation_job_role).';


--
-- Name: annotation_job_crew_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.annotation_job_crew_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.annotation_job_crew_id_seq OWNER TO msea_admin;

--
-- Name: annotation_job_crew_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.annotation_job_crew_id_seq OWNED BY rov.annotation_job_crew.id;


--
-- Name: annotation_job_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.annotation_job_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.annotation_job_id_seq OWNER TO msea_admin;

--
-- Name: annotation_job_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.annotation_job_id_seq OWNED BY rov.annotation_job.id;


--
-- Name: annotation_job_role; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.annotation_job_role (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text
);


ALTER TABLE rov.annotation_job_role OWNER TO msea_admin;

--
-- Name: TABLE annotation_job_role; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.annotation_job_role IS 'Stores the possible roles one can perform on an [annotation job](#rov.annotation_job).';


--
-- Name: COLUMN annotation_job_role.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job_role.name IS 'The name of the role.';


--
-- Name: COLUMN annotation_job_role.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_job_role.note IS 'A descript of the role.';


--
-- Name: annotation_job_role_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.annotation_job_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.annotation_job_role_id_seq OWNER TO msea_admin;

--
-- Name: annotation_job_role_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.annotation_job_role_id_seq OWNED BY rov.annotation_job_role.id;


--
-- Name: annotation_protocol; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.annotation_protocol (
    id integer NOT NULL,
    medium_type_id integer NOT NULL,
    annotation_software_id integer NOT NULL,
    name character varying(255) NOT NULL,
    is_template boolean DEFAULT false NOT NULL,
    image_interval real,
    image_overlap boolean,
    habitat_only boolean,
    species_guide character varying(255),
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    creator_id integer,
    invertebrate_species character varying(16),
    fish_species character varying(16),
    algae_species character varying(16),
    biogenic_habitat boolean,
    protocol_document character varying(256),
    observation_interval real,
    habitat_interval real,
    fov_interval real,
    image_interval_unit character varying(2) DEFAULT 's'::character varying NOT NULL,
    observation_interval_unit character varying(2) DEFAULT 's'::character varying NOT NULL,
    habitat_interval_unit character varying(2) DEFAULT 's'::character varying NOT NULL,
    fov_interval_unit character varying(2) DEFAULT 's'::character varying NOT NULL
);


ALTER TABLE rov.annotation_protocol OWNER TO msea_admin;

--
-- Name: TABLE annotation_protocol; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.annotation_protocol IS 'A table to record annotation protocols for annotation projects. Stores information such as the author of the protocol, the observation interval, the medium used and the types of observations to be made.';


--
-- Name: COLUMN annotation_protocol.medium_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.medium_type_id IS 'The type of media used for annotation. Looked up in [media type](#rov.medium_type) table. ';


--
-- Name: COLUMN annotation_protocol.annotation_software_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.annotation_software_id IS 'Software used to annotate the video or images. Looked up in the [annotation software](#rov.annotation_software) table.';


--
-- Name: COLUMN annotation_protocol.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.name IS 'The name of the annotation protocol. Should be unique.';


--
-- Name: COLUMN annotation_protocol.is_template; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.is_template IS 'If this is meant to be a template that is copied for use, mark this column `true`.';


--
-- Name: COLUMN annotation_protocol.image_interval; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.image_interval IS 'The interval between images or frame grabs. Typically 3 to 10 seconds.';


--
-- Name: COLUMN annotation_protocol.image_overlap; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.image_overlap IS 'Is there overlap between the images (true) or do they represent independent non-overlapping space (false).';


--
-- Name: COLUMN annotation_protocol.habitat_only; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.habitat_only IS 'Set to true if only habitat variables were recorded.';


--
-- Name: COLUMN annotation_protocol.species_guide; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.species_guide IS 'A URL to the iNaturalist species guide that was used for annotation.';


--
-- Name: COLUMN annotation_protocol.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.note IS 'If only a subset of invert, fish or algae species were identified, note here which groups were the primary focus (target species). For example, benthic fish or corals and sponges. Also for other notes of interest.';


--
-- Name: COLUMN annotation_protocol.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.created_on IS 'The date of creation of this record.';


--
-- Name: COLUMN annotation_protocol.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN annotation_protocol.creator_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.creator_id IS 'Records the identity of the person who created this protocol.';


--
-- Name: COLUMN annotation_protocol.invertebrate_species; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.invertebrate_species IS 'Whether all invertebrate species are identified: "all", "subset" or "none".';


--
-- Name: COLUMN annotation_protocol.fish_species; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.fish_species IS 'Whether all fish species are identified: "all", "subset" or "none".';


--
-- Name: COLUMN annotation_protocol.algae_species; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.algae_species IS 'Whether all algae species are identified: "all", "subset" or "none".';


--
-- Name: COLUMN annotation_protocol.biogenic_habitat; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.biogenic_habitat IS 'True if any habitat categories include fauna (e.g., sponge reefs).';


--
-- Name: COLUMN annotation_protocol.protocol_document; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.protocol_document IS 'Link, title, DOI, etc. of a document describing the protocol in full.';


--
-- Name: COLUMN annotation_protocol.observation_interval; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.observation_interval IS 'The time interval between species or anthropogenic observations.';


--
-- Name: COLUMN annotation_protocol.habitat_interval; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.habitat_interval IS 'The time interval between habitat observations.';


--
-- Name: COLUMN annotation_protocol.fov_interval; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.fov_interval IS 'The time interval between field-of-view measurements.';


--
-- Name: COLUMN annotation_protocol.image_interval_unit; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.image_interval_unit IS 'A unit for the image interval, such as ''s'' for seconds or ''m'' for metres.';


--
-- Name: COLUMN annotation_protocol.observation_interval_unit; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.observation_interval_unit IS 'A unit for the observation interval, such as ''s'' for seconds or ''m'' for metres.';


--
-- Name: COLUMN annotation_protocol.habitat_interval_unit; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.habitat_interval_unit IS 'A unit for the habitat interval, such as ''s'' for seconds or ''m'' for metres.';


--
-- Name: COLUMN annotation_protocol.fov_interval_unit; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol.fov_interval_unit IS 'A unit for the field of view interval, such as ''s'' for seconds or ''m'' for metres.';


--
-- Name: annotation_protocol_document; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.annotation_protocol_document (
    id integer NOT NULL,
    annotation_protocol_id integer NOT NULL,
    note text,
    title character varying(256) NOT NULL,
    url character varying(1024),
    file_type character varying(62),
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    file_name character varying(256)
);


ALTER TABLE rov.annotation_protocol_document OWNER TO msea_admin;

--
-- Name: TABLE annotation_protocol_document; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.annotation_protocol_document IS 'Contains documents related to an [annotation protocol](#rov.annotation_protocol). These can be a URL or actual file data. If the file data are present in the database, the URL can still be used to provide the origin of the file, etc.';


--
-- Name: COLUMN annotation_protocol_document.annotation_protocol_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.annotation_protocol_id IS 'A reference to the [annotation protocol](#rov.annotation_protocol).';


--
-- Name: COLUMN annotation_protocol_document.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.note IS 'An optional note about the document.';


--
-- Name: COLUMN annotation_protocol_document.title; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.title IS 'The title of the document.';


--
-- Name: COLUMN annotation_protocol_document.url; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.url IS 'An optional URL for the document. If file data are not given, this field is required.';


--
-- Name: COLUMN annotation_protocol_document.file_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.file_type IS 'The [mime type](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types) of the file. Required if file data are given.';


--
-- Name: COLUMN annotation_protocol_document.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.created_on IS 'The time of creation of the record.';


--
-- Name: COLUMN annotation_protocol_document.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.updated_on IS 'The time of update of the record.';


--
-- Name: COLUMN annotation_protocol_document.file_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_protocol_document.file_name IS 'The original name of the document file.';


--
-- Name: annotation_protocol_document_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.annotation_protocol_document_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.annotation_protocol_document_id_seq OWNER TO msea_admin;

--
-- Name: annotation_protocol_document_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.annotation_protocol_document_id_seq OWNED BY rov.annotation_protocol_document.id;


--
-- Name: annotation_protocol_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.annotation_protocol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.annotation_protocol_id_seq OWNER TO msea_admin;

--
-- Name: annotation_protocol_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.annotation_protocol_id_seq OWNED BY rov.annotation_protocol.id;


--
-- Name: annotation_software; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.annotation_software (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character(16)
);


ALTER TABLE rov.annotation_software OWNER TO msea_admin;

--
-- Name: TABLE annotation_software; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.annotation_software IS 'The software used for annotation.';


--
-- Name: COLUMN annotation_software.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_software.name IS 'The name of the annotation software.';


--
-- Name: COLUMN annotation_software.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_software.note IS 'An optional note about the annotation software.';


--
-- Name: COLUMN annotation_software.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.annotation_software.short_code IS 'Provides a short code for looking up the entity.';


--
-- Name: annotation_software_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.annotation_software_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.annotation_software_id_seq OWNER TO msea_admin;

--
-- Name: annotation_software_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.annotation_software_id_seq OWNED BY rov.annotation_software.id;


--
-- Name: biocover; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.biocover (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.biocover OWNER TO msea_admin;

--
-- Name: TABLE biocover; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.biocover IS 'A lookup table listing the available biocover types for the [habitat_event](#rov.habitat_event) table. TODO: This table may be altered to provide a hierarchical list of types with increasing specificity. TODO: Should perhaps refer to the [taxon](#shared.taxon) table.';


--
-- Name: COLUMN biocover.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.biocover.name IS 'The name of the biocover.';


--
-- Name: COLUMN biocover.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.biocover.note IS 'An optional description of the biocover.';


--
-- Name: COLUMN biocover.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.biocover.short_code IS 'Contains a short code that can be used to look up a biocover (e.g., during import) without relying on the primary key.';


--
-- Name: biocover_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.biocover_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.biocover_id_seq OWNER TO msea_admin;

--
-- Name: biocover_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.biocover_id_seq OWNED BY rov.biocover.id;


--
-- Name: comment_event; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.comment_event (
    event_id integer NOT NULL,
    note text
);


ALTER TABLE rov.comment_event OWNER TO msea_admin;

--
-- Name: TABLE comment_event; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.comment_event IS 'Provides a way to record comments rather than using a sparse note field on the [event](#rov.table).';


--
-- Name: COLUMN comment_event.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.comment_event.event_id IS 'A reference to the [parent event](#rov.event).';


--
-- Name: COLUMN comment_event.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.comment_event.note IS 'A text comment or note.';


--
-- Name: complexity; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.complexity (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.complexity OWNER TO msea_admin;

--
-- Name: TABLE complexity; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.complexity IS 'A lookup table listing the available habitat complexity types for the [habitat_event](#rov.habitat_event) table. TODO: This table may be altered to provide a hierarchical list of types with increasing specificity.';


--
-- Name: COLUMN complexity.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.complexity.name IS 'The name of the complexity type.';


--
-- Name: COLUMN complexity.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.complexity.note IS 'An optional description of the complexity type.';


--
-- Name: COLUMN complexity.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.complexity.short_code IS 'Contains a short code that can be used to look up a complexity (e.g., during import) without relying on the primary key.';


--
-- Name: complexity_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.complexity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.complexity_id_seq OWNER TO msea_admin;

--
-- Name: complexity_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.complexity_id_seq OWNED BY rov.complexity.id;


--
-- Name: coverage; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.coverage (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    minimum real NOT NULL,
    maximum real NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.coverage OWNER TO msea_admin;

--
-- Name: TABLE coverage; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.coverage IS 'A lookup table listing the percentage of coverage for the [habitat_event](#rov.habitat_event) table. The coverages are given as ranges so the text of the level is given in the name field and the values in the min and max fields contain the bounding values.';


--
-- Name: COLUMN coverage.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.coverage.name IS 'A characterization of percent coverage. Presented as a range of percentages.';


--
-- Name: COLUMN coverage.minimum; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.coverage.minimum IS 'The minimum value in the range.';


--
-- Name: COLUMN coverage.maximum; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.coverage.maximum IS 'The minimum value in the range.';


--
-- Name: COLUMN coverage.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.coverage.note IS 'An optional note about the coverage percentage.';


--
-- Name: COLUMN coverage.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.coverage.short_code IS 'Contains a short code that can be used to look up a coverage (e.g., during import) without relying on the primary key.';


--
-- Name: coverage_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.coverage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.coverage_id_seq OWNER TO msea_admin;

--
-- Name: coverage_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.coverage_id_seq OWNED BY rov.coverage.id;


--
-- Name: cruise; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    objective text,
    start_time timestamp(6) without time zone NOT NULL,
    end_time timestamp(6) without time zone,
    planned_track public.geography,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    leg integer DEFAULT 1 NOT NULL,
    summary text,
    approved integer DEFAULT 0 NOT NULL,
    ship_id integer NOT NULL,
    admin_note text
);


ALTER TABLE rov.cruise OWNER TO msea_admin;

--
-- Name: TABLE cruise; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise IS 'Cruise legs occur within a [cruise](#rov.cruise) and are assigned specific [crews](#rov.cruise_leg_crew), [scientific programs](#rov.program), etc. A single leg can cover an entire cruise, legs can be spaced end-to-end, or can theoretically overlap. Legs can share members or equipment, and crew members can have specific roles related to a leg. Cruise legs can be created without a [scientific program](#rov.program) or a [cruise](#rov.cruise) because imported data sets may only list the name of the cruise and not indicate whether it was part of a longer cruise.';


--
-- Name: COLUMN cruise.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.name IS 'A name for this leg of the cruise.';


--
-- Name: COLUMN cruise.objective; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.objective IS 'A statement of the operational or scientific objectives of the cruise.';


--
-- Name: COLUMN cruise.start_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.start_time IS 'The start time of the leg.';


--
-- Name: COLUMN cruise.end_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.end_time IS 'The end time of the leg.';


--
-- Name: COLUMN cruise.planned_track; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.planned_track IS 'A multilinestring containing the planned track of the leg. TODO: Not known whether this is necessary.';


--
-- Name: COLUMN cruise.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.note IS 'Notes about the cruise.';


--
-- Name: COLUMN cruise.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.created_on IS 'The date of creation of this record.';


--
-- Name: COLUMN cruise.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN cruise.leg; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.leg IS 'Cruise legs are numbered from 1.';


--
-- Name: COLUMN cruise.summary; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.summary IS 'A summary of the cruise, with information about whether the objectives were met and any other pertinent information.';


--
-- Name: COLUMN cruise.approved; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.approved IS 'If zero, the record is not approved and should not be shown or used. Currently a non-zero value represents approval, but may be expanded to various levels of approval in the future.';


--
-- Name: COLUMN cruise.ship_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.ship_id IS 'The ID of the [ship](#rov.platform) used on this cruise.';


--
-- Name: COLUMN cruise.admin_note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise.admin_note IS 'Notes by the database administrator about this entity.';


--
-- Name: cruise_crew; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise_crew (
    id integer NOT NULL,
    cruise_id integer NOT NULL,
    person_id integer NOT NULL,
    cruise_role_id integer NOT NULL,
    note text
);


ALTER TABLE rov.cruise_crew OWNER TO msea_admin;

--
-- Name: TABLE cruise_crew; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise_crew IS 'Associates [crew members](#rov.person) with a [cruise leg](#rov.cruise_leg) and their [roles](#rov.cruise_role). A crew member can have multiple roles. Note: these roles are distinct from members of [programs](#rov.program), such as Chief Scientist. It may be necessary to revisit this structure or the division of roles.';


--
-- Name: COLUMN cruise_crew.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_crew.cruise_id IS ' Reference to the [cruise leg](#rov.cruise_leg) to which the member is assigned.';


--
-- Name: COLUMN cruise_crew.person_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_crew.person_id IS 'Reference to the [person](#rov.person) on the crew.';


--
-- Name: COLUMN cruise_crew.cruise_role_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_crew.cruise_role_id IS 'Reference to the [cruise role](#rov.cruise_role).';


--
-- Name: COLUMN cruise_crew.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_crew.note IS 'An optional note about the crew member.';


--
-- Name: cruise_document; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise_document (
    id integer NOT NULL,
    cruise_id integer NOT NULL,
    note text,
    title character varying(256) NOT NULL,
    url character varying(1024),
    file_type character varying(256),
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    file_name character varying(256),
    uploaded_file_id integer
);


ALTER TABLE rov.cruise_document OWNER TO msea_admin;

--
-- Name: TABLE cruise_document; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise_document IS 'Stores information about documents related to a [cruise](#rov.cruise).';


--
-- Name: COLUMN cruise_document.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.cruise_id IS 'The ID of the [cruise](#rov.cruise).';


--
-- Name: COLUMN cruise_document.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.note IS 'A note about the document.';


--
-- Name: COLUMN cruise_document.title; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.title IS 'The title of the document.';


--
-- Name: COLUMN cruise_document.url; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.url IS 'A URL for the online copy of the document. May be used when no file is available.';


--
-- Name: COLUMN cruise_document.file_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.file_type IS 'The file type.';


--
-- Name: COLUMN cruise_document.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN cruise_document.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: COLUMN cruise_document.file_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_document.file_name IS 'The name of the document file.';


--
-- Name: cruise_document_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_document_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_document_id_seq OWNER TO msea_admin;

--
-- Name: cruise_document_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_document_id_seq OWNED BY rov.cruise_document.id;


--
-- Name: cruise_fn_contact; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise_fn_contact (
    id integer NOT NULL,
    cruise_id integer NOT NULL,
    contact_name character varying(128) NOT NULL,
    email character varying(256),
    phone character varying(32),
    nation text,
    note text,
    CONSTRAINT chk_cruise_fn_contact_email_or_phone CHECK (((email IS NOT NULL) OR (phone IS NOT NULL)))
);


ALTER TABLE rov.cruise_fn_contact OWNER TO msea_admin;

--
-- Name: TABLE cruise_fn_contact; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise_fn_contact IS 'A table for associating First Nations contacts with a cruise.';


--
-- Name: COLUMN cruise_fn_contact.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_fn_contact.cruise_id IS 'A reference to the [cruise](#rov.cruise).';


--
-- Name: COLUMN cruise_fn_contact.contact_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_fn_contact.contact_name IS 'The full name of the contact.';


--
-- Name: COLUMN cruise_fn_contact.email; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_fn_contact.email IS 'The email address of the contact.';


--
-- Name: COLUMN cruise_fn_contact.phone; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_fn_contact.phone IS 'The phone number of the contact.';


--
-- Name: COLUMN cruise_fn_contact.nation; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_fn_contact.nation IS 'The name of the nation or group represented by the contact.';


--
-- Name: COLUMN cruise_fn_contact.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_fn_contact.note IS 'A note about the contact.';


--
-- Name: cruise_fn_contact_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_fn_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_fn_contact_id_seq OWNER TO msea_admin;

--
-- Name: cruise_fn_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_fn_contact_id_seq OWNED BY rov.cruise_fn_contact.id;


--
-- Name: cruise_import; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise_import (
    id integer NOT NULL,
    name character varying(512) NOT NULL,
    data jsonb NOT NULL,
    status character varying(32) NOT NULL,
    logs jsonb NOT NULL,
    created_on timestamp without time zone DEFAULT now(),
    updated_on timestamp without time zone DEFAULT now()
);


ALTER TABLE rov.cruise_import OWNER TO msea_admin;

--
-- Name: TABLE cruise_import; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise_import IS 'Stores cruise import tasks in the database. These contain the JSON data description, a status message and complete processing log.';


--
-- Name: COLUMN cruise_import.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_import.name IS 'A unique name for the import job.';


--
-- Name: COLUMN cruise_import.data; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_import.data IS 'A JSON document containing the cruise import data.';


--
-- Name: COLUMN cruise_import.status; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_import.status IS 'A status message about processing.';


--
-- Name: COLUMN cruise_import.logs; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_import.logs IS 'A JSON array containing the complete processing log.';


--
-- Name: cruise_import_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_import_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_import_id_seq OWNER TO msea_admin;

--
-- Name: cruise_import_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_import_id_seq OWNED BY rov.cruise_import.id;


--
-- Name: cruise_leg_crew_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_leg_crew_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_leg_crew_id_seq OWNER TO msea_admin;

--
-- Name: cruise_leg_crew_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_leg_crew_id_seq OWNED BY rov.cruise_crew.id;


--
-- Name: cruise_leg_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_leg_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_leg_id_seq OWNER TO msea_admin;

--
-- Name: cruise_leg_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_leg_id_seq OWNED BY rov.cruise.id;


--
-- Name: cruise_library; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise_library (
    id integer NOT NULL,
    cruiseleg_id integer NOT NULL,
    library_id integer NOT NULL,
    note text
);


ALTER TABLE rov.cruise_library OWNER TO msea_admin;

--
-- Name: TABLE cruise_library; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise_library IS 'This table creates an association between a [cruise leg](#rov.cruise_leg) and [documents in the library](#rov.library) that may be relevant to its research objectives.';


--
-- Name: COLUMN cruise_library.cruiseleg_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_library.cruiseleg_id IS 'A reference to the [cruise leg](#rov.cruise_leg).';


--
-- Name: COLUMN cruise_library.library_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_library.library_id IS 'A reference to the document in the [library](#rov.library).';


--
-- Name: COLUMN cruise_library.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_library.note IS 'An optional note about the document with respect to the cruise leg. May be used to reference points of interest in a paper, etc.';


--
-- Name: cruise_leg_library_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_leg_library_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_leg_library_id_seq OWNER TO msea_admin;

--
-- Name: cruise_leg_library_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_leg_library_id_seq OWNED BY rov.cruise_library.id;


--
-- Name: cruise_role; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise_role (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.cruise_role OWNER TO msea_admin;

--
-- Name: TABLE cruise_role; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise_role IS 'A lookup table of roles available to members of [cruise leg crews](#rov.cruise_leg_crew).';


--
-- Name: COLUMN cruise_role.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_role.name IS 'The name of the role.';


--
-- Name: COLUMN cruise_role.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_role.note IS 'An optional note about the role and its responsibilities.';


--
-- Name: COLUMN cruise_role.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_role.short_code IS 'A short string used to identify the role.';


--
-- Name: cruise_leg_role_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_leg_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_leg_role_id_seq OWNER TO msea_admin;

--
-- Name: cruise_leg_role_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_leg_role_id_seq OWNED BY rov.cruise_role.id;


--
-- Name: cruise_program; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.cruise_program (
    id integer NOT NULL,
    cruise_id integer NOT NULL,
    program_id integer NOT NULL
);


ALTER TABLE rov.cruise_program OWNER TO msea_admin;

--
-- Name: TABLE cruise_program; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.cruise_program IS 'A table to link [programs](#rov.program) and [cruises](#rov.cruise). A cruise can be under the auspices of more than one program or none.';


--
-- Name: COLUMN cruise_program.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_program.cruise_id IS 'The ID of a [cruise](#rov.cruise).';


--
-- Name: COLUMN cruise_program.program_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_program.program_id IS 'The ID of a [program](#rov.program).';


--
-- Name: cruise_program_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.cruise_program_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.cruise_program_id_seq OWNER TO msea_admin;

--
-- Name: cruise_program_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.cruise_program_id_seq OWNED BY rov.cruise_program.id;


--
-- Name: disturbance; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.disturbance (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.disturbance OWNER TO msea_admin;

--
-- Name: TABLE disturbance; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.disturbance IS 'Provides a nominal level of disturbance for [habitat events](#rov.habitat_event).';


--
-- Name: COLUMN disturbance.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.disturbance.name IS 'The textual representation of the disturbance level.';


--
-- Name: COLUMN disturbance.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.disturbance.note IS 'A note about the disturbance level.';


--
-- Name: COLUMN disturbance.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.disturbance.short_code IS 'Contains a short code that can be used to look up a disturbance (e.g., during import) without relying on the primary key.';


--
-- Name: disturbance_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.disturbance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.disturbance_id_seq OWNER TO msea_admin;

--
-- Name: disturbance_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.disturbance_id_seq OWNED BY rov.disturbance.id;


--
-- Name: dive; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.dive (
    id integer NOT NULL,
    site_id integer,
    cruise_id integer NOT NULL,
    name character varying(64) NOT NULL,
    objective text,
    start_time timestamp(6) without time zone NOT NULL,
    end_time timestamp(6) without time zone NOT NULL,
    note text,
    attributes jsonb,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    summary text,
    sub_config_id integer NOT NULL,
    ship_config_id integer NOT NULL,
    admin_note text,
    seatube_id integer
);


ALTER TABLE rov.dive OWNER TO msea_admin;

--
-- Name: TABLE dive; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.dive IS 'A dive is what an ROV does. The dive has a start and end time (not necessarily submerge/resurface), a [crew](#rov.dive_crew) and possibly a name. [Transects](#rov.transect) occur during dives. ';


--
-- Name: COLUMN dive.site_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.site_id IS 'Optional reference to the geographic [site](#rov.site) of the dive.';


--
-- Name: COLUMN dive.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.cruise_id IS 'A reference to the [cruise leg](#rov.cruise_leg) during which the dive was performed.';


--
-- Name: COLUMN dive.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.name IS 'A name for the dive.';


--
-- Name: COLUMN dive.objective; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.objective IS 'A statement of the practical or research objectives for this dive.';


--
-- Name: COLUMN dive.start_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.start_time IS 'The start of the dive. Not necessarily the time the vehicle is placed in the water.';


--
-- Name: COLUMN dive.end_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.end_time IS 'The end of the dive.';


--
-- Name: COLUMN dive.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.note IS 'An optional note about the dive.';


--
-- Name: COLUMN dive.attributes; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.attributes IS 'A JSON column used for recording structured attributes that do not fit with the regular table structure.';


--
-- Name: COLUMN dive.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.created_on IS 'The date of creation of this record.';


--
-- Name: COLUMN dive.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN dive.summary; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.summary IS 'A summary of the [dive](#rov.dive); whether objectives were met, problems encountered, etc.';


--
-- Name: COLUMN dive.sub_config_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.sub_config_id IS 'A reference to the [platform config](#rov.platform_config) for the submersible or ROV.';


--
-- Name: COLUMN dive.ship_config_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.ship_config_id IS 'A reference to the [platform config](#rov.platform_config) for the ship.';


--
-- Name: COLUMN dive.seatube_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive.seatube_id IS 'The ID of a dive on SeaTube corresponding to this dive.';


--
-- Name: dive_crew; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.dive_crew (
    id integer NOT NULL,
    dive_id integer NOT NULL,
    person_id integer NOT NULL,
    dive_role_id integer NOT NULL,
    note text
);


ALTER TABLE rov.dive_crew OWNER TO msea_admin;

--
-- Name: TABLE dive_crew; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.dive_crew IS 'Assigns roles to dive crew members. Crew members are selected from the [person](#rov.person).';


--
-- Name: COLUMN dive_crew.dive_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_crew.dive_id IS 'The [dive](#rov.dive) to which the crew member is assigned.';


--
-- Name: COLUMN dive_crew.person_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_crew.person_id IS 'A reference to the [person](#rov.person).';


--
-- Name: COLUMN dive_crew.dive_role_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_crew.dive_role_id IS 'A reference to the [dive role](#rov.dive_role).';


--
-- Name: COLUMN dive_crew.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_crew.note IS 'An optional note about the crew member.';


--
-- Name: dive_crew_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.dive_crew_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.dive_crew_id_seq OWNER TO msea_admin;

--
-- Name: dive_crew_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.dive_crew_id_seq OWNED BY rov.dive_crew.id;


--
-- Name: dive_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.dive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.dive_id_seq OWNER TO msea_admin;

--
-- Name: dive_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.dive_id_seq OWNED BY rov.dive.id;


--
-- Name: dive_role; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.dive_role (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.dive_role OWNER TO msea_admin;

--
-- Name: TABLE dive_role; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.dive_role IS 'A list of roles available to crew members on a [dive](#rov.dive) via the [dive_crew](#rov.dive_crew) table.';


--
-- Name: COLUMN dive_role.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_role.name IS 'The name of the role.';


--
-- Name: COLUMN dive_role.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_role.note IS 'An optional description of the role.';


--
-- Name: COLUMN dive_role.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_role.short_code IS 'A short string used to identify the role.';


--
-- Name: dive_role_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.dive_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.dive_role_id_seq OWNER TO msea_admin;

--
-- Name: dive_role_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.dive_role_id_seq OWNED BY rov.dive_role.id;


--
-- Name: instrument_config; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.instrument_config (
    id integer NOT NULL,
    configuration jsonb,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    platform_config_id integer,
    name character varying(128) DEFAULT substr(md5((random())::text), 0, 25) NOT NULL,
    instrument_id integer NOT NULL
);


ALTER TABLE rov.instrument_config OWNER TO msea_admin;

--
-- Name: TABLE instrument_config; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.instrument_config IS 'This table records the configuration of an instrument, including settings and its spatial relationship to a parent entity -- another configured vehicle or instrument upon which this instrument is mounted.';


--
-- Name: COLUMN instrument_config.configuration; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument_config.configuration IS 'Configuration information about the instrument config.';


--
-- Name: COLUMN instrument_config.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument_config.note IS 'An optional note about this configuration.';


--
-- Name: COLUMN instrument_config.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument_config.created_on IS 'The date of creation of this record.';


--
-- Name: COLUMN instrument_config.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument_config.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN instrument_config.platform_config_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument_config.platform_config_id IS 'The ID of the [platform](#rov.platform) upon which the instrument is used.';


--
-- Name: COLUMN instrument_config.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument_config.name IS 'The name of the instrument config. Need not be unique: used to identify the config within the platform config.';


--
-- Name: COLUMN instrument_config.instrument_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument_config.instrument_id IS 'Reference to the [instrument](#rov.instrument) targeted by the configuration.';


--
-- Name: platform_config; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.platform_config (
    id integer NOT NULL,
    platform_id integer NOT NULL,
    configuration jsonb,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE rov.platform_config OWNER TO msea_admin;

--
-- Name: TABLE platform_config; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.platform_config IS 'This table stores configuration information about vehicles (ships, ROVs,etc.) used for surveys. All references to vehicles are made through this table rather than directly to the [platform](#rov.platform) table, because the disposition of equipment on the platform is essential to understanding how data has been generated, as well as for simple record-keeping purposes.';


--
-- Name: COLUMN platform_config.platform_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform_config.platform_id IS 'A reference to the [platform](#rov.platform).';


--
-- Name: COLUMN platform_config.configuration; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform_config.configuration IS 'The configuration data as a JSON object.';


--
-- Name: COLUMN platform_config.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform_config.note IS 'An optional note about the configuration record.';


--
-- Name: COLUMN platform_config.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform_config.created_on IS 'The date of creation of this record.';


--
-- Name: COLUMN platform_config.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform_config.updated_on IS 'The date of update of this record.';


--
-- Name: position; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov."position" (
    id integer NOT NULL,
    position_type_id integer NOT NULL,
    "timestamp" timestamp(6) without time zone NOT NULL,
    signal_quality real,
    geom public.geography,
    is_modelled boolean DEFAULT false NOT NULL,
    instrument_config_id integer NOT NULL
);


ALTER TABLE rov."position" OWNER TO msea_admin;

--
-- Name: TABLE "position"; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov."position" IS 'This table preserves time-stamped position measurements from [instruments](#rov.instrument_config). These can be linear (e.g., UTM) or angular (e.g., Geographic) positions in a [specified unit](#rov.position_type). Ship and ROV positions will be recorded here. TODO: Should this table have a spatial object (point) as well, or be split into two tables, one for absolute georeferenced positions and one for relative positions and orientations?';


--
-- Name: COLUMN "position".position_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov."position".position_type_id IS 'A reference to the [position type](#rov.position_type) of this position.';


--
-- Name: COLUMN "position"."timestamp"; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov."position"."timestamp" IS 'The time the position was recorded.';


--
-- Name: COLUMN "position".signal_quality; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov."position".signal_quality IS 'The signal quality of the position as reported by the instrument. TODO: Requires clarification.';


--
-- Name: COLUMN "position".geom; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov."position".geom IS 'The point geometry.';


--
-- Name: COLUMN "position".is_modelled; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov."position".is_modelled IS 'A flag to indicate whether the value is derived from measurements by some modelling process.';


--
-- Name: COLUMN "position".instrument_config_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov."position".instrument_config_id IS 'The [configured instrument](#rov.instrument_config) used to generate this item.';


--
-- Name: dive_track; Type: MATERIALIZED VIEW; Schema: rov; Owner: msea_admin
--

CREATE MATERIALIZED VIEW rov.dive_track AS
 WITH t AS (
         SELECT f.id AS cruise_id,
            d.id AS dive_id,
            concat(f.name, '-', f.leg) AS cruise_name,
            d.name AS dive_name,
            concat('#', SUBSTRING(md5((f.id)::text) FROM 1 FOR 6)) AS colour,
            a.id AS position_id,
            a."timestamp",
            a.instrument_config_id,
            (a.geom)::public.geometry AS geom
           FROM ((((rov."position" a
             JOIN rov.instrument_config b ON ((b.id = a.instrument_config_id)))
             JOIN rov.platform_config c ON ((c.id = b.platform_config_id)))
             JOIN rov.dive d ON ((d.sub_config_id = c.id)))
             JOIN rov.cruise f ON ((f.id = d.cruise_id)))
          WHERE ((d.start_time <= a."timestamp") AND (d.end_time > a."timestamp"))
          ORDER BY a."timestamp"
        )
 SELECT cruise_id,
    dive_id,
    cruise_name,
    dive_name,
    colour,
    public.st_force2d(public.st_makeline(geom ORDER BY "timestamp")) AS geom
   FROM t
  GROUP BY cruise_id, dive_id, dive_name, cruise_name, colour
  WITH NO DATA;


ALTER MATERIALIZED VIEW rov.dive_track OWNER TO msea_admin;

--
-- Name: MATERIALIZED VIEW dive_track; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON MATERIALIZED VIEW rov.dive_track IS 'Constructs a geometry for each [dive](#rov.dive) which describes the path of the submersible.';


--
-- Name: COLUMN dive_track.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_track.cruise_id IS 'The reference to the [cruise](#rov.cruise).';


--
-- Name: COLUMN dive_track.dive_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_track.dive_id IS 'The reference to the [dive](#rov.dive).';


--
-- Name: COLUMN dive_track.cruise_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_track.cruise_name IS 'The [cruise](#rov.cruise) name.';


--
-- Name: COLUMN dive_track.dive_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_track.dive_name IS 'The [dive](#rov.dive) name.';


--
-- Name: COLUMN dive_track.colour; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_track.colour IS 'The a colour code generated from the cruise''s ID. Used for cartography.';


--
-- Name: COLUMN dive_track.geom; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.dive_track.geom IS 'The dive track geometry.';


--
-- Name: equipment_type; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.equipment_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16),
    category public.equipment_category
);


ALTER TABLE rov.equipment_type OWNER TO msea_admin;

--
-- Name: TABLE equipment_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.equipment_type IS 'This is a lookup table to provide the names of types of equipment for the [model_equipment_type](#rov.model_equipment_type) table, e.g., "Digital Still Camera", "Thermometer," "ROV," etc.';


--
-- Name: COLUMN equipment_type.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.equipment_type.name IS 'A name for the equipment type.';


--
-- Name: COLUMN equipment_type.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.equipment_type.note IS 'An optional note about the equipment type.';


--
-- Name: COLUMN equipment_type.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.equipment_type.short_code IS 'A short code for referencing the equipment type in import documents.';


--
-- Name: COLUMN equipment_type.category; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.equipment_type.category IS 'An enumeration column identifying the equipment as platform, instrument or some other type.';


--
-- Name: equipment_type_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.equipment_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.equipment_type_id_seq OWNER TO msea_admin;

--
-- Name: equipment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.equipment_type_id_seq OWNED BY rov.equipment_type.id;


--
-- Name: event; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.event (
    id integer NOT NULL,
    dive_id integer NOT NULL,
    transect_id integer,
    start_time timestamp(6) without time zone NOT NULL,
    end_time timestamp(6) without time zone,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    original_id integer NOT NULL,
    shape jsonb,
    shape_area double precision,
    frames jsonb,
    attributes jsonb,
    original_labels jsonb,
    medium_filename character varying(256),
    tags jsonb,
    annotation_job_id integer,
    instrument_config_id integer,
    protocol_id integer,
    image_quality_id integer,
    survey_mode_id integer
);


ALTER TABLE rov.event OWNER TO msea_admin;

--
-- Name: TABLE event; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.event IS 'The event table is a parent or abstract table that provides properties common to [habitat](#rov.habitat_event), [observation](#rov.observation_event), [status](#rov.status_event) and [measurement](#rov.measurement_event) events. Each of those event records must have a reference to one event record. Conceptually, the event row and its child entity row are considered to be one object.';


--
-- Name: COLUMN event.dive_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.dive_id IS 'A reference to a [dive](#rov.dive).';


--
-- Name: COLUMN event.transect_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.transect_id IS 'An optional reference to a [transect](#rov.transect). TODO: Clarify the use of transects and whether this column is nullable.';


--
-- Name: COLUMN event.start_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.start_time IS 'The time at the start of the event. If the event is instantaneous, this is the time at which it occurred.';


--
-- Name: COLUMN event.end_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.end_time IS 'The end time of the event. If the event is instantaneous, this field is null.';


--
-- Name: COLUMN event.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.created_on IS 'The time of creation of this record.';


--
-- Name: COLUMN event.original_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.original_id IS 'The original ID of the event if read from a source that has IDs.';


--
-- Name: COLUMN event.shape; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.shape IS 'The shape used for annotations (e.g., Biigle). The coordinates for this shape should have been converted to metres. GeoJSON doesn''t support circles, so we use a format that encapsulates the shape type and the raw list of coordinates, something like: { "type": "circle", "shape": [1.0, 1.0, 1.0] } which is a circle with radius 1 at position 1,1. A whole-screen annotation will be a rectangle the size of the screen.';


--
-- Name: COLUMN event.shape_area; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.shape_area IS 'The area of the shape in square metres, calculated from the screenand shape dimensions and the laser point distance.';


--
-- Name: COLUMN event.frames; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.frames IS 'Preserves the frames from the Biigle annotation.';


--
-- Name: COLUMN event.original_labels; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.original_labels IS 'Optionally stores information about annotation labels used to generate this event. For Biigle, stores the label IDs.';


--
-- Name: COLUMN event.medium_filename; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.medium_filename IS 'The name of the media file from which this event is derived.';


--
-- Name: COLUMN event.tags; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.tags IS 'Stores a list of tags relevant to this event.';


--
-- Name: COLUMN event.annotation_job_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.annotation_job_id IS 'Stores a reference to the [annotation job](#rov.annotation_job) during which this event was created.';


--
-- Name: COLUMN event.protocol_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.protocol_id IS 'A reference to the [protocol](#rov.protocol).';


--
-- Name: COLUMN event.image_quality_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.image_quality_id IS 'A reference to the [image quality](#rov.image_quality).';


--
-- Name: COLUMN event.survey_mode_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event.survey_mode_id IS 'A reference to the [survey mode](#rov.survey_mode).';


--
-- Name: event_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.event_id_seq OWNER TO msea_admin;

--
-- Name: event_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.event_id_seq OWNED BY rov.event.id;


--
-- Name: event_logger; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.event_logger (
    id integer NOT NULL,
    person_id integer NOT NULL,
    event_id integer NOT NULL
);


ALTER TABLE rov.event_logger OWNER TO msea_admin;

--
-- Name: TABLE event_logger; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.event_logger IS 'This table tracks the [people](#shared.person) who contributed to annotation, which may be composed of a number of separate labels.';


--
-- Name: COLUMN event_logger.person_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event_logger.person_id IS 'A [person](#shared.person) who contributed to the annotation.';


--
-- Name: COLUMN event_logger.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.event_logger.event_id IS 'The [event](#rov.event) that was created from the annotation(s).';


--
-- Name: event_logger_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.event_logger_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.event_logger_id_seq OWNER TO msea_admin;

--
-- Name: event_logger_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.event_logger_id_seq OWNED BY rov.event_logger.id;


--
-- Name: measurement; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.measurement (
    id integer NOT NULL,
    measurement_type_id integer NOT NULL,
    "timestamp" timestamp(6) without time zone NOT NULL,
    quantity real NOT NULL,
    signal_quality real,
    is_modelled boolean DEFAULT false NOT NULL,
    instrument_config_id integer NOT NULL
);


ALTER TABLE rov.measurement OWNER TO msea_admin;

--
-- Name: TABLE measurement; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.measurement IS 'This table preserves time-stamped measurements generated by [instruments](#rov.instrument_config). Each measurement has an associated quantity and unit. TODO: At this point multi-part measurement would be stored separately as the ability to store vectors isn''t universal across DBMSes.';


--
-- Name: COLUMN measurement.measurement_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement.measurement_type_id IS 'A reference to the [type](#rov.measurement_type) of this measurement.';


--
-- Name: COLUMN measurement."timestamp"; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement."timestamp" IS 'The time that the measurement was recorded.';


--
-- Name: COLUMN measurement.quantity; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement.quantity IS 'The scalar quantity or magnitude of the measurement.';


--
-- Name: COLUMN measurement.signal_quality; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement.signal_quality IS 'A quality of the measurement as reported by the instrument. TODO: Requires clarification.';


--
-- Name: COLUMN measurement.is_modelled; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement.is_modelled IS 'A flag to indicate whether the value is derived from measurements by some modelling process.';


--
-- Name: COLUMN measurement.instrument_config_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement.instrument_config_id IS 'The [configured instrument](#rov.instrument_config) used to generate this item.';


--
-- Name: measurement_type; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.measurement_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    unit character varying(32) NOT NULL,
    minimum real,
    maximum real,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.measurement_type OWNER TO msea_admin;

--
-- Name: TABLE measurement_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.measurement_type IS 'This is a lookup table of types of measurement types for the [measurement](#rov.measurement) or [measurement_event](#rov.measurement_event) tables. This is fairly open-ended and can represent something like salinity with a specific unit, be it the SI unit or a discipline-specific unit. It is provided to allow users to easily select units for a measurement category when importing data.';


--
-- Name: COLUMN measurement_type.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_type.name IS 'The type of measurement. E.g., "Density" or "Salinity."';


--
-- Name: COLUMN measurement_type.unit; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_type.unit IS 'The unit. SI units are preferred but not required. The unit selection should probably depend on field-specific idiomatic or cultural preferences.';


--
-- Name: COLUMN measurement_type.minimum; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_type.minimum IS 'An optional lower bound on the value of the measurement. Null implies no limit.';


--
-- Name: COLUMN measurement_type.maximum; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_type.maximum IS 'An optional upper bound on the value of the measurement. Null implies no limit.';


--
-- Name: COLUMN measurement_type.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_type.note IS 'An optional note about this measurement type.';


--
-- Name: COLUMN measurement_type.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_type.short_code IS 'Contains a short code that can be used to look up a measurement type (e.g., during import) without relying on the primary key.';


--
-- Name: evt_depth; Type: MATERIALIZED VIEW; Schema: rov; Owner: msea_admin
--

CREATE MATERIALIZED VIEW rov.evt_depth AS
 SELECT e.id AS event_id,
    t.id AS start_measurement_id,
    t.diff AS start_diff,
    u.id AS end_measurement_id,
    u.diff AS end_diff
   FROM ((rov.event e
     JOIN LATERAL ( SELECT p.id,
            (p."timestamp" - e.start_time) AS diff
           FROM ( SELECT q.id,
                    q."timestamp"
                   FROM ((rov.measurement q
                     JOIN rov.instrument_config i ON ((i.id = q.instrument_config_id)))
                     JOIN rov.dive d ON ((d.sub_config_id = i.platform_config_id)))
                  WHERE (q.measurement_type_id = ( SELECT measurement_type.id
                           FROM rov.measurement_type
                          WHERE ((measurement_type.short_code)::text = 'depth_m'::text)))) p
          ORDER BY (p."timestamp" OPERATOR(public.<->) e.start_time)
         LIMIT 1) t ON (true))
     LEFT JOIN LATERAL ( SELECT p.id,
            (p."timestamp" - e.end_time) AS diff
           FROM ( SELECT q.id,
                    q."timestamp"
                   FROM ((rov.measurement q
                     JOIN rov.instrument_config i ON ((i.id = q.instrument_config_id)))
                     JOIN rov.dive d ON ((d.sub_config_id = i.platform_config_id)))
                  WHERE (q.measurement_type_id = ( SELECT measurement_type.id
                           FROM rov.measurement_type
                          WHERE ((measurement_type.short_code)::text = 'depth_m'::text)))) p
          ORDER BY (p."timestamp" OPERATOR(public.<->) e.end_time)
         LIMIT 1) u ON (true))
  WITH NO DATA;


ALTER MATERIALIZED VIEW rov.evt_depth OWNER TO msea_admin;

--
-- Name: MATERIALIZED VIEW evt_depth; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON MATERIALIZED VIEW rov.evt_depth IS 'Creates a relation between an [event](#rov.event) and the [depth](#rov.measurement) nearest the start and end times of the event.';


--
-- Name: COLUMN evt_depth.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_depth.event_id IS 'The link to the [event''s](#rov.event)''s event ID.';


--
-- Name: COLUMN evt_depth.start_measurement_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_depth.start_measurement_id IS 'The link to the [depth](#rov.measurement) nearest the event''s start time.';


--
-- Name: COLUMN evt_depth.start_diff; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_depth.start_diff IS 'The time difference between the [event](#rov.event) start time and the corresponding [depth](#rov.measurement) time.';


--
-- Name: COLUMN evt_depth.end_measurement_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_depth.end_measurement_id IS 'The link to the [depth](#rov.measurement) nearest the event''s end time, or null if one is not supplied.';


--
-- Name: COLUMN evt_depth.end_diff; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_depth.end_diff IS 'The time difference between the [event](#rov.event) end time and the corresponding [depth](#rov.measurement) time.';


--
-- Name: evt_pos; Type: MATERIALIZED VIEW; Schema: rov; Owner: msea_admin
--

CREATE MATERIALIZED VIEW rov.evt_pos AS
 SELECT e.id AS event_id,
    t.id AS start_position_id,
    t.diff AS start_diff,
    u.id AS end_position_id,
    u.diff AS end_diff
   FROM ((rov.event e
     JOIN LATERAL ( SELECT p.id,
            (p."timestamp" - e.start_time) AS diff
           FROM ( SELECT q.id,
                    q."timestamp"
                   FROM ((rov."position" q
                     JOIN rov.instrument_config i ON ((i.id = q.instrument_config_id)))
                     JOIN rov.dive d ON ((d.sub_config_id = i.platform_config_id)))) p
          ORDER BY (p."timestamp" OPERATOR(public.<->) e.start_time)
         LIMIT 1) t ON (true))
     LEFT JOIN LATERAL ( SELECT p.id,
            (p."timestamp" - e.end_time) AS diff
           FROM ( SELECT q.id,
                    q."timestamp"
                   FROM ((rov."position" q
                     JOIN rov.instrument_config i ON ((i.id = q.instrument_config_id)))
                     JOIN rov.dive d ON ((d.sub_config_id = i.platform_config_id)))) p
          ORDER BY (p."timestamp" OPERATOR(public.<->) e.end_time)
         LIMIT 1) u ON (true))
  WITH NO DATA;


ALTER MATERIALIZED VIEW rov.evt_pos OWNER TO msea_admin;

--
-- Name: MATERIALIZED VIEW evt_pos; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON MATERIALIZED VIEW rov.evt_pos IS 'Creates a relation between an [event](#rov.event) and the [positions](#rov.position) nearest the start and end times of the event.';


--
-- Name: COLUMN evt_pos.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_pos.event_id IS 'The link to the [event''s](#rov.event)''s event ID.';


--
-- Name: COLUMN evt_pos.start_position_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_pos.start_position_id IS 'The link to the [position](#rov.position) nearest the event''s start time.';


--
-- Name: COLUMN evt_pos.start_diff; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_pos.start_diff IS 'The time difference between the [event](#rov.event) start time and the corresponding [position](#rov.position) time.';


--
-- Name: COLUMN evt_pos.end_position_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_pos.end_position_id IS 'The link to the [position](#rov.position) nearest the event''s end time, or null if one is not supplied.';


--
-- Name: COLUMN evt_pos.end_diff; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.evt_pos.end_diff IS 'The time difference between the [event](#rov.event) end time and the corresponding [position](#rov.position) time.';


--
-- Name: flow; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.flow (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    note text,
    short_code character varying(32)
);


ALTER TABLE rov.flow OWNER TO msea_admin;

--
-- Name: TABLE flow; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.flow IS 'Stores categories of current flow for [habitat](#rov.habitat_event).';


--
-- Name: COLUMN flow.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.flow.name IS 'A note about the flow category.';


--
-- Name: COLUMN flow.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.flow.short_code IS 'A short code for the flow category.';


--
-- Name: flow_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.flow_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.flow_id_seq OWNER TO msea_admin;

--
-- Name: flow_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.flow_id_seq OWNED BY rov.flow.id;


--
-- Name: habitat_event; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.habitat_event (
    event_id integer NOT NULL,
    complexity_id integer,
    substrate_id integer,
    substrate_coverage_id integer,
    biocover_id integer,
    thickness_id integer,
    disturbance_id integer,
    relief_id integer,
    biocover_coverage_id integer,
    taxon_id integer,
    flow_id integer,
    observation_confidence_id integer
);


ALTER TABLE rov.habitat_event OWNER TO msea_admin;

--
-- Name: TABLE habitat_event; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.habitat_event IS 'The habitat event table records information about the [biocover](#rov.biocover), [thickness](#rov.thickness), [substrate](#rov.substrate) and [complexity](#rov.complexity) of the habitat. Some survey protocols record the taxon with biocover observations. A reference to the [taxon](#shared.taxon) table is provided. This table is a realization of the [event](#rov.event) table.';


--
-- Name: COLUMN habitat_event.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.event_id IS 'A reference to the parent [event](#rov.event).';


--
-- Name: COLUMN habitat_event.complexity_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.complexity_id IS 'A reference to the [habitat complexity](#rov.complexity) lookup.';


--
-- Name: COLUMN habitat_event.substrate_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.substrate_id IS 'A reference to the dominant [substrate](#rov.substrate) lookup.';


--
-- Name: COLUMN habitat_event.substrate_coverage_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.substrate_coverage_id IS 'A [coverage](#rov.coverage) value for the [substrate](#rov.substrate).';


--
-- Name: COLUMN habitat_event.biocover_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.biocover_id IS 'A reference to the [biocover](#rov.biocover) lookup.';


--
-- Name: COLUMN habitat_event.thickness_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.thickness_id IS 'A reference to the [biocover thickness](#rov.thickness) lookup.';


--
-- Name: COLUMN habitat_event.disturbance_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.disturbance_id IS 'Indicates the level of disturbance of the substrate and/or biota.';


--
-- Name: COLUMN habitat_event.relief_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.relief_id IS 'A reference to the [relief](#rov.relief) level.';


--
-- Name: COLUMN habitat_event.biocover_coverage_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.biocover_coverage_id IS 'A [coverage](#rov.coverage) value for the [biocover](#rov.biocover).';


--
-- Name: COLUMN habitat_event.taxon_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.taxon_id IS 'A reference to the [taxon](#shared.taxon) used in the creation of this observation.';


--
-- Name: COLUMN habitat_event.flow_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.flow_id IS 'A reference to the [current flow](#rov.flow).';


--
-- Name: COLUMN habitat_event.observation_confidence_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.habitat_event.observation_confidence_id IS 'A reference to the [observation confidence](#rov.observation_confidence).';


--
-- Name: image_quality; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.image_quality (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    rank integer,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.image_quality OWNER TO msea_admin;

--
-- Name: TABLE image_quality; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.image_quality IS 'Nominal image quality levels, originally used by VideoMiner but applicable to new records.';


--
-- Name: COLUMN image_quality.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.image_quality.name IS 'A name for the quality level.';


--
-- Name: COLUMN image_quality.rank; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.image_quality.rank IS 'An ordinal rank (zero is high) for the quality level.';


--
-- Name: COLUMN image_quality.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.image_quality.note IS 'An optional note.';


--
-- Name: COLUMN image_quality.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.image_quality.short_code IS 'Contains a short code that can be used to look up an image quality (e.g., during import) without relying on the primary key.';


--
-- Name: image_quality_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.image_quality_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.image_quality_id_seq OWNER TO msea_admin;

--
-- Name: image_quality_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.image_quality_id_seq OWNED BY rov.image_quality.id;


--
-- Name: import_queue_annotator; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.import_queue_annotator (
    id integer NOT NULL,
    user_id integer NOT NULL,
    note text,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    name character varying(64) NOT NULL,
    status character varying(32),
    cruise_name character varying(32) NOT NULL,
    data jsonb NOT NULL,
    hidden boolean DEFAULT false NOT NULL,
    objective text,
    start_date date,
    end_date date
);


ALTER TABLE rov.import_queue_annotator OWNER TO msea_admin;

--
-- Name: TABLE import_queue_annotator; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.import_queue_annotator IS 'Stores the import packages created by annotators. Some fields are transferred to the [annotation job](#rov.annotation_job) to record the objectives of the project. Each queue item (and therefore each job) should correspond to a single annotation project, and not encompass multiple projects with different objectives.';


--
-- Name: COLUMN import_queue_annotator.user_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.user_id IS 'A reference to the [MSEA user](#shared.mseauser) that created the record.';


--
-- Name: COLUMN import_queue_annotator.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.note IS 'An optional note about the import package.';


--
-- Name: COLUMN import_queue_annotator.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.created_on IS 'The date of creation of the record.';


--
-- Name: COLUMN import_queue_annotator.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.updated_on IS 'The date of update of the record.';


--
-- Name: COLUMN import_queue_annotator.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.name IS 'A unique name for the import queue record.';


--
-- Name: COLUMN import_queue_annotator.status; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.status IS 'A short description of the processing status of the job.';


--
-- Name: COLUMN import_queue_annotator.cruise_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.cruise_name IS 'The name of the [cruise](#rov.cruise) to which this record is linked. A lookup is not used because the cruise record may not have been created yet.';


--
-- Name: COLUMN import_queue_annotator.data; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator.data IS 'Stores the JSON representation of the import job.';


--
-- Name: import_queue_annotation_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.import_queue_annotation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.import_queue_annotation_id_seq OWNER TO msea_admin;

--
-- Name: import_queue_annotation_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.import_queue_annotation_id_seq OWNED BY rov.import_queue_annotator.id;


--
-- Name: import_queue_annotator_label_map_prefill; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.import_queue_annotator_label_map_prefill (
    id integer NOT NULL,
    tags jsonb NOT NULL,
    properties jsonb NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    name character varying(512) NOT NULL,
    label_tree_name character varying(128),
    event_type jsonb
);


ALTER TABLE rov.import_queue_annotator_label_map_prefill OWNER TO msea_admin;

--
-- Name: TABLE import_queue_annotator_label_map_prefill; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.import_queue_annotator_label_map_prefill IS 'A table to store the last configured tags and values for labels. Provides pre-filling in label mapping application.';


--
-- Name: COLUMN import_queue_annotator_label_map_prefill.tags; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator_label_map_prefill.tags IS 'The list of tags.';


--
-- Name: COLUMN import_queue_annotator_label_map_prefill.properties; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator_label_map_prefill.properties IS 'The the tag data.';


--
-- Name: COLUMN import_queue_annotator_label_map_prefill.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator_label_map_prefill.created_on IS 'The date and time of creation of the entity.';


--
-- Name: COLUMN import_queue_annotator_label_map_prefill.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator_label_map_prefill.updated_on IS 'The date and time of the entity''s last update.';


--
-- Name: COLUMN import_queue_annotator_label_map_prefill.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator_label_map_prefill.name IS 'The text of the label.';


--
-- Name: COLUMN import_queue_annotator_label_map_prefill.label_tree_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_annotator_label_map_prefill.label_tree_name IS 'The label tree name. If given identifies the label uniquely with the tree name.';


--
-- Name: import_queue_annotator_label_map_prefill_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.import_queue_annotator_label_map_prefill_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.import_queue_annotator_label_map_prefill_id_seq OWNER TO msea_admin;

--
-- Name: import_queue_annotator_label_map_prefill_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.import_queue_annotator_label_map_prefill_id_seq OWNED BY rov.import_queue_annotator_label_map_prefill.id;


--
-- Name: import_queue_pi; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.import_queue_pi (
    id integer NOT NULL,
    user_id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    status character varying(32),
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    cruise_id integer NOT NULL,
    hidden boolean DEFAULT false NOT NULL
);


ALTER TABLE rov.import_queue_pi OWNER TO msea_admin;

--
-- Name: TABLE import_queue_pi; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.import_queue_pi IS 'Stores the import packages created by principal investigators.';


--
-- Name: COLUMN import_queue_pi.user_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_pi.user_id IS 'A reference to the [MSEA user](#shared.mseauser) that created the record.';


--
-- Name: COLUMN import_queue_pi.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_pi.name IS 'A name of the import job';


--
-- Name: COLUMN import_queue_pi.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_pi.note IS 'An optional note about the import package.';


--
-- Name: COLUMN import_queue_pi.status; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_pi.status IS 'A short description of the processing status of the job.';


--
-- Name: COLUMN import_queue_pi.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_pi.created_on IS 'The date of creation of the record.';


--
-- Name: COLUMN import_queue_pi.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_pi.updated_on IS 'The date of update of the record.';


--
-- Name: COLUMN import_queue_pi.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.import_queue_pi.cruise_id IS 'A reference to the [cruise](#rov.cruise).';


--
-- Name: import_queue_pi_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.import_queue_pi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.import_queue_pi_id_seq OWNER TO msea_admin;

--
-- Name: import_queue_pi_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.import_queue_pi_id_seq OWNED BY rov.import_queue_pi.id;


--
-- Name: instrument; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.instrument (
    id integer NOT NULL,
    model_id integer NOT NULL,
    serial_number character varying(256) NOT NULL,
    retired date,
    attributes jsonb,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    short_code character(32),
    organisation_id integer NOT NULL
);


ALTER TABLE rov.instrument OWNER TO msea_admin;

--
-- Name: TABLE instrument; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.instrument IS 'This table represents instruments, which are concrete instances of the types represented in the [model](#rov.model) table. instruments tend to be things that generate data, be it a thermometer or a camera. For the purposes of this database, an instrument is anything that is not a [platform](#rov.platform).';


--
-- Name: COLUMN instrument.model_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.model_id IS 'A reference to the instrument [model](#rov.model).';


--
-- Name: COLUMN instrument.serial_number; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.serial_number IS 'The serial number of the instrument. If a serial number is not available, some unique identifier can be substituted. No two instruments of the same model may have the same serial number.';


--
-- Name: COLUMN instrument.retired; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.retired IS 'If the instrument is retired, this records the retirement date. If null, the instrument is assumed to be active.';


--
-- Name: COLUMN instrument.attributes; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.attributes IS 'A freeform list of attributes for this instrument.';


--
-- Name: COLUMN instrument.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.note IS 'An optional note about this instrument.';


--
-- Name: COLUMN instrument.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.created_on IS 'The date of creation of this record.';


--
-- Name: COLUMN instrument.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN instrument.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.short_code IS 'Contains a short code that can be used to look up an instrument (e.g., during import) without relying on the primary key.';


--
-- Name: COLUMN instrument.organisation_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.instrument.organisation_id IS 'A reference to the organisation that owns and operates the instrument.';


--
-- Name: instrument_config_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.instrument_config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.instrument_config_id_seq OWNER TO msea_admin;

--
-- Name: instrument_config_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.instrument_config_id_seq OWNED BY rov.instrument_config.id;


--
-- Name: instrument_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.instrument_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.instrument_id_seq OWNER TO msea_admin;

--
-- Name: instrument_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.instrument_id_seq OWNED BY rov.instrument.id;


--
-- Name: measurement_event; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.measurement_event (
    event_id integer NOT NULL,
    measurement_type_id integer NOT NULL,
    quantity real NOT NULL
);


ALTER TABLE rov.measurement_event OWNER TO msea_admin;

--
-- Name: TABLE measurement_event; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.measurement_event IS 'A table for storing human-created measurements.';


--
-- Name: COLUMN measurement_event.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_event.event_id IS 'A reference to the parent [event](#rov.event).';


--
-- Name: COLUMN measurement_event.measurement_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_event.measurement_type_id IS 'A reference to the [measurement type](#rov.measurement_type).';


--
-- Name: COLUMN measurement_event.quantity; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.measurement_event.quantity IS 'The measurement value in the [measurement type](#rov.measurement_type) unit.';


--
-- Name: measurement_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.measurement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.measurement_id_seq OWNER TO msea_admin;

--
-- Name: measurement_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.measurement_id_seq OWNED BY rov.measurement.id;


--
-- Name: measurement_type_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.measurement_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.measurement_type_id_seq OWNER TO msea_admin;

--
-- Name: measurement_type_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.measurement_type_id_seq OWNED BY rov.measurement_type.id;


--
-- Name: medium_format; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.medium_format (
    id integer NOT NULL,
    medium_type_id integer NOT NULL,
    name character varying(32) NOT NULL,
    extensions jsonb NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.medium_format OWNER TO msea_admin;

--
-- Name: TABLE medium_format; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.medium_format IS 'A lookup table for data formats for the [medium](#rov.medium) table. This will include things like video, photo and acoustic. TODO: To be determined whether medium formats should be discriminated more granularly than this. TODO: Should maybe be categorised into video/photo/acoustic/etc. along with things like the format (JPG, MP4, AAC) and possibly more specific encoding parameters.';


--
-- Name: COLUMN medium_format.medium_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_format.medium_type_id IS 'A reference to the [medium type](#rov.medium_type) (e.g., video or photo).';


--
-- Name: COLUMN medium_format.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_format.name IS 'The name of the format.';


--
-- Name: COLUMN medium_format.extensions; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_format.extensions IS 'A list of file extensions that correspond to this medium type. For example, the JPEG image type may have extensions "jpg" or "jpeg" in any case. This list will help applications guess the correct format of a file if it isn''t known. Note that more than one media type can have the same extension, so this feature doesn''t provide a guaranteed one-to-one mapping. Use it only as a guide.';


--
-- Name: COLUMN medium_format.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_format.note IS 'An optional note about the format.';


--
-- Name: COLUMN medium_format.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_format.short_code IS 'Contains a short code that can be used to look up a medium format (e.g., during import) without relying on the primary key.';


--
-- Name: medium_format_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.medium_format_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.medium_format_id_seq OWNER TO msea_admin;

--
-- Name: medium_format_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.medium_format_id_seq OWNED BY rov.medium_format.id;


--
-- Name: medium_type; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.medium_type (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    note text,
    short_code character(16)
);


ALTER TABLE rov.medium_type OWNER TO msea_admin;

--
-- Name: TABLE medium_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.medium_type IS 'A simple lookup to provide media types to the [annotation_protocol](#rov.annotation_protocol) table. These are not specific media formats (as stored in [medium_format](#rov.medium_format)), but provided a higher-level distinction.';


--
-- Name: COLUMN medium_type.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_type.name IS 'The name of the media type.';


--
-- Name: COLUMN medium_type.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_type.note IS 'An optional note about the media type.';


--
-- Name: COLUMN medium_type.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.medium_type.short_code IS 'Provides a short code for looking up the entity.';


--
-- Name: medium_type_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.medium_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.medium_type_id_seq OWNER TO msea_admin;

--
-- Name: medium_type_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.medium_type_id_seq OWNED BY rov.medium_type.id;


--
-- Name: model; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.model (
    id integer NOT NULL,
    brand_name character varying(64) NOT NULL,
    model_name character varying(64) NOT NULL,
    attributes jsonb,
    note text,
    equipment_type_id integer NOT NULL
);


ALTER TABLE rov.model OWNER TO msea_admin;

--
-- Name: TABLE model; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.model IS 'This table records the brand and model of equipment in the inventory. This data is abstract, which is to say, there is only one record for "GoPro Hero4" but there will be one record for each concrete instance of the model in the [instrument](#rov.instrument) table. The model table stores both [instruments](#rov.instrument) and [platforms](#rov.platform).';


--
-- Name: COLUMN model.brand_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model.brand_name IS 'The brand name.';


--
-- Name: COLUMN model.model_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model.model_name IS 'The model name.';


--
-- Name: COLUMN model.attributes; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model.attributes IS 'A free-form JSON field for attributes of this model.';


--
-- Name: COLUMN model.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model.note IS 'An optional note about the model.';


--
-- Name: COLUMN model.equipment_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model.equipment_type_id IS 'A reference to the [equipment type](#rov.equipment_type).';


--
-- Name: model_documentation; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.model_documentation (
    id integer NOT NULL,
    model_id integer NOT NULL,
    library_id integer NOT NULL,
    note text
);


ALTER TABLE rov.model_documentation OWNER TO msea_admin;

--
-- Name: TABLE model_documentation; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.model_documentation IS 'This table creates an association between a [model](#rov.model) and [documentation in the library](#rov.library).';


--
-- Name: COLUMN model_documentation.model_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model_documentation.model_id IS 'A reference to the [model](#rov.model).';


--
-- Name: COLUMN model_documentation.library_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model_documentation.library_id IS 'A reference to the document in the [library](#rov.library).';


--
-- Name: COLUMN model_documentation.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model_documentation.note IS 'An optional note about the document with respect to the cruise leg. May be used to reference points of interest in a paper, etc.';


--
-- Name: model_documentation_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.model_documentation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.model_documentation_id_seq OWNER TO msea_admin;

--
-- Name: model_documentation_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.model_documentation_id_seq OWNED BY rov.model_documentation.id;


--
-- Name: model_equipment_type; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.model_equipment_type (
    id integer NOT NULL,
    equipment_type_id integer NOT NULL,
    model_id integer NOT NULL
);


ALTER TABLE rov.model_equipment_type OWNER TO msea_admin;

--
-- Name: TABLE model_equipment_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.model_equipment_type IS 'This table associates an equipment [model](#rov.model) with the abstract type of equipment it represents. For example, a GoPro Hero4 is both a digital video camera and a digital still camera. These values are taken from the [equipment_type](#rov.equipment_type) table.';


--
-- Name: COLUMN model_equipment_type.equipment_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model_equipment_type.equipment_type_id IS 'The [equipment type](#rov.equipment_type) ID.';


--
-- Name: COLUMN model_equipment_type.model_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.model_equipment_type.model_id IS 'A reference to the [model](#rov.model).';


--
-- Name: model_equipment_type_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.model_equipment_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.model_equipment_type_id_seq OWNER TO msea_admin;

--
-- Name: model_equipment_type_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.model_equipment_type_id_seq OWNED BY rov.model_equipment_type.id;


--
-- Name: model_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.model_id_seq OWNER TO msea_admin;

--
-- Name: model_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.model_id_seq OWNED BY rov.model.id;


--
-- Name: observation; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.observation (
    event_id integer NOT NULL,
    properties jsonb DEFAULT '{}'::jsonb NOT NULL,
    annotation_job_id integer,
    instrument_config_id integer,
    reviewed_by_id integer,
    reviewed_on timestamp(6) without time zone,
    review_note text
);


ALTER TABLE rov.observation OWNER TO msea_admin;

--
-- Name: observation_confidence; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.observation_confidence (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    rank integer NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.observation_confidence OWNER TO msea_admin;

--
-- Name: TABLE observation_confidence; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.observation_confidence IS 'Provides a nominal observation confidence level for [observation events](#rov.observation_event).';


--
-- Name: COLUMN observation_confidence.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_confidence.name IS 'The textual representation of the confidence level.';


--
-- Name: COLUMN observation_confidence.rank; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_confidence.rank IS 'This field is a way of ranking confidence levels so that an ordering can be established.';


--
-- Name: COLUMN observation_confidence.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_confidence.note IS 'A note about the confidence level.';


--
-- Name: COLUMN observation_confidence.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_confidence.short_code IS 'Contains a short code that can be used to look up a observation confidence (e.g., during import) without relying on the primary key.';


--
-- Name: observation_confidence_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.observation_confidence_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.observation_confidence_id_seq OWNER TO msea_admin;

--
-- Name: observation_confidence_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.observation_confidence_id_seq OWNED BY rov.observation_confidence.id;


--
-- Name: observation_event; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.observation_event (
    event_id integer NOT NULL,
    observation_confidence_id integer,
    abundance_id integer,
    count integer,
    category character varying(64),
    coverage_id integer,
    taxon_id integer
);


ALTER TABLE rov.observation_event OWNER TO msea_admin;

--
-- Name: TABLE observation_event; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.observation_event IS 'An observation event records the occurrence of a phenomenon as a result of observation by a human user, usually by analyzing a video recording or photograph. Observations can relate to a species taxonomy or an element in a [label tree](#rov.label_tree). This table will be "sparse," that is, certain a value will be given for only one or two columns in each row, and the interpretation of the values will be informed by the information in the [annotation protocol](#rov.annotation_protocol) table. This table is a realization of the [event](#rov.event) table.';


--
-- Name: COLUMN observation_event.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_event.event_id IS 'A reference to the parent [event](#rov.event).';


--
-- Name: COLUMN observation_event.observation_confidence_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_event.observation_confidence_id IS 'References the [observation confidence](#rov.observation_confidence) lookup to indicate the user''s confidence in the observation.';


--
-- Name: COLUMN observation_event.abundance_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_event.abundance_id IS 'Link to an [abundance level](#rov.abundance).';


--
-- Name: COLUMN observation_event.count; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_event.count IS 'The number of individuals observed. TODO: Interpretation depends on the observation interval as recorded in the[annotation protocol](#rov.annotation_protocol) table.';


--
-- Name: COLUMN observation_event.category; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_event.category IS 'A free-form category label used for distinguishing types of labels within an annotation project. Useful for flagging records for review.';


--
-- Name: COLUMN observation_event.coverage_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_event.coverage_id IS 'Provides an indication of the amount of a scene [covered](#rov.coverage) by an organism. Implies habitat forming.';


--
-- Name: COLUMN observation_event.taxon_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.observation_event.taxon_id IS 'A reference to the [taxon](#shared.taxon) used in the creation of this observation.';


--
-- Name: orientation; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.orientation (
    id integer NOT NULL,
    orientation_type_id integer NOT NULL,
    "timestamp" timestamp(6) without time zone NOT NULL,
    orientation jsonb NOT NULL,
    signal_quality real,
    is_modelled boolean DEFAULT false NOT NULL,
    instrument_config_id integer NOT NULL
);


ALTER TABLE rov.orientation OWNER TO msea_admin;

--
-- Name: TABLE orientation; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.orientation IS 'This table preserves time-stamped orientation measurements from [instruments](#rov.instrument_config). These are interpreted according to a [specified type](#rov.orientation_type). Ship and ROV orientation will be recorded here, though ships don''t ordinarily have an orientation.';


--
-- Name: COLUMN orientation.orientation_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation.orientation_type_id IS 'A reference to the [orientation type](#rov.orientation_type) of this orientation.';


--
-- Name: COLUMN orientation."timestamp"; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation."timestamp" IS 'The time the position was recorded.';


--
-- Name: COLUMN orientation.orientation; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation.orientation IS 'The orientation vector.';


--
-- Name: COLUMN orientation.signal_quality; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation.signal_quality IS 'The signal quality of the position as reported by the instrument. TODO: Requires clarification.';


--
-- Name: COLUMN orientation.is_modelled; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation.is_modelled IS 'A flag to indicate whether the value is derived from measurements by some modelling process.';


--
-- Name: COLUMN orientation.instrument_config_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation.instrument_config_id IS 'The [configured instrument](#rov.instrument_config) used to generate this item.';


--
-- Name: orientation_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.orientation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.orientation_id_seq OWNER TO msea_admin;

--
-- Name: orientation_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.orientation_id_seq OWNED BY rov.orientation.id;


--
-- Name: orientation_type; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.orientation_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    unit character varying(32) NOT NULL,
    note text,
    short_code character varying(8)
);


ALTER TABLE rov.orientation_type OWNER TO msea_admin;

--
-- Name: TABLE orientation_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.orientation_type IS 'These entities describe the interpretation of the orientation vector in the [instrument_config](#rov.instrument_config) and [orientation](#rov.orientation) tables. This can be a 3- or 4-element vector representing yaw, pitch roll; Tait-Bryan angles; Euler angles or a Quaternion.';


--
-- Name: COLUMN orientation_type.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation_type.name IS 'The name of the orientation type (e.g., "Quaternion").';


--
-- Name: COLUMN orientation_type.unit; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation_type.unit IS 'The units used to interpret the elements of the vector.';


--
-- Name: COLUMN orientation_type.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation_type.note IS 'An optional note about the orientation type.';


--
-- Name: COLUMN orientation_type.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.orientation_type.short_code IS 'Contains a short code that can be used to look up an orientation type (e.g., during import) without relying on the primary key.';


--
-- Name: orientation_type_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.orientation_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.orientation_type_id_seq OWNER TO msea_admin;

--
-- Name: orientation_type_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.orientation_type_id_seq OWNED BY rov.orientation_type.id;


--
-- Name: platform; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.platform (
    id integer NOT NULL,
    model_id integer NOT NULL,
    name character varying(64) NOT NULL,
    serial_number character varying(256),
    retired date,
    attributes jsonb,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    short_code character(32),
    organisation_id integer NOT NULL
);


ALTER TABLE rov.platform OWNER TO msea_admin;

--
-- Name: TABLE platform; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.platform IS 'This table maintains the inventory of vehicles, that is, ships and ROVs.';


--
-- Name: COLUMN platform.model_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.model_id IS 'A reference to the [model](#rov.model) of the platform. ';


--
-- Name: COLUMN platform.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.name IS 'The name of the platform. If this is a ship, it might be "CCGS Vector" or if it''s an ROV, it might be given an arbitrary name by the maintainer.';


--
-- Name: COLUMN platform.serial_number; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.serial_number IS 'The serial number of the platform. If this is an ROV it will be the manufacturer''s serial number. If it''s a vessel, this might be the IMO number. In any case, it must be unique.';


--
-- Name: COLUMN platform.retired; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.retired IS ' If the platform is retired, this records the date. If null, the platform is assumed to be active.';


--
-- Name: COLUMN platform.attributes; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.attributes IS 'A freeform list of attributes for this platform.';


--
-- Name: COLUMN platform.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.note IS 'An optional note about this platform.';


--
-- Name: COLUMN platform.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.created_on IS 'The date of creation of this record.';


--
-- Name: COLUMN platform.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN platform.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.short_code IS 'Contains a short code that can be used to look up a platform (e.g., during import) without relying on the primary key.';


--
-- Name: COLUMN platform.organisation_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.platform.organisation_id IS 'A reference to the organisation that owns and operates the platform.';


--
-- Name: platform_config_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.platform_config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.platform_config_id_seq OWNER TO msea_admin;

--
-- Name: platform_config_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.platform_config_id_seq OWNED BY rov.platform_config.id;


--
-- Name: platform_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.platform_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.platform_id_seq OWNER TO msea_admin;

--
-- Name: platform_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.platform_id_seq OWNED BY rov.platform.id;


--
-- Name: position_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.position_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.position_id_seq OWNER TO msea_admin;

--
-- Name: position_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.position_id_seq OWNED BY rov."position".id;


--
-- Name: position_type; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.position_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    unit character varying(32) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.position_type OWNER TO msea_admin;

--
-- Name: TABLE position_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.position_type IS 'Describes the interpretation of a coordinate vector in the [position](#rov.position) table. These can be geographic or Cartesian positions or orientations and have defined linear or angular units.';


--
-- Name: COLUMN position_type.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.position_type.name IS 'The name of the position type. E.g., "Geographic" or "Yaw, Pitch, Roll."';


--
-- Name: COLUMN position_type.unit; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.position_type.unit IS 'The unit. Linear or angular. E.g., "m" or "radians."';


--
-- Name: COLUMN position_type.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.position_type.note IS 'An optional note about this position type.';


--
-- Name: COLUMN position_type.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.position_type.short_code IS 'Contains a short code that can be used to look up a position type (e.g., during import) without relying on the primary key.';


--
-- Name: position_type_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.position_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.position_type_id_seq OWNER TO msea_admin;

--
-- Name: position_type_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.position_type_id_seq OWNED BY rov.position_type.id;


--
-- Name: program; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.program (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    objective character varying(255),
    start_date date NOT NULL,
    end_date date,
    note text,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    summary text
);


ALTER TABLE rov.program OWNER TO msea_admin;

--
-- Name: TABLE program; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.program IS 'This is a listing of scientific programs that can be associated with [cruise legs](#rov.cruise_leg), and gives information about the governorship and funding of specific research activities. [Label trees](#rov.label_tree) and [taxonomies](#shared.taxonomy) may be associated with programs, since some trees are relevant to specific research objectives and not others. TODO: Currently a lable tree/taxonomy can only be linked to one program. Need to study further.';


--
-- Name: COLUMN program.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.name IS 'The name of the program.';


--
-- Name: COLUMN program.objective; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.objective IS 'The objective or mandate of the program.';


--
-- Name: COLUMN program.start_date; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.start_date IS 'The starting date of the program.';


--
-- Name: COLUMN program.end_date; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.end_date IS 'The optional end date of the program.';


--
-- Name: COLUMN program.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.note IS 'An optional note about the program.';


--
-- Name: COLUMN program.created_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.created_on IS 'The date of creation of this item.';


--
-- Name: COLUMN program.updated_on; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN program.summary; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program.summary IS 'A summary of the [program](#rov.program); whether objectives were met, problems encountered, etc.';


--
-- Name: program_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.program_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.program_id_seq OWNER TO msea_admin;

--
-- Name: program_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.program_id_seq OWNED BY rov.program.id;


--
-- Name: program_library; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.program_library (
    id integer NOT NULL,
    library_id integer NOT NULL,
    program_id integer NOT NULL
);


ALTER TABLE rov.program_library OWNER TO msea_admin;

--
-- Name: TABLE program_library; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.program_library IS 'A table to associate [programs](#rov.program) with [library](#shared.library) documents.';


--
-- Name: COLUMN program_library.library_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program_library.library_id IS 'A reference to the [library](#shared.library) item.';


--
-- Name: COLUMN program_library.program_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program_library.program_id IS 'A reference to the [program](#rov.program) item.';


--
-- Name: program_library_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.program_library_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.program_library_id_seq OWNER TO msea_admin;

--
-- Name: program_library_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.program_library_id_seq OWNED BY rov.program_library.id;


--
-- Name: program_member; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.program_member (
    id integer NOT NULL,
    program_id integer NOT NULL,
    person_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE rov.program_member OWNER TO msea_admin;

--
-- Name: TABLE program_member; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.program_member IS 'Assigns [user](#rov.person) [roles](#rov.program_role) to a [program](#rov.program).';


--
-- Name: COLUMN program_member.program_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program_member.program_id IS 'The reference to the [program](#rov.program).';


--
-- Name: COLUMN program_member.person_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program_member.person_id IS 'A reference to the [person](#rov.person).';


--
-- Name: COLUMN program_member.role_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program_member.role_id IS 'A reference to the [program role](#rov.program_role).';


--
-- Name: program_member_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.program_member_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.program_member_id_seq OWNER TO msea_admin;

--
-- Name: program_member_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.program_member_id_seq OWNED BY rov.program_member.id;


--
-- Name: program_role; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.program_role (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text
);


ALTER TABLE rov.program_role OWNER TO msea_admin;

--
-- Name: TABLE program_role; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.program_role IS 'Represents the roles a [person](#rov.person) might perform in respect to a [program](#rov.program). A person can be assigned multiple roles within a single program, and multiple people can work on a program with the same role.';


--
-- Name: COLUMN program_role.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program_role.name IS 'The name of the role. E.g., "Chief Scientist."';


--
-- Name: COLUMN program_role.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.program_role.note IS 'An optional note about the role.';


--
-- Name: program_role_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.program_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.program_role_id_seq OWNER TO msea_admin;

--
-- Name: program_role_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.program_role_id_seq OWNED BY rov.program_role.id;


--
-- Name: protocol; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.protocol (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.protocol OWNER TO msea_admin;

--
-- Name: TABLE protocol; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.protocol IS 'This table stores "(survey) protocol" entries from the source data. TODO: Used by [events](#rov.event) though it''s not yet clear if it should stay this way.';


--
-- Name: COLUMN protocol.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.protocol.name IS 'A name for the survey protocol.';


--
-- Name: COLUMN protocol.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.protocol.note IS 'An optional note about the protocol.';


--
-- Name: COLUMN protocol.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.protocol.short_code IS 'Contains a short code that can be used to look up a survey protocol (e.g., during import) without relying on the primary key.';


--
-- Name: protocol_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.protocol_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.protocol_id_seq OWNER TO msea_admin;

--
-- Name: protocol_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.protocol_id_seq OWNED BY rov.protocol.id;


--
-- Name: relief; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.relief (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.relief OWNER TO msea_admin;

--
-- Name: TABLE relief; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.relief IS 'Provides a nominal level of terrain relief for [habitat events](#rov.habitat_event).';


--
-- Name: COLUMN relief.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.relief.name IS 'The textual representation of the relief level.';


--
-- Name: COLUMN relief.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.relief.note IS 'A note about the relief level.';


--
-- Name: COLUMN relief.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.relief.short_code IS 'Contains a short code that can be used to look up a relief (e.g., during import) without relying on the primary key.';


--
-- Name: relief_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.relief_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.relief_id_seq OWNER TO msea_admin;

--
-- Name: relief_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.relief_id_seq OWNED BY rov.relief.id;


--
-- Name: signup_area; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.signup_area (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    geom public.geography(Polygon,4326)
);


ALTER TABLE rov.signup_area OWNER TO msea_admin;

--
-- Name: TABLE signup_area; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.signup_area IS 'Stores the region within which sign-ups are permitted, generally North America.';


--
-- Name: COLUMN signup_area.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.signup_area.name IS 'The name of the sign-up region.';


--
-- Name: COLUMN signup_area.geom; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.signup_area.geom IS 'The geometry of the sign-up region.';


--
-- Name: signup_area_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.signup_area_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.signup_area_id_seq OWNER TO msea_admin;

--
-- Name: signup_area_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.signup_area_id_seq OWNED BY rov.signup_area.id;


--
-- Name: status_event; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.status_event (
    event_id integer NOT NULL,
    status_type_detail_id integer NOT NULL
);


ALTER TABLE rov.status_event OWNER TO msea_admin;

--
-- Name: TABLE status_event; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.status_event IS 'This table records status events with names given by the [status_type_detail](#rov.status_type_detail) table. Status events represent a state that changes at some time and persists until another state preempts it. For example, "on bottom" would persist until an "off bottom" event is recorded. A status_event is a specialization of an [event](#rov.event).';


--
-- Name: COLUMN status_event.event_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_event.event_id IS 'The associated [event](#rov.event).';


--
-- Name: COLUMN status_event.status_type_detail_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_event.status_type_detail_id IS 'The [detailed status type](#rov.status_type_detail).';


--
-- Name: status_type; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.status_type (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character(32)
);


ALTER TABLE rov.status_type OWNER TO msea_admin;

--
-- Name: TABLE status_type; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.status_type IS 'This is a lookup table for available status types, such as "on bottom" or "off bottom." These are specialized in the [status_type_detail](#rov.status_type_detail) table where a note distinguishes different flavours of a given type. For example, there can be multiple types of "Off Transect" events with a different explanation for each.';


--
-- Name: COLUMN status_type.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_type.name IS 'The name of the status event.';


--
-- Name: COLUMN status_type.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_type.note IS 'An optional description of the status type.';


--
-- Name: COLUMN status_type.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_type.short_code IS 'A short code for referencing the status type in import documents.';


--
-- Name: status_type_detail; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.status_type_detail (
    id integer NOT NULL,
    status_type_id integer NOT NULL,
    detail character varying(64) NOT NULL,
    note text,
    short_code character(32)
);


ALTER TABLE rov.status_type_detail OWNER TO msea_admin;

--
-- Name: TABLE status_type_detail; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.status_type_detail IS 'This table associates a [status type](#rov.status_type) with a [status event](#rov.status_event). The point of this indirection is to allow the addition of detail relative to the status type. For example, there are multiple reasons why an ROV might be "off transect", including that the vehicle has experienced a failure, or the pilot is investigating some interesting object with no research value. The [status type](#rov.status_type) value is the discriminator for the event, but the detail adds context the doesn''t interfere with it.';


--
-- Name: COLUMN status_type_detail.status_type_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_type_detail.status_type_id IS 'A reference to the [status type](#rov.status_type).';


--
-- Name: COLUMN status_type_detail.detail; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_type_detail.detail IS 'The status type detail. This field distinguishes different uses for [status types](#rov.status_type). For example, there can be more than one reason to record an "Off Transect" event.';


--
-- Name: COLUMN status_type_detail.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_type_detail.note IS 'Optional extended description of detail.';


--
-- Name: COLUMN status_type_detail.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.status_type_detail.short_code IS 'Contains a short code that can be used to look up a status type detail (e.g., during import) without relying on the primary key.';


--
-- Name: status_type_detail_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.status_type_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.status_type_detail_id_seq OWNER TO msea_admin;

--
-- Name: status_type_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.status_type_detail_id_seq OWNED BY rov.status_type_detail.id;


--
-- Name: status_type_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.status_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.status_type_id_seq OWNER TO msea_admin;

--
-- Name: status_type_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.status_type_id_seq OWNED BY rov.status_type.id;


--
-- Name: substrate; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.substrate (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(32)
);


ALTER TABLE rov.substrate OWNER TO msea_admin;

--
-- Name: TABLE substrate; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.substrate IS 'A lookup table listing the available substrate types for the [habitat_event](#rov.habitat_event) table. TODO: This table may be altered to provide a hierarchical list of types with increasing specificity.';


--
-- Name: COLUMN substrate.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.substrate.name IS 'The name of the substrate.';


--
-- Name: COLUMN substrate.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.substrate.note IS 'An optional description of the substrate.';


--
-- Name: COLUMN substrate.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.substrate.short_code IS 'Contains a short code that can be used to look up a substrate (e.g., during import) without relying on the primary key.';


--
-- Name: substrate_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.substrate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.substrate_id_seq OWNER TO msea_admin;

--
-- Name: substrate_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.substrate_id_seq OWNED BY rov.substrate.id;


--
-- Name: survey_mode; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.survey_mode (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.survey_mode OWNER TO msea_admin;

--
-- Name: TABLE survey_mode; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.survey_mode IS 'This table stores "survey mode" entries from the source data. TODO: Used by [events](#rov.event) though it''s not yet clear if it should stay this way.';


--
-- Name: COLUMN survey_mode.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.survey_mode.name IS 'The name of the survey mode.';


--
-- Name: COLUMN survey_mode.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.survey_mode.note IS 'An optional note about the survey mode.';


--
-- Name: COLUMN survey_mode.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.survey_mode.short_code IS 'Contains a short code that can be used to look up a survey mode (e.g., during import) without relying on the primary key.';


--
-- Name: survey_mode_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.survey_mode_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.survey_mode_id_seq OWNER TO msea_admin;

--
-- Name: survey_mode_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.survey_mode_id_seq OWNED BY rov.survey_mode.id;


--
-- Name: thickness; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.thickness (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    minimum real,
    maximum real,
    note text,
    short_code character varying(16)
);


ALTER TABLE rov.thickness OWNER TO msea_admin;

--
-- Name: TABLE thickness; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.thickness IS 'A lookup table listing the available substrate thicknesses for the [habitat_event](#rov.habitat_event) table. TODO: This table may be altered to provide a hierarchical list of types with increasing specificity.';


--
-- Name: COLUMN thickness.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.thickness.name IS 'A characterization of biocover thickness.';


--
-- Name: COLUMN thickness.minimum; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.thickness.minimum IS 'The minimum value in the range.';


--
-- Name: COLUMN thickness.maximum; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.thickness.maximum IS 'The maximum value in the range.';


--
-- Name: COLUMN thickness.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.thickness.note IS 'An optional note about the biocover thickness.';


--
-- Name: COLUMN thickness.short_code; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.thickness.short_code IS 'Contains a short code that can be used to look up a thickness (e.g., during import) without relying on the primary key.';


--
-- Name: thickness_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.thickness_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.thickness_id_seq OWNER TO msea_admin;

--
-- Name: thickness_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.thickness_id_seq OWNED BY rov.thickness.id;


--
-- Name: transect; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.transect (
    id integer NOT NULL,
    dive_id integer NOT NULL,
    name character varying(64) NOT NULL,
    objective text,
    start_time timestamp(6) without time zone NOT NULL,
    end_time timestamp(6) without time zone,
    note text,
    attributes jsonb,
    summary text,
    admin_note text
);


ALTER TABLE rov.transect OWNER TO msea_admin;

--
-- Name: TABLE transect; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.transect IS 'A transect is a section of a [dive](#rov.dive) during which interesting data are collected. Every transect is associated with a [dive](#rov.dive).';


--
-- Name: COLUMN transect.dive_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.dive_id IS 'A reference to the [dive](#rov.dive) during which this transect occurred.';


--
-- Name: COLUMN transect.name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.name IS 'The name of the transect.';


--
-- Name: COLUMN transect.objective; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.objective IS 'A statement of the practical or research objectives for this true.';


--
-- Name: COLUMN transect.start_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.start_time IS 'The start time of the transect.';


--
-- Name: COLUMN transect.end_time; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.end_time IS 'The end time of the transect.';


--
-- Name: COLUMN transect.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.note IS 'An optional note about the transect.';


--
-- Name: COLUMN transect.attributes; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.attributes IS 'A JSON column used for recording structured attributes that do not fit with the regular table structure.';


--
-- Name: COLUMN transect.summary; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect.summary IS 'A summary of the [transect](#rov.transect); whether objectives were met, problems encountered, etc.';


--
-- Name: transect_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.transect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.transect_id_seq OWNER TO msea_admin;

--
-- Name: transect_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.transect_id_seq OWNED BY rov.transect.id;


--
-- Name: transect_track; Type: MATERIALIZED VIEW; Schema: rov; Owner: msea_admin
--

CREATE MATERIALIZED VIEW rov.transect_track AS
 WITH t AS (
         SELECT f.id AS cruise_id,
            d.id AS dive_id,
            e.id AS transect_id,
            concat(f.name, '-', f.leg) AS cruise_name,
            d.name AS dive_name,
            e.name AS transect_name,
            concat('#', SUBSTRING(md5((f.id)::text) FROM 1 FOR 6)) AS colour,
            a.id AS position_id,
            a."timestamp",
            a.instrument_config_id,
            (a.geom)::public.geometry AS geom
           FROM (((((rov."position" a
             JOIN rov.instrument_config b ON ((b.id = a.instrument_config_id)))
             JOIN rov.platform_config c ON ((c.id = b.platform_config_id)))
             JOIN rov.dive d ON ((d.sub_config_id = c.id)))
             JOIN rov.cruise f ON ((f.id = d.cruise_id)))
             JOIN rov.transect e ON ((e.dive_id = d.id)))
          WHERE ((e.start_time <= a."timestamp") AND (e.end_time > a."timestamp"))
          ORDER BY a."timestamp"
        )
 SELECT cruise_id,
    dive_id,
    transect_id,
    cruise_name,
    dive_name,
    transect_name,
    colour,
    public.st_force2d(public.st_makeline(geom ORDER BY "timestamp")) AS geom
   FROM t
  GROUP BY cruise_id, dive_id, transect_id, dive_name, cruise_name, transect_name, colour
  WITH NO DATA;


ALTER MATERIALIZED VIEW rov.transect_track OWNER TO msea_admin;

--
-- Name: MATERIALIZED VIEW transect_track; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON MATERIALIZED VIEW rov.transect_track IS 'Constructs a geometry for each [transect](#rov.transect) which describes the path of the submersible.';


--
-- Name: COLUMN transect_track.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.cruise_id IS 'The reference to the [cruise](#rov.cruise).';


--
-- Name: COLUMN transect_track.dive_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.dive_id IS 'The reference to the [dive](#rov.dive).';


--
-- Name: COLUMN transect_track.transect_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.transect_id IS 'The reference to the [transect](#rov.transect).';


--
-- Name: COLUMN transect_track.cruise_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.cruise_name IS 'The [cruise](#rov.cruise) name.';


--
-- Name: COLUMN transect_track.dive_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.dive_name IS 'The [dive](#rov.dive) name.';


--
-- Name: COLUMN transect_track.transect_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.transect_name IS 'The [transect](#rov.transect) name.';


--
-- Name: COLUMN transect_track.colour; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.colour IS 'The a colour code generated from the cruise''s ID. Used for cartography.';


--
-- Name: COLUMN transect_track.geom; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.transect_track.geom IS 'The transect track geometry.';


--
-- Name: weather_observation; Type: TABLE; Schema: rov; Owner: msea_admin
--

CREATE TABLE rov.weather_observation (
    id integer NOT NULL,
    cruise_crew_id integer,
    temperature real,
    pressure real,
    wind_speed real,
    wind_direction real,
    swell character varying(32),
    "time" timestamp(6) without time zone NOT NULL,
    note text,
    cruise_id integer NOT NULL
);


ALTER TABLE rov.weather_observation OWNER TO msea_admin;

--
-- Name: TABLE weather_observation; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON TABLE rov.weather_observation IS 'Surface weather observations can be recorded by any [crew member](#rov.cruise_leg_crew) aboard a ship during a [cruise leg](#rov.cruise_leg).';


--
-- Name: COLUMN weather_observation.cruise_crew_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.cruise_crew_id IS 'A reference to the [cruise leg crew](#rov.cruise_leg_crew) member who is making the report.';


--
-- Name: COLUMN weather_observation.temperature; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.temperature IS 'The air temperature.';


--
-- Name: COLUMN weather_observation.pressure; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.pressure IS 'The air pressure.';


--
-- Name: COLUMN weather_observation.wind_speed; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.wind_speed IS 'The wind speed.';


--
-- Name: COLUMN weather_observation.wind_direction; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.wind_direction IS 'The wind direction.';


--
-- Name: COLUMN weather_observation.swell; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.swell IS 'Description of swell.';


--
-- Name: COLUMN weather_observation."time"; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation."time" IS 'An optional note about the weather.';


--
-- Name: COLUMN weather_observation.note; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.note IS 'An optional note about the observation';


--
-- Name: COLUMN weather_observation.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.weather_observation.cruise_id IS 'A link to the [cruise](#rov.cruise) during which this record was recorded.';


--
-- Name: weather_observation_id_seq; Type: SEQUENCE; Schema: rov; Owner: msea_admin
--

CREATE SEQUENCE rov.weather_observation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE rov.weather_observation_id_seq OWNER TO msea_admin;

--
-- Name: weather_observation_id_seq; Type: SEQUENCE OWNED BY; Schema: rov; Owner: msea_admin
--

ALTER SEQUENCE rov.weather_observation_id_seq OWNED BY rov.weather_observation.id;


--
-- Name: db_version; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.db_version (
    id integer NOT NULL,
    version_major integer NOT NULL,
    version_minor integer NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    revision integer NOT NULL
);


ALTER TABLE shared.db_version OWNER TO msea_admin;

--
-- Name: TABLE db_version; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.db_version IS 'Stores the current database version so that upgrade scripts can perform migrations appropriately.';


--
-- Name: COLUMN db_version.version_major; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.db_version.version_major IS 'The major version.';


--
-- Name: COLUMN db_version.version_minor; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.db_version.version_minor IS 'The minor version number.';


--
-- Name: COLUMN db_version.updated_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.db_version.updated_on IS 'The time the upgrade was performed.';


--
-- Name: COLUMN db_version.revision; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.db_version.revision IS 'The revision number.';


--
-- Name: db_version_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.db_version_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.db_version_id_seq OWNER TO msea_admin;

--
-- Name: db_version_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.db_version_id_seq OWNED BY shared.db_version.id;


--
-- Name: file; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.file (
    id integer NOT NULL,
    file_type_id integer,
    name character varying(255) NOT NULL,
    description text,
    metadata jsonb,
    path character varying(255) NOT NULL,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    hash character varying(64) NOT NULL,
    blob_url character varying(255)
);


ALTER TABLE shared.file OWNER TO msea_admin;

--
-- Name: TABLE file; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.file IS 'Stores a record of a file object and its location on disk, along with some metadata. This entity is used by all other entities in all schemas that refer to a file object. This should make it easier to audit file stores and e.g., find duplicates using the hash.';


--
-- Name: COLUMN file.file_type_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.file_type_id IS 'An optional reference to the [file type](#shared.file_type).';


--
-- Name: COLUMN file.name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.name IS 'The name for the file.';


--
-- Name: COLUMN file.description; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.description IS 'An optional description for the file.';


--
-- Name: COLUMN file.metadata; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.metadata IS 'A JSON dictionary containing metadata relating to the file.';


--
-- Name: COLUMN file.path; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.path IS 'A path of the file, relative to the root directory where files are stored.';


--
-- Name: COLUMN file.created_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.created_on IS 'The creation time of the file record, not necessarily the file itself (this should be stored in metadata).';


--
-- Name: COLUMN file.updated_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.updated_on IS 'The update time of the file record, not necessarily the file itself (this should be stored in metadata).';


--
-- Name: COLUMN file.hash; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.hash IS 'And MD5 hash of the file data. Used to compare files and search for identical versions.';


--
-- Name: COLUMN file.blob_url; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file.blob_url IS 'A URL referencing the file in online blob storage. This field is subject to change depending on where the files reside and will change if they''re moved.';


--
-- Name: file_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.file_id_seq OWNER TO msea_admin;

--
-- Name: file_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.file_id_seq OWNED BY shared.file.id;


--
-- Name: file_type; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.file_type (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    short_code character varying(8) NOT NULL
);


ALTER TABLE shared.file_type OWNER TO msea_admin;

--
-- Name: TABLE file_type; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.file_type IS 'A list of file types.';


--
-- Name: COLUMN file_type.name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file_type.name IS 'The name of the file type.';


--
-- Name: COLUMN file_type.short_code; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.file_type.short_code IS 'A short code for referencing the file type in import documents.';


--
-- Name: file_type_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.file_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.file_type_id_seq OWNER TO msea_admin;

--
-- Name: file_type_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.file_type_id_seq OWNED BY shared.file_type.id;


--
-- Name: library; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.library (
    id integer NOT NULL,
    file_id integer NOT NULL,
    mendeley_id character varying(64),
    title text NOT NULL,
    abstract text,
    publication character varying(128),
    authors jsonb,
    keywords jsonb,
    institution character varying(128),
    year character varying(16),
    doi character varying(128),
    isbn character varying(128),
    issn character varying(128),
    type character varying(32),
    created_on timestamp(6) without time zone DEFAULT now(),
    updated_on timestamp(6) without time zone DEFAULT now()
);


ALTER TABLE shared.library OWNER TO msea_admin;

--
-- Name: TABLE library; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.library IS 'This table stores a partial record of documents, books and papers that can be referenced by other entities. The ISBN and DOI fields are set to unique, but these may vary (in representation, at least) for a single document so care should be taken not to add duplicates. However, updated papers with the same title and author, but different date, are not disallowed.';


--
-- Name: COLUMN library.file_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.file_id IS 'An optional reference to a [file](#shared.file).';


--
-- Name: COLUMN library.mendeley_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.mendeley_id IS 'The ID of the record in Mendeley.';


--
-- Name: COLUMN library.title; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.title IS 'The title of the book, paper, or other document.';


--
-- Name: COLUMN library.abstract; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.abstract IS 'The abstract of the entry.';


--
-- Name: COLUMN library.publication; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.publication IS 'The name of the publication in which the document appeared.';


--
-- Name: COLUMN library.authors; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.authors IS 'The (list of) author(s) of the document. This is a JSON list of objects containing whatever information is necessary. Authors should be listed with at least first_name and last_name, and any other relevant information, such as email or institution.';


--
-- Name: COLUMN library.keywords; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.keywords IS 'A list of keywords relating to the entry.';


--
-- Name: COLUMN library.institution; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.institution IS 'The name of the institution or publisher responsible for the document.';


--
-- Name: COLUMN library.year; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.year IS 'The publishing date of the document.';


--
-- Name: COLUMN library.doi; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.doi IS 'The DOI of the document.';


--
-- Name: COLUMN library.isbn; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.isbn IS 'The ISBN of the book, if it is a book.';


--
-- Name: COLUMN library.issn; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.issn IS 'The ISSN of the entry.';


--
-- Name: COLUMN library.type; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.type IS 'Describes the type of entry: book, article, etc.';


--
-- Name: COLUMN library.created_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.created_on IS 'The creation time of the file record, not necessarily the file itself (this should be stored in metadata).';


--
-- Name: COLUMN library.updated_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.library.updated_on IS 'The update time of the file record, not necessarily the file itself (this should be stored in metadata).';


--
-- Name: library_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.library_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.library_id_seq OWNER TO msea_admin;

--
-- Name: library_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.library_id_seq OWNED BY shared.library.id;


--
-- Name: mseauser; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.mseauser (
    id integer NOT NULL,
    user_id integer NOT NULL,
    biigle_username character varying(32),
    biigle_api_key character varying(32),
    pg_role character varying(32),
    organization character varying(125),
    org_type character varying(32),
    registration_reason text,
    bio text,
    verification_code character(32),
    verification_expiry timestamp without time zone,
    verification_time timestamp without time zone,
    registration_ip inet,
    ip_in_region boolean,
    registration_note text,
    allowed boolean DEFAULT true NOT NULL
);


ALTER TABLE shared.mseauser OWNER TO msea_admin;

--
-- Name: TABLE mseauser; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.mseauser IS 'Represents an MSEA user and is linked to a single Django auth User. Stores extra application-related properties such as the Biigle API key.';


--
-- Name: COLUMN mseauser.user_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.user_id IS 'A reference to the [Django user](#public.auth_user).';


--
-- Name: COLUMN mseauser.biigle_username; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.biigle_username IS 'The Biigle username.';


--
-- Name: COLUMN mseauser.biigle_api_key; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.biigle_api_key IS 'The Biigle API key.';


--
-- Name: COLUMN mseauser.pg_role; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.pg_role IS 'The name of the PostgreSQL role that the user will use to log in directly to the database.';


--
-- Name: COLUMN mseauser.organization; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.organization IS 'The organization with which a user is affiliated.';


--
-- Name: COLUMN mseauser.org_type; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.org_type IS 'The the type of organization with which the user is affiliated.';


--
-- Name: COLUMN mseauser.registration_reason; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.registration_reason IS 'A short note about why the user registered.';


--
-- Name: COLUMN mseauser.bio; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.bio IS 'Biographical information about the user.';


--
-- Name: COLUMN mseauser.verification_code; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.verification_code IS 'An auto-generated string used to identify the user for verification purposes.';


--
-- Name: COLUMN mseauser.verification_expiry; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.verification_expiry IS 'The time of expiration of the verification code.';


--
-- Name: COLUMN mseauser.verification_time; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.verification_time IS 'The date and time at which the user was verified.';


--
-- Name: COLUMN mseauser.registration_ip; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.registration_ip IS 'The IP used by the user to register.';


--
-- Name: COLUMN mseauser.ip_in_region; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.ip_in_region IS 'Set to true if the user''s IP is within the signup region. False positives and negatives are possible.';


--
-- Name: COLUMN mseauser.registration_note; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.registration_note IS 'Administrator notes about the user''s registration status.';


--
-- Name: COLUMN mseauser.allowed; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.mseauser.allowed IS 'If true, the user is allowed to sign in. This can be set to false during sign up (e.g., by failing the IP check) or by an admin.';


--
-- Name: mseauser_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.mseauser_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.mseauser_id_seq OWNER TO msea_admin;

--
-- Name: mseauser_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.mseauser_id_seq OWNED BY shared.mseauser.id;


--
-- Name: user_restriction; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.user_restriction (
    user_id integer NOT NULL,
    restriction_id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL,
    id integer NOT NULL
);


ALTER TABLE shared.user_restriction OWNER TO msea_admin;

--
-- Name: TABLE user_restriction; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.user_restriction IS 'Links an [MSEA user](#shared.mseauser) to a [restriction](#shared.restriction). The user will have access to records related to this group.';


--
-- Name: COLUMN user_restriction.user_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.user_restriction.user_id IS 'The ID of the [MSEA user](#shared.mseauser).';


--
-- Name: COLUMN user_restriction.restriction_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.user_restriction.restriction_id IS 'The ID of the [restriction](#shared.restriction).';


--
-- Name: COLUMN user_restriction.created_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.user_restriction.created_on IS 'The creation time of the record.';


--
-- Name: COLUMN user_restriction.updated_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.user_restriction.updated_on IS 'The last update time of the record.';


--
-- Name: mseauser_restriction_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.mseauser_restriction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.mseauser_restriction_id_seq OWNER TO msea_admin;

--
-- Name: mseauser_restriction_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.mseauser_restriction_id_seq OWNED BY shared.user_restriction.id;


--
-- Name: organisation; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.organisation (
    id integer NOT NULL,
    short_code character varying(16),
    name character varying(64) NOT NULL,
    country character varying(8) NOT NULL,
    note text
);


ALTER TABLE shared.organisation OWNER TO msea_admin;

--
-- Name: TABLE organisation; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.organisation IS 'Convenient storage for organisations involved in MSEA activities.';


--
-- Name: COLUMN organisation.short_code; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.organisation.short_code IS 'A short code for looking up the entity; usually the organisation''s acronym.';


--
-- Name: COLUMN organisation.name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.organisation.name IS 'The full name of the organisation.';


--
-- Name: COLUMN organisation.country; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.organisation.country IS 'The code for the country where the organisation is based (e.g., "CA" for Canada).';


--
-- Name: COLUMN organisation.note; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.organisation.note IS 'An optional note about the organisation.';


--
-- Name: organisation_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.organisation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.organisation_id_seq OWNER TO msea_admin;

--
-- Name: organisation_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.organisation_id_seq OWNED BY shared.organisation.id;


--
-- Name: person; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.person (
    id integer NOT NULL,
    biigle_user_id integer,
    first_name character varying(32) NOT NULL,
    last_name character varying(32) NOT NULL,
    email character varying(128),
    photo bytea,
    bio text,
    biigle_uuid character(36),
    affiliation character varying(512)
);


ALTER TABLE shared.person OWNER TO msea_admin;

--
-- Name: TABLE person; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.person IS 'Stores information about people involved in all aspects of the application. This is not intended to be a comprehensive duplicate of the individual''s HR dossier, just a means of representing a person and providing a means of associating it with a real-world individual, e.g., by their email.';


--
-- Name: COLUMN person.biigle_user_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.biigle_user_id IS 'The Biigle database ID of a user. The UUID should be used instead.';


--
-- Name: COLUMN person.first_name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.first_name IS 'First name.';


--
-- Name: COLUMN person.last_name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.last_name IS 'Last name.';


--
-- Name: COLUMN person.email; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.email IS 'Email address.';


--
-- Name: COLUMN person.photo; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.photo IS 'A photo of the person.';


--
-- Name: COLUMN person.bio; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.bio IS 'A brief biography of the person.';


--
-- Name: COLUMN person.biigle_uuid; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.biigle_uuid IS 'The Biigle UUID is used to identify the user in Biigle apps.';


--
-- Name: COLUMN person.affiliation; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.person.affiliation IS 'The organization with which this user is currently affiliated.';


--
-- Name: person_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.person_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.person_id_seq OWNER TO msea_admin;

--
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.person_id_seq OWNED BY shared.person.id;


--
-- Name: restriction; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.restriction (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    note text,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL,
    short_code character varying(32)
);


ALTER TABLE shared.restriction OWNER TO msea_admin;

--
-- Name: TABLE restriction; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.restriction IS 'Provides a list of restrictions that can be applied to individual observation rows, to restrict access to allowed roles.';


--
-- Name: COLUMN restriction.name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.restriction.name IS 'The name of the restriction.';


--
-- Name: COLUMN restriction.note; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.restriction.note IS 'An optional note about the restriction.';


--
-- Name: COLUMN restriction.created_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.restriction.created_on IS 'The creation time of the restriction.';


--
-- Name: COLUMN restriction.updated_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.restriction.updated_on IS 'The last update time of the restriction.';


--
-- Name: COLUMN restriction.short_code; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.restriction.short_code IS 'A short code used to reference the restriction.';


--
-- Name: restriction_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.restriction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.restriction_id_seq OWNER TO msea_admin;

--
-- Name: restriction_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.restriction_id_seq OWNED BY shared.restriction.id;


--
-- Name: site; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.site (
    id integer NOT NULL,
    spatial_library_id integer,
    name character varying(64) NOT NULL,
    note text
);


ALTER TABLE shared.site OWNER TO msea_admin;

--
-- Name: TABLE site; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.site IS 'Contains the names and geographic boundaries of study sites. This will be useful not only for mapping but for querying data by spatial extent.';


--
-- Name: COLUMN site.spatial_library_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.site.spatial_library_id IS 'A reference to the [spatial library](#shared.spatial_library) entry.';


--
-- Name: COLUMN site.name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.site.name IS 'A name for the site.';


--
-- Name: COLUMN site.note; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.site.note IS 'An optional note about the site.';


--
-- Name: site_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.site_id_seq OWNER TO msea_admin;

--
-- Name: site_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.site_id_seq OWNED BY shared.site.id;


--
-- Name: spatial_library; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.spatial_library (
    id integer NOT NULL,
    file_id integer,
    name character varying(64) NOT NULL,
    note text,
    thumbnail bytea,
    created_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_on timestamp(6) without time zone DEFAULT now() NOT NULL,
    geom public.geography,
    metadata jsonb,
    rast bytea
);


ALTER TABLE shared.spatial_library OWNER TO msea_admin;

--
-- Name: TABLE spatial_library; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.spatial_library IS 'Maintains a library of spatial data.';


--
-- Name: COLUMN spatial_library.file_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.file_id IS 'A reference to a related [file](#shared.file).';


--
-- Name: COLUMN spatial_library.name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.name IS 'A name for the entry.';


--
-- Name: COLUMN spatial_library.note; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.note IS 'A note about this library item.';


--
-- Name: COLUMN spatial_library.thumbnail; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.thumbnail IS 'A small thumbnail in binary (JPG) form.';


--
-- Name: COLUMN spatial_library.created_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.created_on IS 'The time when this record was created.';


--
-- Name: COLUMN spatial_library.updated_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.updated_on IS 'The date of update of this record.';


--
-- Name: COLUMN spatial_library.geom; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.geom IS 'A vector representation of the object(s), projected into WGS84 (lat/lon) and stored as a geography type.';


--
-- Name: COLUMN spatial_library.metadata; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.metadata IS 'A JSON object containing metadata related to the spatial object.';


--
-- Name: COLUMN spatial_library.rast; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library.rast IS 'A raster.';


--
-- Name: spatial_library_file; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.spatial_library_file (
    id integer NOT NULL,
    spatial_library_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE shared.spatial_library_file OWNER TO msea_admin;

--
-- Name: TABLE spatial_library_file; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.spatial_library_file IS 'Allows a [spatial library](#shared.spatial_library) entity to own more than one file.';


--
-- Name: COLUMN spatial_library_file.spatial_library_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library_file.spatial_library_id IS 'A link to the [spatial library](#shared.spatial_library).';


--
-- Name: COLUMN spatial_library_file.file_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.spatial_library_file.file_id IS 'A link to the [file](#shared.file).';


--
-- Name: spatial_library_file_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.spatial_library_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.spatial_library_file_id_seq OWNER TO msea_admin;

--
-- Name: spatial_library_file_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.spatial_library_file_id_seq OWNED BY shared.spatial_library_file.id;


--
-- Name: spatial_library_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.spatial_library_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.spatial_library_id_seq OWNER TO msea_admin;

--
-- Name: spatial_library_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.spatial_library_id_seq OWNED BY shared.spatial_library.id;


--
-- Name: taxon; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.taxon (
    id integer NOT NULL,
    aphia_id integer,
    inaturalist_id integer,
    original_label character varying(255) NOT NULL,
    label character varying(255),
    scientific_name character varying(128),
    common_name character varying(128),
    otu character varying(16),
    reviewed_by_id integer,
    reviewed_on timestamp(6) without time zone,
    review_note text,
    hart_code character varying(8)
);


ALTER TABLE shared.taxon OWNER TO msea_admin;

--
-- Name: TABLE taxon; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.taxon IS 'The taxon table stores information about the identification of observed specimens. A taxon record stores the name of the taxonomic unit (e.g., the species or genus name), or an open nomenclature (ON) identifier using *sp.* or *cf.*; an optional operational taxonomic unit (OTU), and a reference to any known taxa in the Hart, ITIS, iNaturalist or WoRMS databases. The original label stores the name as imported. The label, OTU and Hart code are updated at audit time from the original label and the scientific and common names are updated if available. The auditing process is necessary to produce reasonable data. Some of this work can be performed automatically. Multiple records in this table can refer to the same species or subspecies. It *may or may not be known whether the species referred to by a record are the same or different.* Some amount of auditing will probably be necessary on this table. The contents of this table are driven by observations; it is not populated from existing catalogues but from observations, and the authoritative entity IDs are populated as a post-processing step from existing catalogs. If a match in one of the authoritative catalogue is not found, the relation fields are left empty and the taxon should be audited, to correct spelling or assign a correct identifier.';


--
-- Name: COLUMN taxon.aphia_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.aphia_id IS 'A reference to the [WoRMs](https://www.marinespecies.org/) database. The actual Aphia ID used by WoRMs is used as the primary key in the [WoRMs taxon](#rov.worms_taxon) table.';


--
-- Name: COLUMN taxon.inaturalist_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.inaturalist_id IS 'A reference to the [iNaturalist](https://www.inaturalist.org/projects/marine-life-of-the-northeast-pacific) taxon. The iNaturalist taxon ID is used as the primary key in the [iNaturalist taxon](#rov.inaturalist_taxon) table.';


--
-- Name: COLUMN taxon.original_label; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.original_label IS 'The original label as entered by the annotator or observer. On review or audit, the label, hart_code and otu fields will be populated and references to outside databases updated. This field will not be edited.';


--
-- Name: COLUMN taxon.label; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.label IS 'The label given to the species at audit time. May be modified from the original label. May differ from the scientific or common names.';


--
-- Name: COLUMN taxon.scientific_name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.scientific_name IS 'The scientific (binomial, trinomial) name of the taxon. Updated at audit time, based on the label or other fields.';


--
-- Name: COLUMN taxon.common_name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.common_name IS 'The common name of the taxon.';


--
-- Name: COLUMN taxon.otu; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.otu IS 'The operational taxonomic unit is a unique identifier for this instances of an observation whose identification isn''t certain.';


--
-- Name: COLUMN taxon.reviewed_by_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.reviewed_by_id IS 'Relates to the [person](#rov.person) who reviewed the record.';


--
-- Name: COLUMN taxon.reviewed_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.reviewed_on IS 'Gives the time that the record was reviewed.';


--
-- Name: COLUMN taxon.review_note; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon.review_note IS 'Optional note about the review.';


--
-- Name: taxon_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.taxon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.taxon_id_seq OWNER TO msea_admin;

--
-- Name: taxon_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.taxon_id_seq OWNED BY shared.taxon.id;


--
-- Name: taxon_restriction; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.taxon_restriction (
    taxon_id integer NOT NULL,
    restriction_id integer NOT NULL,
    created_on timestamp without time zone DEFAULT now() NOT NULL,
    updated_on timestamp without time zone DEFAULT now() NOT NULL,
    id integer NOT NULL
);


ALTER TABLE shared.taxon_restriction OWNER TO msea_admin;

--
-- Name: TABLE taxon_restriction; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.taxon_restriction IS 'Links an [MSEA user](#shared.mseauser) to a [restriction](#shared.restriction). The user will have access to records related to this group.';


--
-- Name: COLUMN taxon_restriction.taxon_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon_restriction.taxon_id IS 'The ID of the [taxon](#shared.taxon).';


--
-- Name: COLUMN taxon_restriction.restriction_id; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon_restriction.restriction_id IS 'The ID of the [restriction](#shared.restriction).';


--
-- Name: COLUMN taxon_restriction.created_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon_restriction.created_on IS 'The creation time of the record.';


--
-- Name: COLUMN taxon_restriction.updated_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.taxon_restriction.updated_on IS 'The last update time of the record.';


--
-- Name: taxon_restriction_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.taxon_restriction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.taxon_restriction_id_seq OWNER TO msea_admin;

--
-- Name: taxon_restriction_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.taxon_restriction_id_seq OWNED BY shared.taxon_restriction.id;


--
-- Name: uploaded_file; Type: TABLE; Schema: shared; Owner: msea_admin
--

CREATE TABLE shared.uploaded_file (
    id integer NOT NULL,
    path character varying(256) NOT NULL,
    type character varying(256) NOT NULL,
    name character varying(256) NOT NULL,
    created_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    expires_on timestamp without time zone DEFAULT (CURRENT_TIMESTAMP + '01:00:00'::interval) NOT NULL,
    marked_for_delete boolean DEFAULT false NOT NULL
);


ALTER TABLE shared.uploaded_file OWNER TO msea_admin;

--
-- Name: TABLE uploaded_file; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON TABLE shared.uploaded_file IS 'A table to store uploaded files that can be associated with other entities in separate requests. This table is intended to store temporary entities: when an uploaded file has been handled, these records should be deleted.';


--
-- Name: COLUMN uploaded_file.path; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.uploaded_file.path IS 'The path to the temporary location of the file.';


--
-- Name: COLUMN uploaded_file.type; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.uploaded_file.type IS 'The guessed MIME type of the file.';


--
-- Name: COLUMN uploaded_file.name; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.uploaded_file.name IS 'The original name of the file.';


--
-- Name: COLUMN uploaded_file.created_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.uploaded_file.created_on IS 'The time of creation of the record.';


--
-- Name: COLUMN uploaded_file.expires_on; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.uploaded_file.expires_on IS 'The expiry time of the file. It may be deleted by a maintenance script after this time.';


--
-- Name: COLUMN uploaded_file.marked_for_delete; Type: COMMENT; Schema: shared; Owner: msea_admin
--

COMMENT ON COLUMN shared.uploaded_file.marked_for_delete IS 'If true, the file is marked for delete. Otherwise it will not be touched unless there is an age limit set in the daemon.';


--
-- Name: uploads_id_seq; Type: SEQUENCE; Schema: shared; Owner: msea_admin
--

CREATE SEQUENCE shared.uploads_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE shared.uploads_id_seq OWNER TO msea_admin;

--
-- Name: uploads_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: msea_admin
--

ALTER SEQUENCE shared.uploads_id_seq OWNED BY shared.uploaded_file.id;


--
-- Name: taxon; Type: TABLE; Schema: taxonomy; Owner: msea_admin
--

CREATE TABLE taxonomy.taxon (
    id integer NOT NULL,
    taxon_id character varying(8) NOT NULL,
    source character varying(32) NOT NULL,
    rank character varying(64),
    scientific_name character varying(256),
    common_name character varying(256),
    superdomain character varying(256),
    domain character varying(256),
    kingdom character varying(256),
    subkingdom character varying(256),
    infrakingdom character varying(256),
    phylum character varying(256),
    phylum_division character varying(256),
    subphylum_subdivision character varying(256),
    subphylum character varying(256),
    infraphylum character varying(256),
    parvphylum character varying(256),
    gigaclass character varying(256),
    megaclass character varying(256),
    superclass character varying(256),
    class character varying(256),
    subclass character varying(256),
    infraclass character varying(256),
    subterclass character varying(256),
    superorder character varying(256),
    "order" character varying(256),
    suborder character varying(256),
    infraorder character varying(256),
    parvorder character varying(256),
    superfamily character varying(256),
    family character varying(256),
    subfamily character varying(256),
    supertribe character varying(256),
    tribe character varying(256),
    subtribe character varying(256),
    genus character varying(256),
    genus_hybrid character varying(256),
    subgenus character varying(256),
    section character varying(256),
    subsection character varying(256),
    series character varying(256),
    species character varying(256),
    hybrid character varying(256),
    subspecies character varying(256),
    natio character varying(256),
    variety character varying(256),
    subvariety character varying(256),
    form character varying(256),
    subform character varying(256),
    no_common_name boolean DEFAULT false,
    accepted_taxon_id integer,
    parent_taxon_id integer,
    CONSTRAINT chk_scientific_common_name CHECK ((NOT ((common_name IS NULL) AND (scientific_name IS NULL))))
);


ALTER TABLE taxonomy.taxon OWNER TO msea_admin;

--
-- Name: TABLE taxon; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON TABLE taxonomy.taxon IS 'Stores taxonomic names from a variety of databases in a common format distinguished by source and taxon_id.';


--
-- Name: COLUMN taxon.taxon_id; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.taxon_id IS 'The taxonomic ID from the source database.';


--
-- Name: COLUMN taxon.source; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.source IS 'The name of the source database: "inaturalist", "worms", "obis", etc.';


--
-- Name: COLUMN taxon.rank; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.rank IS 'The name of the taxonomic rank of the record, e.g., "class", "subspecies", etc.';


--
-- Name: COLUMN taxon.scientific_name; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.scientific_name IS 'The scientific name of the species.';


--
-- Name: COLUMN taxon.common_name; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.common_name IS 'The common name of the species.';


--
-- Name: COLUMN taxon.superdomain; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.superdomain IS 'The superdomain name.';


--
-- Name: COLUMN taxon.domain; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.domain IS 'The domain name.';


--
-- Name: COLUMN taxon.kingdom; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.kingdom IS 'The kingdom name.';


--
-- Name: COLUMN taxon.subkingdom; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subkingdom IS 'The subkingdom name.';


--
-- Name: COLUMN taxon.infrakingdom; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.infrakingdom IS 'The infrakingdom name.';


--
-- Name: COLUMN taxon.phylum; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.phylum IS 'The phylum name.';


--
-- Name: COLUMN taxon.phylum_division; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.phylum_division IS 'The phylum division name.';


--
-- Name: COLUMN taxon.subphylum_subdivision; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subphylum_subdivision IS 'The subphylum division name.';


--
-- Name: COLUMN taxon.subphylum; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subphylum IS 'The subphylum name.';


--
-- Name: COLUMN taxon.infraphylum; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.infraphylum IS 'The infraphylum name.';


--
-- Name: COLUMN taxon.parvphylum; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.parvphylum IS 'The parvphylum name.';


--
-- Name: COLUMN taxon.gigaclass; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.gigaclass IS 'The gigaclass name.';


--
-- Name: COLUMN taxon.megaclass; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.megaclass IS 'The megaclass name.';


--
-- Name: COLUMN taxon.superclass; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.superclass IS 'The superclass name.';


--
-- Name: COLUMN taxon.class; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.class IS 'The "class" name.';


--
-- Name: COLUMN taxon.subclass; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subclass IS 'The subclass name.';


--
-- Name: COLUMN taxon.infraclass; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.infraclass IS 'The infraclass name.';


--
-- Name: COLUMN taxon.subterclass; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subterclass IS 'The subterclass name.';


--
-- Name: COLUMN taxon.superorder; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.superorder IS 'The superorder name.';


--
-- Name: COLUMN taxon."order"; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon."order" IS 'The "order" name.';


--
-- Name: COLUMN taxon.suborder; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.suborder IS 'The suborder name.';


--
-- Name: COLUMN taxon.infraorder; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.infraorder IS 'The infraorder name.';


--
-- Name: COLUMN taxon.parvorder; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.parvorder IS 'The parvorder name.';


--
-- Name: COLUMN taxon.superfamily; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.superfamily IS 'The superfamily name.';


--
-- Name: COLUMN taxon.family; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.family IS 'The family name.';


--
-- Name: COLUMN taxon.subfamily; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subfamily IS 'The subfamily name.';


--
-- Name: COLUMN taxon.supertribe; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.supertribe IS 'The supertribe name.';


--
-- Name: COLUMN taxon.tribe; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.tribe IS 'The tribe name.';


--
-- Name: COLUMN taxon.subtribe; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subtribe IS 'The subtribe name.';


--
-- Name: COLUMN taxon.genus; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.genus IS 'The genus name.';


--
-- Name: COLUMN taxon.genus_hybrid; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.genus_hybrid IS 'The genus hybrid name.';


--
-- Name: COLUMN taxon.subgenus; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subgenus IS 'The subgenus name.';


--
-- Name: COLUMN taxon.section; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.section IS 'The section name.';


--
-- Name: COLUMN taxon.subsection; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subsection IS 'The subsection name.';


--
-- Name: COLUMN taxon.series; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.series IS 'The series name.';


--
-- Name: COLUMN taxon.species; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.species IS 'The species name.';


--
-- Name: COLUMN taxon.hybrid; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.hybrid IS 'The hybrid name.';


--
-- Name: COLUMN taxon.subspecies; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subspecies IS 'The subspecies name.';


--
-- Name: COLUMN taxon.natio; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.natio IS 'The natio name.';


--
-- Name: COLUMN taxon.variety; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.variety IS 'The variety name.';


--
-- Name: COLUMN taxon.subvariety; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subvariety IS 'The subvariety name.';


--
-- Name: COLUMN taxon.form; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.form IS 'The form name.';


--
-- Name: COLUMN taxon.subform; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.subform IS 'The subform name.';


--
-- Name: COLUMN taxon.no_common_name; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.no_common_name IS 'If a search has been performed for a common name and none was found, this prevents the processor from trying again.';


--
-- Name: COLUMN taxon.accepted_taxon_id; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.accepted_taxon_id IS 'The taxon ID of the accepted taxon for this unaccepted taxon. If this column is null, the taxon is accepted.';


--
-- Name: COLUMN taxon.parent_taxon_id; Type: COMMENT; Schema: taxonomy; Owner: msea_admin
--

COMMENT ON COLUMN taxonomy.taxon.parent_taxon_id IS 'The taxon ID of the taxon''s parent taxon. If this column is null, the taxon has no parents.';


--
-- Name: taxon_id_seq; Type: SEQUENCE; Schema: taxonomy; Owner: msea_admin
--

CREATE SEQUENCE taxonomy.taxon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE taxonomy.taxon_id_seq OWNER TO msea_admin;

--
-- Name: taxon_id_seq; Type: SEQUENCE OWNED BY; Schema: taxonomy; Owner: msea_admin
--

ALTER SEQUENCE taxonomy.taxon_id_seq OWNED BY taxonomy.taxon.id;


--
-- Name: actor; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.actor (
    actor_id bigint NOT NULL,
    actor_user integer,
    actor_name text NOT NULL
);


ALTER TABLE wiki.actor OWNER TO msea_admin;

--
-- Name: actor_actor_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.actor_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.actor_actor_id_seq OWNER TO msea_admin;

--
-- Name: actor_actor_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.actor_actor_id_seq OWNED BY wiki.actor.actor_id;


--
-- Name: archive; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.archive (
    ar_id integer NOT NULL,
    ar_namespace integer DEFAULT 0 NOT NULL,
    ar_title text DEFAULT ''::text NOT NULL,
    ar_comment_id bigint NOT NULL,
    ar_actor bigint NOT NULL,
    ar_timestamp timestamp with time zone NOT NULL,
    ar_minor_edit smallint DEFAULT 0 NOT NULL,
    ar_rev_id integer NOT NULL,
    ar_deleted smallint DEFAULT 0 NOT NULL,
    ar_len integer,
    ar_page_id integer,
    ar_parent_id integer,
    ar_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.archive OWNER TO msea_admin;

--
-- Name: archive_ar_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.archive_ar_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.archive_ar_id_seq OWNER TO msea_admin;

--
-- Name: archive_ar_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.archive_ar_id_seq OWNED BY wiki.archive.ar_id;


--
-- Name: bot_passwords; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.bot_passwords (
    bp_user integer NOT NULL,
    bp_app_id text NOT NULL,
    bp_password text NOT NULL,
    bp_token text DEFAULT ''::text NOT NULL,
    bp_restrictions text NOT NULL,
    bp_grants text NOT NULL
);


ALTER TABLE wiki.bot_passwords OWNER TO msea_admin;

--
-- Name: category; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.category (
    cat_id integer NOT NULL,
    cat_title text NOT NULL,
    cat_pages integer DEFAULT 0 NOT NULL,
    cat_subcats integer DEFAULT 0 NOT NULL,
    cat_files integer DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.category OWNER TO msea_admin;

--
-- Name: category_cat_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.category_cat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.category_cat_id_seq OWNER TO msea_admin;

--
-- Name: category_cat_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.category_cat_id_seq OWNED BY wiki.category.cat_id;


--
-- Name: categorylinks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.categorylinks (
    cl_from integer DEFAULT 0 NOT NULL,
    cl_to text DEFAULT ''::text NOT NULL,
    cl_sortkey text DEFAULT ''::text NOT NULL,
    cl_sortkey_prefix text DEFAULT ''::text NOT NULL,
    cl_timestamp timestamp with time zone NOT NULL,
    cl_collation text DEFAULT ''::text NOT NULL,
    cl_type text DEFAULT 'page'::text NOT NULL
);


ALTER TABLE wiki.categorylinks OWNER TO msea_admin;

--
-- Name: change_tag; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.change_tag (
    ct_id integer NOT NULL,
    ct_rc_id integer,
    ct_log_id integer,
    ct_rev_id integer,
    ct_params text,
    ct_tag_id integer NOT NULL
);


ALTER TABLE wiki.change_tag OWNER TO msea_admin;

--
-- Name: change_tag_ct_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.change_tag_ct_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.change_tag_ct_id_seq OWNER TO msea_admin;

--
-- Name: change_tag_ct_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.change_tag_ct_id_seq OWNED BY wiki.change_tag.ct_id;


--
-- Name: change_tag_def; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.change_tag_def (
    ctd_id integer NOT NULL,
    ctd_name text NOT NULL,
    ctd_user_defined smallint NOT NULL,
    ctd_count bigint DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.change_tag_def OWNER TO msea_admin;

--
-- Name: change_tag_def_ctd_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.change_tag_def_ctd_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.change_tag_def_ctd_id_seq OWNER TO msea_admin;

--
-- Name: change_tag_def_ctd_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.change_tag_def_ctd_id_seq OWNED BY wiki.change_tag_def.ctd_id;


--
-- Name: comment; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.comment (
    comment_id bigint NOT NULL,
    comment_hash integer NOT NULL,
    comment_text text NOT NULL,
    comment_data text
);


ALTER TABLE wiki.comment OWNER TO msea_admin;

--
-- Name: comment_comment_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.comment_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.comment_comment_id_seq OWNER TO msea_admin;

--
-- Name: comment_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.comment_comment_id_seq OWNED BY wiki.comment.comment_id;


--
-- Name: content; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.content (
    content_id bigint NOT NULL,
    content_size integer NOT NULL,
    content_sha1 text NOT NULL,
    content_model smallint NOT NULL,
    content_address text NOT NULL
);


ALTER TABLE wiki.content OWNER TO msea_admin;

--
-- Name: content_content_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.content_content_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.content_content_id_seq OWNER TO msea_admin;

--
-- Name: content_content_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.content_content_id_seq OWNED BY wiki.content.content_id;


--
-- Name: content_models; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.content_models (
    model_id integer NOT NULL,
    model_name text NOT NULL
);


ALTER TABLE wiki.content_models OWNER TO msea_admin;

--
-- Name: content_models_model_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.content_models_model_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.content_models_model_id_seq OWNER TO msea_admin;

--
-- Name: content_models_model_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.content_models_model_id_seq OWNED BY wiki.content_models.model_id;


--
-- Name: externallinks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.externallinks (
    el_id integer NOT NULL,
    el_from integer DEFAULT 0 NOT NULL,
    el_to text NOT NULL,
    el_index text NOT NULL,
    el_index_60 text NOT NULL,
    el_to_domain_index text DEFAULT ''::text NOT NULL,
    el_to_path text
);


ALTER TABLE wiki.externallinks OWNER TO msea_admin;

--
-- Name: externallinks_el_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.externallinks_el_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.externallinks_el_id_seq OWNER TO msea_admin;

--
-- Name: externallinks_el_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.externallinks_el_id_seq OWNED BY wiki.externallinks.el_id;


--
-- Name: filearchive; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.filearchive (
    fa_id integer NOT NULL,
    fa_name text DEFAULT ''::text NOT NULL,
    fa_archive_name text DEFAULT ''::text,
    fa_storage_group text,
    fa_storage_key text DEFAULT ''::text,
    fa_deleted_user integer,
    fa_deleted_timestamp timestamp with time zone,
    fa_deleted_reason_id bigint NOT NULL,
    fa_size integer DEFAULT 0,
    fa_width integer DEFAULT 0,
    fa_height integer DEFAULT 0,
    fa_metadata text,
    fa_bits integer DEFAULT 0,
    fa_media_type text,
    fa_major_mime text DEFAULT 'unknown'::text,
    fa_minor_mime text DEFAULT 'unknown'::text,
    fa_description_id bigint NOT NULL,
    fa_actor bigint NOT NULL,
    fa_timestamp timestamp with time zone,
    fa_deleted smallint DEFAULT 0 NOT NULL,
    fa_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.filearchive OWNER TO msea_admin;

--
-- Name: filearchive_fa_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.filearchive_fa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.filearchive_fa_id_seq OWNER TO msea_admin;

--
-- Name: filearchive_fa_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.filearchive_fa_id_seq OWNED BY wiki.filearchive.fa_id;


--
-- Name: image; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.image (
    img_name text DEFAULT ''::text NOT NULL,
    img_size integer DEFAULT 0 NOT NULL,
    img_width integer DEFAULT 0 NOT NULL,
    img_height integer DEFAULT 0 NOT NULL,
    img_metadata text NOT NULL,
    img_bits integer DEFAULT 0 NOT NULL,
    img_media_type text,
    img_major_mime text DEFAULT 'unknown'::text NOT NULL,
    img_minor_mime text DEFAULT 'unknown'::text NOT NULL,
    img_description_id bigint NOT NULL,
    img_actor bigint NOT NULL,
    img_timestamp timestamp with time zone NOT NULL,
    img_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.image OWNER TO msea_admin;

--
-- Name: imagelinks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.imagelinks (
    il_from integer DEFAULT 0 NOT NULL,
    il_to text DEFAULT ''::text NOT NULL,
    il_from_namespace integer DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.imagelinks OWNER TO msea_admin;

--
-- Name: interwiki; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.interwiki (
    iw_prefix character varying(32) NOT NULL,
    iw_url text NOT NULL,
    iw_api text NOT NULL,
    iw_wikiid character varying(64) NOT NULL,
    iw_local smallint NOT NULL,
    iw_trans smallint DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.interwiki OWNER TO msea_admin;

--
-- Name: ip_changes; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.ip_changes (
    ipc_rev_id integer DEFAULT 0 NOT NULL,
    ipc_rev_timestamp timestamp with time zone NOT NULL,
    ipc_hex text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.ip_changes OWNER TO msea_admin;

--
-- Name: ipblocks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.ipblocks (
    ipb_id integer NOT NULL,
    ipb_address text NOT NULL,
    ipb_user integer DEFAULT 0 NOT NULL,
    ipb_by_actor bigint NOT NULL,
    ipb_reason_id bigint NOT NULL,
    ipb_timestamp timestamp with time zone NOT NULL,
    ipb_auto smallint DEFAULT 0 NOT NULL,
    ipb_anon_only smallint DEFAULT 0 NOT NULL,
    ipb_create_account smallint DEFAULT 1 NOT NULL,
    ipb_enable_autoblock smallint DEFAULT 1 NOT NULL,
    ipb_expiry timestamp with time zone NOT NULL,
    ipb_range_start text NOT NULL,
    ipb_range_end text NOT NULL,
    ipb_deleted smallint DEFAULT 0 NOT NULL,
    ipb_block_email smallint DEFAULT 0 NOT NULL,
    ipb_allow_usertalk smallint DEFAULT 0 NOT NULL,
    ipb_parent_block_id integer,
    ipb_sitewide smallint DEFAULT 1 NOT NULL
);


ALTER TABLE wiki.ipblocks OWNER TO msea_admin;

--
-- Name: ipblocks_ipb_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.ipblocks_ipb_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.ipblocks_ipb_id_seq OWNER TO msea_admin;

--
-- Name: ipblocks_ipb_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.ipblocks_ipb_id_seq OWNED BY wiki.ipblocks.ipb_id;


--
-- Name: ipblocks_restrictions; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.ipblocks_restrictions (
    ir_ipb_id integer NOT NULL,
    ir_type smallint NOT NULL,
    ir_value integer NOT NULL
);


ALTER TABLE wiki.ipblocks_restrictions OWNER TO msea_admin;

--
-- Name: iwlinks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.iwlinks (
    iwl_from integer DEFAULT 0 NOT NULL,
    iwl_prefix text DEFAULT ''::text NOT NULL,
    iwl_title text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.iwlinks OWNER TO msea_admin;

--
-- Name: job; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.job (
    job_id integer NOT NULL,
    job_cmd text DEFAULT ''::text NOT NULL,
    job_namespace integer NOT NULL,
    job_title text NOT NULL,
    job_timestamp timestamp with time zone,
    job_params text NOT NULL,
    job_random integer DEFAULT 0 NOT NULL,
    job_attempts integer DEFAULT 0 NOT NULL,
    job_token text DEFAULT ''::text NOT NULL,
    job_token_timestamp timestamp with time zone,
    job_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.job OWNER TO msea_admin;

--
-- Name: job_job_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.job_job_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.job_job_id_seq OWNER TO msea_admin;

--
-- Name: job_job_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.job_job_id_seq OWNED BY wiki.job.job_id;


--
-- Name: l10n_cache; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.l10n_cache (
    lc_lang text NOT NULL,
    lc_key character varying(255) NOT NULL,
    lc_value text NOT NULL
);


ALTER TABLE wiki.l10n_cache OWNER TO msea_admin;

--
-- Name: langlinks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.langlinks (
    ll_from integer DEFAULT 0 NOT NULL,
    ll_lang text DEFAULT ''::text NOT NULL,
    ll_title text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.langlinks OWNER TO msea_admin;

--
-- Name: linktarget; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.linktarget (
    lt_id bigint NOT NULL,
    lt_namespace integer NOT NULL,
    lt_title text NOT NULL
);


ALTER TABLE wiki.linktarget OWNER TO msea_admin;

--
-- Name: linktarget_lt_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.linktarget_lt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.linktarget_lt_id_seq OWNER TO msea_admin;

--
-- Name: linktarget_lt_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.linktarget_lt_id_seq OWNED BY wiki.linktarget.lt_id;


--
-- Name: log_search; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.log_search (
    ls_field text NOT NULL,
    ls_value character varying(255) NOT NULL,
    ls_log_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.log_search OWNER TO msea_admin;

--
-- Name: logging; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.logging (
    log_id integer NOT NULL,
    log_type text DEFAULT ''::text NOT NULL,
    log_action text DEFAULT ''::text NOT NULL,
    log_timestamp timestamp with time zone DEFAULT '1970-01-01 00:00:00+00'::timestamp with time zone NOT NULL,
    log_actor bigint NOT NULL,
    log_namespace integer DEFAULT 0 NOT NULL,
    log_title text DEFAULT ''::text NOT NULL,
    log_page integer,
    log_comment_id bigint NOT NULL,
    log_params text NOT NULL,
    log_deleted smallint DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.logging OWNER TO msea_admin;

--
-- Name: logging_log_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.logging_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.logging_log_id_seq OWNER TO msea_admin;

--
-- Name: logging_log_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.logging_log_id_seq OWNED BY wiki.logging.log_id;


--
-- Name: module_deps; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.module_deps (
    md_module text NOT NULL,
    md_skin text NOT NULL,
    md_deps text NOT NULL
);


ALTER TABLE wiki.module_deps OWNER TO msea_admin;

--
-- Name: objectcache; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.objectcache (
    keyname text DEFAULT ''::text NOT NULL,
    value text,
    exptime timestamp with time zone NOT NULL,
    modtoken character varying(17) DEFAULT '00000000000000000'::character varying NOT NULL,
    flags integer
);


ALTER TABLE wiki.objectcache OWNER TO msea_admin;

--
-- Name: oldimage; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.oldimage (
    oi_name text DEFAULT ''::text NOT NULL,
    oi_archive_name text DEFAULT ''::text NOT NULL,
    oi_size integer DEFAULT 0 NOT NULL,
    oi_width integer DEFAULT 0 NOT NULL,
    oi_height integer DEFAULT 0 NOT NULL,
    oi_bits integer DEFAULT 0 NOT NULL,
    oi_description_id bigint NOT NULL,
    oi_actor bigint NOT NULL,
    oi_timestamp timestamp with time zone NOT NULL,
    oi_metadata text NOT NULL,
    oi_media_type text,
    oi_major_mime text DEFAULT 'unknown'::text NOT NULL,
    oi_minor_mime text DEFAULT 'unknown'::text NOT NULL,
    oi_deleted smallint DEFAULT 0 NOT NULL,
    oi_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.oldimage OWNER TO msea_admin;

--
-- Name: page; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.page (
    page_id integer NOT NULL,
    page_namespace integer NOT NULL,
    page_title text NOT NULL,
    page_is_redirect smallint DEFAULT 0 NOT NULL,
    page_is_new smallint DEFAULT 0 NOT NULL,
    page_random double precision NOT NULL,
    page_touched timestamp with time zone NOT NULL,
    page_links_updated timestamp with time zone,
    page_latest integer NOT NULL,
    page_len integer NOT NULL,
    page_content_model text,
    page_lang text,
    titlevector tsvector
);


ALTER TABLE wiki.page OWNER TO msea_admin;

--
-- Name: page_page_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.page_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.page_page_id_seq OWNER TO msea_admin;

--
-- Name: page_page_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.page_page_id_seq OWNED BY wiki.page.page_id;


--
-- Name: page_props; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.page_props (
    pp_page integer NOT NULL,
    pp_propname text NOT NULL,
    pp_value text NOT NULL,
    pp_sortkey double precision
);


ALTER TABLE wiki.page_props OWNER TO msea_admin;

--
-- Name: page_restrictions; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.page_restrictions (
    pr_id integer NOT NULL,
    pr_page integer NOT NULL,
    pr_type text NOT NULL,
    pr_level text NOT NULL,
    pr_cascade smallint NOT NULL,
    pr_expiry timestamp with time zone
);


ALTER TABLE wiki.page_restrictions OWNER TO msea_admin;

--
-- Name: page_restrictions_pr_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.page_restrictions_pr_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.page_restrictions_pr_id_seq OWNER TO msea_admin;

--
-- Name: page_restrictions_pr_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.page_restrictions_pr_id_seq OWNED BY wiki.page_restrictions.pr_id;


--
-- Name: pagelinks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.pagelinks (
    pl_from integer DEFAULT 0 NOT NULL,
    pl_namespace integer DEFAULT 0 NOT NULL,
    pl_title text DEFAULT ''::text NOT NULL,
    pl_from_namespace integer DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.pagelinks OWNER TO msea_admin;

--
-- Name: protected_titles; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.protected_titles (
    pt_namespace integer NOT NULL,
    pt_title text NOT NULL,
    pt_user integer NOT NULL,
    pt_reason_id bigint NOT NULL,
    pt_timestamp timestamp with time zone NOT NULL,
    pt_expiry timestamp with time zone NOT NULL,
    pt_create_perm text NOT NULL
);


ALTER TABLE wiki.protected_titles OWNER TO msea_admin;

--
-- Name: querycache; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.querycache (
    qc_type text NOT NULL,
    qc_value integer DEFAULT 0 NOT NULL,
    qc_namespace integer DEFAULT 0 NOT NULL,
    qc_title text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.querycache OWNER TO msea_admin;

--
-- Name: querycache_info; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.querycache_info (
    qci_type text DEFAULT ''::text NOT NULL,
    qci_timestamp timestamp with time zone DEFAULT '1970-01-01 00:00:00+00'::timestamp with time zone NOT NULL
);


ALTER TABLE wiki.querycache_info OWNER TO msea_admin;

--
-- Name: querycachetwo; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.querycachetwo (
    qcc_type text NOT NULL,
    qcc_value integer DEFAULT 0 NOT NULL,
    qcc_namespace integer DEFAULT 0 NOT NULL,
    qcc_title text DEFAULT ''::text NOT NULL,
    qcc_namespacetwo integer DEFAULT 0 NOT NULL,
    qcc_titletwo text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.querycachetwo OWNER TO msea_admin;

--
-- Name: recentchanges; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.recentchanges (
    rc_id integer NOT NULL,
    rc_timestamp timestamp with time zone NOT NULL,
    rc_actor bigint NOT NULL,
    rc_namespace integer DEFAULT 0 NOT NULL,
    rc_title text DEFAULT ''::text NOT NULL,
    rc_comment_id bigint NOT NULL,
    rc_minor smallint DEFAULT 0 NOT NULL,
    rc_bot smallint DEFAULT 0 NOT NULL,
    rc_new smallint DEFAULT 0 NOT NULL,
    rc_cur_id integer DEFAULT 0 NOT NULL,
    rc_this_oldid integer DEFAULT 0 NOT NULL,
    rc_last_oldid integer DEFAULT 0 NOT NULL,
    rc_type smallint DEFAULT 0 NOT NULL,
    rc_source text DEFAULT ''::text NOT NULL,
    rc_patrolled smallint DEFAULT 0 NOT NULL,
    rc_ip text DEFAULT ''::text NOT NULL,
    rc_old_len integer,
    rc_new_len integer,
    rc_deleted smallint DEFAULT 0 NOT NULL,
    rc_logid integer DEFAULT 0 NOT NULL,
    rc_log_type text,
    rc_log_action text,
    rc_params text
);


ALTER TABLE wiki.recentchanges OWNER TO msea_admin;

--
-- Name: recentchanges_rc_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.recentchanges_rc_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.recentchanges_rc_id_seq OWNER TO msea_admin;

--
-- Name: recentchanges_rc_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.recentchanges_rc_id_seq OWNED BY wiki.recentchanges.rc_id;


--
-- Name: redirect; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.redirect (
    rd_from integer DEFAULT 0 NOT NULL,
    rd_namespace integer DEFAULT 0 NOT NULL,
    rd_title text DEFAULT ''::text NOT NULL,
    rd_interwiki character varying(32) DEFAULT NULL::character varying,
    rd_fragment text
);


ALTER TABLE wiki.redirect OWNER TO msea_admin;

--
-- Name: revision; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.revision (
    rev_id integer NOT NULL,
    rev_page integer NOT NULL,
    rev_comment_id bigint DEFAULT 0 NOT NULL,
    rev_actor bigint DEFAULT 0 NOT NULL,
    rev_timestamp timestamp with time zone NOT NULL,
    rev_minor_edit smallint DEFAULT 0 NOT NULL,
    rev_deleted smallint DEFAULT 0 NOT NULL,
    rev_len integer,
    rev_parent_id integer,
    rev_sha1 text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.revision OWNER TO msea_admin;

--
-- Name: revision_comment_temp; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.revision_comment_temp (
    revcomment_rev integer NOT NULL,
    revcomment_comment_id bigint NOT NULL
);


ALTER TABLE wiki.revision_comment_temp OWNER TO msea_admin;

--
-- Name: revision_rev_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.revision_rev_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.revision_rev_id_seq OWNER TO msea_admin;

--
-- Name: revision_rev_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.revision_rev_id_seq OWNED BY wiki.revision.rev_id;


--
-- Name: searchindex; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.searchindex (
    si_page integer NOT NULL,
    si_title character varying(255) DEFAULT ''::character varying NOT NULL,
    si_text text NOT NULL
);


ALTER TABLE wiki.searchindex OWNER TO msea_admin;

--
-- Name: site_identifiers; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.site_identifiers (
    si_type text NOT NULL,
    si_key text NOT NULL,
    si_site integer NOT NULL
);


ALTER TABLE wiki.site_identifiers OWNER TO msea_admin;

--
-- Name: site_stats; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.site_stats (
    ss_row_id integer NOT NULL,
    ss_total_edits bigint,
    ss_good_articles bigint,
    ss_total_pages bigint,
    ss_users bigint,
    ss_active_users bigint,
    ss_images bigint
);


ALTER TABLE wiki.site_stats OWNER TO msea_admin;

--
-- Name: sites; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.sites (
    site_id integer NOT NULL,
    site_global_key text NOT NULL,
    site_type text NOT NULL,
    site_group text NOT NULL,
    site_source text NOT NULL,
    site_language text NOT NULL,
    site_protocol text NOT NULL,
    site_domain character varying(255) NOT NULL,
    site_data text NOT NULL,
    site_forward smallint NOT NULL,
    site_config text NOT NULL
);


ALTER TABLE wiki.sites OWNER TO msea_admin;

--
-- Name: sites_site_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.sites_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.sites_site_id_seq OWNER TO msea_admin;

--
-- Name: sites_site_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.sites_site_id_seq OWNED BY wiki.sites.site_id;


--
-- Name: slot_roles; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.slot_roles (
    role_id integer NOT NULL,
    role_name text NOT NULL
);


ALTER TABLE wiki.slot_roles OWNER TO msea_admin;

--
-- Name: slot_roles_role_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.slot_roles_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.slot_roles_role_id_seq OWNER TO msea_admin;

--
-- Name: slot_roles_role_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.slot_roles_role_id_seq OWNED BY wiki.slot_roles.role_id;


--
-- Name: slots; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.slots (
    slot_revision_id bigint NOT NULL,
    slot_role_id smallint NOT NULL,
    slot_content_id bigint NOT NULL,
    slot_origin bigint NOT NULL
);


ALTER TABLE wiki.slots OWNER TO msea_admin;

--
-- Name: templatelinks; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.templatelinks (
    tl_from integer DEFAULT 0 NOT NULL,
    tl_target_id bigint NOT NULL,
    tl_from_namespace integer DEFAULT 0 NOT NULL
);


ALTER TABLE wiki.templatelinks OWNER TO msea_admin;

--
-- Name: text; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.text (
    old_id integer NOT NULL,
    old_text text NOT NULL,
    old_flags text NOT NULL,
    textvector tsvector
);


ALTER TABLE wiki.text OWNER TO msea_admin;

--
-- Name: text_old_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.text_old_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.text_old_id_seq OWNER TO msea_admin;

--
-- Name: text_old_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.text_old_id_seq OWNED BY wiki.text.old_id;


--
-- Name: updatelog; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.updatelog (
    ul_key character varying(255) NOT NULL,
    ul_value text
);


ALTER TABLE wiki.updatelog OWNER TO msea_admin;

--
-- Name: uploadstash; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.uploadstash (
    us_id integer NOT NULL,
    us_user integer NOT NULL,
    us_key character varying(255) NOT NULL,
    us_orig_path character varying(255) NOT NULL,
    us_path character varying(255) NOT NULL,
    us_source_type character varying(50) DEFAULT NULL::character varying,
    us_timestamp timestamp with time zone NOT NULL,
    us_status character varying(50) NOT NULL,
    us_chunk_inx integer,
    us_props text,
    us_size integer NOT NULL,
    us_sha1 character varying(31) NOT NULL,
    us_mime character varying(255) DEFAULT NULL::character varying,
    us_media_type wiki.us_media_type_enum,
    us_image_width integer,
    us_image_height integer,
    us_image_bits smallint
);


ALTER TABLE wiki.uploadstash OWNER TO msea_admin;

--
-- Name: uploadstash_us_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.uploadstash_us_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.uploadstash_us_id_seq OWNER TO msea_admin;

--
-- Name: uploadstash_us_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.uploadstash_us_id_seq OWNED BY wiki.uploadstash.us_id;


--
-- Name: user; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki."user" (
    user_id integer NOT NULL,
    user_name text DEFAULT ''::text NOT NULL,
    user_real_name text DEFAULT ''::text NOT NULL,
    user_password text NOT NULL,
    user_newpassword text NOT NULL,
    user_newpass_time timestamp with time zone,
    user_email text NOT NULL,
    user_touched timestamp with time zone NOT NULL,
    user_token text DEFAULT ''::text NOT NULL,
    user_email_authenticated timestamp with time zone,
    user_email_token text,
    user_email_token_expires timestamp with time zone,
    user_registration timestamp with time zone,
    user_editcount integer,
    user_password_expires timestamp with time zone
);


ALTER TABLE wiki."user" OWNER TO msea_admin;

--
-- Name: user_autocreate_serial; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.user_autocreate_serial (
    uas_shard integer NOT NULL,
    uas_value integer NOT NULL
);


ALTER TABLE wiki.user_autocreate_serial OWNER TO msea_admin;

--
-- Name: user_former_groups; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.user_former_groups (
    ufg_user integer DEFAULT 0 NOT NULL,
    ufg_group text DEFAULT ''::text NOT NULL
);


ALTER TABLE wiki.user_former_groups OWNER TO msea_admin;

--
-- Name: user_groups; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.user_groups (
    ug_user integer DEFAULT 0 NOT NULL,
    ug_group text DEFAULT ''::text NOT NULL,
    ug_expiry timestamp with time zone
);


ALTER TABLE wiki.user_groups OWNER TO msea_admin;

--
-- Name: user_newtalk; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.user_newtalk (
    user_id integer DEFAULT 0 NOT NULL,
    user_ip text DEFAULT ''::text NOT NULL,
    user_last_timestamp timestamp with time zone
);


ALTER TABLE wiki.user_newtalk OWNER TO msea_admin;

--
-- Name: user_properties; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.user_properties (
    up_user integer NOT NULL,
    up_property text NOT NULL,
    up_value text
);


ALTER TABLE wiki.user_properties OWNER TO msea_admin;

--
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.user_user_id_seq OWNER TO msea_admin;

--
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.user_user_id_seq OWNED BY wiki."user".user_id;


--
-- Name: watchlist; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.watchlist (
    wl_id integer NOT NULL,
    wl_user integer NOT NULL,
    wl_namespace integer DEFAULT 0 NOT NULL,
    wl_title text DEFAULT ''::text NOT NULL,
    wl_notificationtimestamp timestamp with time zone
);


ALTER TABLE wiki.watchlist OWNER TO msea_admin;

--
-- Name: watchlist_expiry; Type: TABLE; Schema: wiki; Owner: msea_admin
--

CREATE TABLE wiki.watchlist_expiry (
    we_item integer NOT NULL,
    we_expiry timestamp with time zone NOT NULL
);


ALTER TABLE wiki.watchlist_expiry OWNER TO msea_admin;

--
-- Name: watchlist_wl_id_seq; Type: SEQUENCE; Schema: wiki; Owner: msea_admin
--

CREATE SEQUENCE wiki.watchlist_wl_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE wiki.watchlist_wl_id_seq OWNER TO msea_admin;

--
-- Name: watchlist_wl_id_seq; Type: SEQUENCE OWNED BY; Schema: wiki; Owner: msea_admin
--

ALTER SEQUENCE wiki.watchlist_wl_id_seq OWNED BY wiki.watchlist.wl_id;


--
-- Name: event_log_data id; Type: DEFAULT; Schema: iform; Owner: msea_admin
--

ALTER TABLE ONLY iform.event_log_data ALTER COLUMN id SET DEFAULT nextval('iform.event_log_data_id_seq'::regclass);


--
-- Name: quadrat id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat ALTER COLUMN id SET DEFAULT nextval('intertidal.quadrat_id_seq'::regclass);


--
-- Name: quadrat_observation id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_observation ALTER COLUMN id SET DEFAULT nextval('intertidal.quadrat_observation_id_seq'::regclass);


--
-- Name: quadrat_substrate id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_substrate ALTER COLUMN id SET DEFAULT nextval('intertidal.quadrat_substrate_id_seq'::regclass);


--
-- Name: quadrat_substrate_coverage id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_substrate_coverage ALTER COLUMN id SET DEFAULT nextval('intertidal.quadrat_substrate_coverage_id_seq'::regclass);


--
-- Name: survey id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey ALTER COLUMN id SET DEFAULT nextval('intertidal.survey_id_seq'::regclass);


--
-- Name: survey_crew id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_crew ALTER COLUMN id SET DEFAULT nextval('intertidal.survey_crew_id_seq'::regclass);


--
-- Name: survey_role id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_role ALTER COLUMN id SET DEFAULT nextval('intertidal.survey_role_id_seq'::regclass);


--
-- Name: survey_taxon id; Type: DEFAULT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_taxon ALTER COLUMN id SET DEFAULT nextval('intertidal.survey_taxon_id_seq'::regclass);


--
-- Name: cruise id; Type: DEFAULT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.cruise ALTER COLUMN id SET DEFAULT nextval('ndst.cruise_id_seq'::regclass);


--
-- Name: diveconfig id; Type: DEFAULT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.diveconfig ALTER COLUMN id SET DEFAULT nextval('ndst.diveconfig_id_seq'::regclass);


--
-- Name: dives id; Type: DEFAULT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.dives ALTER COLUMN id SET DEFAULT nextval('ndst.dives_id_seq'::regclass);


--
-- Name: equipconfig id; Type: DEFAULT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.equipconfig ALTER COLUMN id SET DEFAULT nextval('ndst.equipconfig_id_seq'::regclass);


--
-- Name: equipment id; Type: DEFAULT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.equipment ALTER COLUMN id SET DEFAULT nextval('ndst.equipment_id_seq'::regclass);


--
-- Name: people id; Type: DEFAULT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.people ALTER COLUMN id SET DEFAULT nextval('ndst.people_id_seq'::regclass);


--
-- Name: transects id; Type: DEFAULT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.transects ALTER COLUMN id SET DEFAULT nextval('ndst.transects_id_seq'::regclass);


--
-- Name: mpa ogc_fid; Type: DEFAULT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa ALTER COLUMN ogc_fid SET DEFAULT nextval('pa.mpas_ogc_fid_seq'::regclass);


--
-- Name: mpa_data_object id; Type: DEFAULT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa_data_object ALTER COLUMN id SET DEFAULT nextval('pa.mpa_data_object_id_seq'::regclass);


--
-- Name: mpa_data_object_file id; Type: DEFAULT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa_data_object_file ALTER COLUMN id SET DEFAULT nextval('pa.mpa_data_object_file_id_seq'::regclass);


--
-- Name: protected_area id; Type: DEFAULT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area ALTER COLUMN id SET DEFAULT nextval('pa.protected_area_id_seq'::regclass);


--
-- Name: protected_area_data_object id; Type: DEFAULT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area_data_object ALTER COLUMN id SET DEFAULT nextval('pa.data_object_id_seq'::regclass);


--
-- Name: rca id; Type: DEFAULT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.rca ALTER COLUMN id SET DEFAULT nextval('pa.rca_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: nyt_notification id; Type: DEFAULT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_notification ALTER COLUMN id SET DEFAULT nextval('public.nyt_notification_id_seq'::regclass);


--
-- Name: nyt_settings id; Type: DEFAULT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_settings ALTER COLUMN id SET DEFAULT nextval('public.nyt_settings_id_seq'::regclass);


--
-- Name: nyt_subscription id; Type: DEFAULT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_subscription ALTER COLUMN id SET DEFAULT nextval('public.nyt_subscription_id_seq'::regclass);


--
-- Name: abundance id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.abundance ALTER COLUMN id SET DEFAULT nextval('rov.abundance_id_seq'::regclass);


--
-- Name: annotation_job id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job ALTER COLUMN id SET DEFAULT nextval('rov.annotation_job_id_seq'::regclass);


--
-- Name: annotation_job_annotation_protocol id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_annotation_protocol ALTER COLUMN id SET DEFAULT nextval('rov.annotation_job_annotation_protocol_id_seq'::regclass);


--
-- Name: annotation_job_crew id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_crew ALTER COLUMN id SET DEFAULT nextval('rov.annotation_job_crew_id_seq'::regclass);


--
-- Name: annotation_job_role id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_role ALTER COLUMN id SET DEFAULT nextval('rov.annotation_job_role_id_seq'::regclass);


--
-- Name: annotation_protocol id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol ALTER COLUMN id SET DEFAULT nextval('rov.annotation_protocol_id_seq'::regclass);


--
-- Name: annotation_protocol_document id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol_document ALTER COLUMN id SET DEFAULT nextval('rov.annotation_protocol_document_id_seq'::regclass);


--
-- Name: annotation_software id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_software ALTER COLUMN id SET DEFAULT nextval('rov.annotation_software_id_seq'::regclass);


--
-- Name: biocover id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.biocover ALTER COLUMN id SET DEFAULT nextval('rov.biocover_id_seq'::regclass);


--
-- Name: complexity id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.complexity ALTER COLUMN id SET DEFAULT nextval('rov.complexity_id_seq'::regclass);


--
-- Name: coverage id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.coverage ALTER COLUMN id SET DEFAULT nextval('rov.coverage_id_seq'::regclass);


--
-- Name: cruise id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise ALTER COLUMN id SET DEFAULT nextval('rov.cruise_leg_id_seq'::regclass);


--
-- Name: cruise_crew id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_crew ALTER COLUMN id SET DEFAULT nextval('rov.cruise_leg_crew_id_seq'::regclass);


--
-- Name: cruise_document id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_document ALTER COLUMN id SET DEFAULT nextval('rov.cruise_document_id_seq'::regclass);


--
-- Name: cruise_fn_contact id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_fn_contact ALTER COLUMN id SET DEFAULT nextval('rov.cruise_fn_contact_id_seq'::regclass);


--
-- Name: cruise_import id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_import ALTER COLUMN id SET DEFAULT nextval('rov.cruise_import_id_seq'::regclass);


--
-- Name: cruise_library id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_library ALTER COLUMN id SET DEFAULT nextval('rov.cruise_leg_library_id_seq'::regclass);


--
-- Name: cruise_program id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_program ALTER COLUMN id SET DEFAULT nextval('rov.cruise_program_id_seq'::regclass);


--
-- Name: cruise_role id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_role ALTER COLUMN id SET DEFAULT nextval('rov.cruise_leg_role_id_seq'::regclass);


--
-- Name: disturbance id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.disturbance ALTER COLUMN id SET DEFAULT nextval('rov.disturbance_id_seq'::regclass);


--
-- Name: dive id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive ALTER COLUMN id SET DEFAULT nextval('rov.dive_id_seq'::regclass);


--
-- Name: dive_crew id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_crew ALTER COLUMN id SET DEFAULT nextval('rov.dive_crew_id_seq'::regclass);


--
-- Name: dive_role id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_role ALTER COLUMN id SET DEFAULT nextval('rov.dive_role_id_seq'::regclass);


--
-- Name: equipment_type id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.equipment_type ALTER COLUMN id SET DEFAULT nextval('rov.equipment_type_id_seq'::regclass);


--
-- Name: event id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event ALTER COLUMN id SET DEFAULT nextval('rov.event_id_seq'::regclass);


--
-- Name: event_logger id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event_logger ALTER COLUMN id SET DEFAULT nextval('rov.event_logger_id_seq'::regclass);


--
-- Name: flow id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.flow ALTER COLUMN id SET DEFAULT nextval('rov.flow_id_seq'::regclass);


--
-- Name: image_quality id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.image_quality ALTER COLUMN id SET DEFAULT nextval('rov.image_quality_id_seq'::regclass);


--
-- Name: import_queue_annotator id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_annotator ALTER COLUMN id SET DEFAULT nextval('rov.import_queue_annotation_id_seq'::regclass);


--
-- Name: import_queue_annotator_label_map_prefill id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_annotator_label_map_prefill ALTER COLUMN id SET DEFAULT nextval('rov.import_queue_annotator_label_map_prefill_id_seq'::regclass);


--
-- Name: import_queue_pi id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_pi ALTER COLUMN id SET DEFAULT nextval('rov.import_queue_pi_id_seq'::regclass);


--
-- Name: instrument id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument ALTER COLUMN id SET DEFAULT nextval('rov.instrument_id_seq'::regclass);


--
-- Name: instrument_config id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument_config ALTER COLUMN id SET DEFAULT nextval('rov.instrument_config_id_seq'::regclass);


--
-- Name: measurement id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement ALTER COLUMN id SET DEFAULT nextval('rov.measurement_id_seq'::regclass);


--
-- Name: measurement_type id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement_type ALTER COLUMN id SET DEFAULT nextval('rov.measurement_type_id_seq'::regclass);


--
-- Name: medium_format id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.medium_format ALTER COLUMN id SET DEFAULT nextval('rov.medium_format_id_seq'::regclass);


--
-- Name: medium_type id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.medium_type ALTER COLUMN id SET DEFAULT nextval('rov.medium_type_id_seq'::regclass);


--
-- Name: model id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model ALTER COLUMN id SET DEFAULT nextval('rov.model_id_seq'::regclass);


--
-- Name: model_documentation id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_documentation ALTER COLUMN id SET DEFAULT nextval('rov.model_documentation_id_seq'::regclass);


--
-- Name: model_equipment_type id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_equipment_type ALTER COLUMN id SET DEFAULT nextval('rov.model_equipment_type_id_seq'::regclass);


--
-- Name: observation_confidence id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_confidence ALTER COLUMN id SET DEFAULT nextval('rov.observation_confidence_id_seq'::regclass);


--
-- Name: orientation id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.orientation ALTER COLUMN id SET DEFAULT nextval('rov.orientation_id_seq'::regclass);


--
-- Name: orientation_type id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.orientation_type ALTER COLUMN id SET DEFAULT nextval('rov.orientation_type_id_seq'::regclass);


--
-- Name: platform id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform ALTER COLUMN id SET DEFAULT nextval('rov.platform_id_seq'::regclass);


--
-- Name: platform_config id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform_config ALTER COLUMN id SET DEFAULT nextval('rov.platform_config_id_seq'::regclass);


--
-- Name: position id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov."position" ALTER COLUMN id SET DEFAULT nextval('rov.position_id_seq'::regclass);


--
-- Name: position_type id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.position_type ALTER COLUMN id SET DEFAULT nextval('rov.position_type_id_seq'::regclass);


--
-- Name: program id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program ALTER COLUMN id SET DEFAULT nextval('rov.program_id_seq'::regclass);


--
-- Name: program_library id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_library ALTER COLUMN id SET DEFAULT nextval('rov.program_library_id_seq'::regclass);


--
-- Name: program_member id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_member ALTER COLUMN id SET DEFAULT nextval('rov.program_member_id_seq'::regclass);


--
-- Name: program_role id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_role ALTER COLUMN id SET DEFAULT nextval('rov.program_role_id_seq'::regclass);


--
-- Name: protocol id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.protocol ALTER COLUMN id SET DEFAULT nextval('rov.protocol_id_seq'::regclass);


--
-- Name: relief id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.relief ALTER COLUMN id SET DEFAULT nextval('rov.relief_id_seq'::regclass);


--
-- Name: signup_area id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.signup_area ALTER COLUMN id SET DEFAULT nextval('rov.signup_area_id_seq'::regclass);


--
-- Name: status_type id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_type ALTER COLUMN id SET DEFAULT nextval('rov.status_type_id_seq'::regclass);


--
-- Name: status_type_detail id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_type_detail ALTER COLUMN id SET DEFAULT nextval('rov.status_type_detail_id_seq'::regclass);


--
-- Name: substrate id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.substrate ALTER COLUMN id SET DEFAULT nextval('rov.substrate_id_seq'::regclass);


--
-- Name: survey_mode id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.survey_mode ALTER COLUMN id SET DEFAULT nextval('rov.survey_mode_id_seq'::regclass);


--
-- Name: thickness id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.thickness ALTER COLUMN id SET DEFAULT nextval('rov.thickness_id_seq'::regclass);


--
-- Name: transect id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.transect ALTER COLUMN id SET DEFAULT nextval('rov.transect_id_seq'::regclass);


--
-- Name: weather_observation id; Type: DEFAULT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.weather_observation ALTER COLUMN id SET DEFAULT nextval('rov.weather_observation_id_seq'::regclass);


--
-- Name: db_version id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.db_version ALTER COLUMN id SET DEFAULT nextval('shared.db_version_id_seq'::regclass);


--
-- Name: file id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.file ALTER COLUMN id SET DEFAULT nextval('shared.file_id_seq'::regclass);


--
-- Name: file_type id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.file_type ALTER COLUMN id SET DEFAULT nextval('shared.file_type_id_seq'::regclass);


--
-- Name: library id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.library ALTER COLUMN id SET DEFAULT nextval('shared.library_id_seq'::regclass);


--
-- Name: mseauser id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.mseauser ALTER COLUMN id SET DEFAULT nextval('shared.mseauser_id_seq'::regclass);


--
-- Name: organisation id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.organisation ALTER COLUMN id SET DEFAULT nextval('shared.organisation_id_seq'::regclass);


--
-- Name: person id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.person ALTER COLUMN id SET DEFAULT nextval('shared.person_id_seq'::regclass);


--
-- Name: restriction id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.restriction ALTER COLUMN id SET DEFAULT nextval('shared.restriction_id_seq'::regclass);


--
-- Name: site id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.site ALTER COLUMN id SET DEFAULT nextval('shared.site_id_seq'::regclass);


--
-- Name: spatial_library id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.spatial_library ALTER COLUMN id SET DEFAULT nextval('shared.spatial_library_id_seq'::regclass);


--
-- Name: spatial_library_file id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.spatial_library_file ALTER COLUMN id SET DEFAULT nextval('shared.spatial_library_file_id_seq'::regclass);


--
-- Name: taxon id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.taxon ALTER COLUMN id SET DEFAULT nextval('shared.taxon_id_seq'::regclass);


--
-- Name: taxon_restriction id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.taxon_restriction ALTER COLUMN id SET DEFAULT nextval('shared.taxon_restriction_id_seq'::regclass);


--
-- Name: uploaded_file id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.uploaded_file ALTER COLUMN id SET DEFAULT nextval('shared.uploads_id_seq'::regclass);


--
-- Name: user_restriction id; Type: DEFAULT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.user_restriction ALTER COLUMN id SET DEFAULT nextval('shared.mseauser_restriction_id_seq'::regclass);


--
-- Name: taxon id; Type: DEFAULT; Schema: taxonomy; Owner: msea_admin
--

ALTER TABLE ONLY taxonomy.taxon ALTER COLUMN id SET DEFAULT nextval('taxonomy.taxon_id_seq'::regclass);


--
-- Name: actor actor_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.actor ALTER COLUMN actor_id SET DEFAULT nextval('wiki.actor_actor_id_seq'::regclass);


--
-- Name: archive ar_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.archive ALTER COLUMN ar_id SET DEFAULT nextval('wiki.archive_ar_id_seq'::regclass);


--
-- Name: category cat_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.category ALTER COLUMN cat_id SET DEFAULT nextval('wiki.category_cat_id_seq'::regclass);


--
-- Name: change_tag ct_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.change_tag ALTER COLUMN ct_id SET DEFAULT nextval('wiki.change_tag_ct_id_seq'::regclass);


--
-- Name: change_tag_def ctd_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.change_tag_def ALTER COLUMN ctd_id SET DEFAULT nextval('wiki.change_tag_def_ctd_id_seq'::regclass);


--
-- Name: comment comment_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.comment ALTER COLUMN comment_id SET DEFAULT nextval('wiki.comment_comment_id_seq'::regclass);


--
-- Name: content content_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.content ALTER COLUMN content_id SET DEFAULT nextval('wiki.content_content_id_seq'::regclass);


--
-- Name: content_models model_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.content_models ALTER COLUMN model_id SET DEFAULT nextval('wiki.content_models_model_id_seq'::regclass);


--
-- Name: externallinks el_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.externallinks ALTER COLUMN el_id SET DEFAULT nextval('wiki.externallinks_el_id_seq'::regclass);


--
-- Name: filearchive fa_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.filearchive ALTER COLUMN fa_id SET DEFAULT nextval('wiki.filearchive_fa_id_seq'::regclass);


--
-- Name: ipblocks ipb_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.ipblocks ALTER COLUMN ipb_id SET DEFAULT nextval('wiki.ipblocks_ipb_id_seq'::regclass);


--
-- Name: job job_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.job ALTER COLUMN job_id SET DEFAULT nextval('wiki.job_job_id_seq'::regclass);


--
-- Name: linktarget lt_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.linktarget ALTER COLUMN lt_id SET DEFAULT nextval('wiki.linktarget_lt_id_seq'::regclass);


--
-- Name: logging log_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.logging ALTER COLUMN log_id SET DEFAULT nextval('wiki.logging_log_id_seq'::regclass);


--
-- Name: page page_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.page ALTER COLUMN page_id SET DEFAULT nextval('wiki.page_page_id_seq'::regclass);


--
-- Name: page_restrictions pr_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.page_restrictions ALTER COLUMN pr_id SET DEFAULT nextval('wiki.page_restrictions_pr_id_seq'::regclass);


--
-- Name: recentchanges rc_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.recentchanges ALTER COLUMN rc_id SET DEFAULT nextval('wiki.recentchanges_rc_id_seq'::regclass);


--
-- Name: revision rev_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.revision ALTER COLUMN rev_id SET DEFAULT nextval('wiki.revision_rev_id_seq'::regclass);


--
-- Name: sites site_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.sites ALTER COLUMN site_id SET DEFAULT nextval('wiki.sites_site_id_seq'::regclass);


--
-- Name: slot_roles role_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.slot_roles ALTER COLUMN role_id SET DEFAULT nextval('wiki.slot_roles_role_id_seq'::regclass);


--
-- Name: text old_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.text ALTER COLUMN old_id SET DEFAULT nextval('wiki.text_old_id_seq'::regclass);


--
-- Name: uploadstash us_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.uploadstash ALTER COLUMN us_id SET DEFAULT nextval('wiki.uploadstash_us_id_seq'::regclass);


--
-- Name: user user_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki."user" ALTER COLUMN user_id SET DEFAULT nextval('wiki.user_user_id_seq'::regclass);


--
-- Name: watchlist wl_id; Type: DEFAULT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.watchlist ALTER COLUMN wl_id SET DEFAULT nextval('wiki.watchlist_wl_id_seq'::regclass);


--
-- Name: cruise cruise_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise
    ADD CONSTRAINT cruise_pkey PRIMARY KEY (id);


--
-- Name: cruise_track; Type: MATERIALIZED VIEW; Schema: rov; Owner: msea_admin
--

CREATE MATERIALIZED VIEW rov.cruise_track AS
 SELECT a.id AS cruise_id,
    concat(a.name, '-', a.leg) AS cruise_name,
    f.instrument_config_id,
    public.st_makeline((f.geom)::public.geometry) AS geom,
    concat('#', "substring"(md5((a.id)::text), 1, 6)) AS colour
   FROM ((((rov.cruise a
     JOIN rov.dive b ON ((b.cruise_id = a.id)))
     JOIN rov.platform_config d ON ((d.id = b.ship_config_id)))
     JOIN rov.instrument_config e ON ((e.platform_config_id = d.id)))
     JOIN ( SELECT "position".id,
            "position".position_type_id,
            "position"."timestamp",
            "position".signal_quality,
            "position".geom,
            "position".is_modelled,
            "position".instrument_config_id
           FROM rov."position"
          ORDER BY "position"."timestamp") f ON ((f.instrument_config_id = e.id)))
  WHERE ((f."timestamp" >= a.start_time) AND (f."timestamp" <= a.end_time))
  GROUP BY a.id, f.instrument_config_id
  WITH NO DATA;


ALTER MATERIALIZED VIEW rov.cruise_track OWNER TO msea_admin;

--
-- Name: MATERIALIZED VIEW cruise_track; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON MATERIALIZED VIEW rov.cruise_track IS 'Constructs a geometry for each [cruise](#rov.cruise) which describes the path of the ship.';


--
-- Name: COLUMN cruise_track.cruise_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_track.cruise_id IS 'The reference to the [cruise](#rov.cruise).';


--
-- Name: COLUMN cruise_track.cruise_name; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_track.cruise_name IS 'The [cruise](#rov.cruise) name and leg.';


--
-- Name: COLUMN cruise_track.instrument_config_id; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_track.instrument_config_id IS 'The reference to the [instrument configuration](#rov.instrument_config).';


--
-- Name: COLUMN cruise_track.geom; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_track.geom IS 'The cruise track geometry.';


--
-- Name: COLUMN cruise_track.colour; Type: COMMENT; Schema: rov; Owner: msea_admin
--

COMMENT ON COLUMN rov.cruise_track.colour IS 'The a colour code generated from the cruise''s ID used for cartography.';


--
-- Name: event_log_data event_log_data_pkey; Type: CONSTRAINT; Schema: iform; Owner: msea_admin
--

ALTER TABLE ONLY iform.event_log_data
    ADD CONSTRAINT event_log_data_pkey PRIMARY KEY (id);


--
-- Name: quadrat quadrat_name_key; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat
    ADD CONSTRAINT quadrat_name_key UNIQUE (name);


--
-- Name: quadrat_observation quadrat_observation_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_observation
    ADD CONSTRAINT quadrat_observation_pkey PRIMARY KEY (id);


--
-- Name: quadrat quadrat_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat
    ADD CONSTRAINT quadrat_pkey PRIMARY KEY (id);


--
-- Name: quadrat_substrate_coverage quadrat_substrate_coverage_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_substrate_coverage
    ADD CONSTRAINT quadrat_substrate_coverage_pkey PRIMARY KEY (id);


--
-- Name: quadrat_substrate quadrat_substrate_name_key; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_substrate
    ADD CONSTRAINT quadrat_substrate_name_key UNIQUE (name);


--
-- Name: quadrat_substrate quadrat_substrate_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_substrate
    ADD CONSTRAINT quadrat_substrate_pkey PRIMARY KEY (id);


--
-- Name: survey_crew survey_crew_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_crew
    ADD CONSTRAINT survey_crew_pkey PRIMARY KEY (id);


--
-- Name: survey survey_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey
    ADD CONSTRAINT survey_pkey PRIMARY KEY (id);


--
-- Name: survey_role survey_role_name_key; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_role
    ADD CONSTRAINT survey_role_name_key UNIQUE (name);


--
-- Name: survey_role survey_role_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_role
    ADD CONSTRAINT survey_role_pkey PRIMARY KEY (id);


--
-- Name: survey_taxon survey_taxon_pkey; Type: CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_taxon
    ADD CONSTRAINT survey_taxon_pkey PRIMARY KEY (id);


--
-- Name: geoip_city_blocks geoip_city_blocks_pkey; Type: CONSTRAINT; Schema: maxmind; Owner: msea_admin
--

ALTER TABLE ONLY maxmind.geoip_city_blocks
    ADD CONSTRAINT geoip_city_blocks_pkey PRIMARY KEY (network);


--
-- Name: geoip_city_locations geoip_city_locations_pkey; Type: CONSTRAINT; Schema: maxmind; Owner: msea_admin
--

ALTER TABLE ONLY maxmind.geoip_city_locations
    ADD CONSTRAINT geoip_city_locations_pkey PRIMARY KEY (geoname_id);


--
-- Name: cruise cruise_pkey; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.cruise
    ADD CONSTRAINT cruise_pkey PRIMARY KEY (id);


--
-- Name: cruise cruise_row_id_key; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.cruise
    ADD CONSTRAINT cruise_row_id_key UNIQUE (row_id);


--
-- Name: diveconfig diveconfig_pkey; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.diveconfig
    ADD CONSTRAINT diveconfig_pkey PRIMARY KEY (id);


--
-- Name: diveconfig diveconfig_row_id_key; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.diveconfig
    ADD CONSTRAINT diveconfig_row_id_key UNIQUE (row_id);


--
-- Name: dives dives_pkey; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.dives
    ADD CONSTRAINT dives_pkey PRIMARY KEY (id);


--
-- Name: dives dives_row_id_key; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.dives
    ADD CONSTRAINT dives_row_id_key UNIQUE (row_id);


--
-- Name: equipconfig equipconfig_pkey; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.equipconfig
    ADD CONSTRAINT equipconfig_pkey PRIMARY KEY (id);


--
-- Name: equipconfig equipconfig_row_id_key; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.equipconfig
    ADD CONSTRAINT equipconfig_row_id_key UNIQUE (row_id);


--
-- Name: equipment equipment_pkey; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.equipment
    ADD CONSTRAINT equipment_pkey PRIMARY KEY (id);


--
-- Name: equipment equipment_row_id_key; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.equipment
    ADD CONSTRAINT equipment_row_id_key UNIQUE (row_id);


--
-- Name: people people_pkey; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);


--
-- Name: people people_row_id_key; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.people
    ADD CONSTRAINT people_row_id_key UNIQUE (row_id);


--
-- Name: transects transects_pkey; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.transects
    ADD CONSTRAINT transects_pkey PRIMARY KEY (id);


--
-- Name: transects transects_row_id_key; Type: CONSTRAINT; Schema: ndst; Owner: msea_admin
--

ALTER TABLE ONLY ndst.transects
    ADD CONSTRAINT transects_row_id_key UNIQUE (row_id);


--
-- Name: protected_area_data_object_file data_object_file_pkey; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area_data_object_file
    ADD CONSTRAINT data_object_file_pkey PRIMARY KEY (id);


--
-- Name: protected_area_data_object data_object_pkey; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area_data_object
    ADD CONSTRAINT data_object_pkey PRIMARY KEY (id);


--
-- Name: mpa_data_object_file mpa_data_object_file_pkey; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa_data_object_file
    ADD CONSTRAINT mpa_data_object_file_pkey PRIMARY KEY (id);


--
-- Name: mpa_data_object mpa_data_object_pkey; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa_data_object
    ADD CONSTRAINT mpa_data_object_pkey PRIMARY KEY (id);


--
-- Name: mpa mpa_unique; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa
    ADD CONSTRAINT mpa_unique UNIQUE (objectid);


--
-- Name: mpa mpas_pk; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa
    ADD CONSTRAINT mpas_pk PRIMARY KEY (ogc_fid);


--
-- Name: protected_area protected_area_pkey; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area
    ADD CONSTRAINT protected_area_pkey PRIMARY KEY (id);


--
-- Name: rca rca_pkey; Type: CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.rca
    ADD CONSTRAINT rca_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user auth_user_email_unique; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_email_unique UNIQUE (email);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: knox_authtoken knox_authtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.knox_authtoken
    ADD CONSTRAINT knox_authtoken_pkey PRIMARY KEY (digest);


--
-- Name: nyt_notification nyt_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_notification
    ADD CONSTRAINT nyt_notification_pkey PRIMARY KEY (id);


--
-- Name: nyt_notificationtype nyt_notificationtype_pkey; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_notificationtype
    ADD CONSTRAINT nyt_notificationtype_pkey PRIMARY KEY (key);


--
-- Name: nyt_settings nyt_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_settings
    ADD CONSTRAINT nyt_settings_pkey PRIMARY KEY (id);


--
-- Name: nyt_subscription nyt_subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_subscription
    ADD CONSTRAINT nyt_subscription_pkey PRIMARY KEY (id);


--
-- Name: thumbnail_kvstore thumbnail_kvstore_pkey; Type: CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.thumbnail_kvstore
    ADD CONSTRAINT thumbnail_kvstore_pkey PRIMARY KEY (key);


--
-- Name: abundance abundance_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.abundance
    ADD CONSTRAINT abundance_name_key UNIQUE (name);


--
-- Name: abundance abundance_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.abundance
    ADD CONSTRAINT abundance_pkey PRIMARY KEY (id);


--
-- Name: annotation_job_annotation_protocol annotation_job_annotation_pro_annotation_job_id_annotation__key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_annotation_protocol
    ADD CONSTRAINT annotation_job_annotation_pro_annotation_job_id_annotation__key UNIQUE (annotation_job_id, annotation_protocol_id);


--
-- Name: annotation_job_annotation_protocol annotation_job_annotation_protocol_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_annotation_protocol
    ADD CONSTRAINT annotation_job_annotation_protocol_pkey PRIMARY KEY (id);


--
-- Name: annotation_job_crew annotation_job_crew_annotation_job_id_person_id_role_id_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_crew
    ADD CONSTRAINT annotation_job_crew_annotation_job_id_person_id_role_id_key UNIQUE (annotation_job_id, person_id, role_id);


--
-- Name: annotation_job_crew annotation_job_crew_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_crew
    ADD CONSTRAINT annotation_job_crew_pkey PRIMARY KEY (id);


--
-- Name: annotation_job annotation_job_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job
    ADD CONSTRAINT annotation_job_pkey PRIMARY KEY (id);


--
-- Name: annotation_job_role annotation_job_role_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_role
    ADD CONSTRAINT annotation_job_role_pkey PRIMARY KEY (id);


--
-- Name: annotation_protocol_document annotation_protocol_document_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol_document
    ADD CONSTRAINT annotation_protocol_document_pkey PRIMARY KEY (id);


--
-- Name: annotation_protocol annotation_protocol_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol
    ADD CONSTRAINT annotation_protocol_name_key UNIQUE (name);


--
-- Name: annotation_protocol annotation_protocol_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol
    ADD CONSTRAINT annotation_protocol_pkey PRIMARY KEY (id);


--
-- Name: annotation_software annotation_software_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_software
    ADD CONSTRAINT annotation_software_name_key UNIQUE (name);


--
-- Name: annotation_software annotation_software_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_software
    ADD CONSTRAINT annotation_software_pkey PRIMARY KEY (id);


--
-- Name: biocover biocover_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.biocover
    ADD CONSTRAINT biocover_name_key UNIQUE (name);


--
-- Name: biocover biocover_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.biocover
    ADD CONSTRAINT biocover_pkey PRIMARY KEY (id);


--
-- Name: comment_event comment_event_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.comment_event
    ADD CONSTRAINT comment_event_pkey PRIMARY KEY (event_id);


--
-- Name: complexity complexity_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.complexity
    ADD CONSTRAINT complexity_name_key UNIQUE (name);


--
-- Name: complexity complexity_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.complexity
    ADD CONSTRAINT complexity_pkey PRIMARY KEY (id);


--
-- Name: coverage cov_unique_range; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.coverage
    ADD CONSTRAINT cov_unique_range UNIQUE (minimum, maximum);


--
-- Name: coverage coverage_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.coverage
    ADD CONSTRAINT coverage_name_key UNIQUE (name);


--
-- Name: coverage coverage_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.coverage
    ADD CONSTRAINT coverage_pkey PRIMARY KEY (id);


--
-- Name: cruise_crew cruise_crew_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_crew
    ADD CONSTRAINT cruise_crew_pkey PRIMARY KEY (id);


--
-- Name: cruise_document cruise_document_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_document
    ADD CONSTRAINT cruise_document_pkey PRIMARY KEY (id);


--
-- Name: cruise_fn_contact cruise_fn_contact_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_fn_contact
    ADD CONSTRAINT cruise_fn_contact_pkey PRIMARY KEY (id);


--
-- Name: cruise_import cruise_import_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_import
    ADD CONSTRAINT cruise_import_pkey PRIMARY KEY (id);


--
-- Name: cruise_library cruise_library_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_library
    ADD CONSTRAINT cruise_library_pkey PRIMARY KEY (id);


--
-- Name: cruise_program cruise_program_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_program
    ADD CONSTRAINT cruise_program_pkey PRIMARY KEY (id);


--
-- Name: cruise_role cruise_role_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_role
    ADD CONSTRAINT cruise_role_name_key UNIQUE (name);


--
-- Name: cruise_role cruise_role_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_role
    ADD CONSTRAINT cruise_role_pkey PRIMARY KEY (id);


--
-- Name: dive d_unique_cruiseleg_name; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive
    ADD CONSTRAINT d_unique_cruiseleg_name UNIQUE (cruise_id, name);


--
-- Name: disturbance disturbance_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.disturbance
    ADD CONSTRAINT disturbance_name_key UNIQUE (name);


--
-- Name: disturbance disturbance_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.disturbance
    ADD CONSTRAINT disturbance_pkey PRIMARY KEY (id);


--
-- Name: dive_crew dive_crew_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_crew
    ADD CONSTRAINT dive_crew_pkey PRIMARY KEY (id);


--
-- Name: dive dive_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive
    ADD CONSTRAINT dive_pkey PRIMARY KEY (id);


--
-- Name: dive_role dive_role_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_role
    ADD CONSTRAINT dive_role_name_key UNIQUE (name);


--
-- Name: dive_role dive_role_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_role
    ADD CONSTRAINT dive_role_pkey PRIMARY KEY (id);


--
-- Name: equipment_type equipment_type_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.equipment_type
    ADD CONSTRAINT equipment_type_name_key UNIQUE (name);


--
-- Name: equipment_type equipment_type_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.equipment_type
    ADD CONSTRAINT equipment_type_pkey PRIMARY KEY (id);


--
-- Name: event_logger event_logger_person_id_event_id_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event_logger
    ADD CONSTRAINT event_logger_person_id_event_id_key UNIQUE (person_id, event_id);


--
-- Name: event_logger event_logger_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event_logger
    ADD CONSTRAINT event_logger_pkey PRIMARY KEY (id);


--
-- Name: event_logger event_logger_unique; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event_logger
    ADD CONSTRAINT event_logger_unique UNIQUE (person_id, event_id);


--
-- Name: event event_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- Name: flow flow_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.flow
    ADD CONSTRAINT flow_pkey PRIMARY KEY (id);


--
-- Name: habitat_event habitat_event_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT habitat_event_pkey PRIMARY KEY (event_id);


--
-- Name: image_quality image_quality_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.image_quality
    ADD CONSTRAINT image_quality_pkey PRIMARY KEY (id);


--
-- Name: import_queue_annotator import_queue_annotation_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_annotator
    ADD CONSTRAINT import_queue_annotation_name_key UNIQUE (name);


--
-- Name: import_queue_annotator import_queue_annotation_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_annotator
    ADD CONSTRAINT import_queue_annotation_pkey PRIMARY KEY (id);


--
-- Name: import_queue_annotator_label_map_prefill import_queue_annotator_label_map_prefill_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_annotator_label_map_prefill
    ADD CONSTRAINT import_queue_annotator_label_map_prefill_pkey PRIMARY KEY (id);


--
-- Name: import_queue_annotator_label_map_prefill import_queue_annotator_label_map_prefill_tree_name_unique; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_annotator_label_map_prefill
    ADD CONSTRAINT import_queue_annotator_label_map_prefill_tree_name_unique UNIQUE (name, label_tree_name);


--
-- Name: import_queue_pi import_queue_pi_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_pi
    ADD CONSTRAINT import_queue_pi_pkey PRIMARY KEY (id);


--
-- Name: instrument inst_unique_model_serial_number; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument
    ADD CONSTRAINT inst_unique_model_serial_number UNIQUE (model_id, serial_number);


--
-- Name: instrument_config instrument_config_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument_config
    ADD CONSTRAINT instrument_config_pkey PRIMARY KEY (id);


--
-- Name: instrument instrument_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument
    ADD CONSTRAINT instrument_pkey PRIMARY KEY (id);


--
-- Name: import_queue_pi iqpi_unique; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_pi
    ADD CONSTRAINT iqpi_unique UNIQUE (name);


--
-- Name: measurement measurement_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement
    ADD CONSTRAINT measurement_pkey PRIMARY KEY (id);


--
-- Name: measurement_type measurement_type_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement_type
    ADD CONSTRAINT measurement_type_pkey PRIMARY KEY (id);


--
-- Name: medium_format medium_format_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.medium_format
    ADD CONSTRAINT medium_format_name_key UNIQUE (name);


--
-- Name: medium_format medium_format_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.medium_format
    ADD CONSTRAINT medium_format_pkey PRIMARY KEY (id);


--
-- Name: medium_type medium_type_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.medium_type
    ADD CONSTRAINT medium_type_name_key UNIQUE (name);


--
-- Name: medium_type medium_type_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.medium_type
    ADD CONSTRAINT medium_type_pkey PRIMARY KEY (id);


--
-- Name: model_documentation model_documentation_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_documentation
    ADD CONSTRAINT model_documentation_pkey PRIMARY KEY (id);


--
-- Name: model_equipment_type model_equipment_type_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_equipment_type
    ADD CONSTRAINT model_equipment_type_pkey PRIMARY KEY (id);


--
-- Name: model model_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model
    ADD CONSTRAINT model_pkey PRIMARY KEY (id);


--
-- Name: measurement_type mt_unique_name_unit; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement_type
    ADD CONSTRAINT mt_unique_name_unit UNIQUE (name, unit);


--
-- Name: observation_confidence observation_confidence_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_confidence
    ADD CONSTRAINT observation_confidence_name_key UNIQUE (name);


--
-- Name: observation_confidence observation_confidence_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_confidence
    ADD CONSTRAINT observation_confidence_pkey PRIMARY KEY (id);


--
-- Name: observation observation_event_id_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation
    ADD CONSTRAINT observation_event_id_key UNIQUE (event_id);


--
-- Name: observation_event observation_event_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_event
    ADD CONSTRAINT observation_event_pkey PRIMARY KEY (event_id);


--
-- Name: orientation orientation_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.orientation
    ADD CONSTRAINT orientation_pkey PRIMARY KEY (id);


--
-- Name: orientation_type orientation_type_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.orientation_type
    ADD CONSTRAINT orientation_type_pkey PRIMARY KEY (id);


--
-- Name: orientation_type ot_unique_name_unit; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.orientation_type
    ADD CONSTRAINT ot_unique_name_unit UNIQUE (name, unit);


--
-- Name: platform plat_model_serial_number; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform
    ADD CONSTRAINT plat_model_serial_number UNIQUE (model_id, serial_number);


--
-- Name: platform_config platform_config_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform_config
    ADD CONSTRAINT platform_config_pkey PRIMARY KEY (id);


--
-- Name: platform platform_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform
    ADD CONSTRAINT platform_pkey PRIMARY KEY (id);


--
-- Name: position position_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov."position"
    ADD CONSTRAINT position_pkey PRIMARY KEY (id);


--
-- Name: position_type position_type_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.position_type
    ADD CONSTRAINT position_type_pkey PRIMARY KEY (id);


--
-- Name: program_library program_library_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_library
    ADD CONSTRAINT program_library_pkey PRIMARY KEY (id);


--
-- Name: program_member program_member_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_member
    ADD CONSTRAINT program_member_pkey PRIMARY KEY (id);


--
-- Name: program program_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program
    ADD CONSTRAINT program_name_key UNIQUE (name);


--
-- Name: program program_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program
    ADD CONSTRAINT program_pkey PRIMARY KEY (id);


--
-- Name: program_role program_role_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_role
    ADD CONSTRAINT program_role_name_key UNIQUE (name);


--
-- Name: program_role program_role_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_role
    ADD CONSTRAINT program_role_pkey PRIMARY KEY (id);


--
-- Name: protocol protocol_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.protocol
    ADD CONSTRAINT protocol_pkey PRIMARY KEY (id);


--
-- Name: position_type pt_unique_name_unit; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.position_type
    ADD CONSTRAINT pt_unique_name_unit UNIQUE (name, unit);


--
-- Name: relief relief_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.relief
    ADD CONSTRAINT relief_name_key UNIQUE (name);


--
-- Name: relief relief_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.relief
    ADD CONSTRAINT relief_pkey PRIMARY KEY (id);


--
-- Name: signup_area signup_area_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.signup_area
    ADD CONSTRAINT signup_area_name_key UNIQUE (name);


--
-- Name: signup_area signup_area_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.signup_area
    ADD CONSTRAINT signup_area_pkey PRIMARY KEY (id);


--
-- Name: status_type_detail st_unique_type_detail; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_type_detail
    ADD CONSTRAINT st_unique_type_detail UNIQUE (status_type_id, detail);


--
-- Name: status_event status_event_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_event
    ADD CONSTRAINT status_event_pkey PRIMARY KEY (event_id);


--
-- Name: status_type_detail status_type_detail_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_type_detail
    ADD CONSTRAINT status_type_detail_pkey PRIMARY KEY (id);


--
-- Name: status_type status_type_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_type
    ADD CONSTRAINT status_type_pkey PRIMARY KEY (id);


--
-- Name: substrate substrate_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.substrate
    ADD CONSTRAINT substrate_name_key UNIQUE (name);


--
-- Name: substrate substrate_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.substrate
    ADD CONSTRAINT substrate_pkey PRIMARY KEY (id);


--
-- Name: survey_mode survey_mode_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.survey_mode
    ADD CONSTRAINT survey_mode_pkey PRIMARY KEY (id);


--
-- Name: thickness thickness_name_key; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.thickness
    ADD CONSTRAINT thickness_name_key UNIQUE (name);


--
-- Name: thickness thickness_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.thickness
    ADD CONSTRAINT thickness_pkey PRIMARY KEY (id);


--
-- Name: transect tr_unique_dive_name; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.transect
    ADD CONSTRAINT tr_unique_dive_name UNIQUE (name, dive_id);


--
-- Name: transect transect_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.transect
    ADD CONSTRAINT transect_pkey PRIMARY KEY (id);


--
-- Name: model unique_brand_model; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model
    ADD CONSTRAINT unique_brand_model UNIQUE (brand_name, model_name);


--
-- Name: weather_observation weather_observation_pkey; Type: CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.weather_observation
    ADD CONSTRAINT weather_observation_pkey PRIMARY KEY (id);


--
-- Name: db_version db_version_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.db_version
    ADD CONSTRAINT db_version_pkey PRIMARY KEY (id);


--
-- Name: file file_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.file
    ADD CONSTRAINT file_pkey PRIMARY KEY (id);


--
-- Name: file_type file_type_name_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.file_type
    ADD CONSTRAINT file_type_name_key UNIQUE (name);


--
-- Name: file_type file_type_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.file_type
    ADD CONSTRAINT file_type_pkey PRIMARY KEY (id);


--
-- Name: file_type file_type_short_code_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.file_type
    ADD CONSTRAINT file_type_short_code_key UNIQUE (short_code);


--
-- Name: library library_doi_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.library
    ADD CONSTRAINT library_doi_key UNIQUE (doi);


--
-- Name: library library_file_id_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.library
    ADD CONSTRAINT library_file_id_key UNIQUE (file_id);


--
-- Name: library library_isbn_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.library
    ADD CONSTRAINT library_isbn_key UNIQUE (isbn);


--
-- Name: library library_issn_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.library
    ADD CONSTRAINT library_issn_key UNIQUE (issn);


--
-- Name: library library_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.library
    ADD CONSTRAINT library_pkey PRIMARY KEY (id);


--
-- Name: mseauser mseauser_pg_role_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.mseauser
    ADD CONSTRAINT mseauser_pg_role_key UNIQUE (pg_role);


--
-- Name: mseauser mseauser_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.mseauser
    ADD CONSTRAINT mseauser_pkey PRIMARY KEY (id);


--
-- Name: user_restriction mseauser_restriction_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.user_restriction
    ADD CONSTRAINT mseauser_restriction_pkey PRIMARY KEY (id);


--
-- Name: mseauser mseauser_user_id_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.mseauser
    ADD CONSTRAINT mseauser_user_id_key UNIQUE (user_id);


--
-- Name: organisation organisation_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.organisation
    ADD CONSTRAINT organisation_pkey PRIMARY KEY (id);


--
-- Name: person person_biigle_user_id_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.person
    ADD CONSTRAINT person_biigle_user_id_key UNIQUE (biigle_user_id);


--
-- Name: person person_biigle_uuid_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.person
    ADD CONSTRAINT person_biigle_uuid_key UNIQUE (biigle_uuid);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: restriction restriction_name_unique; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.restriction
    ADD CONSTRAINT restriction_name_unique UNIQUE (name);


--
-- Name: restriction restriction_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.restriction
    ADD CONSTRAINT restriction_pkey PRIMARY KEY (id);


--
-- Name: site site_name_key; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.site
    ADD CONSTRAINT site_name_key UNIQUE (name);


--
-- Name: site site_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.site
    ADD CONSTRAINT site_pkey PRIMARY KEY (id);


--
-- Name: spatial_library_file spatial_library_file_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.spatial_library_file
    ADD CONSTRAINT spatial_library_file_pkey PRIMARY KEY (id);


--
-- Name: spatial_library spatial_library_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.spatial_library
    ADD CONSTRAINT spatial_library_pkey PRIMARY KEY (id);


--
-- Name: taxon taxon_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.taxon
    ADD CONSTRAINT taxon_pkey PRIMARY KEY (id);


--
-- Name: taxon_restriction taxon_restriction_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.taxon_restriction
    ADD CONSTRAINT taxon_restriction_pkey PRIMARY KEY (id);


--
-- Name: uploaded_file uploads_pkey; Type: CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.uploaded_file
    ADD CONSTRAINT uploads_pkey PRIMARY KEY (id);


--
-- Name: taxon taxon_pkey; Type: CONSTRAINT; Schema: taxonomy; Owner: msea_admin
--

ALTER TABLE ONLY taxonomy.taxon
    ADD CONSTRAINT taxon_pkey PRIMARY KEY (id);


--
-- Name: taxon taxon_source_taxon_id_key; Type: CONSTRAINT; Schema: taxonomy; Owner: msea_admin
--

ALTER TABLE ONLY taxonomy.taxon
    ADD CONSTRAINT taxon_source_taxon_id_key UNIQUE (source, taxon_id);


--
-- Name: actor actor_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.actor
    ADD CONSTRAINT actor_pkey PRIMARY KEY (actor_id);


--
-- Name: archive archive_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.archive
    ADD CONSTRAINT archive_pkey PRIMARY KEY (ar_id);


--
-- Name: bot_passwords bot_passwords_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.bot_passwords
    ADD CONSTRAINT bot_passwords_pkey PRIMARY KEY (bp_user, bp_app_id);


--
-- Name: category category_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (cat_id);


--
-- Name: categorylinks categorylinks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.categorylinks
    ADD CONSTRAINT categorylinks_pkey PRIMARY KEY (cl_from, cl_to);


--
-- Name: change_tag_def change_tag_def_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.change_tag_def
    ADD CONSTRAINT change_tag_def_pkey PRIMARY KEY (ctd_id);


--
-- Name: change_tag change_tag_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.change_tag
    ADD CONSTRAINT change_tag_pkey PRIMARY KEY (ct_id);


--
-- Name: comment comment_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (comment_id);


--
-- Name: content_models content_models_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.content_models
    ADD CONSTRAINT content_models_pkey PRIMARY KEY (model_id);


--
-- Name: content content_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.content
    ADD CONSTRAINT content_pkey PRIMARY KEY (content_id);


--
-- Name: externallinks externallinks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.externallinks
    ADD CONSTRAINT externallinks_pkey PRIMARY KEY (el_id);


--
-- Name: filearchive filearchive_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.filearchive
    ADD CONSTRAINT filearchive_pkey PRIMARY KEY (fa_id);


--
-- Name: image image_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.image
    ADD CONSTRAINT image_pkey PRIMARY KEY (img_name);


--
-- Name: imagelinks imagelinks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.imagelinks
    ADD CONSTRAINT imagelinks_pkey PRIMARY KEY (il_from, il_to);


--
-- Name: interwiki interwiki_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.interwiki
    ADD CONSTRAINT interwiki_pkey PRIMARY KEY (iw_prefix);


--
-- Name: ip_changes ip_changes_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.ip_changes
    ADD CONSTRAINT ip_changes_pkey PRIMARY KEY (ipc_rev_id);


--
-- Name: ipblocks ipblocks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.ipblocks
    ADD CONSTRAINT ipblocks_pkey PRIMARY KEY (ipb_id);


--
-- Name: ipblocks_restrictions ipblocks_restrictions_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.ipblocks_restrictions
    ADD CONSTRAINT ipblocks_restrictions_pkey PRIMARY KEY (ir_ipb_id, ir_type, ir_value);


--
-- Name: iwlinks iwlinks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.iwlinks
    ADD CONSTRAINT iwlinks_pkey PRIMARY KEY (iwl_from, iwl_prefix, iwl_title);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.job
    ADD CONSTRAINT job_pkey PRIMARY KEY (job_id);


--
-- Name: l10n_cache l10n_cache_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.l10n_cache
    ADD CONSTRAINT l10n_cache_pkey PRIMARY KEY (lc_lang, lc_key);


--
-- Name: langlinks langlinks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.langlinks
    ADD CONSTRAINT langlinks_pkey PRIMARY KEY (ll_from, ll_lang);


--
-- Name: linktarget linktarget_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.linktarget
    ADD CONSTRAINT linktarget_pkey PRIMARY KEY (lt_id);


--
-- Name: log_search log_search_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.log_search
    ADD CONSTRAINT log_search_pkey PRIMARY KEY (ls_field, ls_value, ls_log_id);


--
-- Name: logging logging_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.logging
    ADD CONSTRAINT logging_pkey PRIMARY KEY (log_id);


--
-- Name: module_deps module_deps_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.module_deps
    ADD CONSTRAINT module_deps_pkey PRIMARY KEY (md_module, md_skin);


--
-- Name: objectcache objectcache_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.objectcache
    ADD CONSTRAINT objectcache_pkey PRIMARY KEY (keyname);


--
-- Name: page page_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.page
    ADD CONSTRAINT page_pkey PRIMARY KEY (page_id);


--
-- Name: page_props page_props_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.page_props
    ADD CONSTRAINT page_props_pkey PRIMARY KEY (pp_page, pp_propname);


--
-- Name: page_restrictions page_restrictions_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.page_restrictions
    ADD CONSTRAINT page_restrictions_pkey PRIMARY KEY (pr_id);


--
-- Name: pagelinks pagelinks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.pagelinks
    ADD CONSTRAINT pagelinks_pkey PRIMARY KEY (pl_from, pl_namespace, pl_title);


--
-- Name: protected_titles protected_titles_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.protected_titles
    ADD CONSTRAINT protected_titles_pkey PRIMARY KEY (pt_namespace, pt_title);


--
-- Name: querycache_info querycache_info_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.querycache_info
    ADD CONSTRAINT querycache_info_pkey PRIMARY KEY (qci_type);


--
-- Name: recentchanges recentchanges_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.recentchanges
    ADD CONSTRAINT recentchanges_pkey PRIMARY KEY (rc_id);


--
-- Name: redirect redirect_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.redirect
    ADD CONSTRAINT redirect_pkey PRIMARY KEY (rd_from);


--
-- Name: revision_comment_temp revision_comment_temp_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.revision_comment_temp
    ADD CONSTRAINT revision_comment_temp_pkey PRIMARY KEY (revcomment_rev, revcomment_comment_id);


--
-- Name: revision revision_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.revision
    ADD CONSTRAINT revision_pkey PRIMARY KEY (rev_id);


--
-- Name: site_identifiers site_identifiers_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.site_identifiers
    ADD CONSTRAINT site_identifiers_pkey PRIMARY KEY (si_type, si_key);


--
-- Name: site_stats site_stats_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.site_stats
    ADD CONSTRAINT site_stats_pkey PRIMARY KEY (ss_row_id);


--
-- Name: sites sites_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.sites
    ADD CONSTRAINT sites_pkey PRIMARY KEY (site_id);


--
-- Name: slot_roles slot_roles_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.slot_roles
    ADD CONSTRAINT slot_roles_pkey PRIMARY KEY (role_id);


--
-- Name: slots slots_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.slots
    ADD CONSTRAINT slots_pkey PRIMARY KEY (slot_revision_id, slot_role_id);


--
-- Name: templatelinks templatelinks_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.templatelinks
    ADD CONSTRAINT templatelinks_pkey PRIMARY KEY (tl_from, tl_target_id);


--
-- Name: text text_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.text
    ADD CONSTRAINT text_pkey PRIMARY KEY (old_id);


--
-- Name: updatelog updatelog_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.updatelog
    ADD CONSTRAINT updatelog_pkey PRIMARY KEY (ul_key);


--
-- Name: uploadstash uploadstash_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.uploadstash
    ADD CONSTRAINT uploadstash_pkey PRIMARY KEY (us_id);


--
-- Name: user_autocreate_serial user_autocreate_serial_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.user_autocreate_serial
    ADD CONSTRAINT user_autocreate_serial_pkey PRIMARY KEY (uas_shard);


--
-- Name: user_former_groups user_former_groups_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.user_former_groups
    ADD CONSTRAINT user_former_groups_pkey PRIMARY KEY (ufg_user, ufg_group);


--
-- Name: user_groups user_groups_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.user_groups
    ADD CONSTRAINT user_groups_pkey PRIMARY KEY (ug_user, ug_group);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (user_id);


--
-- Name: user_properties user_properties_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.user_properties
    ADD CONSTRAINT user_properties_pkey PRIMARY KEY (up_user, up_property);


--
-- Name: watchlist_expiry watchlist_expiry_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.watchlist_expiry
    ADD CONSTRAINT watchlist_expiry_pkey PRIMARY KEY (we_item);


--
-- Name: watchlist watchlist_pkey; Type: CONSTRAINT; Schema: wiki; Owner: msea_admin
--

ALTER TABLE ONLY wiki.watchlist
    ADD CONSTRAINT watchlist_pkey PRIMARY KEY (wl_id);


--
-- Name: geoip_city_blocks_location_idx; Type: INDEX; Schema: maxmind; Owner: msea_admin
--

CREATE INDEX geoip_city_blocks_location_idx ON maxmind.geoip_city_blocks USING gist (location);


--
-- Name: geoip_city_blocks_network_idx; Type: INDEX; Schema: maxmind; Owner: msea_admin
--

CREATE INDEX geoip_city_blocks_network_idx ON maxmind.geoip_city_blocks USING gist (network inet_ops);


--
-- Name: geoip_city_blocks_region_idx; Type: INDEX; Schema: maxmind; Owner: msea_admin
--

CREATE INDEX geoip_city_blocks_region_idx ON maxmind.geoip_city_blocks USING gist (region);


--
-- Name: data_object_name_idx; Type: INDEX; Schema: pa; Owner: msea_admin
--

CREATE INDEX data_object_name_idx ON pa.protected_area_data_object USING btree (name);


--
-- Name: mpas_geometry_geom_idx; Type: INDEX; Schema: pa; Owner: msea_admin
--

CREATE INDEX mpas_geometry_geom_idx ON pa.mpa USING gist (geometry);


--
-- Name: protected_area_geom_idx; Type: INDEX; Schema: pa; Owner: msea_admin
--

CREATE INDEX protected_area_geom_idx ON pa.protected_area USING gist (geom);


--
-- Name: rca_geom_idx; Type: INDEX; Schema: pa; Owner: msea_admin
--

CREATE INDEX rca_geom_idx ON pa.rca USING gist (geom);


--
-- Name: rca_name_idx; Type: INDEX; Schema: pa; Owner: msea_admin
--

CREATE INDEX rca_name_idx ON pa.rca USING btree (name);


--
-- Name: rca_rca_id_idx; Type: INDEX; Schema: pa; Owner: msea_admin
--

CREATE INDEX rca_rca_id_idx ON pa.rca USING btree (rca_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: msea
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: knox_authtoken_digest_188c7e77_like; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX knox_authtoken_digest_188c7e77_like ON public.knox_authtoken USING btree (digest varchar_pattern_ops);


--
-- Name: knox_authtoken_token_key_8f4f7d47; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX knox_authtoken_token_key_8f4f7d47 ON public.knox_authtoken USING btree (token_key);


--
-- Name: knox_authtoken_token_key_8f4f7d47_like; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX knox_authtoken_token_key_8f4f7d47_like ON public.knox_authtoken USING btree (token_key varchar_pattern_ops);


--
-- Name: knox_authtoken_user_id_e5a5d899; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX knox_authtoken_user_id_e5a5d899 ON public.knox_authtoken USING btree (user_id);


--
-- Name: nyt_notification_subscription_id_5a132ae1; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_notification_subscription_id_5a132ae1 ON public.nyt_notification USING btree (subscription_id);


--
-- Name: nyt_notification_user_id_acbb5c10; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_notification_user_id_acbb5c10 ON public.nyt_notification USING btree (user_id);


--
-- Name: nyt_notificationtype_content_type_id_18800dea; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_notificationtype_content_type_id_18800dea ON public.nyt_notificationtype USING btree (content_type_id);


--
-- Name: nyt_notificationtype_key_eff7451c_like; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_notificationtype_key_eff7451c_like ON public.nyt_notificationtype USING btree (key varchar_pattern_ops);


--
-- Name: nyt_settings_user_id_1fad6d98; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_settings_user_id_1fad6d98 ON public.nyt_settings USING btree (user_id);


--
-- Name: nyt_subscription_latest_id_bbb7d98b; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_subscription_latest_id_bbb7d98b ON public.nyt_subscription USING btree (latest_id);


--
-- Name: nyt_subscription_notification_type_id_ca8af379; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_subscription_notification_type_id_ca8af379 ON public.nyt_subscription USING btree (notification_type_id);


--
-- Name: nyt_subscription_notification_type_id_ca8af379_like; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_subscription_notification_type_id_ca8af379_like ON public.nyt_subscription USING btree (notification_type_id varchar_pattern_ops);


--
-- Name: nyt_subscription_settings_id_761bae06; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX nyt_subscription_settings_id_761bae06 ON public.nyt_subscription USING btree (settings_id);


--
-- Name: thumbnail_kvstore_key_3f850178_like; Type: INDEX; Schema: public; Owner: msea_admin
--

CREATE INDEX thumbnail_kvstore_key_3f850178_like ON public.thumbnail_kvstore USING btree (key varchar_pattern_ops);


--
-- Name: annotation_protocol_name_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX annotation_protocol_name_idx ON rov.annotation_protocol USING btree (name);


--
-- Name: cruise_crew_unique_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE UNIQUE INDEX cruise_crew_unique_idx ON rov.cruise_crew USING btree (cruise_id, person_id, cruise_role_id);


--
-- Name: cruise_end_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX cruise_end_time_idx ON rov.cruise USING btree (end_time);


--
-- Name: cruise_leg_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX cruise_leg_idx ON rov.cruise USING btree (leg);


--
-- Name: cruise_leg_unique_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE UNIQUE INDEX cruise_leg_unique_idx ON rov.cruise USING btree (name, leg);


--
-- Name: cruise_name_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX cruise_name_idx ON rov.cruise USING btree (name);


--
-- Name: cruise_start_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX cruise_start_time_idx ON rov.cruise USING btree (start_time);


--
-- Name: dive_crew_unique_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE UNIQUE INDEX dive_crew_unique_idx ON rov.dive_crew USING btree (dive_id, person_id, dive_role_id);


--
-- Name: dive_cruise_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX dive_cruise_id_idx ON rov.dive USING btree (cruise_id);


--
-- Name: dive_end_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX dive_end_time_idx ON rov.dive USING btree (end_time);


--
-- Name: dive_name_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX dive_name_idx ON rov.dive USING btree (name);


--
-- Name: dive_ship_config_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX dive_ship_config_id_idx ON rov.dive USING btree (ship_config_id);


--
-- Name: dive_start_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX dive_start_time_idx ON rov.dive USING btree (start_time);


--
-- Name: dive_sub_config_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX dive_sub_config_id_idx ON rov.dive USING btree (sub_config_id);


--
-- Name: dive_track_index_geom; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX dive_track_index_geom ON rov.dive_track USING gist (geom);


--
-- Name: dive_track_uniq; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE UNIQUE INDEX dive_track_uniq ON rov.dive_track USING btree (cruise_id, dive_id, cruise_name, dive_name, colour);


--
-- Name: event_dive_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_dive_id_idx ON rov.event USING btree (dive_id);


--
-- Name: event_end_time_gist_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_end_time_gist_idx ON rov.event USING gist (end_time);


--
-- Name: event_end_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_end_time_idx ON rov.event USING btree (end_time);


--
-- Name: event_epoch_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_epoch_idx ON rov.event USING btree (date_part('epoch'::text, start_time));


--
-- Name: event_original_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_original_id_idx ON rov.event USING btree (original_id);


--
-- Name: event_start_time_gist_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_start_time_gist_idx ON rov.event USING gist (start_time);


--
-- Name: event_start_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_start_time_idx ON rov.event USING btree (start_time);


--
-- Name: event_transect_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX event_transect_id_idx ON rov.event USING btree (transect_id);


--
-- Name: evt_depth_end_measurement_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX evt_depth_end_measurement_id_idx ON rov.evt_depth USING btree (end_measurement_id);


--
-- Name: evt_depth_event_id; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX evt_depth_event_id ON rov.evt_depth USING btree (event_id);


--
-- Name: evt_depth_start_measurement_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX evt_depth_start_measurement_id_idx ON rov.evt_depth USING btree (start_measurement_id);


--
-- Name: evt_pos_end_position_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX evt_pos_end_position_id_idx ON rov.evt_pos USING btree (end_position_id);


--
-- Name: evt_pos_event_id; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX evt_pos_event_id ON rov.evt_pos USING btree (event_id);


--
-- Name: evt_pos_start_position_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX evt_pos_start_position_id_idx ON rov.evt_pos USING btree (start_position_id);


--
-- Name: instrument_config_platform_config_id; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX instrument_config_platform_config_id ON rov.instrument_config USING btree (platform_config_id);


--
-- Name: measurement_epoch_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX measurement_epoch_idx ON rov.measurement USING btree (date_part('epoch'::text, "timestamp"));


--
-- Name: measurement_instrument_config_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX measurement_instrument_config_id_idx ON rov.measurement USING btree (instrument_config_id);


--
-- Name: measurement_measurement_type_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX measurement_measurement_type_id_idx ON rov.measurement USING btree (measurement_type_id);


--
-- Name: measurement_timestamp_gist_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX measurement_timestamp_gist_idx ON rov.measurement USING gist ("timestamp");


--
-- Name: measurement_timestamp_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX measurement_timestamp_idx ON rov.measurement USING btree ("timestamp");


--
-- Name: measurement_type_name_id; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX measurement_type_name_id ON rov.measurement_type USING btree (name);


--
-- Name: msearement_timestamp_gist_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX msearement_timestamp_gist_idx ON rov.measurement USING gist ("timestamp");


--
-- Name: observation_event_event_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX observation_event_event_id_idx ON rov.observation_event USING btree (event_id);


--
-- Name: observation_properties_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX observation_properties_idx ON rov.observation USING gin (properties);


--
-- Name: orientation_instrument_config_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX orientation_instrument_config_id_idx ON rov.orientation USING btree (instrument_config_id);


--
-- Name: orientation_orientation_type_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX orientation_orientation_type_id_idx ON rov.orientation USING btree (orientation_type_id);


--
-- Name: orientation_timestamp_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX orientation_timestamp_idx ON rov.orientation USING btree ("timestamp");


--
-- Name: platform_config_platform_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX platform_config_platform_id_idx ON rov.platform_config USING btree (platform_id);


--
-- Name: position_epoch_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX position_epoch_idx ON rov."position" USING btree (date_part('epoch'::text, "timestamp"));


--
-- Name: position_geom_geography_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX position_geom_geography_idx ON rov."position" USING gist (geom);


--
-- Name: position_geom_geometry_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX position_geom_geometry_idx ON rov."position" USING gist (geom);


--
-- Name: position_instrument_config_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX position_instrument_config_id_idx ON rov."position" USING btree (instrument_config_id);


--
-- Name: position_measurement_type_id_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX position_measurement_type_id_idx ON rov."position" USING btree (position_type_id);


--
-- Name: position_timestamp_gist_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX position_timestamp_gist_idx ON rov."position" USING gist ("timestamp");


--
-- Name: position_timestamp_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX position_timestamp_idx ON rov."position" USING btree ("timestamp");


--
-- Name: program_end_date_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX program_end_date_idx ON rov.program USING btree (end_date);


--
-- Name: program_name_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX program_name_idx ON rov.program USING btree (name);


--
-- Name: program_start_date_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX program_start_date_idx ON rov.program USING btree (start_date);


--
-- Name: signup_area_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX signup_area_idx ON rov.signup_area USING gist (geom);


--
-- Name: transect_end_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX transect_end_time_idx ON rov.transect USING btree (end_time);


--
-- Name: transect_name_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX transect_name_idx ON rov.transect USING btree (name);


--
-- Name: transect_start_time_idx; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX transect_start_time_idx ON rov.transect USING btree (start_time);


--
-- Name: transect_track_index_geom; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE INDEX transect_track_index_geom ON rov.transect_track USING gist (geom);


--
-- Name: transect_track_uniq; Type: INDEX; Schema: rov; Owner: msea_admin
--

CREATE UNIQUE INDEX transect_track_uniq ON rov.transect_track USING btree (cruise_id, dive_id, transect_id, cruise_name, dive_name, colour);


--
-- Name: library_mendeleyid_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX library_mendeleyid_idx ON shared.library USING btree (mendeley_id);


--
-- Name: library_title_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX library_title_idx ON shared.library USING btree (title);


--
-- Name: person_email_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX person_email_idx ON shared.person USING btree (lower((email)::text));


--
-- Name: person_email_unique; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE UNIQUE INDEX person_email_unique ON shared.person USING btree (lower((email)::text));


--
-- Name: person_first_name_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX person_first_name_idx ON shared.person USING btree (lower((first_name)::text));


--
-- Name: person_last_name_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX person_last_name_idx ON shared.person USING btree (lower((last_name)::text));


--
-- Name: person_unique; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE UNIQUE INDEX person_unique ON shared.person USING btree (lower((first_name)::text), lower((last_name)::text), lower((email)::text), lower((affiliation)::text));


--
-- Name: site_name_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX site_name_idx ON shared.site USING btree (name);


--
-- Name: spatial_library_geom_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX spatial_library_geom_idx ON shared.spatial_library USING btree (geom);


--
-- Name: spatial_library_name_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX spatial_library_name_idx ON shared.spatial_library USING btree (name);


--
-- Name: taxon_common_name_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX taxon_common_name_idx ON shared.taxon USING gin (common_name public.gin_trgm_ops);


--
-- Name: taxon_original_label_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX taxon_original_label_idx ON shared.taxon USING gin (original_label public.gin_trgm_ops);


--
-- Name: taxon_scientific_name_idx; Type: INDEX; Schema: shared; Owner: msea_admin
--

CREATE INDEX taxon_scientific_name_idx ON shared.taxon USING gin (scientific_name public.gin_trgm_ops);


--
-- Name: taxon_common_name_idx; Type: INDEX; Schema: taxonomy; Owner: msea_admin
--

CREATE INDEX taxon_common_name_idx ON taxonomy.taxon USING gist (common_name public.gist_trgm_ops);


--
-- Name: taxon_rank_idx; Type: INDEX; Schema: taxonomy; Owner: msea_admin
--

CREATE INDEX taxon_rank_idx ON taxonomy.taxon USING btree (rank);


--
-- Name: taxon_scientific_name_idx; Type: INDEX; Schema: taxonomy; Owner: msea_admin
--

CREATE INDEX taxon_scientific_name_idx ON taxonomy.taxon USING gist (scientific_name public.gist_trgm_ops);


--
-- Name: taxon_source_idx; Type: INDEX; Schema: taxonomy; Owner: msea_admin
--

CREATE INDEX taxon_source_idx ON taxonomy.taxon USING btree (source);


--
-- Name: actor_name; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX actor_name ON wiki.actor USING btree (actor_name);


--
-- Name: actor_user; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX actor_user ON wiki.actor USING btree (actor_user);


--
-- Name: ar_actor_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ar_actor_timestamp ON wiki.archive USING btree (ar_actor, ar_timestamp);


--
-- Name: ar_name_title_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ar_name_title_timestamp ON wiki.archive USING btree (ar_namespace, ar_title, ar_timestamp);


--
-- Name: ar_revid_uniq; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX ar_revid_uniq ON wiki.archive USING btree (ar_rev_id);


--
-- Name: cat_pages; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX cat_pages ON wiki.category USING btree (cat_pages);


--
-- Name: cat_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX cat_title ON wiki.category USING btree (cat_title);


--
-- Name: cl_collation_ext; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX cl_collation_ext ON wiki.categorylinks USING btree (cl_collation, cl_to, cl_type, cl_from);


--
-- Name: cl_sortkey; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX cl_sortkey ON wiki.categorylinks USING btree (cl_to, cl_type, cl_sortkey, cl_from);


--
-- Name: cl_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX cl_timestamp ON wiki.categorylinks USING btree (cl_to, cl_timestamp);


--
-- Name: comment_hash; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX comment_hash ON wiki.comment USING btree (comment_hash);


--
-- Name: ct_log_tag_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX ct_log_tag_id ON wiki.change_tag USING btree (ct_log_id, ct_tag_id);


--
-- Name: ct_rc_tag_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX ct_rc_tag_id ON wiki.change_tag USING btree (ct_rc_id, ct_tag_id);


--
-- Name: ct_rev_tag_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX ct_rev_tag_id ON wiki.change_tag USING btree (ct_rev_id, ct_tag_id);


--
-- Name: ct_tag_id_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ct_tag_id_id ON wiki.change_tag USING btree (ct_tag_id, ct_rc_id, ct_rev_id, ct_log_id);


--
-- Name: ctd_count; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ctd_count ON wiki.change_tag_def USING btree (ctd_count);


--
-- Name: ctd_name; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX ctd_name ON wiki.change_tag_def USING btree (ctd_name);


--
-- Name: ctd_user_defined; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ctd_user_defined ON wiki.change_tag_def USING btree (ctd_user_defined);


--
-- Name: el_from; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX el_from ON wiki.externallinks USING btree (el_from);


--
-- Name: el_from_index_60; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX el_from_index_60 ON wiki.externallinks USING btree (el_from, el_index_60, el_id);


--
-- Name: el_index; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX el_index ON wiki.externallinks USING btree (el_index);


--
-- Name: el_index_60; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX el_index_60 ON wiki.externallinks USING btree (el_index_60, el_id);


--
-- Name: el_to; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX el_to ON wiki.externallinks USING btree (el_to, el_from);


--
-- Name: el_to_domain_index_to_path; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX el_to_domain_index_to_path ON wiki.externallinks USING btree (el_to_domain_index, el_to_path);


--
-- Name: exptime; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX exptime ON wiki.objectcache USING btree (exptime);


--
-- Name: fa_actor_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX fa_actor_timestamp ON wiki.filearchive USING btree (fa_actor, fa_timestamp);


--
-- Name: fa_deleted_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX fa_deleted_timestamp ON wiki.filearchive USING btree (fa_deleted_timestamp);


--
-- Name: fa_name; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX fa_name ON wiki.filearchive USING btree (fa_name, fa_timestamp);


--
-- Name: fa_sha1; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX fa_sha1 ON wiki.filearchive USING btree (fa_sha1);


--
-- Name: fa_storage_group; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX fa_storage_group ON wiki.filearchive USING btree (fa_storage_group, fa_storage_key);


--
-- Name: il_backlinks_namespace; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX il_backlinks_namespace ON wiki.imagelinks USING btree (il_from_namespace, il_to, il_from);


--
-- Name: il_to; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX il_to ON wiki.imagelinks USING btree (il_to, il_from);


--
-- Name: img_actor_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX img_actor_timestamp ON wiki.image USING btree (img_actor, img_timestamp);


--
-- Name: img_media_mime; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX img_media_mime ON wiki.image USING btree (img_media_type, img_major_mime, img_minor_mime);


--
-- Name: img_sha1; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX img_sha1 ON wiki.image USING btree (img_sha1);


--
-- Name: img_size; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX img_size ON wiki.image USING btree (img_size);


--
-- Name: img_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX img_timestamp ON wiki.image USING btree (img_timestamp);


--
-- Name: ipb_address_unique; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX ipb_address_unique ON wiki.ipblocks USING btree (ipb_address, ipb_user, ipb_auto);


--
-- Name: ipb_expiry; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ipb_expiry ON wiki.ipblocks USING btree (ipb_expiry);


--
-- Name: ipb_parent_block_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ipb_parent_block_id ON wiki.ipblocks USING btree (ipb_parent_block_id);


--
-- Name: ipb_range; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ipb_range ON wiki.ipblocks USING btree (ipb_range_start, ipb_range_end);


--
-- Name: ipb_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ipb_timestamp ON wiki.ipblocks USING btree (ipb_timestamp);


--
-- Name: ipb_user; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ipb_user ON wiki.ipblocks USING btree (ipb_user);


--
-- Name: ipc_hex_time; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ipc_hex_time ON wiki.ip_changes USING btree (ipc_hex, ipc_rev_timestamp);


--
-- Name: ipc_rev_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ipc_rev_timestamp ON wiki.ip_changes USING btree (ipc_rev_timestamp);


--
-- Name: ir_type_value; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ir_type_value ON wiki.ipblocks_restrictions USING btree (ir_type, ir_value);


--
-- Name: iwl_prefix_from_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX iwl_prefix_from_title ON wiki.iwlinks USING btree (iwl_prefix, iwl_from, iwl_title);


--
-- Name: iwl_prefix_title_from; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX iwl_prefix_title_from ON wiki.iwlinks USING btree (iwl_prefix, iwl_title, iwl_from);


--
-- Name: job_cmd; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX job_cmd ON wiki.job USING btree (job_cmd, job_namespace, job_title, job_params);


--
-- Name: job_cmd_token; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX job_cmd_token ON wiki.job USING btree (job_cmd, job_token, job_random);


--
-- Name: job_cmd_token_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX job_cmd_token_id ON wiki.job USING btree (job_cmd, job_token, job_id);


--
-- Name: job_sha1; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX job_sha1 ON wiki.job USING btree (job_sha1);


--
-- Name: job_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX job_timestamp ON wiki.job USING btree (job_timestamp);


--
-- Name: ll_lang; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ll_lang ON wiki.langlinks USING btree (ll_lang, ll_title);


--
-- Name: log_actor_time; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX log_actor_time ON wiki.logging USING btree (log_actor, log_timestamp);


--
-- Name: log_actor_type_time; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX log_actor_type_time ON wiki.logging USING btree (log_actor, log_type, log_timestamp);


--
-- Name: log_page_id_time; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX log_page_id_time ON wiki.logging USING btree (log_page, log_timestamp);


--
-- Name: log_page_time; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX log_page_time ON wiki.logging USING btree (log_namespace, log_title, log_timestamp);


--
-- Name: log_times; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX log_times ON wiki.logging USING btree (log_timestamp);


--
-- Name: log_type_action; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX log_type_action ON wiki.logging USING btree (log_type, log_action, log_timestamp);


--
-- Name: log_type_time; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX log_type_time ON wiki.logging USING btree (log_type, log_timestamp);


--
-- Name: ls_log_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ls_log_id ON wiki.log_search USING btree (ls_log_id);


--
-- Name: lt_namespace_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX lt_namespace_title ON wiki.linktarget USING btree (lt_namespace, lt_title);


--
-- Name: model_name; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX model_name ON wiki.content_models USING btree (model_name);


--
-- Name: oi_actor_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX oi_actor_timestamp ON wiki.oldimage USING btree (oi_actor, oi_timestamp);


--
-- Name: oi_name_archive_name; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX oi_name_archive_name ON wiki.oldimage USING btree (oi_name, oi_archive_name);


--
-- Name: oi_name_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX oi_name_timestamp ON wiki.oldimage USING btree (oi_name, oi_timestamp);


--
-- Name: oi_sha1; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX oi_sha1 ON wiki.oldimage USING btree (oi_sha1);


--
-- Name: oi_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX oi_timestamp ON wiki.oldimage USING btree (oi_timestamp);


--
-- Name: page_len; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX page_len ON wiki.page USING btree (page_len);


--
-- Name: page_name_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX page_name_title ON wiki.page USING btree (page_namespace, page_title);


--
-- Name: page_random; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX page_random ON wiki.page USING btree (page_random);


--
-- Name: page_redirect_namespace_len; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX page_redirect_namespace_len ON wiki.page USING btree (page_is_redirect, page_namespace, page_len);


--
-- Name: pl_backlinks_namespace; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX pl_backlinks_namespace ON wiki.pagelinks USING btree (pl_from_namespace, pl_namespace, pl_title, pl_from);


--
-- Name: pl_namespace; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX pl_namespace ON wiki.pagelinks USING btree (pl_namespace, pl_title, pl_from);


--
-- Name: pp_propname_page; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX pp_propname_page ON wiki.page_props USING btree (pp_propname, pp_page);


--
-- Name: pp_propname_sortkey_page; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX pp_propname_sortkey_page ON wiki.page_props USING btree (pp_propname, pp_sortkey, pp_page) WHERE (pp_sortkey IS NOT NULL);


--
-- Name: pr_cascade; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX pr_cascade ON wiki.page_restrictions USING btree (pr_cascade);


--
-- Name: pr_level; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX pr_level ON wiki.page_restrictions USING btree (pr_level);


--
-- Name: pr_pagetype; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX pr_pagetype ON wiki.page_restrictions USING btree (pr_page, pr_type);


--
-- Name: pr_typelevel; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX pr_typelevel ON wiki.page_restrictions USING btree (pr_type, pr_level);


--
-- Name: pt_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX pt_timestamp ON wiki.protected_titles USING btree (pt_timestamp);


--
-- Name: qc_type; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX qc_type ON wiki.querycache USING btree (qc_type, qc_value);


--
-- Name: qcc_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX qcc_title ON wiki.querycachetwo USING btree (qcc_type, qcc_namespace, qcc_title);


--
-- Name: qcc_titletwo; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX qcc_titletwo ON wiki.querycachetwo USING btree (qcc_type, qcc_namespacetwo, qcc_titletwo);


--
-- Name: qcc_type; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX qcc_type ON wiki.querycachetwo USING btree (qcc_type, qcc_value);


--
-- Name: rc_actor; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_actor ON wiki.recentchanges USING btree (rc_actor, rc_timestamp);


--
-- Name: rc_cur_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_cur_id ON wiki.recentchanges USING btree (rc_cur_id);


--
-- Name: rc_ip; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_ip ON wiki.recentchanges USING btree (rc_ip);


--
-- Name: rc_name_type_patrolled_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_name_type_patrolled_timestamp ON wiki.recentchanges USING btree (rc_namespace, rc_type, rc_patrolled, rc_timestamp);


--
-- Name: rc_namespace_title_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_namespace_title_timestamp ON wiki.recentchanges USING btree (rc_namespace, rc_title, rc_timestamp);


--
-- Name: rc_new_name_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_new_name_timestamp ON wiki.recentchanges USING btree (rc_new, rc_namespace, rc_timestamp);


--
-- Name: rc_ns_actor; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_ns_actor ON wiki.recentchanges USING btree (rc_namespace, rc_actor);


--
-- Name: rc_this_oldid; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_this_oldid ON wiki.recentchanges USING btree (rc_this_oldid);


--
-- Name: rc_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rc_timestamp ON wiki.recentchanges USING btree (rc_timestamp);


--
-- Name: rd_ns_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rd_ns_title ON wiki.redirect USING btree (rd_namespace, rd_title, rd_from);


--
-- Name: rev_actor_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rev_actor_timestamp ON wiki.revision USING btree (rev_actor, rev_timestamp, rev_id);


--
-- Name: rev_page_actor_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rev_page_actor_timestamp ON wiki.revision USING btree (rev_page, rev_actor, rev_timestamp);


--
-- Name: rev_page_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rev_page_timestamp ON wiki.revision USING btree (rev_page, rev_timestamp);


--
-- Name: rev_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX rev_timestamp ON wiki.revision USING btree (rev_timestamp);


--
-- Name: revcomment_rev; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX revcomment_rev ON wiki.revision_comment_temp USING btree (revcomment_rev);


--
-- Name: role_name; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX role_name ON wiki.slot_roles USING btree (role_name);


--
-- Name: si_key; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX si_key ON wiki.site_identifiers USING btree (si_key);


--
-- Name: si_page; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX si_page ON wiki.searchindex USING btree (si_page);


--
-- Name: si_site; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX si_site ON wiki.site_identifiers USING btree (si_site);


--
-- Name: si_text; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX si_text ON wiki.searchindex USING btree (si_text);


--
-- Name: si_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX si_title ON wiki.searchindex USING btree (si_title);


--
-- Name: site_domain; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX site_domain ON wiki.sites USING btree (site_domain);


--
-- Name: site_forward; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX site_forward ON wiki.sites USING btree (site_forward);


--
-- Name: site_global_key; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX site_global_key ON wiki.sites USING btree (site_global_key);


--
-- Name: site_group; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX site_group ON wiki.sites USING btree (site_group);


--
-- Name: site_language; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX site_language ON wiki.sites USING btree (site_language);


--
-- Name: site_protocol; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX site_protocol ON wiki.sites USING btree (site_protocol);


--
-- Name: site_source; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX site_source ON wiki.sites USING btree (site_source);


--
-- Name: site_type; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX site_type ON wiki.sites USING btree (site_type);


--
-- Name: slot_revision_origin_role; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX slot_revision_origin_role ON wiki.slots USING btree (slot_revision_id, slot_origin, slot_role_id);


--
-- Name: tl_backlinks_namespace_target_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX tl_backlinks_namespace_target_id ON wiki.templatelinks USING btree (tl_from_namespace, tl_target_id, tl_from);


--
-- Name: tl_target_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX tl_target_id ON wiki.templatelinks USING btree (tl_target_id, tl_from);


--
-- Name: ts2_page_text; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ts2_page_text ON wiki.text USING gin (textvector);


--
-- Name: ts2_page_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ts2_page_title ON wiki.page USING gin (titlevector);


--
-- Name: ug_expiry; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ug_expiry ON wiki.user_groups USING btree (ug_expiry);


--
-- Name: ug_group; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX ug_group ON wiki.user_groups USING btree (ug_group);


--
-- Name: un_user_id; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX un_user_id ON wiki.user_newtalk USING btree (user_id);


--
-- Name: un_user_ip; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX un_user_ip ON wiki.user_newtalk USING btree (user_ip);


--
-- Name: up_property; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX up_property ON wiki.user_properties USING btree (up_property);


--
-- Name: us_key; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX us_key ON wiki.uploadstash USING btree (us_key);


--
-- Name: us_timestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX us_timestamp ON wiki.uploadstash USING btree (us_timestamp);


--
-- Name: us_user; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX us_user ON wiki.uploadstash USING btree (us_user);


--
-- Name: user_email; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX user_email ON wiki."user" USING btree (user_email);


--
-- Name: user_email_token; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX user_email_token ON wiki."user" USING btree (user_email_token);


--
-- Name: user_name; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX user_name ON wiki."user" USING btree (user_name);


--
-- Name: we_expiry; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX we_expiry ON wiki.watchlist_expiry USING btree (we_expiry);


--
-- Name: wl_namespace_title; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX wl_namespace_title ON wiki.watchlist USING btree (wl_namespace, wl_title);


--
-- Name: wl_user; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE UNIQUE INDEX wl_user ON wiki.watchlist USING btree (wl_user, wl_namespace, wl_title);


--
-- Name: wl_user_notificationtimestamp; Type: INDEX; Schema: wiki; Owner: msea_admin
--

CREATE INDEX wl_user_notificationtimestamp ON wiki.watchlist USING btree (wl_user, wl_notificationtimestamp);


--
-- Name: geoip_city_blocks t_geoip_update_geomes; Type: TRIGGER; Schema: maxmind; Owner: msea_admin
--

CREATE TRIGGER t_geoip_update_geomes AFTER INSERT ON maxmind.geoip_city_blocks FOR EACH ROW EXECUTE FUNCTION public.f_geoip_update_geoms();


--
-- Name: cruise ndst_cruise_updated_on; Type: TRIGGER; Schema: ndst; Owner: msea_admin
--

CREATE TRIGGER ndst_cruise_updated_on BEFORE UPDATE ON ndst.cruise FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: diveconfig ndst_diveconfig_updated_on; Type: TRIGGER; Schema: ndst; Owner: msea_admin
--

CREATE TRIGGER ndst_diveconfig_updated_on BEFORE UPDATE ON ndst.diveconfig FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: dives ndst_dives_updated_on; Type: TRIGGER; Schema: ndst; Owner: msea_admin
--

CREATE TRIGGER ndst_dives_updated_on BEFORE UPDATE ON ndst.dives FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: equipconfig ndst_equipconfig_updated_on; Type: TRIGGER; Schema: ndst; Owner: msea_admin
--

CREATE TRIGGER ndst_equipconfig_updated_on BEFORE UPDATE ON ndst.equipconfig FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: equipment ndst_equipment_updated_on; Type: TRIGGER; Schema: ndst; Owner: msea_admin
--

CREATE TRIGGER ndst_equipment_updated_on BEFORE UPDATE ON ndst.equipment FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: people ndst_people_updated_on; Type: TRIGGER; Schema: ndst; Owner: msea_admin
--

CREATE TRIGGER ndst_people_updated_on BEFORE UPDATE ON ndst.people FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: transects ndst_transects_updated_on; Type: TRIGGER; Schema: ndst; Owner: msea_admin
--

CREATE TRIGGER ndst_transects_updated_on BEFORE UPDATE ON ndst.transects FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: mpa_data_object mpa_data_object_updated_on; Type: TRIGGER; Schema: pa; Owner: msea_admin
--

CREATE TRIGGER mpa_data_object_updated_on BEFORE UPDATE ON pa.mpa_data_object FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: protected_area_data_object protected_area_data_object_updated_on; Type: TRIGGER; Schema: pa; Owner: msea_admin
--

CREATE TRIGGER protected_area_data_object_updated_on BEFORE UPDATE ON pa.protected_area_data_object FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: annotation_protocol_document annotation_protocol_document_updated; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER annotation_protocol_document_updated BEFORE UPDATE ON rov.annotation_protocol_document FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: annotation_protocol annotation_protocol_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER annotation_protocol_updated_on BEFORE UPDATE ON rov.annotation_protocol FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();

--
-- Name: cruise_document cruise_document_updated; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER cruise_document_updated BEFORE UPDATE ON rov.cruise_document FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: cruise cruise_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER cruise_updated_on BEFORE UPDATE ON rov.cruise FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: dive dive_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER dive_updated_on BEFORE UPDATE ON rov.dive FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: import_queue_annotator import_queue_annotation_updated; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER import_queue_annotation_updated BEFORE UPDATE ON rov.import_queue_annotator FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: import_queue_pi import_queue_pi_updated; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER import_queue_pi_updated BEFORE UPDATE ON rov.import_queue_pi FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: instrument_config instrument_config_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER instrument_config_updated_on BEFORE UPDATE ON rov.instrument_config FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: instrument instrument_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER instrument_updated_on BEFORE UPDATE ON rov.instrument FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: import_queue_annotator_label_map_prefill iqa_label_map_prefill_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER iqa_label_map_prefill_updated_on BEFORE UPDATE ON rov.import_queue_annotator_label_map_prefill FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: platform_config platform_config_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER platform_config_updated_on BEFORE UPDATE ON rov.platform_config FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: platform platform_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER platform_updated_on BEFORE UPDATE ON rov.platform FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: program program_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER program_updated_on BEFORE UPDATE ON rov.program FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: annotation_job rov_annotation_job_updated_on; Type: TRIGGER; Schema: rov; Owner: msea_admin
--

CREATE TRIGGER rov_annotation_job_updated_on BEFORE UPDATE ON rov.annotation_job FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: db_version db_version_updated_on; Type: TRIGGER; Schema: shared; Owner: msea_admin
--

CREATE TRIGGER db_version_updated_on BEFORE UPDATE ON shared.db_version FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: file file_updated_on; Type: TRIGGER; Schema: shared; Owner: msea_admin
--

CREATE TRIGGER file_updated_on BEFORE UPDATE ON shared.file FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: library library_updated_on; Type: TRIGGER; Schema: shared; Owner: msea_admin
--

CREATE TRIGGER library_updated_on BEFORE UPDATE ON shared.library FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: user_restriction mseauser_restriction_updated_on; Type: TRIGGER; Schema: shared; Owner: msea_admin
--

CREATE TRIGGER mseauser_restriction_updated_on BEFORE UPDATE ON shared.user_restriction FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: restriction restriction_updated_on; Type: TRIGGER; Schema: shared; Owner: msea_admin
--

CREATE TRIGGER restriction_updated_on BEFORE UPDATE ON shared.restriction FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: spatial_library spatial_library_updated_on; Type: TRIGGER; Schema: shared; Owner: msea_admin
--

CREATE TRIGGER spatial_library_updated_on BEFORE UPDATE ON shared.spatial_library FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: taxon_restriction taxon_restriction_updated_on; Type: TRIGGER; Schema: shared; Owner: msea_admin
--

CREATE TRIGGER taxon_restriction_updated_on BEFORE UPDATE ON shared.taxon_restriction FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();


--
-- Name: text ts2_page_text; Type: TRIGGER; Schema: wiki; Owner: msea_admin
--

CREATE TRIGGER ts2_page_text BEFORE INSERT OR UPDATE ON wiki.text FOR EACH ROW EXECUTE FUNCTION wiki.ts2_page_text();


--
-- Name: page ts2_page_title; Type: TRIGGER; Schema: wiki; Owner: msea_admin
--

CREATE TRIGGER ts2_page_title BEFORE INSERT OR UPDATE ON wiki.page FOR EACH ROW EXECUTE FUNCTION wiki.ts2_page_title();


--
-- Name: survey_crew fk_person_survey_crew; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_crew
    ADD CONSTRAINT fk_person_survey_crew FOREIGN KEY (person_id) REFERENCES shared.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: survey_taxon fk_quadrat_observation_survey_taxon; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_taxon
    ADD CONSTRAINT fk_quadrat_observation_survey_taxon FOREIGN KEY (survey_id) REFERENCES intertidal.quadrat_observation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: quadrat_observation fk_quadrat_quadrat_observation; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_observation
    ADD CONSTRAINT fk_quadrat_quadrat_observation FOREIGN KEY (quadrat_id) REFERENCES intertidal.quadrat(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: quadrat_substrate_coverage fk_quadrat_quadrat_substrate_coverage; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_substrate_coverage
    ADD CONSTRAINT fk_quadrat_quadrat_substrate_coverage FOREIGN KEY (quadrat_id) REFERENCES intertidal.quadrat(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: quadrat_substrate_coverage fk_quadrat_substrate_quadtrate_substrate_coverage; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat_substrate_coverage
    ADD CONSTRAINT fk_quadrat_substrate_quadtrate_substrate_coverage FOREIGN KEY (substrate_id) REFERENCES intertidal.quadrat_substrate(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: survey fk_site_survey; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey
    ADD CONSTRAINT fk_site_survey FOREIGN KEY (site_id) REFERENCES shared.site(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: survey_crew fk_survey_crew_survey_role; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_crew
    ADD CONSTRAINT fk_survey_crew_survey_role FOREIGN KEY (role_id) REFERENCES intertidal.survey_role(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: quadrat fk_survey_quadrat; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.quadrat
    ADD CONSTRAINT fk_survey_quadrat FOREIGN KEY (survey_id) REFERENCES intertidal.survey(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: survey_crew fk_survey_survey_crew; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_crew
    ADD CONSTRAINT fk_survey_survey_crew FOREIGN KEY (survey_id) REFERENCES intertidal.survey(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: survey_taxon fk_taxon_survey_taxon; Type: FK CONSTRAINT; Schema: intertidal; Owner: msea_admin
--

ALTER TABLE ONLY intertidal.survey_taxon
    ADD CONSTRAINT fk_taxon_survey_taxon FOREIGN KEY (taxon_id) REFERENCES shared.taxon(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: geoip_city_blocks geoip_city_blocks_geoname_id_fkey; Type: FK CONSTRAINT; Schema: maxmind; Owner: msea_admin
--

ALTER TABLE ONLY maxmind.geoip_city_blocks
    ADD CONSTRAINT geoip_city_blocks_geoname_id_fkey FOREIGN KEY (geoname_id) REFERENCES maxmind.geoip_city_locations(geoname_id);


--
-- Name: geoip_city_blocks geoip_city_blocks_registered_country_geoname_id_fkey; Type: FK CONSTRAINT; Schema: maxmind; Owner: msea_admin
--

ALTER TABLE ONLY maxmind.geoip_city_blocks
    ADD CONSTRAINT geoip_city_blocks_registered_country_geoname_id_fkey FOREIGN KEY (registered_country_geoname_id) REFERENCES maxmind.geoip_city_locations(geoname_id);


--
-- Name: geoip_city_blocks geoip_city_blocks_represented_country_geoname_id_fkey; Type: FK CONSTRAINT; Schema: maxmind; Owner: msea_admin
--

ALTER TABLE ONLY maxmind.geoip_city_blocks
    ADD CONSTRAINT geoip_city_blocks_represented_country_geoname_id_fkey FOREIGN KEY (represented_country_geoname_id) REFERENCES maxmind.geoip_city_locations(geoname_id);


--
-- Name: protected_area_data_object_file fk_data_object_data_object_file; Type: FK CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area_data_object_file
    ADD CONSTRAINT fk_data_object_data_object_file FOREIGN KEY (data_object_id) REFERENCES pa.protected_area_data_object(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mpa_data_object_file fk_data_object_data_object_file; Type: FK CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa_data_object_file
    ADD CONSTRAINT fk_data_object_data_object_file FOREIGN KEY (data_object_id) REFERENCES pa.mpa_data_object(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: protected_area_data_object_file fk_file_data_object_file; Type: FK CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area_data_object_file
    ADD CONSTRAINT fk_file_data_object_file FOREIGN KEY (file_id) REFERENCES shared.file(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mpa_data_object_file fk_file_data_object_file; Type: FK CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa_data_object_file
    ADD CONSTRAINT fk_file_data_object_file FOREIGN KEY (file_id) REFERENCES shared.file(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mpa_data_object fk_mpa_data_object; Type: FK CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.mpa_data_object
    ADD CONSTRAINT fk_mpa_data_object FOREIGN KEY (mpa_id) REFERENCES pa.mpa(objectid) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: protected_area_data_object fk_protected_area_data_object_protected_area; Type: FK CONSTRAINT; Schema: pa; Owner: msea_admin
--

ALTER TABLE ONLY pa.protected_area_data_object
    ADD CONSTRAINT fk_protected_area_data_object_protected_area FOREIGN KEY (protected_area_id) REFERENCES pa.protected_area(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: msea
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: knox_authtoken knox_authtoken_user_id_e5a5d899_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.knox_authtoken
    ADD CONSTRAINT knox_authtoken_user_id_e5a5d899_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nyt_notification nyt_notification_subscription_id_5a132ae1_fk_nyt_subsc; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_notification
    ADD CONSTRAINT nyt_notification_subscription_id_5a132ae1_fk_nyt_subsc FOREIGN KEY (subscription_id) REFERENCES public.nyt_subscription(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nyt_notification nyt_notification_user_id_acbb5c10_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_notification
    ADD CONSTRAINT nyt_notification_user_id_acbb5c10_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nyt_notificationtype nyt_notificationtype_content_type_id_18800dea_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_notificationtype
    ADD CONSTRAINT nyt_notificationtype_content_type_id_18800dea_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nyt_settings nyt_settings_user_id_1fad6d98_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_settings
    ADD CONSTRAINT nyt_settings_user_id_1fad6d98_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nyt_subscription nyt_subscription_latest_id_bbb7d98b_fk_nyt_notification_id; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_subscription
    ADD CONSTRAINT nyt_subscription_latest_id_bbb7d98b_fk_nyt_notification_id FOREIGN KEY (latest_id) REFERENCES public.nyt_notification(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nyt_subscription nyt_subscription_notification_type_id_ca8af379_fk_nyt_notif; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_subscription
    ADD CONSTRAINT nyt_subscription_notification_type_id_ca8af379_fk_nyt_notif FOREIGN KEY (notification_type_id) REFERENCES public.nyt_notificationtype(key) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: nyt_subscription nyt_subscription_settings_id_761bae06_fk_nyt_settings_id; Type: FK CONSTRAINT; Schema: public; Owner: msea_admin
--

ALTER TABLE ONLY public.nyt_subscription
    ADD CONSTRAINT nyt_subscription_settings_id_761bae06_fk_nyt_settings_id FOREIGN KEY (settings_id) REFERENCES public.nyt_settings(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: annotation_job_annotation_protocol annotation_job_annotation_protocol_annotation_job_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_annotation_protocol
    ADD CONSTRAINT annotation_job_annotation_protocol_annotation_job_id_fkey FOREIGN KEY (annotation_job_id) REFERENCES rov.annotation_job(id);


--
-- Name: annotation_job_annotation_protocol annotation_job_annotation_protocol_annotation_protocol_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_annotation_protocol
    ADD CONSTRAINT annotation_job_annotation_protocol_annotation_protocol_id_fkey FOREIGN KEY (annotation_protocol_id) REFERENCES rov.annotation_protocol(id);


--
-- Name: annotation_job_crew annotation_job_crew_annotation_job_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_crew
    ADD CONSTRAINT annotation_job_crew_annotation_job_id_fkey FOREIGN KEY (annotation_job_id) REFERENCES rov.annotation_job(id);


--
-- Name: annotation_job_crew annotation_job_crew_person_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_crew
    ADD CONSTRAINT annotation_job_crew_person_id_fkey FOREIGN KEY (person_id) REFERENCES shared.person(id);


--
-- Name: annotation_job_crew annotation_job_crew_role_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_job_crew
    ADD CONSTRAINT annotation_job_crew_role_id_fkey FOREIGN KEY (role_id) REFERENCES rov.annotation_job_role(id);


--
-- Name: habitat_event biocover_coverage_habitat_event_idx; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT biocover_coverage_habitat_event_idx FOREIGN KEY (biocover_coverage_id) REFERENCES rov.coverage(id);


--
-- Name: cruise_document cruise_document_uploaded_file_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_document
    ADD CONSTRAINT cruise_document_uploaded_file_id_fkey FOREIGN KEY (uploaded_file_id) REFERENCES shared.uploaded_file(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: event event_annotation_job_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT event_annotation_job_id_fkey FOREIGN KEY (annotation_job_id) REFERENCES rov.annotation_job(id);


--
-- Name: event event_image_quality_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT event_image_quality_id_fkey FOREIGN KEY (image_quality_id) REFERENCES rov.image_quality(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: event event_instrument_config_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT event_instrument_config_id_fkey FOREIGN KEY (instrument_config_id) REFERENCES rov.instrument_config(id);


--
-- Name: event event_protocol_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT event_protocol_id_fkey FOREIGN KEY (protocol_id) REFERENCES rov.protocol(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: event event_survey_mode_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT event_survey_mode_id_fkey FOREIGN KEY (survey_mode_id) REFERENCES rov.survey_mode(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: cruise_crew fc_cruise_cruise_crew; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_crew
    ADD CONSTRAINT fc_cruise_cruise_crew FOREIGN KEY (cruise_id) REFERENCES rov.cruise(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: annotation_protocol fk_annotation_protocol_annotation_software; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol
    ADD CONSTRAINT fk_annotation_protocol_annotation_software FOREIGN KEY (annotation_software_id) REFERENCES rov.annotation_software(id);


--
-- Name: annotation_protocol fk_annotation_protocol_creator; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol
    ADD CONSTRAINT fk_annotation_protocol_creator FOREIGN KEY (creator_id) REFERENCES shared.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: annotation_protocol_document fk_annotation_protocol_document_annotation_protocol; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol_document
    ADD CONSTRAINT fk_annotation_protocol_document_annotation_protocol FOREIGN KEY (annotation_protocol_id) REFERENCES rov.annotation_protocol(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: annotation_protocol fk_annotation_protocol_medium_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.annotation_protocol
    ADD CONSTRAINT fk_annotation_protocol_medium_type FOREIGN KEY (medium_type_id) REFERENCES rov.medium_type(id);


--
-- Name: habitat_event fk_biocover_habitat_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT fk_biocover_habitat_event FOREIGN KEY (biocover_id) REFERENCES rov.biocover(id);


--
-- Name: habitat_event fk_complexity_habitat_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT fk_complexity_habitat_event FOREIGN KEY (complexity_id) REFERENCES rov.complexity(id);


--
-- Name: cruise_crew fk_cruise_crew_cruise_role; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_crew
    ADD CONSTRAINT fk_cruise_crew_cruise_role FOREIGN KEY (cruise_role_id) REFERENCES rov.cruise_role(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: weather_observation fk_cruise_crew_weather_observation; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.weather_observation
    ADD CONSTRAINT fk_cruise_crew_weather_observation FOREIGN KEY (cruise_crew_id) REFERENCES rov.cruise_crew(id);


--
-- Name: cruise_library fk_cruise_cruise_library; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_library
    ADD CONSTRAINT fk_cruise_cruise_library FOREIGN KEY (cruiseleg_id) REFERENCES rov.cruise(id);


--
-- Name: dive fk_cruise_dive; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive
    ADD CONSTRAINT fk_cruise_dive FOREIGN KEY (cruise_id) REFERENCES rov.cruise(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cruise_document fk_cruise_document_cruise; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_document
    ADD CONSTRAINT fk_cruise_document_cruise FOREIGN KEY (cruise_id) REFERENCES rov.cruise(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cruise_fn_contact fk_cruise_fn_contact_cruise; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_fn_contact
    ADD CONSTRAINT fk_cruise_fn_contact_cruise FOREIGN KEY (cruise_id) REFERENCES rov.cruise(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cruise_program fk_cruise_program_cruise_id; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_program
    ADD CONSTRAINT fk_cruise_program_cruise_id FOREIGN KEY (cruise_id) REFERENCES rov.cruise(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cruise_program fk_cruise_program_program_id; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_program
    ADD CONSTRAINT fk_cruise_program_program_id FOREIGN KEY (program_id) REFERENCES rov.program(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cruise fk_cruise_ship; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise
    ADD CONSTRAINT fk_cruise_ship FOREIGN KEY (ship_id) REFERENCES rov.platform(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: habitat_event fk_disturbance_habitat_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT fk_disturbance_habitat_event FOREIGN KEY (disturbance_id) REFERENCES rov.disturbance(id);


--
-- Name: dive_crew fk_dive_crew_dive_role; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_crew
    ADD CONSTRAINT fk_dive_crew_dive_role FOREIGN KEY (dive_role_id) REFERENCES rov.dive_role(id);


--
-- Name: dive_crew fk_dive_dive_crew; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_crew
    ADD CONSTRAINT fk_dive_dive_crew FOREIGN KEY (dive_id) REFERENCES rov.dive(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: event fk_dive_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT fk_dive_event FOREIGN KEY (dive_id) REFERENCES rov.dive(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dive fk_dive_ship_config; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive
    ADD CONSTRAINT fk_dive_ship_config FOREIGN KEY (ship_config_id) REFERENCES rov.platform_config(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dive fk_dive_site; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive
    ADD CONSTRAINT fk_dive_site FOREIGN KEY (site_id) REFERENCES shared.site(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: dive fk_dive_sub_config; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive
    ADD CONSTRAINT fk_dive_sub_config FOREIGN KEY (sub_config_id) REFERENCES rov.platform_config(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transect fk_dive_transect; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.transect
    ADD CONSTRAINT fk_dive_transect FOREIGN KEY (dive_id) REFERENCES rov.dive(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: habitat_event fk_dominant_substrate_habitat_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT fk_dominant_substrate_habitat_event FOREIGN KEY (substrate_id) REFERENCES rov.substrate(id);


--
-- Name: model_equipment_type fk_equipment_type_model_equipment_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_equipment_type
    ADD CONSTRAINT fk_equipment_type_model_equipment_type FOREIGN KEY (equipment_type_id) REFERENCES rov.equipment_type(id) ON DELETE RESTRICT;


--
-- Name: comment_event fk_event_comment_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.comment_event
    ADD CONSTRAINT fk_event_comment_event FOREIGN KEY (event_id) REFERENCES rov.event(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: habitat_event fk_event_habitat_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT fk_event_habitat_event FOREIGN KEY (event_id) REFERENCES rov.event(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: event_logger fk_event_logger_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event_logger
    ADD CONSTRAINT fk_event_logger_event FOREIGN KEY (event_id) REFERENCES rov.event(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: event_logger fk_event_logger_person; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event_logger
    ADD CONSTRAINT fk_event_logger_person FOREIGN KEY (person_id) REFERENCES shared.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: observation_event fk_event_observation_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_event
    ADD CONSTRAINT fk_event_observation_event FOREIGN KEY (event_id) REFERENCES rov.event(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: status_event fk_event_status_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_event
    ADD CONSTRAINT fk_event_status_event FOREIGN KEY (event_id) REFERENCES rov.event(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: import_queue_pi fk_import_queue_pi_cruise; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.import_queue_pi
    ADD CONSTRAINT fk_import_queue_pi_cruise FOREIGN KEY (cruise_id) REFERENCES rov.cruise(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: instrument_config fk_instrument_config_instrument; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument_config
    ADD CONSTRAINT fk_instrument_config_instrument FOREIGN KEY (instrument_id) REFERENCES rov.instrument(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: instrument_config fk_instrument_config_platform_config; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument_config
    ADD CONSTRAINT fk_instrument_config_platform_config FOREIGN KEY (platform_config_id) REFERENCES rov.platform_config(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: instrument fk_instrument_model; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument
    ADD CONSTRAINT fk_instrument_model FOREIGN KEY (model_id) REFERENCES rov.model(id);


--
-- Name: instrument fk_instrument_organisation; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.instrument
    ADD CONSTRAINT fk_instrument_organisation FOREIGN KEY (organisation_id) REFERENCES shared.organisation(id);


--
-- Name: cruise_library fk_library_cruise_library; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_library
    ADD CONSTRAINT fk_library_cruise_library FOREIGN KEY (library_id) REFERENCES shared.library(id);


--
-- Name: model_documentation fk_library_model_documentation; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_documentation
    ADD CONSTRAINT fk_library_model_documentation FOREIGN KEY (library_id) REFERENCES shared.library(id);


--
-- Name: measurement_event fk_measurement_event_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement_event
    ADD CONSTRAINT fk_measurement_event_event FOREIGN KEY (event_id) REFERENCES rov.event(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: measurement_event fk_measurement_event_measurement_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement_event
    ADD CONSTRAINT fk_measurement_event_measurement_type FOREIGN KEY (measurement_type_id) REFERENCES rov.measurement_type(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: measurement fk_measurement_instrument_config; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement
    ADD CONSTRAINT fk_measurement_instrument_config FOREIGN KEY (instrument_config_id) REFERENCES rov.instrument_config(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: measurement fk_measurement_measurement_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.measurement
    ADD CONSTRAINT fk_measurement_measurement_type FOREIGN KEY (measurement_type_id) REFERENCES rov.measurement_type(id) ON DELETE RESTRICT;


--
-- Name: medium_format fk_medium_type_medium_format; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.medium_format
    ADD CONSTRAINT fk_medium_type_medium_format FOREIGN KEY (medium_type_id) REFERENCES rov.medium_type(id);


--
-- Name: model fk_model_equipment_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model
    ADD CONSTRAINT fk_model_equipment_type FOREIGN KEY (equipment_type_id) REFERENCES rov.equipment_type(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: model_documentation fk_model_model_documentation; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_documentation
    ADD CONSTRAINT fk_model_model_documentation FOREIGN KEY (model_id) REFERENCES rov.model(id);


--
-- Name: model_equipment_type fk_model_model_equipment_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.model_equipment_type
    ADD CONSTRAINT fk_model_model_equipment_type FOREIGN KEY (model_id) REFERENCES rov.model(id) ON DELETE RESTRICT;


--
-- Name: observation_event fk_observation_event_abundance; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_event
    ADD CONSTRAINT fk_observation_event_abundance FOREIGN KEY (abundance_id) REFERENCES rov.abundance(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: observation_event fk_observation_event_coverage; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_event
    ADD CONSTRAINT fk_observation_event_coverage FOREIGN KEY (coverage_id) REFERENCES rov.coverage(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: observation_event fk_observation_event_observation_confidence; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_event
    ADD CONSTRAINT fk_observation_event_observation_confidence FOREIGN KEY (observation_confidence_id) REFERENCES rov.observation_confidence(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: orientation fk_orientation_instrument_config; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.orientation
    ADD CONSTRAINT fk_orientation_instrument_config FOREIGN KEY (instrument_config_id) REFERENCES rov.instrument_config(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: orientation fk_orientation_orientation_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.orientation
    ADD CONSTRAINT fk_orientation_orientation_type FOREIGN KEY (orientation_type_id) REFERENCES rov.orientation_type(id);


--
-- Name: cruise_crew fk_person_cruise_crew; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.cruise_crew
    ADD CONSTRAINT fk_person_cruise_crew FOREIGN KEY (person_id) REFERENCES shared.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: dive_crew fk_person_dive_crew; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.dive_crew
    ADD CONSTRAINT fk_person_dive_crew FOREIGN KEY (person_id) REFERENCES shared.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: program_member fk_person_program_member; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_member
    ADD CONSTRAINT fk_person_program_member FOREIGN KEY (person_id) REFERENCES shared.person(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: platform fk_platform_model; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform
    ADD CONSTRAINT fk_platform_model FOREIGN KEY (model_id) REFERENCES rov.model(id) ON DELETE RESTRICT;


--
-- Name: platform fk_platform_organisation; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform
    ADD CONSTRAINT fk_platform_organisation FOREIGN KEY (organisation_id) REFERENCES shared.organisation(id);


--
-- Name: platform_config fk_platform_platform_config; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.platform_config
    ADD CONSTRAINT fk_platform_platform_config FOREIGN KEY (platform_id) REFERENCES rov.platform(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: position fk_position_instrument_config; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov."position"
    ADD CONSTRAINT fk_position_instrument_config FOREIGN KEY (instrument_config_id) REFERENCES rov.instrument_config(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: position fk_position_position_type; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov."position"
    ADD CONSTRAINT fk_position_position_type FOREIGN KEY (position_type_id) REFERENCES rov.position_type(id);


--
-- Name: program_library fk_program_library_library; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_library
    ADD CONSTRAINT fk_program_library_library FOREIGN KEY (library_id) REFERENCES shared.library(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: program_library fk_program_library_program; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_library
    ADD CONSTRAINT fk_program_library_program FOREIGN KEY (program_id) REFERENCES rov.program(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: program_member fk_program_member_program_role; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_member
    ADD CONSTRAINT fk_program_member_program_role FOREIGN KEY (role_id) REFERENCES rov.program_role(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: program_member fk_program_program_member; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.program_member
    ADD CONSTRAINT fk_program_program_member FOREIGN KEY (program_id) REFERENCES rov.program(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: habitat_event fk_relief_habitat_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT fk_relief_habitat_event FOREIGN KEY (relief_id) REFERENCES rov.relief(id);


--
-- Name: status_event fk_status_event_status_type_detail; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_event
    ADD CONSTRAINT fk_status_event_status_type_detail FOREIGN KEY (status_type_detail_id) REFERENCES rov.status_type_detail(id);


--
-- Name: status_type_detail fk_status_type_status_type_detail; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.status_type_detail
    ADD CONSTRAINT fk_status_type_status_type_detail FOREIGN KEY (status_type_id) REFERENCES rov.status_type(id);


--
-- Name: habitat_event fk_thickness_habitat_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT fk_thickness_habitat_event FOREIGN KEY (thickness_id) REFERENCES rov.thickness(id);


--
-- Name: event fk_transect_event; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.event
    ADD CONSTRAINT fk_transect_event FOREIGN KEY (transect_id) REFERENCES rov.transect(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: weather_observation fk_weather_observation_cruise; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.weather_observation
    ADD CONSTRAINT fk_weather_observation_cruise FOREIGN KEY (cruise_id) REFERENCES rov.cruise(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: habitat_event habitat_event_flow_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT habitat_event_flow_id_fkey FOREIGN KEY (flow_id) REFERENCES rov.flow(id);


--
-- Name: habitat_event habitat_event_observation_confidence_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT habitat_event_observation_confidence_id_fkey FOREIGN KEY (observation_confidence_id) REFERENCES rov.observation_confidence(id);


--
-- Name: habitat_event habitat_event_taxon_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT habitat_event_taxon_id_fkey FOREIGN KEY (taxon_id) REFERENCES shared.taxon(id);


--
-- Name: observation observation_annotation_job_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation
    ADD CONSTRAINT observation_annotation_job_id_fkey FOREIGN KEY (annotation_job_id) REFERENCES rov.annotation_job(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: observation observation_event_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation
    ADD CONSTRAINT observation_event_id_fkey FOREIGN KEY (event_id) REFERENCES rov.event(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: observation_event observation_event_taxon_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation_event
    ADD CONSTRAINT observation_event_taxon_id_fkey FOREIGN KEY (taxon_id) REFERENCES shared.taxon(id);


--
-- Name: observation observation_instrument_config_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation
    ADD CONSTRAINT observation_instrument_config_id_fkey FOREIGN KEY (instrument_config_id) REFERENCES rov.instrument_config(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: observation observation_reviewed_by_id_fkey; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.observation
    ADD CONSTRAINT observation_reviewed_by_id_fkey FOREIGN KEY (reviewed_by_id) REFERENCES shared.person(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: habitat_event substrate_coverage_habitat_event_idx; Type: FK CONSTRAINT; Schema: rov; Owner: msea_admin
--

ALTER TABLE ONLY rov.habitat_event
    ADD CONSTRAINT substrate_coverage_habitat_event_idx FOREIGN KEY (substrate_coverage_id) REFERENCES rov.coverage(id);


--
-- Name: file fk_file_shared_file; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.file
    ADD CONSTRAINT fk_file_shared_file FOREIGN KEY (file_type_id) REFERENCES shared.file_type(id);


--
-- Name: spatial_library_file fk_file_spatial_library_file; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.spatial_library_file
    ADD CONSTRAINT fk_file_spatial_library_file FOREIGN KEY (file_id) REFERENCES shared.file(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: library fk_library_file; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.library
    ADD CONSTRAINT fk_library_file FOREIGN KEY (file_id) REFERENCES shared.file(id);


--
-- Name: site fk_site_spatial_library; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.site
    ADD CONSTRAINT fk_site_spatial_library FOREIGN KEY (spatial_library_id) REFERENCES shared.spatial_library(id);


--
-- Name: spatial_library_file fk_spatial_library_spatial_library_file; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.spatial_library_file
    ADD CONSTRAINT fk_spatial_library_spatial_library_file FOREIGN KEY (spatial_library_id) REFERENCES shared.spatial_library(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: user_restriction mseauser_restriction_restriction; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.user_restriction
    ADD CONSTRAINT mseauser_restriction_restriction FOREIGN KEY (restriction_id) REFERENCES shared.restriction(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: taxon_restriction taxon_restriction_restriction; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.taxon_restriction
    ADD CONSTRAINT taxon_restriction_restriction FOREIGN KEY (restriction_id) REFERENCES shared.restriction(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: taxon_restriction taxon_restriction_taxon; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.taxon_restriction
    ADD CONSTRAINT taxon_restriction_taxon FOREIGN KEY (taxon_id) REFERENCES shared.taxon(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_restriction user_restriction; Type: FK CONSTRAINT; Schema: shared; Owner: msea_admin
--

ALTER TABLE ONLY shared.user_restriction
    ADD CONSTRAINT user_restriction FOREIGN KEY (user_id) REFERENCES public.auth_user(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: taxon restricted_admin_access; Type: POLICY; Schema: shared; Owner: msea_admin
--

CREATE POLICY restricted_admin_access ON shared.taxon TO msea_admin USING (true) WITH CHECK (true);


--
-- Name: taxon restricted_taxon_access; Type: POLICY; Schema: shared; Owner: msea_admin
--

CREATE POLICY restricted_taxon_access ON shared.taxon FOR SELECT USING (((NOT (EXISTS ( SELECT taxon_restriction.taxon_id,
    taxon_restriction.restriction_id,
    taxon_restriction.created_on,
    taxon_restriction.updated_on
   FROM shared.taxon_restriction
  WHERE (taxon_restriction.taxon_id = taxon.id)))) OR (EXISTS ( SELECT a.user_id AS mseauser_id,
    a.restriction_id,
    a.created_on,
    a.updated_on,
    b.id,
    b.user_id,
    b.biigle_username,
    b.biigle_api_key,
    b.pg_role,
    c.taxon_id,
    c.restriction_id,
    c.created_on,
    c.updated_on
   FROM ((shared.user_restriction a
     JOIN shared.mseauser b ON ((b.id = a.user_id)))
     JOIN shared.taxon_restriction c ON ((c.restriction_id = a.restriction_id)))
  WHERE (((b.pg_role)::text = CURRENT_USER) AND (c.taxon_id = taxon.id))))));


--
-- Name: SCHEMA iform; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA iform TO PUBLIC;
GRANT USAGE ON SCHEMA iform TO msea;


--
-- Name: SCHEMA intertidal; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA intertidal TO msea;
GRANT USAGE ON SCHEMA intertidal TO PUBLIC;


--
-- Name: SCHEMA maxmind; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA maxmind TO PUBLIC;
GRANT USAGE ON SCHEMA maxmind TO msea;


--
-- Name: SCHEMA ndst; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA ndst TO PUBLIC;
GRANT USAGE ON SCHEMA ndst TO msea;


--
-- Name: SCHEMA pa; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA pa TO msea;
GRANT USAGE ON SCHEMA pa TO PUBLIC;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: msea_admin
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: SCHEMA rov; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA rov TO msea;
GRANT USAGE ON SCHEMA rov TO PUBLIC;


--
-- Name: SCHEMA shared; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA shared TO msea;
GRANT USAGE ON SCHEMA shared TO PUBLIC;


--
-- Name: SCHEMA taxonomy; Type: ACL; Schema: -; Owner: msea_admin
--

GRANT USAGE ON SCHEMA taxonomy TO PUBLIC;
GRANT USAGE ON SCHEMA taxonomy TO msea;


--
-- Name: SCHEMA wiki; Type: ACL; Schema: -; Owner: msea
--

GRANT USAGE ON SCHEMA wiki TO PUBLIC;


--
-- Name: TABLE event_log_data; Type: ACL; Schema: iform; Owner: msea_admin
--

GRANT SELECT ON TABLE iform.event_log_data TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE iform.event_log_data TO msea;


--
-- Name: TABLE event_log; Type: ACL; Schema: iform; Owner: msea_admin
--

GRANT SELECT ON TABLE iform.event_log TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE iform.event_log TO msea;


--
-- Name: SEQUENCE event_log_data_id_seq; Type: ACL; Schema: iform; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE iform.event_log_data_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE iform.event_log_data_id_seq TO msea;


--
-- Name: TABLE quadrat; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.quadrat TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.quadrat TO msea;


--
-- Name: SEQUENCE quadrat_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.quadrat_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.quadrat_id_seq TO PUBLIC;


--
-- Name: TABLE quadrat_observation; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.quadrat_observation TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.quadrat_observation TO msea;


--
-- Name: SEQUENCE quadrat_observation_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.quadrat_observation_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.quadrat_observation_id_seq TO PUBLIC;


--
-- Name: TABLE quadrat_substrate; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.quadrat_substrate TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.quadrat_substrate TO msea;


--
-- Name: TABLE quadrat_substrate_coverage; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.quadrat_substrate_coverage TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.quadrat_substrate_coverage TO msea;


--
-- Name: SEQUENCE quadrat_substrate_coverage_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.quadrat_substrate_coverage_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.quadrat_substrate_coverage_id_seq TO PUBLIC;


--
-- Name: SEQUENCE quadrat_substrate_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.quadrat_substrate_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.quadrat_substrate_id_seq TO PUBLIC;


--
-- Name: TABLE survey; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.survey TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.survey TO msea;


--
-- Name: TABLE survey_crew; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.survey_crew TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.survey_crew TO msea;


--
-- Name: SEQUENCE survey_crew_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.survey_crew_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.survey_crew_id_seq TO PUBLIC;


--
-- Name: SEQUENCE survey_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.survey_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.survey_id_seq TO PUBLIC;


--
-- Name: TABLE survey_role; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.survey_role TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.survey_role TO msea;


--
-- Name: SEQUENCE survey_role_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.survey_role_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.survey_role_id_seq TO PUBLIC;


--
-- Name: TABLE survey_taxon; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT SELECT ON TABLE intertidal.survey_taxon TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE intertidal.survey_taxon TO msea;


--
-- Name: SEQUENCE survey_taxon_id_seq; Type: ACL; Schema: intertidal; Owner: msea_admin
--

GRANT ALL ON SEQUENCE intertidal.survey_taxon_id_seq TO msea;
GRANT USAGE ON SEQUENCE intertidal.survey_taxon_id_seq TO PUBLIC;


--
-- Name: TABLE geoip_city_blocks; Type: ACL; Schema: maxmind; Owner: msea_admin
--

GRANT SELECT ON TABLE maxmind.geoip_city_blocks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE maxmind.geoip_city_blocks TO msea;


--
-- Name: TABLE geoip_city_locations; Type: ACL; Schema: maxmind; Owner: msea_admin
--

GRANT SELECT ON TABLE maxmind.geoip_city_locations TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE maxmind.geoip_city_locations TO msea;


--
-- Name: TABLE geoip_last_update; Type: ACL; Schema: maxmind; Owner: msea_admin
--

GRANT SELECT ON TABLE maxmind.geoip_last_update TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE maxmind.geoip_last_update TO msea;


--
-- Name: TABLE cruise; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT SELECT ON TABLE ndst.cruise TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ndst.cruise TO msea;


--
-- Name: SEQUENCE cruise_id_seq; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE ndst.cruise_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE ndst.cruise_id_seq TO msea;


--
-- Name: TABLE diveconfig; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT SELECT ON TABLE ndst.diveconfig TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ndst.diveconfig TO msea;


--
-- Name: SEQUENCE diveconfig_id_seq; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE ndst.diveconfig_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE ndst.diveconfig_id_seq TO msea;


--
-- Name: TABLE dives; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT SELECT ON TABLE ndst.dives TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ndst.dives TO msea;


--
-- Name: SEQUENCE dives_id_seq; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE ndst.dives_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE ndst.dives_id_seq TO msea;


--
-- Name: TABLE equipconfig; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT SELECT ON TABLE ndst.equipconfig TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ndst.equipconfig TO msea;


--
-- Name: SEQUENCE equipconfig_id_seq; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE ndst.equipconfig_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE ndst.equipconfig_id_seq TO msea;


--
-- Name: TABLE equipment; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT SELECT ON TABLE ndst.equipment TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ndst.equipment TO msea;


--
-- Name: SEQUENCE equipment_id_seq; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE ndst.equipment_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE ndst.equipment_id_seq TO msea;


--
-- Name: TABLE people; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT SELECT ON TABLE ndst.people TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ndst.people TO msea;


--
-- Name: SEQUENCE people_id_seq; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE ndst.people_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE ndst.people_id_seq TO msea;


--
-- Name: TABLE transects; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT SELECT ON TABLE ndst.transects TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ndst.transects TO msea;


--
-- Name: SEQUENCE transects_id_seq; Type: ACL; Schema: ndst; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE ndst.transects_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE ndst.transects_id_seq TO msea;


--
-- Name: TABLE protected_area_data_object; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT SELECT ON TABLE pa.protected_area_data_object TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pa.protected_area_data_object TO msea;


--
-- Name: SEQUENCE data_object_id_seq; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT ALL ON SEQUENCE pa.data_object_id_seq TO msea;
GRANT USAGE ON SEQUENCE pa.data_object_id_seq TO PUBLIC;


--
-- Name: TABLE mpa; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT SELECT ON TABLE pa.mpa TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pa.mpa TO msea;


--
-- Name: TABLE mpa_data_object; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT SELECT ON TABLE pa.mpa_data_object TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pa.mpa_data_object TO msea;


--
-- Name: TABLE mpa_data_object_file; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT SELECT ON TABLE pa.mpa_data_object_file TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pa.mpa_data_object_file TO msea;


--
-- Name: SEQUENCE mpa_data_object_file_id_seq; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT ALL ON SEQUENCE pa.mpa_data_object_file_id_seq TO msea;
GRANT USAGE ON SEQUENCE pa.mpa_data_object_file_id_seq TO PUBLIC;


--
-- Name: SEQUENCE mpa_data_object_id_seq; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT ALL ON SEQUENCE pa.mpa_data_object_id_seq TO msea;
GRANT USAGE ON SEQUENCE pa.mpa_data_object_id_seq TO PUBLIC;


--
-- Name: SEQUENCE mpas_ogc_fid_seq; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT ALL ON SEQUENCE pa.mpas_ogc_fid_seq TO msea;
GRANT USAGE ON SEQUENCE pa.mpas_ogc_fid_seq TO PUBLIC;


--
-- Name: TABLE protected_area; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT SELECT ON TABLE pa.protected_area TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pa.protected_area TO msea;


--
-- Name: TABLE protected_area_data_object_file; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT SELECT ON TABLE pa.protected_area_data_object_file TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pa.protected_area_data_object_file TO msea;


--
-- Name: SEQUENCE protected_area_id_seq; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT ALL ON SEQUENCE pa.protected_area_id_seq TO msea;
GRANT USAGE ON SEQUENCE pa.protected_area_id_seq TO PUBLIC;


--
-- Name: TABLE rca; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT SELECT ON TABLE pa.rca TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pa.rca TO msea;


--
-- Name: SEQUENCE rca_id_seq; Type: ACL; Schema: pa; Owner: msea_admin
--

GRANT ALL ON SEQUENCE pa.rca_id_seq TO msea;
GRANT USAGE ON SEQUENCE pa.rca_id_seq TO PUBLIC;


--
-- Name: TABLE knox_authtoken; Type: ACL; Schema: public; Owner: msea_admin
--

GRANT ALL ON TABLE public.knox_authtoken TO msea;


--
-- Name: TABLE abundance; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.abundance TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.abundance TO msea;


--
-- Name: SEQUENCE abundance_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.abundance_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.abundance_id_seq TO PUBLIC;


--
-- Name: TABLE annotation_job; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.annotation_job TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.annotation_job TO msea;


--
-- Name: TABLE annotation_job_annotation_protocol; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.annotation_job_annotation_protocol TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.annotation_job_annotation_protocol TO msea;


--
-- Name: SEQUENCE annotation_job_annotation_protocol_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.annotation_job_annotation_protocol_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.annotation_job_annotation_protocol_id_seq TO msea;


--
-- Name: TABLE annotation_job_crew; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.annotation_job_crew TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.annotation_job_crew TO msea;


--
-- Name: SEQUENCE annotation_job_crew_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.annotation_job_crew_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.annotation_job_crew_id_seq TO msea;


--
-- Name: SEQUENCE annotation_job_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.annotation_job_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.annotation_job_id_seq TO msea;


--
-- Name: TABLE annotation_job_role; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.annotation_job_role TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.annotation_job_role TO msea;


--
-- Name: SEQUENCE annotation_job_role_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.annotation_job_role_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.annotation_job_role_id_seq TO msea;


--
-- Name: TABLE annotation_protocol; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.annotation_protocol TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.annotation_protocol TO msea;


--
-- Name: TABLE annotation_protocol_document; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.annotation_protocol_document TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.annotation_protocol_document TO msea;


--
-- Name: SEQUENCE annotation_protocol_document_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.annotation_protocol_document_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.annotation_protocol_document_id_seq TO msea;


--
-- Name: SEQUENCE annotation_protocol_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.annotation_protocol_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.annotation_protocol_id_seq TO PUBLIC;


--
-- Name: TABLE annotation_software; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.annotation_software TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.annotation_software TO msea;


--
-- Name: SEQUENCE annotation_software_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.annotation_software_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.annotation_software_id_seq TO PUBLIC;


--
-- Name: TABLE biocover; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.biocover TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.biocover TO msea;


--
-- Name: SEQUENCE biocover_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.biocover_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.biocover_id_seq TO PUBLIC;


--
-- Name: TABLE comment_event; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.comment_event TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.comment_event TO msea;


--
-- Name: TABLE complexity; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.complexity TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.complexity TO msea;


--
-- Name: SEQUENCE complexity_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.complexity_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.complexity_id_seq TO PUBLIC;


--
-- Name: TABLE coverage; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.coverage TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.coverage TO msea;


--
-- Name: SEQUENCE coverage_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.coverage_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.coverage_id_seq TO PUBLIC;


--
-- Name: TABLE cruise; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise TO msea;


--
-- Name: TABLE cruise_crew; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_crew TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_crew TO msea;


--
-- Name: TABLE cruise_document; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_document TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_document TO msea;


--
-- Name: SEQUENCE cruise_document_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.cruise_document_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.cruise_document_id_seq TO msea;


--
-- Name: TABLE cruise_fn_contact; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_fn_contact TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_fn_contact TO msea;


--
-- Name: SEQUENCE cruise_fn_contact_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.cruise_fn_contact_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.cruise_fn_contact_id_seq TO PUBLIC;


--
-- Name: TABLE cruise_import; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_import TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_import TO msea;


--
-- Name: SEQUENCE cruise_import_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.cruise_import_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.cruise_import_id_seq TO msea;


--
-- Name: SEQUENCE cruise_leg_crew_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.cruise_leg_crew_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.cruise_leg_crew_id_seq TO PUBLIC;


--
-- Name: SEQUENCE cruise_leg_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.cruise_leg_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.cruise_leg_id_seq TO PUBLIC;


--
-- Name: TABLE cruise_library; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_library TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_library TO msea;


--
-- Name: SEQUENCE cruise_leg_library_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.cruise_leg_library_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.cruise_leg_library_id_seq TO PUBLIC;


--
-- Name: TABLE cruise_role; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_role TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_role TO msea;


--
-- Name: SEQUENCE cruise_leg_role_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.cruise_leg_role_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.cruise_leg_role_id_seq TO PUBLIC;


--
-- Name: TABLE cruise_program; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_program TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_program TO msea;


--
-- Name: SEQUENCE cruise_program_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.cruise_program_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.cruise_program_id_seq TO PUBLIC;


--
-- Name: TABLE disturbance; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.disturbance TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.disturbance TO msea;


--
-- Name: SEQUENCE disturbance_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.disturbance_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.disturbance_id_seq TO PUBLIC;


--
-- Name: TABLE dive; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.dive TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.dive TO msea;


--
-- Name: TABLE dive_crew; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.dive_crew TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.dive_crew TO msea;


--
-- Name: SEQUENCE dive_crew_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.dive_crew_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.dive_crew_id_seq TO PUBLIC;


--
-- Name: SEQUENCE dive_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.dive_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.dive_id_seq TO PUBLIC;


--
-- Name: TABLE dive_role; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.dive_role TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.dive_role TO msea;


--
-- Name: SEQUENCE dive_role_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.dive_role_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.dive_role_id_seq TO PUBLIC;


--
-- Name: TABLE instrument_config; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.instrument_config TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.instrument_config TO msea;


--
-- Name: TABLE platform_config; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.platform_config TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.platform_config TO msea;


--
-- Name: TABLE "position"; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov."position" TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov."position" TO msea;


--
-- Name: TABLE dive_track; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.dive_track TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.dive_track TO msea;


--
-- Name: TABLE equipment_type; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.equipment_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.equipment_type TO msea;


--
-- Name: SEQUENCE equipment_type_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.equipment_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.equipment_type_id_seq TO PUBLIC;


--
-- Name: TABLE event; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.event TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.event TO msea;


--
-- Name: SEQUENCE event_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.event_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.event_id_seq TO PUBLIC;


--
-- Name: TABLE event_logger; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.event_logger TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.event_logger TO msea;


--
-- Name: SEQUENCE event_logger_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.event_logger_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.event_logger_id_seq TO msea;


--
-- Name: TABLE measurement; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.measurement TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.measurement TO msea;


--
-- Name: TABLE measurement_type; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.measurement_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.measurement_type TO msea;


--
-- Name: TABLE evt_depth; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.evt_depth TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.evt_depth TO msea;


--
-- Name: TABLE evt_pos; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.evt_pos TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.evt_pos TO msea;


--
-- Name: TABLE flow; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.flow TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.flow TO msea;


--
-- Name: SEQUENCE flow_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.flow_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.flow_id_seq TO msea;


--
-- Name: TABLE habitat_event; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.habitat_event TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.habitat_event TO msea;


--
-- Name: TABLE image_quality; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.image_quality TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.image_quality TO msea;


--
-- Name: SEQUENCE image_quality_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.image_quality_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.image_quality_id_seq TO PUBLIC;


--
-- Name: TABLE import_queue_annotator; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.import_queue_annotator TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.import_queue_annotator TO msea;


--
-- Name: SEQUENCE import_queue_annotation_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.import_queue_annotation_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.import_queue_annotation_id_seq TO msea;


--
-- Name: TABLE import_queue_annotator_label_map_prefill; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.import_queue_annotator_label_map_prefill TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.import_queue_annotator_label_map_prefill TO msea;


--
-- Name: SEQUENCE import_queue_annotator_label_map_prefill_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.import_queue_annotator_label_map_prefill_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.import_queue_annotator_label_map_prefill_id_seq TO msea;


--
-- Name: TABLE import_queue_pi; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.import_queue_pi TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.import_queue_pi TO msea;


--
-- Name: SEQUENCE import_queue_pi_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.import_queue_pi_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.import_queue_pi_id_seq TO msea;


--
-- Name: TABLE instrument; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.instrument TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.instrument TO msea;


--
-- Name: SEQUENCE instrument_config_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.instrument_config_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.instrument_config_id_seq TO PUBLIC;


--
-- Name: SEQUENCE instrument_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.instrument_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.instrument_id_seq TO PUBLIC;


--
-- Name: TABLE measurement_event; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.measurement_event TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.measurement_event TO msea;


--
-- Name: SEQUENCE measurement_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.measurement_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.measurement_id_seq TO PUBLIC;


--
-- Name: SEQUENCE measurement_type_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.measurement_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.measurement_type_id_seq TO PUBLIC;


--
-- Name: TABLE medium_format; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.medium_format TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.medium_format TO msea;


--
-- Name: SEQUENCE medium_format_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.medium_format_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.medium_format_id_seq TO PUBLIC;


--
-- Name: TABLE medium_type; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.medium_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.medium_type TO msea;


--
-- Name: SEQUENCE medium_type_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.medium_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.medium_type_id_seq TO PUBLIC;


--
-- Name: TABLE model; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.model TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.model TO msea;


--
-- Name: TABLE model_documentation; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.model_documentation TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.model_documentation TO msea;


--
-- Name: SEQUENCE model_documentation_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.model_documentation_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.model_documentation_id_seq TO PUBLIC;


--
-- Name: TABLE model_equipment_type; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.model_equipment_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.model_equipment_type TO msea;


--
-- Name: SEQUENCE model_equipment_type_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.model_equipment_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.model_equipment_type_id_seq TO PUBLIC;


--
-- Name: SEQUENCE model_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.model_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.model_id_seq TO PUBLIC;


--
-- Name: TABLE observation; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.observation TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.observation TO msea;


--
-- Name: TABLE observation_confidence; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.observation_confidence TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.observation_confidence TO msea;


--
-- Name: SEQUENCE observation_confidence_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.observation_confidence_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.observation_confidence_id_seq TO PUBLIC;


--
-- Name: TABLE observation_event; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.observation_event TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.observation_event TO msea;


--
-- Name: TABLE orientation; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.orientation TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.orientation TO msea;


--
-- Name: SEQUENCE orientation_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.orientation_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.orientation_id_seq TO PUBLIC;


--
-- Name: TABLE orientation_type; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.orientation_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.orientation_type TO msea;


--
-- Name: SEQUENCE orientation_type_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.orientation_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.orientation_type_id_seq TO PUBLIC;


--
-- Name: TABLE platform; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.platform TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.platform TO msea;


--
-- Name: SEQUENCE platform_config_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.platform_config_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.platform_config_id_seq TO PUBLIC;


--
-- Name: SEQUENCE platform_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.platform_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.platform_id_seq TO PUBLIC;


--
-- Name: SEQUENCE position_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.position_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.position_id_seq TO PUBLIC;


--
-- Name: TABLE position_type; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.position_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.position_type TO msea;


--
-- Name: SEQUENCE position_type_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.position_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.position_type_id_seq TO PUBLIC;


--
-- Name: TABLE program; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.program TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.program TO msea;


--
-- Name: SEQUENCE program_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.program_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.program_id_seq TO PUBLIC;


--
-- Name: TABLE program_library; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.program_library TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.program_library TO msea;


--
-- Name: SEQUENCE program_library_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.program_library_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.program_library_id_seq TO PUBLIC;


--
-- Name: TABLE program_member; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.program_member TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.program_member TO msea;


--
-- Name: SEQUENCE program_member_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.program_member_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.program_member_id_seq TO PUBLIC;


--
-- Name: TABLE program_role; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.program_role TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.program_role TO msea;


--
-- Name: SEQUENCE program_role_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.program_role_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.program_role_id_seq TO PUBLIC;


--
-- Name: TABLE protocol; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.protocol TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.protocol TO msea;


--
-- Name: SEQUENCE protocol_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.protocol_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.protocol_id_seq TO PUBLIC;


--
-- Name: TABLE relief; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.relief TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.relief TO msea;


--
-- Name: SEQUENCE relief_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.relief_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.relief_id_seq TO PUBLIC;


--
-- Name: TABLE signup_area; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.signup_area TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.signup_area TO msea;


--
-- Name: SEQUENCE signup_area_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE rov.signup_area_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE rov.signup_area_id_seq TO msea;


--
-- Name: TABLE status_event; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.status_event TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.status_event TO msea;


--
-- Name: TABLE status_type; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.status_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.status_type TO msea;


--
-- Name: TABLE status_type_detail; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.status_type_detail TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.status_type_detail TO msea;


--
-- Name: SEQUENCE status_type_detail_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.status_type_detail_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.status_type_detail_id_seq TO PUBLIC;


--
-- Name: SEQUENCE status_type_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.status_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.status_type_id_seq TO PUBLIC;


--
-- Name: TABLE substrate; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.substrate TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.substrate TO msea;


--
-- Name: SEQUENCE substrate_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.substrate_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.substrate_id_seq TO PUBLIC;


--
-- Name: TABLE survey_mode; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.survey_mode TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.survey_mode TO msea;


--
-- Name: SEQUENCE survey_mode_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.survey_mode_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.survey_mode_id_seq TO PUBLIC;


--
-- Name: TABLE thickness; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.thickness TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.thickness TO msea;


--
-- Name: SEQUENCE thickness_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.thickness_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.thickness_id_seq TO PUBLIC;


--
-- Name: TABLE transect; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.transect TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.transect TO msea;


--
-- Name: SEQUENCE transect_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.transect_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.transect_id_seq TO PUBLIC;


--
-- Name: TABLE transect_track; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.transect_track TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.transect_track TO msea;


--
-- Name: TABLE weather_observation; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.weather_observation TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.weather_observation TO msea;


--
-- Name: SEQUENCE weather_observation_id_seq; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT ALL ON SEQUENCE rov.weather_observation_id_seq TO msea;
GRANT USAGE ON SEQUENCE rov.weather_observation_id_seq TO PUBLIC;


--
-- Name: TABLE db_version; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.db_version TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.db_version TO msea;


--
-- Name: SEQUENCE db_version_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.db_version_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.db_version_id_seq TO PUBLIC;


--
-- Name: TABLE file; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.file TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.file TO msea;


--
-- Name: SEQUENCE file_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.file_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.file_id_seq TO PUBLIC;


--
-- Name: TABLE file_type; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.file_type TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.file_type TO msea;


--
-- Name: SEQUENCE file_type_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.file_type_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.file_type_id_seq TO PUBLIC;


--
-- Name: TABLE library; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.library TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.library TO msea;


--
-- Name: SEQUENCE library_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.library_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.library_id_seq TO PUBLIC;


--
-- Name: TABLE mseauser; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.mseauser TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.mseauser TO msea;


--
-- Name: SEQUENCE mseauser_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.mseauser_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.mseauser_id_seq TO PUBLIC;


--
-- Name: TABLE user_restriction; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.user_restriction TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.user_restriction TO msea;


--
-- Name: SEQUENCE mseauser_restriction_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE shared.mseauser_restriction_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE shared.mseauser_restriction_id_seq TO msea;


--
-- Name: TABLE organisation; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.organisation TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.organisation TO msea;


--
-- Name: SEQUENCE organisation_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.organisation_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.organisation_id_seq TO PUBLIC;


--
-- Name: TABLE person; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.person TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.person TO msea;


--
-- Name: SEQUENCE person_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.person_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.person_id_seq TO PUBLIC;


--
-- Name: TABLE restriction; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.restriction TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.restriction TO msea;


--
-- Name: SEQUENCE restriction_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.restriction_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.restriction_id_seq TO PUBLIC;


--
-- Name: TABLE site; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.site TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.site TO msea;


--
-- Name: SEQUENCE site_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.site_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.site_id_seq TO PUBLIC;


--
-- Name: TABLE spatial_library; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.spatial_library TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.spatial_library TO msea;


--
-- Name: TABLE spatial_library_file; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.spatial_library_file TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.spatial_library_file TO msea;


--
-- Name: SEQUENCE spatial_library_file_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.spatial_library_file_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.spatial_library_file_id_seq TO PUBLIC;


--
-- Name: SEQUENCE spatial_library_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.spatial_library_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.spatial_library_id_seq TO PUBLIC;


--
-- Name: TABLE taxon; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.taxon TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.taxon TO msea;


--
-- Name: SEQUENCE taxon_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT ALL ON SEQUENCE shared.taxon_id_seq TO msea;
GRANT USAGE ON SEQUENCE shared.taxon_id_seq TO PUBLIC;


--
-- Name: TABLE taxon_restriction; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.taxon_restriction TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.taxon_restriction TO msea;


--
-- Name: SEQUENCE taxon_restriction_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE shared.taxon_restriction_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE shared.taxon_restriction_id_seq TO msea;


--
-- Name: TABLE uploaded_file; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT SELECT ON TABLE shared.uploaded_file TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE shared.uploaded_file TO msea;


--
-- Name: SEQUENCE uploads_id_seq; Type: ACL; Schema: shared; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE shared.uploads_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE shared.uploads_id_seq TO msea;


--
-- Name: TABLE taxon; Type: ACL; Schema: taxonomy; Owner: msea_admin
--

GRANT SELECT ON TABLE taxonomy.taxon TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE taxonomy.taxon TO msea;


--
-- Name: SEQUENCE taxon_id_seq; Type: ACL; Schema: taxonomy; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE taxonomy.taxon_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE taxonomy.taxon_id_seq TO msea;


--
-- Name: TABLE actor; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.actor FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.actor TO msea_admin;
GRANT SELECT ON TABLE wiki.actor TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.actor TO msea;


--
-- Name: SEQUENCE actor_actor_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.actor_actor_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.actor_actor_id_seq TO msea;


--
-- Name: TABLE archive; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.archive FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.archive TO msea_admin;
GRANT SELECT ON TABLE wiki.archive TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.archive TO msea;


--
-- Name: SEQUENCE archive_ar_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.archive_ar_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.archive_ar_id_seq TO msea;


--
-- Name: TABLE bot_passwords; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.bot_passwords FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.bot_passwords TO msea_admin;
GRANT SELECT ON TABLE wiki.bot_passwords TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.bot_passwords TO msea;


--
-- Name: TABLE category; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.category FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.category TO msea_admin;
GRANT SELECT ON TABLE wiki.category TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.category TO msea;


--
-- Name: SEQUENCE category_cat_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.category_cat_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.category_cat_id_seq TO msea;


--
-- Name: TABLE categorylinks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.categorylinks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.categorylinks TO msea_admin;
GRANT SELECT ON TABLE wiki.categorylinks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.categorylinks TO msea;


--
-- Name: TABLE change_tag; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.change_tag FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.change_tag TO msea_admin;
GRANT SELECT ON TABLE wiki.change_tag TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.change_tag TO msea;


--
-- Name: SEQUENCE change_tag_ct_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.change_tag_ct_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.change_tag_ct_id_seq TO msea;


--
-- Name: TABLE change_tag_def; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.change_tag_def FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.change_tag_def TO msea_admin;
GRANT SELECT ON TABLE wiki.change_tag_def TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.change_tag_def TO msea;


--
-- Name: SEQUENCE change_tag_def_ctd_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.change_tag_def_ctd_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.change_tag_def_ctd_id_seq TO msea;


--
-- Name: TABLE comment; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.comment FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.comment TO msea_admin;
GRANT SELECT ON TABLE wiki.comment TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.comment TO msea;


--
-- Name: SEQUENCE comment_comment_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.comment_comment_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.comment_comment_id_seq TO msea;


--
-- Name: TABLE content; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.content FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.content TO msea_admin;
GRANT SELECT ON TABLE wiki.content TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.content TO msea;


--
-- Name: SEQUENCE content_content_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.content_content_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.content_content_id_seq TO msea;


--
-- Name: TABLE content_models; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.content_models FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.content_models TO msea_admin;
GRANT SELECT ON TABLE wiki.content_models TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.content_models TO msea;


--
-- Name: SEQUENCE content_models_model_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.content_models_model_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.content_models_model_id_seq TO msea;


--
-- Name: TABLE externallinks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.externallinks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.externallinks TO msea_admin;
GRANT SELECT ON TABLE wiki.externallinks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.externallinks TO msea;


--
-- Name: SEQUENCE externallinks_el_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.externallinks_el_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.externallinks_el_id_seq TO msea;


--
-- Name: TABLE filearchive; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.filearchive FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.filearchive TO msea_admin;
GRANT SELECT ON TABLE wiki.filearchive TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.filearchive TO msea;


--
-- Name: SEQUENCE filearchive_fa_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.filearchive_fa_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.filearchive_fa_id_seq TO msea;


--
-- Name: TABLE image; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.image FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.image TO msea_admin;
GRANT SELECT ON TABLE wiki.image TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.image TO msea;


--
-- Name: TABLE imagelinks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.imagelinks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.imagelinks TO msea_admin;
GRANT SELECT ON TABLE wiki.imagelinks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.imagelinks TO msea;


--
-- Name: TABLE interwiki; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.interwiki FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.interwiki TO msea_admin;
GRANT SELECT ON TABLE wiki.interwiki TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.interwiki TO msea;


--
-- Name: TABLE ip_changes; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.ip_changes FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.ip_changes TO msea_admin;
GRANT SELECT ON TABLE wiki.ip_changes TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.ip_changes TO msea;


--
-- Name: TABLE ipblocks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.ipblocks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.ipblocks TO msea_admin;
GRANT SELECT ON TABLE wiki.ipblocks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.ipblocks TO msea;


--
-- Name: SEQUENCE ipblocks_ipb_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.ipblocks_ipb_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.ipblocks_ipb_id_seq TO msea;


--
-- Name: TABLE ipblocks_restrictions; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.ipblocks_restrictions FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.ipblocks_restrictions TO msea_admin;
GRANT SELECT ON TABLE wiki.ipblocks_restrictions TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.ipblocks_restrictions TO msea;


--
-- Name: TABLE iwlinks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.iwlinks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.iwlinks TO msea_admin;
GRANT SELECT ON TABLE wiki.iwlinks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.iwlinks TO msea;


--
-- Name: TABLE job; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.job FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.job TO msea_admin;
GRANT SELECT ON TABLE wiki.job TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.job TO msea;


--
-- Name: SEQUENCE job_job_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.job_job_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.job_job_id_seq TO msea;


--
-- Name: TABLE l10n_cache; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.l10n_cache FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.l10n_cache TO msea_admin;
GRANT SELECT ON TABLE wiki.l10n_cache TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.l10n_cache TO msea;


--
-- Name: TABLE langlinks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.langlinks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.langlinks TO msea_admin;
GRANT SELECT ON TABLE wiki.langlinks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.langlinks TO msea;


--
-- Name: TABLE linktarget; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.linktarget FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.linktarget TO msea_admin;
GRANT SELECT ON TABLE wiki.linktarget TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.linktarget TO msea;


--
-- Name: SEQUENCE linktarget_lt_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.linktarget_lt_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.linktarget_lt_id_seq TO msea;


--
-- Name: TABLE log_search; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.log_search FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.log_search TO msea_admin;
GRANT SELECT ON TABLE wiki.log_search TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.log_search TO msea;


--
-- Name: TABLE logging; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.logging FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.logging TO msea_admin;
GRANT SELECT ON TABLE wiki.logging TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.logging TO msea;


--
-- Name: SEQUENCE logging_log_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.logging_log_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.logging_log_id_seq TO msea;


--
-- Name: TABLE module_deps; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.module_deps FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.module_deps TO msea_admin;
GRANT SELECT ON TABLE wiki.module_deps TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.module_deps TO msea;


--
-- Name: TABLE objectcache; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.objectcache FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.objectcache TO msea_admin;
GRANT SELECT ON TABLE wiki.objectcache TO PUBLIC;
GRANT ALL ON TABLE wiki.objectcache TO msea;


--
-- Name: TABLE oldimage; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.oldimage FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.oldimage TO msea_admin;
GRANT SELECT ON TABLE wiki.oldimage TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.oldimage TO msea;


--
-- Name: TABLE page; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.page FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.page TO msea_admin;
GRANT SELECT ON TABLE wiki.page TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.page TO msea;


--
-- Name: SEQUENCE page_page_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.page_page_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.page_page_id_seq TO msea;


--
-- Name: TABLE page_props; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.page_props FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.page_props TO msea_admin;
GRANT SELECT ON TABLE wiki.page_props TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.page_props TO msea;


--
-- Name: TABLE page_restrictions; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.page_restrictions FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.page_restrictions TO msea_admin;
GRANT SELECT ON TABLE wiki.page_restrictions TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.page_restrictions TO msea;


--
-- Name: SEQUENCE page_restrictions_pr_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.page_restrictions_pr_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.page_restrictions_pr_id_seq TO msea;


--
-- Name: TABLE pagelinks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.pagelinks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.pagelinks TO msea_admin;
GRANT SELECT ON TABLE wiki.pagelinks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.pagelinks TO msea;


--
-- Name: TABLE protected_titles; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.protected_titles FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.protected_titles TO msea_admin;
GRANT SELECT ON TABLE wiki.protected_titles TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.protected_titles TO msea;


--
-- Name: TABLE querycache; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.querycache FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.querycache TO msea_admin;
GRANT SELECT ON TABLE wiki.querycache TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.querycache TO msea;


--
-- Name: TABLE querycache_info; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.querycache_info FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.querycache_info TO msea_admin;
GRANT SELECT ON TABLE wiki.querycache_info TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.querycache_info TO msea;


--
-- Name: TABLE querycachetwo; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.querycachetwo FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.querycachetwo TO msea_admin;
GRANT SELECT ON TABLE wiki.querycachetwo TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.querycachetwo TO msea;


--
-- Name: TABLE recentchanges; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.recentchanges FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.recentchanges TO msea_admin;
GRANT SELECT ON TABLE wiki.recentchanges TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.recentchanges TO msea;


--
-- Name: SEQUENCE recentchanges_rc_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.recentchanges_rc_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.recentchanges_rc_id_seq TO msea;


--
-- Name: TABLE redirect; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.redirect FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.redirect TO msea_admin;
GRANT SELECT ON TABLE wiki.redirect TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.redirect TO msea;


--
-- Name: TABLE revision; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.revision FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.revision TO msea_admin;
GRANT SELECT ON TABLE wiki.revision TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.revision TO msea;


--
-- Name: TABLE revision_comment_temp; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.revision_comment_temp FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.revision_comment_temp TO msea_admin;
GRANT SELECT ON TABLE wiki.revision_comment_temp TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.revision_comment_temp TO msea;


--
-- Name: SEQUENCE revision_rev_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.revision_rev_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.revision_rev_id_seq TO msea;


--
-- Name: TABLE searchindex; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.searchindex FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.searchindex TO msea_admin;
GRANT SELECT ON TABLE wiki.searchindex TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.searchindex TO msea;


--
-- Name: TABLE site_identifiers; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.site_identifiers FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.site_identifiers TO msea_admin;
GRANT SELECT ON TABLE wiki.site_identifiers TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.site_identifiers TO msea;


--
-- Name: TABLE site_stats; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.site_stats FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.site_stats TO msea_admin;
GRANT SELECT ON TABLE wiki.site_stats TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.site_stats TO msea;


--
-- Name: TABLE sites; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.sites FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.sites TO msea_admin;
GRANT SELECT ON TABLE wiki.sites TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.sites TO msea;


--
-- Name: SEQUENCE sites_site_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.sites_site_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.sites_site_id_seq TO msea;


--
-- Name: TABLE slot_roles; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.slot_roles FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.slot_roles TO msea_admin;
GRANT SELECT ON TABLE wiki.slot_roles TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.slot_roles TO msea;


--
-- Name: SEQUENCE slot_roles_role_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.slot_roles_role_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.slot_roles_role_id_seq TO msea;


--
-- Name: TABLE slots; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.slots FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.slots TO msea_admin;
GRANT SELECT ON TABLE wiki.slots TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.slots TO msea;


--
-- Name: TABLE templatelinks; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.templatelinks FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.templatelinks TO msea_admin;
GRANT SELECT ON TABLE wiki.templatelinks TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.templatelinks TO msea;


--
-- Name: TABLE text; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.text FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.text TO msea_admin;
GRANT SELECT ON TABLE wiki.text TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.text TO msea;


--
-- Name: SEQUENCE text_old_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.text_old_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.text_old_id_seq TO msea;


--
-- Name: TABLE updatelog; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.updatelog FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.updatelog TO msea_admin;
GRANT SELECT ON TABLE wiki.updatelog TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.updatelog TO msea;


--
-- Name: TABLE uploadstash; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.uploadstash FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.uploadstash TO msea_admin;
GRANT SELECT ON TABLE wiki.uploadstash TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.uploadstash TO msea;


--
-- Name: SEQUENCE uploadstash_us_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.uploadstash_us_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.uploadstash_us_id_seq TO msea;


--
-- Name: TABLE "user"; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki."user" FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki."user" TO msea_admin;
GRANT SELECT ON TABLE wiki."user" TO PUBLIC;
GRANT ALL ON TABLE wiki."user" TO msea;


--
-- Name: TABLE user_autocreate_serial; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.user_autocreate_serial FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_autocreate_serial TO msea_admin;
GRANT SELECT ON TABLE wiki.user_autocreate_serial TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_autocreate_serial TO msea;


--
-- Name: TABLE user_former_groups; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.user_former_groups FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_former_groups TO msea_admin;
GRANT SELECT ON TABLE wiki.user_former_groups TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_former_groups TO msea;


--
-- Name: TABLE user_groups; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.user_groups FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_groups TO msea_admin;
GRANT SELECT ON TABLE wiki.user_groups TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_groups TO msea;


--
-- Name: TABLE user_newtalk; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.user_newtalk FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_newtalk TO msea_admin;
GRANT SELECT ON TABLE wiki.user_newtalk TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_newtalk TO msea;


--
-- Name: TABLE user_properties; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.user_properties FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.user_properties TO msea_admin;
GRANT SELECT ON TABLE wiki.user_properties TO PUBLIC;
GRANT ALL ON TABLE wiki.user_properties TO msea;


--
-- Name: SEQUENCE user_user_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.user_user_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.user_user_id_seq TO msea;


--
-- Name: TABLE watchlist; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.watchlist FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.watchlist TO msea_admin;
GRANT SELECT ON TABLE wiki.watchlist TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.watchlist TO msea;


--
-- Name: TABLE watchlist_expiry; Type: ACL; Schema: wiki; Owner: msea_admin
--

REVOKE ALL ON TABLE wiki.watchlist_expiry FROM msea_admin;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.watchlist_expiry TO msea_admin;
GRANT SELECT ON TABLE wiki.watchlist_expiry TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE wiki.watchlist_expiry TO msea;


--
-- Name: SEQUENCE watchlist_wl_id_seq; Type: ACL; Schema: wiki; Owner: msea_admin
--

GRANT USAGE ON SEQUENCE wiki.watchlist_wl_id_seq TO PUBLIC;
GRANT USAGE ON SEQUENCE wiki.watchlist_wl_id_seq TO msea;


--
-- Name: TABLE cruise_track; Type: ACL; Schema: rov; Owner: msea_admin
--

GRANT SELECT ON TABLE rov.cruise_track TO PUBLIC;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE rov.cruise_track TO msea;


--
-- PostgreSQL database dump complete
--
