-- Change the materialized views to cache tables which can be updated incrementally.
-- Create functions to perform the updates.

-- Fix some indices.

create index if not exists measurement_timestamp_idx on rov.measurement using gist(timestamp);

alter table rov.dive drop constraint if exists fk_dive_sub_config;
alter table rov.dive add constraint fk_dive_sub_config foreign key (sub_config_id) references rov.platform_config(id);
alter table rov.event drop constraint if exists fk_event_dive;
alter table rov.event add constraint fk_event_dive foreign key(dive_id) references rov.dive(id);

drop index if exists rov.dive_ship_config_id_idx;
create index dive_ship_config_id_idx on rov.dive(ship_config_id);
drop index if exists rov.event_dive_id_idx;
create index event_dive_id_idx on rov.event(dive_id);

-- Create a schema for cache tables.
create schema if not exists cache;

comment on schema cache is 'A schema specifically for synthetic read-only tables which increase performance by pre-joining base data.';

--
-- Event dive cache.
--

-- Drop the existing view.
drop materialized view if exists rov.evt_pos cascade;

-- Create the cache table for event positions.
drop table if exists cache.evt_pos;
create table cache.evt_pos (
    event_id integer not null references rov.event(id) on delete cascade on update cascade,
    instrument_config_id integer not null references rov.instrument_config(id) on delete restrict on update cascade,
    timestamps timestamp without time zone[],
    geoms geometry('multipoint', 4326)
);

-- Unique events.
alter table cache.evt_pos add constraint cache_evt_pos_unique_idx unique(event_id, instrument_config_id);

-- Create indices.
create index cache_evt_pos_event_id_idx on cache.evt_pos(event_id);
create index cache_evt_pos_instrument_config_id_idx on cache.evt_pos(instrument_config_id);
create index cache_evt_pos_geom_idx on cache.evt_pos using gist(geoms);
create index cache_evt_pos_timestamps_idx on cache.evt_pos using gin(timestamps);
create index if not exists rov_event_start_time_idx on rov.event using gist(start_time);
create index if not exists rov_event_end_time_idx on rov.event using gist(start_time);
create index if not exists rov_position_timestamp_idx on rov.position using gist(timestamp);
create index if not exists rov_position_instrument_config_idx on rov.position(instrument_config_id);

-- Comments on table.
comment on table cache.evt_pos is 'Creates a relation between an [event](#cache.event) and the [positions](#rov.position) within the span of the event, as a MultiPoint.';
comment on column cache.evt_pos.event_id is 'The link to the [event''s](#rov.event)''s event ID.';
comment on column cache.evt_pos.instrument_config_id is 'The link to the [instrument configuration](#rov.instrument_config).';
comment on column cache.evt_pos.timestamps is 'The list of timestamps corresponding to the geometries in the geoms column.';
comment on column cache.evt_pos.geoms is 'The list of positions comprising the ROV track during the time span of the event. A MultiPoint.';

-- A function to populate the table with events that are not already in the table.
drop function if exists cache_refresh_evt_pos;
create function cache_refresh_evt_pos()
returns void
as $$
    declare 
        -- Records for the loops.
        row0 record;
        -- Counter for logging.
		counter integer;
    begin
        
        raise info 'Processing event positions.';

        -- Initialize to 0 so there will be an output on start.
		counter := 0;

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id, i.id as instrument_config_id, e.start_time, e.end_time
            from rov.event e
                left join cache.evt_pos o on o.event_id = e.id
                inner join rov.dive d on d.id = e.dive_id
                inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
            where o.event_id is null
            order by d.start_time, e.start_time
        loop

            -- Output status.
			if counter % 1000 = 0 then
				raise info 'Rows processed: %.', counter;
			end if;

			counter := counter + 1;

            case when row0.end_time is not null then
                -- If the end time is available, seek positions between the start and end times.
                insert into cache.evt_pos (event_id, instrument_config_id, timestamps, geoms)
                    select row0.event_id, row0.instrument_config_id, 
                        array_agg(p.timestamp) as timestamps, st_collect(st_force2d(p.geom::geometry)) as geoms
                    from (
                        select timestamp, geom
                        from rov.position
                        where instrument_config_id = row0.instrument_config_id
                            and row0.start_time <= timestamp and timestamp <= row0.end_time
                        order by timestamp
                    ) p
                    having count(p.timestamp) > 0;
            else
                -- If the end time is not available, filter to within 1 minute and then sort by nearest to the start time.
                insert into cache.evt_pos (event_id, instrument_config_id, timestamps, geoms)
                    select row0.event_id, row0.instrument_config_id, 
                        array_agg(p.timestamp) as timestamps, st_collect(st_force2d(p.geom::geometry)) as geoms
                    from (
                        select timestamp, geom
                        from rov.position      
                        where instrument_config_id = row0.instrument_config_id
                            and timestamp < (row0.start_time + interval '1 minute')
                            and timestamp > (row0.start_time - interval '1 minute')
                        order by timestamp <-> row0.start_time
                        limit 1
                    ) p
                    having count(p.timestamp) > 0;
            end case;
        end loop;
    end;
$$ language 'plpgsql';

comment on function cache_refresh_evt_pos is 'Populates the cache table with positions corresponding '
    'to each event. For events that cover a time span, the timestamp and '
    'position will be stored as multiple ordered values in the array. for events '
    'without an end time a single element will be contained in the array.';

--
-- Event depth cache table.
--

-- Drop the existing view.
drop materialized view if exists rov.evt_depth cascade;

-- Add missing index.
create index if not exists measurement_timestamp_idx on rov.measurement using gist(timestamp);

-- Create a table for event depths.
drop table if exists cache.evt_depth;
create table cache.evt_depth (
    event_id integer not null references rov.event(id) on delete cascade on update cascade,
    instrument_config_id integer not null references rov.instrument_config(id) on delete restrict on update cascade,
    timestamps timestamp without time zone[],
    depths float[] not null
);

-- Unique events.
alter table cache.evt_depth add constraint cache_evt_depth_unique_idx unique(event_id, instrument_config_id);

-- Documentation comments.
comment on table cache.evt_depth is 'Creates a relation between an [event](#rov.event) and the [depth](#rov.measurement) nearest the start and end times of the event.';
comment on column cache.evt_depth.event_id is 'The link to the [event''s](#rov.event) event ID.';
comment on column cache.evt_depth.instrument_config_id is 'The link to the [instrument configuration](#rov.instrument_config).';
comment on column cache.evt_depth.timestamps is 'The array of timestamps corresponding to the depths.';
comment on column cache.evt_depth.depths is 'The array of [depths](#rov.measurement) between the event''s start and end times. If the event covers a span of time, multiple depths are returned.';

-- Create indices.
create index cache_evt_depth_event_id_idx on cache.evt_depth(event_id);
create index cache_evt_depth_instrument_config_id_idx on cache.evt_depth(instrument_config_id);
create index cache_evt_depth_timestamp_idx on cache.evt_depth using gin(timestamps);
create index cache_evt_depth_depth_idx on cache.evt_depth using gin(depths);

-- A function to refresh the depths.
drop function if exists cache_refresh_evt_depth;
create function cache_refresh_evt_depth()
returns void 
as $$
    declare 
        -- Records for the outer and inner loops.
        row0 record;
        -- Counter for logging.
		counter integer;
        -- The ID of the depth type.
        depth_type integer;
    begin
        
        raise info 'Processing event depths.';

        -- Initialize to 0 so there will be an output on start.
		counter := 0;

        -- Get the depth type
        select id into depth_type from rov.measurement_type where name = 'Depth' and unit = 'm' limit 1;

        -- Iterate over the events that are not already in the cache table.
        for row0 in 
            select e.id as event_id, i.id as instrument_config_id, e.start_time, e.end_time
            from rov.event e
                left join cache.evt_depth o on o.event_id = e.id
                inner join rov.dive d on d.id = e.dive_id
                inner join rov.instrument_config i on i.platform_config_id = d.sub_config_id
            where o.event_id is null
            order by d.start_time, e.start_time
        loop

            -- Output status.
			if counter % 1000 = 0 then
				raise info 'Rows processed: %.', counter;
			end if;

			counter := counter + 1;

            case when row0.end_time is not null then
                -- If the end time is available, seek positions between the start and end times.
                insert into cache.evt_depth (event_id, instrument_config_id, timestamps, depths)
                    select row0.event_id, row0.instrument_config_id, 
                        array_agg(m.timestamp) as timestamps, array_agg(m.quantity) as depths
                    from (
                        select timestamp, quantity
                        from rov.measurement
                        where measurement_type_id = depth_type 
                            and instrument_config_id = row0.instrument_config_id
                            and row0.start_time <= timestamp and timestamp <= row0.end_time
                        order by timestamp
                    ) m
                    having count(m.timestamp) > 0;
            else
                -- If the end time is not available, filter to within 1 minute and then sort by nearest to the start time.
                insert into cache.evt_depth (event_id, instrument_config_id, timestamps, depths)
                    select row0.event_id, row0.instrument_config_id, 
                        array_agg(m.timestamp) as timestamps, array_agg(m.quantity) as depths
                    from (
                        select timestamp, quantity
                        from rov.measurement
                        where measurement_type_id = depth_type
                            and instrument_config_id = row0.instrument_config_id
                            and timestamp < (row0.start_time + interval '1 minute')
                            and timestamp > (row0.start_time - interval '1 minute')
                        order by timestamp <-> row0.start_time
                        limit 1
                    ) m
                    having count(m.timestamp) > 0;
            end case;
        end loop;
    end;
$$ language 'plpgsql';

comment on function cache_refresh_evt_depth is 'Populates the cache table with depths corresponding '
    'to each event. For events that cover a time span, the timestamp and '
    'depth will be stored as multiple ordered values in the array. for events '
    'without an end time a single element will be contained in the array.';

-- 
-- Dive tracks.
--

-- Drop existing view.
drop materialized view if exists rov.dive_track;

-- Create cache table for dive tracks.
create table if not exists cache.dive_track (
    dive_id integer primary key not null references rov.dive(id) on delete cascade on update cascade,
    cruise_id integer not null references rov.cruise(id) on delete cascade on update cascade,
    dive_name varchar(32) not null,
    cruise_name varchar(64) not null,
    colour char(7) not null,
    geom geometry('linestring', 4326) not null
);

-- Create a function to refresh dive tracks.
drop function if exists cache_refresh_dive_track;
create function cache_refresh_dive_track()
returns void 
as $$
    begin
        with t as (
            select 
                f.id as cruise_id, 
                d.id as dive_id, 
                p.id as position_id, 
                p.timestamp, 
                p.geom::geometry as geom,
                concat(f.name, '-', f.leg) as cruise_name, 
                d.name as dive_name, 
                concat('#', substring(md5(f.id::text) from 1 for 6)) as colour
            from rov.dive d
                left join cache.dive_track v on v.dive_id=d.id
                inner join rov.platform_config c on c.id=d.sub_config_id
                inner join rov.instrument_config b on b.platform_config_id=c.id
                inner join rov.position p on p.instrument_config_id=b.id
                inner join rov.cruise f on f.id=d.cruise_id
            where v.dive_id is null
                and d.start_time <= p.timestamp and d.end_time > p.timestamp
            order by p.timestamp
        )
        insert into cache.dive_track (dive_id, cruise_id, dive_name, cruise_name, colour, geom)
            select dive_id, cruise_id, dive_name, cruise_name, colour,
                st_force2d(st_makeline(geom::geometry order by timestamp)) as geom
            from t
            group by cruise_id, dive_id, dive_name, cruise_name, colour;
    end;
$$ language 'plpgsql';

comment on cache_refresh_dive_track is 'Stores a linestring representing the path '
    'of a platform over the course of a dive.';

-- Drop old indices.
drop index if exists rov.dive_track_index_geom;
drop index if exists cache.dive_track_index_geom;
drop index if exists rov.dive_track_uniq;

-- Create spatial index.
create index dive_track_index_geom on cache.dive_track using gist(geom);

comment on table cache.dive_track is 'Constructs a geometry for each [dive](#rov.dive) which describes the path of the submersible.';
comment on column cache.dive_track.dive_id is 'The reference to the [dive](#rov.dive).';
comment on column cache.dive_track.dive_name is 'The [dive](#rov.dive) name.';
comment on column cache.dive_track.cruise_id is 'The reference to the [cruise](#rov.cruise).';
comment on column cache.dive_track.cruise_name is 'The [cruise](#rov.cruise) name.';
comment on column cache.dive_track.geom is 'The dive track geometry.';
comment on column cache.dive_track.colour is 'The a colour code generated from the cruise''s ID. Used for cartography.';


--
-- Transect tracks.
--

-- Drop existing view.
drop materialized view if exists rov.transect_track;

-- Create table for transect tracks.
create table if not exists cache.transect_track (
    transect_id integer primary key not null references rov.transect(id) on delete cascade on update cascade,
    dive_id integer not null references rov.dive(id) on delete cascade on update cascade,
    cruise_id integer not null references rov.cruise(id) on delete cascade on update cascade,
    transect_name varchar(32) not null,
    dive_name varchar(32) not null,
    cruise_name varchar(64) not null,
    colour char(7) not null,
    geom geometry('linestring', 4326) not null
);

-- Create function to refresh transect tracks.
drop function if exists cache_refresh_transect_track;
create function cache_refresh_transect_track()
returns void
as $$
    begin
        with t as (
            select 
                f.id as cruise_id,
                d.id as dive_id, 
                e.id as transect_id, 
                a.id as position_id, 
                a.timestamp, 
                a.geom::geometry as geom,
                concat(f.name, '-', f.leg) as cruise_name,
                d.name as dive_name, 
                e.name as transect_name,
                concat('#', substring(md5(f.id::text) from 1 for 6)) as colour
            from rov.transect e
                left join cache.transect_track v on v.transect_id=e.id
                inner join rov.dive d on d.id=e.dive_id
                inner join rov.platform_config c on c.id=d.sub_config_id
                inner join rov.instrument_config b on b.platform_config_id=c.id
                inner join rov.position a on a.instrument_config_id=b.id
                inner join rov.cruise f on f.id=d.cruise_id
            where v.transect_id is null
                and e.start_time <= a.timestamp and e.end_time > a.timestamp
            order by a.timestamp
        )
        insert into cache.transect_track (transect_id, dive_id, cruise_id, transect_name, dive_name, cruise_name, colour, geom)
            select transect_id, dive_id, cruise_id, transect_name, dive_name, cruise_name, colour,
                st_force2d(st_makeline(geom::geometry order by timestamp)) as geom
            from t
            group by cruise_id, dive_id, transect_id, cruise_name, dive_name, transect_name, colour;
    end;
$$ language 'plpgsql';

comment on cache_refresh_transect_track is 'Stores a linestring representing the path '
    'of a platform over the course of a transect.';

-- Drop indices.
drop index if exists rov.transect_track_index_geom;
drop index if exists cache.transect_track_index_geom;
drop index if exists rov.transect_track_uniq;

-- Create spatial index.
create index transect_track_index_geom on cache.transect_track using gist(geom);

comment on table cache.transect_track is 'Constructs a geometry for each [transect](#rov.transect) which describes the path of the submersible.';
comment on column cache.transect_track.transect_id is 'The reference to the [transect](#rov.transect).';
comment on column cache.transect_track.dive_id is 'The reference to the [dive](#rov.dive).';
comment on column cache.transect_track.dive_name is 'The [dive](#rov.dive) name.';
comment on column cache.transect_track.transect_name is 'The [transect](#rov.transect) name.';
comment on column cache.transect_track.cruise_id is 'The reference to the [cruise](#rov.cruise).';
comment on column cache.transect_track.cruise_name is 'The [cruise](#rov.cruise) name.';
comment on column cache.transect_track.geom is 'The transect track geometry.';
comment on column cache.transect_track.colour is 'The a colour code generated from the cruise''s ID. Used for cartography.';


--
-- Cruise tracks.
--

-- Drop existing view.
drop materialized view if exists rov.cruise_track;

-- Create cruise track table.
create table if not exists cache.cruise_track (
    cruise_id integer not null primary key references rov.cruise(id) on delete cascade on update cascade,
    cruise_name varchar(64) not null,
    colour char(7) not null,
    geom geometry('linestring', 4326) not null
);

-- Create function to refresh cruise tracks.
drop function if exists cache_refresh_cruise_track;
create function cache_refresh_cruise_track()
returns void
as $$
    begin
        insert into cache.cruise_track (cruise_id, cruise_name, geom, colour)
            select 
                a.id as cruise_id, 
                concat(a.name, '-', a.leg) as cruise_name, 
                st_makeline(st_force2d(f.geom::geometry)) as geom,
                concat('#', substring(md5(a.id::text) from 1 for 6)) as colour
            from rov.cruise a
                left join cache.cruise_track v on v.cruise_id=a.id
                inner join rov.dive b on b.cruise_id=a.id
                inner join rov.platform_config d on d.id=b.ship_config_id
                inner join rov.instrument_config e on e.platform_config_id=d.id
                inner join (select * from rov."position" order by "timestamp") f on f.instrument_config_id=e.id
            where v.cruise_id is null
                and f."timestamp" >= a.start_time and f."timestamp" <= a.end_time
            group by a.id;
    end;
$$ language 'plpgsql';

comment on cache_refresh_cruise_track is 'Stores a linestring representing the path '
    'of a platform over the course of a cruise.';

-- Drop indices.
drop index if exists rov.cruise_track_index_geom;
drop index if exists cache.cruise_track_index_geom;
drop index if exists rov.cruise_track_uniq;

-- Create spatial index.
create index cruise_track_index_geom on cache.cruise_track using gist(geom);

comment on table cache.cruise_track is 'Constructs a geometry for each [cruise](#rov.cruise) which describes the path of the ship.';
comment on column cache.cruise_track.cruise_id is 'The reference to the [cruise](#rov.cruise).';
comment on column cache.cruise_track.cruise_name is 'The [cruise](#rov.cruise) name and leg.';
comment on column cache.cruise_track.geom is 'The cruise track geometry.';
comment on column cache.cruise_track.colour is 'The a colour code generated from the cruise''s ID used for cartography.';


-- A function to refresh or rebuild the cache tables.
drop function if exists cache_refresh_tables(boolean);
create or replace function cache_refresh_tables(clean boolean)
returns void
as $$
    begin
        if clean = 't' then
            delete from cache.evt_pos;
            delete from cache.evt_depth;
            delete from cache.cruise_track;
            delete from cache.dive_track;
            delete from cache.transect_track;
        end if;
        perform cache_refresh_evt_pos();
        perform cache_refresh_evt_depth();
        perform cache_refresh_cruise_track();
        perform cache_refresh_dive_track();
        perform cache_refresh_transect_track();
    end;
$$ language 'plpgsql';

-- Refresh tables without clearing first.
drop function if exists cache_refresh_tables();
create or replace function cache_refresh_tables()
returns void
as $$
    begin
        perform cache_refresh_tables('f');
    end;
$$ language 'plpgsql';

