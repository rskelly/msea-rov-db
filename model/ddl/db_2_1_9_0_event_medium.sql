-- Remove medium objects. The medium filename is enough.
alter table rov.event drop column if exists medium_id;

drop table if exists rov.medium cascade;


