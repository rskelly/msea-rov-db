-- Change the uniqe constaint on user emails to allow null.
CREATE EXTENSION IF NOT EXISTS citext;  
drop index shared.person_email_unique;
alter table shared.person alter column email type citext;
create unique index person_email_unique on shared.person(email) where email is not null;

-- Remove null constraint from is_template.
alter table rov.annotation_protocol alter column is_template drop not null;

-- Remove short code columns (obsolete).
alter table rov.annotation_protocol drop column if exists short_code;
alter table rov.annotation_software drop column if exists short_code;
alter table rov.equipment_type drop column if exists short_code;
alter table rov.instrument drop column if exists short_code;
alter table rov.cruise_crew drop column if exists short_code;
alter table rov.dive_crew drop column if exists short_code;
alter table shared.organisation drop column if exists short_code;