-- Move life stage tags to a life_stage property within the properties object.
update rov.event set properties=jsonb_set(properties, '{"life_stage"}', '"Egg Case"')  where properties->>'tags' ilike 'Egg Cas';
update rov.event set properties=jsonb_set(properties, '{"tags"}', properties->'tags' || '["Egg Case"]') where properties->>'tags' ilike '%Egg Cas%';
