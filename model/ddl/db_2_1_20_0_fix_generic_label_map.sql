-- Fix the generic label map by removing the name
-- and making the label_text field unique.

-- Delete duplicates.
delete from rov.generic_label_map 
where id in (
    select a.id 
    from rov.generic_label_map a 
        inner join rov.generic_label_map b on a.label_text=b.label_text 
    where a.name<>b.name
);

-- Drop the name column.
alter table rov.generic_label_map drop column if exists name;

-- Make label_text unique.
alter table rov.generic_label_map alter column label_text type citext;
alter table rov.generic_label_map add constraint generic_label_map_label_text_unique unique (label_text);
