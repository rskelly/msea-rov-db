-- New restriction system. A restriction applies to a specific property in the properties of an event,
-- such that a query can remove events with any property key and value that matches any restriction
-- with the same key and value, or a pattern that matches the value.

-- These are unnecessary.
drop table shared.taxon_restriction cascade;
drop table shared.user_restriction cascade;
drop table if exists shared._restriction;
drop table if exists shared.restriction;

-- Create the restriction table. The name is descriptive; key is the property name that the
-- restriction applies to; pattern is an SQL pattern (using %) that matches property values.
create table if not exists shared.restriction (
    id serial primary key not null,
    name varchar(128) not null,
    key varchar(128) not null,
    pattern text not null,
    note text,
    created_on timestamp without time zone not null default now(),
    updated_on timestamp without time zone not null default now()
);

-- The restrictions should be unique on key and pattern.
alter table shared.restriction add constraint shared_restriction_unique unique(key, pattern);

comment on table shared.restriction is 'The restriction table provides restrictions for [events](#rov.event), based on specific property keys and values. If any event has a key and matching value in a restriction, the event is not shown to non-administrators.';
comment on column shared.restriction.name is 'The name of the restriction.';
comment on column shared.restriction.key is 'The top-level property name or key.';
comment on column shared.restriction.pattern is 'A pattern matching the property value using the SQL ILIKE operator. An exact (case-insensitive) match is a bare string, while appending % at any location is a wildcard match. For example, ''%term'' matches ''exterm'' but not ''exterminate''.';
comment on column shared.restriction.note is 'An optional note about the restriction.';
comment on column shared.restriction.created_on is 'The date of creation of the restriction.';
comment on column shared.restriction.updated_on is 'The date of last update of the restriction.';

-- Links restrictions to groups that may see restricted items. All groups not linked to
-- a restriction are restricted.
create table if not exists shared.restriction_group (
    id serial primary key not null,
    restriction_id integer not null references shared.restriction(id) on delete cascade,
    group_id integer not null references public.auth_group(id) on delete cascade
);

-- Restriction is unique per restriction and group.
alter table shared.restriction_group add constraint shared_restriction_group_unique unique(restriction_id, group_id);

comment on table shared.restriction_group is 'Links the restriction to a group. Members of linked groups are able to view the restricted items.';
comment on column shared.restriction_group.restriction_id is 'The ID of the [restriction](#rov.restriction).';
comment on column shared.restriction_group.group_id is 'The ID of the Django authentication group.';

-- Add an updated-on trigger.
CREATE or replace TRIGGER shared_restriction_updated_on BEFORE UPDATE ON shared.restriction FOR EACH ROW EXECUTE FUNCTION public.updated_on_column();

-- Trigger function to add admin group permission to restriction after creation.
create or replace function restriction_group_set_admin()
returns trigger
as $$
declare
    gid integer;
begin
    select id into gid from public.auth_group where name = 'msea_admin' limit 1;
    raise notice '%', gid;
    insert into shared.restriction_group (group_id, restriction_id) values (gid, new.id);
    return new;
end;
$$ language plpgsql;

comment on function restriction_group_set_admin is 'When a [restriction](#rov.restriction) is inserted, automatically inserts a record into [restriction_group](#rov.restriction) group to give admins permission to see the restricted record. Other groups can be added manualy.';

-- Add a trigger to set the admin group for new restrictions.
CREATE or replace TRIGGER shared_restriction_group_add_admin 
	after insert ON shared.restriction FOR EACH ROW EXECUTE FUNCTION restriction_group_set_admin();

-- Insert some default restrictions.
insert into shared.restriction (name, key, pattern) values
    ('Abalone -- Scientific Name', 'scientific_name', '%abalone%'),
    ('Abalone -- Common Name', 'common_name', '%abalone%'),
    ('Pycnopodia -- Scientific Name', 'scientific_name', '%pycnopodia%'),
    ('Pycnopodia -- Common Name', 'common_name', '%pycnopodia%')
on conflict do nothing;


-- A function that will return the event table with restriction applied.
create or replace function get_unrestricted_events()
returns setof rov.event
as $$
    begin
        
        return query 
            select distinct e.*
            from rov.event e
                left join shared.restriction r on e.properties->>r.key ilike r.pattern
            where r.name is null;

        return;

    end;
$$ language 'plpgsql';

comment on function get_unrestricted_events is 'Return the [events](#rov.event) that are not subject to restriction.';

-- Return the restricted events.
create or replace function get_restricted_events(current_user_id integer)
returns setof rov.event
as $$
    begin
        
        return query 
            with t as (
                -- Get the restrictions for which this user has permission.
                select r.id 
                from shared.restriction r 
                    inner join shared.restriction_group g on g.restriction_id = r.id 
                    inner join auth_user_groups aug on aug.group_id = g.group_id 
                where aug.user_id = current_user_id
            )
            select distinct e.* 
            from rov.event e 
                left join shared.restriction r on e.properties->>r.key ilike r.pattern
            where r.id is null or exists(select * from t where t.id = r.id); 

        return;
    end;
$$ language 'plpgsql';

comment on function get_restricted_events is 'Return the [events](#rov.event) that are either unrestricted, or subject to restrictions for which at least one of the user''s groups is permitted.';