-- Add a table to join Biigle Label Map to Restriction.

create table rov.biigle_label_map_restriction (
    id serial primary key,
    restriction_id integer not null references shared.restriction(id) on delete restrict,
    biigle_label_map_id integer not null references rov.biigle_label_map(id) on delete cascade
);

alter table rov.biigle_label_map_restriction add constraint unique_biigle_label_map_restriction unique(restriction_id, biigle_label_map_id);

comment on table rov.biigle_label_map_restriction is 'Links a [Biigle label map](#rov.biigle_label_map) to a [restriction][#shared.restriction].';
comment on column rov.biigle_label_map_restriction.restriction_id is 'A link to the [restriction](#shared.restriction).';
comment on column rov.biigle_label_map_restriction.biigle_label_map_id is 'A link to the [label map](#rov.biigle_label_map).';