drop table shared.taxon cascade;

drop table rov.observation_event;
drop table rov.status_event;
drop table rov.comment_event;
drop table rov.measurement_event;
drop table rov.habitat_event;
drop table rov.__event cascade;
