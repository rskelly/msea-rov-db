#!/usr/bin/env python

"""
This script applies deltas to a database configured by the given configuration file,
or creates it from scratch from the baseline DDL file.

Database versioning works as follows: the first version of the database is the "baseline,"
and all subsequent modifications are applied as patches, or "deltas." The database maintains a
record of its current version, so the script knows which patches to apply to bring it up to
compliance with a specific version.

The baseline database is version one, and resides in files named with the pattern,

    db_[major version]_[minor_version]_[order]_[file type].sql

The major and minor versions track which version the file belongs to and the file type indicates
what part of the database's structure will be modified when the file is run. On initial
all verion 1.0 files will be run in the order of the order component. On subsequent upgrade runs,
the patch files will be run in version order up to the target version, or the latest version if no
version is specified.

The program is invoked from the command line as,

    python db_manager.py [flags] <config file> [version]

The version is optional, and defaults to the latest.

The -c flag commits the changes. Without it, the update is a dry run.
The -u flag only updates the database version. Use it only when the actual changes have been performed manually.
The -d flag prints out the actual DDL statements.

The config file is mandatory and follows the following format:

    dbname:[database name]
    host:[host]
    user:[database user]
    password:[database password]
    port:[database port]
    ddl_dir:[location of data definition (sql) files]

Multiple configuration files can be used, for example for local or remote development.
"""

import os
import sys
import json
import psycopg2
import traceback
import logging
from operator import itemgetter

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'app'))

from msea.util import util

app_root = os.path.dirname(os.path.realpath(__file__))

def get_version(conn):
    """
    Get the version of the database at the connection. The version is stored in shared.db_version.
    if the database or table doesn't exist, the version is 0.0, otherwise the current version
    is returned as a tuple.
    """
    print('Getting schema version...')
    major = 0
    minor = 0
    rev = 0

    try:
        with conn.cursor() as cur:
            cur.execute(
                'select version_major, version_minor, revision from shared.db_version order by version_major desc, version_minor desc, revision desc limit 1')
            major, minor, rev = cur.fetchone()
    except:
        pass
    conn.rollback()

    print('Schema version {}.{}.{}'.format(major, minor, rev))
    return major, minor, rev


def version_valid(version, start_version, target_version):
    """
    Returns true if the version is greater than the start version and
    less than or equal to the target version.
    """
    v1, v2, v3 = version
    s1, s2, s3 = start_version
    t1, t2, t3 = target_version
    return (
            (v1 > s1 or (v1 == s1 and v2 > s2) or (v1 == s1 and v2 == s2 and v3 > s3))
            and
            (v1 < t1 or (v1 == t1 and v2 < t2) or (v1 == t1 and v2 == t2 and v3 <= t3))
    )


def get_patch_set(start_version, target_version=(999999, 999999, 999999)):
    """
    Get the patch set which would upgrade the database from the start_version to
    the target_version. This involves producing a list of files sorted by version, and
    selecting those that are greater than the current version and less than or equal to
    the target version. The list of files is returned in the order they are to be applied.
    The start and target versions are tuples containing the major and minor versions.
    """
    print('Getting patch set...')
    files = {}
    ddl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.environ['MSEA_DDL_DIR'])
    for f in [x for x in os.listdir(ddl_dir) if x.lower().endswith('.sql')]:
        try:
            major, minor, rev, order = list(map(int, f.split('_')[1:5]))
        except Exception as e:
            logging.warning(str(e))

        k = (major, minor, rev)

        if version_valid(k, start_version, target_version):
            print('Found patch file,', f, '; major: ', major, '; minor: ', minor, '; rev: ', rev, '; order: ', order)
            if not files.get(k):
                files[k] = []
            files[k].append((order, os.path.join(ddl_dir, f)))

    return files



def empty_sql(sql):
    """
    If a chunk of SQL has an executable query, return False.
    """
    if not sql:
        return True
    sql = list(map(str.strip, sql.split('\n')))
    valid = 0
    for s in sql:
        if not s or s.startswith('--') or s.startswith(';'):
            continue
        valid += 1
    return valid <= 0



def split_sql(txt):
    """
    Splits a string on the semicolon, unless the semicolon appears between
    $$ delimeters.
    TODO: This assumes that the programmer always uses $$ to delimit the body of a 
    procedure, which is not a safe assumption.
    """
    # A list of split indices.
    idx = []
    # A dict to track counts of wrapping characters.
    p = {"'": 0, "$$": 0}
    
    # Returns true if any values in the dict are odd.
    def has_odd(p_):
        for v in p_.values():
            if v % 2 == 1:
                return True
        return False

    # Remove all of the lines that start with --.
    txt = '\n'.join([line for line in txt.split('\n') if not line.startswith('--')])

    # Iterate over the individual characters.
    for i in range(len(txt)):
        try:
            # Look for the keys at the current location in the string.
            for k in p.keys():
                k_ = txt[i:i+len(k)]
                if k == k_:
                    # If found, increment the count.
                    p[k] += 1
            # If a semicolon is found and we're not in any wrapping, add its index.
            if not has_odd(p) and txt[i] == ';':
                idx.append(i)
        except: pass

    # Spit the text at each index.
    result = []
    i0 = 0
    for i in idx:
        result.append(txt[i0:i+1])
        i0 = i+1

    return result



def run_upgrade(target_version, dry_run=True, update_version=False, debug=False, permissions=False):
    """
    Runs the upgrade against the configured database. If it fails, hopefully the
    error will be descriptive.
    """
    print('Running upgrade.')
    conn = util.db_connect(autocommit=False)
    version = get_version(conn)
    patches = get_patch_set(version, target_version)
    conn.close()

    if not patches and not permissions:
        print('Database is up to date.')
        return
    else:
        print('Running upgrade from', len(patches), 'patches')

    # Iterate over the keys in major, minor, rev order.
    conn = util.db_connect()
    conn.autocommit = False

    try:
        if patches:
            for k in sorted(patches.keys(), key=itemgetter(0, 1, 2)):

                with conn.cursor() as cur:

                    # Iterate over the files in order key order.
                    if not update_version:
                        for o, f in sorted(patches[k], key=itemgetter(0)):
                            print('Running patch', f, o)
                            with open(f, 'r') as f:
                                cmds = list(map(str.strip, split_sql(f.read())))
                                for cmd in cmds:
                                    if empty_sql(cmd):
                                        continue
                                    if debug:
                                        print(cmd)
                                    cur.execute(cmd)

                    # Update the version number on success.
                    major, minor, rev = k
                    print('Updating to version {}.{}.{}'.format(major, minor, rev))
                    cur.execute('''
                        insert into shared.db_version (version_major, version_minor, revision, updated_on) 
                            values (%s, %s, %s, CURRENT_TIMESTAMP)
                    ''', [major, minor, rev])
        
        if permissions:
            with conn.cursor() as cur:
                # Run the permissions script.
                with open(os.path.join(app_root, 'db_permissions.sql'), 'r') as f:
                    print('Setting permissions.')
                    while True:
                        line = f.readline()
                        if not line:
                            break
                        line = line.strip()
                        if line and not line[0] == '-':
                            if debug:
                                print(line)
                            cur.execute(line)


    except Exception as e:
        conn.rollback()
        conn.close()
        traceback.print_exc()
        return False

    if dry_run:
        conn.rollback()
        print('Rolled back (dry run)')
    else:
        conn.commit()
        print('Commit')
    conn.close()
    return True


if __name__ == '__main__':

    dry_run = True
    update_version = False
    debug = False
    permissions = False
    try:
        a = 1
        while sys.argv[a].startswith('-'):
            if sys.argv[a] == '-c':
                dry_run = False
            if sys.argv[a] == '-u':
                update_version = True
            if sys.argv[a] == '-d':
                debug = True
            if sys.argv[a] == '-p':
                permissions = True
            a += 1
        configfile = sys.argv[a]
        a += 1
        try:
            version = sys.argv[a]
            a += a
        except:
            version = '999999.999999.999999'
        try:
            version = list(map(int, version.split('.')))
        except Exception as e:
            print('The version string is invalid: ', version)
            sys.exit(1)
        
        util.load_env(configfile)

        run_upgrade(version, dry_run=dry_run, update_version=update_version, debug=debug,
                    permissions=permissions)
        print('Done')
    except Exception as e:
        logging.warning(str(e))
        traceback.print_exc()
        print('Usage: python db_manager.py [flags] <config file> [version]')
        print('  -c  If given, commit changes.')
        print('  -u  If given, update version only, do not apply changes.')
        print('  -d  Print out the debug queries.')
