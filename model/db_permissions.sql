-- Grants permissions on application tables to the msea database user.
-- Only works when used from an account with sufficient privileges, such as msea_admin.

revoke all privileges on all tables in schema shared from public;
revoke all privileges on all tables in schema taxonomy from public;
revoke all privileges on all tables in schema rov from public;
revoke all privileges on all tables in schema ndst from public;
revoke all privileges on all tables in schema maxmind from public;
revoke all privileges on all tables in schema cache from public;

grant select on all tables in schema shared to public;
grant select on all tables in schema taxonomy to public;
grant select on all tables in schema rov to public;
grant select on all tables in schema ndst to public;
grant select on all tables in schema maxmind to public;
grant select on all tables in schema cache to public;

grant usage on all sequences in schema shared to public;
grant usage on all sequences in schema taxonomy to public;
grant usage on all sequences in schema rov to public;
grant usage on all sequences in schema ndst to public;
grant usage on all sequences in schema maxmind to public;
grant usage on all sequences in schema cache to public;

grant usage on schema shared to public;
grant usage on schema taxonomy to public;
grant usage on schema rov to public;
grant usage on schema ndst to public;
grant usage on schema maxmind to public;
grant usage on schema cache to public;

revoke all privileges on all tables in schema shared from msea;
revoke all privileges on all tables in schema taxonomy from msea;
revoke all privileges on all tables in schema rov from msea;
revoke all privileges on all tables in schema ndst from msea;
revoke all privileges on all tables in schema maxmind from msea;
revoke all privileges on all tables in schema cache from msea;

grant select on all tables in schema shared to msea;
grant select on all tables in schema taxonomy to msea;
grant select on all tables in schema rov to msea;
grant select on all tables in schema ndst to msea;
grant select on all tables in schema maxmind to msea;
grant select on all tables in schema cache to msea;

grant update on all tables in schema shared to msea;
grant update on all tables in schema taxonomy to msea;
grant update on all tables in schema rov to msea;
grant update on all tables in schema ndst to msea;
grant update on all tables in schema maxmind to msea;
grant update on all tables in schema cache to msea;

grant delete on all tables in schema shared to msea;
grant delete on all tables in schema taxonomy to msea;
grant delete on all tables in schema rov to msea;
grant delete on all tables in schema ndst to msea;
grant delete on all tables in schema maxmind to msea;
grant delete on all tables in schema cache to msea;

grant insert on all tables in schema shared to msea;
grant insert on all tables in schema taxonomy to msea;
grant insert on all tables in schema rov to msea;
grant insert on all tables in schema ndst to msea;
grant insert on all tables in schema maxmind to msea;
grant insert on all tables in schema cache to msea;

grant usage on all sequences in schema shared to msea;
grant usage on all sequences in schema taxonomy to msea;
grant usage on all sequences in schema rov to msea;
grant usage on all sequences in schema ndst to msea;
grant usage on all sequences in schema maxmind to msea;
grant usage on all sequences in schema cache to msea;

grant usage on schema shared to msea;
grant usage on schema taxonomy to msea;
grant usage on schema rov to msea;
grant usage on schema ndst to msea;
grant usage on schema maxmind to msea;
grant usage on schema cache to msea;

grant all on knox_authtoken to msea;
