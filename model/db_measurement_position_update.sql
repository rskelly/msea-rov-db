-- Add any new measurements to the measurement_position table, if they have not already been added.
-- If new positions are added which would affect the previous measurements, this will not work because
-- this query does not affect existing measurement records. For that case, use refresh materialized view (slow).
with t as (
    select id
    from rov.measurement
    where id not in (select measurement_id from rov.measurement_position)
)
insert into rov.measurement_position (measurement_id, position_id)
select a.id as measurement_id, b.id as position_id
from rov.measurement a, t
cross join lateral (
    select id, "timestamp"
    from rov."position"
    order by a."timestamp" <-> "timestamp"
    limit 1
) as b
where a.id=t.id