#!/usr/bin/env python

"""
Extracts documentation from a database and formats it using markdown.
"""

import os
import markdown

from msea.util import util

# List of schemas to document. Can be found in psql by the command \dn.
# The list is tiered by the database. Some schemas belong to a specific
# database, and some are shared.
schema_groups = [
    (
        'Shared',
        'These are shared tables used by other tables in other schemas.',
        ('shared',)
    ), (
        'Taxonomy',
        'These are tables which contain external taxonomic data and are used by tables in other schemas.',
        ('taxonomy',)
    ), (
        'Intertidal Database',
        'These are tables associated with intertidal surveys.',
        ('intertidal', 'iform', )
    ), (
        'Annotation Database',
        'These are tables related to the annotation database.',
        ('rov', 'ndst', '')
    ), (
        'Protected Areas',
        'These tables relate to protected areas.',
        ('pa',)
    # ), (
    #     'Public',
    #     'These are public tables that may or may not have any relation to MSEA databases.',
    #     ('public', )
    ), (
        'Wiki',
        'These tables are owned by the Mediawiki installation.',
        ('wiki',)
    )
]


# We skip these columns because they're internal or not of interest to the developer.
skip_cols = ('id',)

def process_entities(sql, type, type_label, schema, cur, f):
    """
    Produce descriptions and tables for the entities in the query.
    :param sql: The query that finds the relations to document. The query has placeholders for the relation type and schema, in that order.
    :param type: The type of relation to document.
    :param schema: The schema to retreive relations for.
    :param cur: The database cursor.
    :param f: The open file handle to write to.
    """
    # Find the relations.
    cur.execute(sql, (type, schema,))

    # Run over the relations.
    rels = cur.fetchall()
    for rel_oid, rel_name, in rels:
        print('Relation:', rel_name)
        f.write(f'##### {type_label}: {rel_name} <a name="{schema}.{rel_name}"></a> \n\n')

        # Get the description of the relation.
        cur.execute('select description from pg_catalog.pg_description where objsubid=0 and objoid=%s', (rel_oid,))
        desc = cur.fetchone()
        if desc:
            f.write(f'{desc[0]}\n\n')

        # Get the relation columns and interesting info about them. 
        # TODO: Might be nice to document uniqueness or references.
        cur.execute("""
            select a.attnum, a.attname, b.typname, b.typnotnull, b.typdefault from pg_attribute a
                inner join pg_type b on b.oid=a.atttypid
            where a.attrelid=%s and a.attnum>0 order by attname
        """, (rel_oid,))
        columns = cur.fetchall()

        f.write('| Column Name | Type | Not Null | Default | Description |\n')
        f.write('| --- | --- | --- | --- | --- |\n')

        for col_oid, col_name, col_type, col_notnull, col_default in columns:
            if col_name in skip_cols:
                # Skip columns in the skip list.
                continue
            
            # Initialize the output row.
            row = [col_name, col_type, col_notnull, col_default]

            # Get the description for the column.
            cur.execute('select description from pg_catalog.pg_description where objsubid=%s and objoid=%s', (col_oid, rel_oid,))
            desc = cur.fetchone()
            if desc:
                row.append(desc[0])
            else:
                # If nothing is returned, it's empty.
                row.append('')
            
            # Write the row.
            f.write('| ' + ' | '.join(list(map(str, row))) + ' |\n')

        f.write('\n')

def run(output_name, config_file):
    """
    Run the document creation process.
    :param output_name: The output file name with no extension. The md and html extension will be added. Any given extension will be removed.
    :param config_file: The path to a configuration file providing connection parameters for the database.
    """
    # Strip the extension from the output file.
    output_name = os.path.splitext(output_name)[0]

    # Load the configuration and connect to the DB.
    config = util.load_config(config_file)
    conn = util.db_connect(config, autocommit=False)
    cur = conn.cursor()

    # Open the markdown file for output.
    with open(output_name + '.md', 'w') as f:
        # Start with the schema groups.
        f.write('## Groups\n\n')

        for name, description, schemas in schema_groups:
            print('Schema group:', name)

            # Write header.
            f.write(f'### {name}\n\n')
            f.write(f'{description}\n\n')

            # Run over schemas.
            for schema in schemas:
                print('Sechema:', schema)

                f.write(f'#### Schema: {schema}\n\n')
                
                # Do tables.
                process_entities("""
                    select oid, relname 
                    from pg_class 
                    where (not relname ilike 'django_%%') and (not relname ilike 'auth_%%') 
                        and relkind=%s 
                        and relnamespace=(select oid from pg_namespace where nspname=%s)
                    order by relname
                """, 'r', "Table", schema, cur, f)

                # Do materialized views.
                process_entities("""
                    select oid, relname 
                    from pg_class 
                    where relkind=%s 
                        and relnamespace=(select oid from pg_namespace where nspname=%s)
                    order by relname
                """, 'm', "Materialized View", schema, cur, f)

                # Do views.
                process_entities("""
                    select oid, relname 
                    from pg_class 
                    where relkind=%s 
                        and relnamespace=(select oid from pg_namespace where nspname=%s)
                    order by relname
                """, 'v', "View", schema, cur, f)

        # Load the Markdown file and re-save it as an HTML file.
        with open(output_name + '.md', 'r') as fi:
            with open(output_name + '.html', 'w') as fo:
                html = markdown.markdown(fi.read(), extensions=['tables'])
                fo.write(html)


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(
        prog='db_extract_docs.py',
        description='Extracts documentation comments from PostgreSQL tables and columns and saves them to a file.',
        epilog='')

    parser.add_argument('config_file', help='A file containing colon-delimited connection parameters for the database.')
    parser.add_argument('output_name', help='The output name. Extensions for html and markdown will be affixed. Any given extension will be stripped.')
    
    args = parser.parse_args()

    run(args.output_name, args.config_file)
    