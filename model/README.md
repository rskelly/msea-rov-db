# The Data Model

The database model was originally developed using [Visual Paradigm](https://www.visual-paradigm.com/), an 
industry-standard modeling tool for enterprise. VP provides a free "community" version and a paid version, available 
at a number of levels.

Currently, the database is developed manually through data-definition language (DDL) files: there is an initial 
"baseline" DDL file that creates the original database, and a set of "upgrade" files that apply incremental changes to 
the initial model. The `db_manager.py` script applies the updates and updates the `shared.db_version` table with the
current version.

At some point, all of these changes will be rolled up into a new baseline file and continue from there. It tends to get a bit bloated.

The folder structure is as follows:

* `model`
  * `ddl` -- The DDL files are stored here.
  * `db_manager.py` -- The database installation/upgrade script.
  * `db_permissions.sql` -- Sets the correct permissions on schemas, tables and other objects in the database.
  * `db_extract_docs.py` -- Extract documentation into a file and format suitable for the [wiki](https://msea.science/wiki/).
  
## Installing and Upgrading

*Note: we are in the process of containerizing all MSEA applications, but the database still runs on bare metal. The database creation
and upgrade scripts can be run directly from the code respository.

The database is created in the typical PostgreSQL way, using the PostgreSQL command line tools:

    createuser -s -W msea_admin

This creates an admin user named `msea_admin` and asks for a password. The password you select will need to be set
in the `~/configs/.env_rov` file with other site and database properties. The format is,

    POSTGRES_DB=msea
    POSTGRES_USER=msea_admin
    POSTGRES_PASSWORD=[password]
    POSTGRES_PORT=5432
    POSTGRES_HOST=localhost
    MSEA_DDL_DIR=/home/git/msea-rov-db/model/ddl

The properties are:

* `POSTGRES_DB` - The name of the database.
* `POSTGRES_HOST` - The host name or IP where the server is available.
* `POSTGRES_USER` - The username of the user.
* `POSTGRES_PASSWORD` - The password of the user.
* `POSTGRES_PORT` - The port where the server is available. This is 5432 by default, but another port may be used if that port is taken or unavailable. 
* `MSEA_DDL_DIR` - The location of the DDL files.

*Note that there is an additional set of database-related properties prefixed with, "DB_*". These are for the use of Django applications and standalone scripts which do not need to modify the database structure.*

The next step is to actually install the database. This is performed by running the Python script, `db_manager.py` 
from the command line. The default invocation requires only the path to the configuration file.

    python db_manager.py /path/to/configs/.env_rov

*Note: for development purposes, the `env` file is in docker/configs. For deployment, it's stored in `~/configs`.

This will assume that you want to install and upgrade the database all the way to the latest version. In this mode,
it will perform a "dry run" and perform all operations within a transaction that is then automatically rolled back. If
no errors appear, you can assume that the installation will be successful. In that case, run the tool again with the 
`-c` flag (for commit) and the `-p` flag to update permissions (more on that below):

    python db_manager.py -c -p ~/configs/.env_rov

After this, the database should be in the current version, and its `shared.db_version` table updated. If any errors 
appear, a developer must take steps to repair them. If the errors can be repaired in the scripts,
the tool can be run again. If not, and the database must be manually upgraded to match the scripts, the version can
be updated without actually running the scripts again by supplying the `-u` flag. This should only ever be done as a 
last resort. If manual modifications must be made, running the [unit tests](../app/tests) can help
find any errors (if they're up to date, which they currently are not).

At this point, the database is ready for use. The upgrade scripts are designed to work even when there is data in the
database -- in fact, upgrade scripts *must* move or adjust existing data when the structure is revised.

In production environments, the `msea` user is used for accessing the database. Because the entities are owned by 
`msea_admin`, selected permissions must be granted to `msea`. The `db_permissions.sql` file does this. Invoke it
as `msea_admin`:

    psql -U msea_admin -d msea_dev -f db_permissions.sql

Note that the `msea_dev` database is named here. In a production environment, the name of that database would be 
used (presumably `msea`).

## Initial Data

Initial data can be loaded by downloading a backup of the live site and importing it into the database. This is done using 
the `psql` program:

    psql -U msea_admin -d msea -f /path/to/backup.file

The database will have to be dropped and restored by this method before the upgrades can be applied. If this succeeds, you can
be confident that upgrades will succeed on the production database.

## Where is the Documentation?

Documentation for each entity in the model (tables, relations, constraints, etc.) is written using the `COMMENT ON...`
feature of SQL, and embedded directly in the database. Comments are extracted and formatted as markdown by the
`db_extract_docs.py` script which produces markdown which can be embedded in the [wiki](https://msea.science/wiki/).

