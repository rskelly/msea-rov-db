#!/bin/bash

# Note the Django-based sites require database configurations to be placed
# in the home directory of the uWSGI (Python) user, which is ideally www-data 
# on Debian. www-data's home directory is /var/www, so the configs should be
# in files called /var/www/configs/db_[whatever].conf. There will be one each
# for dev (e.g., db_dev.conf), prod, prod_admin and stage.
# Note: the configuration files must not be stored in version control. They
# contain sensitive information.

for i in "$@";
do
	case $i in 
		-e)
			# If set, run the python install.
			py="1";;
		-v)
			# If set, compile vue.
			vue="1";;
		-m)
			# If set, install the maintenance cron job.
			maint="1";;
	esac
done

# Get the mode.
read -p "Enter a mode (stage or prod) " mode

if [ "$mode" != "stage" ] && [ "$mode" != "prod" ];
then
	echo "The mode must be prod or stage.";
	exit 1;
fi

### Go to the vue folder and build the UI.
### Node/npm must be installed in the system.
if [ "$vue" == "1" ];
then
	pushd ../vue
	npm run build
	popd
fi

### Setup UWSGI applications and restart servers.

# Chose the target name.
if [ "$mode" == "prod" ];
then
	name="msea";
	configs=("msea.nginx");
	includes=("ipfilter.nginx" "wiki.nginx")
	config_file="/home/msea/configs/db_prod_admin.conf"
else
	name="mseastage";
	configs=("msea.nginx");
	includes=("ipfilter.nginx")
	config_file="/home/msea/configs/db_stage_admin.conf"
fi;

# The user/group of the web server.
web_user="www-data"
web_group="www-data"

# Set the ultimate owner.
user="$(id -un)"
group="$(id -gn)"
echo "Working as $user:$group"

# Get the repo dir.
repo_dir=$(dirname $(dirname $PWD) )
echo "Working dir: $repo_dir"

# Link the uWSGI config files to the apps directory.
sudo rm /etc/uwsgi/apps-enabled/${name}_uwsgi.ini
sudo ln -s $PWD/${name}_uwsgi.ini /etc/uwsgi/apps-enabled/${name}_uwsgi.ini

# Create and link the folders for the prod and staging sites.
# Will attempt to establish and install the .venv environment.
echo "Updating $name";

# Try to create the folder.
echo "Make directory"
sudo mkdir -p "/var/$name"

# Set the ownership of the dir so we can work on it.
echo "Change ownership"
sudo chown -R $user:$group "/var/$name"

# Rebuild the links.
echo "Deploy files."
rsync -r -u --delete "$repo_dir/app/main" "/var/$name/"
rsync -r -u --delete "$repo_dir/app/msea" "/var/$name/"
rsync -r -u --delete "$repo_dir/app/vue/dist" "/var/$name/"
rsync -r -u --delete "$repo_dir/app/email_templates" "/var/$name/"

echo "Navigating to $name"
pushd "/var/$name"

if [ "$py" == "1" ];
then
	# Try to establish the .venv environment.
	if [ -d .venv ];
	then
		rm -rf .venv
	fi

	echo "Creating venv"
	python3 -m venv .venv
	echo "Activating venv; removing libraries"
	source .venv/bin/activate
	pip install --upgrade pip
	echo "Installing Python libraries"
	pip install -r "$repo_dir/app/server_config/requirements_debian.txt"
	echo "Deactivating venv"
	deactivate
fi

popd

# Update the ownership of the app folder. Does not follow symlinks.
echo "Change ownership"
sudo chown -R $web_user:$web_group "/var/$name"

# Restart the service.
sudo service uwsgi restart

### Nginx configuration

# Install the nginx configurations and restart the server.
# This installs the staging site, the main site and the shiny apps site.
# Note: the file names follow the convention of [top-level domain].[subdomain]
# - msea.nginx      -- The main MSEA submersible database site.
# - mseastage.nginx -- The staging site for the above.
# - ipfilter.nginx  -- The IP filter for DFO (etc.) subnets.
# - wiki.nginx		-- The wiki configuration file.

echo "Link the includes."
for f in ${includes[@]};
do
	echo "Copying $f to nginx includes."
	sudo mkdir -p /etc/nginx/includes
	sudo rm "/etc/nginx/includes/${f%.*}"
	sudo cp $f "/etc/nginx/includes/${f%.*}"
done;

for f in ${configs[@]};
do
	echo "Copying $f to nginx config."
	sudo rm /etc/nginx/sites-available/${f%.*};
	sudo rm /etc/nginx/sites-enabled/${f%.*};
	sudo cp $f /etc/nginx/sites-available/${f%.*};
	sudo ln -s /etc/nginx/sites-available/${f%.*} /etc/nginx/sites-enabled/${f%.*};
done;

sudo service nginx restart


### Update the cron tasks with database maintenance tasks.

if [ "$maint" == "1" ];
then
	echo "Installing maintenance cron jobs."
	cat << EOF > /tmp/cron.txt
# Execute tasks at the appropriate time.
# This executes at 8:05UTC each Sunday.
5 8 * * 7 root bash /home/msea/git/msea-rov-db/maintenance/db_maintenance_1w.sh $config_file
# This executes at 8:05UTC every day.
5 8 * * * root bash /home/msea/git/msea-rov-db/maintenance/db_maintenance_24h.sh $config_file
EOF
	sudo mv /tmp/cron.txt /etc/cron.d/$name
	sudo chown root:root /etc/cron.d/$name
fi


