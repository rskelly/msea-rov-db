#!/bin/bash

### Update the cron tasks with database maintenance tasks.


echo "Installing maintenance cron jobs."
cat << EOF > /tmp/cron.txt
# Execute tasks at the appropriate time.
# This executes at 8:05UTC each Sunday.
5 8 * * 7 root bash /home/msea/git/msea-rov-db/maintenance/db_maintenance_1w.sh $config_file
# This executes at 8:05UTC every day.
5 8 * * * root bash /home/msea/git/msea-rov-db/maintenance/db_maintenance_24h.sh $config_file
EOF
sudo mv /tmp/cron.txt /etc/cron.d/$name
sudo chown root:root /etc/cron.d/$name



