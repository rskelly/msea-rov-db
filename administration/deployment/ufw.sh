#!/bin/bash

# Configures the UFW firewall to allow access to services within the DFO subnet(s)
# and deny access to everyone else.

endpoints=(22 5432 80 443 873)
# subnets=(205.193.114.0/24 205.193.112.0/24 198.103.161.0/24)
# Note: DFO seems to have closed the ports we need so now we have to accept connections from anywhere.
subnets=(any)

ufw reset
ufw default deny incoming
ufw default allow outgoing

for e in ${endpoints[@]};
do
	for s in ${subnets[@]};
	do
		echo $e $s;
		ufw allow from $s to any port $e
	done
done

ufw enable

# 22/tcp                     ALLOW       Anywhere
# 8000/tcp                   ALLOW       205.193.112.0/24
# 8000/tcp                   ALLOW       205.193.114.0/24
# 8000/tcp                   ALLOW       198.103.161.0/24
# 80/tcp                     ALLOW       Anywhere
# 443/tcp                    ALLOW       Anywhere
# 8081/tcp                   ALLOW       Anywhere
# 22/tcp (v6)                ALLOW       Anywhere (v6)
# 80/tcp (v6)                ALLOW       Anywhere (v6)
# 443/tcp (v6)               ALLOW       Anywhere (v6)
# 8081/tcp (v6)              ALLOW       Anywhere (v6)
