# Deployment

These are scripts and configurations for the compilation, configuration and deployment of the site.

## crontab.txt

This is installed in the system cron to perform maintenance operations, which are stored in `../maintenance`, currently every 24 hours and every 7 days.

## install.sh

This is the script that actually compiles, installs and configures the site. The script is run by invoking from the command line,

    ./install.sh [-e] [-v] [-m]

where `-e` causes the Python libraries to be installed from the `requirements.txt` file; `-v` compiles the Vue site; and `-m` installs the maintenance cron job.

When the script is executed, the user will be prompted to enter `stage` or `prod` to install the production or staging site. 

* Note: at the present time, the staging site is not available due to a reorganization of the site structure. Be careful! *

## ipfilter.nginx

This is an IP filter which can be included in location blocks in the Nginx configuration to limit visitors to those on a DFO subnet (or the developer). This is currently inactive, as the site is to be open to the public.

## msea.nginx

This is the configuration for all sites on the machine. 

TODO: Other sites should be configured by their own files but have not advanced to the state of independence. As they do so, their configurations will be removed from this file.

## msea_uwsgi.ini

This is the uWSGI configuration for the Django application. Nginx communicates with the app through a pipe, and the app uses a virtual environment configured here.

## mseastage.nginx

This is the configuration for the staging site, which is not currently active.

## mseastage_uwsgi.ini

This is the uWSGI configuration for the Django application on the staging site, which is not currently active.

## requirements_*.txt

These are requirements files providing lists of required Python libraries for the Django apps' virtual environments.

## ufw.sh

This script configures the firewall for the server machine.

## wiki.nginx

This is an Nginx configuration file for the Wiki site.


