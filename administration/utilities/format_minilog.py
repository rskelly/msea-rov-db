#!/usr/bin/env python

'''
Parses a series of minilog files with .### extensions, in order to try to compile
a complete dataset. Will parse dates into m/d/y format because that's
just customary at this point.

The date format can be mixed. This tries to fix that and orders all the entries by time. It's a 
naive algorithm so it could use a lot of memory.
'''

import os
import sys
import csv
from datetime import datetime, timezone
from scipy.cluster.vq import kmeans2
import numpy as np

dirname = sys.argv[1]
divetimes = sys.argv[2]

centroids = []

with open(divetimes, 'r') as f:
    db = csv.reader(f)
    head = next(db)
    for row in db:
        st = datetime.strptime('{} {}'.format(row[1], row[2]), '%m/%d/%Y %H:%M:%S').replace(tzinfo=timezone.utc)
        et = datetime.strptime('{} {}'.format(row[1], row[3]), '%m/%d/%Y %H:%M:%S').replace(tzinfo=timezone.utc)
        centroids.append(st.timestamp() + (et.timestamp() - st.timestamp()) / 2.)  # , 0.))

# def get_diveid(dt):
# 	keys = sorted(timemap.keys())
# 	dtt = dt.timestamp()
# 	if dtt < keys[0]:
# 		return timemap[keys[0]]['diveid']
# 	if dtt > keys[-1]:
# 		return timemap[keys[-1]]['diveid']
# 	for i in range(len(keys) - 1):
# 		t1, t2 = keys[i:i+2]
# 		if dtt >= t1 and dtt < t2:
# 			return timemap[t1]['diveid']
# 	raise Exception('Impossible')

# files = {}
# records = []

# for f in [x for x in os.listdir(dirname) if x.startswith('Asc') and x.find('.') > -1]:
# 	k = n, k = f.split('.')
# 	files[int(k)] = f

# for k in sorted(files.keys()):
# 	with open(os.path.join(dirname, files[k]), 'r') as f:
# 		line = f.readline()
# 		while line:
# 			if line[0] != '*':
# 				date, time, temp, depth = line.strip().split(',')
# 				dt = None
# 				try:
# 					dt = datetime.strptime('{} {}'.format(date, time), '%d-%m-%Y %H:%M:%S').replace(tzinfo = timezone.utc)
# 				except: pass
# 				try:
# 					dt = datetime.strptime('{} {}'.format(date, time), '%Y-%m-%d %H:%M:%S').replace(tzinfo = timezone.utc)
# 				except: pass
# 				if not dt:
# 					print(line)
# 					sys.exit(1)
# 				diveid = get_diveid(dt)
# 				records.append((dt.timestamp(), diveid, dt.strftime('%m/%d/%Y'), dt.strftime('%H:%M:%S'), temp, depth))
# 			line = f.readline()

# records = sorted(records, key = lambda a: a[0])

# print('diveid,date,time,temperature,depth')
# for row in records:
# 	print('{},{},{},{}'.format(*row[1:]))

data = []
data2 = []
for f in [x for x in os.listdir(dirname) if x.startswith('Asc') and x.find('.') > -1]:
    with open(os.path.join(dirname, f), 'r') as f:
        line = f.readline()
        while line:
            if line[0] != '*':
                date, time, temp, depth = line.strip().split(',')
                dt = None
                try:
                    dt = datetime.strptime('{} {}'.format(date, time), '%d-%m-%Y %H:%M:%S').replace(tzinfo=timezone.utc)
                except:
                    pass
                try:
                    dt = datetime.strptime('{} {}'.format(date, time), '%Y-%m-%d %H:%M:%S').replace(tzinfo=timezone.utc)
                except:
                    pass
                if not dt:
                    print(line)
                    sys.exit(1)
                data.append(dt.timestamp())
                data2.append((dt.timestamp(), dt.strftime('%Y-%m-%dT%H:%M:%SZ'), temp, depth))
            line = f.readline()

data = np.array(data)
centroids = np.array(centroids)

centroids, labels = kmeans2(data, iter=10, k=centroids, minit='matrix')

for i in range(len(data2)):
    print('{},{},{},{}'.format(labels[i] + 1, *data2[i][1:]))
