#!/usr/bin/env python

"""
This script converts json files with a generic listing of model values into django
fixtures. The input file specification is an object whose keys index a set of identical
objects containing the following keys:

'model' - string - The model class name (i.e., for Django);
'fields' - array - The list of field names.
'values' - array of arrays - Each sub-array is a list of values corresponding to the field names.

Any field name suffixed with ':j' is serialized as JSON on the assumption that it is
meant to fit in a json(b) field.

An example:

	{
		"mytable": {
			"model": "myapp.MyModel",
			"fields": ["a", "b", "c:j"],
			"values": [
				[1, "hello", {"x": 1, "y": "boo"}]],
				[2, "goodbye", null]
			]
		}
	}

Here, the 3rd element in each subarray (corresponding to field 'c') is serialized
as JSON. The others are saved directly.

Input files should begin with _ and end with .json. The _ is striped before the file is saved
as a Django fixture.
"""

import os
import sys
import importlib
import json

# Input, output directory.
indir = sys.argv[1]
outdir = sys.argv[2]

for f in [x for x in os.listdir(indir) if x.startswith('_') and x.endswith('.json')]:

	infile = os.path.join(indir, f)
	outfile = os.path.join(outdir, f[1:])

	print(infile, '-->', outfile)

	output = []

	# Open and load the input file.
	with open(infile, 'r') as f:
		print(infile)
		data = f.read()
		data = json.loads(data)

		# Iterate over the named model definitions.
		for name, item in data.items():
			
			# Iterate over the entity definitions.
			for v in item['values']:

				# Zip the fields and values to make a dict.
				row = dict(zip(item['fields'], v))

				# Extract and remove the ID (pk)
				id = int(row['id'])
				del row['id']

				# Serialize the values marked with ':j' and remove the keys.
				row0 = {}
				for k, v in row.items():
					if k.find(':') > -1:
						row0[k[:k.index(':')]] = v #json.dumps(v)
					else:
						row0[k] = v
				row = row0
				
				# Output the fixture definition.
				output.append({
					'model': item['model'],
					'pk': id,
					'fields': row
				})

	# Write the fixture file.
	with open(outfile, 'w') as f:
		f.write(json.dumps(output, indent = 2))
