#!/usr/bin/env python

"""
Imports geometries from a spatial file and places them in the spatial_library and spatial_library geometry tables.
"""

import json
import os
import psycopg2
from osgeo import ogr
from map_thumb import *

connection = 'dbname=sap user=rob'

if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description='Load the geometry data as an entry in the spatial library.')
    parser.add_argument('userid', metavar='user id', type=int, help='The user\'s ID.')
    parser.add_argument('srid', metavar='srid', type=int, help='The SRID of the source file.')
    parser.add_argument('name', metavar='name', type=str, help='A name for the library entry.')
    parser.add_argument('note', metavar='note', type=str, help='A note for the library entry.')
    parser.add_argument('file', metavar='input file', type=str, help='The input file. A Shapefile, SQLite, etc.')
    parser.add_argument('layer', metavar='data layer', type=str,
                        help='The layer where data are stored. Leave blank if there\'s a default.')
    parser.add_argument('geom', metavar='geom field', type=str,
                        help='The name of the geometry field in the source file.')
    args = parser.parse_args()

    print('Running', args)

    conn = psycopg2.connect(connection)
    conn.autocommit = False
    cur = conn.cursor()

    cur.execute(
        'insert into rov.spatial_library (name, note, created_by_id, original_file) values (%s, %s, %s, %s) returning id',
        [args.name, args.note, args.userid, os.path.basename(args.file)])
    id, = cur.fetchone()

    ds = ogr.Open(args.file, 0)
    if args.layer:
        layer = ds.GetLayerByName(args.layer)
    else:
        layer = ds.GetLayer()

    shapes = []

    for feat in layer:
        if args.geom:
            geom = feat.GetGeomFieldRef(args.geom)
        else:
            geom = feat.GetGeometryRef()

        meta = {}
        for i in range(feat.GetFieldCount()):
            fdef = feat.GetFieldDefnRef(i)
            name = fdef.GetName()
            type = fdef.GetType()
            meta[name] = feat.GetFieldAsString(i)

        gtxt = geom.ExportToJson()
        mtxt = json.dumps(meta)

        shapes.extend(read_geojson(gtxt))

        cur.execute('''
			insert into rov.spatial_library_geometry (spatiallibrary_id, metadata, geom) 
			values (%s, %s, st_transform(st_setsrid(st_geomfromgeojson(%s), %s), %s))
		''', [id, mtxt, gtxt, args.srid, 4326])

    thumb = render_png(shapes, 100, 100)

    cur.execute('update rov.spatial_library set thumbnail=%s where id=%s', [thumb, id])

    conn.commit()

    print('Done')
