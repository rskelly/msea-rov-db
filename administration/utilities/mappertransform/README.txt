# Label Mapper JSON Transformation

The label mapper was updated to be simpler and easier to use. As part of this process, the JSON had to be transformed. The affected pats of each JSON packet involves the labels in the label tree. The original structure is below. Notice that the label elements with configurations are in the wrong tree: they should be in the trees contained in the project object, but they're in the main label tree list.

The element below contains a taxonomic observation with configurations. The tags section contains a list of objects with a label and propName property. This will be reduced to a list of strings containing the label properties. The tagData section will be renamed properties but will otherwise maintain the same structure. The properties of some elements have been changed. 

In instances where a property stored a referenced object, the ID will now be stored, rather than its ID. Three new properties are added to each object, which are used for configuration of the label mapper, eventType, subEvent and subSubEvent. These contain the keys of the event types that were used to populate the event type drop-downs.

    {
        "id": 13114,
        "name": "Acanthogorgia sp., yellow (CA32)",
        "tags":
        [
            {
                "label": "Observation",
                "propName": "observation"
            },
            {
                "label": "Species",
                "propName": "observation/species"
            }
        ],
        "_type": "biigle",
        "tagData":
        {
            "observation/species/otu": "CA32",
            "observation/species/aphia_id": "125293",
            "observation/species/hart_code": "3TD",
            "observation/species/common_name": "Acanthogorgia",
            "observation/species/inaturalist_id": "459889",
            "observation/species/scientific_name": "Acanthogorgia",
            "test/property": {"id": 1, "label": "Label}
        },
        "parent_id": 11628
    },

The updated object is shown below.

    {
        "id": 13114,
        "name": "Acanthogorgia sp., yellow (CA32)",
        "eventType": "observation/species",
        "subEvent": null,
        "subSubEvent": null,
        "tags": 
        [
            "Observation", 
            "Species"
        ],
        "_type": "biigle",
        "properties":
        {
            "observation/species/otu": "CA32",
            "observation/species/aphia_id": "125293",
            "observation/species/hart_code": "3TD",
            "observation/species/common_name": "Acanthogorgia",
            "observation/species/inaturalist_id": "459889",
            "observation/species/scientific_name": "Acanthogorgia",
            "test/property": 1
        },
        "parent_id": 11628
    },


    {
        "project":
        {
            "id": 1113,
            "name": "DFO PAC2021-036 Seamount Survey",
            "_type": "biigle",
            "members":
            {
                "214":
                {
                    "member":
                    {
                        "id": 214,
                        "last_name": "de Moura Neves",
                        "first_name": "Barbara"
                    },
                    "person":
                    {
                        "id": 2519,
                        "bio": null,
                        "email": "barbara.neves@dfo-mpo.gc.ca",
                        "photo": null,
                        "last_name": "Neves",
                        "first_name": "Barbara",
                        "biigle_uuid": null,
                        "biigle_user_id": null
                    }
                },

                ...

                "3263":
                {
                    "member":
                    {
                        "id": 3263,
                        "last_name": "Nielsen",
                        "first_name": "Ashley"
                    },
                    "person":
                    {
                        "id": 2708,
                        "bio": null,
                        "email": "ashley.nielsen@dfo-mpo.gc.ca",
                        "photo": null,
                        "last_name": "Nielsen",
                        "first_name": "Ashley",
                        "biigle_uuid": null,
                        "biigle_user_id": null
                    }
                }
            },
            "labelTrees":
            [
                {
                    "id": 1,
                    "name": "Global",
                    "_type": "biigle",
                    "labels":
                    [
                        {
                            "id": 1558,
                            "name": "Laser Point",
                            "_type": "biigle",
                            "color": "ff2600",
                            "parent_id": null,
                            "source_id": null,
                            "label_tree_id": 1,
                            "label_source_id": null
                        },
                        {
                            "id": 10493,
                            "name": "lost+found",
                            "_type": "biigle",
                            "color": "c7c7c7",
                            "parent_id": null,
                            "source_id": null,
                            "label_tree_id": 1,
                            "label_source_id": null
                        }
                    ],
                    "description": "The global label tree.",
                    "internal_id": "225,107,119,217,211,138,26,41,208,45,171,175,116,214,50,89,74,101,83,221,98,56,13,57,220,168,32,200,145,191,193,7"
                },

                ...

                {
                    "id": 181,
                    "name": "NE Pacific Seamounts (DFO)_image labels",
                    "_type": "biigle",
                    "labels":
                    [
                        {
                            "id": 12551,
                            "name": "1a_Image label: Not annotated",
                            "_type": "biigle",
                            "color": "ff0000",
                            "parent_id": null,
                            "source_id": null,
                            "label_tree_id": 181,
                            "label_source_id": null
                        },

                        ...

                        {
                            "id": 14120,
                            "name": "VME",
                            "_type": "biigle",
                            "color": "f26e30",
                            "parent_id": null,
                            "source_id": null,
                            "label_tree_id": 181,
                            "label_source_id": null
                        }
                    ],
                    "description": null,
                    "internal_id": "238,149,53,95,113,100,243,146,151,35,5,242,61,46,111,44,159,203,230,131,96,59,122,255,42,141,154,41,125,179,238,251"
                }
            ],
            "description": "Expedition using the CCGS Tully and BOOTS to survey the Canadian Pacific offshore seamounts (June 2021)"
        },
        "version": 1,
        "labelTrees":
        [
            {
                "id": 1,
                "name": "Global",
                "_type": "biigle",
                "labels":
                [
                    {
                        "id": 1558,
                        "name": "Laser Point",
                        "tags":
                        [
                            {
                                "label": "Status",
                                "propName": "status"
                            },
                            {
                                "label": "Laser Point",
                                "propName": "status/laser_point"
                            }
                        ],
                        "_type": "biigle",
                        "tagData":
                        {},
                        "parent_id": null
                    },
                    {
                        "id": 10493,
                        "name": "lost+found",
                        "tags":
                        [
                            {
                                "label": "Ignore",
                                "propName": "ignore"
                            }
                        ],
                        "_type": "biigle",
                        "tagData":
                        {},
                        "parent_id": null
                    }
                ],
                "description": "The global label tree.",
                "internal_id": "225,107,119,217,211,138,26,41,208,45,171,175,116,214,50,89,74,101,83,221,98,56,13,57,220,168,32,200,145,191,193,7"
            },
            {
                "id": 169,
                "name": "NE Pacific Seamounts (DFO)_annotation",
                "_type": "biigle",
                "labels":
                [
                    {
                        "id": 11678,
                        "name": "2_Anthropogenic",
                        "tags":
                        [
                            {
                                "label": "Ignore",
                                "propName": "ignore"
                            }
                        ],
                        "_type": "biigle",
                        "tagData":
                        {},
                        "parent_id": null
                    },

                    ...

                    {
                        "id": 13114,
                        "name": "Acanthogorgia sp., yellow (CA32)",
                        "tags":
                        [
                            {
                                "label": "Observation",
                                "propName": "observation"
                            },
                            {
                                "label": "Species",
                                "propName": "observation/species"
                            }
                        ],
                        "_type": "biigle",
                        "tagData":
                        {
                            "observation/species/otu": "CA32",
                            "observation/species/aphia_id": "125293",
                            "observation/species/hart_code": "3TD",
                            "observation/species/common_name": "Acanthogorgia",
                            "observation/species/inaturalist_id": "459889",
                            "observation/species/scientific_name": "Acanthogorgia"
                        },
                        "parent_id": 11628
                    },

                    ...


                    {
                        "id": 14120,
                        "name": "VME",
                        "tags":
                        [
                            {
                                "label": "Ignore",
                                "propName": "ignore"
                            }
                        ],
                        "_type": "biigle",
                        "tagData":
                        {},
                        "parent_id": null
                    }
                ],
                "description": null,
                "internal_id": "238,149,53,95,113,100,243,146,151,35,5,242,61,46,111,44,159,203,230,131,96,59,122,255,42,141,154,41,125,179,238,251"
            }
        ],
        "annotationProtocol":
        {
            "id": 1010,
            "name": "PAC2021-036_video",
            "note": null,
            "creator":
            {
                "id": 2678,
                "bio": null,
                "email": "merlin.best@dfo-mpo.gc.ca",
                "photo": null,
                "last_name": "Best",
                "first_name": "Merlin",
                "biigle_uuid": null,
                "biigle_user_id": null
            },
            "documents":
            [
                {
                    "id": 47,
                    "url": "https://www.inaturalist.org/guides/11538",
                    "note": null,
                    "title": "iNatualist Species Guide",
                    "file_name": null,
                    "file_type": null,
                    "created_on": "2023-09-15T17:35:52Z",
                    "updated_on": "2023-09-15T17:35:52Z",
                    "annotation_protocol_id": 1010
                }
            ],
            "created_on": "2023-08-12T00:00:18Z",
            "updated_on": "2023-09-15T18:08:44Z",
            "is_template": false,
            "medium_type":
            {
                "id": 2,
                "name": "Video",
                "note": null,
                "short_code": "video           "
            },
            "fish_species": "-- Select One --",
            "fov_interval": null,
            "habitat_only": false,
            "algae_species": "-- Select One --",
            "image_overlap": false,
            "species_guide": null,
            "image_interval": null,
            "biogenic_habitat": false,
            "habitat_interval": null,
            "fov_interval_unit": "s",
            "protocol_document": null,
            "annotation_software":
            {
                "id": 18,
                "name": "Biigle",
                "note": null,
                "short_code": "biigle          "
            },
            "image_interval_unit": "s",
            "invertebrate_species": "-- Select One --",
            "observation_interval": null,
            "habitat_interval_unit": "s",
            "observation_interval_unit": "s"
        }
    }

