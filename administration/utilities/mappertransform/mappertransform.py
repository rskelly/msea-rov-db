#!/usr/bin/env python

"""
Loads import queue configration data from the database and transforms the JSON structure into a format useable by 
the updated label mapper.
"""

import sys
import os
import json

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))), 'app'))

from msea.util import util

"""
The mapping of old keys to new keys. Each value is a tuple containing the new key and a tuple containing
any keys that the new key depends on.
"""
newkeys = {
    'comment': 'comment',

    'habitat': 'habitat',
    'habitat/biocover': 'habitat/biocover',
    'habitat/biocover/species/common_name': 'habitat/common_name', 
    'habitat/biocover/species/scientific_name': 'habitat/scientific_name', 
    'habitat/biocover/species/aphia_id': 'habitat/aphia_id', 
    'habitat/biocover/species/inaturalist_id': 'habitat/inaturalist_id', 
    'habitat/biocover/species/hart_code': 'habitat/hart_code', 
    'habitat/biocover/common_name': 'habitat/common_name', 
    'habitat/biocover/scientific_name': 'habitat/scientific_name', 
    'habitat/biocover/aphia_id': 'habitat/aphia_id',
    'habitat/biocover/inaturalist_id': 'habitat/inaturalist_id', 
    'habitat/biocover/hart_code': 'habitat/hart_code', 
    'habitat/biocover/otu': 'habitat/otu', 
    'habitat/biocover/coverage': 'habitat/coverage', 
    'habitat/biocover/type': 'habitat/biocover', 
    'habitat/biocover/biocover': 'habitat/biocover', 
    'habitat/biocover/confidence': 'habitat/confidence', 
    'habitat/biocover/abundance': 'habitat/abundance', 
    'habitat/biocover/restriction': 'habitat/restriction', 

    'habitat/species': 'habitat', 
    'habitat/species/common_name': 'habitat/common_name', 
    'habitat/species/scientific_name': 'habitat/scientific_name', 
    'habitat/species/aphia_id': 'habitat/aphia_id', 
    'habitat/species/hart_code': 'habitat/hart_code', 
    'habitat/species/inaturalist_id': 'habitat/inaturalist_id', 
    'habitat/species/otu': 'habitat/otu', 
    'habitat/species/coverage': 'habitat/coverage', 
    'habitat/species/type': 'habitat/biocover', 
    'habitat/species/confidence': 'habitat/confidence', 
    'habitat/species/abundance': 'habitat/abundance', 
    'habitat/species/restriction': 'habitat/restriction', 

    'habitat/substrate': 'habitat', 
    'habitat/substrate/complexity': 'habitat/complexity', 
    'habitat/substrate/coverage': 'habitat/coverage', 
    'habitat/substrate/disturbance': 'habitat/disturbance', 
    'habitat/substrate/relief': 'habitat/relief', 
    'habitat/substrate/restrictions': 'habitat/restrictions', 
    'habitat/substrate/species/common_name': 'habitat/common_name', 
    'habitat/substrate/species/scientific_name': 'habitat/scientific_name', 
    'habitat/substrate/species/aphia_id': 'habitat/aphia_id', 
    'habitat/substrate/species/hart_code': 'habitat/hart_code', 
    'habitat/substrate/species/inaturalist_id': 'habitat/inaturalist_id', 
    'habitat/substrate/species/otu': 'habitat/otu', 
    'habitat/substrate/coverage': 'habitat/coverage', 
    'habitat/substrate/type': 'habitat/substrate', 
    'habitat/substrate/substrate': 'habitat/substrate', 
    'habitat/substrate/confidence': 'habitat/confidence', 
    'habitat/substrate/abundance': 'habitat/abundance', 
    'habitat/substrate/restriction': 'habitat/restriction', 
    'habitat/substrate/common_name': 'habitat/common_name', 
    'habitat/substrate/scientific_name': 'habitat/scientific_name', 
    'habitat/substrate/aphia_id': 'habitat/aphia_id', 
    'habitat/substrate/inaturalist_id': 'habitat/inaturalist_id', 
    'habitat/substrate/hart_code': 'habitat/hart_code', 
    'habitat/substrate/otu': 'habitat/otu', 
    
    'ignore': 'ignore', 

    'observation/anthropogenic': 'observation', 
    'observation/anthropogenic/common_name': 'observation/common_name', 
    'observation/anthropogenic/hart_code': 'observation/hart_code', 
    'observation/anthropogenic/confidence': 'observation/anthropogenic/confidence',
    'observation/anthropogenic/restrictions': 'observation/restrictions',

    'observation/species': 'observation',
    'observation/species/abundance': 'observation/abundance',
    'observation/abundance': 'observation/abundance',
    'observation/species/aphia_id': 'observation/aphia_id',
    'observation/species/common_name': 'observation/common_name',
    'observation/species/confidence': 'observation/confidence',
    'observation/confidence': 'observation/confidence',
    'observation/species/hart_code': 'observation/hart_code',
    'observation/species/inaturalist_id': 'observation/inaturalist_id',
    'observation/species/otu': 'observation/otu',
    'observation/species/restriction': 'observation/restriction',
    'observation/restrictions': 'observation/restriction',
    'observation/species/scientific_name': 'observation/scientific_name',

    'status': 'status',
    'status/image_quality': 'status/type',
    'status/laser_point': 'status/type',
    'status/off_transect': 'status/type',
    'status/survey_mode': 'status/type',
    'status/survey_protocol': 'status/type',
    'status/off_bottom': 'status/type',
    'off_bottom': 'status/type',
    'status/on_bottom': 'status/type',
    'on_bottom': 'status/type',
    'status/not_annotated': 'status/type',
    'not_annotated': 'status/type',
    'status/on_transect': 'status/type',
    'on_transect': 'status/type',
}

prop2tag = {
    'habitat/substrate/dominant': ['Dominant'],
    'habitat/substrate/subdominant': ['Subdominant'],
    'observation/group': ['Group'],
    'observation/dead': ['Dead'],
    'observation/drifting': ['Drifting'],
    'observation/large': ['Large'],
    'observation/medium': ['Medium'],
    'observation/small': ['Small'],
    'observation/egg_casings': ['Egg Casings'],
    'habitat/substrate/dead': ['Dead'],
    'habitat/substrate/alive': ['Alive'],
    'habitat/substrate/rubble': ['Rubble'],
    'habitat/substrate/reef': ['Reef'],
    'habitat/biocover/reef': ['Reef'],
    'habitat/biocover/alive': ['Alive'],
    'habitat/biocover/dead': ['Dead'],
    'habitat/substrate/silt': ['Silt'],
    'observation/encrusting': ['Encrusting'],
    'observation/dead_partial': ['Partially Dead'],
    'observation/toppled': ['Toppled'],
    'status/indicators': ['Indicator'],
    'status/indicators/high_flow': ['High Flow'],
    'status/indicators/low_flow': ['Low Flow'],
    'status/indicators/no_flow': ['No Flow'],
    'status/indicators/no_flow_indicator': ['No Reliable Flow Indicator'],
    'observation/detached': ['Detached'],
    'observation/attached': ['Attached'],
    'observation/mat': ['Mat'],
    'observation/patches': ['Patches'],
    'observation/solitary': ['Solitary'],
    'habitat/biocover/small': ['Small'],
    'habitat/biocover/medium': ['Medium'],
    'habitat/biocover/large': ['Large'],
    'habitat/biocover/clumps': ['Clumps'],
    'habitat/biocover/blocks': ['Blocks'],
    'habitat/biocover/rubble': ['Rubble'],
    'habitat/biocover/big': ['Large'],
    'habitat/biocover/dominant': ['Dominant'],
    'habitat/biocover/subdominant': ['Subdominant'],
}

tag2prop = {
    'Ignore': 'ignore',
    'Comment': 'comment',
    'Laser Point': 'status/type',
    'Image Quality': 'status/type',
    'Not Annotated': 'status/type',
    'Survey Protoco': 'status/type',
    'Survey Mode': 'status/type',
    'On Bottom': 'status/type',
    'Off Bottom': 'status/type',
    'Off Transect': 'status/type',
    'On Transect': 'status/type'
}

ignore = {
    'habitat/biocover/species',
    'habitat/substrate/species',
}

tagrem = [
    'Habitat', 'Substrate', 'Biocover', 'Observation', 'Species', 'Anthropogenic', 'Type',
    'Status', 'Coverage', 'Disturbance', 'Relief', 'Complexity', 'Restrictions', 'Confidence', 'Abundance',
    'Ignore',
]

"""
Tracks the keys that were missing from the newkeys dict. If this list is
non-empty on completion: None,the result cannot be committed.
"""
missingkeys = set()
removedtags = set()
appendedtags = set()

def transform_label(label):
    """
    Transform the label's configuration.
    """
    # Rename tagData to properties.
    if label.get('tagData') != None:
        label['properties'] = label['tagData']
        del label['tagData']

    tags = label.get('tags', [])
    properties = label.get('properties', {})

    # Collapse tags into strings using the label.
    for i in range(len(tags)):
        tag = tags[i]
        if type(tag) == str:
            pass
        else:
            tags[i] = tag['label']
            if tag.get('propName') and not tag['propName'] in properties.keys():
                properties[tag['propName']] = None


    # Process the tags.
    tags0 = []
    for tag in tags:
        if tag in tagrem:
            removedtags.add(tag)
        elif tag in tag2prop.keys():
            k = tag2prop[tag]
            if k and not k in properties.keys():
                properties[k] = None
        else:
            appendedtags.add(tag)
            tags0.append(tag)
    tags = tags0

    # Transform the key names and add dependencies.
    properties0 = {}
    for key, data in properties.items():
        newkey = newkeys.get(key)
        newtags = prop2tag.get(key)
        ign = key in ignore
        
        if ign:
        
            continue
        
        elif newkey:

            # Update the event type.
            types = newkey.split('/')
            label['eventType'] = types[0]

            # Update the properties if one isn't already set.
            if not properties0.get(newkey):
                properties0[newkey] = data['id'] if type(data) == dict else data

        elif newtags:
            tags.extend(newtags)

        else:
        #    raise Exception('Something wrong ' + key)
            print('Ignoring key', key)

    # Populate the updated tags, props.
    label['properties'] = properties0
    label['tags'] = list(set(tags))


def has_label_config(trees):
    """
    Return true if any of the labels in the trees have label map configurations.
    """
    for tree in trees:
        for label in tree['labels']:
            if label.get('tags') or label.get('tagData') or label.get('properties'):
                return True
    return False

def merge_trees(project_trees, source_trees):
    """
    Move the label configs from the source trees into the project trees.
    Return a tuple containing the source trees and project trees, in that order.
    """
    for i in range(len(project_trees)):
        # Get the trees and sort their labels.
        pt = project_trees[i]
        st = source_trees[i]
        pt['labels'] = sorted(pt['labels'], key=lambda a: a['id'])
        st['labels'] = sorted(st['labels'], key=lambda a: a['id'])
        # Iterate over the labels.
        for j in range(len(pt['labels'])):
            pl = pt['labels'][j]
            sl = st['labels'][j]
            # If there's no pl tags, create a list.
            if pl.get('tags') is None:
                pl['tags'] = []
            # If there are sl tags, copy them to the pl.
            pl['tags'].extend(sl.get('tags', []))
            # Get the properties from the sl.
            props = {}
            props.update(sl.get('tagData', {}))
            props.update(sl.get('properties', {}))
            # If there's no pl properties, create a dict.
            if pl.get('properties') is None:
                pl['properties'] = {}
            # Copy the sl properties to pl.
            for k, v in props.items():
                pl['properties'][k] = v
            # Remove configs from the sl.
            try:
                del sl['tags']
            except: pass
            try:
                del sl['tagData']
            except: pass
            try:
                del sl['properties']
            except: pass
    return source_trees, project_trees

def transform_job(job):
    """
    Transform the project by transforming the labels and moving them to the 
    from the source trees to the trees inside the project data.
    """
    project_trees = job['project']['labelTrees']
    source_trees = job['labelTrees']

    # Figure out if the trees have label configs.
    pt_has_config = has_label_config(project_trees)
    st_has_config = has_label_config(source_trees)

    if pt_has_config and not st_has_config:
        # project tree has label configs. this is good. leave it.
        pass
    elif st_has_config and not pt_has_config:
        # source tree has configs. switch them.
        tmp = job['project']['labelTrees'] = source_trees
        source_trees = job['labelTrees'] = project_trees
        project_trees = tmp
    elif st_has_config and pt_has_config:
        # both trees have configs. merge them. and remove from the source tree.
        source_trees, project_trees = merge_trees(project_trees, source_trees)
        pass
    else:
        # there are no configs. do nothing.
        pass

    # Process the labels.
    for tree in project_trees:
        for label in tree['labels']:
            transform_label(label)

def run(config_file):

    config = util.load_config(config_file)
    conn = util.db_connect(config)
    cur = conn.cursor()

    cur.execute('select id, data from rov.import_queue_annotator')
    items = []
    for id, data in cur:
        transform_job(data)
        items.append((id, data))
    
    if len(missingkeys):
        print('Missing keys:')
        print(missingkeys)

    for id, data in items:
        cur.execute('update rov.import_queue_annotator set data=%s::json where id=%s', [json.dumps(data), id])

if __name__ == '__main__':

    run(sys.argv[1])

    print('appended')
    print(appendedtags)
    print('removed')
    print(removedtags)
    # for k in sorted(list(missingkeys)):
    #     parts = k.split('/')
    #     kk = []
    #     for i in range(1, min(len(parts), 3)):
    #         kk.append('/'.join(parts[:i]))
    #     kk = "['" + "','".join(kk) + "']" if len(kk) else '[]'
    #     print(f"'{k}': ['{k}', {kk}],")
