#!/usr/bin/env python

"""
Find the gaps in timeseries or when depth reaches (near) zero to delineate dives.
"""

import csv
import sys
from datetime import datetime

filename = sys.argv[1]

with open(filename, 'r') as f:
	db = csv.reader(f)
	head = next(db)
	_dt = None
	_row = None
	_st = None
	for row in db:
		row = dict(zip(head, row))
		d = row['date']
		t = row['time'][:row['time'].find('.')]
		dt = datetime.strptime('{} {}'.format(d, t), '%m/%d/%Y %H:%M:%S')
		if not _st:
			_st = dt
		if _dt and (dt - _dt).total_seconds() > 600 and float(row['depth']) < 10:
			print(_row['id'], _st, _dt, _row['depth'])
			_st = dt
		_dt = dt
		_row = row
	print(_row['id'], _st, _dt, _row['depth'])
