# WoRMS

These are utilities for working with the WoRMS database and entries in taxonomy.worms_taxon.

## update_worms.py

Updates entries in the taxon table by iterating through the AphiaIDs and updating the records. Also loads
the synonyms of each entry.

