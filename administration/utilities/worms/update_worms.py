"""
Iterate through the Aphia IDs in taxonomy.worms_taxon and update each record from the ReST API.
Also updates synonymous records.
"""

import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))), 'app'))

from msea.util import django

def run(config_file):

    django.django_setup(config_file)

    from msea.models.shared import WoRMSTaxon, Taxon

    
    taxa = WoRMSTaxon.objects.filter(aphiaid__in=Taxon.objects.exclude(aphia_id=None)).distinct().order_by('aphiaid')
    i = 1
    n = len(taxa)
    for taxon in taxa:
        print(f'Processing {taxon.scientificname} ({taxon.aphiaid}) ({i} of {n})')
        if taxon.needs_update():
            WoRMSTaxon.get_from_aphiaid_remote(taxon.aphiaid)
            #WoRMSTaxon.get_synonyms(taxon.aphiaid)
        else:
            print('Skipped')
        i += 1


if __name__ == '__main__':

    config_file = sys.argv[1]
    
    run(config_file)


