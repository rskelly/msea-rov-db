#!/usr/bin/env python

import os
import sys

file = sys.argv[1]

try:
    mode = sys.argv[2]
except:
    mode = 'h'

# https://stackoverflow.com/a/23646049
def reverse_readline(filename, buf_size=8192):
    """A generator that returns the lines of a file in reverse order"""
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split('\n')
            # The first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # If the previous chunk starts right from the beginning of line
                # do not concat the segment to the last line of new chunk.
                # Instead, yield the segment first 
                if buffer[-1] != '\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if lines[index]:
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment


n = 10

if mode == 'h':
    with open(file, 'r', encoding = 'utf-8') as f:
        line = f.readline()
        l = 0
        while line:
            if l > n:
                print(line)
            line = f.readline()
            l += 1
elif mode == 't':
    lines = []
    l = 0
    for line in reverse_readline(file):
        lines.append(line)
        if l > n:
            break
        l += 1
    for line in lines[::-1]:
        print(line)
elif mode == 'l':
    with open(file, 'r', encoding = 'utf-8') as f:
        line = f.readline()
        l = 0
        while line:
            line = f.readline()
            l += 1
        print(l, 'lines')

