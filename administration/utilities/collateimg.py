#!/usr/bin/env python

"""
Get images from the folders recursively and put them in one folder.
"""

import os
import sys
import shutil

indir = sys.argv[1]
outdir = sys.argv[2]


def crawl(dirname, lst=[]):
    for f in os.listdir(dirname):
        if os.path.isdir(f) and f not in ('.', '..'):
            crawl(os.path.join(dirname, f), lst)
        elif f.lower().endswith('.jpg'):
            lst.append(os.path.join(dirname, f))
    return lst


lst = crawl(indir)

print('Found', len(lst), 'files')

try:
    os.makedirs(outdir)
except:
    pass

print('Copying...')

for f in lst:
    shutil.copyfile(f, os.path.join(outdir, os.path.basename(f)))

print('Done')
