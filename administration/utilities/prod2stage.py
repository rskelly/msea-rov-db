#!/usr/bin/env python

# Dumps the production database and restores it to stage. Backs
# up the staging database.

import os
import sys
from msea.util import util

try:
    mode = sys.argv[1]
except:
    mode = 0

def run(cmd):
    """
    Run the command or just print it depending on the mode.
    """
    print(cmd)
    if mode:
        os.system(cmd)

prod_config = util.load_config('~/configs/db_prod_admin.conf')
stage_config = util.load_config('~/configs/db_stage_admin.conf')

#run("sudo service postgresql restart")
#run(f"PGPASSWORD={prod_config['password']} pg_dump -U {prod_config['user']} -d {prod_config['dbname']} > /tmp/prod_dump.sql")
#run(f"PGPASSWORD={stage_config['password']} pg_dump -U {stage_config['user']} -d {stage_config['dbname']} > /tmp/stage_dump.sql")
run(f"PGPASSWORD={stage_config['password']} dropdb -U {stage_config['user']} {stage_config['dbname']}")
run(f"PGPASSWORD={stage_config['password']} createdb -U {stage_config['user']} {stage_config['dbname']}")
run(f"PGPASSWORD={stage_config['password']} psql -U {stage_config['user']} -d {stage_config['dbname']} -f /tmp/prod_dump.sql")

