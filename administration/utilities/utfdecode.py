"""
Decode and re-encode a UTF-8 file, replacing the errors with nothing.
"""

import codecs
import sys


def decode(infile, outfile):
    with open(outfile, 'w', encoding='utf-8') as o:
        with codecs.open(infile, encoding='utf-8', errors='replace') as f:
            while True:
                chunk = f.read(4096)
                chunk = chunk.replace('\ufffd', '')
                if not chunk:
                    break
                o.write(chunk)


if __name__ == '__main__':
    infile = sys.argv[1]
    outfile = sys.argv[2]

    decode(infile, outfile)
