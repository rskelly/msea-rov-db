# Utilities

Programs used for various purposes in database development, productivity, etc.

These may rot as the database structure evolves.


## Reporting

These are things used for producing or managing reports.

### bibtex.py

Consumes a bibtex file (e.g., from Mendeley) and produces a bibliography for the library Wiki page.

### bibtex2sql.py

Consumes a bibtex file (e.g., from Mendeley) and produces SQL to update the library database (supercedes `bibtex.py`).

### heatmap.py

Produces a TIF-format heamap of species observations.

### map_thumb.py

Renders map thumbnails from the vectors stored with records by overlaying them on a google map.


## Data Import

These scripts are for data import, or preparing data for import.

### dive_times.py

Divides a tracking file into "dives" by finding gaps in times or when the depth approaches zero. Duplicates a similar script in the import directory. (TODO: Figure out which to keep.)

### format_minilog.py

Parses a series of minilog files with .### extensions, in order to try to compile
a complete dataset. Will parse dates into m/d/y format because that's
just customary at this point.

The date format can be mixed. This tries to fix that and orders all the entries by time. It's a 
naive algorithm so it could use a lot of memory.

### interpolate_in_column.py

Read a CSV file and interpolate the values between two numeric values in the given columns such that there are
no gaps within the column. Non-numeric cells and blanks are allowed at each end. A non-numeric cell within
a range of numbers with cause an error.

Columns are given by zero-based index, since there is no concept of column names.

### mdb_convert.py

Extracts MDB files, including old ones, to a set of CSV files in the target folder.

### seabird_convert.py

Converts seabirt .CNV file sets into a single CSV file formatted for input to the database.

### spatial_library_add.py

Imports spatial data form files into a spatial library in the database.

### utfdecode.py

Decode and re-encode a UTF-8 file, replacing the errors with nothing.


## Data Management

These scripts are for managing data in the database.

### consolidate_person.py

This script looks through the shared.person table and finds case-insensitive email and name duplicates, giving the user the option to select one to replace the others with. The unneeded duplicates are deleted.

CAUTION: This does not accommodate multiple people with the same first and last name. Select the zero option to do nothing and fix those manually if necessary.

## Database Structure

These scripts help to modify the structure, configuration or run-time properties of the database.

### db_permissions.sql

An SQL script that grants permissions to the production database user. The website
will not run without this!


## Django

These scripts work with Django.

### generate_fixtures.py

This script searches through the Django model classes for those marked `__permanent = True` and creates a 
Django fixture file for each. The file can be loaded into a new database (e.g., the testing database) to give
a baseline collection of entities for lookups, etc.

`generate_fixtures.py` uses a dependency graph to calculate the order of output. If a circular dependency occurs, the 
structure of the application is likely broken and should be fixed.

### json2fixture.py

Produces Django fixture files from a simpler, more efficient JSON format that is easier to maintain. Format documentation is in the script.

### taxonomy_export_to_fixture.py

Exports taxonomy tables to the fixture format used by `json2fixture.py`.


## Other

### head.py

A naive replacement for the `head` program available on linux.

### collateimg.py

Crawl a file structure and place all the image files in one folder.

### worms_match.py

Finds matches in the WoRMS database for entries in the taxonomy table. Not needed because of the audit tool.


## Obsolete.

### obis_taxon_tree.py

Consumes the parquet file produced by OBIS, which contains the complete observation record
included in the WoRMS database. Reads sequentially through the rows, creating a unique
record for each AphiaID/Scientific Name and connecting the record to the next-higher taxon. Calculates
the geographic boundary of observations from coordinates stored in each record.

If the higher-level taxon isn't in the dataset, it is retrieved from WoRMS via the REST API
and added to the database. 

### prod2stage.md

Dumps the production database and restores it to stage. Backs
up the staging database.

### update_file_meta.py

This script generates file hashes for files stored in the
rov.file table. The argument is a root folder onto which the
file path is appended.