#!/usr/bin/env python

"""
Open an Access database (including old ones) and export the tables to csv files stored in a target folder.
"""

import pyodbc
import os
import sys


def extract(infile, outdir):
	"""
	Extract tables from the Access database in infile, and
	write CSV tables to outdir.
	"""

	try:
		os.makedirs(outdir)
	except: pass

	conn = pyodbc.connect(r'Driver={{Microsoft Access Driver (*.mdb, *.accdb)}};DBQ={};'.format(infile))
	cur = conn.cursor()

	for table in cursor.tables():
		cur.execute('SELECT * FROM "{}"'.format(table.table_name))
		fields = [f[0] for f in cur.description]
		csvfile = os.path.join(outdir, '{}.csv'.format(table.table_name))
		with open(csvfile, 'w') as f:
			f.write('{}\n'.format(','.join(fields)))
			for row in cur:
				f.write('{}\n', ','.join(list(map(str, row))))


if __name__ == '__main__':

	infile = sys.argv[1]
	outdir = sys.argv[2]

	extract(infile, outdir)