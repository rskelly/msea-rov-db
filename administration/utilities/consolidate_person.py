#!/usr/bin/python

'''
Consolidate names in the personnel table by checking first and last names
and the case of email addresses.
'''

import sys

from msea.util import util

sel = 'select id,first_name,last_name,email from shared.person'
update = [
	'update rov.dive_crew set person_id=%s where person_id=%s',
	'update rov.cruise_crew set person_id=%s where person_id=%s',
	'update rov.program_member set person_id=%s where person_id=%s',
	'update rov.project_member set person_id=%s where person_id=%s',
	'update rov.event_logger set person_id=%s where person_id=%s',
]
delu = 'delete from shared.person where id=%s'
lower = 'update shared.person set email=lower(email)'


def update_records(cur, from_id, to_id):
	'''
	Change the person ID in each query from from_id to to_id.
	'''
	if from_id != to_id:
		for q in update:
			cur.execute(q, [to_id, from_id])


def run(config):

	config = util.load_config(config)

	# Connect
	conn = util.db_connect(config)
	cur = conn.cursor()

	try:

		cur.execute('begin')

		# Check for email duplicates.
		print('Checking email case for duplication.')

		cur.execute(sel)

		by_email = {}
		choices = {}
		del_users = set()

		for id, first_name, last_name, email in cur:
			
			# Lowercase the email and concatenate the names.
			email_ = email.lower()

			if not by_email.get(email_):

				# If not found, add the person with this email.
				by_email[email_] = {
					'first': first_name,
					'last': last_name,
					'id': id,
					'email': email
				}

			else:

				# There's a conflict. Create a list, add the original if needed and the new item.
				if not choices.get(email_):
					choices[email_] = [by_email[email_]]

				choices[email_].append({
					'first': first_name,
					'last': last_name,
					'id': id,
					'email': email
				})


		# Iterate over the choice groups and let the user choose which one to replace the others with.
		changed = 0
		for email, options in choices.items():

			print(f'There are {len(options)} duplicates for the email, {email}:')
			print(f"0: No change.")

			i = 1
			for option in options:
				print(f"{i}: Replace all duplicates with user {option['last']}, {option['first']}, {option['email']}.")
				i += 1

			res = 0
			while True:
				try:
					res = int(input('Choose a number: '))
					break
				except:
					print('Invalid input.')

			if res > 0:
				id_keep = options[res - 1]['id']
				for option in options:
					id_remove = option['id']
					if id_remove != id_keep:
						print(f'Replacing {id_remove} with {id_keep}.')
						update_records(cur, id_remove, id_keep)
						del_users.add(id_remove)
						changed += 1

		if changed:
			print(f'Fixed {changed} duplicates.')
		else:
			print('No duplicates found.')



		# Delete the duplicate users.
		if del_users:
			print(f'Deleting {len(del_users)} entries.')
			for id in del_users:
				cur.execute(delu, [id])


		print('Checking for duplicate names.')

		cur.execute(sel)

		by_name = {}
		choices = {}
		del_users = set()

		for id, first_name, last_name, email in cur:

			name = last_name.lower() + '_' + first_name.lower()

			if not by_name.get(name):
				# If not found, add the person with this nae.
				by_name[name] = {
					'first': first_name,
					'last': last_name,
					'id': id,
					'email': email
				}

			elif email.lower() != by_name[name]['email'].lower():

				# The name is the same but the email is different. Force a choice.
				if not choices.get(name):
					choices[name] = [by_name[name]]

				choices[name].append({
					'first': first_name,
					'last': last_name,
					'id': id,
					'email': email
				})


		# Iterate over the choice groups and let the user choose which one to replace the others with.
		changed = 0
		for name, options in choices.items():
			last, first = name.split('_')
			print(f'There are {len(options)} duplicates for the name, {last}, {first}:')
			print(f"0: No change.")

			i = 1
			for option in options:
				print(f"{i}: Replace all duplicates with user {option['last']}, {option['first']}, {option['email']}.")
				i += 1

			res = 0
			while True:
				try:
					res = int(input('Choose a number: '))
					break
				except:
					print('Invalid input.')

			if res > 0:
				id_keep = options[res - 1]['id']
				for option in options:
					id_remove = option['id']
					if id_remove != id_keep:
						update_records(cur, id_remove, id_keep)
						del_users.add(id_remove)
						changed += 1

		if changed:
			print(f'Fixed {changed} duplicates.')
		else:
			print('No duplicates found.')

		cur.execute(lower)

		# Delete the duplicate users.
		if del_users:
			print(f'Deleting {len(del_users)} entries.')
			for id in del_users:
				cur.execute(delu, [id])

		cur.execute('commit')

	except Exception as e:
		logging.warning(str(e))
		cur.execute('rollback')


if __name__ == '__main__':

	try:
		config = sys.argv[1]
		run(config)
	except Exception as e:
		logging.warning(str(e))
		print('Usage: consolidate_person.py <config>')
