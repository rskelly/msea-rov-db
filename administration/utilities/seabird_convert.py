#!/usr/bin/env python

"""
Convert Seabird CNV text files into CSV files, correcting the timezone offset, etc.
"""

import os
from datetime import datetime, timezone, timedelta

with open(f'ctd.csv', 'w') as fo:

	head = False

	for f in [f for f in os.listdir('.') if f.lower().endswith('.cnv')]:
		fields_s = set()
		fields_l = []

		if f.find('_') > -1:
			p = '_'
		else:
			p = ' '

		dive_name, _, dive_num = os.path.splitext(f)[0].partition(p)

		print(dive_name, dive_num)
		# TODO: Find the index of first number char and split there?
		try:
			dive_name = str(int(dive_num))
		except:
			dive_name = dive_num

		tz = timezone.utc
		if f.find('PDT') > -1 or f.find('Unk') > -1:
			tz = timezone(timedelta(hours = -7))
		elif f.find('PST') > -1:
			tx = timezone(timedelta(hours = -8))

		with open(f, 'r') as fi:
			mode = 0
			fields = []
			start_time = None
			timeS = None
			while True:
				line = fi.readline()
				if not line:
					break
				line = line.strip()
				if mode == 0:
					if line == '*END*':
						mode = 1
						continue
					elif line.startswith('# name'):
						_, _, b = line.partition(' = ')
						name, label = b.split(': ')
						fields.append(name)
					elif line.startswith('# start_time'):
						_, _, b = line.partition(' = ')
						date, _ = b.split(' [')
						start_time = datetime.strptime(date, '%b %d %Y %H:%M:%S').replace(tzinfo = tz).astimezone(timezone.utc)
				
				if mode == 1:
					values = line.split()
					for field in fields:
						if not field in fields_s:
							fields_l.append(field)
							fields_s.add(field)
					if not head:
						fo.write(','.join(['dive_name', 'timestamp'] + fields_l) + '\n')
						head = True
					mode += 1
				
				if mode == 2:
					values = line.split()
					
					ts = float(values[fields.index('timeS')])
					if timeS is not None and ts - timeS < 1.:
						continue
					timeS = ts

					dt = start_time + timedelta(seconds = float(values[fields.index('timeS')]))
					row = [dive_name, dt.strftime('%Y-%m-%dT%H:%M:%SZ')] + [''] * len(fields_l)
					
					for i in range(len(values)):
						row[fields_l.index(fields[i]) + 2] = values[i]
					
					fo.write(','.join(row) + '\n')
	



