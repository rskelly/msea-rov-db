#!/usr/bin/env python

"""
This script loads a bibtex bibliography file and updates the MSEA ROV database library.
"""

import os
import sys
import csv
import argparse
import json
import django
from django.db import transaction, connection
from datetime import datetime, timedelta, timezone
from bibtex import BibTexParser

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'import'))

# Import the utils to support this program.
from msea.util import util
from msea.util import django

# Setup the Django environment.
django.django_setup()

# Import the MSEA models. Has to be done after setting up Django.
from msea.models import *
from msea.forms import *

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Convert a BibText file into SQL for the MSEA ROV database.')
    parser.add_argument('connection', metavar='database connection', type=str,
                        help='Connection string. Enclose in quotes.')
    parser.add_argument('input', metavar='input file', type=str, help='Input file. BibTex format.')
    args = parser.parse_args()

    with transaction.atomic():

        with open(args.input, 'r', encoding='utf-8') as f:
            p = BibTexParser(f)
            records, meta = p.parse()

            for record in records:
                lib = None
                identifiers = {}
                for ident in record.get('identifier', []):
                    identifiers[ident['type']] = ident['id']

                # Try to read the item and update it if necessary.
                try:
                    lib = Library.objects.get(mendeley_id=record['id'])
                except:
                    pass
                try:
                    if identifiers.get('doi'):
                        lib = Library.objects.get(doi=identifiers['doi'])
                    elif identifiers.get('isbn'):
                        lib = Library.objects.get(isbn=identifiers['isbn'])
                    elif identifiers.get('issn'):
                        lib = Library.objects.get(issn=identifiers['issn'])
                except:
                    pass

                if lib:
                    lib.title = record['title']
                    lib.publication = record.get('booktitle', record.get('journal', {}).get('name'))
                    lib.authors = record.get('author')
                    lib.keywords = record.get('keyword')
                    lib.abstract = record.get('abstract')
                    lib.year = record.get('year')
                    lib.doi = identifiers.get('doi')
                    lib.isbn = identifiers.get('isbn')
                    lib.issn = identifiers.get('issn')
                    lib.type = record.get('type')
                    lib.path = os.path.basename(record.get('file')).split(':')[0] if record.get('file') else None
                else:
                    # It doesn't exist; create it.
                    lib = Library.objects.create(**{
                        'mendeleyid': record['id'],
                        'title': record['title'],
                        'publication': record.get('booktitle', record.get('journal', {}).get('name')),
                        'authors': record.get('author'),
                        'keywords': record.get('keyword'),
                        'abstract': record.get('abstract'),
                        'year': record.get('year'),
                        'doi': identifiers.get('doi'),
                        'isbn': identifiers.get('isbn'),
                        'issn': identifiers.get('issn'),
                        'type': record.get('type'),
                        'path': os.path.basename(record.get('file')).split(':')[0] if record.get('file') else None,
                    })
                lib.save()
