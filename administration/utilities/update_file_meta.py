#!/usr/bin/env python

"""
This script generates file hashes for files stored in the
rov.file table. The argument is a root folder onto which the
file path is appended.
"""

import os
import sys
import psycopg2
import hashlib

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'import'))

from msea.util import util

rootdir = sys.argv[1]

conn = psycopg2.connect('dbname=sap user=rob')
cur = conn.cursor()
cur.execute('select id, path from shared.file')
for id, path in cur.fetchall():
    file = os.path.join(rootdir, path)
    if os.path.isfile(file):
        #		hash = hash_file(file)
        #		meta = get_file_meta(file)
        #		print(file, hash)
        #		cur.execute('update shared.file set hash=%s, metadata=%s::jsonb where id=%s', [hash, json.dumps(meta), id])
        hash = util.hash_file(file)
        print(file, hash)
        cur.execute('update shared.file set hash=%s where id=%s', [hash, id])
    else:
        print('Not found', file)
