#!/usr/bin/env python

"""
Read a CSV file and interpolate the values between two numeric values in the given columns such that there are
no gaps within the column. Non-numeric cells and blanks are allowed at each end. A non-numeric cell within
a range of numbers with cause an error.

Columns are given by zero-based index, since there is no concept of column names.
"""

import os
import sys
import csv

csv_dir = sys.argv[1]
out_dir = sys.argv[2]
columns = list(map(int, sys.argv[3:]))

if csv_dir == out_dir:
    raise Exception('Can\'t write to input dir.')

try:
    os.makedirs(out_dir)
except: pass

class _range:

    def __init__(self):
        self.on = False
        self.col = -1
        self.start = -1
        self.end = -1
        self.values = []
        self.i = -1

    def next(self):
        self.i += 1
        if self.i < len(self.values):
            return self.values[self.i]
        return None

    def reset(self):
        self.i = -1

    def interp(self):
        f = self.values[0]
        l = self.values[1]
        n = self.end - self.start
        s = (l - f) / n
        self.values = [f + s * x for x in range(n + 1)]


def is_num(v):
    try:
        float(v)
        return True
    except:
        return False


for f in [f for f in os.listdir(csv_dir) if f.endswith('.csv')]:
    cols = {}
    repl = dict([(c, {}) for c in columns])
    blanks = dict([(col, False) for col in columns])

    with open(os.path.join(csv_dir, f), 'r') as fi:
        dbi = csv.reader(fi)
        i = 0
        for row in dbi:
            for col in columns:
                if is_num(row[col]) and not blanks[col]:
                    r = cols.get(col)
                    if not r:
                        r = cols[col] = _range()
                        r.col = col
                        r.values.append(float(row[col]))
                    else:
                        r.values[0] = float(row[col])
                    r.start = i
                elif not is_num(row[col]) and not blanks[col]:
                    blanks[col] = cols.get(col) is not None
                elif is_num(row[col]) and blanks[col]:
                    r = cols[col]
                    r.end = i
                    r.values.append(float(row[col]))
                    if r.end - r.start > 1:
                        repl[col][r.start] = r
                    del cols[col]
                    blanks[col] = False
            i += 1

        active = {}
        with open(os.path.join(out_dir, f), 'w', newline='') as fo:
            dbo = csv.writer(fo)
            with open(os.path.join(csv_dir, f), 'r') as fi:
                dbi = csv.reader(fi)
                i = 0
                for row in dbi:
                    for col in columns:
                        if repl.get(col, {}).get(i) is not None:
                            active[col] = repl[col][i]
                            active[col].reset()
                            active[col].interp()
                        if active.get(col):
                            v = active[col].next()
                            if v is not None:
                                row[col] = v
                            else:
                                del active[col]
                    i += 1
                    dbo.writerow(row)
