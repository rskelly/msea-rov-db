#!/usr/bin/env python

"""
This script reads through the records in the taxon table in the ROV database
and attempts to match each entry to a record in the WoRMs database, returning an AphiaID.

First, the script will attempt to match a batch of scientific names (Genus species) exactly,
then the non-matches will be re-run using the fuzzy match method. If a fuzzy match is made, the
worms_fuzzy flag is set. If no match is found, the aphia_id is null. These records must be audited.

When observations are ingested an attempt is made to find a link to the species or subspecies taxon.
If that can't be managed, an attempt is made to match the lowest taxon and the identification is
recorded as an OTU.
"""

import os
import sys
import csv
import django
import json
from urllib import request, parse

from django.db import transaction, connection
from datetime import datetime, timedelta, timezone

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'import'))

# Import the utils to support this program.
from msea.util import util

# Setup the Django environment.
util.django_setup()

# Import the MSEA models. Has to be done after setting up Django.
from msea.models import *
from msea.forms import *

# In the import folder.
from worms import *


def update_taxon():
	'''
	Update the taxon record.
	'''
	# Load the taxon records to update.
	taxa = Taxon.objects.filter(worms_substring = True) #models.Q(scientific_name__isnull = False) & (models.Q(worms_substring__isnull = True) | models.Q(worms_fuzzy__isnull = True)))
	#taxa = Taxon.objects.filter(hart_code = '5CT')
	
	count = taxa.count()
	num = 1

	# Iterate over the taxa and update.
	for taxon in taxa:
		print(taxon, num, 'of', count)
		num += 1
		
		sciname = taxon.scientific_name.lower()
		otuname = split_otu(sciname)
		print('Searching', sciname, otuname)

		fuzzy = False
		substring = False
		data = None

		# For each taxon we try the get_sciname function first, then the fuzzy one.
		for label, fn in (('sciname', get_sciname), ('fuzzy', get_fuzzy)):
			searchname = otuname if otuname else sciname
			print(label, searchname)
			status, res, data = fn(searchname)
			if status == 200:
				if label == 'fuzzy':
					fuzzy = True
				break

		if not data:
			print('No result for', sciname)
			continue

		with transaction.atomic():

			# Create a data object by mapping the fields from the retrieved object.
			item = {}
			for frm, to in fields:
				item[to] = data[0][frm]

			try:
				# Try to get an existing WORMS object.
				worm = WoRMs.objects.get(aphia_id = item['aphia_id'])
			except:
				# Create the new WORMS object.
				worm = WoRMs.objects.create(**item)
				worm.save()

			taxon.worms = worm
			taxon.worms_fuzzy = fuzzy
			#taxon.worms_substring = substring
			taxon.otu = sciname if otuname else None
			taxon.save()

			print('Saved', taxon.scientific_name, 'as', worm.scientific_name, 'fuzzy', fuzzy)


if __name__ == '__main__':

	update_taxon()

