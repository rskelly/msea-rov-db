#!/usr/bin/env python

"""
Consumes the parquet file produced by OBIS, which contains the complete observation record
included in the WoRMS database. Reads sequentially through the rows, creating a unique
record for each AphiaID/Scientific Name and connecting the record to the next-higher taxon. Calculates
the geographic boundary of observations from coordinates stored in each record.

If the higher-level taxon isn't in the dataset, it is retrieved from WoRMS via the REST API
and added to the database. 


Schema is here: https://obis.org/data/access/#
Quality flags: https://github.com/iobis/obis-qc
Download link: https://obis.org/data/access/# The actual file is something like, 'exports/obis_20231025.parquet' which is linked via javascript.
"""

import pyarrow.parquet as pq
import sqlite3
import os

try:
    os.unlink('obis.sqlite')
except:
    print('couldnt delete file')

conn = sqlite3.connect('obis.sqlite')
cur = conn.cursor()

cur.execute('''
    create table if not exists taxa (
        aphia_id integer not null unique, 
        scientific_name varchar(256) not null,
        taxon_rank varchar(32) not null,
        parent_aphia_id integer,
        kingdom_id integer,
        phylum_id integer,
        class_id integer, subclass_id integer, infraclass_id integer,
        superorder_id integer, order_id integer, suborder_id integer,
        superfamily_id integer, family_id integer, subfamily_id integer,
        genus_id integer, subgenus_id integer, 
        species_id integer, subspecies_id integer,
        count integer,
        lat1 float, lat2 float, lon1 float, lon2 float
    )
''')

# These are the levels actually used in the database.
taxonomicLevels = [
    'kingdomid', 'phylumid', 
    'classid', 'subclassid', 'infraclassid', 
    'superorderid', 'orderid', 'suborderid', 
    'superfamilyid', 'familyid', 'subfamilyid', 
    'genusid', 'subgenusid', 'speciesid', 'subspeciesid', 
]

# All levels present in the OBIS database. Used for finding the next-highest level.
taxonomicLevelsAll = [
    'superdomain', 'domain', 'kingdom', 'subkingdom', 'infrakingdom', 
    'phylum', 'phylum_division', 'subphylum_subdivision', 'subphylum', 'infraphylum', 'parvphylum', 
    'gigaclass', 'megaclass', 'superclass', 'class', 'subclass', 'infraclass', 'subterclass', 
    'superorder', 'order', 'suborder', 'infraorder', 'parvorder', 'superfamily', 'family', 'subfamily', 
    'supertribe', 'tribe', 'subtribe', 'genus', 'subgenus', 'section', 'subsection', 'series', 
    'species', 'subspecies', 'natio', 'variety', 'subvariety', 'forma', 'subforma', 'superdomain', 
    'domain', 'kingdom', 'subkingdom', 'infrakingdom', 'phylum', 'phylum_division', 'subphylum_subdivision', 'subphylum', 
    'infraphylum', 'parvphylum', 'gigaclass', 'megaclass', 'superclass', 'class', 'subclass', 'infraclass', 'subterclass', 
    'superorder', 'order', 'suborder', 'infraorder', 'parvorder', 'superfamily', 'family', 'subfamily', 'supertribe', 'tribe', 
    'subtribe', 'genus', 'subgenus', 'section', 'subsection', 'series', 'species', 'subspecies', 'natio', 'variety', 
    'subvariety', 'forma', 'subforma'
]

# The fields needed from the OBIS database.
fields = ['AphiaID', 'decimalLongitude', 'decimalLatitude', 'scientificName', 'originalScientificName', 'taxonRank'] + taxonomicLevels
# The fields used in the insert statememt.
fields0 = ['AphiaID', 'scientificName', 'taxonRank'] + taxonomicLevels
# The OBIS db file.
obis_file = r'c:\Users\skellyr\Downloads\obis_20231025.parquet'

def load_obis(obis_file):
    """
    Load data from the OBIS table in parquet format into an sqlite table,
    with unique Aphia IDs and scientific names. The spatial bounds of observations
    are computed and some taxonomic relationships are preserved.
    :param obis_file: The path to the OBIS database file in parquet format.
    """
    # Connect to the table.
    print('Opening', obis_file)
    table = pq.read_table(obis_file, columns=fields)
    print('Opened.')

    count = 1
    for batch in table.to_batches():
        print('Batch', count)
        count += 1

        # Dict to store unique taxa within the batch.
        taxa = {}
        
        # Iterate over the items in the batch.
        for item in batch.to_pylist():

            aid = item['AphiaID']

            if not taxa.get(aid):
                # If the Aphia ID isn't in the dict, populate a new item.
                item['lat1'] = item['decimalLatitude']
                item['lat2'] = item['decimalLatitude']
                item['lon1'] = item['decimalLongitude']
                item['lon2'] = item['decimalLongitude']
                item['count'] = 1
                taxa[aid] = item
            else:
                # Update the taxon.
                taxon = taxa[aid]
                taxon['lat1'] = min(taxon['lat1'], item['decimalLatitude'])
                taxon['lat2'] = max(taxon['lat2'], item['decimalLatitude'])
                taxon['lon1'] = min(taxon['lon1'], item['decimalLongitude'])
                taxon['lon2'] = max(taxon['lon2'], item['decimalLongitude'])
                taxon['count'] += 1

        # Iterate over the taxa dict.
        for k, v in taxa.items():
            if not v['scientificName'] or not v['taxonRank']:
                # If the scientific name or taxon rank are empty skip.
                continue

            # Select the existing item by Aphia ID.
            cur.execute('select aphia_id, lon1, lon2, lat1, lat2 from taxa where aphia_id=?', [k])
            result = cur.fetchone()
            if result:
                # If the taxon exists, update the count and expand the bounds.
                cur.execute('update taxa set count=count+1, lon1=min(lon1, ?), lon2=max(lon2, ?), lat1=min(lat1, ?), lat2=max(lat2, ?) where aphia_id=?', result)
            else:
                # If the taxon doesn't exist, add it.
                cur.execute('''
                    insert into taxa (
                        aphia_id, scientific_name, taxon_rank, 
                        kingdom_id, phylum_id, 
                        class_id, subclass_id, infraclass_id, 
                        superorder_id, order_id, suborder_id,
                        superfamily_id, family_id, subfamily_id, 
                        genus_id, subgenus_id, 
                        species_id, subspecies_id, 
                        lat1, lat2, lon1, lon2
                    ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                ''', [v[k0] for k0 in fields0 + ['lat1', 'lat2', 'lon1', 'lon2']])
                            
    # Print the count of inserted rows.
    cur.execute('select count(*) from taxa')
    count, = cur.fetchone()
    print('Rows', count)

def build_tree():
    """
    Populate the parent taxon field in the database with the correct parent. If
    the parent isn't present in the DB, load it from the API and insert it into 
    the database.
    """

    cur0 = conn.cursor()

    cur.execute('select * from taxa')
    cols = [col[0] for col in cur.description]
    
    for row in cur:
        row = dict(zip(cols, row))

        next_rank = None
        t_rank = row['taxon_rank']
        t_idx = taxonomicLevelsAll.index(t_rank)
        for i in range(t_idx, -1, -1):
            if taxonomicLevelsAll[i] in taxonomicLevels:
                next_rank = taxonomicLevelsAll[i]
                break
        
        if not next_rank:
            raise Exception(f'A higher rank than {t_rank} was not found.')
    
        next_id = row[f'{next_rank.lower()}id']

        cur0.execute('select * from taxa where aphia_id=?', [next_id])
        if not cur0.fetchone():
            # load from worms
            requests.get()
        cur0.execute('update taxa set parent_taxon_id')
        


