#!/usr/bin/env python

import os
import sys
import psycopg2

# -- Create a mapping of species ovservations and the nearest positions.
# create materialized view rov.obs_position as
# with t as (
# 	select distinct on (a.id) a.id as event_id, c.taxon_id, a.dive_id, b.id as position_id, a.start_time, (abs(extract(epoch from a.start_time) - extract(epoch from b.timestamp))) as diff
# 	from rov.event a inner join rov.observation_event c on a.id=c.event_id inner join rov.position b on a.dive_id = b.dive_id
# 	order by a.id
# )
# select a.label, t.start_time, b.name as dive_name, c.geom
# from rov.taxon a inner join t on a.id = t.taxon_id
# 	inner join rov.dive b on b.id = t.dive_id
# 	inner join rov.position c on c.id = t.position_id;

conn = psycopg2.connect('dbname=sap user=rob port=5432')
cur = conn.cursor()

cur.execute('''
SET postgis.gdal_enabled_drivers = 'ENABLE_ALL';
with corners as (
	select 
		st_xmin(st_envelope(st_transform(st_union(geom), 3857))) as xmin,
		st_ymin(st_envelope(st_transform(st_union(geom), 3857))) as ymin,
		st_xmax(st_envelope(st_transform(st_union(geom), 3857))) as xmax,
		st_ymax(st_envelope(st_transform(st_union(geom), 3857))) as ymax
	from rov.obs_position
), cols as (
	select * from generate_series((select xmin from corners)::int4, (select xmax + 1000.0 from corners)::int4, 1000) as col
), rows as (
	select * from generate_series((select ymin from corners)::int4, (select ymax + 1000.0 from corners)::int4, 1000) as row
), grid as (
	select st_setsrid(st_makepoint(col + 500.0, row - 500.0), 3857) as geom from cols, rows
), values as (
	select count(*) as n, a.geom_p
	from grid inner join rov.obs_position a on st_dwithin(a.geom_p, grid.geom, 1000.0)
	group by a.geom_p
), rast as (
	select st_addband(
		st_makeemptyraster(
			(select count(*) from cols)::int4, 
			(select count(*) from rows)::int4, 
			xmin, ymax, 1000.0, -1000.0, 0.0, 0.0, 3857
		),
		'8BUI'::text
	) as rast
	from corners
), heatmap as (
	select st_setvalue(rast.rast, 1, values.geom_p, values.n) as rast 
	from rast, values
)
select st_asgdalraster(heatmap.rast, 'GTiff') as data from heatmap
''')

with open('C:\\Users\\skellyr\\Desktop\\test.tif', 'wb') as f:
	f.write(cur.fetchone()[0])

