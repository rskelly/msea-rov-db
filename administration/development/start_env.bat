rem This script starts (git) console windows and applications.
rem One (re)starts PostgreSQL.
rem One starts the Django application.
rem One goes to the import folder. 
rem Change as needed.

set MSEA=C:\Users\skellyr\Documents\git\msea-rov-db
set BCMD=C:\Users\skellyr\AppData\Local\Programs\Git\git-bash.exe
set PATH=C:\Users\skellyr\nodejs;%PATH%

start "PostgreSQL"  %BCMD% -c "cd \"%cd%\" && ./start_pg.sh restart ; exec $SHELL"
start "Import"      %BCMD% -c "cd \"%cd%\" && cd import && source ../.venv/Scripts/activate ; exec $SHELL"
start "Vue"         %BCMD% -c "cd \"%cd%\" && cd app/vue && npm run dev ; exec $SHELL"
timeout /t 60
start "Application" %BCMD% -c "cd \"%cd%\" && cd app  && source ../.venv/Scripts/activate && python manage.py runserver 8080 --settings=main.settings.dev ; exec $SHELL"
