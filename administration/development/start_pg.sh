#!/bin/bash

# The script sets environment variables helpful for PostgreSQL

export PATH=$PATH:/c/Users/skellyr/pgsql/bin
export PGDATA=/c/Users/skellyr/pgdata
export PGDATABASE=postgres
export PGUSER=postgres
export PGPORT=5432
export PGLOCALEDIR=/c/Users/skellyr/pgsql/share/locale

/c/Users/skellyr/pgsql/bin/pg_ctl -l $PGDATA/log $1