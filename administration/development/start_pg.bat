@ECHO OFF
REM The script sets environment variables helpful for PostgreSQL

set PATH=C:\Users\skellyr\pgsql\bin;%PATH%
set PGDATA=C:\Users\skellyr\pgdata
set PGDATABASE=postgres
set PGUSER=postgres
set PGPORT=5432
set PGLOCALEDIR=C:\Users\skellyr\pgsql\share\locale

C:\Users\skellyr\pgsql\bin\pg_ctl -l %PGDATA%\log %1
