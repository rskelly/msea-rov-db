#!/usr/bin/env python

"""
Download the MaxMind GeoIPLite City database, check the hash and update the PostgreSQL version.
Should be called by cron.

1. Determine whether file should be downloaded.
2. Download file if necessary.
3. Unzip the files.
4. Update the location table.
5. Update the location table from the EN file.
"""
import os
import csv
import sys
import requests
import hashlib
import zipfile
from datetime import datetime

up = os.path.dirname
sys.path.append(os.path.join(up(up(up(os.path.realpath(__file__)))), 'app'))

from msea.util.django import django_setup
from msea.util import util

# Set up django.
django_setup()

from django.db import connection

# Database URL
db_url = 'https://download.maxmind.com/geoip/databases/GeoLite2-City-CSV/download?suffix=zip'
# SHA256 URL
hash_url = 'https://download.maxmind.com/geoip/databases/GeoLite2-City-CSV/download?suffix=zip.sha256'
# DL location.
tmp_dir = util.tempdir()

# If true, don't actually download.
testing = False
# Update last modified only, do not load.
update_lm = False

def get_session():
    """
    Create and return an authenticated session.
    """
    session = requests.Session()
    session.auth = (os.getenv('MAXMIND_ACCOUNT_ID'), os.getenv('MAXMIND_LICENSE_KEY'))
    return session

def get_last_modified(response):
    """
    Return a datetime representing the last modified header in the response.
    """
    lm = response.headers.get('last-modified')
    print('Last modified:', lm)

    if lm:
        # If a last modified header is available, parse it and compare.
        return datetime.strptime(lm, '%a, %d %b %Y %H:%M:%S GMT')
    return None

def needs_download(cur):
    """
    If the date in the last-update table is more than 2 weeks old, return true.
    """
    with connection.cursor() as cur:
        cur.execute('select last_update from maxmind.geoip_last_update limit 1')
        result = cur.fetchone() or (None,)
        try:
            # Get the last update time from the database and convert into a number weeks.
            l_lu, = result

            # Get the last modified time and convert into a number of weeks.
            res = get_session().head(db_url, stream=True)
            r_lu = get_last_modified(res)

            print('Update times -- ours:', l_lu, '; theirs:', r_lu)
            if r_lu and l_lu:
                r_lu_s = int(r_lu.timestamp() / (3600 * 24 * 7))
                l_lu_s = int(l_lu.timestamp() / (3600 * 24 * 7))
                # If a last modified header is available, parse it and compare.
                print('--', l_lu_s, r_lu_s)
                return l_lu_s < r_lu_s
        except Exception as e:
            print(e)
            sys.exit(1)
        return True

def download_file(session, url, stream=False):
    """
    Download a file.
    """
    if testing:
        res = session.head(url)
    else:
        res = session.get(url, stream=stream)
    if 'content-disposition' in res.headers:
        filename = os.path.join(tmp_dir, res.headers['content-disposition'].split('filename=')[1])
    else:
        tmp = util.maketempfile()
        filename = tmp.path
    lm = get_last_modified(res)
    if not testing and stream:
        with open(filename, mode="wb") as file:
            for chunk in res.iter_content(chunk_size=10 * 1024):
                file.write(chunk)
    elif not testing:
        res = session.get(url)
        with open(filename, mode="wb") as file:
            file.write(res.content)
    return filename, lm

def get_file_hash(filename):
    """
    Get the SHA256 hash of a file.
    """
    h = hashlib.sha256()
    with open(filename, 'rb') as f:
        while True:
            data = f.read(8096)
            if not data:
                break
            h.update(data)
        return h.hexdigest()    

def crawl_dir(dirname, files=[]):
    """
    Get the list of all regular files in the the directory, recursively.
    """
    for f in os.listdir(dirname):
        ff = os.path.join(dirname, f)
        if os.path.isdir(ff):
            crawl_dir(ff, files)
        else:
            files.append(ff)
    return files

def unzip_file(filename):
    """
    Unzip the file into the destination directory and return a list of the files it contains.
    """
    tmp = util.maketempdir()
    with zipfile.ZipFile(filename) as z:
        z.extractall(tmp)
    return crawl_dir(tmp)

def update_geoip():

    with connection.cursor() as cur:

        if needs_download(cur):
            # Create an authenticated session.
            session = get_session()

            # Download the database.
            hash_file, _ = download_file(session, hash_url)
            db_file, last_modified = download_file(session, db_url, True)

            if update_lm:
                print('Updating last modified:', last_modified)
                cur.execute('delete from maxmind.geoip_last_update')
                cur.execute('insert into maxmind.geoip_last_update (last_update) values (%s)', [last_modified.strftime('%Y-%m-%d %H:%M:%S GMT')])
                return

            # Get the hash and filename.
            with open(hash_file, 'r') as f:
                old_hash, check_file = f.read().strip().split()

            # Check that the hash file matches the downloaded file.
            if check_file != os.path.basename(db_file):
                raise Exception(f'The downloaded file is {db_file} but the check file is {check_file}.')
            
            # Check the file hash.
            new_hash = get_file_hash(db_file)
            print(new_hash, old_hash)
            if old_hash != new_hash:
                raise Exception(f'The downloaded file does not match the hash.')
            
            # Get the list of files from the archive.
            files = unzip_file(db_file)
            files = {os.path.basename(f): f for f in files}
            
            # Iterate over the files and process the necessary ones.
            data_files = [
                ('GeoLite2-City-Locations-en.csv', '"maxmind"."geoip_city_locations"', None), 
                ('GeoLite2-City-Blocks-IPv4.csv', '"maxmind"."geoip_city_blocks"', ('location', 'region')),
                ('GeoLite2-City-Blocks-IPv6.csv', '"maxmind"."geoip_city_blocks"', ('location', 'region'))
            ]

            for ff, table, _ in reversed(data_files):
                # Delete to avoid the complexity of update on conflict.
                try:
                    print('Deleting from', table)
                    cur.execute(f'delete from {table}')
                except Exception as e:
                    print(e)
                    sys.exit(1)

            user = os.getenv('DB_USER')
            passwd = os.getenv('DB_PASSWORD')
            dbname = os.getenv('NAME')

            for ff, table, excl in data_files:
                # Iterate over the data files and import into the corresponding table.
                print('Updating', table)

                # Get the full file path.
                f = files[ff]

                # If there's a column exclusion list, create a list of columns from the CSV header 
                # to feed into the COPY command. 
                with open(f, 'r', encoding='utf-8-sig') as fi:
                    db = csv.reader(fi)
                    cols = next(db)
                    if excl:
                        cols = list(filter(lambda a: a not in excl, cols))
                    cols = '({})'.format(','.join(cols))

                cmds = [
                    f"export PGPASSWORD={passwd}; psql -U {user} -d {dbname} -c 'delete from {table}'",
                    f"export PGPASSWORD={passwd}; cat {f}|psql -U {user} -d {dbname} -c 'copy {table} {cols} from STDIN with csv header'"
                ]
                for cmd in cmds:
                    os.system(cmd)

            # Get the last modified header and set that as the last updated time.
            print('Updating last modified:', last_modified)
            cur.execute('delete from maxmind.geoip_last_update')
            cur.execute('insert into maxmind.geoip_last_update (last_update) values (%s)', [last_modified.strftime('%Y-%m-%d %H:%M:%S GMT')])
        else:
            print('Update not needed.')
        
if __name__ == '__main__':

    config_file = sys.argv[1]

    update_geoip(config_file)
