#!/usr/bin/env python

"""
Sends a daily digest email with abbreviated information about
new sign-ups to the database webside.
"""
import os
import sys
from datetime import datetime

up = os.path.dirname
sys.path.append(os.path.join(up(up(up(os.path.realpath(__file__)))), 'app'))

from msea.util.django import django_setup
from msea.util import util

django_setup()

from django.db import connection

def send_digest():
    """
    Remove accounts whose verification links have expired.
    Reset password reset fields when they've expired.
    """
    print('Sending account digest.')
    # Get the configuration.
    host = os.getenv('MSEA_HOST')
    from_email = os.getenv('MSEA_CONTACT_EMAIL')

    # Connect to the database.
    with connection.cursor() as cur:

        # Get the list of signups <= 24h old.
        cur.execute("""
                select first_name, last_name, email, org_type, organization, registration_reason,
                    registration_ip, registration_location, verification_time is not null as verified,
                    date_joined
                from public.auth_user
                where (extract(epoch from (now() - date_joined)) / 3600) <= 24
        """)

        # Generate the user table.
        # Append user rows to table.
        user_table = []
        for first_name, last_name, email, org_type, organization, reason, \
                    ip, location, verified, date_joined in cur:
            user_table.append(f'<tr><td><a href="{host}/rov/admin/users?filter={email}" title="User Record - Direct Link">{email}</a></td>' \
                            f'<td>{last_name}</td><td>{first_name}</td><td>{organization}</td><td>{org_type}</td>' \
                            f'<td>{reason}</td><td>{ip}</td><td>{location}</td><td>{verified}</td><td>{date_joined}</td></tr>')
        
        if not len(user_table):
            print('No new users.')
            return
        else:
            print('Found', len(user_table), 'new users.')

        # Serialize table into a string.
        user_table = [
            '<table>', 
            '<tr><th>Email</th><th>Last Name</th><th>First Name</th><th>Organization</th><th>Org. Type</th><th>Reg. Reason</th><th>IP</th><th>Location</th><th>Verified</th><th>Date Joined</th></tr>'
        ] + user_table + ['</table>']
        user_table = '\n'.join(user_table)

        # Get the list of email recipients.
        cur.execute("""
                select a.email 
                from public.auth_user a
                    inner join public.auth_user_groups b on b.user_id=a.id
                    inner join public.auth_group c on c.id=b.group_id
                where c.name = 'msea_signup_digest_recipient'
        """)
        recipients = cur.fetchall()

        if len(recipients) == 0:
            print('No recipients.')
            return
        else:
            print('Found', len(recipients), 'recipients.')

        # Generate the message and date string.
        message = util.process_email_template(
            os.path.join(os.path.dirname(__file__), 'email_templates/account_digest.html'), 
            user_table=user_table, host=host
        )
        subject = datetime.now().strftime('Daily user signup digest for %A, %B %d, %Y')

        # Send the emails.
        for recipient, in recipients:
            util.send_email(
                to_email=recipient,
                from_email=from_email,
                subject=subject,
                message=message,
            )

if __name__ == '__main__':

    send_digest()
