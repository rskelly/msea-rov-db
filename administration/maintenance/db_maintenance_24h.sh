#!/bin/bash

# This file is called by cron to perform various regular DB and site
# maintenance tasks. The task runs every 24 hours.

# The file will be run as msea (by cron).
# Do not edit this file without consulting Rob at robert.skelly@dfo-mpo.gc.ca

# The script path.
script_path=$repo_path/var/rov/maintenance

# Start venv. It must have already been installed with requirements.
echo 'Starting venv.'
source $script_path/.venv/bin/activate

# Run the maintenance file.
echo 'Running 24h maintenance file.'
python $script_path/db_maintenance_24h.py
