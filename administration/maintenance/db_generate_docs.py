#!/usr/bin/env python

"""
Generate a complete documentation page in markup using comments saved in the database itself.

Saves the documentation to the wiki page stored in wiki.article, finding the appropriate record using
the slug.
"""

import os
import re
import sys
import logging
import requests
import json

up = os.path.dirname
sys.path.append(os.path.join(up(up(up(os.path.realpath(__file__)))), 'app'))
               
from msea.util.django import django_setup

# Set up django.
django_setup()

from django.db import connection

# The schemas to document.
schemas = ['ndst', 'rov', 'shared', 'intertidal', 'pa', 'taxonomy', 'inaturalist']

# The type of output.
type = 'wiki'

# The temporary output file. Will be uploaded to the wiki page.
out_file = '/tmp/db_docs'


def fix_null(v):
    """
    If the value is None, return a string.
    """
    if v is None:
        return '[None]'
    return v

def clean(v):
    """
    Clean the text for the implementation. E.g., clean MD-style links so they're in wiki style.
    """
    if type == 'wiki':
        return clean_wiki(v)
    else:
        return v
    
def clean_wiki(v):
    """
    Clean the text in wiki style.
    """
    if v:
        def repl(m):
            return link_format_wiki(m.group(1), m.group(2))
        return re.sub(r'\[(.+?)\]\((.+?)\)', repl, v)
    else:
        return v

def write(fd, txt, style=None):
    """
    Write the string to the filehandle. If style is given,
    wrap the text in appropriate tags or formatting.
    """
    style = style.strip() if style else None
    txt = txt.strip() if txt else None
    if not txt:
        return
    if type == 'wiki':
        write_wiki(fd, txt, style)
    elif type == 'md':
        write_md(fd, txt, style)

def style_wiki_start(style):
    """
    Generate the starting tag for a wiki style.
    """
    if not style:
        return ''
    elif style == 'em':
        return "''"
    elif style[0] == 'h':
        return ''.join(['=' for i in range(int(style[1:]))]) + ' '
    else:
        return ''

def style_wiki_end(style):
    """
    Generate the ending tag for a wiki style.
    """
    if not style:
        return ''
    elif style == 'em':
        return "''"
    elif style[0] == 'h':
        return ' ' + ''.join(['=' for i in range(int(style[1:]))])
    else:
        return ''

def write_wiki(fd, txt, style='span'):
    """
    Write the string in wiki format with style appropriate to the style.
    """
    fd.write(style_wiki_start(style) + txt + style_wiki_end(style) + '\n\n')

def style_md_start(style):
    """
    Generate the starting tag for a markdown style.
    """
    if not style:
        return ''
    elif style == 'em':
        return '_'
    elif style[0] == 'h':
        return ''.join(['#' for i in range(int(style[1:]))]) + ' '
    else:
        return ''

def style_md_end(style):
    """
    Generate the ending tag for a wiki style.
    """
    if not style:
        return ''
    elif style == 'em':
        return '_'
    else:
        return ''

def write_md(fd, txt, style):
    """
    Write the string in markdown format with style appropriate to the style.
    """
    fd.write(style_md_start(style) + txt + style_md_end(style) + '\n\n')

def alink(f, name, link):
    """
    Format a link.
    """
    name = name.strip()
    link = link.strip()
    if type == 'wiki':
        alink_wiki(f, name, link)
    elif type == 'md':
        alink_md(f, name, link)

def link_format_wiki(name, link):
    """
    Format a link for wiki.
    """
    if link.startswith('http'):
        # External link.
        return f'[{link.strip()} {name.strip()}]'
    elif link.startswith('#'):
        # Internal link (to anchor).
        return f'[[{link.strip()}|{name.strip()}]]'
    else:
        # A link to a wiki page.
        return f'[[{link.strip()} {name.strip()}]]'

def link_format_md(name, link):
    """
    Format a link for markdown.
    """
    return f'[{name}]({link})'

def alink_wiki(f, name, link):
    """
    Format a wiki link.
    """
    f.write(link_format_wiki(name, link))

def alink_md(f, name, link):
    """
    Format a markdown link.
    """
    f.write(link_format_md(name, link))

def thead(f, labels, caption=None):
    """
    Write a table head.
    """
    caption = caption.strip() if caption else None
    if type == 'wiki':
        thead_wiki(f, labels, caption)
    elif type == 'md':
        thead_md(f, labels, caption)

def thead_wiki(f, labels, caption=None):
    """
    Write a wiki table header.
    """
    f.write('{| class="wikitable" \n')
    if caption:
        f.write(f'|+ {caption}\n')
    f.write('|-\n')
    f.write('! ' + ' !! '.join(list(map(lambda a: str(a).strip(), labels))) + '\n')

def thead_md(f, labels, caption=None):
    """
    Write a table heading in markdown format.
    """
    f.write('| ' + ' | '.join(labels) + ' |\n')
    f.write('|' + '|'.join(['-' for x in labels]) + '|\n')

def trow(f, cells):
    """
    Write a table row.
    """
    if type == 'wiki':
        trow_wiki(f, cells)
    elif type == 'md':
        trow_md(f, cells)

def trow_wiki(f, cells):
    """
    Write a wiki table row.
    """
    f.write('|-\n')
    f.write('| ' + ' || '.join(list(map(lambda a: str(a).strip(), cells))) + '\n')

def trow_md(f, cells):
    """
    Write a table row in markdown format.
    """
    f.write('| ' + ' | '.join(list(map(lambda a: str(a).strip(), cells))) + ' |\n')

def tfoot(f):
    """
    Write the end of a table.
    """
    if type == 'wiki':
        tfoot_wiki(f)
    elif type == 'md':
        tfoot_md(f)

def tfoot_wiki(f):
    """
    Write the end of a wiki table.
    """
    f.write('|}\n\n')

def tfoot_md(f):
    pass

def anchor(f, name):
    """
    Write an anchor.
    """
    if type == 'wiki':
        anchor_wiki(f, name)
    elif type == 'md':
        anchor_md(f, name)

def anchor_md(f, name):
    """
    Write wiki anchor.
    """
    f.write(f'<span id="{name}"></span>\n')

def anchor_wiki(f, name):
    """
    Write wiki anchor.
    """
    f.write(f'<span id="{name}"></span>\n')

def update_wiki():
    """
    Update the wiki page designated for DB documentation. It 
    """
    # The document replaces everything between these elements, which must exist in the
    # current page text.
    start_line = '<!-- BEGIN GENERATED CONTENT -->'
    end_line = '<!-- END GENERATED CONTENT -->'

    # Update the sandbox page on English Wikipedia with "Hello, world!"
    url = 'https://msea.science/wiki/rest.php/v1/page/Annotation_Database_Entity_Documentation'

    result = requests.get(url)
    page = result.json()

    # Get the page version so we can writer to the lates.
    latest = page['latest']['id']

    # Get the full text, header and footer of the page.
    source = page['source']
    head = source[:source.index(start_line) + 1]
    foot = source[source.index(end_line)]

    # Read the documentation text from the output file.
    with open(out_file, 'r') as f:
        content = f.read()

    # Assemble the new page.
    updated_source = head + '\n' + content + '\n' + foot

    # Substitute your OAuth token
    headers = {
        "User-Agent" : 'MSEA Wiki Automatic DB Update/0.1 (https://www.mediawiki.org/wiki/API_talk:REST_API)',
        "Content-Type" : 'application/json',
        'Authorization' : f'Bearer {token}'
    }

    # Use the get page endpoint to get the latest revision ID
    request_data = {
        'source' : updated_source,
        'comment': 'Automatic update of DB documentation.',
        'latest' : {'id': latest}
    }

    response = requests.put( url, headers=headers, data = json.dumps(request_data) )

def run(outfile):
    """
    Run the processor.
    """

    # Connect to the DB.
    with connection.cursor() as cur:

        # r = ordinary table, i = index, S = sequence, v = view,
        # m = materialized view, c = composite type, t = TOAST table, f = foreign table
        kinds = [('r', 'Table'), ('v', 'View'), ('m', 'Materialized View'), ('p', 'Procedure'), ('f', 'Function')]

        # Extract the relations.
        cur.execute('select oid, nspname from pg_namespace where nspname in %s order by nspname', [tuple(schemas)])
        ns = [n for n in cur]

        with open(outfile, 'w') as f:

            for nsoid, nsname in ns:

                cur.execute('select obj_description(%s)', [nsoid])
                nsdesc = cur.fetchone()[0]

                anchor(f, nsname)
                write(f, f'Schema: {nsname}', 'h2')

                write(f, clean(nsdesc))

                for ktype, kname in kinds:

                    # For each kind of relation, select the object ID and relation name for the namespace.
                    cur.execute('''
                        select oid, relname from pg_class where relkind=%s and relnamespace=%s order by relname
                    ''', [ktype, nsoid])
                    rels = [r for r in cur]

                    for roid, rname in rels:

                        anchor(f, f'{nsname}.{rname}')
                        write(f, f'{kname}: {rname}', 'h3')

                        # Get the description of the relation.
                        cur.execute('select obj_description(%s)', [roid])
                        tabledesc = cur.fetchone()[0]

                        write(f, f'{clean(tabledesc)}')

                        if ktype in ('v', 'm'):
                            # Get column metadata for a view.
                            cur.execute('''
                                SELECT a.attname, a.attnum, pg_catalog.format_type(a.atttypid, a.atttypmod), a.attnotnull
                                FROM pg_attribute a
                                    JOIN pg_class t on a.attrelid = t.oid
                                    JOIN pg_namespace s on t.relnamespace = s.oid
                                WHERE a.attnum > 0 AND NOT a.attisdropped AND t.relname = %s AND s.nspname = %s
                                ORDER BY a.attname
                            ''', [rname, nsname])
                        else:
                            # Get column metadata for a table.
                            cur.execute('''
                                select column_name, ordinal_position, data_type, is_nullable 
                                from information_schema.columns 
                                where table_schema=%s and table_name=%s
                                order by column_name
                            ''', [nsname, rname])

                        cols = [c for c in cur]
                        
                        # print(nsname, rname, len(cols))
                        # If there are columns, write the attribute table.
                        if cols:
                            thead(f, ('Name', 'Type', 'Nullable', 'Documentation'), f'Columns in "{rname}"')

                        for cname, cidx, ctype, cnullable in cols:
                            # print(cname)
                            if cname == 'id':
                                # Don't bother with the ID column. Every table has one.
                                continue
                            if cname == 'geom' and ctype == 'USER-DEFINED':
                                # Special handling for geometries.
                                ctype = 'geometry'

                            # Get the description for the column by index from the relation and write it.
                            cur.execute('select col_description(%s, %s)', [roid, cidx])
                            cdesc = cur.fetchone()[0]

                            trow(f, (cname, ctype, cnullable, clean(fix_null(cdesc))))

                        if cols:
                            tfoot(f)
                        else:
                            f.write('\n')


if __name__ == '__main__':

    try:
        run(out_file)

    except Exception as e:
        import traceback
        traceback.print_exception(e)
        logging.warning(str(e))
        print('Usage: db_generate_docs.py')
        sys.exit(1)
