#!/usr/bin/env python

"""
Refresh a specified list of materialized views in order.
"""
import os
import sys

up = os.path.dirname
sys.path.append(os.path.join(up(up(up(os.path.realpath(__file__)))), 'app'))

from msea.util.django import django_setup

# Set up django.
django_setup()

from django.db import connection

with connection.cursor() as cur:
	# Refresh each named view.
	print(f"Refreshing cache tables.")
	# Refresh the view.
	cur.execute(f"select cache_refresh_tables('f');")

print('Done')
