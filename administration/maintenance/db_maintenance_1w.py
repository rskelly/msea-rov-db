#!/usr/bin/env python

# This file is called by cron to perform various regular DB and site
# maintenance tasks. The task runs once per week on Saturday night.

# The file will be run as root (by cron) so paths are not inferred from the user.
# Do not edit this file without consulting Rob at robert.skelly@dfo-mpo.gc.ca

import os
import sys
import json
from datetime import datetime
from db_maxmind_update import update_geoip

from msea.util.django import django_setup
from msea.util import util

# Set up django.
django_setup()

from django.db import connection
from django.config import settings

# The config path.
print('db_mainenance_1w',)

# The root path.
repo_path = '/home/msea/git/msea-rov-db'

# Venv path.
venv_path = f'{repo_path}/.venv/bin/activate'

# The script path.
script_path = f'{repo_path}/maintenance'

# The temporary path where temp files are stored.
tmp_dir = '/tmp'

# Date for backup file.
fdate = datetime.now().strftime('%Y%m%d_%H%M%S')

# Location of backup.
bdir = '/mnt/msea_db/pg_backups'
bfile = f'{bdir}/{fdate}'

# Turn actions on or off.
mconfig = settings['MSEA_MAINTENANCE']
docs = mconfig.get('generate_docs', False)	# Generate documentation (doesn't work).
geoip = mconfig.get('update_geoip', False)  # Update the geoip database.

# Generate DB documentation.
if docs:
    print('Generating documentation.')
    os.system(f'sudo -s -H -u msea source {venv_path} && python {script_path}/db_generate_docs.py')

# Update the GeoIP database.
if geoip:
    print('Update GeoIP database.')
    update_geoip()



