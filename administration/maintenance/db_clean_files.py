#!/usr/bin/env python

"""
Deletes any file in the uploaded files directory that does not have a corresponding
uploaded_file entry. The clean_uploaded_files() procedure must be run first, to 
remove unlinked uploaded_file rows.
"""

import os
import sys

up = os.path.dirname
sys.path.append(os.path.join(up(up(up(os.path.realpath(__file__)))), 'app'))

from msea.util.django import django_setup

# Set up django.
django_setup()

from django.db import connection

from msea.models.shared import UploadedFile

with connection.cursor() as cur:

    # Execute the database cleanup.
    print(f"Deleting unlinked uploaded files.")
    cur.execute(f"select clean_uploaded_files();")

    # Find a list of files that do not have entries by inspecting the path column and file basename.
    for file in os.listdir(os.getenv('MSEA_UPLOAD_DIR')):
        uf = None
        try:
            # Find the upload file.
            uf = UploadFile.objects.get(path=file)
        except UploadedFile.DoesNotExist:
            continue # The file is not found, continue.
        except Exception as e:
            print(f'Failed to load upload file for path {file}: ' + str(e))

        # Delete the file.
        if not uf:
            ff = os.path.join(os.getenv('MSEA_UPLOAD_DIR'), file)
            os.unlink(ff)

    print('Done')
