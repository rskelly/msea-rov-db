#!/usr/bin/env python

"""
Generates documentation for the ReST API and saves it as a markdown file that can be imported into
the Wiki.
"""
import requests

api_root = 'https://msea.science/rov/api/v1'
headers = {'Content-type': 'application/json'}

def get_root():
    """
    Gets the root endpoint, which returns a dictionary endpoint names as keys and URLs as values.
    """
    r = requests.get(api_root, headers=headers)
    return r.json()

def get_endpoint(url):
    """
    Gets information about the specific endpoint.
    """
    r = requests.get(url, headers=headers)
    print(r.text)
    return r.json()

if __name__ == '__main__':

    root = get_root()

    for k, v in root.items():
        print(k)
        try:
            e = get_endpoint(v)
            print(e)
            #sys.exit(1)
        except Exception as e:
            print(e)