#!/usr/bin/env python

"""
Cleans up accounts that have not been verified after 24 hours.
"""

import os
import sys

up = os.path.dirname
sys.path.append(os.path.join(up(up(up(os.path.realpath(__file__)))), 'app'))

from msea.util import util
from msea.util.django import django_setup

django_setup()

from django.db import connection

def cleanup_accounts():
    """
    Remove accounts whose verification links have expired.
    Reset password reset fields when they've expired.
    """
    with connection.cursor() as cur:

        # Delete expired signups.
        cur.execute("""
                delete from public.auth_user 
                where verification_time is null 
                    and verification_code is not null 
                    and verification_expiry < (now() + interval '1 week')
        """)

        # Clear expired password resets.
        cur.execute("""
                update public.auth_user 
                set password_reset_code = null, password_reset_expiry = null
                where password_reset_code is not null 
                    and password_reset_expiry < (now() + interval '24 hours')
        """)


if __name__ == '__main__':

    cleanup_accounts()
