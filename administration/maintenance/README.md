# Maintenance

These are scripts for managing/maintaining/backing up the database.

## api_generate_docs.py

Generates documentation for the ReST API and saves it as a markdown file that can be imported into
the Wiki.

## db_account_cleanup.py

Cleans up accounts that have not been verified after 24 hours. Called by cron job.

## db_account_digest.py

Sends an email listing accounts that have been created over the last 24 hours. Sent to members of the `msea_signup_digest_recipient` group.

## db_clean_files.py

Deletes any rows of shared.uploaded_file that are not referenced by any other entity. Then, deletes any file
in the uploads directory that does not have a corresponding row in uploaded_file.

Uploaded files can not be stored as standalone entities, they must be referenced by some other table.

## db_generate_docs.py

Generate a complete documentation page in markup using comments saved in the database itself.

Saves the documentation to the wiki page stored in wiki.article, finding the appropriate record using the slug.

## db_maintenance_1w.py, db_maintenance_1w.sh

Performs 7 day maintenance tasks:
* Generate documentation from the PostgreSQL table documentation.
* Check and update the GeoIP database.
* TODO: Check and update the WoRMS taxonomic database. (https://gitlab.com/rskelly/msea-rov-db/-/issues/129)

The shell script is run by cron, and runs the Python script (only because what we want to do is easier in Python).

## db_maintenance_24h.py, db_maintenance_24h.sh

Performs 24 hour maintenance tasks:
* Performs database backups.
* Refreshes materialized views.
* Cleans up unverified accounts.
* Send digest of new accounts.
* Clean up temporary files.

The shell script is run by cron, and runs the Python script (only because what we want to do is easier in Python).

## db_maxmind_update.py

Checks and updates the MaxMind GeoIP database, which is loaded into tables in the `maxmind` schema.

## db_refresh_matviews.py

Refresh a specified list of materialized views in order.