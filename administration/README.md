# Administration

This folder contains items related to managing the server and development environment, and building/deploying the site.

## deployment 

Configurations and scripts relating to deployment of the site in the production environment.

* Deprecated, moved to ./docker.

## development

Scripts used for running the development environment.

* Deprecated, moved to ./docker.

## maintenance

Scripts for site maintenance. Some of these install cron jobs which perform notification and administration actions.

## utilities

Scripts for performing various tasks.

* Deprecated.
